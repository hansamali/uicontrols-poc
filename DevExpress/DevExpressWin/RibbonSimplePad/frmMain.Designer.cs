using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraSplashScreen;
namespace DevExpress.XtraBars.Demos.RibbonSimplePad {
    partial class frmMain {

        #region Designer generated code
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup2 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup3 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup4 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup5 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.iWeb = new DevExpress.XtraBars.BarButtonItem();
            this.iAbout = new DevExpress.XtraBars.BarButtonItem();
            this.iCenter = new DevExpress.XtraBars.BarButtonItem();
            this.iSelectAll = new DevExpress.XtraBars.BarButtonItem();
            this.iCopy = new DevExpress.XtraBars.BarButtonItem();
            this.iCut = new DevExpress.XtraBars.BarButtonItem();
            this.iPaste = new DevExpress.XtraBars.BarButtonItem();
            this.iClear = new DevExpress.XtraBars.BarButtonItem();
            this.iFont = new DevExpress.XtraBars.BarButtonItem();
            this.gddFont = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.beiFontSize = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bbiFontColorPopup = new DevExpress.XtraBars.BarButtonItem();
            this.popupControlContainer1 = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.pmAppMain = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.pccBottom = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.sbExit = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.idNew = new DevExpress.XtraBars.BarButtonItem();
            this.pmNew = new DevExpress.XtraBars.PopupMenu(this.components);
            this.iNew = new DevExpress.XtraBars.BarButtonItem();
            this.iTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.iOpen = new DevExpress.XtraBars.BarButtonItem();
            this.sbiSave = new DevExpress.XtraBars.BarSubItem();
            this.iSave = new DevExpress.XtraBars.BarButtonItem();
            this.iSaveAs = new DevExpress.XtraBars.BarButtonItem();
            this.iPrint = new DevExpress.XtraBars.BarButtonItem();
            this.iClose = new DevExpress.XtraBars.BarButtonItem();
            this.pccAppMenu = new DevExpress.XtraBars.Demos.RibbonSimplePad.Office2007PopupControlContainer();
            this.pcAppMenuFileLabels = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.iUndo = new DevExpress.XtraBars.BarButtonItem();
            this.iReplace = new DevExpress.XtraBars.BarButtonItem();
            this.iExit = new DevExpress.XtraBars.BarButtonItem();
            this.iFind = new DevExpress.XtraBars.BarButtonItem();
            this.iBullets = new DevExpress.XtraBars.BarButtonItem();
            this.iProtected = new DevExpress.XtraBars.BarButtonItem();
            this.iBold = new DevExpress.XtraBars.BarButtonItem();
            this.iItalic = new DevExpress.XtraBars.BarButtonItem();
            this.iUnderline = new DevExpress.XtraBars.BarButtonItem();
            this.iAlignLeft = new DevExpress.XtraBars.BarButtonItem();
            this.iAlignRight = new DevExpress.XtraBars.BarButtonItem();
            this.iFontColor = new DevExpress.XtraBars.BarButtonItem();
            this.gddFontColor = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.siPosition = new DevExpress.XtraBars.BarButtonItem();
            this.siModified = new DevExpress.XtraBars.BarButtonItem();
            this.siDocName = new DevExpress.XtraBars.BarStaticItem();
            this.bgFontStyle = new DevExpress.XtraBars.BarButtonGroup();
            this.bgAlign = new DevExpress.XtraBars.BarButtonGroup();
            this.bgFont = new DevExpress.XtraBars.BarButtonGroup();
            this.bgBullets = new DevExpress.XtraBars.BarButtonGroup();
            this.sbiPaste = new DevExpress.XtraBars.BarSubItem();
            this.iPasteSpecial = new DevExpress.XtraBars.BarButtonItem();
            this.sbiFind = new DevExpress.XtraBars.BarSubItem();
            this.iLargeUndo = new DevExpress.XtraBars.BarLargeButtonItem();
            this.iPaintStyle = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.rgbiFont = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.rgbiFontColor = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.biStyle = new DevExpress.XtraBars.BarEditItem();
            this.riicStyle = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.editButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this.beScheme = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barToggleSwitchItem1 = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.bbColorMix = new DevExpress.XtraBars.BarButtonItem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.selectionMiniToolbar = new DevExpress.XtraBars.Ribbon.RibbonMiniToolbar(this.components);
            this.ribbonPageCategory1 = new DevExpress.XtraBars.Ribbon.RibbonPageCategory();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup12 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup13 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgFont = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgFontColor = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.pmMain = new DevExpress.XtraBars.PopupMenu(this.components);
            this.imageCollection3 = new DevExpress.Utils.ImageCollection(this.components);
            this.backstageViewControl1 = new DevExpress.XtraBars.Ribbon.BackstageViewControl();
            this.backstageViewClientControl9 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.recentOpen = new DevExpress.XtraBars.Ribbon.RecentItemControl();
            this.recentStackPanel1 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentStackPanel2 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentTabItem1 = new DevExpress.XtraBars.Ribbon.RecentTabItem();
            this.recentStackPanel3 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentTabItem2 = new DevExpress.XtraBars.Ribbon.RecentTabItem();
            this.recentStackPanel4 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.backstageViewClientControl8 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.recentSaveAs = new DevExpress.XtraBars.Ribbon.RecentItemControl();
            this.recentStackPanel5 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentStackPanel6 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentTabItem3 = new DevExpress.XtraBars.Ribbon.RecentTabItem();
            this.recentStackPanel7 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.backstageViewClientControl11 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.recentControlPrint = new DevExpress.XtraBars.Ribbon.RecentItemControl();
            this.recentPrintOptionsContainer = new DevExpress.XtraBars.Ribbon.RecentControlItemControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ddbDuplex = new DevExpress.XtraEditors.DropDownButton();
            this.ddbOrientation = new DevExpress.XtraEditors.DropDownButton();
            this.ddbPaperSize = new DevExpress.XtraEditors.DropDownButton();
            this.ddbMargins = new DevExpress.XtraEditors.DropDownButton();
            this.ddbCollate = new DevExpress.XtraEditors.DropDownButton();
            this.backstageViewLabel2 = new DevExpress.XtraBars.Demos.RibbonSimplePad.BackstageViewLabel();
            this.ddbPrinter = new DevExpress.XtraEditors.DropDownButton();
            this.printerLabel = new DevExpress.XtraBars.Demos.RibbonSimplePad.BackstageViewLabel();
            this.printButton = new DevExpress.XtraEditors.SimpleButton();
            this.copySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCopiesSpinEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.recentPrintPreviewContainer = new DevExpress.XtraBars.Ribbon.RecentControlItemControlContainer();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.printControl2 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.zoomTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pageButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.zoomTrackBarControl1 = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.recentStackPanel8 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentPrintPreview = new DevExpress.XtraBars.Ribbon.RecentControlContainerItem();
            this.recentStackPanel9 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentPrintOptions = new DevExpress.XtraBars.Ribbon.RecentControlContainerItem();
            this.backstageViewClientControl10 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.recentControlExport = new DevExpress.XtraBars.Ribbon.RecentItemControl();
            this.recentStackPanel14 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentStackPanel10 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentTabItem4 = new DevExpress.XtraBars.Ribbon.RecentTabItem();
            this.recentStackPanel11 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentControlRecentItem1 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentControlRecentItem2 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentControlRecentItem4 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentControlRecentItem5 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentControlRecentItem6 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentControlRecentItem7 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentControlRecentItem8 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentControlRecentItem9 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.backstageViewClientControl2 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.recentItemControl1 = new DevExpress.XtraBars.Ribbon.RecentItemControl();
            this.recentStackPanel12 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentLabelItem1 = new DevExpress.XtraBars.Ribbon.RecentLabelItem();
            this.recentHyperlinkItem1 = new DevExpress.XtraBars.Ribbon.RecentHyperlinkItem();
            this.recentLabelItem2 = new DevExpress.XtraBars.Ribbon.RecentLabelItem();
            this.recentStackPanel13 = new DevExpress.XtraBars.Ribbon.RecentStackPanel();
            this.recentPinItem2 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentPinItem3 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentPinItem4 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.backstageViewTabItem3 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.bvItemSave = new DevExpress.XtraBars.Ribbon.BackstageViewButtonItem();
            this.backstageViewTabItem1 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewTabItem5 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewTabItem4 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewTabItem6 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.bvItemClose = new DevExpress.XtraBars.Ribbon.BackstageViewButtonItem();
            this.bvItemExit = new DevExpress.XtraBars.Ribbon.BackstageViewButtonItem();
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.recentControlRecentItem10 = new DevExpress.XtraBars.Ribbon.RecentPinItem();
            this.recentControlButtonItem1 = new DevExpress.XtraBars.Ribbon.RecentButtonItem();
            this.backstageViewClientControl7 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl1 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl3 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.taskbarAssistant1 = new DevExpress.Utils.Taskbar.TaskbarAssistant();
            this.thumbButtonNewDoc = new DevExpress.Utils.Taskbar.ThumbnailButton();
            this.thumbButtonPrev = new DevExpress.Utils.Taskbar.ThumbnailButton();
            this.thumbButtonNext = new DevExpress.Utils.Taskbar.ThumbnailButton();
            this.thumbButtonExit = new DevExpress.Utils.Taskbar.ThumbnailButton();
            this.backstageViewTabItem2 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.bvTabPrint = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewClientControl4 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            ((System.ComponentModel.ISupportInitialize)(this.gddFont)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmAppMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccBottom)).BeginInit();
            this.pccBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccAppMenu)).BeginInit();
            this.pccAppMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcAppMenuFileLabels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddFontColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riicStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backstageViewControl1)).BeginInit();
            this.backstageViewControl1.SuspendLayout();
            this.backstageViewClientControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recentOpen)).BeginInit();
            this.backstageViewClientControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recentSaveAs)).BeginInit();
            this.backstageViewClientControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recentControlPrint)).BeginInit();
            this.recentControlPrint.SuspendLayout();
            this.recentPrintOptionsContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCopiesSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.recentPrintPreviewContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).BeginInit();
            this.backstageViewClientControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recentControlExport)).BeginInit();
            this.backstageViewClientControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recentItemControl1)).BeginInit();
            this.backstageViewClientControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // iWeb
            // 
            this.iWeb.Caption = "&DevExpress on the Web";
            this.iWeb.CategoryGuid = new System.Guid("e07a4c24-66ac-4de6-bbcb-c0b6cfa7798b");
            this.iWeb.Description = "Opens the web page.";
            this.iWeb.Hint = "DevExpress on the Web";
            this.iWeb.Id = 21;
            this.iWeb.ImageIndex = 24;
            this.iWeb.Name = "iWeb";
            this.iWeb.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iWeb_ItemClick);
            // 
            // iAbout
            // 
            this.iAbout.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.iAbout.Caption = "&About";
            this.iAbout.CategoryGuid = new System.Guid("e07a4c24-66ac-4de6-bbcb-c0b6cfa7798b");
            this.iAbout.Description = "Displays the description of this program.";
            this.iAbout.Hint = "Displays the About dialog";
            this.iAbout.Id = 22;
            this.iAbout.ImageIndex = 28;
            this.iAbout.Name = "iAbout";
            this.iAbout.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.iAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAbout_ItemClick);
            // 
            // iCenter
            // 
            this.iCenter.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iCenter.Caption = "&Center";
            this.iCenter.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iCenter.Description = "Centers the selected text.";
            this.iCenter.GroupIndex = 1;
            this.iCenter.Hint = "Center";
            this.iCenter.Id = 28;
            this.iCenter.ImageIndex = 19;
            this.iCenter.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this.iCenter.Name = "iCenter";
            this.iCenter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAlign_ItemClick);
            // 
            // iSelectAll
            // 
            this.iSelectAll.Caption = "Select A&ll";
            this.iSelectAll.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iSelectAll.Description = "Selects all text in the active document.";
            this.iSelectAll.Hint = "Selects all text in the active document.";
            this.iSelectAll.Id = 13;
            this.iSelectAll.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A));
            this.iSelectAll.Name = "iSelectAll";
            this.iSelectAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSelectAll_ItemClick);
            // 
            // iCopy
            // 
            this.iCopy.Caption = "&Copy";
            this.iCopy.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iCopy.Description = "Copies the selection to the Clipboard.";
            this.iCopy.Hint = "Copy";
            this.iCopy.Id = 10;
            this.iCopy.ImageIndex = 1;
            this.iCopy.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C));
            this.iCopy.Name = "iCopy";
            this.iCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCopy_ItemClick);
            // 
            // iCut
            // 
            this.iCut.Caption = "Cu&t";
            this.iCut.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iCut.Description = "Removes the selection from the active document and places it on the Clipboard.";
            this.iCut.Hint = "Cut";
            this.iCut.Id = 9;
            this.iCut.ImageIndex = 2;
            this.iCut.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X));
            this.iCut.Name = "iCut";
            this.iCut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCut_ItemClick);
            // 
            // iPaste
            // 
            this.iPaste.Caption = "&Paste";
            this.iPaste.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iPaste.Description = "Inserts the contents of the Clipboard at the insertion point, and replaces any se" +
    "lection. This command is available only if you have cut or copied a text.";
            this.iPaste.Hint = "Paste";
            this.iPaste.Id = 11;
            this.iPaste.ImageIndex = 8;
            this.iPaste.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V));
            this.iPaste.Name = "iPaste";
            this.iPaste.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iPaste_ItemClick);
            // 
            // iClear
            // 
            this.iClear.Caption = "Cle&ar";
            this.iClear.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iClear.Description = "Deletes the selected text without putting it on the Clipboard. This command is av" +
    "ailable only if a text is selected. ";
            this.iClear.Hint = "Clear";
            this.iClear.Id = 12;
            this.iClear.ImageIndex = 13;
            this.iClear.Name = "iClear";
            this.iClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iClear_ItemClick);
            // 
            // iFont
            // 
            this.iFont.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.iFont.Caption = "&Font...";
            this.iFont.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iFont.Description = "Changes the font and character spacing formats of the selected text.";
            this.iFont.DropDownControl = this.gddFont;
            this.iFont.Hint = "Font Dialog";
            this.iFont.Id = 17;
            this.iFont.ImageIndex = 4;
            this.iFont.Name = "iFont";
            this.iFont.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.iFont.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFont_ItemClick);
            // 
            // gddFont
            // 
            // 
            // 
            // 
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseFont = true;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseTextOptions = true;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseFont = true;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseTextOptions = true;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddFont.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Hovered.Options.UseTextOptions = true;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Hovered.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Normal.Options.UseTextOptions = true;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Normal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Pressed.Options.UseTextOptions = true;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gddFont.Gallery.Appearance.ItemDescriptionAppearance.Pressed.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gddFont.Gallery.ColumnCount = 1;
            this.gddFont.Gallery.FixedImageSize = false;
            galleryItemGroup1.Caption = "Main";
            this.gddFont.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.gddFont.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Left;
            this.gddFont.Gallery.RowCount = 6;
            this.gddFont.Gallery.ShowGroupCaption = false;
            this.gddFont.Gallery.ShowItemText = true;
            this.gddFont.Gallery.SizeMode = DevExpress.XtraBars.Ribbon.GallerySizeMode.Vertical;
            this.gddFont.ItemLinks.Add(this.beiFontSize);
            this.gddFont.ItemLinks.Add(this.bbiFontColorPopup);
            this.gddFont.MenuCaption = "Fonts";
            this.gddFont.Name = "gddFont";
            this.gddFont.Ribbon = this.ribbonControl1;
            this.gddFont.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            this.gddFont.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.gddFont_Gallery_ItemClick);
            this.gddFont.GalleryCustomDrawItemText += new DevExpress.XtraBars.Ribbon.GalleryItemCustomDrawEventHandler(this.gddFont_Gallery_CustomDrawItemText);
            this.gddFont.Popup += new System.EventHandler(this.gddFont_Popup);
            // 
            // beiFontSize
            // 
            this.beiFontSize.Caption = "Font Size";
            this.beiFontSize.Edit = this.repositoryItemSpinEdit1;
            this.beiFontSize.Hint = "Font Size";
            this.beiFontSize.Id = 27;
            this.beiFontSize.Name = "beiFontSize";
            this.beiFontSize.EditValueChanged += new System.EventHandler(this.beiFontSize_EditValueChanged);
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // bbiFontColorPopup
            // 
            this.bbiFontColorPopup.ActAsDropDown = true;
            this.bbiFontColorPopup.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiFontColorPopup.Caption = "Font Color";
            this.bbiFontColorPopup.Description = "Formats the selected text with the color you click";
            this.bbiFontColorPopup.DropDownControl = this.popupControlContainer1;
            this.bbiFontColorPopup.Hint = "Formats the selected text with the color you click";
            this.bbiFontColorPopup.Id = 36;
            this.bbiFontColorPopup.Name = "bbiFontColorPopup";
            // 
            // popupControlContainer1
            // 
            this.popupControlContainer1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popupControlContainer1.Location = new System.Drawing.Point(0, 0);
            this.popupControlContainer1.Name = "popupControlContainer1";
            this.popupControlContainer1.Size = new System.Drawing.Size(0, 0);
            this.popupControlContainer1.TabIndex = 6;
            this.popupControlContainer1.Visible = false;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.AllowCustomization = true;
            this.ribbonControl1.ApplicationButtonDropDownControl = this.pmAppMain;
            this.ribbonControl1.ApplicationButtonText = null;
            this.ribbonControl1.AutoSizeItems = true;
            this.ribbonControl1.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("File", new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f")),
            new DevExpress.XtraBars.BarManagerCategory("Edit", new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1")),
            new DevExpress.XtraBars.BarManagerCategory("Format", new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258")),
            new DevExpress.XtraBars.BarManagerCategory("Help", new System.Guid("e07a4c24-66ac-4de6-bbcb-c0b6cfa7798b")),
            new DevExpress.XtraBars.BarManagerCategory("Status", new System.Guid("77795bb7-9bc5-4dd2-a297-cc758682e23d"))});
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Images = this.imageCollection2;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.iOpen,
            this.iSave,
            this.iUndo,
            this.iReplace,
            this.idNew,
            this.iClose,
            this.iSaveAs,
            this.iPrint,
            this.iExit,
            this.iCut,
            this.iCopy,
            this.iPaste,
            this.iClear,
            this.iSelectAll,
            this.iFind,
            this.iFont,
            this.iBullets,
            this.iProtected,
            this.iWeb,
            this.iAbout,
            this.iBold,
            this.iItalic,
            this.iUnderline,
            this.iAlignLeft,
            this.iCenter,
            this.iAlignRight,
            this.iFontColor,
            this.siPosition,
            this.siModified,
            this.siDocName,
            this.bgFontStyle,
            this.bgAlign,
            this.bgFont,
            this.bgBullets,
            this.sbiSave,
            this.sbiPaste,
            this.sbiFind,
            this.iPasteSpecial,
            this.iNew,
            this.iLargeUndo,
            this.iTemplate,
            this.iPaintStyle,
            this.rgbiSkins,
            this.beiFontSize,
            this.rgbiFont,
            this.bbiFontColorPopup,
            this.rgbiFontColor,
            this.barEditItem1,
            this.biStyle,
            this.editButtonGroup,
            this.beScheme,
            this.barToggleSwitchItem1,
            this.bbColorMix});
            this.ribbonControl1.LargeImages = this.imageCollection1;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 408;
            this.ribbonControl1.MiniToolbars.Add(this.selectionMiniToolbar);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.OptionsTouch.ShowTouchUISelectorInQAT = true;
            this.ribbonControl1.OptionsTouch.ShowTouchUISelectorVisibilityItemInQATMenu = true;
            this.ribbonControl1.PageCategories.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageCategory[] {
            this.ribbonPageCategory1});
            this.ribbonControl1.PageCategoryAlignment = DevExpress.XtraBars.Ribbon.RibbonPageCategoryAlignment.Right;
            this.ribbonControl1.PageHeaderItemLinks.Add(this.biStyle);
            this.ribbonControl1.PageHeaderItemLinks.Add(this.beScheme);
            this.ribbonControl1.PageHeaderItemLinks.Add(this.iAbout);
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage3});
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.iOpen);
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.iSave);
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.iUndo);
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.iReplace, true);
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.iPaintStyle);
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemPictureEdit1,
            this.riicStyle,
            this.repositoryItemComboBox1,
            this.repositoryItemTrackBar1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.Size = new System.Drawing.Size(1057, 147);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.TransparentEditors = true;
            this.ribbonControl1.BeforeApplicationButtonContentControlShow += new System.EventHandler(this.ribbonControl1_BeforeApplicationButtonContentControlShow);
            this.ribbonControl1.ApplicationButtonDoubleClick += new System.EventHandler(this.ribbonControl1_ApplicationButtonDoubleClick);
            this.ribbonControl1.ResetLayout += new System.EventHandler<DevExpress.XtraBars.Ribbon.ResetLayoutEventArgs>(this.ribbonControl1_ResetLayout);
            // 
            // pmAppMain
            // 
            this.pmAppMain.BottomPaneControlContainer = this.pccBottom;
            this.pmAppMain.ItemLinks.Add(this.idNew);
            this.pmAppMain.ItemLinks.Add(this.iOpen);
            this.pmAppMain.ItemLinks.Add(this.sbiSave, true);
            this.pmAppMain.ItemLinks.Add(this.iPrint);
            this.pmAppMain.ItemLinks.Add(this.iClose, true);
            this.pmAppMain.Name = "pmAppMain";
            this.pmAppMain.Ribbon = this.ribbonControl1;
            this.pmAppMain.RightPaneControlContainer = this.pccAppMenu;
            this.pmAppMain.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            this.pmAppMain.ShowRightPane = true;
            // 
            // pccBottom
            // 
            this.pccBottom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pccBottom.Appearance.Options.UseBackColor = true;
            this.pccBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pccBottom.Controls.Add(this.sbExit);
            this.pccBottom.Location = new System.Drawing.Point(573, 527);
            this.pccBottom.Name = "pccBottom";
            this.pccBottom.Ribbon = this.ribbonControl1;
            this.pccBottom.Size = new System.Drawing.Size(115, 28);
            this.pccBottom.TabIndex = 6;
            this.pccBottom.Visible = false;
            this.pccBottom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pccBottom.AutoSize = true;
            // 
            // sbExit
            // 
            this.sbExit.AllowFocus = false;
            this.sbExit.ImageIndex = 13;
            this.sbExit.ImageList = this.imageCollection2;
            this.sbExit.Location = new System.Drawing.Point(3, 3);
            this.sbExit.Name = "sbExit";
            this.sbExit.Size = new System.Drawing.Size(108, 21);
            this.sbExit.AutoSize = true;
            this.sbExit.TabIndex = 0;
            this.sbExit.Text = "E&xit Application";
            this.sbExit.Click += new System.EventHandler(this.sbExit_Click);
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(29, "SaveAs_16x16.png");
            // 
            // idNew
            // 
            this.idNew.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.idNew.Caption = "New";
            this.idNew.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.idNew.Description = "Creates a new, blank file.";
            this.idNew.DropDownControl = this.pmNew;
            this.idNew.Hint = "Creates a new, blank file";
            this.idNew.Id = 0;
            this.idNew.ImageIndex = 6;
            this.idNew.LargeImageIndex = 0;
            this.idNew.Name = "idNew";
            this.idNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.idNew_ItemClick);
            // 
            // pmNew
            // 
            this.pmNew.ItemLinks.Add(this.iNew);
            this.pmNew.ItemLinks.Add(this.iTemplate);
            this.pmNew.MenuCaption = "New";
            this.pmNew.Name = "pmNew";
            this.pmNew.Ribbon = this.ribbonControl1;
            this.pmNew.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // iNew
            // 
            this.iNew.Caption = "&New";
            this.iNew.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iNew.Description = "Creates a new, blank file.";
            this.iNew.Hint = "Creates a new, blank file";
            this.iNew.Id = 0;
            this.iNew.ImageIndex = 6;
            this.iNew.LargeImageIndex = 0;
            this.iNew.Name = "iNew";
            this.iNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.idNew_ItemClick);
            // 
            // iTemplate
            // 
            this.iTemplate.Caption = "Template...";
            this.iTemplate.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iTemplate.Description = "Creates a new template";
            this.iTemplate.Enabled = false;
            this.iTemplate.Hint = "Creates a new template";
            this.iTemplate.Id = 1;
            this.iTemplate.ImageIndex = 6;
            this.iTemplate.Name = "iTemplate";
            // 
            // iOpen
            // 
            this.iOpen.Caption = "&Open...";
            this.iOpen.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iOpen.Description = "Opens a file.";
            this.iOpen.Hint = "Open a file";
            this.iOpen.Id = 1;
            this.iOpen.ImageIndex = 7;
            this.iOpen.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O));
            this.iOpen.LargeImageIndex = 9;
            this.iOpen.Name = "iOpen";
            this.iOpen.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.iOpen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iOpen_ItemClick);
            // 
            // sbiSave
            // 
            this.sbiSave.Caption = "Save";
            this.sbiSave.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.sbiSave.Description = "Saves the active document";
            this.sbiSave.Hint = "Saves the active document";
            this.sbiSave.Id = 0;
            this.sbiSave.ImageIndex = 10;
            this.sbiSave.LargeImageIndex = 2;
            this.sbiSave.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.iSaveAs)});
            this.sbiSave.MenuCaption = "Save";
            this.sbiSave.Name = "sbiSave";
            this.sbiSave.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // iSave
            // 
            this.iSave.Caption = "&Save";
            this.iSave.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iSave.Description = "Saves the active document with its current file name.";
            this.iSave.Hint = "Saves the active document with its current file name";
            this.iSave.Id = 3;
            this.iSave.ImageIndex = 10;
            this.iSave.LargeImageIndex = 7;
            this.iSave.Name = "iSave";
            this.iSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.iSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSave_ItemClick);
            // 
            // iSaveAs
            // 
            this.iSaveAs.Caption = "Save &As...";
            this.iSaveAs.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iSaveAs.Description = "Saves the active document with a different file name.";
            this.iSaveAs.Hint = "Saves the active document with a different file name";
            this.iSaveAs.Id = 4;
            this.iSaveAs.ImageIndex = 21;
            this.iSaveAs.LargeImageIndex = 2;
            this.iSaveAs.Name = "iSaveAs";
            this.iSaveAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSaveAs_ItemClick);
            // 
            // iPrint
            // 
            this.iPrint.Caption = "&Print";
            this.iPrint.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iPrint.Description = "Prints the active document.";
            this.iPrint.Hint = "Prints the active document";
            this.iPrint.Id = 5;
            this.iPrint.ImageIndex = 9;
            this.iPrint.LargeImageIndex = 6;
            this.iPrint.Name = "iPrint";
            this.iPrint.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.iPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iPrint_ItemClick);
            // 
            // iClose
            // 
            this.iClose.Caption = "&Close";
            this.iClose.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iClose.Description = "Closes the active document.";
            this.iClose.Hint = "Closes the active document";
            this.iClose.Id = 2;
            this.iClose.ImageIndex = 12;
            this.iClose.LargeImageIndex = 8;
            this.iClose.Name = "iClose";
            this.iClose.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.iClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iClose_ItemClick);
            // 
            // pccAppMenu
            // 
            this.pccAppMenu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pccAppMenu.Appearance.Options.UseBackColor = true;
            this.pccAppMenu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pccAppMenu.Controls.Add(this.pcAppMenuFileLabels);
            this.pccAppMenu.Controls.Add(this.labelControl1);
            this.pccAppMenu.Controls.Add(this.panelControl1);
            this.pccAppMenu.Location = new System.Drawing.Point(573, 359);
            this.pccAppMenu.Name = "pccAppMenu";
            this.pccAppMenu.Ribbon = this.ribbonControl1;
            this.pccAppMenu.Size = new System.Drawing.Size(310, 162);
            this.pccAppMenu.TabIndex = 3;
            this.pccAppMenu.Visible = false;
            // 
            // pcAppMenuFileLabels
            // 
            this.pcAppMenuFileLabels.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pcAppMenuFileLabels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcAppMenuFileLabels.Location = new System.Drawing.Point(10, 19);
            this.pcAppMenuFileLabels.Name = "pcAppMenuFileLabels";
            this.pcAppMenuFileLabels.Size = new System.Drawing.Size(300, 143);
            this.pcAppMenuFileLabels.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl1.LineVisible = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(300, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Recent Documents:";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(10, 162);
            this.panelControl1.TabIndex = 1;
            // 
            // iUndo
            // 
            this.iUndo.Caption = "&Undo";
            this.iUndo.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iUndo.Description = "Reverses the last command or deletes the last entry you typed.";
            this.iUndo.Hint = "Undo";
            this.iUndo.Id = 8;
            this.iUndo.ImageIndex = 11;
            this.iUndo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z));
            this.iUndo.Name = "iUndo";
            this.iUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iUndo_ItemClick);
            // 
            // iReplace
            // 
            this.iReplace.Caption = "R&eplace...";
            this.iReplace.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iReplace.Description = "Searches for and replaces the specified text.";
            this.iReplace.Hint = "Replace";
            this.iReplace.Id = 15;
            this.iReplace.ImageIndex = 14;
            this.iReplace.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H));
            this.iReplace.Name = "iReplace";
            this.iReplace.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iReplace_ItemClick);
            // 
            // iExit
            // 
            this.iExit.Caption = "E&xit";
            this.iExit.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iExit.Description = "Closes this program after prompting you to save unsaved document.";
            this.iExit.Hint = "Closes this program after prompting you to save unsaved document.";
            this.iExit.Id = 6;
            this.iExit.ImageIndex = 22;
            this.iExit.LargeImageIndex = 1;
            this.iExit.Name = "iExit";
            this.iExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iExit_ItemClick);
            // 
            // iFind
            // 
            this.iFind.Caption = "&Find...";
            this.iFind.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iFind.Description = "Searches for the specified text.";
            this.iFind.Hint = "Find";
            this.iFind.Id = 14;
            this.iFind.ImageIndex = 3;
            this.iFind.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F));
            this.iFind.Name = "iFind";
            this.iFind.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFind_ItemClick);
            // 
            // iBullets
            // 
            this.iBullets.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iBullets.Caption = "&Bullets";
            this.iBullets.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iBullets.Description = "Adds bullets to or removes bullets from selected paragraphs.";
            this.iBullets.Hint = "Bullets";
            this.iBullets.Id = 18;
            this.iBullets.ImageIndex = 0;
            this.iBullets.Name = "iBullets";
            this.iBullets.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iBullets_ItemClick);
            // 
            // iProtected
            // 
            this.iProtected.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iProtected.Caption = "P&rotected";
            this.iProtected.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iProtected.Description = "Protects the selected text.";
            this.iProtected.Hint = "Protects the selected text";
            this.iProtected.Id = 19;
            this.iProtected.Name = "iProtected";
            this.iProtected.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iProtected_ItemClick);
            // 
            // iBold
            // 
            this.iBold.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iBold.Caption = "&Bold";
            this.iBold.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iBold.Description = "Makes selected text and numbers bold. If the selection is already bold, clicking " +
    "button removes bold formatting.";
            this.iBold.Hint = "Bold";
            this.iBold.Id = 24;
            this.iBold.ImageIndex = 15;
            this.iBold.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B));
            this.iBold.Name = "iBold";
            this.iBold.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFontStyle_ItemClick);
            // 
            // iItalic
            // 
            this.iItalic.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iItalic.Caption = "&Italic";
            this.iItalic.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iItalic.Description = "Makes selected text and numbers italic. If the selection is already italic, click" +
    "ing button removes italic formatting.";
            this.iItalic.Hint = "Italic";
            this.iItalic.Id = 25;
            this.iItalic.ImageIndex = 16;
            this.iItalic.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this.iItalic.Name = "iItalic";
            this.iItalic.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFontStyle_ItemClick);
            // 
            // iUnderline
            // 
            this.iUnderline.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iUnderline.Caption = "&Underline";
            this.iUnderline.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iUnderline.Description = "Underlines selected text and numbers. If the selection is already underlined, cli" +
    "cking button removes underlining.";
            this.iUnderline.Hint = "Underline";
            this.iUnderline.Id = 26;
            this.iUnderline.ImageIndex = 17;
            this.iUnderline.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U));
            this.iUnderline.Name = "iUnderline";
            this.iUnderline.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFontStyle_ItemClick);
            // 
            // iAlignLeft
            // 
            this.iAlignLeft.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iAlignLeft.Caption = "Align &Left";
            this.iAlignLeft.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iAlignLeft.Description = "Aligns the selected text to the left.";
            this.iAlignLeft.GroupIndex = 1;
            this.iAlignLeft.Hint = "Align Left";
            this.iAlignLeft.Id = 27;
            this.iAlignLeft.ImageIndex = 18;
            this.iAlignLeft.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L));
            this.iAlignLeft.Name = "iAlignLeft";
            this.iAlignLeft.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAlign_ItemClick);
            // 
            // iAlignRight
            // 
            this.iAlignRight.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iAlignRight.Caption = "Align &Right";
            this.iAlignRight.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iAlignRight.Description = "Aligns the selected text to the right.";
            this.iAlignRight.GroupIndex = 1;
            this.iAlignRight.Hint = "Align Right";
            this.iAlignRight.Id = 29;
            this.iAlignRight.ImageIndex = 20;
            this.iAlignRight.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R));
            this.iAlignRight.Name = "iAlignRight";
            this.iAlignRight.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAlign_ItemClick);
            // 
            // iFontColor
            // 
            this.iFontColor.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.iFontColor.Caption = "Font C&olor";
            this.iFontColor.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iFontColor.Description = "Formats the selected text with the color you click.";
            this.iFontColor.DropDownControl = this.gddFontColor;
            this.iFontColor.Hint = "Font Color";
            this.iFontColor.Id = 30;
            this.iFontColor.ImageIndex = 5;
            this.iFontColor.Name = "iFontColor";
            this.iFontColor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFontColor_ItemClick);
            // 
            // gddFontColor
            // 
            // 
            // 
            // 
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseFont = true;
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseFont = true;
            this.gddFontColor.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gddFontColor.Gallery.FilterCaption = "All Colors";
            this.gddFontColor.Gallery.FixedImageSize = false;
            galleryItemGroup2.Caption = "Web Colors";
            galleryItemGroup3.Caption = "System Colors";
            this.gddFontColor.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup2,
            galleryItemGroup3});
            this.gddFontColor.Gallery.ImageSize = new System.Drawing.Size(48, 16);
            this.gddFontColor.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top;
            this.gddFontColor.Gallery.RowCount = 5;
            this.gddFontColor.Gallery.ShowItemText = true;
            this.gddFontColor.Gallery.SizeMode = DevExpress.XtraBars.Ribbon.GallerySizeMode.Both;
            this.gddFontColor.MenuCaption = "Font Colors";
            this.gddFontColor.Name = "gddFontColor";
            this.gddFontColor.Ribbon = this.ribbonControl1;
            this.gddFontColor.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            this.gddFontColor.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.gddFontColor_Gallery_ItemClick);
            this.gddFontColor.GalleryCustomDrawItemImage += new DevExpress.XtraBars.Ribbon.GalleryItemCustomDrawEventHandler(this.gddFontColor_Gallery_CustomDrawItemImage);
            this.gddFontColor.Popup += new System.EventHandler(this.gddFontColor_Popup);
            // 
            // siPosition
            // 
            this.siPosition.CategoryGuid = new System.Guid("77795bb7-9bc5-4dd2-a297-cc758682e23d");
            this.siPosition.Id = 0;
            this.siPosition.Name = "siPosition";
            // 
            // siModified
            // 
            this.siModified.CategoryGuid = new System.Guid("77795bb7-9bc5-4dd2-a297-cc758682e23d");
            this.siModified.Id = 1;
            this.siModified.ImageIndex = 27;
            this.siModified.Name = "siModified";
            // 
            // siDocName
            // 
            this.siDocName.CategoryGuid = new System.Guid("77795bb7-9bc5-4dd2-a297-cc758682e23d");
            this.siDocName.Id = 2;
            this.siDocName.Name = "siDocName";
            this.siDocName.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bgFontStyle
            // 
            this.bgFontStyle.Caption = "FontStyle";
            this.bgFontStyle.Id = 0;
            this.bgFontStyle.ItemLinks.Add(this.iBold);
            this.bgFontStyle.ItemLinks.Add(this.iItalic);
            this.bgFontStyle.ItemLinks.Add(this.iUnderline);
            this.bgFontStyle.Name = "bgFontStyle";
            this.bgFontStyle.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bgAlign
            // 
            this.bgAlign.Caption = "Align";
            this.bgAlign.Id = 0;
            this.bgAlign.ItemLinks.Add(this.iAlignLeft);
            this.bgAlign.ItemLinks.Add(this.iCenter);
            this.bgAlign.ItemLinks.Add(this.iAlignRight);
            this.bgAlign.Name = "bgAlign";
            this.bgAlign.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bgFont
            // 
            this.bgFont.Caption = "Font";
            this.bgFont.Id = 0;
            this.bgFont.ItemLinks.Add(this.iFont);
            this.bgFont.ItemLinks.Add(this.iFontColor);
            this.bgFont.Name = "bgFont";
            this.bgFont.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bgBullets
            // 
            this.bgBullets.Caption = "Bullets";
            this.bgBullets.Id = 1;
            this.bgBullets.ItemLinks.Add(this.iBullets);
            this.bgBullets.ItemLinks.Add(this.iProtected);
            this.bgBullets.Name = "bgBullets";
            this.bgBullets.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // sbiPaste
            // 
            this.sbiPaste.Caption = "Paste";
            this.sbiPaste.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.sbiPaste.Description = "Inserts the contents of the Clipboard at the insertion point";
            this.sbiPaste.Hint = "Inserts the contents of the Clipboard at the insertion point";
            this.sbiPaste.Id = 1;
            this.sbiPaste.ImageIndex = 8;
            this.sbiPaste.LargeImageIndex = 3;
            this.sbiPaste.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iPaste),
            new DevExpress.XtraBars.LinkPersistInfo(this.iPasteSpecial)});
            this.sbiPaste.MenuCaption = "Paste";
            this.sbiPaste.Name = "sbiPaste";
            this.sbiPaste.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // iPasteSpecial
            // 
            this.iPasteSpecial.Caption = "Paste &Special...";
            this.iPasteSpecial.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iPasteSpecial.Description = "Opens the Paste Special dialog";
            this.iPasteSpecial.Enabled = false;
            this.iPasteSpecial.Hint = "Opens the Paste Special dialog";
            this.iPasteSpecial.Id = 3;
            this.iPasteSpecial.ImageIndex = 8;
            this.iPasteSpecial.Name = "iPasteSpecial";
            // 
            // sbiFind
            // 
            this.sbiFind.Caption = "Find";
            this.sbiFind.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.sbiFind.Description = "Searches for the specified text";
            this.sbiFind.Hint = "Searches for the specified text";
            this.sbiFind.Id = 2;
            this.sbiFind.ImageIndex = 3;
            this.sbiFind.LargeImageIndex = 4;
            this.sbiFind.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iFind),
            new DevExpress.XtraBars.LinkPersistInfo(this.iReplace)});
            this.sbiFind.MenuCaption = "Find and Replace";
            this.sbiFind.Name = "sbiFind";
            this.sbiFind.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.sbiFind.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // iLargeUndo
            // 
            this.iLargeUndo.Caption = "&Undo";
            this.iLargeUndo.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iLargeUndo.Hint = "Undo";
            this.iLargeUndo.Id = 0;
            this.iLargeUndo.ImageIndex = 11;
            this.iLargeUndo.LargeImageIndex = 5;
            this.iLargeUndo.Name = "iLargeUndo";
            this.iLargeUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iUndo_ItemClick);
            // 
            // iPaintStyle
            // 
            this.iPaintStyle.ActAsDropDown = true;
            this.iPaintStyle.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.iPaintStyle.Caption = "Paint style";
            this.iPaintStyle.Description = "Select a paint scheme";
            this.iPaintStyle.Hint = "Select a paint scheme";
            this.iPaintStyle.Id = 7;
            this.iPaintStyle.ImageIndex = 26;
            this.iPaintStyle.Name = "iPaintStyle";
            // 
            // rgbiSkins
            // 
            this.rgbiSkins.Caption = "Skins";
            // 
            // 
            // 
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Id = 13;
            this.rgbiSkins.Name = "rgbiSkins";
            // 
            // rgbiFont
            // 
            this.rgbiFont.Caption = "Font";
            // 
            // 
            // 
            galleryItemGroup4.Caption = "Main";
            this.rgbiFont.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup4});
            this.rgbiFont.Gallery.ImageSize = new System.Drawing.Size(40, 40);
            this.rgbiFont.Gallery.ItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.rgbiFont_Gallery_ItemClick);
            this.rgbiFont.GalleryDropDown = this.gddFont;
            this.rgbiFont.Id = 29;
            this.rgbiFont.Name = "rgbiFont";
            // 
            // rgbiFontColor
            // 
            this.rgbiFontColor.Caption = "Color";
            // 
            // 
            // 
            this.rgbiFontColor.Gallery.ColumnCount = 10;
            galleryItemGroup5.Caption = "Main";
            this.rgbiFontColor.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup5});
            this.rgbiFontColor.Gallery.ImageSize = new System.Drawing.Size(20, 14);
            this.rgbiFontColor.Gallery.ItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.rgbiFontColor_Gallery_ItemClick);
            this.rgbiFontColor.Gallery.CustomDrawItemImage += new DevExpress.XtraBars.Ribbon.GalleryItemCustomDrawEventHandler(this.gddFontColor_Gallery_CustomDrawItemImage);
            this.rgbiFontColor.GalleryDropDown = this.gddFontColor;
            this.rgbiFontColor.Id = 37;
            this.rgbiFontColor.Name = "rgbiFontColor";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItem1.CanOpenEdit = false;
            this.barEditItem1.Edit = this.repositoryItemPictureEdit1;
            this.barEditItem1.EditWidth = 130;
            this.barEditItem1.Id = 94;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.barEditItem1_ItemPress);
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.AllowFocused = false;
            this.repositoryItemPictureEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // biStyle
            // 
            this.biStyle.Edit = this.riicStyle;
            this.biStyle.EditWidth = 75;
            this.biStyle.Hint = "Ribbon Style";
            this.biStyle.Id = 106;
            this.biStyle.Name = "biStyle";
            this.biStyle.EditValueChanged += new System.EventHandler(this.biStyle_EditValueChanged);
            // 
            // riicStyle
            // 
            this.riicStyle.Appearance.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.riicStyle.Appearance.Options.UseFont = true;
            this.riicStyle.AutoHeight = false;
            this.riicStyle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riicStyle.Name = "riicStyle";
            // 
            // editButtonGroup
            // 
            this.editButtonGroup.Id = 145;
            this.editButtonGroup.ItemLinks.Add(this.iCut);
            this.editButtonGroup.ItemLinks.Add(this.iCopy);
            this.editButtonGroup.ItemLinks.Add(this.iPaste);
            this.editButtonGroup.ItemLinks.Add(this.iClear);
            this.editButtonGroup.Name = "editButtonGroup";
            this.editButtonGroup.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // beScheme
            // 
            this.beScheme.Edit = this.repositoryItemComboBox1;
            this.beScheme.EditWidth = 75;
            this.beScheme.Id = 188;
            this.beScheme.Name = "beScheme";
            this.beScheme.EditValueChanged += new System.EventHandler(this.beScheme_EditValueChanged);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // barToggleSwitchItem1
            // 
            this.barToggleSwitchItem1.Caption = "Auto Save";
            this.barToggleSwitchItem1.Id = 213;
            this.barToggleSwitchItem1.Name = "barToggleSwitchItem1";
            this.barToggleSwitchItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbColorMix
            // 
            this.bbColorMix.Caption = "&Color Mix";
            this.bbColorMix.Glyph = ((System.Drawing.Image)(resources.GetObject("bbColorMix.Glyph")));
            this.bbColorMix.Id = 238;
            this.bbColorMix.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbColorMix.LargeGlyph")));
            this.bbColorMix.LargeImageIndex = 0;
            this.bbColorMix.Name = "bbColorMix";
            this.bbColorMix.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbColorMix_ItemClick);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // selectionMiniToolbar
            // 
            this.selectionMiniToolbar.Alignment = System.Drawing.ContentAlignment.TopRight;
            this.selectionMiniToolbar.ItemLinks.Add(this.bgFont);
            this.selectionMiniToolbar.ItemLinks.Add(this.bgFontStyle);
            this.selectionMiniToolbar.ItemLinks.Add(this.bgAlign);
            this.selectionMiniToolbar.ItemLinks.Add(this.editButtonGroup);
            this.selectionMiniToolbar.ParentControl = this;
            // 
            // ribbonPageCategory1
            // 
            this.ribbonPageCategory1.Name = "ribbonPageCategory1";
            this.ribbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage4});
            this.ribbonPageCategory1.Text = "Selection";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup12,
            this.ribbonPageGroup13});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "Selection";
            // 
            // ribbonPageGroup12
            // 
            this.ribbonPageGroup12.ItemLinks.Add(this.sbiPaste);
            this.ribbonPageGroup12.ItemLinks.Add(this.iCut, true);
            this.ribbonPageGroup12.ItemLinks.Add(this.iCopy);
            this.ribbonPageGroup12.ItemLinks.Add(this.iClear);
            this.ribbonPageGroup12.Name = "ribbonPageGroup12";
            this.ribbonPageGroup12.ShowCaptionButton = false;
            this.ribbonPageGroup12.Text = "Edit";
            // 
            // ribbonPageGroup13
            // 
            this.ribbonPageGroup13.ItemLinks.Add(this.iBold);
            this.ribbonPageGroup13.ItemLinks.Add(this.iItalic);
            this.ribbonPageGroup13.ItemLinks.Add(this.iUnderline);
            this.ribbonPageGroup13.ItemLinks.Add(this.rgbiFont);
            this.ribbonPageGroup13.ItemLinks.Add(this.rgbiFontColor);
            this.ribbonPageGroup13.Name = "ribbonPageGroup13";
            this.ribbonPageGroup13.ShowCaptionButton = false;
            this.ribbonPageGroup13.Text = "Format";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup10,
            this.ribbonPageGroup4,
            this.ribbonPageGroup9});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Home";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ImageIndex = 1;
            this.ribbonPageGroup1.ItemLinks.Add(this.idNew);
            this.ribbonPageGroup1.ItemLinks.Add(this.iOpen);
            this.ribbonPageGroup1.ItemLinks.Add(this.iClose);
            this.ribbonPageGroup1.ItemLinks.Add(this.iPrint);
            this.ribbonPageGroup1.ItemLinks.Add(this.barToggleSwitchItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.sbiSave, true);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            toolTipTitleItem1.Text = "Open File Dialog";
            toolTipItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipItem1.Appearance.Options.UseImage = true;
            toolTipItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipItem1.Image")));
            toolTipItem1.Text = "Show the Open file dialog box";
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "For more information see help";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.ribbonPageGroup1.SuperTip = superToolTip1;
            this.ribbonPageGroup1.Text = "File";
            this.ribbonPageGroup1.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup1_CaptionButtonClick);
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ImageIndex = 2;
            this.ribbonPageGroup2.ItemLinks.Add(this.sbiPaste);
            this.ribbonPageGroup2.ItemLinks.Add(this.iCut);
            this.ribbonPageGroup2.ItemLinks.Add(this.iCopy);
            this.ribbonPageGroup2.ItemLinks.Add(this.iClear);
            this.ribbonPageGroup2.ItemLinks.Add(this.iUndo, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.iSelectAll);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            toolTipTitleItem3.Text = "Edit Popup Menu";
            toolTipItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipItem2.Appearance.Options.UseImage = true;
            toolTipItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipItem2.Image")));
            toolTipItem2.Text = "Show the Edit popup menu";
            superToolTip2.Items.Add(toolTipTitleItem3);
            superToolTip2.Items.Add(toolTipItem2);
            this.ribbonPageGroup2.SuperTip = superToolTip2;
            this.ribbonPageGroup2.Text = "Edit";
            this.ribbonPageGroup2.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup2_CaptionButtonClick);
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ImageIndex = 26;
            this.ribbonPageGroup3.ItemLinks.Add(this.bgFontStyle);
            this.ribbonPageGroup3.ItemLinks.Add(this.bgFont);
            this.ribbonPageGroup3.ItemLinks.Add(this.bgBullets);
            this.ribbonPageGroup3.ItemLinks.Add(this.bgAlign);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Format";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.ItemLinks.Add(this.rgbiSkins);
            this.ribbonPageGroup10.ItemLinks.Add(this.bbColorMix);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.ShowCaptionButton = false;
            this.ribbonPageGroup10.Text = "Skins";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ImageIndex = 3;
            this.ribbonPageGroup4.ItemLinks.Add(this.sbiFind);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "Find";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.AllowTextClipping = false;
            this.ribbonPageGroup9.ImageIndex = 22;
            this.ribbonPageGroup9.ItemLinks.Add(this.iExit);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            toolTipTitleItem4.Text = "Save File Dialog";
            toolTipItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipItem3.Appearance.Options.UseImage = true;
            toolTipItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipItem3.Image")));
            toolTipItem3.Text = "Show the Save file dialog box";
            superToolTip3.Items.Add(toolTipTitleItem4);
            superToolTip3.Items.Add(toolTipItem3);
            this.ribbonPageGroup9.SuperTip = superToolTip3;
            this.ribbonPageGroup9.Text = "Exit";
            this.ribbonPageGroup9.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.ribbonPageGroup9_CaptionButtonClick);
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup5,
            this.ribbonPageGroup6,
            this.ribbonPageGroup7,
            this.ribbonPageGroup8});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Alternative Page";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ImageIndex = 1;
            this.ribbonPageGroup5.ItemLinks.Add(this.idNew);
            this.ribbonPageGroup5.ItemLinks.Add(this.iOpen);
            this.ribbonPageGroup5.ItemLinks.Add(this.iSave);
            this.ribbonPageGroup5.ItemLinks.Add(this.iPrint);
            this.ribbonPageGroup5.ItemLinks.Add(this.iCut, true);
            this.ribbonPageGroup5.ItemLinks.Add(this.iCopy);
            this.ribbonPageGroup5.ItemLinks.Add(this.iPaste);
            this.ribbonPageGroup5.ItemLinks.Add(this.iLargeUndo);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Standard";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ImageIndex = 26;
            this.ribbonPageGroup6.ItemLinks.Add(this.iBold);
            this.ribbonPageGroup6.ItemLinks.Add(this.iItalic);
            this.ribbonPageGroup6.ItemLinks.Add(this.iUnderline);
            this.ribbonPageGroup6.ItemLinks.Add(this.iAlignLeft, true);
            this.ribbonPageGroup6.ItemLinks.Add(this.iCenter);
            this.ribbonPageGroup6.ItemLinks.Add(this.iAlignRight);
            this.ribbonPageGroup6.ItemLinks.Add(this.iBullets, true);
            this.ribbonPageGroup6.ItemLinks.Add(this.iProtected);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            this.ribbonPageGroup6.Text = "Format";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ImageIndex = 4;
            this.ribbonPageGroup7.ItemLinks.Add(this.iFont);
            this.ribbonPageGroup7.ItemLinks.Add(this.iFontColor);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.ShowCaptionButton = false;
            this.ribbonPageGroup7.Text = "Font";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ImageIndex = 25;
            this.ribbonPageGroup8.ItemLinks.Add(this.iWeb);
            this.ribbonPageGroup8.ItemLinks.Add(this.iAbout);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.ShowCaptionButton = false;
            this.ribbonPageGroup8.Text = "Help";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup11,
            this.rpgFont,
            this.rpgFontColor});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Gallery Page";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.rgbiSkins);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.ShowCaptionButton = false;
            this.ribbonPageGroup11.Text = "Skins";
            // 
            // rpgFont
            // 
            this.rpgFont.ItemLinks.Add(this.rgbiFont);
            this.rpgFont.Name = "rpgFont";
            toolTipTitleItem5.Text = "Font Dialog";
            toolTipItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipItem4.Appearance.Options.UseImage = true;
            toolTipItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipItem4.Image")));
            toolTipItem4.Text = "Show the Font dialog box";
            superToolTip4.Items.Add(toolTipTitleItem5);
            superToolTip4.Items.Add(toolTipItem4);
            this.rpgFont.SuperTip = superToolTip4;
            this.rpgFont.Text = "Font";
            this.rpgFont.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.rpgFont_CaptionButtonClick);
            // 
            // rpgFontColor
            // 
            this.rpgFontColor.ItemLinks.Add(this.rgbiFontColor);
            this.rpgFontColor.Name = "rpgFontColor";
            toolTipTitleItem6.Text = "Color Edit";
            toolTipItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipItem5.Appearance.Options.UseImage = true;
            toolTipItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipItem5.Image")));
            toolTipItem5.Text = "Show the Color edit popup";
            superToolTip5.Items.Add(toolTipTitleItem6);
            superToolTip5.Items.Add(toolTipItem5);
            this.rpgFontColor.SuperTip = superToolTip5;
            this.rpgFontColor.Text = "Font Color";
            this.rpgFontColor.CaptionButtonClick += new DevExpress.XtraBars.Ribbon.RibbonPageGroupEventHandler(this.rpgFontColor_CaptionButtonClick);
            // 
            // repositoryItemTrackBar1
            // 
            this.repositoryItemTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemTrackBar1.LabelAppearance.Options.UseTextOptions = true;
            this.repositoryItemTrackBar1.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTrackBar1.Maximum = 1000;
            this.repositoryItemTrackBar1.Name = "repositoryItemTrackBar1";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.siPosition);
            this.ribbonStatusBar1.ItemLinks.Add(this.siModified, true);
            this.ribbonStatusBar1.ItemLinks.Add(this.siDocName, true);
            this.ribbonStatusBar1.ItemLinks.Add(this.barEditItem1);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 718);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1057, 23);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Black";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.FloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabbedMdiManager1.FloatOnDrag = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.PageAdded += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.OnTabbedMdiManagerPageCollectionChanged);
            this.xtraTabbedMdiManager1.PageRemoved += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.OnTabbedMdiManagerPageCollectionChanged);
            this.xtraTabbedMdiManager1.FloatMDIChildActivated += new System.EventHandler(this.xtraTabbedMdiManager1_FloatMDIChildActivated);
            this.xtraTabbedMdiManager1.FloatMDIChildDeactivated += new System.EventHandler(this.xtraTabbedMdiManager1_FloatMDIChildDeactivated);
            // 
            // pmMain
            // 
            this.pmMain.ItemLinks.Add(this.iUndo);
            this.pmMain.ItemLinks.Add(this.iCopy, true);
            this.pmMain.ItemLinks.Add(this.iCut);
            this.pmMain.ItemLinks.Add(this.iPaste);
            this.pmMain.ItemLinks.Add(this.iClear);
            this.pmMain.ItemLinks.Add(this.iSelectAll);
            this.pmMain.ItemLinks.Add(this.iFont, true);
            this.pmMain.ItemLinks.Add(this.iBullets);
            this.pmMain.MenuCaption = "Edit Menu";
            this.pmMain.MultiColumn = DevExpress.Utils.DefaultBoolean.True;
            this.pmMain.Name = "pmMain";
            this.pmMain.OptionsMultiColumn.ShowItemText = DevExpress.Utils.DefaultBoolean.True;
            this.pmMain.Ribbon = this.ribbonControl1;
            this.pmMain.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.False;
            // 
            // imageCollection3
            // 
            this.imageCollection3.ImageSize = new System.Drawing.Size(15, 15);
            this.imageCollection3.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection3.ImageStream")));
            // 
            // backstageViewControl1
            // 
            this.backstageViewControl1.BackstageViewShowRibbonItems = ((DevExpress.XtraBars.Ribbon.BackstageViewShowRibbonItems)((DevExpress.XtraBars.Ribbon.BackstageViewShowRibbonItems.FormButtons | DevExpress.XtraBars.Ribbon.BackstageViewShowRibbonItems.Title)));
            this.backstageViewControl1.ColorScheme = DevExpress.XtraBars.Ribbon.RibbonControlColorScheme.Yellow;
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl8);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl9);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl11);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl10);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl2);
            this.backstageViewControl1.Images = this.imageCollection2;
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem3);
            this.backstageViewControl1.Items.Add(this.bvItemSave);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem1);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem5);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem4);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem6);
            this.backstageViewControl1.Items.Add(this.bvItemClose);
            this.backstageViewControl1.Items.Add(this.bvItemExit);
            this.backstageViewControl1.Location = new System.Drawing.Point(12, 91);
            this.backstageViewControl1.Name = "backstageViewControl1";
            this.backstageViewControl1.Office2013StyleOptions.RightPaneContentVerticalOffset = 70;
            this.backstageViewControl1.OwnerControl = this.ribbonControl1;
            this.backstageViewControl1.SelectedTab = this.backstageViewTabItem3;
            this.backstageViewControl1.SelectedTabIndex = 0;
            this.backstageViewControl1.Size = new System.Drawing.Size(967, 611);
            this.backstageViewControl1.TabIndex = 9;
            this.backstageViewControl1.Text = "backstageViewControl1";
            // 
            // backstageViewClientControl9
            // 
            this.backstageViewClientControl9.Controls.Add(this.recentOpen);
            this.backstageViewClientControl9.Location = new System.Drawing.Point(133, 71);
            this.backstageViewClientControl9.Name = "backstageViewClientControl9";
            this.backstageViewClientControl9.Size = new System.Drawing.Size(833, 539);
            this.backstageViewClientControl9.TabIndex = 7;
            // 
            // recentOpen
            // 
            this.recentOpen.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.recentOpen.ContentPanelMinWidth = 100;
            this.recentOpen.DefaultContentPanel = this.recentStackPanel1;
            this.recentOpen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recentOpen.Location = new System.Drawing.Point(0, 0);
            this.recentOpen.MainPanel = this.recentStackPanel2;
            this.recentOpen.MainPanelMinWidth = 100;
            this.recentOpen.MinimumSize = new System.Drawing.Size(400, 200);
            this.recentOpen.Name = "recentOpen";
            this.recentOpen.SelectedTab = this.recentTabItem2;
            this.recentOpen.ShowTitle = false;
            this.recentOpen.Size = new System.Drawing.Size(833, 539);
            this.recentOpen.TabIndex = 0;
            this.recentOpen.Text = "recentItemControl1";
            this.recentOpen.Title = "Open";
            this.recentOpen.ItemClick += new DevExpress.XtraBars.Ribbon.RecentItemEventHandler(this.recentControlOpen_ItemClick);
            // 
            // recentStackPanel1
            // 
            this.recentStackPanel1.Caption = "Computer";
            this.recentStackPanel1.CaptionToContentIndent = 46;
            this.recentStackPanel1.Name = "recentStackPanel1";
            // 
            // recentStackPanel2
            // 
            this.recentStackPanel2.Caption = "Open";
            this.recentStackPanel2.CaptionToContentIndent = 46;
            this.recentStackPanel2.Items.AddRange(new DevExpress.XtraBars.Ribbon.RecentItemBase[] {
            this.recentTabItem1,
            this.recentTabItem2});
            this.recentStackPanel2.Name = "recentStackPanel2";
            // 
            // recentTabItem1
            // 
            this.recentTabItem1.Caption = "Recent Documents";
            this.recentTabItem1.Name = "recentTabItem1";
            this.recentTabItem1.SuperTip = null;
            this.recentTabItem1.TabPanel = this.recentStackPanel3;
            // 
            // recentStackPanel3
            // 
            this.recentStackPanel3.Caption = "Recent Documents";
            this.recentStackPanel3.CaptionToContentIndent = 46;
            this.recentStackPanel3.Name = "recentStackPanel3";
            // 
            // recentTabItem2
            // 
            this.recentTabItem2.Caption = "Computer";
            this.recentTabItem2.Name = "recentTabItem2";
            this.recentTabItem2.SuperTip = null;
            this.recentTabItem2.TabPanel = this.recentStackPanel4;
            // 
            // recentStackPanel4
            // 
            this.recentStackPanel4.Caption = "Computer";
            this.recentStackPanel4.CaptionToContentIndent = 46;
            this.recentStackPanel4.Name = "recentStackPanel4";
            // 
            // backstageViewClientControl8
            // 
            this.backstageViewClientControl8.Controls.Add(this.recentSaveAs);
            this.backstageViewClientControl8.Location = new System.Drawing.Point(133, 71);
            this.backstageViewClientControl8.Name = "backstageViewClientControl8";
            this.backstageViewClientControl8.Size = new System.Drawing.Size(833, 539);
            this.backstageViewClientControl8.TabIndex = 6;
            // 
            // recentSaveAs
            // 
            this.recentSaveAs.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.recentSaveAs.ContentPanelMinWidth = 100;
            this.recentSaveAs.DefaultContentPanel = this.recentStackPanel5;
            this.recentSaveAs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recentSaveAs.Location = new System.Drawing.Point(0, 0);
            this.recentSaveAs.MainPanel = this.recentStackPanel6;
            this.recentSaveAs.MainPanelMinWidth = 100;
            this.recentSaveAs.MinimumSize = new System.Drawing.Size(400, 200);
            this.recentSaveAs.Name = "recentSaveAs";
            this.recentSaveAs.SelectedTab = this.recentTabItem3;
            this.recentSaveAs.ShowTitle = false;
            this.recentSaveAs.Size = new System.Drawing.Size(833, 539);
            this.recentSaveAs.TabIndex = 0;
            this.recentSaveAs.Text = "recentItemControl1";
            this.recentSaveAs.Title = "Save As";
            this.recentSaveAs.ItemClick += new DevExpress.XtraBars.Ribbon.RecentItemEventHandler(this.recentControlSave_ItemClick);
            // 
            // recentStackPanel5
            // 
            this.recentStackPanel5.Caption = "Computer";
            this.recentStackPanel5.CaptionToContentIndent = 46;
            this.recentStackPanel5.Name = "recentStackPanel5";
            // 
            // recentStackPanel6
            // 
            this.recentStackPanel6.Caption = "Save As";
            this.recentStackPanel6.CaptionToContentIndent = 46;
            this.recentStackPanel6.Items.AddRange(new DevExpress.XtraBars.Ribbon.RecentItemBase[] {
            this.recentTabItem3});
            this.recentStackPanel6.Name = "recentStackPanel6";
            // 
            // recentTabItem3
            // 
            this.recentTabItem3.Caption = "Computer";
            this.recentTabItem3.Name = "recentTabItem3";
            this.recentTabItem3.SuperTip = null;
            this.recentTabItem3.TabPanel = this.recentStackPanel7;
            // 
            // recentStackPanel7
            // 
            this.recentStackPanel7.Caption = "Computer";
            this.recentStackPanel7.CaptionToContentIndent = 46;
            this.recentStackPanel7.Name = "recentStackPanel7";
            // 
            // backstageViewClientControl11
            // 
            this.backstageViewClientControl11.Controls.Add(this.recentControlPrint);
            this.backstageViewClientControl11.Location = new System.Drawing.Point(133, 71);
            this.backstageViewClientControl11.Name = "backstageViewClientControl11";
            this.backstageViewClientControl11.Size = new System.Drawing.Size(833, 539);
            this.backstageViewClientControl11.TabIndex = 9;
            // 
            // recentControlPrint
            // 
            this.recentControlPrint.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.recentControlPrint.ContentPanelMinWidth = 100;
            this.recentControlPrint.Controls.Add(this.recentPrintOptionsContainer);
            this.recentControlPrint.Controls.Add(this.recentPrintPreviewContainer);
            this.recentControlPrint.DefaultContentPanel = this.recentStackPanel8;
            this.recentControlPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recentControlPrint.Location = new System.Drawing.Point(0, 0);
            this.recentControlPrint.MainPanel = this.recentStackPanel9;
            this.recentControlPrint.MainPanelMinWidth = 100;
            this.recentControlPrint.Name = "recentControlPrint";
            this.recentControlPrint.SelectedTab = null;
            this.recentControlPrint.ShowTitle = false;
            this.recentControlPrint.Size = new System.Drawing.Size(833, 539);
            this.recentControlPrint.TabIndex = 0;
            this.recentControlPrint.Text = "recentControl1";
            this.recentControlPrint.Title = "Print";
            // 
            // recentPrintOptionsContainer
            // 
            this.recentPrintOptionsContainer.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.recentPrintOptionsContainer.Appearance.Options.UseBackColor = true;
            this.recentPrintOptionsContainer.Controls.Add(this.layoutControl1);
            this.recentPrintOptionsContainer.Name = "recentPrintOptionsContainer";
            this.recentPrintOptionsContainer.Size = new System.Drawing.Size(267, 441);
            this.recentPrintOptionsContainer.TabIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.ddbDuplex);
            this.layoutControl1.Controls.Add(this.ddbOrientation);
            this.layoutControl1.Controls.Add(this.ddbPaperSize);
            this.layoutControl1.Controls.Add(this.ddbMargins);
            this.layoutControl1.Controls.Add(this.ddbCollate);
            this.layoutControl1.Controls.Add(this.backstageViewLabel2);
            this.layoutControl1.Controls.Add(this.ddbPrinter);
            this.layoutControl1.Controls.Add(this.printerLabel);
            this.layoutControl1.Controls.Add(this.printButton);
            this.layoutControl1.Controls.Add(this.copySpinEdit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(267, 441);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ddbDuplex
            // 
            this.ddbDuplex.Appearance.Options.UseTextOptions = true;
            this.ddbDuplex.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbDuplex.Location = new System.Drawing.Point(2, 186);
            this.ddbDuplex.Name = "ddbDuplex";
            this.ddbDuplex.Size = new System.Drawing.Size(260, 52);
            this.ddbDuplex.StyleController = this.layoutControl1;
            this.ddbDuplex.TabIndex = 17;
            this.ddbDuplex.Text = "Print OneSided";
            // 
            // ddbOrientation
            // 
            this.ddbOrientation.Appearance.Options.UseTextOptions = true;
            this.ddbOrientation.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbOrientation.Location = new System.Drawing.Point(2, 242);
            this.ddbOrientation.Name = "ddbOrientation";
            this.ddbOrientation.Size = new System.Drawing.Size(260, 52);
            this.ddbOrientation.StyleController = this.layoutControl1;
            this.ddbOrientation.TabIndex = 13;
            this.ddbOrientation.Text = "Orientation";
            // 
            // ddbPaperSize
            // 
            this.ddbPaperSize.Appearance.Options.UseTextOptions = true;
            this.ddbPaperSize.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbPaperSize.Location = new System.Drawing.Point(2, 298);
            this.ddbPaperSize.Name = "ddbPaperSize";
            this.ddbPaperSize.Size = new System.Drawing.Size(260, 52);
            this.ddbPaperSize.StyleController = this.layoutControl1;
            this.ddbPaperSize.TabIndex = 15;
            this.ddbPaperSize.Text = "Paper Size";
            // 
            // ddbMargins
            // 
            this.ddbMargins.Appearance.Options.UseTextOptions = true;
            this.ddbMargins.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbMargins.Location = new System.Drawing.Point(2, 354);
            this.ddbMargins.Name = "ddbMargins";
            this.ddbMargins.Size = new System.Drawing.Size(260, 52);
            this.ddbMargins.StyleController = this.layoutControl1;
            this.ddbMargins.TabIndex = 14;
            this.ddbMargins.Text = "Margins";
            // 
            // ddbCollate
            // 
            this.ddbCollate.Appearance.Options.UseTextOptions = true;
            this.ddbCollate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbCollate.Location = new System.Drawing.Point(2, 410);
            this.ddbCollate.Name = "ddbCollate";
            this.ddbCollate.Size = new System.Drawing.Size(260, 52);
            this.ddbCollate.StyleController = this.layoutControl1;
            this.ddbCollate.TabIndex = 16;
            this.ddbCollate.Text = "Collated";
            // 
            // backstageViewLabel2
            // 
            this.backstageViewLabel2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.backstageViewLabel2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.backstageViewLabel2.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.backstageViewLabel2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.backstageViewLabel2.LineVisible = true;
            this.backstageViewLabel2.Location = new System.Drawing.Point(2, 162);
            this.backstageViewLabel2.Name = "backstageViewLabel2";
            this.backstageViewLabel2.ShowLineShadow = false;
            this.backstageViewLabel2.Size = new System.Drawing.Size(260, 20);
            this.backstageViewLabel2.StyleController = this.layoutControl1;
            this.backstageViewLabel2.TabIndex = 12;
            this.backstageViewLabel2.Text = "Settings";
            // 
            // ddbPrinter
            // 
            this.ddbPrinter.Appearance.Options.UseTextOptions = true;
            this.ddbPrinter.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbPrinter.AutoWidthInLayoutControl = true;
            this.ddbPrinter.Location = new System.Drawing.Point(0, 104);
            this.ddbPrinter.Name = "ddbPrinter";
            this.ddbPrinter.Size = new System.Drawing.Size(264, 56);
            this.ddbPrinter.StyleController = this.layoutControl1;
            this.ddbPrinter.TabIndex = 11;
            this.ddbPrinter.Text = "Printer";
            // 
            // printerLabel
            // 
            this.printerLabel.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.printerLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.printerLabel.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.printerLabel.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.printerLabel.LineVisible = true;
            this.printerLabel.Location = new System.Drawing.Point(2, 82);
            this.printerLabel.Name = "printerLabel";
            this.printerLabel.ShowLineShadow = false;
            this.printerLabel.Size = new System.Drawing.Size(260, 20);
            this.printerLabel.StyleController = this.layoutControl1;
            this.printerLabel.TabIndex = 10;
            this.printerLabel.Text = "Printer";
            // 
            // printButton
            // 
            this.printButton.Image = ((System.Drawing.Image)(resources.GetObject("printButton.Image")));
            this.printButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.printButton.Location = new System.Drawing.Point(2, 2);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(80, 76);
            this.printButton.StyleController = this.layoutControl1;
            this.printButton.TabIndex = 5;
            this.printButton.Text = "Print";
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // copySpinEdit
            // 
            this.copySpinEdit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.copySpinEdit.Location = new System.Drawing.Point(143, 2);
            this.copySpinEdit.Name = "copySpinEdit";
            this.copySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.copySpinEdit.Properties.IsFloatValue = false;
            this.copySpinEdit.Properties.Mask.EditMask = "N00";
            this.copySpinEdit.Properties.MaxValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.copySpinEdit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.copySpinEdit.Size = new System.Drawing.Size(101, 20);
            this.copySpinEdit.StyleController = this.layoutControl1;
            this.copySpinEdit.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.lciCopiesSpinEdit,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(264, 464);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.printButton;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(84, 80);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(84, 80);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(84, 80);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciCopiesSpinEdit
            // 
            this.lciCopiesSpinEdit.Control = this.copySpinEdit;
            this.lciCopiesSpinEdit.CustomizationFormText = "Copies:";
            this.lciCopiesSpinEdit.Location = new System.Drawing.Point(84, 0);
            this.lciCopiesSpinEdit.MaxSize = new System.Drawing.Size(180, 24);
            this.lciCopiesSpinEdit.MinSize = new System.Drawing.Size(180, 24);
            this.lciCopiesSpinEdit.Name = "lciCopiesSpinEdit";
            this.lciCopiesSpinEdit.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 20, 2, 2);
            this.lciCopiesSpinEdit.Size = new System.Drawing.Size(180, 80);
            this.lciCopiesSpinEdit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCopiesSpinEdit.Text = "Copies:";
            this.lciCopiesSpinEdit.TextSize = new System.Drawing.Size(36, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.printerLabel;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(264, 24);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ddbPrinter;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 104);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 56);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(100, 56);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(264, 56);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.backstageViewLabel2;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 160);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(264, 24);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.ddbDuplex;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 184);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 56);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(100, 56);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(264, 56);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ddbOrientation;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 240);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 56);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(83, 56);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(264, 56);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.ddbPaperSize;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 296);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 56);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(79, 56);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(264, 56);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.ddbMargins;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 352);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 56);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(66, 56);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(264, 56);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.ddbCollate;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 408);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 56);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(68, 56);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(264, 56);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // recentPrintPreviewContainer
            // 
            this.recentPrintPreviewContainer.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.recentPrintPreviewContainer.Appearance.Options.UseBackColor = true;
            this.recentPrintPreviewContainer.Controls.Add(this.panelControl2);
            this.recentPrintPreviewContainer.Name = "recentPrintPreviewContainer";
            this.recentPrintPreviewContainer.Size = new System.Drawing.Size(500, 527);
            this.recentPrintPreviewContainer.TabIndex = 2;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.printControl2);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(500, 527);
            this.panelControl2.TabIndex = 0;
            // 
            // printControl2
            // 
            this.printControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printControl2.IsMetric = true;
            this.printControl2.Location = new System.Drawing.Point(2, 2);
            this.printControl2.Name = "printControl2";
            this.printControl2.Size = new System.Drawing.Size(496, 495);
            this.printControl2.TabIndex = 3;
            this.printControl2.SelectedPageChanged += new System.EventHandler(this.printControl2_SelectedPageChanged);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.zoomTextEdit);
            this.panelControl3.Controls.Add(this.panel2);
            this.panelControl3.Controls.Add(this.pageButtonEdit);
            this.panelControl3.Controls.Add(this.zoomTrackBarControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 497);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(496, 28);
            this.panelControl3.TabIndex = 2;
            // 
            // zoomTextEdit
            // 
            this.zoomTextEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.zoomTextEdit.EditValue = ((short)(100));
            this.zoomTextEdit.Location = new System.Drawing.Point(197, 2);
            this.zoomTextEdit.Name = "zoomTextEdit";
            this.zoomTextEdit.Properties.DisplayFormat.FormatString = "{0}%";
            this.zoomTextEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.zoomTextEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.zoomTextEdit.Properties.Mask.EditMask = "n0";
            this.zoomTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.zoomTextEdit.Size = new System.Drawing.Size(73, 20);
            this.zoomTextEdit.TabIndex = 6;
            this.zoomTextEdit.EditValueChanged += new System.EventHandler(this.zoomTextEdit_EditValueChanged);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(270, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 24);
            this.panel2.TabIndex = 5;
            // 
            // pageButtonEdit
            // 
            this.pageButtonEdit.Dock = System.Windows.Forms.DockStyle.Left;
            this.pageButtonEdit.EditValue = "1";
            this.pageButtonEdit.Location = new System.Drawing.Point(2, 2);
            this.pageButtonEdit.Name = "pageButtonEdit";
            this.pageButtonEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.pageButtonEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pageButtonEdit.Properties.AutoHeight = false;
            this.pageButtonEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pageButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Left, "", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right)});
            this.pageButtonEdit.Properties.DisplayFormat.FormatString = "Page {0} of 1";
            this.pageButtonEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pageButtonEdit.Size = new System.Drawing.Size(118, 24);
            this.pageButtonEdit.TabIndex = 4;
            this.pageButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.pageButtonEdit_ButtonClick);
            this.pageButtonEdit.EditValueChanged += new System.EventHandler(this.pageButtonEdit_EditValueChanged);
            this.pageButtonEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.pageButtonEdit_EditValueChanging);
            // 
            // zoomTrackBarControl1
            // 
            this.zoomTrackBarControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.zoomTrackBarControl1.EditValue = 40;
            this.zoomTrackBarControl1.Location = new System.Drawing.Point(280, 2);
            this.zoomTrackBarControl1.MenuManager = this.ribbonControl1;
            this.zoomTrackBarControl1.Name = "zoomTrackBarControl1";
            this.zoomTrackBarControl1.Properties.Maximum = 80;
            this.zoomTrackBarControl1.Properties.Middle = 40;
            this.zoomTrackBarControl1.Properties.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            this.zoomTrackBarControl1.Properties.SmallChange = 2;
            this.zoomTrackBarControl1.Size = new System.Drawing.Size(214, 24);
            this.zoomTrackBarControl1.TabIndex = 0;
            this.zoomTrackBarControl1.Value = 40;
            this.zoomTrackBarControl1.EditValueChanged += new System.EventHandler(this.zoomTrackBarControl1_EditValueChanged);
            // 
            // recentStackPanel8
            // 
            this.recentStackPanel8.Items.AddRange(new DevExpress.XtraBars.Ribbon.RecentItemBase[] {
            this.recentPrintPreview});
            this.recentStackPanel8.Name = "recentStackPanel8";
            this.recentStackPanel8.PanelPadding = new System.Windows.Forms.Padding(1);
            // 
            // recentPrintPreview
            // 
            this.recentPrintPreview.ControlContainer = this.recentPrintPreviewContainer;
            this.recentPrintPreview.FillSpace = true;
            this.recentPrintPreview.Name = "recentPrintPreview";
            this.recentPrintPreview.SuperTip = null;
            // 
            // recentStackPanel9
            // 
            this.recentStackPanel9.Caption = "Print";
            this.recentStackPanel9.Items.AddRange(new DevExpress.XtraBars.Ribbon.RecentItemBase[] {
            this.recentPrintOptions});
            this.recentStackPanel9.Name = "recentStackPanel9";
            // 
            // recentPrintOptions
            // 
            this.recentPrintOptions.ControlContainer = this.recentPrintOptionsContainer;
            this.recentPrintOptions.FillSpace = true;
            this.recentPrintOptions.Name = "recentPrintOptions";
            this.recentPrintOptions.SuperTip = null;
            // 
            // backstageViewClientControl10
            // 
            this.backstageViewClientControl10.Controls.Add(this.recentControlExport);
            this.backstageViewClientControl10.Location = new System.Drawing.Point(133, 71);
            this.backstageViewClientControl10.Name = "backstageViewClientControl10";
            this.backstageViewClientControl10.Size = new System.Drawing.Size(833, 539);
            this.backstageViewClientControl10.TabIndex = 8;
            // 
            // recentControlExport
            // 
            this.recentControlExport.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.recentControlExport.ContentPanelMinWidth = 100;
            this.recentControlExport.DefaultContentPanel = this.recentStackPanel14;
            this.recentControlExport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recentControlExport.Location = new System.Drawing.Point(0, 0);
            this.recentControlExport.MainPanel = this.recentStackPanel10;
            this.recentControlExport.MainPanelMinWidth = 100;
            this.recentControlExport.MinimumSize = new System.Drawing.Size(400, 100);
            this.recentControlExport.Name = "recentControlExport";
            this.recentControlExport.SelectedTab = this.recentTabItem4;
            this.recentControlExport.ShowTitle = false;
            this.recentControlExport.Size = new System.Drawing.Size(833, 539);
            this.recentControlExport.TabIndex = 0;
            this.recentControlExport.Text = "recentItemControl1";
            this.recentControlExport.Title = "Title";
            this.recentControlExport.ItemClick += new DevExpress.XtraBars.Ribbon.RecentItemEventHandler(this.recentControlExport_ItemClick);
            // 
            // recentStackPanel14
            // 
            this.recentStackPanel14.Name = "recentStackPanel14";
            // 
            // recentStackPanel10
            // 
            this.recentStackPanel10.Caption = "Export";
            this.recentStackPanel10.CaptionToContentIndent = 46;
            this.recentStackPanel10.Items.AddRange(new DevExpress.XtraBars.Ribbon.RecentItemBase[] {
            this.recentTabItem4});
            this.recentStackPanel10.Name = "recentStackPanel10";
            // 
            // recentTabItem4
            // 
            this.recentTabItem4.Caption = "Export To";
            this.recentTabItem4.Name = "recentTabItem4";
            this.recentTabItem4.SuperTip = null;
            this.recentTabItem4.TabPanel = this.recentStackPanel11;
            // 
            // recentStackPanel11
            // 
            this.recentStackPanel11.Caption = "Export To";
            this.recentStackPanel11.CaptionToContentIndent = 46;
            this.recentStackPanel11.Items.AddRange(new DevExpress.XtraBars.Ribbon.RecentItemBase[] {
            this.recentControlRecentItem1,
            this.recentControlRecentItem2,
            this.recentControlRecentItem4,
            this.recentControlRecentItem5,
            this.recentControlRecentItem6,
            this.recentControlRecentItem7,
            this.recentControlRecentItem8,
            this.recentControlRecentItem9});
            this.recentStackPanel11.Name = "recentStackPanel11";
            // 
            // recentControlRecentItem1
            // 
            this.recentControlRecentItem1.Caption = "PDF  File";
            this.recentControlRecentItem1.Description = "Adobe Portable Document Format";
            this.recentControlRecentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem1.Glyph")));
            this.recentControlRecentItem1.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem1.Name = "recentControlRecentItem1";
            this.recentControlRecentItem1.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem1.SuperTip = null;
            // 
            // recentControlRecentItem2
            // 
            this.recentControlRecentItem2.Caption = "HTML File";
            this.recentControlRecentItem2.Description = "Web Page";
            this.recentControlRecentItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem2.Glyph")));
            this.recentControlRecentItem2.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem2.Name = "recentControlRecentItem2";
            this.recentControlRecentItem2.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem2.SuperTip = null;
            // 
            // recentControlRecentItem4
            // 
            this.recentControlRecentItem4.Caption = "MHT File";
            this.recentControlRecentItem4.Description = "Single File Web Page";
            this.recentControlRecentItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem4.Glyph")));
            this.recentControlRecentItem4.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem4.Name = "recentControlRecentItem4";
            this.recentControlRecentItem4.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem4.SuperTip = null;
            // 
            // recentControlRecentItem5
            // 
            this.recentControlRecentItem5.Caption = "RTF File";
            this.recentControlRecentItem5.Description = "Rich Text Format";
            this.recentControlRecentItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem5.Glyph")));
            this.recentControlRecentItem5.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem5.Name = "recentControlRecentItem5";
            this.recentControlRecentItem5.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem5.SuperTip = null;
            // 
            // recentControlRecentItem6
            // 
            this.recentControlRecentItem6.Caption = "XLS File";
            this.recentControlRecentItem6.Description = "Microsoft Excel 2000-2003 Workbook";
            this.recentControlRecentItem6.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem6.Glyph")));
            this.recentControlRecentItem6.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem6.Name = "recentControlRecentItem6";
            this.recentControlRecentItem6.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem6.SuperTip = null;
            // 
            // recentControlRecentItem7
            // 
            this.recentControlRecentItem7.Caption = "XLSX File";
            this.recentControlRecentItem7.Description = "Microsoft Excel 2007-2016 Workbook";
            this.recentControlRecentItem7.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem7.Glyph")));
            this.recentControlRecentItem7.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem7.Name = "recentControlRecentItem7";
            this.recentControlRecentItem7.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem7.SuperTip = null;
            // 
            // recentControlRecentItem8
            // 
            this.recentControlRecentItem8.Caption = "CSV File";
            this.recentControlRecentItem8.Description = "Comma-Separated Values Text";
            this.recentControlRecentItem8.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem8.Glyph")));
            this.recentControlRecentItem8.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem8.Name = "recentControlRecentItem8";
            this.recentControlRecentItem8.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem8.SuperTip = null;
            // 
            // recentControlRecentItem9
            // 
            this.recentControlRecentItem9.Caption = "Text File";
            this.recentControlRecentItem9.Description = "Plain Text";
            this.recentControlRecentItem9.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem9.Glyph")));
            this.recentControlRecentItem9.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem9.Name = "recentControlRecentItem9";
            this.recentControlRecentItem9.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem9.SuperTip = null;
            // 
            // backstageViewClientControl2
            // 
            this.backstageViewClientControl2.Controls.Add(this.recentItemControl1);
            this.backstageViewClientControl2.Location = new System.Drawing.Point(133, 71);
            this.backstageViewClientControl2.Name = "backstageViewClientControl2";
            this.backstageViewClientControl2.Size = new System.Drawing.Size(833, 539);
            this.backstageViewClientControl2.TabIndex = 10;
            // 
            // recentItemControl1
            // 
            this.recentItemControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.recentItemControl1.ContentPanelMinWidth = 350;
            this.recentItemControl1.DefaultContentPanel = this.recentStackPanel12;
            this.recentItemControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recentItemControl1.Location = new System.Drawing.Point(0, 0);
            this.recentItemControl1.MainPanel = this.recentStackPanel13;
            this.recentItemControl1.MainPanelMinWidth = 530;
            this.recentItemControl1.Name = "recentItemControl1";
            this.recentItemControl1.SelectedTab = null;
            this.recentItemControl1.ShowTitle = false;
            this.recentItemControl1.Size = new System.Drawing.Size(833, 539);
            this.recentItemControl1.SplitterPosition = 530;
            this.recentItemControl1.TabIndex = 0;
            this.recentItemControl1.Text = "recentItemControl1";
            this.recentItemControl1.Title = "Title";
            // 
            // recentStackPanel12
            // 
            this.recentStackPanel12.Caption = "About";
            this.recentStackPanel12.CaptionToContentIndent = 46;
            this.recentStackPanel12.Items.AddRange(new DevExpress.XtraBars.Ribbon.RecentItemBase[] {
            this.recentLabelItem1,
            this.recentHyperlinkItem1,
            this.recentLabelItem2});
            this.recentStackPanel12.Name = "recentStackPanel12";
            // 
            // recentLabelItem1
            // 
            this.recentLabelItem1.Caption = null;
            this.recentLabelItem1.Glyph = global::DevExpress.XtraBars.Demos.RibbonSimplePad.Properties.Resources.XtraBars;
            this.recentLabelItem1.Name = "recentLabelItem1";
            this.recentLabelItem1.SuperTip = null;
            // 
            // recentHyperlinkItem1
            // 
            this.recentHyperlinkItem1.AllowSelect = DevExpress.Utils.DefaultBoolean.False;
            this.recentHyperlinkItem1.Caption = "www.devexpress.com";
            this.recentHyperlinkItem1.LinkColor = System.Drawing.Color.Empty;
            this.recentHyperlinkItem1.Name = "recentHyperlinkItem1";
            this.recentHyperlinkItem1.SuperTip = null;
            this.recentHyperlinkItem1.VisitedColor = System.Drawing.Color.Empty;
            // 
            // recentLabelItem2
            // 
            this.recentLabelItem2.AllowSelect = DevExpress.Utils.DefaultBoolean.False;
            this.recentLabelItem2.Caption = "Copyright (c) 2000-2013 DevExpress inc. ALL RIGHTS RESERVED.";
            this.recentLabelItem2.Name = "recentLabelItem2";
            this.recentLabelItem2.SuperTip = null;
            // 
            // recentStackPanel13
            // 
            this.recentStackPanel13.Caption = "Support";
            this.recentStackPanel13.CaptionToContentIndent = 46;
            this.recentStackPanel13.Items.AddRange(new DevExpress.XtraBars.Ribbon.RecentItemBase[] {
            this.recentPinItem2,
            this.recentPinItem3,
            this.recentPinItem4});
            this.recentStackPanel13.Name = "recentStackPanel13";
            // 
            // recentPinItem2
            // 
            this.recentPinItem2.Caption = "DevExpress Online Help";
            this.recentPinItem2.Description = "Get help using DevExpress components";
            this.recentPinItem2.Glyph = global::DevExpress.XtraBars.Demos.RibbonSimplePad.Properties.Resources.Online_Help;
            this.recentPinItem2.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentPinItem2.Name = "recentPinItem2";
            this.recentPinItem2.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentPinItem2.SuperTip = null;
            // 
            // recentPinItem3
            // 
            this.recentPinItem3.Caption = "Getting Started";
            this.recentPinItem3.Description = "See what\'s new and find resources to help you learn the basics quickly.";
            this.recentPinItem3.Glyph = global::DevExpress.XtraBars.Demos.RibbonSimplePad.Properties.Resources.Code_Central;
            this.recentPinItem3.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentPinItem3.Name = "recentPinItem3";
            this.recentPinItem3.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentPinItem3.SuperTip = null;
            // 
            // recentPinItem4
            // 
            this.recentPinItem4.Caption = "Contact Us";
            this.recentPinItem4.Description = "Let us know if you need help or how we can make our components better";
            this.recentPinItem4.Glyph = global::DevExpress.XtraBars.Demos.RibbonSimplePad.Properties.Resources.Contact_Us;
            this.recentPinItem4.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentPinItem4.Name = "recentPinItem4";
            this.recentPinItem4.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentPinItem4.SuperTip = null;
            // 
            // backstageViewTabItem3
            // 
            this.backstageViewTabItem3.Caption = "Open";
            this.backstageViewTabItem3.ContentControl = this.backstageViewClientControl9;
            this.backstageViewTabItem3.Name = "backstageViewTabItem3";
            this.backstageViewTabItem3.Selected = true;
            // 
            // bvItemSave
            // 
            this.bvItemSave.Caption = "Save";
            this.bvItemSave.Name = "bvItemSave";
            this.bvItemSave.ItemClick += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.bvItemSave_ItemClick);
            // 
            // backstageViewTabItem1
            // 
            this.backstageViewTabItem1.Caption = "Save As";
            this.backstageViewTabItem1.ContentControl = this.backstageViewClientControl8;
            this.backstageViewTabItem1.Name = "backstageViewTabItem1";
            this.backstageViewTabItem1.Selected = false;
            // 
            // backstageViewTabItem5
            // 
            this.backstageViewTabItem5.Caption = "Print";
            this.backstageViewTabItem5.ContentControl = this.backstageViewClientControl11;
            this.backstageViewTabItem5.Name = "backstageViewTabItem5";
            this.backstageViewTabItem5.Selected = false;
            // 
            // backstageViewTabItem4
            // 
            this.backstageViewTabItem4.Caption = "Export";
            this.backstageViewTabItem4.ContentControl = this.backstageViewClientControl10;
            this.backstageViewTabItem4.Name = "backstageViewTabItem4";
            this.backstageViewTabItem4.Selected = false;
            // 
            // backstageViewTabItem6
            // 
            this.backstageViewTabItem6.Caption = "Help";
            this.backstageViewTabItem6.ContentControl = this.backstageViewClientControl2;
            this.backstageViewTabItem6.Name = "backstageViewTabItem6";
            this.backstageViewTabItem6.Selected = false;
            // 
            // bvItemClose
            // 
            this.bvItemClose.Caption = "Close";
            this.bvItemClose.Name = "bvItemClose";
            this.bvItemClose.ItemClick += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.bvItemClose_ItemClick);
            // 
            // bvItemExit
            // 
            this.bvItemExit.Caption = "Exit";
            this.bvItemExit.Name = "bvItemExit";
            this.bvItemExit.ItemClick += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.bvItemExit_ItemClick);
            // 
            // printControl1
            // 
            this.printControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printControl1.IsMetric = true;
            this.printControl1.Location = new System.Drawing.Point(0, 0);
            this.printControl1.Name = "printControl1";
            this.printControl1.Size = new System.Drawing.Size(639, 539);
            this.printControl1.TabIndex = 3;
            // 
            // recentControlRecentItem10
            // 
            this.recentControlRecentItem10.Caption = "Image File";
            this.recentControlRecentItem10.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.recentControlRecentItem10.Glyph = ((System.Drawing.Image)(resources.GetObject("recentControlRecentItem10.Glyph")));
            this.recentControlRecentItem10.GlyphAlignment = DevExpress.XtraBars.Ribbon.RecentPinItemGlyphAlignment.Center;
            this.recentControlRecentItem10.Name = "recentControlRecentItem10";
            this.recentControlRecentItem10.PinButtonVisibility = DevExpress.XtraBars.Ribbon.RecentPinButtonVisibility.Never;
            this.recentControlRecentItem10.SuperTip = null;
            // 
            // recentControlButtonItem1
            // 
            this.recentControlButtonItem1.AutoSize = false;
            this.recentControlButtonItem1.Caption = "Save As";
            this.recentControlButtonItem1.Name = "recentControlButtonItem1";
            this.recentControlButtonItem1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.recentControlButtonItem1.Size = new System.Drawing.Size(89, 92);
            this.recentControlButtonItem1.SuperTip = null;
            // 
            // backstageViewClientControl7
            // 
            this.backstageViewClientControl7.Location = new System.Drawing.Point(0, 0);
            this.backstageViewClientControl7.Name = "backstageViewClientControl7";
            this.backstageViewClientControl7.Size = new System.Drawing.Size(150, 150);
            this.backstageViewClientControl7.TabIndex = 6;
            // 
            // backstageViewClientControl1
            // 
            this.backstageViewClientControl1.Location = new System.Drawing.Point(0, 0);
            this.backstageViewClientControl1.Name = "backstageViewClientControl1";
            this.backstageViewClientControl1.Size = new System.Drawing.Size(150, 150);
            this.backstageViewClientControl1.TabIndex = 0;
            // 
            // backstageViewClientControl3
            // 
            this.backstageViewClientControl3.Location = new System.Drawing.Point(0, 0);
            this.backstageViewClientControl3.Name = "backstageViewClientControl3";
            this.backstageViewClientControl3.Size = new System.Drawing.Size(150, 150);
            this.backstageViewClientControl3.TabIndex = 2;
            // 
            // taskbarAssistant1
            // 
            this.taskbarAssistant1.ParentControl = this;
            this.taskbarAssistant1.ThumbnailButtons.Add(this.thumbButtonNewDoc);
            this.taskbarAssistant1.ThumbnailButtons.Add(this.thumbButtonPrev);
            this.taskbarAssistant1.ThumbnailButtons.Add(this.thumbButtonNext);
            this.taskbarAssistant1.ThumbnailButtons.Add(this.thumbButtonExit);
            // 
            // thumbButtonNewDoc
            // 
            this.thumbButtonNewDoc.Image = ((System.Drawing.Bitmap)(resources.GetObject("thumbButtonNewDoc.Image")));
            this.thumbButtonNewDoc.Tooltip = "Create New Document";
            this.thumbButtonNewDoc.Click += new System.EventHandler<DevExpress.Utils.Taskbar.ThumbButtonClickEventArgs>(this.OnNewDocThumbButtonClick);
            // 
            // thumbButtonPrev
            // 
            this.thumbButtonPrev.Image = ((System.Drawing.Bitmap)(resources.GetObject("thumbButtonPrev.Image")));
            this.thumbButtonPrev.Tooltip = "Previous Document";
            this.thumbButtonPrev.Click += new System.EventHandler<DevExpress.Utils.Taskbar.ThumbButtonClickEventArgs>(this.OnPrevThumbButtonClick);
            // 
            // thumbButtonNext
            // 
            this.thumbButtonNext.Image = ((System.Drawing.Bitmap)(resources.GetObject("thumbButtonNext.Image")));
            this.thumbButtonNext.Tooltip = "Next Document";
            this.thumbButtonNext.Click += new System.EventHandler<DevExpress.Utils.Taskbar.ThumbButtonClickEventArgs>(this.OnNextDocThumbButtonClick);
            // 
            // thumbButtonExit
            // 
            this.thumbButtonExit.Image = ((System.Drawing.Bitmap)(resources.GetObject("thumbButtonExit.Image")));
            this.thumbButtonExit.Tooltip = "Exit";
            this.thumbButtonExit.Click += new System.EventHandler<DevExpress.Utils.Taskbar.ThumbButtonClickEventArgs>(this.OnExitThumbButtonClick);
            // 
            // backstageViewTabItem2
            // 
            this.backstageViewTabItem2.Caption = "Open";
            this.backstageViewTabItem2.ContentControl = this.backstageViewClientControl8;
            this.backstageViewTabItem2.Name = "backstageViewTabItem2";
            this.backstageViewTabItem2.Selected = false;
            // 
            // bvTabPrint
            // 
            this.bvTabPrint.Caption = "Print";
            this.bvTabPrint.ContentControl = null;
            this.bvTabPrint.Name = "bvTabPrint";
            this.bvTabPrint.Selected = false;
            this.bvTabPrint.SelectedChanged += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.bvTabPrint_SelectedChanged);
            // 
            // backstageViewClientControl4
            // 
            this.backstageViewClientControl4.Controls.Add(this.printControl1);
            this.backstageViewClientControl4.Location = new System.Drawing.Point(133, 71);
            this.backstageViewClientControl4.Name = "backstageViewClientControl4";
            this.backstageViewClientControl4.Size = new System.Drawing.Size(639, 539);
            this.backstageViewClientControl4.TabIndex = 3;
            // 
            // frmMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1057, 741);
            this.Controls.Add(this.backstageViewControl1);
            this.Controls.Add(this.pccBottom);
            this.Controls.Add(this.pccAppMenu);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar1;
            this.Text = "SimplePad (C# Demo)";
            this.Activated += new System.EventHandler(this.frmMain_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MdiChildActivate += new System.EventHandler(this.frmMain_MdiChildActivate);
            ((System.ComponentModel.ISupportInitialize)(this.gddFont)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmAppMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccBottom)).EndInit();
            this.pccBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccAppMenu)).EndInit();
            this.pccAppMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcAppMenuFileLabels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gddFontColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riicStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backstageViewControl1)).EndInit();
            this.backstageViewControl1.ResumeLayout(false);
            this.backstageViewClientControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recentOpen)).EndInit();
            this.backstageViewClientControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recentSaveAs)).EndInit();
            this.backstageViewClientControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recentControlPrint)).EndInit();
            this.recentControlPrint.ResumeLayout(false);
            this.recentPrintOptionsContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.copySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCopiesSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.recentPrintPreviewContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).EndInit();
            this.backstageViewClientControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recentControlExport)).EndInit();
            this.backstageViewClientControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recentItemControl1)).EndInit();
            this.backstageViewClientControl4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.BarButtonItem iClose;
        private DevExpress.XtraBars.BarButtonItem iSave;
        private DevExpress.XtraBars.BarButtonItem iOpen;
        private DevExpress.XtraBars.BarButtonItem iSaveAs;
        private DevExpress.XtraBars.BarButtonItem idNew;
        private DevExpress.XtraBars.BarButtonItem iExit;
        private DevExpress.XtraBars.BarButtonItem iPrint;
        private DevExpress.XtraBars.BarButtonItem iClear;
        private DevExpress.XtraBars.BarButtonItem iPaste;
        private DevExpress.XtraBars.BarButtonItem iFind;
        private DevExpress.XtraBars.BarButtonItem iCut;
        private DevExpress.XtraBars.BarButtonItem iCopy;
        private DevExpress.XtraBars.BarButtonItem iUndo;
        private DevExpress.XtraBars.BarButtonItem iReplace;
        private DevExpress.XtraBars.BarButtonItem iSelectAll;
        private DevExpress.XtraBars.BarButtonItem iBold;
        private DevExpress.XtraBars.BarButtonItem iAlignRight;
        private DevExpress.XtraBars.BarButtonItem iCenter;
        private DevExpress.XtraBars.BarButtonItem iUnderline;
        private DevExpress.XtraBars.BarButtonItem iAlignLeft;
        private DevExpress.XtraBars.BarButtonItem iItalic;
        private DevExpress.XtraBars.BarButtonItem iFont;
        private DevExpress.XtraBars.BarButtonItem iBullets;
        private DevExpress.XtraBars.BarButtonItem iProtected;
        private DevExpress.XtraBars.BarButtonItem iFontColor;
        private DevExpress.XtraBars.BarButtonItem iWeb;
        private DevExpress.XtraBars.BarButtonItem siPosition;
        private DevExpress.XtraBars.BarButtonItem siModified;
        private DevExpress.XtraBars.BarStaticItem siDocName;
        private DevExpress.XtraBars.PopupControlContainer popupControlContainer1;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarButtonGroup bgAlign;
        private DevExpress.XtraBars.BarButtonGroup bgFontStyle;
        private DevExpress.XtraBars.BarButtonGroup bgFont;
        private DevExpress.XtraBars.BarButtonGroup bgBullets;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarSubItem sbiSave;
        private DevExpress.XtraBars.BarSubItem sbiPaste;
        private DevExpress.XtraBars.BarSubItem sbiFind;
        private DevExpress.XtraBars.BarButtonItem iPasteSpecial;
        private DevExpress.XtraBars.BarButtonItem iNew;
        private DevExpress.XtraBars.BarLargeButtonItem iLargeUndo;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem iTemplate;
        private DevExpress.XtraBars.PopupMenu pmNew;
        private DevExpress.XtraBars.PopupMenu pmMain;
        private DevExpress.XtraBars.BarButtonItem iPaintStyle;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiSkins;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu pmAppMain;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown gddFont;
        private DevExpress.XtraBars.BarEditItem beiFontSize;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgFont;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiFont;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown gddFontColor;
        private DevExpress.XtraBars.BarButtonItem bbiFontColorPopup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgFontColor;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiFontColor;
        private DevExpress.XtraBars.BarButtonItem iAbout;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.Ribbon.RibbonPageCategory ribbonPageCategory1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup12;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup13;
        private Office2007PopupControlContainer pccAppMenu;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pcAppMenuFileLabels;
        private DevExpress.Utils.ImageCollection imageCollection3;
        private BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private BarEditItem biStyle;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox riicStyle;
        private PopupControlContainer pccBottom;
        private DevExpress.XtraEditors.SimpleButton sbExit;
        private DevExpress.XtraBars.Ribbon.RibbonMiniToolbar selectionMiniToolbar;
        private BarButtonGroup editButtonGroup;
        private DevExpress.XtraBars.Ribbon.BackstageViewControl backstageViewControl1;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl1;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl3;
        private DevExpress.XtraBars.Ribbon.BackstageViewButtonItem bvItemSave;
        private DevExpress.XtraBars.Ribbon.BackstageViewButtonItem bvItemClose;
        private DevExpress.XtraBars.Ribbon.BackstageViewButtonItem bvItemExit;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl7;
        private BarEditItem beScheme;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.Utils.Taskbar.TaskbarAssistant taskbarAssistant1;
        private DevExpress.Utils.Taskbar.ThumbnailButton thumbButtonNewDoc;
        private DevExpress.Utils.Taskbar.ThumbnailButton thumbButtonNext;
        private DevExpress.Utils.Taskbar.ThumbnailButton thumbButtonExit;
        private DevExpress.Utils.Taskbar.ThumbnailButton thumbButtonPrev;
        private BarToggleSwitchItem barToggleSwitchItem1;
        private XtraEditors.Repository.RepositoryItemTrackBar repositoryItemTrackBar1;
        private BarButtonItem bbColorMix;
        private System.ComponentModel.IContainer components;
        private Ribbon.BackstageViewClientControl backstageViewClientControl8;
        private Ribbon.BackstageViewTabItem backstageViewTabItem1;
        private Ribbon.BackstageViewTabItem backstageViewTabItem2;
        private Ribbon.BackstageViewClientControl backstageViewClientControl9;
        private Ribbon.BackstageViewTabItem backstageViewTabItem3;
        private Ribbon.BackstageViewClientControl backstageViewClientControl10;
        private Ribbon.BackstageViewTabItem backstageViewTabItem4;
        private Ribbon.RecentPinItem recentControlRecentItem1;
        private Ribbon.RecentPinItem recentControlRecentItem2;
        private Ribbon.RecentPinItem recentControlRecentItem4;
        private Ribbon.RecentPinItem recentControlRecentItem5;
        private Ribbon.RecentPinItem recentControlRecentItem6;
        private Ribbon.RecentPinItem recentControlRecentItem7;
        private Ribbon.RecentPinItem recentControlRecentItem8;
        private Ribbon.RecentPinItem recentControlRecentItem9;
        private Ribbon.RecentPinItem recentControlRecentItem10;
        private Ribbon.RecentButtonItem recentControlButtonItem1;
        private Ribbon.BackstageViewClientControl backstageViewClientControl11;
        private Ribbon.RecentItemControl recentControlPrint;
        private Ribbon.RecentControlItemControlContainer recentPrintOptionsContainer;
        private Ribbon.RecentControlItemControlContainer recentPrintPreviewContainer;
        private Ribbon.RecentControlContainerItem recentPrintOptions;
        private Ribbon.RecentControlContainerItem recentPrintPreview;
        private Ribbon.BackstageViewTabItem backstageViewTabItem5;
        private Ribbon.BackstageViewTabItem bvTabPrint;
        //private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private Ribbon.BackstageViewClientControl backstageViewClientControl4;
        private XtraEditors.PanelControl panelControl2;
        private XtraEditors.PanelControl panelControl3;
        private XtraPrinting.Control.PrintControl printControl1;
        private XtraEditors.ZoomTrackBarControl zoomTrackBarControl1;
        private XtraEditors.ButtonEdit pageButtonEdit;
        private System.Windows.Forms.Panel panel2;
        private XtraLayout.LayoutControl layoutControl1;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private XtraEditors.SimpleButton printButton;
        private XtraLayout.LayoutControlItem layoutControlItem1;
        private XtraEditors.SpinEdit copySpinEdit;
        private XtraLayout.LayoutControlItem lciCopiesSpinEdit;
        private BackstageViewLabel printerLabel;
        private XtraLayout.LayoutControlItem layoutControlItem3;
        private XtraEditors.TextEdit zoomTextEdit;
        private XtraEditors.DropDownButton ddbPrinter;
        private XtraLayout.LayoutControlItem layoutControlItem2;
        private Ribbon.RecentItemControl recentOpen;
        private Ribbon.RecentTabItem recentTabItem1;
        private Ribbon.RecentTabItem recentTabItem2;
        private Ribbon.RecentItemControl recentSaveAs;
        private Ribbon.RecentTabItem recentTabItem3;
        private Ribbon.RecentItemControl recentControlExport;
        private Ribbon.RecentTabItem recentTabItem4;
        private Ribbon.BackstageViewClientControl backstageViewClientControl2;
        private Ribbon.RecentItemControl recentItemControl1;
        private Ribbon.RecentPinItem recentPinItem2;
        private Ribbon.RecentPinItem recentPinItem3;
        private Ribbon.BackstageViewTabItem backstageViewTabItem6;
        private Ribbon.RecentPinItem recentPinItem4;
        private Ribbon.RecentLabelItem recentLabelItem1;
        private Ribbon.RecentHyperlinkItem recentHyperlinkItem1;
        private Ribbon.RecentLabelItem recentLabelItem2;
        private XtraEditors.DropDownButton ddbDuplex;
        private XtraEditors.DropDownButton ddbOrientation;
        private XtraEditors.DropDownButton ddbPaperSize;
        private XtraEditors.DropDownButton ddbMargins;
        private XtraEditors.DropDownButton ddbCollate;
        private BackstageViewLabel backstageViewLabel2;
        private XtraLayout.LayoutControlItem layoutControlItem4;
        private XtraLayout.LayoutControlItem layoutControlItem5;
        private XtraLayout.LayoutControlItem layoutControlItem6;
        private XtraLayout.LayoutControlItem layoutControlItem7;
        private XtraLayout.LayoutControlItem layoutControlItem8;
        private XtraLayout.LayoutControlItem layoutControlItem9;
        private XtraPrinting.Control.PrintControl printControl2;
        private RecentStackPanel recentStackPanel1;
        private RecentStackPanel recentStackPanel2;
        private RecentStackPanel recentStackPanel3;
        private RecentStackPanel recentStackPanel4;
        private RecentStackPanel recentStackPanel5;
        private RecentStackPanel recentStackPanel6;
        private RecentStackPanel recentStackPanel7;
        private RecentStackPanel recentStackPanel8;
        private RecentStackPanel recentStackPanel9;
        private RecentStackPanel recentStackPanel10;
        private RecentStackPanel recentStackPanel11;
        private RecentStackPanel recentStackPanel12;
        private RecentStackPanel recentStackPanel13;
        private RecentStackPanel recentStackPanel14;
    }
}
