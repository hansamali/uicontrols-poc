﻿namespace DevExpress.XtraBars.Demos.TileControlDemo {
    partial class UserControl1 {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement28 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement29 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement30 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement31 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement32 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement33 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement34 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement35 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement36 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement37 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement38 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement39 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement40 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement41 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement42 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement43 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement44 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement45 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement46 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement47 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame5 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement48 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame6 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement49 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement50 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement51 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame7 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement52 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame8 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement53 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement54 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itemDesktop = new DevExpress.XtraEditors.TileItem();
            this.itemPeople = new DevExpress.XtraEditors.TileItem();
            this.itemMail = new DevExpress.XtraEditors.TileItem();
            this.itemGames = new DevExpress.XtraEditors.TileItem();
            this.itemCamera = new DevExpress.XtraEditors.TileItem();
            this.itemVideo = new DevExpress.XtraEditors.TileItem();
            this.itemMusic = new DevExpress.XtraEditors.TileItem();
            this.itemSkyDrive = new DevExpress.XtraEditors.TileItem();
            this.itemStore = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itemCalendar = new DevExpress.XtraEditors.TileItem();
            this.itemMaps = new DevExpress.XtraEditors.TileItem();
            this.itemReadingList = new DevExpress.XtraEditors.TileItem();
            this.itemSports = new DevExpress.XtraEditors.TileItem();
            this.itemNews = new DevExpress.XtraEditors.TileItem();
            this.itemInternetExplorer = new DevExpress.XtraEditors.TileItem();
            this.itemPhotos = new DevExpress.XtraEditors.TileItem();
            this.itemFinances = new DevExpress.XtraEditors.TileItem();
            this.itemWeather = new DevExpress.XtraEditors.TileItem();
            this.itemFoodsDrink = new DevExpress.XtraEditors.TileItem();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.itemTravel = new DevExpress.XtraEditors.TileItem();
            this.itemHealthFitness = new DevExpress.XtraEditors.TileItem();
            this.itemHelpTips = new DevExpress.XtraEditors.TileItem();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Transparent;
            this.tileControl1.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileControl1.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileControl1.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileControl1.AppearanceText.Font = new System.Drawing.Font("Segoe UI Light", 35F);
            this.tileControl1.AppearanceText.ForeColor = System.Drawing.Color.White;
            this.tileControl1.AppearanceText.Options.UseFont = true;
            this.tileControl1.AppearanceText.Options.UseForeColor = true;
            this.tileControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.DragSize = new System.Drawing.Size(0, 0);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.ItemBorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never;
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 60;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.RowCount = 4;
            this.tileControl1.ShowText = true;
            this.tileControl1.Size = new System.Drawing.Size(1140, 801);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "DevExpress TileControl Demo (Press Esc to Exit)";
            this.tileControl1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnTileControlKeyUp);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itemDesktop);
            this.tileGroup2.Items.Add(this.itemPeople);
            this.tileGroup2.Items.Add(this.itemMail);
            this.tileGroup2.Items.Add(this.itemGames);
            this.tileGroup2.Items.Add(this.itemCamera);
            this.tileGroup2.Items.Add(this.itemVideo);
            this.tileGroup2.Items.Add(this.itemMusic);
            this.tileGroup2.Items.Add(this.itemSkyDrive);
            this.tileGroup2.Items.Add(this.itemStore);
            this.tileGroup2.Name = "tileGroup2";
            this.tileGroup2.Text = null;
            // 
            // itemDesktop
            // 
            tileItemElement28.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement28.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement28.Text = "Desktop";
            tileItemElement28.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement28.TextLocation = new System.Drawing.Point(12, -8);
            this.itemDesktop.Elements.Add(tileItemElement28);
            this.itemDesktop.Id = 56;
            this.itemDesktop.ItemSize = DevExpress.XtraEditors.TileItemSize.Large;
            this.itemDesktop.Name = "itemDesktop";
            this.itemDesktop.Padding = new System.Windows.Forms.Padding(0);
            this.itemDesktop.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itemDesktop_ItemClick);
            // 
            // itemPeople
            // 
            this.itemPeople.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(81)))), ((int)(((byte)(43)))));
            this.itemPeople.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement29.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.People;
            tileItemElement29.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement29.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement29.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement29.Text = "People";
            tileItemElement29.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemPeople.Elements.Add(tileItemElement29);
            this.itemPeople.Id = 10;
            this.itemPeople.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemPeople.Name = "itemPeople";
            // 
            // itemMail
            // 
            this.itemMail.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(134)))), ((int)(((byte)(153)))));
            this.itemMail.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement30.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Mail;
            tileItemElement30.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement30.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement30.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement30.Text = "Mail";
            tileItemElement30.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemMail.Elements.Add(tileItemElement30);
            this.itemMail.Id = 16;
            this.itemMail.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemMail.Name = "itemMail";
            // 
            // itemGames
            // 
            this.itemGames.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(0)))));
            this.itemGames.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement31.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Games;
            tileItemElement31.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement31.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement31.ImageSize = new System.Drawing.Size(56, 56);
            tileItemElement31.Text = "";
            tileItemElement31.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemGames.Elements.Add(tileItemElement31);
            this.itemGames.Id = 19;
            this.itemGames.ItemSize = DevExpress.XtraEditors.TileItemSize.Small;
            this.itemGames.Name = "itemGames";
            // 
            // itemCamera
            // 
            this.itemCamera.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(0)))), ((int)(((byte)(153)))));
            this.itemCamera.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement32.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Camera;
            tileItemElement32.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement32.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement32.ImageSize = new System.Drawing.Size(56, 56);
            tileItemElement32.Text = "";
            this.itemCamera.Elements.Add(tileItemElement32);
            this.itemCamera.Id = 48;
            this.itemCamera.ItemSize = DevExpress.XtraEditors.TileItemSize.Small;
            this.itemCamera.Name = "itemCamera";
            // 
            // itemVideo
            // 
            this.itemVideo.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(29)))), ((int)(((byte)(71)))));
            this.itemVideo.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement33.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Video;
            tileItemElement33.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement33.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement33.ImageSize = new System.Drawing.Size(56, 56);
            tileItemElement33.Text = "";
            this.itemVideo.Elements.Add(tileItemElement33);
            this.itemVideo.Id = 7;
            this.itemVideo.ItemSize = DevExpress.XtraEditors.TileItemSize.Small;
            this.itemVideo.Name = "itemVideo";
            // 
            // itemMusic
            // 
            this.itemMusic.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(80)))), ((int)(((byte)(43)))));
            this.itemMusic.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement34.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Music;
            tileItemElement34.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement34.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement34.ImageSize = new System.Drawing.Size(56, 56);
            tileItemElement34.Text = "";
            this.itemMusic.Elements.Add(tileItemElement34);
            this.itemMusic.Id = 17;
            this.itemMusic.ItemSize = DevExpress.XtraEditors.TileItemSize.Small;
            this.itemMusic.Name = "itemMusic";
            // 
            // itemSkyDrive
            // 
            this.itemSkyDrive.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(85)))), ((int)(((byte)(189)))));
            this.itemSkyDrive.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement35.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.SkyDrive;
            tileItemElement35.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement35.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement35.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement35.Text = "SkyDrive";
            tileItemElement35.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemSkyDrive.Elements.Add(tileItemElement35);
            this.itemSkyDrive.Id = 45;
            this.itemSkyDrive.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.itemSkyDrive.Name = "itemSkyDrive";
            // 
            // itemStore
            // 
            this.itemStore.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(0)))));
            this.itemStore.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement36.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Store;
            tileItemElement36.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement36.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement36.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement36.Text = "Store";
            tileItemElement36.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemStore.Elements.Add(tileItemElement36);
            this.itemStore.Id = 47;
            this.itemStore.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemStore.Name = "itemStore";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itemCalendar);
            this.tileGroup3.Items.Add(this.itemMaps);
            this.tileGroup3.Items.Add(this.itemReadingList);
            this.tileGroup3.Items.Add(this.itemSports);
            this.tileGroup3.Items.Add(this.itemNews);
            this.tileGroup3.Items.Add(this.itemInternetExplorer);
            this.tileGroup3.Items.Add(this.itemPhotos);
            this.tileGroup3.Items.Add(this.itemFinances);
            this.tileGroup3.Items.Add(this.itemWeather);
            this.tileGroup3.Items.Add(this.itemFoodsDrink);
            this.tileGroup3.Name = "tileGroup3";
            this.tileGroup3.Text = null;
            // 
            // itemCalendar
            // 
            this.itemCalendar.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(56)))), ((int)(((byte)(179)))));
            this.itemCalendar.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement37.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement37.Appearance.Hovered.Options.UseFont = true;
            tileItemElement37.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement37.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement37.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement37.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement37.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement37.Appearance.Normal.Options.UseFont = true;
            tileItemElement37.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement37.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement37.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement37.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement37.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement37.Appearance.Selected.Options.UseFont = true;
            tileItemElement37.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement37.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement37.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement37.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement37.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Calendar;
            tileItemElement37.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement37.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement37.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement37.MaxWidth = 100;
            tileItemElement37.Text = "day";
            tileItemElement37.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement37.TextLocation = new System.Drawing.Point(0, -15);
            tileItemElement38.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement38.Appearance.Hovered.Options.UseFont = true;
            tileItemElement38.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement38.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement38.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement38.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement38.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement38.Appearance.Normal.Options.UseFont = true;
            tileItemElement38.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement38.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement38.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement38.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement38.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement38.Appearance.Selected.Options.UseFont = true;
            tileItemElement38.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement38.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement38.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement38.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement38.MaxWidth = 70;
            tileItemElement38.Text = "DayOfWeek";
            tileItemElement38.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight;
            tileItemElement38.TextLocation = new System.Drawing.Point(-3, 15);
            this.itemCalendar.Elements.Add(tileItemElement37);
            this.itemCalendar.Elements.Add(tileItemElement38);
            this.itemCalendar.Id = 25;
            this.itemCalendar.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemCalendar.Name = "itemCalendar";
            // 
            // itemMaps
            // 
            this.itemMaps.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(0)))), ((int)(((byte)(161)))));
            this.itemMaps.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement39.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Maps;
            tileItemElement39.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement39.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement39.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement39.Text = "Maps";
            tileItemElement39.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemMaps.Elements.Add(tileItemElement39);
            this.itemMaps.Id = 31;
            this.itemMaps.Name = "itemMaps";
            // 
            // itemReadingList
            // 
            this.itemReadingList.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(29)))), ((int)(((byte)(67)))));
            this.itemReadingList.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement40.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.ReadingList;
            tileItemElement40.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement40.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement40.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement40.Text = "Reading List";
            tileItemElement40.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemReadingList.Elements.Add(tileItemElement40);
            this.itemReadingList.Id = 33;
            this.itemReadingList.Name = "itemReadingList";
            // 
            // itemSports
            // 
            this.itemSports.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(61)))), ((int)(((byte)(186)))));
            this.itemSports.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement41.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Sports;
            tileItemElement41.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement41.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement41.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement41.Text = "Sports";
            tileItemElement41.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemSports.Elements.Add(tileItemElement41);
            this.itemSports.Id = 46;
            this.itemSports.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemSports.Name = "itemSports";
            // 
            // itemNews
            // 
            this.itemNews.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(29)))), ((int)(((byte)(71)))));
            this.itemNews.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement42.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.News;
            tileItemElement42.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement42.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement42.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement42.Text = "News";
            tileItemElement42.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemNews.Elements.Add(tileItemElement42);
            this.itemNews.Id = 50;
            this.itemNews.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemNews.Name = "itemNews";
            // 
            // itemInternetExplorer
            // 
            this.itemInternetExplorer.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(126)))), ((int)(((byte)(235)))));
            this.itemInternetExplorer.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement43.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.InternetExplorer;
            tileItemElement43.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement43.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement43.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement43.Text = "Internet Explorer";
            tileItemElement43.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemInternetExplorer.Elements.Add(tileItemElement43);
            this.itemInternetExplorer.Id = 32;
            this.itemInternetExplorer.Name = "itemInternetExplorer";
            // 
            // itemPhotos
            // 
            this.itemPhotos.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(164)))));
            this.itemPhotos.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement44.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Photos;
            tileItemElement44.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement44.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement44.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement44.Text = "Photos";
            tileItemElement44.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemPhotos.Elements.Add(tileItemElement44);
            this.itemPhotos.Id = 52;
            this.itemPhotos.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.itemPhotos.Name = "itemPhotos";
            // 
            // itemFinances
            // 
            this.itemFinances.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.itemFinances.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement45.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Finances;
            tileItemElement45.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement45.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement45.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement45.Text = "Finances";
            tileItemElement45.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemFinances.Elements.Add(tileItemElement45);
            this.itemFinances.Id = 51;
            this.itemFinances.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemFinances.Name = "itemFinances";
            // 
            // itemWeather
            // 
            this.itemWeather.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(130)))), ((int)(((byte)(237)))));
            this.itemWeather.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement46.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Weather;
            tileItemElement46.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement46.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement46.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement46.Text = "Weather";
            tileItemElement46.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemWeather.Elements.Add(tileItemElement46);
            this.itemWeather.Id = 55;
            this.itemWeather.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemWeather.Name = "itemWeather";
            // 
            // itemFoodsDrink
            // 
            this.itemFoodsDrink.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(163)))));
            this.itemFoodsDrink.AppearanceItem.Normal.Options.UseBackColor = true;
            this.itemFoodsDrink.CurrentFrameIndex = 1;
            tileItemElement47.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Foods_Slide1;
            tileItemElement47.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement47.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement47.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement47.Text = "Foods & Drink";
            tileItemElement47.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemFoodsDrink.Elements.Add(tileItemElement47);
            tileItemFrame5.Animation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollDown;
            tileItemElement48.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.FoodsDrink;
            tileItemElement48.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement48.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement48.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement48.Text = "Foods & Drink";
            tileItemElement48.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemFrame5.Elements.Add(tileItemElement48);
            tileItemFrame5.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.FoodsDrink;
            tileItemElement49.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Foods_Slide1;
            tileItemElement49.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement49.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement49.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement49.Text = "Foods & Drink";
            tileItemElement49.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemFrame6.Elements.Add(tileItemElement49);
            tileItemFrame6.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Foods_Slide1;
            tileItemFrame6.Interval = 3500;
            this.itemFoodsDrink.Frames.Add(tileItemFrame5);
            this.itemFoodsDrink.Frames.Add(tileItemFrame6);
            this.itemFoodsDrink.Id = 57;
            this.itemFoodsDrink.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemFoodsDrink.Name = "itemFoodsDrink";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itemTravel);
            this.tileGroup4.Items.Add(this.itemHealthFitness);
            this.tileGroup4.Items.Add(this.itemHelpTips);
            this.tileGroup4.Name = "tileGroup4";
            this.tileGroup4.Text = null;
            // 
            // itemTravel
            // 
            this.itemTravel.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(163)))));
            this.itemTravel.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement50.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.Travel;
            tileItemElement50.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement50.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement50.ImageSize = new System.Drawing.Size(248, 120);
            tileItemElement50.Text = "Travel";
            tileItemElement50.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemTravel.Elements.Add(tileItemElement50);
            this.itemTravel.Id = 53;
            this.itemTravel.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.itemTravel.Name = "itemTravel";
            // 
            // itemHealthFitness
            // 
            this.itemHealthFitness.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(58)))), ((int)(((byte)(181)))));
            this.itemHealthFitness.AppearanceItem.Normal.Options.UseBackColor = true;
            this.itemHealthFitness.CurrentFrameIndex = 1;
            tileItemElement51.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.HealthFitnes_Slide1;
            tileItemElement51.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement51.Text = "Steps, time, distanse: Walking can reach health...";
            tileItemElement51.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemHealthFitness.Elements.Add(tileItemElement51);
            tileItemFrame7.Animation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollTop;
            tileItemElement52.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.HealthFitness;
            tileItemElement52.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement52.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement52.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement52.Text = "Health & Fitness";
            tileItemElement52.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemFrame7.Elements.Add(tileItemElement52);
            tileItemFrame7.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.HealthFitness;
            tileItemFrame7.Interval = 3000;
            tileItemFrame8.Animation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollDown;
            tileItemElement53.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.HealthFitnes_Slide1;
            tileItemElement53.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement53.Text = "Steps, time, distanse: Walking can reach health...";
            tileItemElement53.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemFrame8.Elements.Add(tileItemElement53);
            tileItemFrame8.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.HealthFitnes_Slide1;
            tileItemFrame8.Interval = 3000;
            this.itemHealthFitness.Frames.Add(tileItemFrame7);
            this.itemHealthFitness.Frames.Add(tileItemFrame8);
            this.itemHealthFitness.Id = 58;
            this.itemHealthFitness.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.itemHealthFitness.Name = "itemHealthFitness";
            // 
            // itemHelpTips
            // 
            this.itemHelpTips.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(77)))), ((int)(((byte)(43)))));
            this.itemHelpTips.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement54.Image = global::DevExpress.XtraBars.TileControlDemo.Properties.Resources.HelpTips;
            tileItemElement54.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement54.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement54.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement54.Text = "Help & Tips";
            tileItemElement54.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.itemHelpTips.Elements.Add(tileItemElement54);
            this.itemHelpTips.Id = 59;
            this.itemHelpTips.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.itemHelpTips.Name = "itemHelpTips";
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.tileControl1);
            this.Name = "UserControl1";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Size = new System.Drawing.Size(1144, 805);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itemVideo;
        private DevExpress.XtraEditors.TileItem itemMail;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem itemPeople;
        private DevExpress.XtraEditors.TileItem itemMusic;
        private DevExpress.XtraEditors.TileItem itemGames;
        private DevExpress.XtraEditors.TileItem itemCalendar;
        private DevExpress.XtraEditors.TileItem itemMaps;
        private DevExpress.XtraEditors.TileItem itemInternetExplorer;
        private DevExpress.XtraEditors.TileItem itemReadingList;
        private DevExpress.XtraEditors.TileItem itemSkyDrive;
        private DevExpress.XtraEditors.TileItem itemSports;
        private DevExpress.XtraEditors.TileItem itemStore;
        private DevExpress.XtraEditors.TileItem itemCamera;
        private DevExpress.XtraEditors.TileItem itemNews;
        private DevExpress.XtraEditors.TileItem itemFinances;
        private DevExpress.XtraEditors.TileItem itemPhotos;
        private DevExpress.XtraEditors.TileItem itemTravel;
        private DevExpress.XtraEditors.TileItem itemWeather;
        private DevExpress.XtraEditors.TileItem itemDesktop;
        private DevExpress.XtraEditors.TileItem itemFoodsDrink;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem itemHealthFitness;
        private DevExpress.XtraEditors.TileItem itemHelpTips;
    }
}
