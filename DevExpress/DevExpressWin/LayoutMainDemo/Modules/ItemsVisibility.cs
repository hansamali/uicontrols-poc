using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.XtraLayout.Utils;
namespace DevExpress.XtraLayout.Demos {
    public partial class ItemsVisibility : TutorialControl {
        private LayoutControl layoutControl1;
        private DevExpress.XtraEditors.CheckEdit showDesc;
        private DevExpress.XtraEditors.CheckEdit showP1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private LayoutControlGroup layoutControlGroup1;
        private LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.CheckEdit showP2;
        private LayoutControlGroup layoutControlGroup4;
        private LayoutControlItem layoutControlItem6;
        private SplitterItem splitterItem2;
        private LayoutControlItem descriptionItem;
        private EmptySpaceItem emptySpaceItem2;
        private LayoutControlItem picture2Item;
        private SplitterItem splitterItem1;
        private LayoutControlItem picture1Item;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private LayoutControlItem layoutControlItem1;
        private LayoutControlItem layoutControlItem4;
        public ItemsVisibility() {
            InitializeComponent();
        }
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemsVisibility));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.showP2 = new DevExpress.XtraEditors.CheckEdit();
            this.showDesc = new DevExpress.XtraEditors.CheckEdit();
            this.showP1 = new DevExpress.XtraEditors.CheckEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.descriptionItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.picture2Item = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.picture1Item = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showP2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showP1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.descriptionItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1Item)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.showP2);
            this.layoutControl1.Controls.Add(this.showDesc);
            this.layoutControl1.Controls.Add(this.showP1);
            this.layoutControl1.Controls.Add(this.pictureEdit2);
            this.layoutControl1.Controls.Add(this.pictureEdit1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(477, 447);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(330, 28);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(93, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 10;
            this.simpleButton1.Text = "Rotate Layout 90 Degrees";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // showP2
            // 
            this.showP2.AutoSizeInLayoutControl = true;
            this.showP2.EditValue = true;
            this.showP2.Location = new System.Drawing.Point(228, 28);
            this.showP2.Name = "showP2";
            this.showP2.Properties.Caption = "Show Picture2";
            this.showP2.Size = new System.Drawing.Size(91, 19);
            this.showP2.StyleController = this.layoutControl1;
            this.showP2.TabIndex = 9;
            this.showP2.CheckedChanged += new System.EventHandler(this.checkEdit3_CheckedChanged);
            // 
            // showDesc
            // 
            this.showDesc.AutoSizeInLayoutControl = true;
            this.showDesc.EditValue = true;
            this.showDesc.Location = new System.Drawing.Point(10, 28);
            this.showDesc.Name = "showDesc";
            this.showDesc.Properties.Caption = "Show Description";
            this.showDesc.Size = new System.Drawing.Size(105, 19);
            this.showDesc.StyleController = this.layoutControl1;
            this.showDesc.TabIndex = 8;
            this.showDesc.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // showP1
            // 
            this.showP1.AutoSizeInLayoutControl = true;
            this.showP1.EditValue = true;
            this.showP1.Location = new System.Drawing.Point(126, 28);
            this.showP1.Name = "showP1";
            this.showP1.Properties.Caption = "Show Picture1";
            this.showP1.Size = new System.Drawing.Size(91, 19);
            this.showP1.StyleController = this.layoutControl1;
            this.showP1.TabIndex = 7;
            this.showP1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = global::DevExpress.XtraLayout.Demos.Properties.Resources.bc11;
            this.pictureEdit2.Location = new System.Drawing.Point(7, 64);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Size = new System.Drawing.Size(221, 178);
            this.pictureEdit2.StyleController = this.layoutControl1;
            this.pictureEdit2.TabIndex = 6;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::DevExpress.XtraLayout.Demos.Properties.Resources.bc12;
            this.pictureEdit1.Location = new System.Drawing.Point(245, 64);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(226, 178);
            this.pictureEdit1.StyleController = this.layoutControl1;
            this.pictureEdit1.TabIndex = 5;
            // 
            // memoEdit1
            // 
            this.memoEdit1.EditValue = resources.GetString("memoEdit1.EditValue");
            this.memoEdit1.Location = new System.Drawing.Point(7, 284);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(464, 157);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.splitterItem2,
            this.descriptionItem,
            this.picture2Item,
            this.splitterItem1,
            this.picture1Item});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(477, 447);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "View Options";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.emptySpaceItem2,
            this.layoutControlItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(475, 57);
            this.layoutControlGroup4.Text = "View Options";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.showDesc;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(116, 33);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.showP1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(116, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(102, 33);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.showP2;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(218, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(102, 33);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(424, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(45, 33);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(320, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(104, 33);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // splitterItem2
            // 
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(0, 246);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(475, 6);
            // 
            // descriptionItem
            // 
            this.descriptionItem.Control = this.memoEdit1;
            this.descriptionItem.CustomizationFormText = "layoutControlItem1";
            this.descriptionItem.Location = new System.Drawing.Point(0, 252);
            this.descriptionItem.Name = "descriptionItem";
            this.descriptionItem.Size = new System.Drawing.Size(475, 193);
            this.descriptionItem.Text = "Description";
            this.descriptionItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.descriptionItem.TextSize = new System.Drawing.Size(53, 20);
            // 
            // picture2Item
            // 
            this.picture2Item.Control = this.pictureEdit1;
            this.picture2Item.CustomizationFormText = "layoutControlItem2";
            this.picture2Item.Location = new System.Drawing.Point(238, 57);
            this.picture2Item.Name = "picture2Item";
            this.picture2Item.Size = new System.Drawing.Size(237, 189);
            this.picture2Item.Text = "picture2Item";
            this.picture2Item.TextLocation = DevExpress.Utils.Locations.Left;
            this.picture2Item.TextSize = new System.Drawing.Size(0, 0);
            this.picture2Item.TextToControlDistance = 0;
            this.picture2Item.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(232, 57);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 189);
            // 
            // picture1Item
            // 
            this.picture1Item.Control = this.pictureEdit2;
            this.picture1Item.CustomizationFormText = "layoutControlItem3";
            this.picture1Item.Location = new System.Drawing.Point(0, 57);
            this.picture1Item.Name = "picture1Item";
            this.picture1Item.Size = new System.Drawing.Size(232, 189);
            this.picture1Item.Text = "picture1Item";
            this.picture1Item.TextLocation = DevExpress.Utils.Locations.Left;
            this.picture1Item.TextSize = new System.Drawing.Size(0, 0);
            this.picture1Item.TextToControlDistance = 0;
            this.picture1Item.TextVisible = false;
            // 
            // ItemsVisibility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "ItemsVisibility";
            this.Size = new System.Drawing.Size(477, 447);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.showP2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showP1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.descriptionItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1Item)).EndInit();
            this.ResumeLayout(false);

        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e) {
            UpdateVisibility();
        }
        private void checkEdit1_CheckedChanged(object sender, EventArgs e) {
            UpdateVisibility();
        }
        private void checkEdit3_CheckedChanged(object sender, EventArgs e) {
            UpdateVisibility();
        }
        protected void UpdateVisibility() {
            UpdateVisibility(showDesc.Checked, showP1.Checked, showP2.Checked);
        }
        protected void SetVisibility(LayoutVisibility picture1ItemVisibility, LayoutVisibility picture2ItemVisibility, LayoutVisibility descriptionItemVisibility) {
            picture1Item.Visibility = picture1ItemVisibility;
            picture2Item.Visibility = picture2ItemVisibility;
            descriptionItem.Visibility = descriptionItemVisibility;
            if(picture1ItemVisibility == LayoutVisibility.Always && picture2ItemVisibility == LayoutVisibility.Always)
                splitterItem1.Visibility = LayoutVisibility.Always;
            else
                splitterItem1.Visibility = LayoutVisibility.Never;
            if((picture1ItemVisibility == LayoutVisibility.Always || picture2ItemVisibility == LayoutVisibility.Always) && descriptionItemVisibility == LayoutVisibility.Always)
                splitterItem2.Visibility = LayoutVisibility.Always;
            else
                splitterItem2.Visibility = LayoutVisibility.Never;
        }
        protected void UpdateVisibility(bool showDescription, bool showPicure1, bool showPicture2) {
            SetVisibility(
                LayoutVisibilityConvertor.FromBoolean(showPicure1),
                LayoutVisibilityConvertor.FromBoolean(showPicture2),
                LayoutVisibilityConvertor.FromBoolean(showDescription));
        }

        private void simpleButton1_Click(object sender, EventArgs e) {
            layoutControl1.Root.RotateLayout();
        }
    }
}
