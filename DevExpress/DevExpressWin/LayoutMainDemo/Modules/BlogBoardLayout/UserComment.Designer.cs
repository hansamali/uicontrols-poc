﻿#if DEBUG_1

namespace DevExpress.XtraLayout.Demos.Modules {
    partial class UserComment {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.memoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.labelTime = new DevExpress.XtraEditors.LabelControl();
            this.labelNickName = new DevExpress.XtraEditors.LabelControl();
            this.labelFullName = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.labelFullNameItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelNickNameItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.memoEditItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelTimeItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.pictureEditItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFullNameItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelNickNameItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTimeItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.layoutControl1.Controls.Add(this.memoEdit);
            this.layoutControl1.Controls.Add(this.labelTime);
            this.layoutControl1.Controls.Add(this.labelNickName);
            this.layoutControl1.Controls.Add(this.labelFullName);
            this.layoutControl1.Controls.Add(this.pictureEdit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(897, 161, 730, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(746, 104);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // memoEdit
            // 
            this.memoEdit.Location = new System.Drawing.Point(86, 51);
            this.memoEdit.Name = "memoEdit";
            this.memoEdit.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.memoEdit.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit.Size = new System.Drawing.Size(638, 31);
            this.memoEdit.StyleController = this.layoutControl1;
            this.memoEdit.TabIndex = 8;
            this.memoEdit.UseOptimizedRendering = true;
            // 
            // labelTime
            // 
            this.labelTime.Location = new System.Drawing.Point(399, 22);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(325, 13);
            this.labelTime.StyleController = this.layoutControl1;
            this.labelTime.TabIndex = 7;
            this.labelTime.Text = "labelControl3";
            // 
            // labelNickName
            // 
            this.labelNickName.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNickName.Location = new System.Drawing.Point(229, 22);
            this.labelNickName.Name = "labelNickName";
            this.labelNickName.Size = new System.Drawing.Size(166, 13);
            this.labelNickName.StyleController = this.layoutControl1;
            this.labelNickName.TabIndex = 6;
            this.labelNickName.Text = "labelControl2";
            // 
            // labelFullName
            // 
            this.labelFullName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelFullName.Location = new System.Drawing.Point(86, 22);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(139, 13);
            this.labelFullName.StyleController = this.layoutControl1;
            this.labelFullName.TabIndex = 5;
            this.labelFullName.Text = "labelControl1";
            // 
            // pictureEdit
            // 
            this.pictureEdit.Location = new System.Drawing.Point(22, 22);
            this.pictureEdit.Margin = new System.Windows.Forms.Padding(0);
            this.pictureEdit.MaximumSize = new System.Drawing.Size(60, 54);
            this.pictureEdit.Name = "pictureEdit";
            this.pictureEdit.Properties.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.pictureEdit.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit.Size = new System.Drawing.Size(60, 54);
            this.pictureEdit.StyleController = this.layoutControl1;
            this.pictureEdit.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.layoutControlGroup1.AppearanceGroup.BorderColor = System.Drawing.Color.Black;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup1.CaptionImagePadding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.labelFullNameItem,
            this.labelNickNameItem,
            this.memoEditItem,
            this.labelTimeItem,
            this.pictureEditItem,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 20, 20, 20);
            this.layoutControlGroup1.Size = new System.Drawing.Size(746, 104);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // labelFullNameItem
            // 
            this.labelFullNameItem.Control = this.labelFullName;
            this.labelFullNameItem.CustomizationFormText = "labelFullNameItem";
            this.labelFullNameItem.Location = new System.Drawing.Point(64, 0);
            this.labelFullNameItem.MinSize = new System.Drawing.Size(67, 17);
            this.labelFullNameItem.Name = "labelFullNameItem";
            this.labelFullNameItem.Size = new System.Drawing.Size(143, 17);
            this.labelFullNameItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.labelFullNameItem.Text = "labelFullNameItem";
            this.labelFullNameItem.TextSize = new System.Drawing.Size(0, 0);
            this.labelFullNameItem.TextToControlDistance = 0;
            this.labelFullNameItem.TextVisible = false;
            // 
            // labelNickNameItem
            // 
            this.labelNickNameItem.Control = this.labelNickName;
            this.labelNickNameItem.CustomizationFormText = "labelUserNameItem";
            this.labelNickNameItem.Location = new System.Drawing.Point(207, 0);
            this.labelNickNameItem.MinSize = new System.Drawing.Size(67, 17);
            this.labelNickNameItem.Name = "labelUserNameItem";
            this.labelNickNameItem.Size = new System.Drawing.Size(170, 17);
            this.labelNickNameItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.labelNickNameItem.Text = "labelUserNameItem";
            this.labelNickNameItem.TextSize = new System.Drawing.Size(0, 0);
            this.labelNickNameItem.TextToControlDistance = 0;
            this.labelNickNameItem.TextVisible = false;
            // 
            // memoEditItem
            // 
            this.memoEditItem.Control = this.memoEdit;
            this.memoEditItem.CustomizationFormText = "memoEditItem";
            this.memoEditItem.Location = new System.Drawing.Point(64, 29);
            this.memoEditItem.MinSize = new System.Drawing.Size(14, 18);
            this.memoEditItem.Name = "memoEditItem";
            this.memoEditItem.Size = new System.Drawing.Size(642, 35);
            this.memoEditItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.memoEditItem.Text = "memoEditItem";
            this.memoEditItem.TextSize = new System.Drawing.Size(0, 0);
            this.memoEditItem.TextToControlDistance = 0;
            this.memoEditItem.TextVisible = false;
            // 
            // labelTimeItem
            // 
            this.labelTimeItem.Control = this.labelTime;
            this.labelTimeItem.CustomizationFormText = "labelTimeItem";
            this.labelTimeItem.Location = new System.Drawing.Point(377, 0);
            this.labelTimeItem.MinSize = new System.Drawing.Size(67, 17);
            this.labelTimeItem.Name = "labelTimeItem";
            this.labelTimeItem.Size = new System.Drawing.Size(329, 17);
            this.labelTimeItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.labelTimeItem.Text = "labelTimeItem";
            this.labelTimeItem.TextSize = new System.Drawing.Size(0, 0);
            this.labelTimeItem.TextToControlDistance = 0;
            this.labelTimeItem.TextVisible = false;
            // 
            // pictureEditItem
            // 
            this.pictureEditItem.Control = this.pictureEdit;
            this.pictureEditItem.CustomizationFormText = "pictureEditItem";
            this.pictureEditItem.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.pictureEditItem.Location = new System.Drawing.Point(0, 0);
            this.pictureEditItem.Name = "pictureEditItem";
            this.pictureEditItem.Size = new System.Drawing.Size(64, 64);
            this.pictureEditItem.Text = "pictureEditItem";
            this.pictureEditItem.TextSize = new System.Drawing.Size(0, 0);
            this.pictureEditItem.TextToControlDistance = 0;
            this.pictureEditItem.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(64, 17);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(642, 12);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // UserComment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UserComment";
            this.Size = new System.Drawing.Size(746, 104);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFullNameItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelNickNameItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTimeItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public LayoutControl layoutControl1;
        public LayoutControlGroup layoutControlGroup1;
        public XtraEditors.LabelControl labelTime;
        public XtraEditors.LabelControl labelNickName;
        public XtraEditors.LabelControl labelFullName;
        public XtraEditors.PictureEdit pictureEdit;
        public LayoutControlItem pictureEditItem;
        public LayoutControlItem labelFullNameItem;
        public LayoutControlItem labelNickNameItem;
        public LayoutControlItem labelTimeItem;
        public XtraEditors.MemoEdit memoEdit;
        public LayoutControlItem memoEditItem;
        private EmptySpaceItem emptySpaceItem2;
    }
}
#endif
