using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraRichEdit;

namespace DevExpress.XtraLayout.Demos {
	/// <summary>
	/// Summary description for Employees.
	/// </summary>
    public partial class OneNote : DevExpress.XtraLayout.Demos.TutorialControl {
		public OneNote() {
			// This call is required by the Windows.Forms Form Designer.
			CreateWaitDialog();
			InitializeComponent();
			InitData();
			
			// TODO: Add any initialization after the InitForm call

		}


		private void InitData() {
            RtfLoadHelper.Load("Hyperlinks.rtf", richEdit1);
		}

		private void checkEdit1_CheckedChanged(object sender, System.EventArgs e) {
			layoutControl1.BeginUpdate();
			layoutControl1.EndUpdate();
		}
	}
    public class RtfLoadHelper {
        public static void Load(String fileName, RichEditControl richEditControl) {
            string path = DemoUtils.GetRelativePath(fileName);
            if(!String.IsNullOrEmpty(path))
                richEditControl.LoadDocument(path, DocumentFormat.Rtf);
        }
    }
    public class DocLoadHelper {
        public static void Load(string fileName, RichEditControl richEditControl) {
            string path = DemoUtils.GetRelativePath(fileName);
            if(!string.IsNullOrEmpty(path))
                richEditControl.LoadDocument(path, DocumentFormat.Doc);
        }
    }
    public class HtmlLoadHelper {
        public static void Load(String fileName, RichEditControl richEditControl) {
            string path = DemoUtils.GetRelativePath(fileName);
            if(!String.IsNullOrEmpty(path))
                richEditControl.LoadDocument(path, DocumentFormat.Html);
        }
    }
    public class OpenXmlLoadHelper {
        public static void Load(String fileName, RichEditControl richEditControl) {
            string path = DemoUtils.GetRelativePath(fileName);
            if(!String.IsNullOrEmpty(path))
                richEditControl.LoadDocument(path, DocumentFormat.OpenXml);
        }
    }
    public class DemoUtils {
        public static string GetRelativePath(string name) {
            name = "Data\\" + name;
            string path = System.Windows.Forms.Application.StartupPath;
            string s = "\\";
            for(int i = 0; i <= 10; i++) {
                if(System.IO.File.Exists(path + s + name))
                    return (path + s + name);
                else
                    s += "..\\";
            }
            return "";
        }
        public static void SetConnectionString(System.Data.OleDb.OleDbConnection oleDbConnection, string path) {
            oleDbConnection.ConnectionString = String.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source={0};Mode=Share Deny None;Extended Properties="""";Jet OLEDB:System database="""";Jet OLEDB:Registry Path="""";Jet OLEDB:Database Password="""";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Password="""";Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False", path);
        }
    }
}
