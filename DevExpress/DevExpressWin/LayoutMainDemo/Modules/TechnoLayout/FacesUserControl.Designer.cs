﻿namespace DevExpress.XtraLayout.Demos.Modules.TechnoLayout {
    partial class FacesUserControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FacesUserControl));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit16 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit14 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit19 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit15 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit11 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit17 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit18 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit12 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit10 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit13 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit9 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgFaces = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgFaces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.pictureEdit5);
            this.layoutControl1.Controls.Add(this.pictureEdit16);
            this.layoutControl1.Controls.Add(this.pictureEdit14);
            this.layoutControl1.Controls.Add(this.pictureEdit1);
            this.layoutControl1.Controls.Add(this.pictureEdit2);
            this.layoutControl1.Controls.Add(this.pictureEdit4);
            this.layoutControl1.Controls.Add(this.pictureEdit19);
            this.layoutControl1.Controls.Add(this.pictureEdit8);
            this.layoutControl1.Controls.Add(this.pictureEdit15);
            this.layoutControl1.Controls.Add(this.pictureEdit6);
            this.layoutControl1.Controls.Add(this.pictureEdit11);
            this.layoutControl1.Controls.Add(this.pictureEdit17);
            this.layoutControl1.Controls.Add(this.pictureEdit3);
            this.layoutControl1.Controls.Add(this.pictureEdit7);
            this.layoutControl1.Controls.Add(this.pictureEdit18);
            this.layoutControl1.Controls.Add(this.pictureEdit12);
            this.layoutControl1.Controls.Add(this.pictureEdit10);
            this.layoutControl1.Controls.Add(this.pictureEdit13);
            this.layoutControl1.Controls.Add(this.pictureEdit9);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2769, 237, 969, 642);
            this.layoutControl1.OptionsView.UseParentAutoScaleFactor = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(780, 658);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Location = new System.Drawing.Point(30, 48);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit5.Properties.UseDisabledStatePainter = false;
            this.pictureEdit5.Size = new System.Drawing.Size(266, 266);
            this.pictureEdit5.StyleController = this.layoutControl1;
            this.pictureEdit5.TabIndex = 4;
            this.pictureEdit5.TabStop = true;
            // 
            // pictureEdit16
            // 
            this.pictureEdit16.Location = new System.Drawing.Point(330, 48);
            this.pictureEdit16.Name = "pictureEdit16";
            this.pictureEdit16.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit16.Properties.UseDisabledStatePainter = false;
            this.pictureEdit16.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit16.StyleController = this.layoutControl1;
            this.pictureEdit16.TabIndex = 7;
            // 
            // pictureEdit14
            // 
            this.pictureEdit14.Location = new System.Drawing.Point(430, 48);
            this.pictureEdit14.Name = "pictureEdit14";
            this.pictureEdit14.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit14.Properties.UseDisabledStatePainter = false;
            this.pictureEdit14.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit14.StyleController = this.layoutControl1;
            this.pictureEdit14.TabIndex = 5;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(530, 48);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Properties.UseDisabledStatePainter = false;
            this.pictureEdit1.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit1.StyleController = this.layoutControl1;
            this.pictureEdit1.TabIndex = 10;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Location = new System.Drawing.Point(630, 48);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Properties.UseDisabledStatePainter = false;
            this.pictureEdit2.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit2.StyleController = this.layoutControl1;
            this.pictureEdit2.TabIndex = 6;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Location = new System.Drawing.Point(330, 148);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit4.Properties.UseDisabledStatePainter = false;
            this.pictureEdit4.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit4.StyleController = this.layoutControl1;
            this.pictureEdit4.TabIndex = 18;
            // 
            // pictureEdit19
            // 
            this.pictureEdit19.Location = new System.Drawing.Point(430, 148);
            this.pictureEdit19.Name = "pictureEdit19";
            this.pictureEdit19.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit19.Properties.UseDisabledStatePainter = false;
            this.pictureEdit19.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit19.StyleController = this.layoutControl1;
            this.pictureEdit19.TabIndex = 19;
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.Location = new System.Drawing.Point(530, 148);
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit8.Properties.UseDisabledStatePainter = false;
            this.pictureEdit8.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit8.StyleController = this.layoutControl1;
            this.pictureEdit8.TabIndex = 8;
            // 
            // pictureEdit15
            // 
            this.pictureEdit15.Location = new System.Drawing.Point(630, 148);
            this.pictureEdit15.Name = "pictureEdit15";
            this.pictureEdit15.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit15.Properties.UseDisabledStatePainter = false;
            this.pictureEdit15.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit15.StyleController = this.layoutControl1;
            this.pictureEdit15.TabIndex = 9;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Location = new System.Drawing.Point(330, 248);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit6.Properties.UseDisabledStatePainter = false;
            this.pictureEdit6.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit6.StyleController = this.layoutControl1;
            this.pictureEdit6.TabIndex = 12;
            // 
            // pictureEdit11
            // 
            this.pictureEdit11.Location = new System.Drawing.Point(430, 248);
            this.pictureEdit11.Name = "pictureEdit11";
            this.pictureEdit11.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit11.Properties.UseDisabledStatePainter = false;
            this.pictureEdit11.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit11.StyleController = this.layoutControl1;
            this.pictureEdit11.TabIndex = 20;
            // 
            // pictureEdit17
            // 
            this.pictureEdit17.Location = new System.Drawing.Point(530, 248);
            this.pictureEdit17.Name = "pictureEdit17";
            this.pictureEdit17.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit17.Properties.UseDisabledStatePainter = false;
            this.pictureEdit17.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit17.StyleController = this.layoutControl1;
            this.pictureEdit17.TabIndex = 16;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Location = new System.Drawing.Point(630, 248);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit3.Properties.UseDisabledStatePainter = false;
            this.pictureEdit3.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit3.StyleController = this.layoutControl1;
            this.pictureEdit3.TabIndex = 22;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Location = new System.Drawing.Point(30, 348);
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit7.Properties.UseDisabledStatePainter = false;
            this.pictureEdit7.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit7.StyleController = this.layoutControl1;
            this.pictureEdit7.TabIndex = 13;
            // 
            // pictureEdit18
            // 
            this.pictureEdit18.Location = new System.Drawing.Point(130, 348);
            this.pictureEdit18.Name = "pictureEdit18";
            this.pictureEdit18.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit18.Properties.UseDisabledStatePainter = false;
            this.pictureEdit18.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit18.StyleController = this.layoutControl1;
            this.pictureEdit18.TabIndex = 17;
            // 
            // pictureEdit12
            // 
            this.pictureEdit12.Location = new System.Drawing.Point(230, 348);
            this.pictureEdit12.Name = "pictureEdit12";
            this.pictureEdit12.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit12.Properties.UseDisabledStatePainter = false;
            this.pictureEdit12.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit12.StyleController = this.layoutControl1;
            this.pictureEdit12.TabIndex = 14;
            // 
            // pictureEdit10
            // 
            this.pictureEdit10.Location = new System.Drawing.Point(330, 348);
            this.pictureEdit10.Name = "pictureEdit10";
            this.pictureEdit10.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit10.Properties.UseDisabledStatePainter = false;
            this.pictureEdit10.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit10.StyleController = this.layoutControl1;
            this.pictureEdit10.TabIndex = 21;
            // 
            // pictureEdit13
            // 
            this.pictureEdit13.Location = new System.Drawing.Point(430, 348);
            this.pictureEdit13.Name = "pictureEdit13";
            this.pictureEdit13.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit13.Properties.UseDisabledStatePainter = false;
            this.pictureEdit13.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit13.StyleController = this.layoutControl1;
            this.pictureEdit13.TabIndex = 15;
            // 
            // pictureEdit9
            // 
            this.pictureEdit9.Location = new System.Drawing.Point(530, 348);
            this.pictureEdit9.Name = "pictureEdit9";
            this.pictureEdit9.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit9.Properties.UseDisabledStatePainter = false;
            this.pictureEdit9.Size = new System.Drawing.Size(66, 66);
            this.pictureEdit9.StyleController = this.layoutControl1;
            this.pictureEdit9.TabIndex = 23;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgFaces});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(780, 658);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lcgFaces
            // 
            this.lcgFaces.CellSize = new System.Drawing.Size(10, 10);
            this.lcgFaces.CustomizationFormText = "lcgFaces";
            this.lcgFaces.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.layoutControlItem7,
            this.layoutControlItem3,
            this.layoutControlItem8,
            this.layoutControlItem15,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem9,
            this.layoutControlItem16,
            this.layoutControlItem13,
            this.layoutControlItem18,
            this.layoutControlItem10,
            this.layoutControlItem14,
            this.layoutControlItem11,
            this.layoutControlItem17,
            this.layoutControlItem12,
            this.layoutControlItem19});
            this.lcgFaces.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Flow;
            this.lcgFaces.Location = new System.Drawing.Point(0, 0);
            this.lcgFaces.Name = "lcgFaces";
            this.lcgFaces.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 10, 10);
            this.lcgFaces.ShowInCustomizationForm = false;
            this.lcgFaces.Size = new System.Drawing.Size(776, 654);
            this.lcgFaces.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgFaces.Text = "Faces";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pictureEdit5;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(300, 300);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.pictureEdit16;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(300, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.pictureEdit14;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(400, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.pictureEdit1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(500, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.pictureEdit2;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(600, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.pictureEdit4;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(300, 100);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.pictureEdit19;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(400, 100);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem15.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem15.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.pictureEdit8;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(500, 100);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.pictureEdit15;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(600, 100);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.pictureEdit6;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(300, 200);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.pictureEdit11;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(400, 200);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem16.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.pictureEdit17;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(500, 200);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.pictureEdit3;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(600, 200);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem18.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.pictureEdit7;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 300);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.pictureEdit18;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(100, 300);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.pictureEdit12;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(200, 300);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.pictureEdit10;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(300, 300);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem17.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.pictureEdit13;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(400, 300);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.pictureEdit9;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(500, 300);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(100, 100);
            this.layoutControlItem19.Spacing = new DevExpress.XtraLayout.Utils.Padding(15, 15, 15, 15);
            this.layoutControlItem19.Text = "12";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(602, 602);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper0_1_, "Striper0_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Striper0_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper1_1_, "Striper1_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Striper1_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper10_1_, "Striper10_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Striper10_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper12_1_, "Striper12_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "Striper12_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper13_1_, "Striper13_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Striper13_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper14_1_, "Striper14_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Striper14_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper15_1_, "Striper15_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "Striper15_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper16_1_, "Striper16_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "Striper16_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper18_1_, "Striper18_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "Striper18_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper19_1_, "Striper19_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 9);
            this.imageCollection1.Images.SetKeyName(9, "Striper19_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper2_1_, "Striper2_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 10);
            this.imageCollection1.Images.SetKeyName(10, "Striper2_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper20_1_, "Striper20_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 11);
            this.imageCollection1.Images.SetKeyName(11, "Striper20_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper21_1_, "Striper21_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 12);
            this.imageCollection1.Images.SetKeyName(12, "Striper21_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper22_1_, "Striper22_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 13);
            this.imageCollection1.Images.SetKeyName(13, "Striper22_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper24_1_, "Striper24_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 14);
            this.imageCollection1.Images.SetKeyName(14, "Striper24_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper25_1_, "Striper25_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 15);
            this.imageCollection1.Images.SetKeyName(15, "Striper25_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper26_1_, "Striper26_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 16);
            this.imageCollection1.Images.SetKeyName(16, "Striper26_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper27_1_, "Striper27_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 17);
            this.imageCollection1.Images.SetKeyName(17, "Striper27_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper28_1_, "Striper28_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 18);
            this.imageCollection1.Images.SetKeyName(18, "Striper28_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper3_1_, "Striper3_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 19);
            this.imageCollection1.Images.SetKeyName(19, "Striper3_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper4_1_, "Striper4_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 20);
            this.imageCollection1.Images.SetKeyName(20, "Striper4_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper6_1_, "Striper6_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 21);
            this.imageCollection1.Images.SetKeyName(21, "Striper6_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper7_1_, "Striper7_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 22);
            this.imageCollection1.Images.SetKeyName(22, "Striper7_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper8_1_, "Striper8_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 23);
            this.imageCollection1.Images.SetKeyName(23, "Striper8_1_");
            this.imageCollection1.InsertImage(global::DevExpress.XtraLayout.Demos.Properties.Resources.Striper9_1_, "Striper9_1_", typeof(global::DevExpress.XtraLayout.Demos.Properties.Resources), 24);
            this.imageCollection1.Images.SetKeyName(24, "Striper9_1_");
            // 
            // FacesUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "FacesUserControl";
            this.Size = new System.Drawing.Size(780, 658);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgFaces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public LayoutControl layoutControl1;
        private LayoutControlGroup layoutControlGroup1;
        private XtraEditors.PictureEdit pictureEdit5;
        private XtraEditors.PictureEdit pictureEdit16;
        private XtraEditors.PictureEdit pictureEdit14;
        private XtraEditors.PictureEdit pictureEdit1;
        private XtraEditors.PictureEdit pictureEdit2;
        private XtraEditors.PictureEdit pictureEdit4;
        private XtraEditors.PictureEdit pictureEdit19;
        private XtraEditors.PictureEdit pictureEdit8;
        private XtraEditors.PictureEdit pictureEdit15;
        private XtraEditors.PictureEdit pictureEdit6;
        private XtraEditors.PictureEdit pictureEdit11;
        private XtraEditors.PictureEdit pictureEdit17;
        private XtraEditors.PictureEdit pictureEdit3;
        private XtraEditors.PictureEdit pictureEdit7;
        private XtraEditors.PictureEdit pictureEdit18;
        private XtraEditors.PictureEdit pictureEdit12;
        private XtraEditors.PictureEdit pictureEdit10;
        private XtraEditors.PictureEdit pictureEdit13;
        private XtraEditors.PictureEdit pictureEdit9;
        public LayoutControlGroup lcgFaces;
        public LayoutControlItem layoutControlItem1;
        private LayoutControlItem layoutControlItem4;
        private LayoutControlItem layoutControlItem2;
        private LayoutControlItem layoutControlItem7;
        private LayoutControlItem layoutControlItem3;
        private LayoutControlItem layoutControlItem8;
        private LayoutControlItem layoutControlItem15;
        private LayoutControlItem layoutControlItem5;
        private LayoutControlItem layoutControlItem6;
        private LayoutControlItem layoutControlItem9;
        private LayoutControlItem layoutControlItem16;
        private LayoutControlItem layoutControlItem13;
        private LayoutControlItem layoutControlItem18;
        private LayoutControlItem layoutControlItem10;
        private LayoutControlItem layoutControlItem14;
        private LayoutControlItem layoutControlItem11;
        private LayoutControlItem layoutControlItem17;
        private LayoutControlItem layoutControlItem12;
        private LayoutControlItem layoutControlItem19;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}
