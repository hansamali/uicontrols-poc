namespace DevExpress.XtraLayout.Demos {
    partial class 
        MultilanguageForm {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultilanguageForm));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.xpCollection1 = new DevExpress.Xpo.XPCollection(this.components);
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutModeGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xpCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutModeGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControl1.Appearance.DisabledLayoutGroupCaption.FontSizeDelta")));
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControl1.Appearance.DisabledLayoutGroupCaption.FontStyleDelta")));
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControl1.Appearance.DisabledLayoutGroupCaption.GradientMode")));
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControl1.Appearance.DisabledLayoutGroupCaption.Image")));
            this.layoutControl1.Appearance.DisabledLayoutItem.FontSizeDelta = ((int)(resources.GetObject("layoutControl1.Appearance.DisabledLayoutItem.FontSizeDelta")));
            this.layoutControl1.Appearance.DisabledLayoutItem.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControl1.Appearance.DisabledLayoutItem.FontStyleDelta")));
            this.layoutControl1.Appearance.DisabledLayoutItem.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControl1.Appearance.DisabledLayoutItem.GradientMode")));
            this.layoutControl1.Appearance.DisabledLayoutItem.Image = ((System.Drawing.Image)(resources.GetObject("layoutControl1.Appearance.DisabledLayoutItem.Image")));
            this.layoutControl1.Controls.Add(this.dataNavigator1);
            this.layoutControl1.Controls.Add(this.labelControl8);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Controls.Add(this.labelControl7);
            this.layoutControl1.Controls.Add(this.labelControl6);
            this.layoutControl1.Controls.Add(this.labelControl5);
            this.layoutControl1.Controls.Add(this.labelControl4);
            this.layoutControl1.Controls.Add(this.labelControl3);
            this.layoutControl1.Controls.Add(this.labelControl2);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.pictureEdit1);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(524, 175, 852, 623);
            this.layoutControl1.OptionsView.UseParentAutoScaleFactor = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            // 
            // dataNavigator1
            // 
            resources.ApplyResources(this.dataNavigator1, "dataNavigator1");
            this.dataNavigator1.DataSource = this.xpCollection1;
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.StyleController = this.layoutControl1;
            // 
            // xpCollection1
            // 
            this.xpCollection1.ObjectType = typeof(DevExpress.XtraLayout.Demos.VideoCatalogItem);
            // 
            // labelControl8
            // 
            resources.ApplyResources(this.labelControl8, "labelControl8");
             this.labelControl8.AppearanceDisabled.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.DisabledImage")));
            this.labelControl8.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl8.Appearance.Font")));
            this.labelControl8.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl8.Appearance.FontSizeDelta")));
            this.labelControl8.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl8.Appearance.FontStyleDelta")));
            this.labelControl8.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl8.Appearance.GradientMode")));
            this.labelControl8.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.HoverImage")));
            this.labelControl8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.Image")));
            this.labelControl8.AppearancePressed.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.PressedImage")));
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.StyleController = this.layoutControl1;
            // 
            // memoEdit1
            // 
            resources.ApplyResources(this.memoEdit1, "memoEdit1");
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.xpCollection1, "Description", true));
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.AccessibleDescription = resources.GetString("memoEdit1.Properties.AccessibleDescription");
            this.memoEdit1.Properties.AccessibleName = resources.GetString("memoEdit1.Properties.AccessibleName");
            this.memoEdit1.Properties.NullValuePrompt = resources.GetString("memoEdit1.Properties.NullValuePrompt");
            this.memoEdit1.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("memoEdit1.Properties.NullValuePromptShowForEmptyValue")));
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabStop = false;
            // 
            // labelControl7
            // 
            resources.ApplyResources(this.labelControl7, "labelControl7");
            this.labelControl7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.xpCollection1, "RunTime", true));
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.StyleController = this.layoutControl1;
            // 
            // labelControl6
            // 
            resources.ApplyResources(this.labelControl6, "labelControl6");
            this.labelControl6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.xpCollection1, "Country", true));
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.StyleController = this.layoutControl1;
            // 
            // labelControl5
            // 
            resources.ApplyResources(this.labelControl5, "labelControl5");
            this.labelControl5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.xpCollection1, "Cast", true));
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.StyleController = this.layoutControl1;
            // 
            // labelControl4
            // 
            resources.ApplyResources(this.labelControl4, "labelControl4");
            this.labelControl4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.xpCollection1, "Genre", true));
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.StyleController = this.layoutControl1;
            // 
            // labelControl3
            // 
            resources.ApplyResources(this.labelControl3, "labelControl3");
            this.labelControl3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.xpCollection1, "Year", true));
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.StyleController = this.layoutControl1;
            // 
            // labelControl2
            // 
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.xpCollection1, "Director", true));
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.StyleController = this.layoutControl1;
            // 
            // labelControl1
            // 
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.AppearanceDisabled.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.DisabledImage")));
            this.labelControl1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl1.Appearance.Font")));
            this.labelControl1.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl1.Appearance.FontSizeDelta")));
            this.labelControl1.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl1.Appearance.FontStyleDelta")));
            this.labelControl1.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl1.Appearance.ForeColor")));
            this.labelControl1.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl1.Appearance.GradientMode")));
            this.labelControl1.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.HoverImage")));
            this.labelControl1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.Image")));
            this.labelControl1.AppearancePressed.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.PressedImage")));
            this.labelControl1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.xpCollection1, "Caption", true));
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.StyleController = this.layoutControl1;
            // 
            // pictureEdit1
            // 
            resources.ApplyResources(this.pictureEdit1, "pictureEdit1");
            this.pictureEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.xpCollection1, "Photo", true));
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AccessibleDescription = resources.GetString("pictureEdit1.Properties.AccessibleDescription");
            this.pictureEdit1.Properties.AccessibleName = resources.GetString("pictureEdit1.Properties.AccessibleName");
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.StyleController = this.layoutControl1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceItemCaption.Font = ((System.Drawing.Font)(resources.GetObject("layoutControlGroup1.AppearanceItemCaption.Font")));
            this.layoutControlGroup1.AppearanceItemCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControlGroup1.AppearanceItemCaption.FontSizeDelta")));
            this.layoutControlGroup1.AppearanceItemCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControlGroup1.AppearanceItemCaption.FontStyleDelta")));
            this.layoutControlGroup1.AppearanceItemCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControlGroup1.AppearanceItemCaption.GradientMode")));
            this.layoutControlGroup1.AppearanceItemCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup1.AppearanceItemCaption.Image")));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
            resources.ApplyResources(this.layoutControlGroup1, "layoutControlGroup1");
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem2,
            this.layoutModeGroup,
            this.emptySpaceItem3,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(488, 361);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pictureEdit1;
            resources.ApplyResources(this.layoutControlItem1, "layoutControlItem1");
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 35);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(230, 195);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(230, 195);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(230, 195);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.memoEdit1;
            resources.ApplyResources(this.layoutControlItem9, "layoutControlItem9");
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 240);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem9.Size = new System.Drawing.Size(486, 90);
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(77, 13);
            this.layoutControlItem9.TextToControlDistance = 0;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.labelControl8;
            resources.ApplyResources(this.layoutControlItem10, "layoutControlItem10");
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem10.Size = new System.Drawing.Size(486, 35);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.dataNavigator1;
            resources.ApplyResources(this.layoutControlItem11, "layoutControlItem11");
            this.layoutControlItem11.Location = new System.Drawing.Point(291, 330);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem11.Size = new System.Drawing.Size(195, 29);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            resources.ApplyResources(this.emptySpaceItem2, "emptySpaceItem2");
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 330);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.emptySpaceItem2.Size = new System.Drawing.Size(291, 29);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutModeGroup
            // 
            this.layoutModeGroup.AllowDrawBackground = false;
            this.layoutModeGroup.CellSize = new System.Drawing.Size(12, 12);
            resources.ApplyResources(this.layoutModeGroup, "layoutModeGroup");
            this.layoutModeGroup.GroupBordersVisible = false;
            this.layoutModeGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutModeGroup.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Flow;
            this.layoutModeGroup.Location = new System.Drawing.Point(240, 35);
            this.layoutModeGroup.Name = "layoutModeGroup";
            this.layoutModeGroup.Size = new System.Drawing.Size(246, 205);
            this.layoutModeGroup.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControlItem2.AppearanceItemCaption.FontSizeDelta")));
            this.layoutControlItem2.AppearanceItemCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControlItem2.AppearanceItemCaption.FontStyleDelta")));
            this.layoutControlItem2.AppearanceItemCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControlItem2.AppearanceItemCaption.GradientMode")));
            this.layoutControlItem2.AppearanceItemCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlItem2.AppearanceItemCaption.Image")));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem2.Control = this.labelControl1;
            resources.ApplyResources(this.layoutControlItem2, "layoutControlItem2");
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(192, 36);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControlItem3.AppearanceItemCaption.FontSizeDelta")));
            this.layoutControlItem3.AppearanceItemCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControlItem3.AppearanceItemCaption.FontStyleDelta")));
            this.layoutControlItem3.AppearanceItemCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControlItem3.AppearanceItemCaption.GradientMode")));
            this.layoutControlItem3.AppearanceItemCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlItem3.AppearanceItemCaption.Image")));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem3.Control = this.labelControl2;
            resources.ApplyResources(this.layoutControlItem3, "layoutControlItem3");
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControlItem4.AppearanceItemCaption.FontSizeDelta")));
            this.layoutControlItem4.AppearanceItemCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControlItem4.AppearanceItemCaption.FontStyleDelta")));
            this.layoutControlItem4.AppearanceItemCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControlItem4.AppearanceItemCaption.GradientMode")));
            this.layoutControlItem4.AppearanceItemCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlItem4.AppearanceItemCaption.Image")));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem4.Control = this.labelControl3;
            resources.ApplyResources(this.layoutControlItem4, "layoutControlItem4");
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControlItem5.AppearanceItemCaption.FontSizeDelta")));
            this.layoutControlItem5.AppearanceItemCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControlItem5.AppearanceItemCaption.FontStyleDelta")));
            this.layoutControlItem5.AppearanceItemCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControlItem5.AppearanceItemCaption.GradientMode")));
            this.layoutControlItem5.AppearanceItemCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlItem5.AppearanceItemCaption.Image")));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem5.Control = this.labelControl4;
            resources.ApplyResources(this.layoutControlItem5, "layoutControlItem5");
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControlItem6.AppearanceItemCaption.FontSizeDelta")));
            this.layoutControlItem6.AppearanceItemCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControlItem6.AppearanceItemCaption.FontStyleDelta")));
            this.layoutControlItem6.AppearanceItemCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControlItem6.AppearanceItemCaption.GradientMode")));
            this.layoutControlItem6.AppearanceItemCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlItem6.AppearanceItemCaption.Image")));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem6.Control = this.labelControl5;
            resources.ApplyResources(this.layoutControlItem6, "layoutControlItem6");
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 108);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem6.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControlItem7.AppearanceItemCaption.FontSizeDelta")));
            this.layoutControlItem7.AppearanceItemCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControlItem7.AppearanceItemCaption.FontStyleDelta")));
            this.layoutControlItem7.AppearanceItemCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControlItem7.AppearanceItemCaption.GradientMode")));
            this.layoutControlItem7.AppearanceItemCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlItem7.AppearanceItemCaption.Image")));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem7.Control = this.labelControl6;
            resources.ApplyResources(this.layoutControlItem7, "layoutControlItem7");
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 132);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem7.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.FontSizeDelta = ((int)(resources.GetObject("layoutControlItem8.AppearanceItemCaption.FontSizeDelta")));
            this.layoutControlItem8.AppearanceItemCaption.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("layoutControlItem8.AppearanceItemCaption.FontStyleDelta")));
            this.layoutControlItem8.AppearanceItemCaption.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("layoutControlItem8.AppearanceItemCaption.GradientMode")));
            this.layoutControlItem8.AppearanceItemCaption.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlItem8.AppearanceItemCaption.Image")));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem8.Control = this.labelControl7;
            resources.ApplyResources(this.layoutControlItem8, "layoutControlItem8");
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem8.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(62, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            resources.ApplyResources(this.emptySpaceItem3, "emptySpaceItem3");
            this.emptySpaceItem3.Location = new System.Drawing.Point(230, 35);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(10, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 205);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            resources.ApplyResources(this.emptySpaceItem1, "emptySpaceItem1");
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 230);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(230, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // MultilanguageForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "MultilanguageForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xpCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutModeGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.Xpo.XPCollection xpCollection1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private LayoutControlGroup layoutModeGroup;
        private System.ComponentModel.IContainer components;
        public LayoutControl layoutControl1;
        private EmptySpaceItem emptySpaceItem3;
        private LayoutControlItem layoutControlItem2;
        private EmptySpaceItem emptySpaceItem1;
    }
}
