namespace DevExpress.XtraLayout.Demos {
    partial class Validating {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>                                                                                       
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Validating));
            this.dsCategories1 = new DevExpress.XtraLayout.Demos.dsCategories();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.subTotalTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.discountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.orderUnitPriceCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.unitsOnOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.unitsInStockSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.reorderLevelSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.productsUnitPriceCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.quantityPerUnitTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.supplierLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.productNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator2 = new DevExpress.XtraEditors.DataNavigator();
            this.orderIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.quantitySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator3 = new DevExpress.XtraEditors.DataNavigator();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dsCategories1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subTotalTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderUnitPriceCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitsOnOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitsInStockSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reorderLevelSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsUnitPriceCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityPerUnitTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.supplierLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantitySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // dsCategories1
            // 
            this.dsCategories1.DataSetName = "dsCategories";
            this.dsCategories1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsCategories1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.subTotalTextEdit);
            this.layoutControl1.Controls.Add(this.discountSpinEdit);
            this.layoutControl1.Controls.Add(this.orderUnitPriceCalcEdit);
            this.layoutControl1.Controls.Add(this.checkEdit1);
            this.layoutControl1.Controls.Add(this.unitsOnOrderSpinEdit);
            this.layoutControl1.Controls.Add(this.unitsInStockSpinEdit);
            this.layoutControl1.Controls.Add(this.reorderLevelSpinEdit);
            this.layoutControl1.Controls.Add(this.productsUnitPriceCalcEdit);
            this.layoutControl1.Controls.Add(this.quantityPerUnitTextEdit);
            this.layoutControl1.Controls.Add(this.supplierLookUpEdit);
            this.layoutControl1.Controls.Add(this.productNameTextEdit);
            this.layoutControl1.Controls.Add(this.dataNavigator2);
            this.layoutControl1.Controls.Add(this.orderIdTextEdit);
            this.layoutControl1.Controls.Add(this.quantitySpinEdit);
            this.layoutControl1.Controls.Add(this.dataNavigator3);
            this.layoutControl1.OptionsView.UseParentAutoScaleFactor = true;
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(725, 512);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // subTotalTextEdit
            // 
            this.subTotalTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.SubTotal", true));
            this.subTotalTextEdit.EditValue = "textEdit5";
            this.subTotalTextEdit.Location = new System.Drawing.Point(113, 448);
            this.subTotalTextEdit.Name = "subTotalTextEdit";
            this.subTotalTextEdit.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.subTotalTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.subTotalTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.subTotalTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.subTotalTextEdit.Properties.Mask.EditMask = "c";
            this.subTotalTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.subTotalTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.subTotalTextEdit.Properties.ReadOnly = true;
            this.subTotalTextEdit.Size = new System.Drawing.Size(581, 20);
            this.subTotalTextEdit.StyleController = this.layoutControl1;
            this.subTotalTextEdit.TabIndex = 17;
            // 
            // discountSpinEdit
            // 
            this.discountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.Discount", true));
            this.discountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.discountSpinEdit.Location = new System.Drawing.Point(113, 355);
            this.discountSpinEdit.Name = "discountSpinEdit";
            this.discountSpinEdit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.discountSpinEdit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.discountSpinEdit.Properties.Mask.EditMask = "p";
            this.discountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.discountSpinEdit.Properties.ValidateOnEnterKey = true;
            this.discountSpinEdit.Size = new System.Drawing.Size(581, 20);
            this.discountSpinEdit.StyleController = this.layoutControl1;
            this.discountSpinEdit.TabIndex = 16;
            this.discountSpinEdit.Tag = "Discount";
            this.discountSpinEdit.ToolTip = "This field value can\'t be larger than 50%";
            this.discountSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.discountSpinEdit_Validating);
            this.discountSpinEdit.Validated += new System.EventHandler(this.Detail_Validated);
            // 
            // orderUnitPriceCalcEdit
            // 
            this.orderUnitPriceCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.UnitPrice", true));
            this.orderUnitPriceCalcEdit.EditValue = ((object)(resources.GetObject("orderUnitPriceCalcEdit.EditValue")));
            this.orderUnitPriceCalcEdit.Location = new System.Drawing.Point(113, 417);
            this.orderUnitPriceCalcEdit.Name = "orderUnitPriceCalcEdit";
            this.orderUnitPriceCalcEdit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.orderUnitPriceCalcEdit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.orderUnitPriceCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.orderUnitPriceCalcEdit.Properties.Mask.EditMask = "c";
            this.orderUnitPriceCalcEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.orderUnitPriceCalcEdit.Properties.ValidateOnEnterKey = true;
            this.orderUnitPriceCalcEdit.Size = new System.Drawing.Size(581, 20);
            this.orderUnitPriceCalcEdit.StyleController = this.layoutControl1;
            this.orderUnitPriceCalcEdit.TabIndex = 15;
            this.orderUnitPriceCalcEdit.Tag = "Unit Price";
            this.orderUnitPriceCalcEdit.ToolTip = "This field value can\'t be less than 0";
            this.orderUnitPriceCalcEdit.Validating += new System.ComponentModel.CancelEventHandler(this.unitPriceCalcEdit_Validating);
            this.orderUnitPriceCalcEdit.Validated += new System.EventHandler(this.Detail_Validated);
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.Discontinued", true));
            this.checkEdit1.EditValue = ((object)(resources.GetObject("checkEdit1.EditValue")));
            this.checkEdit1.Location = new System.Drawing.Point(15, 240);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Discontinued";
            this.checkEdit1.Size = new System.Drawing.Size(95, 19);
            this.checkEdit1.StyleController = this.layoutControl1;
            this.checkEdit1.TabIndex = 14;
            this.checkEdit1.TabStop = false;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // unitsOnOrderSpinEdit
            // 
            this.unitsOnOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.UnitsOnOrder", true));
            this.unitsOnOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.unitsOnOrderSpinEdit.Location = new System.Drawing.Point(113, 209);
            this.unitsOnOrderSpinEdit.Name = "unitsOnOrderSpinEdit";
            this.unitsOnOrderSpinEdit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.unitsOnOrderSpinEdit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.unitsOnOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.unitsOnOrderSpinEdit.Properties.IsFloatValue = false;
            this.unitsOnOrderSpinEdit.Properties.Mask.EditMask = "N00";
            this.unitsOnOrderSpinEdit.Size = new System.Drawing.Size(581, 20);
            this.unitsOnOrderSpinEdit.StyleController = this.layoutControl1;
            this.unitsOnOrderSpinEdit.TabIndex = 13;
            this.unitsOnOrderSpinEdit.Tag = "Units On Order";
            this.unitsOnOrderSpinEdit.ToolTip = "This field value can\'t be less than 0";
            this.unitsOnOrderSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.quantitySpinEdit_Validating);
            // 
            // unitsInStockSpinEdit
            // 
            this.unitsInStockSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.UnitsInStock", true));
            this.unitsInStockSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.unitsInStockSpinEdit.Location = new System.Drawing.Point(113, 147);
            this.unitsInStockSpinEdit.Name = "unitsInStockSpinEdit";
            this.unitsInStockSpinEdit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.unitsInStockSpinEdit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.unitsInStockSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.unitsInStockSpinEdit.Properties.IsFloatValue = false;
            this.unitsInStockSpinEdit.Properties.Mask.EditMask = "N00";
            this.unitsInStockSpinEdit.Size = new System.Drawing.Size(581, 20);
            this.unitsInStockSpinEdit.StyleController = this.layoutControl1;
            this.unitsInStockSpinEdit.TabIndex = 12;
            this.unitsInStockSpinEdit.Tag = "Units In Stock";
            this.unitsInStockSpinEdit.ToolTip = "This field value can\'t be less than 0";
            this.unitsInStockSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.quantitySpinEdit_Validating);
            // 
            // reorderLevelSpinEdit
            // 
            this.reorderLevelSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ReorderLevel", true));
            this.reorderLevelSpinEdit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.reorderLevelSpinEdit.Location = new System.Drawing.Point(113, 178);
            this.reorderLevelSpinEdit.Name = "reorderLevelSpinEdit";
            this.reorderLevelSpinEdit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.reorderLevelSpinEdit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.reorderLevelSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.reorderLevelSpinEdit.Properties.IsFloatValue = false;
            this.reorderLevelSpinEdit.Properties.Mask.EditMask = "N00";
            this.reorderLevelSpinEdit.Size = new System.Drawing.Size(581, 20);
            this.reorderLevelSpinEdit.StyleController = this.layoutControl1;
            this.reorderLevelSpinEdit.TabIndex = 11;
            this.reorderLevelSpinEdit.Tag = "Reorder Level";
            this.reorderLevelSpinEdit.ToolTip = "This field value can\'t be less than 0";
            this.reorderLevelSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.quantitySpinEdit_Validating);
            // 
            // productsUnitPriceCalcEdit
            // 
            this.productsUnitPriceCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.UnitPrice", true));
            this.productsUnitPriceCalcEdit.EditValue = ((object)(resources.GetObject("productsUnitPriceCalcEdit.EditValue")));
            this.productsUnitPriceCalcEdit.Location = new System.Drawing.Point(113, 116);
            this.productsUnitPriceCalcEdit.Name = "productsUnitPriceCalcEdit";
            this.productsUnitPriceCalcEdit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.productsUnitPriceCalcEdit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.productsUnitPriceCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.productsUnitPriceCalcEdit.Properties.Mask.EditMask = "c";
            this.productsUnitPriceCalcEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.productsUnitPriceCalcEdit.Size = new System.Drawing.Size(581, 20);
            this.productsUnitPriceCalcEdit.StyleController = this.layoutControl1;
            this.productsUnitPriceCalcEdit.TabIndex = 10;
            this.productsUnitPriceCalcEdit.Tag = "Unit Price";
            this.productsUnitPriceCalcEdit.ToolTip = "This field value can\'t be less than 0";
            this.productsUnitPriceCalcEdit.Validating += new System.ComponentModel.CancelEventHandler(this.unitPriceCalcEdit_Validating);
            // 
            // quantityPerUnitTextEdit
            // 
            this.quantityPerUnitTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsCategories1, "Categories.CategoriesProducts.QuantityPerUnit", true));
            this.quantityPerUnitTextEdit.Location = new System.Drawing.Point(113, 85);
            this.quantityPerUnitTextEdit.Name = "quantityPerUnitTextEdit";
            this.quantityPerUnitTextEdit.Size = new System.Drawing.Size(581, 20);
            this.quantityPerUnitTextEdit.StyleController = this.layoutControl1;
            this.quantityPerUnitTextEdit.TabIndex = 9;
            // 
            // supplierLookUpEdit
            // 
            this.supplierLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.SupplierID", true));
            this.supplierLookUpEdit.EditValue = ((object)(resources.GetObject("supplierLookUpEdit.EditValue")));
            this.supplierLookUpEdit.Location = new System.Drawing.Point(113, 54);
            this.supplierLookUpEdit.Name = "supplierLookUpEdit";
            this.supplierLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.supplierLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyName", "Company Name", 78, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ContactName", "Contact Name", 71, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Country", "Country", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.supplierLookUpEdit.Properties.DataSource = this.dsCategories1.Suppliers;
            this.supplierLookUpEdit.Properties.DisplayMember = "CompanyName";
            this.supplierLookUpEdit.Properties.PopupWidth = 400;
            this.supplierLookUpEdit.Properties.ValueMember = "SupplierID";
            this.supplierLookUpEdit.Size = new System.Drawing.Size(581, 20);
            this.supplierLookUpEdit.StyleController = this.layoutControl1;
            this.supplierLookUpEdit.TabIndex = 8;
            // 
            // productNameTextEdit
            // 
            this.productNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsCategories1, "Categories.CategoriesProducts.ProductName", true));
            this.productNameTextEdit.Location = new System.Drawing.Point(113, 23);
            this.productNameTextEdit.Name = "productNameTextEdit";
            this.productNameTextEdit.Size = new System.Drawing.Size(581, 20);
            this.productNameTextEdit.StyleController = this.layoutControl1;
            this.productNameTextEdit.TabIndex = 1;
            this.productNameTextEdit.Tag = "Product Name";
            this.productNameTextEdit.ToolTip = "This field can\'t be empty";
            this.productNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.productNameTextEdit_Validating);
            // 
            // dataNavigator2
            // 
            this.dataNavigator2.Buttons.NextPage.Visible = false;
            this.dataNavigator2.Buttons.PrevPage.Visible = false;
            this.dataNavigator2.DataMember = "Categories.CategoriesProducts";
            this.dataNavigator2.DataSource = this.dsCategories1;
            this.dataNavigator2.Location = new System.Drawing.Point(15, 270);
            this.dataNavigator2.Name = "dataNavigator2";
            this.dataNavigator2.Size = new System.Drawing.Size(226, 19);
            this.dataNavigator2.StyleController = this.layoutControl1;
            this.dataNavigator2.TabIndex = 4;
            this.dataNavigator2.Text = "dataNavigator2";
            this.dataNavigator2.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator2.TextStringFormat = "Product {0} of {1}";
            this.dataNavigator2.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator2_ButtonClick_1);
            this.dataNavigator2.PositionChanged += new System.EventHandler(this.dataNavigator3_PositionChanged);
            // 
            // orderIdTextEdit
            // 
            this.orderIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.OrderID", true));
            this.orderIdTextEdit.EditValue = "textEdit3";
            this.orderIdTextEdit.Location = new System.Drawing.Point(113, 325);
            this.orderIdTextEdit.Name = "orderIdTextEdit";
            this.orderIdTextEdit.Size = new System.Drawing.Size(158, 20);
            this.orderIdTextEdit.StyleController = this.layoutControl1;
            this.orderIdTextEdit.TabIndex = 2;
            // 
            // quantitySpinEdit
            // 
            this.quantitySpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.Quantity", true));
            this.quantitySpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.quantitySpinEdit.Location = new System.Drawing.Point(113, 386);
            this.quantitySpinEdit.Name = "quantitySpinEdit";
            this.quantitySpinEdit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.quantitySpinEdit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.quantitySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.quantitySpinEdit.Properties.IsFloatValue = false;
            this.quantitySpinEdit.Properties.Mask.EditMask = "N00";
            this.quantitySpinEdit.Properties.ValidateOnEnterKey = true;
            this.quantitySpinEdit.Size = new System.Drawing.Size(581, 20);
            this.quantitySpinEdit.StyleController = this.layoutControl1;
            this.quantitySpinEdit.TabIndex = 12;
            this.quantitySpinEdit.Tag = "Quantity";
            this.quantitySpinEdit.ToolTip = "This field value can\'t be less than 0";
            this.quantitySpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.quantitySpinEdit_Validating);
            this.quantitySpinEdit.Validated += new System.EventHandler(this.Detail_Validated);
            // 
            // dataNavigator3
            // 
            this.dataNavigator3.Buttons.NextPage.Visible = false;
            this.dataNavigator3.Buttons.PrevPage.Visible = false;
            this.dataNavigator3.DataMember = "Categories.CategoriesProducts.ProductsOrder_x0020_Details";
            this.dataNavigator3.DataSource = this.dsCategories1;
            this.dataNavigator3.Location = new System.Drawing.Point(15, 479);
            this.dataNavigator3.Name = "dataNavigator3";
            this.dataNavigator3.Size = new System.Drawing.Size(223, 19);
            this.dataNavigator3.StyleController = this.layoutControl1;
            this.dataNavigator3.TabIndex = 7;
            this.dataNavigator3.Text = "dataNavigator3";
            this.dataNavigator3.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator3.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator2_ButtonClick_1);
            this.dataNavigator3.PositionChanged += new System.EventHandler(this.dataNavigator3_PositionChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Group";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -11);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(708, 523);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Group";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.CaptionImageIndex = 6;
            this.layoutControlGroup3.CustomizationFormText = "Products";
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem10});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(696, 302);
            this.layoutControlGroup3.Text = "Products";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.productNameTextEdit;
            this.layoutControlItem3.CustomizationFormText = "*Product Name: ";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem3.Text = "*Product Name: ";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.supplierLookUpEdit;
            this.layoutControlItem9.CustomizationFormText = "Supplier:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem9.Text = "Supplier:";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.productsUnitPriceCalcEdit;
            this.layoutControlItem11.CustomizationFormText = "*Unit Price:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 93);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem11.Text = "*Unit Price:";
            this.layoutControlItem11.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.unitsInStockSpinEdit;
            this.layoutControlItem13.CustomizationFormText = "*Units In Stock:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 124);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem13.Text = "*Units In Stock:";
            this.layoutControlItem13.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.reorderLevelSpinEdit;
            this.layoutControlItem12.CustomizationFormText = "*Reorder Level:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem12.Text = "*Reorder Level:";
            this.layoutControlItem12.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.unitsOnOrderSpinEdit;
            this.layoutControlItem14.CustomizationFormText = "*Units On Order:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 186);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem14.Text = "*Units On Order:";
            this.layoutControlItem14.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.checkEdit1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 217);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(690, 30);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dataNavigator2;
            this.layoutControlItem4.CustomizationFormText = "DataNavigator";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 247);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(690, 30);
            this.layoutControlItem4.Text = "DataNavigator";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.quantityPerUnitTextEdit;
            this.layoutControlItem10.CustomizationFormText = "Quantity Per Unit:  ";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem10.Text = "Quantity Per Unit:  ";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CaptionImageIndex = 7;
            this.layoutControlGroup4.CustomizationFormText = "Order Details";
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem18,
            this.layoutControlItem1,
            this.layoutControlItem16,
            this.layoutControlItem2,
            this.layoutControlItem6});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 302);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(696, 209);
            this.layoutControlGroup4.Text = "Order Details";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.orderIdTextEdit;
            this.layoutControlItem5.CustomizationFormText = "Order ID:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(158, 30);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(158, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(690, 30);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Order ID:";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.discountSpinEdit;
            this.layoutControlItem18.CustomizationFormText = "*Discount:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem18.Text = "*Discount:";
            this.layoutControlItem18.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.quantitySpinEdit;
            this.layoutControlItem1.CustomizationFormText = "*Quantity:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 61);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem1.Text = "*Quantity:";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.orderUnitPriceCalcEdit;
            this.layoutControlItem16.CustomizationFormText = "*Unit Price:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 92);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem16.Text = "*Unit Price:";
            this.layoutControlItem16.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(93, 20);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.subTotalTextEdit;
            this.layoutControlItem2.CustomizationFormText = "Sub Total:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 123);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(690, 31);
            this.layoutControlItem2.Text = "Sub Total:";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(93, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dataNavigator3;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 154);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(690, 30);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this.layoutControl1;
            this.dxErrorProvider1.DataMember = "";
            // 
            // Validating
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "Validating";
            this.Size = new System.Drawing.Size(725, 512);
            ((System.ComponentModel.ISupportInitialize)(this.dsCategories1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.subTotalTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderUnitPriceCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitsOnOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitsInStockSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reorderLevelSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsUnitPriceCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityPerUnitTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.supplierLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantitySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.DataNavigator dataNavigator2;
        private DevExpress.XtraLayout.Demos.dsCategories dsCategories1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.TextEdit subTotalTextEdit;
        private DevExpress.XtraEditors.SpinEdit discountSpinEdit;
        private DevExpress.XtraEditors.CalcEdit orderUnitPriceCalcEdit;
        private DevExpress.XtraEditors.SpinEdit unitsOnOrderSpinEdit;
        private DevExpress.XtraEditors.SpinEdit unitsInStockSpinEdit;
        private DevExpress.XtraEditors.SpinEdit reorderLevelSpinEdit;
        private DevExpress.XtraEditors.CalcEdit productsUnitPriceCalcEdit;
        private DevExpress.XtraEditors.LookUpEdit supplierLookUpEdit;
        private DevExpress.XtraEditors.TextEdit productNameTextEdit;
        private DevExpress.XtraEditors.TextEdit orderIdTextEdit;
        private DevExpress.XtraEditors.SpinEdit quantitySpinEdit;
        private DevExpress.XtraEditors.TextEdit quantityPerUnitTextEdit;
        private System.ComponentModel.IContainer components = null;

    }
}
