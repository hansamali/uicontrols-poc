namespace DevExpress.XtraLayout.Demos {
    partial class MasterDetail {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MasterDetail));
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.dsCategories1 = new DevExpress.XtraLayout.Demos.dsCategories();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.spinEdit5 = new DevExpress.XtraEditors.SpinEdit();
            this.calcEdit2 = new DevExpress.XtraEditors.CalcEdit();
            this.spinEdit3 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator2 = new DevExpress.XtraEditors.DataNavigator();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator3 = new DevExpress.XtraEditors.DataNavigator();
            this.spinEdit4 = new DevExpress.XtraEditors.SpinEdit();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsCategories1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            this.SuspendLayout();
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsCategories1, "Categories.CategoryName", true));
            this.textEdit1.Location = new System.Drawing.Point(116, 44);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(162, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 0;
            // 
            // dsCategories1
            // 
            this.dsCategories1.DataSetName = "dsCategories";
            this.dsCategories1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsCategories1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.spinEdit5);
            this.layoutControl1.Controls.Add(this.calcEdit2);
            this.layoutControl1.Controls.Add(this.spinEdit3);
            this.layoutControl1.Controls.Add(this.spinEdit2);
            this.layoutControl1.Controls.Add(this.spinEdit1);
            this.layoutControl1.Controls.Add(this.calcEdit1);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.lookUpEdit1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Controls.Add(this.pictureEdit1);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Controls.Add(this.dataNavigator1);
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.dataNavigator2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.dataNavigator3);
            this.layoutControl1.Controls.Add(this.spinEdit4);
            this.layoutControl1.Controls.Add(this.checkEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Images = this.imageCollection1;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(773, 285, 450, 350);
            this.layoutControl1.OptionsView.UseParentAutoScaleFactor = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(480, 512);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit5
            // 
            this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.SubTotal", true));
            this.textEdit5.EditValue = "textEdit5";
            this.textEdit5.Location = new System.Drawing.Point(416, 455);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.textEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit5.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit5.Properties.Mask.EditMask = "c";
            this.textEdit5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit5.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit5.Properties.ReadOnly = true;
            this.textEdit5.Size = new System.Drawing.Size(50, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 17;
            // 
            // spinEdit5
            // 
            this.spinEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.Discount", true));
            this.spinEdit5.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit5.Location = new System.Drawing.Point(266, 431);
            this.spinEdit5.Name = "spinEdit5";
            this.spinEdit5.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEdit5.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEdit5.Properties.Mask.EditMask = "p";
            this.spinEdit5.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEdit5.Properties.ValidateOnEnterKey = true;
            this.spinEdit5.Size = new System.Drawing.Size(50, 20);
            this.spinEdit5.StyleController = this.layoutControl1;
            this.spinEdit5.TabIndex = 16;
            this.spinEdit5.Validated += new System.EventHandler(this.Detail_Validated);
            // 
            // calcEdit2
            // 
            this.calcEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.UnitPrice", true));
            this.calcEdit2.EditValue = ((object)(resources.GetObject("calcEdit2.EditValue")));
            this.calcEdit2.Location = new System.Drawing.Point(116, 455);
            this.calcEdit2.Name = "calcEdit2";
            this.calcEdit2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.calcEdit2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.calcEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit2.Properties.Mask.EditMask = "c";
            this.calcEdit2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEdit2.Properties.ValidateOnEnterKey = true;
            this.calcEdit2.Size = new System.Drawing.Size(50, 20);
            this.calcEdit2.StyleController = this.layoutControl1;
            this.calcEdit2.TabIndex = 15;
            this.calcEdit2.Validated += new System.EventHandler(this.Detail_Validated);
            // 
            // spinEdit3
            // 
            this.spinEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.UnitsOnOrder", true));
            this.spinEdit3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit3.Location = new System.Drawing.Point(266, 359);
            this.spinEdit3.Name = "spinEdit3";
            this.spinEdit3.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEdit3.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit3.Properties.IsFloatValue = false;
            this.spinEdit3.Properties.Mask.EditMask = "N00";
            this.spinEdit3.Size = new System.Drawing.Size(62, 20);
            this.spinEdit3.StyleController = this.layoutControl1;
            this.spinEdit3.TabIndex = 13;
            // 
            // spinEdit2
            // 
            this.spinEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.UnitsInStock", true));
            this.spinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(116, 359);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEdit2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit2.Properties.IsFloatValue = false;
            this.spinEdit2.Properties.Mask.EditMask = "N00";
            this.spinEdit2.Size = new System.Drawing.Size(50, 20);
            this.spinEdit2.StyleController = this.layoutControl1;
            this.spinEdit2.TabIndex = 12;
            // 
            // spinEdit1
            // 
            this.spinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ReorderLevel", true));
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(266, 335);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Size = new System.Drawing.Size(62, 20);
            this.spinEdit1.StyleController = this.layoutControl1;
            this.spinEdit1.TabIndex = 11;
            // 
            // calcEdit1
            // 
            this.calcEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.UnitPrice", true));
            this.calcEdit1.EditValue = ((object)(resources.GetObject("calcEdit1.EditValue")));
            this.calcEdit1.Location = new System.Drawing.Point(116, 335);
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.calcEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Properties.Mask.EditMask = "c";
            this.calcEdit1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEdit1.Size = new System.Drawing.Size(50, 20);
            this.calcEdit1.StyleController = this.layoutControl1;
            this.calcEdit1.TabIndex = 10;
            // 
            // textEdit4
            // 
            this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsCategories1, "Categories.CategoriesProducts.QuantityPerUnit", true));
            this.textEdit4.Location = new System.Drawing.Point(266, 287);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(62, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 9;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.SupplierID", true));
            this.lookUpEdit1.EditValue = ((object)(resources.GetObject("lookUpEdit1.EditValue")));
            this.lookUpEdit1.Location = new System.Drawing.Point(116, 311);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyName", "Company Name", 78, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ContactName", "Contact Name", 71, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Country", "Country", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit1.Properties.DataSource = this.dsCategories1.Suppliers;
            this.lookUpEdit1.Properties.DisplayMember = "CompanyName";
            this.lookUpEdit1.Properties.PopupWidth = 400;
            this.lookUpEdit1.Properties.ValueMember = "SupplierID";
            this.lookUpEdit1.Size = new System.Drawing.Size(212, 20);
            this.lookUpEdit1.StyleController = this.layoutControl1;
            this.lookUpEdit1.TabIndex = 8;
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsCategories1, "Categories.Description", true));
            this.memoEdit1.Location = new System.Drawing.Point(20, 84);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(258, 151);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(282, 44);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(184, 118);
            this.pictureEdit1.StyleController = this.layoutControl1;
            this.pictureEdit1.TabIndex = 6;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataMember = "Categories";
            this.dataNavigator1.DataSource = this.dsCategories1;
            this.dataNavigator1.Location = new System.Drawing.Point(282, 216);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(184, 19);
            this.dataNavigator1.StyleController = this.layoutControl1;
            this.dataNavigator1.TabIndex = 3;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.TextStringFormat = "Category {0} of {1}";
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsCategories1, "Categories.CategoriesProducts.ProductName", true));
            this.textEdit2.Location = new System.Drawing.Point(116, 287);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(50, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 1;
            // 
            // dataNavigator2
            // 
            this.dataNavigator2.Buttons.Append.Visible = false;
            this.dataNavigator2.Buttons.CancelEdit.Visible = false;
            this.dataNavigator2.Buttons.EndEdit.Visible = false;
            this.dataNavigator2.Buttons.NextPage.Visible = false;
            this.dataNavigator2.Buttons.PrevPage.Visible = false;
            this.dataNavigator2.Buttons.Remove.Visible = false;
            this.dataNavigator2.DataMember = "Categories.CategoriesProducts";
            this.dataNavigator2.DataSource = this.dsCategories1;
            this.dataNavigator2.Location = new System.Drawing.Point(332, 287);
            this.dataNavigator2.Name = "dataNavigator2";
            this.dataNavigator2.Size = new System.Drawing.Size(134, 19);
            this.dataNavigator2.StyleController = this.layoutControl1;
            this.dataNavigator2.TabIndex = 4;
            this.dataNavigator2.Text = "dataNavigator2";
            this.dataNavigator2.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator2.TextStringFormat = "Product {0} of {1}";
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.OrderID", true));
            this.textEdit3.EditValue = "textEdit3";
            this.textEdit3.Location = new System.Drawing.Point(116, 431);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(50, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 2;
            // 
            // dataNavigator3
            // 
            this.dataNavigator3.Buttons.Append.Visible = false;
            this.dataNavigator3.Buttons.CancelEdit.Visible = false;
            this.dataNavigator3.Buttons.EndEdit.Visible = false;
            this.dataNavigator3.Buttons.NextPage.Visible = false;
            this.dataNavigator3.Buttons.PrevPage.Visible = false;
            this.dataNavigator3.Buttons.Remove.Visible = false;
            this.dataNavigator3.DataMember = "Categories.CategoriesProducts.ProductsOrder_x0020_Details";
            this.dataNavigator3.DataSource = this.dsCategories1;
            this.dataNavigator3.Location = new System.Drawing.Point(320, 431);
            this.dataNavigator3.Name = "dataNavigator3";
            this.dataNavigator3.Size = new System.Drawing.Size(146, 19);
            this.dataNavigator3.StyleController = this.layoutControl1;
            this.dataNavigator3.TabIndex = 5;
            this.dataNavigator3.Text = "dataNavigator3";
            this.dataNavigator3.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spinEdit4
            // 
            this.spinEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.ProductsOrder_x0020_Details.Quantity", true));
            this.spinEdit4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit4.Location = new System.Drawing.Point(266, 455);
            this.spinEdit4.Name = "spinEdit4";
            this.spinEdit4.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEdit4.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit4.Properties.IsFloatValue = false;
            this.spinEdit4.Properties.Mask.EditMask = "N00";
            this.spinEdit4.Properties.ValidateOnEnterKey = true;
            this.spinEdit4.Size = new System.Drawing.Size(50, 20);
            this.spinEdit4.StyleController = this.layoutControl1;
            this.spinEdit4.TabIndex = 12;
            this.spinEdit4.Validated += new System.EventHandler(this.Detail_Validated);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Group";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(486, 495);
            this.layoutControlGroup1.Text = "Group";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CaptionImageIndex = 5;
            this.layoutControlGroup2.CustomizationFormText = "Categories";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem1,
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(474, 243);
            this.layoutControlGroup2.Text = "Categories";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit1;
            this.layoutControlItem1.CustomizationFormText = "&Category Name:   ";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem1.Text = "&Category Name:   ";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.pictureEdit1;
            this.layoutControlItem7.CustomizationFormText = "Picture";
            this.layoutControlItem7.Location = new System.Drawing.Point(262, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(188, 122);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(188, 122);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(188, 122);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Picture";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.memoEdit1;
            this.layoutControlItem8.CustomizationFormText = "&Description:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(262, 171);
            this.layoutControlItem8.Text = "&Description:";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(93, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(262, 122);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(188, 50);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dataNavigator1;
            this.layoutControlItem2.CustomizationFormText = "DataNavigator";
            this.layoutControlItem2.Location = new System.Drawing.Point(262, 172);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsPrint.AllowPrint = false;
            this.layoutControlItem2.Size = new System.Drawing.Size(188, 23);
            this.layoutControlItem2.Text = "DataNavigator";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.CaptionImageIndex = 6;
            this.layoutControlGroup3.CustomizationFormText = "Products";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem10});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 243);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(474, 144);
            this.layoutControlGroup3.Text = "Products";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit2;
            this.layoutControlItem3.CustomizationFormText = "&Product Name: ";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem3.Text = "&Product Name: ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dataNavigator2;
            this.layoutControlItem4.CustomizationFormText = "DataNavigator";
            this.layoutControlItem4.Location = new System.Drawing.Point(312, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsPrint.AllowPrint = false;
            this.layoutControlItem4.Size = new System.Drawing.Size(138, 24);
            this.layoutControlItem4.Text = "DataNavigator";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.lookUpEdit1;
            this.layoutControlItem9.CustomizationFormText = "Supplier:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem9.Text = "Supplier:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.calcEdit1;
            this.layoutControlItem11.CustomizationFormText = "Unit Price:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem11.Text = "Unit Price:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.spinEdit1;
            this.layoutControlItem12.CustomizationFormText = "Reorder Level:";
            this.layoutControlItem12.Location = new System.Drawing.Point(150, 48);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(162, 24);
            this.layoutControlItem12.Text = "Reorder Level:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.spinEdit2;
            this.layoutControlItem13.CustomizationFormText = "Units In Stock:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem13.Text = "Units In Stock:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.spinEdit3;
            this.layoutControlItem14.CustomizationFormText = "Units On Order:";
            this.layoutControlItem14.Location = new System.Drawing.Point(150, 72);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(162, 24);
            this.layoutControlItem14.Text = "Units On Order:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.checkEdit1;
            this.layoutControlItem15.CustomizationFormText = "Discontinued";
            this.layoutControlItem15.Location = new System.Drawing.Point(312, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(138, 72);
            this.layoutControlItem15.Text = "Discontinued";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dsCategories1, "Categories.CategoriesProducts.Discontinued", true));
            this.checkEdit1.EditValue = ((object)(resources.GetObject("checkEdit1.EditValue")));
            this.checkEdit1.Location = new System.Drawing.Point(332, 311);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Discontinued";
            this.checkEdit1.Size = new System.Drawing.Size(134, 19);
            this.checkEdit1.StyleController = this.layoutControl1;
            this.checkEdit1.TabIndex = 14;
            this.checkEdit1.TabStop = false;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEdit4;
            this.layoutControlItem10.CustomizationFormText = "Quantity Per Unit:  ";
            this.layoutControlItem10.Location = new System.Drawing.Point(150, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(162, 24);
            this.layoutControlItem10.Text = "Quantity Per Unit:  ";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CaptionImageIndex = 7;
            this.layoutControlGroup4.CustomizationFormText = "Order Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem16,
            this.layoutControlItem19,
            this.layoutControlItem17,
            this.layoutControlItem18});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 387);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(474, 96);
            this.layoutControlGroup4.Text = "Order Details";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit3;
            this.layoutControlItem5.CustomizationFormText = "Order ID:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem5.Text = "Order ID:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dataNavigator3;
            this.layoutControlItem6.CustomizationFormText = "DataNavigator";
            this.layoutControlItem6.Location = new System.Drawing.Point(300, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsPrint.AllowPrint = false;
            this.layoutControlItem6.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem6.Text = "DataNavigator";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.calcEdit2;
            this.layoutControlItem16.CustomizationFormText = "Unit Price:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem16.Text = "Unit Price:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit5;
            this.layoutControlItem19.CustomizationFormText = "Sub Total:";
            this.layoutControlItem19.Location = new System.Drawing.Point(300, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem19.Text = "Sub Total:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.spinEdit4;
            this.layoutControlItem17.CustomizationFormText = "&Quantity:";
            this.layoutControlItem17.Location = new System.Drawing.Point(150, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem17.Text = "&Quantity:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.spinEdit5;
            this.layoutControlItem18.CustomizationFormText = "Discount:";
            this.layoutControlItem18.Location = new System.Drawing.Point(150, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem18.Text = "Discount:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(93, 13);
            // 
            // MasterDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "MasterDetail";
            this.Size = new System.Drawing.Size(726, 512);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsCategories1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator3;
        private DevExpress.XtraLayout.Demos.dsCategories dsCategories1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SpinEdit spinEdit3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.CalcEdit calcEdit2;
        private DevExpress.XtraEditors.SpinEdit spinEdit4;
        private DevExpress.XtraEditors.SpinEdit spinEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.ComponentModel.IContainer components = null;

    }
}
