namespace DevExpress.XtraGrid.Demos {
    partial class AdvancedGridEditing  {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvancedGridEditing));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.detailShowHideBtn = new DevExpress.XtraEditors.SimpleButton();
            this.cbMasterTablePosition = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.masterGrid = new DevExpress.XtraGrid.GridControl();
            this.employesLayoutView = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.colLastName = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colLastName = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colFirstName = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colFirstName = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colPhoto = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutViewField_colPhoto = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colFullName = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.detailGrid = new DevExpress.XtraGrid.GridControl();
            this.viewOrderGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiredDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShippedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipVia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFreight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipPostalCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipCountry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.editOrderlayoutView = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.colOrderID1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colOrderID1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colCustomerID1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colCustomerID1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colEmployeeID = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colOrderDate1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colOrderDate1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colRequiredDate1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colRequiredDate1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colShippedDate1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colShippedDate1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colShipVia1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colShipVia1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colFreight1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colFreight1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colShipName1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colShipName1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colShipAddress1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colShipAddress1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colShipCity1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colShipCity1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colShipRegion1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colShipRegion1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colShipPostalCode1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colShipPostalCode1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colShipCountry1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colShipCountry1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.LayoutItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.LayoutItem3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.LayoutItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutItem9 = new DevExpress.XtraLayout.SimpleSeparator();
            this.LayoutItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutItem14 = new DevExpress.XtraLayout.SimpleSeparator();
            this.LayoutItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.allowEditOnDoubleClick = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.detailTableItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.masterTableItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbMasterTablePosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employesLayoutView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewOrderGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editOrderlayoutView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colOrderID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCustomerID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colOrderDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colRequiredDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShippedDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipVia1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colFreight1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipCity1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipRegion1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipPostalCode1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipCountry1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowEditOnDoubleClick.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailTableItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterTableItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.OptionsView.UseParentAutoScaleFactor = true;

            this.layoutControl1.Controls.Add(this.detailShowHideBtn);
            this.layoutControl1.Controls.Add(this.cbMasterTablePosition);
            this.layoutControl1.Controls.Add(this.masterGrid);
            this.layoutControl1.Controls.Add(this.detailGrid);
            this.layoutControl1.Controls.Add(this.allowEditOnDoubleClick);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            // 
            // detailShowHideBtn
            // 
            resources.ApplyResources(this.detailShowHideBtn, "detailShowHideBtn");
            this.detailShowHideBtn.Name = "detailShowHideBtn";
            this.detailShowHideBtn.StyleController = this.layoutControl1;
            // 
            // cbMasterTablePosition
            // 
            resources.ApplyResources(this.cbMasterTablePosition, "cbMasterTablePosition");
            this.cbMasterTablePosition.Name = "cbMasterTablePosition";
            this.cbMasterTablePosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cbMasterTablePosition.Properties.Buttons"))))});
            this.cbMasterTablePosition.StyleController = this.layoutControl1;
            // 
            // masterGrid
            // 
            this.masterGrid.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.masterGrid, "masterGrid");
            this.masterGrid.MainView = this.employesLayoutView;
            this.masterGrid.Name = "masterGrid";
            this.masterGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemPictureEdit2});
            this.masterGrid.ShowOnlyPredefinedDetails = true;
            this.masterGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.employesLayoutView});
            // 
            // employesLayoutView
            // 
            this.employesLayoutView.Appearance.CardCaption.Font = ((System.Drawing.Font)(resources.GetObject("employesLayoutView.Appearance.CardCaption.Font")));
            this.employesLayoutView.Appearance.CardCaption.Options.UseFont = true;
            this.employesLayoutView.CardMinSize = new System.Drawing.Size(98, 127);
            this.employesLayoutView.CardVertInterval = 1;
            this.employesLayoutView.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.colLastName,
            this.colFirstName,
            this.colPhoto,
            this.colFullName});
            this.employesLayoutView.GridControl = this.masterGrid;
            this.employesLayoutView.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colFirstName,
            this.layoutViewField_colLastName});
            this.employesLayoutView.Name = "employesLayoutView";
            this.employesLayoutView.OptionsBehavior.Editable = false;
            this.employesLayoutView.OptionsCustomization.AllowFilter = false;
            this.employesLayoutView.OptionsCustomization.AllowSort = false;
            this.employesLayoutView.OptionsCustomization.ShowGroupLayout = false;
            this.employesLayoutView.OptionsMultiRecordMode.StretchCardToViewHeight = true;
            this.employesLayoutView.OptionsMultiRecordMode.StretchCardToViewWidth = true;
            this.employesLayoutView.OptionsView.ShowCardBorderIfCaptionHidden = false;
            this.employesLayoutView.OptionsView.ShowCardCaption = false;
            this.employesLayoutView.OptionsView.ShowCardExpandButton = false;
            this.employesLayoutView.OptionsView.ShowCardLines = false;
            this.employesLayoutView.OptionsView.ShowHeaderPanel = false;
            this.employesLayoutView.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
            this.employesLayoutView.TemplateCard = this.layoutViewCard1;
            // 
            // colLastName
            // 
            resources.ApplyResources(this.colLastName, "colLastName");
            this.colLastName.FieldName = "LastName";
            this.colLastName.LayoutViewField = this.layoutViewField_colLastName;
            this.colLastName.Name = "colLastName";
            // 
            // layoutViewField_colLastName
            // 
            this.layoutViewField_colLastName.EditorPreferredWidth = 20;
            this.layoutViewField_colLastName.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colLastName.Name = "layoutViewField_colLastName";
            this.layoutViewField_colLastName.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colLastName.Size = new System.Drawing.Size(98, 154);
            this.layoutViewField_colLastName.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colLastName.TextToControlDistance = 0;
            this.layoutViewField_colLastName.TextVisible = false;
            // 
            // colFirstName
            // 
            resources.ApplyResources(this.colFirstName, "colFirstName");
            this.colFirstName.FieldName = "FirstName";
            this.colFirstName.LayoutViewField = this.layoutViewField_colFirstName;
            this.colFirstName.Name = "colFirstName";
            // 
            // layoutViewField_colFirstName
            // 
            this.layoutViewField_colFirstName.EditorPreferredWidth = 20;
            this.layoutViewField_colFirstName.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colFirstName.Name = "layoutViewField_colFirstName";
            this.layoutViewField_colFirstName.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colFirstName.Size = new System.Drawing.Size(98, 154);
            this.layoutViewField_colFirstName.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colFirstName.TextToControlDistance = 0;
            this.layoutViewField_colFirstName.TextVisible = false;
            // 
            // colPhoto
            // 
            resources.ApplyResources(this.colPhoto, "colPhoto");
            this.colPhoto.ColumnEdit = this.repositoryItemPictureEdit2;
            this.colPhoto.FieldName = "Photo";
            this.colPhoto.LayoutViewField = this.layoutViewField_colPhoto;
            this.colPhoto.Name = "colPhoto";
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // layoutViewField_colPhoto
            // 
            this.layoutViewField_colPhoto.EditorPreferredWidth = 95;
            this.layoutViewField_colPhoto.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colPhoto.MaxSize = new System.Drawing.Size(98, 108);
            this.layoutViewField_colPhoto.MinSize = new System.Drawing.Size(98, 108);
            this.layoutViewField_colPhoto.Name = "layoutViewField_colPhoto";
            this.layoutViewField_colPhoto.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colPhoto.Size = new System.Drawing.Size(98, 108);
            this.layoutViewField_colPhoto.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colPhoto.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colPhoto.TextToControlDistance = 0;
            this.layoutViewField_colPhoto.TextVisible = false;
            // 
            // colFullName
            // 
            resources.ApplyResources(this.colFullName, "colFullName");
            this.colFullName.FieldName = "colFullName";
            this.colFullName.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this.colFullName.Name = "colFullName";
            this.colFullName.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 95;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(0, 108);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(98, 19);
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn1.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn1.TextVisible = false;
            // 
            // layoutViewCard1
            // 
            resources.ApplyResources(this.layoutViewCard1, "layoutViewCard1");
            this.layoutViewCard1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.GroupBordersVisible = false;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colPhoto,
            this.layoutViewField_layoutViewColumn1});
            this.layoutViewCard1.Name = "layoutViewCard1";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // detailGrid
            // 
            resources.ApplyResources(this.detailGrid, "detailGrid");
            this.detailGrid.MainView = this.viewOrderGridView;
            this.detailGrid.Name = "detailGrid";
            this.detailGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.repositoryItemImageComboBox1,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4,
            this.repositoryItemSpinEdit1});
            this.detailGrid.ShowOnlyPredefinedDetails = true;
            this.detailGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewOrderGridView,
            this.editOrderlayoutView});
            // 
            // viewOrderGridView
            // 
            this.viewOrderGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOrderID,
            this.colCustomerID,
            this.colEmployeeID1,
            this.colOrderDate,
            this.colRequiredDate,
            this.colShippedDate,
            this.colShipVia,
            this.colFreight,
            this.colShipName,
            this.colShipAddress,
            this.colShipCity,
            this.colShipRegion,
            this.colShipPostalCode,
            this.colShipCountry});
            this.viewOrderGridView.CustomizationFormBounds = new System.Drawing.Rectangle(838, 428, 208, 189);
            this.viewOrderGridView.GridControl = this.detailGrid;
            this.viewOrderGridView.Name = "viewOrderGridView";
            this.viewOrderGridView.OptionsBehavior.Editable = false;
            this.viewOrderGridView.OptionsView.ShowGroupPanel = false;
            this.viewOrderGridView.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // colOrderID
            // 
            resources.ApplyResources(this.colOrderID, "colOrderID");
            this.colOrderID.FieldName = "OrderID";
            this.colOrderID.Name = "colOrderID";
            // 
            // colCustomerID
            // 
            resources.ApplyResources(this.colCustomerID, "colCustomerID");
            this.colCustomerID.FieldName = "CustomerID";
            this.colCustomerID.Name = "colCustomerID";
            // 
            // colEmployeeID1
            // 
            resources.ApplyResources(this.colEmployeeID1, "colEmployeeID1");
            this.colEmployeeID1.FieldName = "EmployeeID";
            this.colEmployeeID1.Name = "colEmployeeID1";
            // 
            // colOrderDate
            // 
            resources.ApplyResources(this.colOrderDate, "colOrderDate");
            this.colOrderDate.FieldName = "OrderDate";
            this.colOrderDate.Name = "colOrderDate";
            // 
            // colRequiredDate
            // 
            resources.ApplyResources(this.colRequiredDate, "colRequiredDate");
            this.colRequiredDate.FieldName = "RequiredDate";
            this.colRequiredDate.Name = "colRequiredDate";
            // 
            // colShippedDate
            // 
            resources.ApplyResources(this.colShippedDate, "colShippedDate");
            this.colShippedDate.FieldName = "ShippedDate";
            this.colShippedDate.Name = "colShippedDate";
            // 
            // colShipVia
            // 
            resources.ApplyResources(this.colShipVia, "colShipVia");
            this.colShipVia.FieldName = "ShipVia";
            this.colShipVia.Name = "colShipVia";
            // 
            // colFreight
            // 
            resources.ApplyResources(this.colFreight, "colFreight");
            this.colFreight.DisplayFormat.FormatString = "$#,0.00";
            this.colFreight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFreight.FieldName = "Freight";
            this.colFreight.Name = "colFreight";
            // 
            // colShipName
            // 
            resources.ApplyResources(this.colShipName, "colShipName");
            this.colShipName.FieldName = "ShipName";
            this.colShipName.Name = "colShipName";
            // 
            // colShipAddress
            // 
            resources.ApplyResources(this.colShipAddress, "colShipAddress");
            this.colShipAddress.FieldName = "ShipAddress";
            this.colShipAddress.Name = "colShipAddress";
            // 
            // colShipCity
            // 
            resources.ApplyResources(this.colShipCity, "colShipCity");
            this.colShipCity.FieldName = "ShipCity";
            this.colShipCity.Name = "colShipCity";
            // 
            // colShipRegion
            // 
            resources.ApplyResources(this.colShipRegion, "colShipRegion");
            this.colShipRegion.FieldName = "ShipRegion";
            this.colShipRegion.Name = "colShipRegion";
            // 
            // colShipPostalCode
            // 
            resources.ApplyResources(this.colShipPostalCode, "colShipPostalCode");
            this.colShipPostalCode.FieldName = "ShipPostalCode";
            this.colShipPostalCode.Name = "colShipPostalCode";
            // 
            // colShipCountry
            // 
            resources.ApplyResources(this.colShipCountry, "colShipCountry");
            this.colShipCountry.FieldName = "ShipCountry";
            this.colShipCountry.Name = "colShipCountry";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            resources.ApplyResources(this.repositoryItemImageComboBox1, "repositoryItemImageComboBox1");
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemImageComboBox1.Buttons"))))});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(resources.GetString("repositoryItemImageComboBox1.Items"), ((object)(resources.GetObject("repositoryItemImageComboBox1.Items1"))), ((int)(resources.GetObject("repositoryItemImageComboBox1.Items2")))),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(resources.GetString("repositoryItemImageComboBox1.Items3"), ((object)(resources.GetObject("repositoryItemImageComboBox1.Items4"))), ((int)(resources.GetObject("repositoryItemImageComboBox1.Items5")))),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(resources.GetString("repositoryItemImageComboBox1.Items6"), ((object)(resources.GetObject("repositoryItemImageComboBox1.Items7"))), ((int)(resources.GetObject("repositoryItemImageComboBox1.Items8"))))});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // repositoryItemComboBox1
            // 
            resources.ApplyResources(this.repositoryItemComboBox1, "repositoryItemComboBox1");
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemComboBox1.Buttons"))))});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            resources.GetString("repositoryItemComboBox1.Items"),
            resources.GetString("repositoryItemComboBox1.Items1"),
            resources.GetString("repositoryItemComboBox1.Items2"),
            resources.GetString("repositoryItemComboBox1.Items3"),
            resources.GetString("repositoryItemComboBox1.Items4"),
            resources.GetString("repositoryItemComboBox1.Items5"),
            resources.GetString("repositoryItemComboBox1.Items6"),
            resources.GetString("repositoryItemComboBox1.Items7"),
            resources.GetString("repositoryItemComboBox1.Items8"),
            resources.GetString("repositoryItemComboBox1.Items9"),
            resources.GetString("repositoryItemComboBox1.Items10"),
            resources.GetString("repositoryItemComboBox1.Items11"),
            resources.GetString("repositoryItemComboBox1.Items12"),
            resources.GetString("repositoryItemComboBox1.Items13"),
            resources.GetString("repositoryItemComboBox1.Items14"),
            resources.GetString("repositoryItemComboBox1.Items15"),
            resources.GetString("repositoryItemComboBox1.Items16"),
            resources.GetString("repositoryItemComboBox1.Items17"),
            resources.GetString("repositoryItemComboBox1.Items18"),
            resources.GetString("repositoryItemComboBox1.Items19"),
            resources.GetString("repositoryItemComboBox1.Items20")});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            resources.ApplyResources(this.repositoryItemComboBox2, "repositoryItemComboBox2");
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemComboBox2.Buttons"))))});
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            resources.GetString("repositoryItemComboBox2.Items"),
            resources.GetString("repositoryItemComboBox2.Items1"),
            resources.GetString("repositoryItemComboBox2.Items2"),
            resources.GetString("repositoryItemComboBox2.Items3"),
            resources.GetString("repositoryItemComboBox2.Items4"),
            resources.GetString("repositoryItemComboBox2.Items5"),
            resources.GetString("repositoryItemComboBox2.Items6"),
            resources.GetString("repositoryItemComboBox2.Items7"),
            resources.GetString("repositoryItemComboBox2.Items8"),
            resources.GetString("repositoryItemComboBox2.Items9"),
            resources.GetString("repositoryItemComboBox2.Items10"),
            resources.GetString("repositoryItemComboBox2.Items11"),
            resources.GetString("repositoryItemComboBox2.Items12"),
            resources.GetString("repositoryItemComboBox2.Items13"),
            resources.GetString("repositoryItemComboBox2.Items14"),
            resources.GetString("repositoryItemComboBox2.Items15"),
            resources.GetString("repositoryItemComboBox2.Items16"),
            resources.GetString("repositoryItemComboBox2.Items17"),
            resources.GetString("repositoryItemComboBox2.Items18"),
            resources.GetString("repositoryItemComboBox2.Items19"),
            resources.GetString("repositoryItemComboBox2.Items20"),
            resources.GetString("repositoryItemComboBox2.Items21"),
            resources.GetString("repositoryItemComboBox2.Items22"),
            resources.GetString("repositoryItemComboBox2.Items23"),
            resources.GetString("repositoryItemComboBox2.Items24"),
            resources.GetString("repositoryItemComboBox2.Items25"),
            resources.GetString("repositoryItemComboBox2.Items26"),
            resources.GetString("repositoryItemComboBox2.Items27"),
            resources.GetString("repositoryItemComboBox2.Items28"),
            resources.GetString("repositoryItemComboBox2.Items29"),
            resources.GetString("repositoryItemComboBox2.Items30"),
            resources.GetString("repositoryItemComboBox2.Items31"),
            resources.GetString("repositoryItemComboBox2.Items32"),
            resources.GetString("repositoryItemComboBox2.Items33"),
            resources.GetString("repositoryItemComboBox2.Items34"),
            resources.GetString("repositoryItemComboBox2.Items35"),
            resources.GetString("repositoryItemComboBox2.Items36"),
            resources.GetString("repositoryItemComboBox2.Items37"),
            resources.GetString("repositoryItemComboBox2.Items38"),
            resources.GetString("repositoryItemComboBox2.Items39"),
            resources.GetString("repositoryItemComboBox2.Items40"),
            resources.GetString("repositoryItemComboBox2.Items41"),
            resources.GetString("repositoryItemComboBox2.Items42"),
            resources.GetString("repositoryItemComboBox2.Items43"),
            resources.GetString("repositoryItemComboBox2.Items44"),
            resources.GetString("repositoryItemComboBox2.Items45"),
            resources.GetString("repositoryItemComboBox2.Items46"),
            resources.GetString("repositoryItemComboBox2.Items47"),
            resources.GetString("repositoryItemComboBox2.Items48"),
            resources.GetString("repositoryItemComboBox2.Items49"),
            resources.GetString("repositoryItemComboBox2.Items50"),
            resources.GetString("repositoryItemComboBox2.Items51"),
            resources.GetString("repositoryItemComboBox2.Items52"),
            resources.GetString("repositoryItemComboBox2.Items53"),
            resources.GetString("repositoryItemComboBox2.Items54"),
            resources.GetString("repositoryItemComboBox2.Items55"),
            resources.GetString("repositoryItemComboBox2.Items56"),
            resources.GetString("repositoryItemComboBox2.Items57"),
            resources.GetString("repositoryItemComboBox2.Items58"),
            resources.GetString("repositoryItemComboBox2.Items59"),
            resources.GetString("repositoryItemComboBox2.Items60"),
            resources.GetString("repositoryItemComboBox2.Items61"),
            resources.GetString("repositoryItemComboBox2.Items62"),
            resources.GetString("repositoryItemComboBox2.Items63"),
            resources.GetString("repositoryItemComboBox2.Items64"),
            resources.GetString("repositoryItemComboBox2.Items65"),
            resources.GetString("repositoryItemComboBox2.Items66"),
            resources.GetString("repositoryItemComboBox2.Items67"),
            resources.GetString("repositoryItemComboBox2.Items68"),
            resources.GetString("repositoryItemComboBox2.Items69"),
            resources.GetString("repositoryItemComboBox2.Items70"),
            resources.GetString("repositoryItemComboBox2.Items71"),
            resources.GetString("repositoryItemComboBox2.Items72"),
            resources.GetString("repositoryItemComboBox2.Items73"),
            resources.GetString("repositoryItemComboBox2.Items74"),
            resources.GetString("repositoryItemComboBox2.Items75"),
            resources.GetString("repositoryItemComboBox2.Items76"),
            resources.GetString("repositoryItemComboBox2.Items77"),
            resources.GetString("repositoryItemComboBox2.Items78"),
            resources.GetString("repositoryItemComboBox2.Items79"),
            resources.GetString("repositoryItemComboBox2.Items80"),
            resources.GetString("repositoryItemComboBox2.Items81"),
            resources.GetString("repositoryItemComboBox2.Items82"),
            resources.GetString("repositoryItemComboBox2.Items83")});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemComboBox3
            // 
            resources.ApplyResources(this.repositoryItemComboBox3, "repositoryItemComboBox3");
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemComboBox3.Buttons"))))});
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            resources.GetString("repositoryItemComboBox3.Items"),
            resources.GetString("repositoryItemComboBox3.Items1"),
            resources.GetString("repositoryItemComboBox3.Items2"),
            resources.GetString("repositoryItemComboBox3.Items3"),
            resources.GetString("repositoryItemComboBox3.Items4"),
            resources.GetString("repositoryItemComboBox3.Items5"),
            resources.GetString("repositoryItemComboBox3.Items6"),
            resources.GetString("repositoryItemComboBox3.Items7"),
            resources.GetString("repositoryItemComboBox3.Items8"),
            resources.GetString("repositoryItemComboBox3.Items9"),
            resources.GetString("repositoryItemComboBox3.Items10"),
            resources.GetString("repositoryItemComboBox3.Items11"),
            resources.GetString("repositoryItemComboBox3.Items12"),
            resources.GetString("repositoryItemComboBox3.Items13"),
            resources.GetString("repositoryItemComboBox3.Items14"),
            resources.GetString("repositoryItemComboBox3.Items15"),
            resources.GetString("repositoryItemComboBox3.Items16"),
            resources.GetString("repositoryItemComboBox3.Items17"),
            resources.GetString("repositoryItemComboBox3.Items18")});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox4
            // 
            resources.ApplyResources(this.repositoryItemComboBox4, "repositoryItemComboBox4");
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemComboBox4.Buttons"))))});
            this.repositoryItemComboBox4.Items.AddRange(new object[] {
            resources.GetString("repositoryItemComboBox4.Items"),
            resources.GetString("repositoryItemComboBox4.Items1"),
            resources.GetString("repositoryItemComboBox4.Items2"),
            resources.GetString("repositoryItemComboBox4.Items3"),
            resources.GetString("repositoryItemComboBox4.Items4"),
            resources.GetString("repositoryItemComboBox4.Items5"),
            resources.GetString("repositoryItemComboBox4.Items6"),
            resources.GetString("repositoryItemComboBox4.Items7"),
            resources.GetString("repositoryItemComboBox4.Items8"),
            resources.GetString("repositoryItemComboBox4.Items9"),
            resources.GetString("repositoryItemComboBox4.Items10"),
            resources.GetString("repositoryItemComboBox4.Items11"),
            resources.GetString("repositoryItemComboBox4.Items12"),
            resources.GetString("repositoryItemComboBox4.Items13"),
            resources.GetString("repositoryItemComboBox4.Items14"),
            resources.GetString("repositoryItemComboBox4.Items15"),
            resources.GetString("repositoryItemComboBox4.Items16"),
            resources.GetString("repositoryItemComboBox4.Items17"),
            resources.GetString("repositoryItemComboBox4.Items18"),
            resources.GetString("repositoryItemComboBox4.Items19"),
            resources.GetString("repositoryItemComboBox4.Items20"),
            resources.GetString("repositoryItemComboBox4.Items21"),
            resources.GetString("repositoryItemComboBox4.Items22"),
            resources.GetString("repositoryItemComboBox4.Items23"),
            resources.GetString("repositoryItemComboBox4.Items24"),
            resources.GetString("repositoryItemComboBox4.Items25"),
            resources.GetString("repositoryItemComboBox4.Items26"),
            resources.GetString("repositoryItemComboBox4.Items27"),
            resources.GetString("repositoryItemComboBox4.Items28"),
            resources.GetString("repositoryItemComboBox4.Items29"),
            resources.GetString("repositoryItemComboBox4.Items30"),
            resources.GetString("repositoryItemComboBox4.Items31"),
            resources.GetString("repositoryItemComboBox4.Items32"),
            resources.GetString("repositoryItemComboBox4.Items33"),
            resources.GetString("repositoryItemComboBox4.Items34"),
            resources.GetString("repositoryItemComboBox4.Items35"),
            resources.GetString("repositoryItemComboBox4.Items36"),
            resources.GetString("repositoryItemComboBox4.Items37"),
            resources.GetString("repositoryItemComboBox4.Items38"),
            resources.GetString("repositoryItemComboBox4.Items39"),
            resources.GetString("repositoryItemComboBox4.Items40"),
            resources.GetString("repositoryItemComboBox4.Items41"),
            resources.GetString("repositoryItemComboBox4.Items42"),
            resources.GetString("repositoryItemComboBox4.Items43"),
            resources.GetString("repositoryItemComboBox4.Items44"),
            resources.GetString("repositoryItemComboBox4.Items45"),
            resources.GetString("repositoryItemComboBox4.Items46"),
            resources.GetString("repositoryItemComboBox4.Items47"),
            resources.GetString("repositoryItemComboBox4.Items48"),
            resources.GetString("repositoryItemComboBox4.Items49"),
            resources.GetString("repositoryItemComboBox4.Items50"),
            resources.GetString("repositoryItemComboBox4.Items51"),
            resources.GetString("repositoryItemComboBox4.Items52"),
            resources.GetString("repositoryItemComboBox4.Items53"),
            resources.GetString("repositoryItemComboBox4.Items54"),
            resources.GetString("repositoryItemComboBox4.Items55"),
            resources.GetString("repositoryItemComboBox4.Items56"),
            resources.GetString("repositoryItemComboBox4.Items57"),
            resources.GetString("repositoryItemComboBox4.Items58"),
            resources.GetString("repositoryItemComboBox4.Items59"),
            resources.GetString("repositoryItemComboBox4.Items60"),
            resources.GetString("repositoryItemComboBox4.Items61"),
            resources.GetString("repositoryItemComboBox4.Items62"),
            resources.GetString("repositoryItemComboBox4.Items63"),
            resources.GetString("repositoryItemComboBox4.Items64"),
            resources.GetString("repositoryItemComboBox4.Items65"),
            resources.GetString("repositoryItemComboBox4.Items66"),
            resources.GetString("repositoryItemComboBox4.Items67"),
            resources.GetString("repositoryItemComboBox4.Items68"),
            resources.GetString("repositoryItemComboBox4.Items69")});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemSpinEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            resources.ApplyResources(this.repositoryItemSpinEdit1, "repositoryItemSpinEdit1");
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.Mask.EditMask = resources.GetString("repositoryItemSpinEdit1.Mask.EditMask");
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat")));
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // editOrderlayoutView
            // 
            resources.ApplyResources(this.editOrderlayoutView, "editOrderlayoutView");
            this.editOrderlayoutView.CardMinSize = new System.Drawing.Size(400, 295);
            this.editOrderlayoutView.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.colOrderID1,
            this.colCustomerID1,
            this.colEmployeeID,
            this.colOrderDate1,
            this.colRequiredDate1,
            this.colShippedDate1,
            this.colShipVia1,
            this.colFreight1,
            this.colShipName1,
            this.colShipAddress1,
            this.colShipCity1,
            this.colShipRegion1,
            this.colShipPostalCode1,
            this.colShipCountry1});
            this.editOrderlayoutView.GridControl = this.detailGrid;
            this.editOrderlayoutView.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colOrderID1,
            this.layoutViewField_colCustomerID1,
            this.layoutViewField_colEmployeeID});
            this.editOrderlayoutView.Name = "editOrderlayoutView";
            this.editOrderlayoutView.OptionsBehavior.AllowExpandCollapse = false;
            this.editOrderlayoutView.OptionsCustomization.AllowFilter = false;
            this.editOrderlayoutView.OptionsCustomization.AllowSort = false;
            this.editOrderlayoutView.OptionsCustomization.ShowGroupLayout = false;
            this.editOrderlayoutView.OptionsSingleRecordMode.StretchCardToViewHeight = true;
            this.editOrderlayoutView.OptionsSingleRecordMode.StretchCardToViewWidth = true;
            this.editOrderlayoutView.OptionsView.ShowCardCaption = false;
            this.editOrderlayoutView.OptionsView.ShowCardExpandButton = false;
            this.editOrderlayoutView.OptionsView.ShowHeaderPanel = false;
            this.editOrderlayoutView.TemplateCard = this.layoutViewCard2;
            // 
            // colOrderID1
            // 
            resources.ApplyResources(this.colOrderID1, "colOrderID1");
            this.colOrderID1.FieldName = "OrderID";
            this.colOrderID1.LayoutViewField = this.layoutViewField_colOrderID1;
            this.colOrderID1.Name = "colOrderID1";
            // 
            // layoutViewField_colOrderID1
            // 
            this.layoutViewField_colOrderID1.EditorPreferredWidth = 25;
            this.layoutViewField_colOrderID1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colOrderID1.Name = "layoutViewField_colOrderID1";
            this.layoutViewField_colOrderID1.Size = new System.Drawing.Size(397, 295);
            this.layoutViewField_colOrderID1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colOrderID1.TextSize = new System.Drawing.Size(43, 20);
            this.layoutViewField_colOrderID1.TextToControlDistance = 5;
            // 
            // colCustomerID1
            // 
            resources.ApplyResources(this.colCustomerID1, "colCustomerID1");
            this.colCustomerID1.FieldName = "CustomerID";
            this.colCustomerID1.LayoutViewField = this.layoutViewField_colCustomerID1;
            this.colCustomerID1.Name = "colCustomerID1";
            // 
            // layoutViewField_colCustomerID1
            // 
            this.layoutViewField_colCustomerID1.EditorPreferredWidth = 25;
            this.layoutViewField_colCustomerID1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colCustomerID1.Name = "layoutViewField_colCustomerID1";
            this.layoutViewField_colCustomerID1.Size = new System.Drawing.Size(397, 295);
            this.layoutViewField_colCustomerID1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colCustomerID1.TextSize = new System.Drawing.Size(61, 20);
            this.layoutViewField_colCustomerID1.TextToControlDistance = 5;
            // 
            // colEmployeeID
            // 
            resources.ApplyResources(this.colEmployeeID, "colEmployeeID");
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.LayoutViewField = this.layoutViewField_colEmployeeID;
            this.colEmployeeID.Name = "colEmployeeID";
            // 
            // layoutViewField_colEmployeeID
            // 
            this.layoutViewField_colEmployeeID.EditorPreferredWidth = 25;
            this.layoutViewField_colEmployeeID.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colEmployeeID.Name = "layoutViewField_colEmployeeID";
            this.layoutViewField_colEmployeeID.Size = new System.Drawing.Size(397, 295);
            this.layoutViewField_colEmployeeID.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colEmployeeID.TextSize = new System.Drawing.Size(61, 20);
            this.layoutViewField_colEmployeeID.TextToControlDistance = 5;
            // 
            // colOrderDate1
            // 
            resources.ApplyResources(this.colOrderDate1, "colOrderDate1");
            this.colOrderDate1.FieldName = "OrderDate";
            this.colOrderDate1.LayoutViewField = this.layoutViewField_colOrderDate1;
            this.colOrderDate1.Name = "colOrderDate1";
            // 
            // layoutViewField_colOrderDate1
            // 
            this.layoutViewField_colOrderDate1.EditorPreferredWidth = 89;
            this.layoutViewField_colOrderDate1.Location = new System.Drawing.Point(0, 26);
            this.layoutViewField_colOrderDate1.Name = "layoutViewField_colOrderDate1";
            this.layoutViewField_colOrderDate1.Size = new System.Drawing.Size(156, 23);
            this.layoutViewField_colOrderDate1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colOrderDate1.TextSize = new System.Drawing.Size(58, 13);
            // 
            // colRequiredDate1
            // 
            resources.ApplyResources(this.colRequiredDate1, "colRequiredDate1");
            this.colRequiredDate1.FieldName = "RequiredDate";
            this.colRequiredDate1.LayoutViewField = this.layoutViewField_colRequiredDate1;
            this.colRequiredDate1.Name = "colRequiredDate1";
            // 
            // layoutViewField_colRequiredDate1
            // 
            this.layoutViewField_colRequiredDate1.EditorPreferredWidth = 74;
            this.layoutViewField_colRequiredDate1.Location = new System.Drawing.Point(0, 49);
            this.layoutViewField_colRequiredDate1.Name = "layoutViewField_colRequiredDate1";
            this.layoutViewField_colRequiredDate1.Size = new System.Drawing.Size(156, 23);
            this.layoutViewField_colRequiredDate1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colRequiredDate1.TextSize = new System.Drawing.Size(73, 13);
            // 
            // colShippedDate1
            // 
            resources.ApplyResources(this.colShippedDate1, "colShippedDate1");
            this.colShippedDate1.FieldName = "ShippedDate";
            this.colShippedDate1.LayoutViewField = this.layoutViewField_colShippedDate1;
            this.colShippedDate1.Name = "colShippedDate1";
            // 
            // layoutViewField_colShippedDate1
            // 
            this.layoutViewField_colShippedDate1.EditorPreferredWidth = 80;
            this.layoutViewField_colShippedDate1.Location = new System.Drawing.Point(0, 143);
            this.layoutViewField_colShippedDate1.Name = "layoutViewField_colShippedDate1";
            this.layoutViewField_colShippedDate1.Size = new System.Drawing.Size(157, 23);
            this.layoutViewField_colShippedDate1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colShippedDate1.TextSize = new System.Drawing.Size(68, 13);
            // 
            // colShipVia1
            // 
            resources.ApplyResources(this.colShipVia1, "colShipVia1");
            this.colShipVia1.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colShipVia1.FieldName = "ShipVia";
            this.colShipVia1.LayoutViewField = this.layoutViewField_colShipVia1;
            this.colShipVia1.Name = "colShipVia1";
            // 
            // layoutViewField_colShipVia1
            // 
            this.layoutViewField_colShipVia1.EditorPreferredWidth = 107;
            this.layoutViewField_colShipVia1.Location = new System.Drawing.Point(0, 97);
            this.layoutViewField_colShipVia1.Name = "layoutViewField_colShipVia1";
            this.layoutViewField_colShipVia1.Size = new System.Drawing.Size(157, 23);
            this.layoutViewField_colShipVia1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colShipVia1.TextSize = new System.Drawing.Size(41, 13);
            // 
            // colFreight1
            // 
            resources.ApplyResources(this.colFreight1, "colFreight1");
            this.colFreight1.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colFreight1.DisplayFormat.FormatString = "$0.00";
            this.colFreight1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFreight1.FieldName = "Freight";
            this.colFreight1.LayoutViewField = this.layoutViewField_colFreight1;
            this.colFreight1.Name = "colFreight1";
            // 
            // layoutViewField_colFreight1
            // 
            this.layoutViewField_colFreight1.EditorPreferredWidth = 106;
            this.layoutViewField_colFreight1.Location = new System.Drawing.Point(0, 72);
            this.layoutViewField_colFreight1.Name = "layoutViewField_colFreight1";
            this.layoutViewField_colFreight1.Size = new System.Drawing.Size(156, 23);
            this.layoutViewField_colFreight1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colFreight1.TextSize = new System.Drawing.Size(38, 13);
            // 
            // colShipName1
            // 
            resources.ApplyResources(this.colShipName1, "colShipName1");
            this.colShipName1.FieldName = "ShipName";
            this.colShipName1.LayoutViewField = this.layoutViewField_colShipName1;
            this.colShipName1.Name = "colShipName1";
            // 
            // layoutViewField_colShipName1
            // 
            this.layoutViewField_colShipName1.EditorPreferredWidth = 94;
            this.layoutViewField_colShipName1.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField_colShipName1.Name = "layoutViewField_colShipName1";
            this.layoutViewField_colShipName1.Size = new System.Drawing.Size(157, 23);
            this.layoutViewField_colShipName1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colShipName1.TextSize = new System.Drawing.Size(54, 13);
            // 
            // colShipAddress1
            // 
            resources.ApplyResources(this.colShipAddress1, "colShipAddress1");
            this.colShipAddress1.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colShipAddress1.FieldName = "ShipAddress";
            this.colShipAddress1.LayoutViewField = this.layoutViewField_colShipAddress1;
            this.colShipAddress1.Name = "colShipAddress1";
            // 
            // layoutViewField_colShipAddress1
            // 
            this.layoutViewField_colShipAddress1.EditorPreferredWidth = 325;
            this.layoutViewField_colShipAddress1.Location = new System.Drawing.Point(0, 168);
            this.layoutViewField_colShipAddress1.Name = "layoutViewField_colShipAddress1";
            this.layoutViewField_colShipAddress1.Size = new System.Drawing.Size(400, 25);
            this.layoutViewField_colShipAddress1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colShipAddress1.TextSize = new System.Drawing.Size(66, 13);
            // 
            // colShipCity1
            // 
            resources.ApplyResources(this.colShipCity1, "colShipCity1");
            this.colShipCity1.ColumnEdit = this.repositoryItemComboBox4;
            this.colShipCity1.FieldName = "ShipCity";
            this.colShipCity1.LayoutViewField = this.layoutViewField_colShipCity1;
            this.colShipCity1.Name = "colShipCity1";
            // 
            // layoutViewField_colShipCity1
            // 
            this.layoutViewField_colShipCity1.EditorPreferredWidth = 103;
            this.layoutViewField_colShipCity1.Location = new System.Drawing.Point(0, 216);
            this.layoutViewField_colShipCity1.Name = "layoutViewField_colShipCity1";
            this.layoutViewField_colShipCity1.Size = new System.Drawing.Size(158, 23);
            this.layoutViewField_colShipCity1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colShipCity1.TextSize = new System.Drawing.Size(46, 13);
            // 
            // colShipRegion1
            // 
            resources.ApplyResources(this.colShipRegion1, "colShipRegion1");
            this.colShipRegion1.ColumnEdit = this.repositoryItemComboBox3;
            this.colShipRegion1.FieldName = "ShipRegion";
            this.colShipRegion1.LayoutViewField = this.layoutViewField_colShipRegion1;
            this.colShipRegion1.Name = "colShipRegion1";
            // 
            // layoutViewField_colShipRegion1
            // 
            this.layoutViewField_colShipRegion1.EditorPreferredWidth = 89;
            this.layoutViewField_colShipRegion1.Location = new System.Drawing.Point(0, 239);
            this.layoutViewField_colShipRegion1.Name = "layoutViewField_colShipRegion1";
            this.layoutViewField_colShipRegion1.Size = new System.Drawing.Size(158, 23);
            this.layoutViewField_colShipRegion1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colShipRegion1.TextSize = new System.Drawing.Size(60, 13);
            // 
            // colShipPostalCode1
            // 
            resources.ApplyResources(this.colShipPostalCode1, "colShipPostalCode1");
            this.colShipPostalCode1.ColumnEdit = this.repositoryItemComboBox2;
            this.colShipPostalCode1.FieldName = "ShipPostalCode";
            this.colShipPostalCode1.LayoutViewField = this.layoutViewField_colShipPostalCode1;
            this.colShipPostalCode1.Name = "colShipPostalCode1";
            // 
            // layoutViewField_colShipPostalCode1
            // 
            this.layoutViewField_colShipPostalCode1.EditorPreferredWidth = 68;
            this.layoutViewField_colShipPostalCode1.Location = new System.Drawing.Point(0, 262);
            this.layoutViewField_colShipPostalCode1.Name = "layoutViewField_colShipPostalCode1";
            this.layoutViewField_colShipPostalCode1.Size = new System.Drawing.Size(158, 23);
            this.layoutViewField_colShipPostalCode1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colShipPostalCode1.TextSize = new System.Drawing.Size(84, 13);
            // 
            // colShipCountry1
            // 
            resources.ApplyResources(this.colShipCountry1, "colShipCountry1");
            this.colShipCountry1.ColumnEdit = this.repositoryItemComboBox1;
            this.colShipCountry1.FieldName = "ShipCountry";
            this.colShipCountry1.LayoutViewField = this.layoutViewField_colShipCountry1;
            this.colShipCountry1.Name = "colShipCountry1";
            // 
            // layoutViewField_colShipCountry1
            // 
            this.layoutViewField_colShipCountry1.EditorPreferredWidth = 83;
            this.layoutViewField_colShipCountry1.Location = new System.Drawing.Point(0, 193);
            this.layoutViewField_colShipCountry1.Name = "layoutViewField_colShipCountry1";
            this.layoutViewField_colShipCountry1.Size = new System.Drawing.Size(158, 23);
            this.layoutViewField_colShipCountry1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colShipCountry1.TextSize = new System.Drawing.Size(66, 13);
            // 
            // layoutViewCard2
            // 
            this.layoutViewCard2.AllowDrawBackground = false;
            resources.ApplyResources(this.layoutViewCard2, "layoutViewCard2");
            this.layoutViewCard2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard2.GroupBordersVisible = false;
            this.layoutViewCard2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colFreight1,
            this.LayoutItem2,
            this.LayoutItem3,
            this.layoutViewField_colShipPostalCode1,
            this.LayoutItem5,
            this.layoutViewField_colShipRegion1,
            this.layoutViewField_colShipCity1,
            this.layoutViewField_colShipCountry1,
            this.LayoutItem9,
            this.LayoutItem10,
            this.layoutViewField_colShippedDate1,
            this.layoutViewField_colShipName1,
            this.layoutViewField_colShipVia1,
            this.LayoutItem14,
            this.LayoutItem15,
            this.layoutViewField_colRequiredDate1,
            this.layoutViewField_colOrderDate1,
            this.LayoutItem18,
            this.layoutViewField_colShipAddress1});
            this.layoutViewCard2.Name = "layoutViewCard2";
            this.layoutViewCard2.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            // 
            // LayoutItem2
            // 
            this.LayoutItem2.AllowHotTrack = false;
            this.LayoutItem2.AppearanceItemCaption.Font = ((System.Drawing.Font)(resources.GetObject("LayoutItem2.AppearanceItemCaption.Font")));
            this.LayoutItem2.AppearanceItemCaption.Options.UseFont = true;
            resources.ApplyResources(this.LayoutItem2, "LayoutItem2");
            this.LayoutItem2.Location = new System.Drawing.Point(0, 0);
            this.LayoutItem2.MaxSize = new System.Drawing.Size(397, 24);
            this.LayoutItem2.MinSize = new System.Drawing.Size(397, 24);
            this.LayoutItem2.Name = "LayoutItem2";
            this.LayoutItem2.Size = new System.Drawing.Size(400, 24);
            this.LayoutItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LayoutItem2.TextSize = new System.Drawing.Size(230, 19);
            // 
            // LayoutItem3
            // 
            this.LayoutItem3.AllowHotTrack = false;
            resources.ApplyResources(this.LayoutItem3, "LayoutItem3");
            this.LayoutItem3.Location = new System.Drawing.Point(0, 24);
            this.LayoutItem3.Name = "LayoutItem3";
            this.LayoutItem3.Size = new System.Drawing.Size(400, 2);
            // 
            // LayoutItem5
            // 
            this.LayoutItem5.AllowHotTrack = false;
            resources.ApplyResources(this.LayoutItem5, "LayoutItem5");
            this.LayoutItem5.Location = new System.Drawing.Point(158, 193);
            this.LayoutItem5.Name = "LayoutItem5";
            this.LayoutItem5.Size = new System.Drawing.Size(242, 102);
            this.LayoutItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutItem9
            // 
            this.LayoutItem9.AllowHotTrack = false;
            resources.ApplyResources(this.LayoutItem9, "LayoutItem9");
            this.LayoutItem9.Location = new System.Drawing.Point(0, 166);
            this.LayoutItem9.Name = "LayoutItem9";
            this.LayoutItem9.Size = new System.Drawing.Size(400, 2);
            // 
            // LayoutItem10
            // 
            this.LayoutItem10.AllowHotTrack = false;
            resources.ApplyResources(this.LayoutItem10, "LayoutItem10");
            this.LayoutItem10.Location = new System.Drawing.Point(157, 97);
            this.LayoutItem10.Name = "LayoutItem10";
            this.LayoutItem10.Size = new System.Drawing.Size(243, 69);
            this.LayoutItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutItem14
            // 
            this.LayoutItem14.AllowHotTrack = false;
            resources.ApplyResources(this.LayoutItem14, "LayoutItem14");
            this.LayoutItem14.Location = new System.Drawing.Point(0, 95);
            this.LayoutItem14.Name = "LayoutItem14";
            this.LayoutItem14.Size = new System.Drawing.Size(400, 2);
            // 
            // LayoutItem15
            // 
            this.LayoutItem15.AllowHotTrack = false;
            resources.ApplyResources(this.LayoutItem15, "LayoutItem15");
            this.LayoutItem15.Location = new System.Drawing.Point(156, 26);
            this.LayoutItem15.Name = "LayoutItem15";
            this.LayoutItem15.Size = new System.Drawing.Size(244, 69);
            this.LayoutItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutItem18
            // 
            this.LayoutItem18.AllowHotTrack = false;
            resources.ApplyResources(this.LayoutItem18, "LayoutItem18");
            this.LayoutItem18.Location = new System.Drawing.Point(0, 285);
            this.LayoutItem18.Name = "LayoutItem18";
            this.LayoutItem18.Size = new System.Drawing.Size(158, 10);
            this.LayoutItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // allowEditOnDoubleClick
            // 
            resources.ApplyResources(this.allowEditOnDoubleClick, "allowEditOnDoubleClick");
            this.allowEditOnDoubleClick.Name = "allowEditOnDoubleClick";
            this.allowEditOnDoubleClick.Properties.Caption = resources.GetString("allowEditOnDoubleClick.Properties.Caption");
            this.allowEditOnDoubleClick.StyleController = this.layoutControl1;
            this.allowEditOnDoubleClick.CheckedChanged += new System.EventHandler(this.allowEditOnDoubleClick_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            resources.ApplyResources(this.layoutControlGroup1, "layoutControlGroup1");
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.detailTableItem,
            this.masterTableItem,
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(-20, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(577, 368);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // detailTableItem
            // 
            this.detailTableItem.Control = this.detailGrid;
            resources.ApplyResources(this.detailTableItem, "detailTableItem");
            this.detailTableItem.Location = new System.Drawing.Point(125, 57);
            this.detailTableItem.Name = "detailTableItem";
            this.detailTableItem.Size = new System.Drawing.Size(432, 291);
            this.detailTableItem.TextSize = new System.Drawing.Size(0, 0);
            this.detailTableItem.TextToControlDistance = 0;
            this.detailTableItem.TextVisible = false;
            // 
            // masterTableItem
            // 
            this.masterTableItem.Control = this.masterGrid;
            resources.ApplyResources(this.masterTableItem, "masterTableItem");
            this.masterTableItem.Location = new System.Drawing.Point(0, 57);
            this.masterTableItem.Name = "masterTableItem";
            this.masterTableItem.Size = new System.Drawing.Size(125, 291);
            this.masterTableItem.TextSize = new System.Drawing.Size(0, 0);
            this.masterTableItem.TextToControlDistance = 0;
            this.masterTableItem.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            resources.ApplyResources(this.layoutControlGroup2, "layoutControlGroup2");
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(557, 57);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.cbMasterTablePosition;
            this.layoutControlItem3.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            resources.ApplyResources(this.layoutControlItem3, "layoutControlItem3");
            this.layoutControlItem3.FillControlToClientArea = false;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(250, 31);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(250, 31);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(250, 33);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.allowEditOnDoubleClick;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            resources.ApplyResources(this.layoutControlItem2, "layoutControlItem2");
            this.layoutControlItem2.Location = new System.Drawing.Point(250, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(148, 33);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            this.layoutControlItem2.TrimClientAreaToControl = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            resources.ApplyResources(this.emptySpaceItem1, "emptySpaceItem1");
            this.emptySpaceItem1.Location = new System.Drawing.Point(398, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 33);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.detailShowHideBtn;
            resources.ApplyResources(this.layoutControlItem1, "layoutControlItem1");
            this.layoutControlItem1.Location = new System.Drawing.Point(408, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(125, 33);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(125, 33);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(125, 33);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // AdvancedGridEditing
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "AdvancedGridEditing";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbMasterTablePosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employesLayoutView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewOrderGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editOrderlayoutView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colOrderID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCustomerID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colOrderDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colRequiredDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShippedDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipVia1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colFreight1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipCity1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipRegion1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipPostalCode1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colShipCountry1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowEditOnDoubleClick.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailTableItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterTableItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton detailShowHideBtn;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbMasterTablePosition;
        private GridControl masterGrid;
        private DevExpress.XtraGrid.Views.Layout.LayoutView employesLayoutView;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colLastName;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colFirstName;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colPhoto;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private GridControl detailGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView viewOrderGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiredDate;
        private DevExpress.XtraGrid.Columns.GridColumn colShippedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colShipVia;
        private DevExpress.XtraGrid.Columns.GridColumn colFreight;
        private DevExpress.XtraGrid.Columns.GridColumn colShipName;
        private DevExpress.XtraGrid.Columns.GridColumn colShipAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colShipCity;
        private DevExpress.XtraGrid.Columns.GridColumn colShipRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colShipPostalCode;
        private DevExpress.XtraGrid.Columns.GridColumn colShipCountry;
        private DevExpress.XtraGrid.Views.Layout.LayoutView editOrderlayoutView;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colOrderID1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colCustomerID1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colOrderDate1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colRequiredDate1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colShippedDate1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colShipVia1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colFreight1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colShipName1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colShipAddress1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colShipCity1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colShipRegion1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colShipPostalCode1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colShipCountry1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem detailTableItem;
        private DevExpress.XtraLayout.LayoutControlItem masterTableItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CheckEdit allowEditOnDoubleClick;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colOrderID1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colCustomerID1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colEmployeeID;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colOrderDate1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colRequiredDate1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colShippedDate1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colShipVia1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colFreight1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colShipName1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colShipAddress1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colShipCity1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colShipRegion1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colShipPostalCode1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colShipCountry1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard2;
        private DevExpress.XtraLayout.SimpleLabelItem LayoutItem2;
        private DevExpress.XtraLayout.SimpleSeparator LayoutItem3;
        private DevExpress.XtraLayout.EmptySpaceItem LayoutItem5;
        private DevExpress.XtraLayout.SimpleSeparator LayoutItem9;
        private DevExpress.XtraLayout.EmptySpaceItem LayoutItem10;
        private DevExpress.XtraLayout.SimpleSeparator LayoutItem14;
        private DevExpress.XtraLayout.EmptySpaceItem LayoutItem15;
        private DevExpress.XtraLayout.EmptySpaceItem LayoutItem18;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colFullName;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colLastName;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colFirstName;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colPhoto;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
    }
}
