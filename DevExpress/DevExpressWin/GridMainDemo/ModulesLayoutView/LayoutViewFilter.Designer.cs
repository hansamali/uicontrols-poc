﻿namespace DevExpress.XtraGrid.Demos {
    partial class LayoutViewFilter {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.accordionControl = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.filteringUIContext = new DevExpress.Utils.Filtering.FilteringUIContext(this.components);
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.modelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutView = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.colTrademark = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colTrademark = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colName = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colName = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colModification = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colModification = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colCategory = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colCategory = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colPrice = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colPrice = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colMPGCity = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colMPGCity = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colMPGHighway = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colMPGHighway = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colDoors = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colDoors = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colBodyStyle = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colBodyStyle = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colCylinders = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colCylinders = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colHorsepower = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colHorsepower = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colTorque = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colTorque = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colTransmissionSpeeds = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colTransmissionSpeeds = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colTransmissionType = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colTransmissionType = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colDescription = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colDescription = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colInStock = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colInStock = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colTrademarkImage = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colTrademarkImage = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colTrademarkName = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colTrademarkName = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colDeliveryDate = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colDeliveryDate = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colImage = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutViewField_colImage = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colID = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colID = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colPhoto = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colPhoto = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filteringUIContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTrademark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colModification)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMPGCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMPGHighway)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDoors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colBodyStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCylinders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colHorsepower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTorque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTransmissionSpeeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTransmissionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colInStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTrademarkImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTrademarkName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            this.SuspendLayout();
            // 
            // accordionControl
            // 
            this.accordionControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.accordionControl.ExpandElementMode = DevExpress.XtraBars.Navigation.ExpandElementMode.Multiple;
            this.accordionControl.Location = new System.Drawing.Point(597, 0);
            this.accordionControl.Name = "accordionControl";
            this.accordionControl.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Touch;
            this.accordionControl.Size = new System.Drawing.Size(318, 588);
            this.accordionControl.TabIndex = 0;
            this.accordionControl.Text = "accordionControl1";
            // 
            // filteringUIContext
            // 
            this.filteringUIContext.Control = this.accordionControl;
            this.filteringUIContext.ModelType = typeof(DevExpress.XtraGrid.Demos.FilteringModel);
            this.filteringUIContext.FilterCriteriaChanged += new DevExpress.Utils.Filtering.FilterCiteriaChangedEventHandler(this.filteringUIContext_FilterCriteriaChanged);
            this.filteringUIContext.QueryRangeData += new DevExpress.Utils.Filtering.QueryDataEventHandler<DevExpress.Utils.Filtering.QueryRangeDataEventArgs, DevExpress.Utils.Filtering.RangeData>(this.filteringUIContext_QueryRangeData);
            this.filteringUIContext.QueryLookupData += new DevExpress.Utils.Filtering.QueryDataEventHandler<DevExpress.Utils.Filtering.QueryLookupDataEventArgs, DevExpress.Utils.Filtering.LookupData>(this.filteringUIContext_QueryLookupData);
            // 
            // gridControl
            // 
            this.gridControl.DataSource = this.modelBindingSource;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.layoutView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1});
            this.gridControl.Size = new System.Drawing.Size(597, 588);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView});
            // 
            // modelBindingSource
            // 
            this.modelBindingSource.DataSource = typeof(DevExpress.XtraGrid.Demos.VehiclesData.Model);
            // 
            // layoutView
            // 
            this.layoutView.Appearance.CardCaption.Font = new System.Drawing.Font("Segoe UI Light", 11.25F);
            this.layoutView.Appearance.CardCaption.Options.UseFont = true;
            this.layoutView.Appearance.FocusedCardCaption.Font = new System.Drawing.Font("Segoe UI Light", 11.25F);
            this.layoutView.Appearance.FocusedCardCaption.Options.UseFont = true;
            this.layoutView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.layoutView.CardCaptionFormat = "{19}";
            this.layoutView.CardHorzInterval = 12;
            this.layoutView.CardMinSize = new System.Drawing.Size(195, 189);
            this.layoutView.CardVertInterval = 12;
            this.layoutView.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.colTrademark,
            this.colName,
            this.colModification,
            this.colCategory,
            this.colPrice,
            this.colMPGCity,
            this.colMPGHighway,
            this.colDoors,
            this.colBodyStyle,
            this.colCylinders,
            this.colHorsepower,
            this.colTorque,
            this.colTransmissionSpeeds,
            this.colTransmissionType,
            this.colDescription,
            this.colInStock,
            this.colTrademarkImage,
            this.colTrademarkName,
            this.colDeliveryDate,
            this.colImage,
            this.colID,
            this.colPhoto});
            this.layoutView.GridControl = this.gridControl;
            this.layoutView.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colTrademark,
            this.layoutViewField_colModification,
            this.layoutViewField_colCategory,
            this.layoutViewField_colMPGCity,
            this.layoutViewField_colMPGHighway,
            this.layoutViewField_colDoors,
            this.layoutViewField_colBodyStyle,
            this.layoutViewField_colCylinders,
            this.layoutViewField_colHorsepower,
            this.layoutViewField_colTorque,
            this.layoutViewField_colTransmissionSpeeds,
            this.layoutViewField_colTransmissionType,
            this.layoutViewField_colDescription,
            this.layoutViewField_colInStock,
            this.layoutViewField_colTrademarkImage,
            this.layoutViewField_colTrademarkName,
            this.layoutViewField_colID,
            this.layoutViewField_colDeliveryDate,
            this.layoutViewField_colPhoto});
            this.layoutView.Name = "layoutView";
            this.layoutView.OptionsBehavior.Editable = false;
            this.layoutView.OptionsBehavior.ReadOnly = true;
            this.layoutView.OptionsCustomization.AllowFilter = false;
            this.layoutView.OptionsFilter.AllowFilterEditor = false;
            this.layoutView.OptionsFilter.AllowMRUFilterList = false;
            this.layoutView.OptionsItemText.TextToControlDistance = 0;
            this.layoutView.OptionsMultiRecordMode.MultiRowScrollBarOrientation = DevExpress.XtraGrid.Views.Layout.ScrollBarOrientation.Vertical;
            this.layoutView.OptionsView.AllowHotTrackFields = false;
            this.layoutView.OptionsView.FocusRectStyle = DevExpress.XtraGrid.Views.Layout.FocusRectStyle.None;
            this.layoutView.OptionsView.ShowCardExpandButton = false;
            this.layoutView.OptionsView.ShowCardFieldBorders = true;
            this.layoutView.OptionsView.ShowHeaderPanel = false;
            this.layoutView.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.MultiRow;
            this.layoutView.PaintStyleName = "Skin";
            this.layoutView.TemplateCard = this.layoutViewCard1;
            this.layoutView.ColumnFilterChanged += new System.EventHandler(this.layoutView_ColumnFilterChanged);
            // 
            // colTrademark
            // 
            this.colTrademark.FieldName = "Trademark";
            this.colTrademark.LayoutViewField = this.layoutViewField_colTrademark;
            this.colTrademark.Name = "colTrademark";
            // 
            // layoutViewField_colTrademark
            // 
            this.layoutViewField_colTrademark.EditorPreferredWidth = 20;
            this.layoutViewField_colTrademark.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colTrademark.Name = "layoutViewField_colTrademark";
            this.layoutViewField_colTrademark.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colTrademark.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colName
            // 
            this.colName.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.colName.AppearanceCell.Options.UseFont = true;
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.FieldName = "Name";
            this.colName.LayoutViewField = this.layoutViewField_colName;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            // 
            // layoutViewField_colName
            // 
            this.layoutViewField_colName.EditorPreferredWidth = 187;
            this.layoutViewField_colName.Location = new System.Drawing.Point(0, 109);
            this.layoutViewField_colName.Name = "layoutViewField_colName";
            this.layoutViewField_colName.Size = new System.Drawing.Size(191, 24);
            this.layoutViewField_colName.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colName.TextVisible = false;
            // 
            // colModification
            // 
            this.colModification.FieldName = "Modification";
            this.colModification.LayoutViewField = this.layoutViewField_colModification;
            this.colModification.Name = "colModification";
            // 
            // layoutViewField_colModification
            // 
            this.layoutViewField_colModification.EditorPreferredWidth = 20;
            this.layoutViewField_colModification.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colModification.Name = "layoutViewField_colModification";
            this.layoutViewField_colModification.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colModification.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colCategory
            // 
            this.colCategory.FieldName = "Category";
            this.colCategory.LayoutViewField = this.layoutViewField_colCategory;
            this.colCategory.Name = "colCategory";
            // 
            // layoutViewField_colCategory
            // 
            this.layoutViewField_colCategory.EditorPreferredWidth = 20;
            this.layoutViewField_colCategory.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colCategory.Name = "layoutViewField_colCategory";
            this.layoutViewField_colCategory.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colCategory.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colPrice
            // 
            this.colPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.FieldName = "Price";
            this.colPrice.LayoutViewField = this.layoutViewField_colPrice;
            this.colPrice.Name = "colPrice";
            // 
            // layoutViewField_colPrice
            // 
            this.layoutViewField_colPrice.EditorPreferredWidth = 187;
            this.layoutViewField_colPrice.Location = new System.Drawing.Point(0, 133);
            this.layoutViewField_colPrice.Name = "layoutViewField_colPrice";
            this.layoutViewField_colPrice.Size = new System.Drawing.Size(191, 27);
            this.layoutViewField_colPrice.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colPrice.TextVisible = false;
            // 
            // colMPGCity
            // 
            this.colMPGCity.FieldName = "MPGCity";
            this.colMPGCity.LayoutViewField = this.layoutViewField_colMPGCity;
            this.colMPGCity.Name = "colMPGCity";
            // 
            // layoutViewField_colMPGCity
            // 
            this.layoutViewField_colMPGCity.EditorPreferredWidth = 20;
            this.layoutViewField_colMPGCity.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colMPGCity.Name = "layoutViewField_colMPGCity";
            this.layoutViewField_colMPGCity.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colMPGCity.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colMPGHighway
            // 
            this.colMPGHighway.FieldName = "MPGHighway";
            this.colMPGHighway.LayoutViewField = this.layoutViewField_colMPGHighway;
            this.colMPGHighway.Name = "colMPGHighway";
            // 
            // layoutViewField_colMPGHighway
            // 
            this.layoutViewField_colMPGHighway.EditorPreferredWidth = 20;
            this.layoutViewField_colMPGHighway.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colMPGHighway.Name = "layoutViewField_colMPGHighway";
            this.layoutViewField_colMPGHighway.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colMPGHighway.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colDoors
            // 
            this.colDoors.FieldName = "Doors";
            this.colDoors.LayoutViewField = this.layoutViewField_colDoors;
            this.colDoors.Name = "colDoors";
            // 
            // layoutViewField_colDoors
            // 
            this.layoutViewField_colDoors.EditorPreferredWidth = 20;
            this.layoutViewField_colDoors.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colDoors.Name = "layoutViewField_colDoors";
            this.layoutViewField_colDoors.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colDoors.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colBodyStyle
            // 
            this.colBodyStyle.FieldName = "BodyStyle";
            this.colBodyStyle.LayoutViewField = this.layoutViewField_colBodyStyle;
            this.colBodyStyle.Name = "colBodyStyle";
            // 
            // layoutViewField_colBodyStyle
            // 
            this.layoutViewField_colBodyStyle.EditorPreferredWidth = 20;
            this.layoutViewField_colBodyStyle.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colBodyStyle.Name = "layoutViewField_colBodyStyle";
            this.layoutViewField_colBodyStyle.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colBodyStyle.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colCylinders
            // 
            this.colCylinders.FieldName = "Cylinders";
            this.colCylinders.LayoutViewField = this.layoutViewField_colCylinders;
            this.colCylinders.Name = "colCylinders";
            // 
            // layoutViewField_colCylinders
            // 
            this.layoutViewField_colCylinders.EditorPreferredWidth = 20;
            this.layoutViewField_colCylinders.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colCylinders.Name = "layoutViewField_colCylinders";
            this.layoutViewField_colCylinders.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colCylinders.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colHorsepower
            // 
            this.colHorsepower.FieldName = "Horsepower";
            this.colHorsepower.LayoutViewField = this.layoutViewField_colHorsepower;
            this.colHorsepower.Name = "colHorsepower";
            // 
            // layoutViewField_colHorsepower
            // 
            this.layoutViewField_colHorsepower.EditorPreferredWidth = 20;
            this.layoutViewField_colHorsepower.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colHorsepower.Name = "layoutViewField_colHorsepower";
            this.layoutViewField_colHorsepower.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colHorsepower.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colTorque
            // 
            this.colTorque.FieldName = "Torque";
            this.colTorque.LayoutViewField = this.layoutViewField_colTorque;
            this.colTorque.Name = "colTorque";
            // 
            // layoutViewField_colTorque
            // 
            this.layoutViewField_colTorque.EditorPreferredWidth = 20;
            this.layoutViewField_colTorque.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colTorque.Name = "layoutViewField_colTorque";
            this.layoutViewField_colTorque.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colTorque.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colTransmissionSpeeds
            // 
            this.colTransmissionSpeeds.FieldName = "TransmissionSpeeds";
            this.colTransmissionSpeeds.LayoutViewField = this.layoutViewField_colTransmissionSpeeds;
            this.colTransmissionSpeeds.Name = "colTransmissionSpeeds";
            // 
            // layoutViewField_colTransmissionSpeeds
            // 
            this.layoutViewField_colTransmissionSpeeds.EditorPreferredWidth = 20;
            this.layoutViewField_colTransmissionSpeeds.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colTransmissionSpeeds.Name = "layoutViewField_colTransmissionSpeeds";
            this.layoutViewField_colTransmissionSpeeds.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colTransmissionSpeeds.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colTransmissionType
            // 
            this.colTransmissionType.FieldName = "TransmissionType";
            this.colTransmissionType.LayoutViewField = this.layoutViewField_colTransmissionType;
            this.colTransmissionType.Name = "colTransmissionType";
            // 
            // layoutViewField_colTransmissionType
            // 
            this.layoutViewField_colTransmissionType.EditorPreferredWidth = 20;
            this.layoutViewField_colTransmissionType.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colTransmissionType.Name = "layoutViewField_colTransmissionType";
            this.layoutViewField_colTransmissionType.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colTransmissionType.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.LayoutViewField = this.layoutViewField_colDescription;
            this.colDescription.Name = "colDescription";
            // 
            // layoutViewField_colDescription
            // 
            this.layoutViewField_colDescription.EditorPreferredWidth = 20;
            this.layoutViewField_colDescription.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colDescription.Name = "layoutViewField_colDescription";
            this.layoutViewField_colDescription.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colDescription.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colInStock
            // 
            this.colInStock.FieldName = "InStock";
            this.colInStock.LayoutViewField = this.layoutViewField_colInStock;
            this.colInStock.Name = "colInStock";
            // 
            // layoutViewField_colInStock
            // 
            this.layoutViewField_colInStock.EditorPreferredWidth = 20;
            this.layoutViewField_colInStock.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colInStock.Name = "layoutViewField_colInStock";
            this.layoutViewField_colInStock.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colInStock.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colTrademarkImage
            // 
            this.colTrademarkImage.FieldName = "TrademarkImage";
            this.colTrademarkImage.LayoutViewField = this.layoutViewField_colTrademarkImage;
            this.colTrademarkImage.Name = "colTrademarkImage";
            this.colTrademarkImage.OptionsColumn.ReadOnly = true;
            // 
            // layoutViewField_colTrademarkImage
            // 
            this.layoutViewField_colTrademarkImage.EditorPreferredWidth = 20;
            this.layoutViewField_colTrademarkImage.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colTrademarkImage.Name = "layoutViewField_colTrademarkImage";
            this.layoutViewField_colTrademarkImage.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colTrademarkImage.StartNewLine = true;
            this.layoutViewField_colTrademarkImage.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colTrademarkName
            // 
            this.colTrademarkName.FieldName = "TrademarkName";
            this.colTrademarkName.LayoutViewField = this.layoutViewField_colTrademarkName;
            this.colTrademarkName.Name = "colTrademarkName";
            this.colTrademarkName.OptionsColumn.ReadOnly = true;
            // 
            // layoutViewField_colTrademarkName
            // 
            this.layoutViewField_colTrademarkName.EditorPreferredWidth = 20;
            this.layoutViewField_colTrademarkName.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colTrademarkName.Name = "layoutViewField_colTrademarkName";
            this.layoutViewField_colTrademarkName.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colTrademarkName.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.FieldName = "DeliveryDate";
            this.colDeliveryDate.LayoutViewField = this.layoutViewField_colDeliveryDate;
            this.colDeliveryDate.Name = "colDeliveryDate";
            // 
            // layoutViewField_colDeliveryDate
            // 
            this.layoutViewField_colDeliveryDate.EditorPreferredWidth = 20;
            this.layoutViewField_colDeliveryDate.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colDeliveryDate.Name = "layoutViewField_colDeliveryDate";
            this.layoutViewField_colDeliveryDate.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colDeliveryDate.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colImage
            // 
            this.colImage.ColumnEdit = this.repositoryItemPictureEdit1;
            this.colImage.FieldName = "Photo";
            this.colImage.LayoutViewField = this.layoutViewField_colImage;
            this.colImage.Name = "colImage";
            this.colImage.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.PictureInterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // layoutViewField_colImage
            // 
            this.layoutViewField_colImage.EditorPreferredWidth = 187;
            this.layoutViewField_colImage.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colImage.MaxSize = new System.Drawing.Size(191, 109);
            this.layoutViewField_colImage.MinSize = new System.Drawing.Size(191, 109);
            this.layoutViewField_colImage.Name = "layoutViewField_colImage";
            this.layoutViewField_colImage.Size = new System.Drawing.Size(191, 109);
            this.layoutViewField_colImage.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colImage.StartNewLine = true;
            this.layoutViewField_colImage.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colImage.TextVisible = false;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.LayoutViewField = this.layoutViewField_colID;
            this.colID.Name = "colID";
            // 
            // layoutViewField_colID
            // 
            this.layoutViewField_colID.EditorPreferredWidth = 20;
            this.layoutViewField_colID.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colID.Name = "layoutViewField_colID";
            this.layoutViewField_colID.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colID.TextSize = new System.Drawing.Size(103, 20);
            // 
            // colPhoto
            // 
            this.colPhoto.FieldName = "Photo";
            this.colPhoto.LayoutViewField = this.layoutViewField_colPhoto;
            this.colPhoto.Name = "colPhoto";
            // 
            // layoutViewField_colPhoto
            // 
            this.layoutViewField_colPhoto.EditorPreferredWidth = 20;
            this.layoutViewField_colPhoto.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colPhoto.Name = "layoutViewField_colPhoto";
            this.layoutViewField_colPhoto.Size = new System.Drawing.Size(174, 144);
            this.layoutViewField_colPhoto.StartNewLine = true;
            this.layoutViewField_colPhoto.TextSize = new System.Drawing.Size(103, 20);
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.BackgroundImageVisible = true;
            this.layoutViewCard1.CustomizationFormText = "TemplateCard";
            this.layoutViewCard1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewCard1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colPrice,
            this.layoutViewField_colName,
            this.layoutViewField_colImage});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 0;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // LayoutViewFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.accordionControl);
            this.Name = "LayoutViewFilter";
            this.Size = new System.Drawing.Size(915, 588);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filteringUIContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTrademark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colModification)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMPGCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMPGHighway)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDoors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colBodyStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCylinders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colHorsepower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTorque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTransmissionSpeeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTransmissionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colInStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTrademarkImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTrademarkName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl;
        private XtraGrid.GridControl gridControl;
        private Views.Layout.LayoutView layoutView;
        private Utils.Filtering.FilteringUIContext filteringUIContext;
        private System.Windows.Forms.BindingSource modelBindingSource;
        private Columns.LayoutViewColumn colTrademark;
        private Columns.LayoutViewColumn colName;
        private Columns.LayoutViewColumn colModification;
        private Columns.LayoutViewColumn colCategory;
        private Columns.LayoutViewColumn colPrice;
        private Columns.LayoutViewColumn colMPGCity;
        private Columns.LayoutViewColumn colMPGHighway;
        private Columns.LayoutViewColumn colDoors;
        private Columns.LayoutViewColumn colBodyStyle;
        private Columns.LayoutViewColumn colCylinders;
        private Columns.LayoutViewColumn colHorsepower;
        private Columns.LayoutViewColumn colTorque;
        private Columns.LayoutViewColumn colTransmissionSpeeds;
        private Columns.LayoutViewColumn colTransmissionType;
        private Columns.LayoutViewColumn colDescription;
        private Columns.LayoutViewColumn colInStock;
        private Columns.LayoutViewColumn colTrademarkImage;
        private Columns.LayoutViewColumn colTrademarkName;
        private XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private Columns.LayoutViewColumn colDeliveryDate;
        private Columns.LayoutViewColumn colImage;
        private Columns.LayoutViewColumn colID;
        private Columns.LayoutViewColumn colPhoto;
        private Views.Layout.LayoutViewField layoutViewField_colTrademark;
        private Views.Layout.LayoutViewField layoutViewField_colName;
        private Views.Layout.LayoutViewField layoutViewField_colModification;
        private Views.Layout.LayoutViewField layoutViewField_colCategory;
        private Views.Layout.LayoutViewField layoutViewField_colPrice;
        private Views.Layout.LayoutViewField layoutViewField_colMPGCity;
        private Views.Layout.LayoutViewField layoutViewField_colMPGHighway;
        private Views.Layout.LayoutViewField layoutViewField_colDoors;
        private Views.Layout.LayoutViewField layoutViewField_colBodyStyle;
        private Views.Layout.LayoutViewField layoutViewField_colCylinders;
        private Views.Layout.LayoutViewField layoutViewField_colHorsepower;
        private Views.Layout.LayoutViewField layoutViewField_colTorque;
        private Views.Layout.LayoutViewField layoutViewField_colTransmissionSpeeds;
        private Views.Layout.LayoutViewField layoutViewField_colTransmissionType;
        private Views.Layout.LayoutViewField layoutViewField_colDescription;
        private Views.Layout.LayoutViewField layoutViewField_colInStock;
        private Views.Layout.LayoutViewField layoutViewField_colTrademarkImage;
        private Views.Layout.LayoutViewField layoutViewField_colTrademarkName;
        private Views.Layout.LayoutViewField layoutViewField_colDeliveryDate;
        private Views.Layout.LayoutViewField layoutViewField_colImage;
        private Views.Layout.LayoutViewField layoutViewField_colID;
        private Views.Layout.LayoutViewField layoutViewField_colPhoto;
        private Views.Layout.LayoutViewCard layoutViewCard1;
    }
}
