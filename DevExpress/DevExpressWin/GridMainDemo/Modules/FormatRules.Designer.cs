﻿namespace DevExpress.XtraGrid.Demos {
    partial class FormatRules {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleDataBar formatConditionRuleDataBar1 = new DevExpress.XtraEditors.FormatConditionRuleDataBar();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet1 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet1 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon1 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon2 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon3 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleDataBar formatConditionRuleDataBar2 = new DevExpress.XtraEditors.FormatConditionRuleDataBar();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet2 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet2 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon4 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon5 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon6 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet3 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet3 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon7 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon8 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon9 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon10 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon11 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleTopBottom formatConditionRuleTopBottom1 = new DevExpress.XtraEditors.FormatConditionRuleTopBottom();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule7 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleTopBottom formatConditionRuleTopBottom2 = new DevExpress.XtraEditors.FormatConditionRuleTopBottom();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule8 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule9 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule10 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule11 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule12 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleTopBottom formatConditionRuleTopBottom3 = new DevExpress.XtraEditors.FormatConditionRuleTopBottom();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule13 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule14 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleTopBottom formatConditionRuleTopBottom4 = new DevExpress.XtraEditors.FormatConditionRuleTopBottom();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule15 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleTopBottom formatConditionRuleTopBottom5 = new DevExpress.XtraEditors.FormatConditionRuleTopBottom();
            this.colSales = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSalesVsTarget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomersSatisfaction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colMarketShare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // colSales
            // 
            this.colSales.ColumnEdit = this.repositoryItemTextEdit3;
            this.colSales.DisplayFormat.FormatString = "#,##0,,M";
            this.colSales.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSales.FieldName = "Sales";
            this.colSales.Name = "colSales";
            this.colSales.OptionsColumn.AllowEdit = false;
            this.colSales.OptionsColumn.AllowFocus = false;
            this.colSales.Visible = true;
            this.colSales.VisibleIndex = 1;
            this.colSales.Width = 184;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "n";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colSalesVsTarget
            // 
            this.colSalesVsTarget.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSalesVsTarget.FieldName = "SalesVsTarget";
            this.colSalesVsTarget.Name = "colSalesVsTarget";
            this.colSalesVsTarget.OptionsColumn.AllowEdit = false;
            this.colSalesVsTarget.OptionsColumn.AllowFocus = false;
            this.colSalesVsTarget.Visible = true;
            this.colSalesVsTarget.VisibleIndex = 2;
            this.colSalesVsTarget.Width = 105;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "p";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colProfit
            // 
            this.colProfit.ColumnEdit = this.repositoryItemTextEdit3;
            this.colProfit.DisplayFormat.FormatString = "#,##0,,M";
            this.colProfit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 3;
            this.colProfit.Width = 204;
            // 
            // colCustomersSatisfaction
            // 
            this.colCustomersSatisfaction.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colCustomersSatisfaction.FieldName = "CustomersSatisfaction";
            this.colCustomersSatisfaction.Name = "colCustomersSatisfaction";
            this.colCustomersSatisfaction.OptionsColumn.AllowEdit = false;
            this.colCustomersSatisfaction.OptionsColumn.AllowFocus = false;
            this.colCustomersSatisfaction.Visible = true;
            this.colCustomersSatisfaction.VisibleIndex = 4;
            this.colCustomersSatisfaction.Width = 132;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // colMarketShare
            // 
            this.colMarketShare.ColumnEdit = this.repositoryItemTextEdit2;
            this.colMarketShare.FieldName = "MarketShare";
            this.colMarketShare.Name = "colMarketShare";
            this.colMarketShare.OptionsColumn.AllowEdit = false;
            this.colMarketShare.OptionsColumn.AllowFocus = false;
            this.colMarketShare.Visible = true;
            this.colMarketShare.VisibleIndex = 5;
            this.colMarketShare.Width = 134;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "p0";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3});
            this.gridControl1.Size = new System.Drawing.Size(921, 562);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colState,
            this.colSales,
            this.colProfit,
            this.colSalesVsTarget,
            this.colMarketShare,
            this.colCustomersSatisfaction});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            gridFormatRule1.Column = this.colSales;
            gridFormatRule1.Name = "Sales DataBar";
            formatConditionRuleDataBar1.MinimumType = DevExpress.XtraEditors.FormatConditionValueType.Number;
            formatConditionRuleDataBar1.PredefinedName = "Light Blue Gradient";
            gridFormatRule1.Rule = formatConditionRuleDataBar1;
            gridFormatRule2.Column = this.colSalesVsTarget;
            gridFormatRule2.Name = "SalesVsTargets IconSet";
            formatConditionIconSet1.CategoryName = "PositiveNegative";
            formatConditionIconSetIcon1.PredefinedName = "Triangles3_3.png";
            formatConditionIconSetIcon1.Value = new decimal(new int[] {
            -1,
            -1,
            -1,
            -2147483648});
            formatConditionIconSetIcon1.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon2.PredefinedName = "Triangles3_2.png";
            formatConditionIconSetIcon2.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon3.PredefinedName = "Triangles3_1.png";
            formatConditionIconSet1.Icons.Add(formatConditionIconSetIcon1);
            formatConditionIconSet1.Icons.Add(formatConditionIconSetIcon2);
            formatConditionIconSet1.Icons.Add(formatConditionIconSetIcon3);
            formatConditionIconSet1.Name = "PositiveNegativeTriangles";
            formatConditionIconSet1.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Number;
            formatConditionRuleIconSet1.IconSet = formatConditionIconSet1;
            gridFormatRule2.Rule = formatConditionRuleIconSet1;
            gridFormatRule3.Column = this.colProfit;
            gridFormatRule3.Name = "Profit DataBar";
            formatConditionRuleDataBar2.PredefinedName = "Mint Gradient";
            gridFormatRule3.Rule = formatConditionRuleDataBar2;
            gridFormatRule4.Column = this.colCustomersSatisfaction;
            gridFormatRule4.Name = "Satisfaction IconSet";
            formatConditionIconSet2.CategoryName = "Ratings";
            formatConditionIconSetIcon4.PredefinedName = "Stars3_1.png";
            formatConditionIconSetIcon4.Value = new decimal(new int[] {
            67,
            0,
            0,
            0});
            formatConditionIconSetIcon4.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon5.PredefinedName = "Stars3_2.png";
            formatConditionIconSetIcon5.Value = new decimal(new int[] {
            33,
            0,
            0,
            0});
            formatConditionIconSetIcon5.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon6.PredefinedName = "Stars3_3.png";
            formatConditionIconSetIcon6.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSet2.Icons.Add(formatConditionIconSetIcon4);
            formatConditionIconSet2.Icons.Add(formatConditionIconSetIcon5);
            formatConditionIconSet2.Icons.Add(formatConditionIconSetIcon6);
            formatConditionIconSet2.Name = "Stars3";
            formatConditionIconSet2.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleIconSet2.IconSet = formatConditionIconSet2;
            gridFormatRule4.Rule = formatConditionRuleIconSet2;
            gridFormatRule5.Column = this.colMarketShare;
            gridFormatRule5.Name = "Market IconSet";
            formatConditionIconSet3.CategoryName = "Directional";
            formatConditionIconSetIcon7.PredefinedName = "Arrows5_1.png";
            formatConditionIconSetIcon7.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            formatConditionIconSetIcon7.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon8.PredefinedName = "Arrows5_2.png";
            formatConditionIconSetIcon8.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            formatConditionIconSetIcon8.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon9.PredefinedName = "Arrows5_3.png";
            formatConditionIconSetIcon9.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            formatConditionIconSetIcon9.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon10.PredefinedName = "Arrows5_4.png";
            formatConditionIconSetIcon10.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            formatConditionIconSetIcon10.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon11.PredefinedName = "Arrows5_5.png";
            formatConditionIconSetIcon11.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSet3.Icons.Add(formatConditionIconSetIcon7);
            formatConditionIconSet3.Icons.Add(formatConditionIconSetIcon8);
            formatConditionIconSet3.Icons.Add(formatConditionIconSetIcon9);
            formatConditionIconSet3.Icons.Add(formatConditionIconSetIcon10);
            formatConditionIconSet3.Icons.Add(formatConditionIconSetIcon11);
            formatConditionIconSet3.Name = "Arrows5Colored";
            formatConditionIconSet3.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleIconSet3.IconSet = formatConditionIconSet3;
            gridFormatRule5.Rule = formatConditionRuleIconSet3;
            gridFormatRule6.Column = this.colSales;
            gridFormatRule6.Name = "Sales Top";
            formatConditionRuleTopBottom1.PredefinedName = "Bold Text";
            formatConditionRuleTopBottom1.Rank = new decimal(new int[] {
            5,
            0,
            0,
            0});
            gridFormatRule6.Rule = formatConditionRuleTopBottom1;
            gridFormatRule7.Column = this.colProfit;
            gridFormatRule7.Name = "Profit Top";
            formatConditionRuleTopBottom2.PredefinedName = "Bold Text";
            formatConditionRuleTopBottom2.Rank = new decimal(new int[] {
            5,
            0,
            0,
            0});
            gridFormatRule7.Rule = formatConditionRuleTopBottom2;
            gridFormatRule8.Column = this.colProfit;
            gridFormatRule8.Name = "Profit Positive";
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue1.PredefinedName = "Green Text";
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule8.Rule = formatConditionRuleValue1;
            gridFormatRule9.Column = this.colProfit;
            gridFormatRule9.Name = "Profit Negative";
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Less;
            formatConditionRuleValue2.PredefinedName = "Red Text";
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule9.Rule = formatConditionRuleValue2;
            gridFormatRule10.Column = this.colSalesVsTarget;
            gridFormatRule10.Name = "SalesVsTargets Positive";
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue3.PredefinedName = "Green Text";
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule10.Rule = formatConditionRuleValue3;
            gridFormatRule11.Column = this.colSalesVsTarget;
            gridFormatRule11.Name = "SalesVsTargets Negative";
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Less;
            formatConditionRuleValue4.PredefinedName = "Red Text";
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule11.Rule = formatConditionRuleValue4;
            gridFormatRule12.Column = this.colCustomersSatisfaction;
            gridFormatRule12.Name = "Satisfaction Bottom";
            formatConditionRuleTopBottom3.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleTopBottom3.Rank = new decimal(new int[] {
            30,
            0,
            0,
            0});
            formatConditionRuleTopBottom3.RankType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleTopBottom3.TopBottom = DevExpress.XtraEditors.FormatConditionTopBottomType.Bottom;
            gridFormatRule12.Rule = formatConditionRuleTopBottom3;
            gridFormatRule13.Column = this.colCustomersSatisfaction;
            gridFormatRule13.Name = "Satisfaction Less 3.5";
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Less;
            formatConditionRuleValue5.PredefinedName = "Red Text";
            formatConditionRuleValue5.Value1 = 3.5D;
            gridFormatRule13.Rule = formatConditionRuleValue5;
            gridFormatRule14.Column = this.colMarketShare;
            gridFormatRule14.Name = "Market Top";
            formatConditionRuleTopBottom4.PredefinedName = "Green Bold Text";
            formatConditionRuleTopBottom4.Rank = new decimal(new int[] {
            20,
            0,
            0,
            0});
            formatConditionRuleTopBottom4.RankType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            gridFormatRule14.Rule = formatConditionRuleTopBottom4;
            gridFormatRule15.Column = this.colMarketShare;
            gridFormatRule15.Name = "Market Bottom";
            formatConditionRuleTopBottom5.PredefinedName = "Red Bold Text";
            formatConditionRuleTopBottom5.Rank = new decimal(new int[] {
            20,
            0,
            0,
            0});
            formatConditionRuleTopBottom5.RankType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleTopBottom5.TopBottom = DevExpress.XtraEditors.FormatConditionTopBottomType.Bottom;
            gridFormatRule15.Rule = formatConditionRuleTopBottom5;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.FormatRules.Add(gridFormatRule2);
            this.gridView1.FormatRules.Add(gridFormatRule3);
            this.gridView1.FormatRules.Add(gridFormatRule4);
            this.gridView1.FormatRules.Add(gridFormatRule5);
            this.gridView1.FormatRules.Add(gridFormatRule6);
            this.gridView1.FormatRules.Add(gridFormatRule7);
            this.gridView1.FormatRules.Add(gridFormatRule8);
            this.gridView1.FormatRules.Add(gridFormatRule9);
            this.gridView1.FormatRules.Add(gridFormatRule10);
            this.gridView1.FormatRules.Add(gridFormatRule11);
            this.gridView1.FormatRules.Add(gridFormatRule12);
            this.gridView1.FormatRules.Add(gridFormatRule13);
            this.gridView1.FormatRules.Add(gridFormatRule14);
            this.gridView1.FormatRules.Add(gridFormatRule15);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colState, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // colState
            // 
            this.colState.FieldName = "State";
            this.colState.Name = "colState";
            this.colState.OptionsColumn.AllowEdit = false;
            this.colState.OptionsColumn.AllowFocus = false;
            this.colState.Visible = true;
            this.colState.VisibleIndex = 0;
            this.colState.Width = 151;
            // 
            // FormatRules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "FormatRules";
            this.Size = new System.Drawing.Size(921, 562);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GridControl gridControl1;
        private Views.Grid.GridView gridView1;
        private Columns.GridColumn colState;
        private Columns.GridColumn colSales;
        private Columns.GridColumn colProfit;
        private Columns.GridColumn colSalesVsTarget;
        private Columns.GridColumn colMarketShare;
        private Columns.GridColumn colCustomersSatisfaction;
        private XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
    }
}
