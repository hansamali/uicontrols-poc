﻿namespace DevExpress.XtraGrid.Demos {
    partial class ClipboardTutorial {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule7 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRule3ColorScale formatConditionRule3ColorScale2 = new DevExpress.XtraEditors.FormatConditionRule3ColorScale();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule8 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleTopBottom formatConditionRuleTopBottom2 = new DevExpress.XtraEditors.FormatConditionRuleTopBottom();
            this.colSalesVsTarget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMarketShare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSales = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomersSatisfaction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridControl1 = new DevExpress.XtraGrid.Demos.ClipboardGridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.propertyGridControl = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.simpleButtonCopyPaste = new DevExpress.XtraEditors.SimpleButton();
            this.richEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            this.spreadsheetControl1 = new DevExpress.XtraSpreadsheet.SpreadsheetControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.richEditLCI = new DevExpress.XtraLayout.LayoutControlItem();
            this.copyPasteButtonLCI = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridViewPropertiesLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItemProperties = new DevExpress.XtraLayout.SplitterItem();
            this.webBrowserLCI = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridControlLCI = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItemGrid = new DevExpress.XtraLayout.SplitterItem();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditLCI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyPasteButtonLCI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPropertiesLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItemProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webBrowserLCI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLCI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItemGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // colSalesVsTarget
            // 
            this.colSalesVsTarget.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSalesVsTarget.FieldName = "SalesVsTarget";
            this.colSalesVsTarget.Name = "colSalesVsTarget";
            this.colSalesVsTarget.OptionsColumn.AllowEdit = false;
            this.colSalesVsTarget.OptionsColumn.AllowFocus = false;
            this.colSalesVsTarget.Visible = true;
            this.colSalesVsTarget.VisibleIndex = 2;
            this.colSalesVsTarget.Width = 105;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "p";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colMarketShare
            // 
            this.colMarketShare.ColumnEdit = this.repositoryItemTextEdit2;
            this.colMarketShare.FieldName = "MarketShare";
            this.colMarketShare.Name = "colMarketShare";
            this.colMarketShare.OptionsColumn.AllowEdit = false;
            this.colMarketShare.OptionsColumn.AllowFocus = false;
            this.colMarketShare.Visible = true;
            this.colMarketShare.VisibleIndex = 5;
            this.colMarketShare.Width = 134;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "p0";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colProfit
            // 
            this.colProfit.ColumnEdit = this.repositoryItemTextEdit3;
            this.colProfit.DisplayFormat.FormatString = "#,##0,,M";
            this.colProfit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 3;
            this.colProfit.Width = 204;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "n";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colState
            // 
            this.colState.FieldName = "State";
            this.colState.Name = "colState";
            this.colState.OptionsColumn.AllowEdit = false;
            this.colState.OptionsColumn.AllowFocus = false;
            this.colState.Visible = true;
            this.colState.VisibleIndex = 0;
            this.colState.Width = 151;
            // 
            // colSales
            // 
            this.colSales.ColumnEdit = this.repositoryItemTextEdit3;
            this.colSales.DisplayFormat.FormatString = "#,##0,,M";
            this.colSales.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSales.FieldName = "Sales";
            this.colSales.Name = "colSales";
            this.colSales.OptionsColumn.AllowEdit = false;
            this.colSales.OptionsColumn.AllowFocus = false;
            this.colSales.Visible = true;
            this.colSales.VisibleIndex = 1;
            this.colSales.Width = 184;
            // 
            // colCustomersSatisfaction
            // 
            this.colCustomersSatisfaction.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colCustomersSatisfaction.FieldName = "CustomersSatisfaction";
            this.colCustomersSatisfaction.Name = "colCustomersSatisfaction";
            this.colCustomersSatisfaction.OptionsColumn.AllowEdit = false;
            this.colCustomersSatisfaction.OptionsColumn.AllowFocus = false;
            this.colCustomersSatisfaction.Visible = true;
            this.colCustomersSatisfaction.VisibleIndex = 4;
            this.colCustomersSatisfaction.Width = 132;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Location = new System.Drawing.Point(2, 28);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3});
            this.gridControl1.Size = new System.Drawing.Size(443, 532);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colState,
            this.colSales,
            this.colProfit,
            this.colSalesVsTarget,
            this.colMarketShare,
            this.colCustomersSatisfaction});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            gridFormatRule5.Column = this.colSalesVsTarget;
            gridFormatRule5.Name = "SalesFormat";
            formatConditionRuleValue3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Green;
            formatConditionRuleValue3.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue3.Value1 = "0";
            gridFormatRule5.Rule = formatConditionRuleValue3;
            gridFormatRule6.Column = this.colSalesVsTarget;
            gridFormatRule6.Name = "Sales vs Target Less then zero";
            formatConditionRuleValue4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Less;
            formatConditionRuleValue4.Value1 = "0";
            gridFormatRule6.Rule = formatConditionRuleValue4;
            gridFormatRule7.Column = this.colMarketShare;
            gridFormatRule7.Name = "MarketShare";
            formatConditionRule3ColorScale2.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            formatConditionRule3ColorScale2.MaximumColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            formatConditionRule3ColorScale2.Middle = new decimal(new int[] {
            66,
            0,
            0,
            0});
            formatConditionRule3ColorScale2.MiddleColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            formatConditionRule3ColorScale2.Minimum = new decimal(new int[] {
            33,
            0,
            0,
            0});
            formatConditionRule3ColorScale2.MinimumColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            gridFormatRule7.Rule = formatConditionRule3ColorScale2;
            gridFormatRule8.ApplyToRow = true;
            gridFormatRule8.Column = this.colProfit;
            gridFormatRule8.Name = "ProfitFormat";
            formatConditionRuleTopBottom2.PredefinedName = "Bold Text";
            formatConditionRuleTopBottom2.Rank = new decimal(new int[] {
            6,
            0,
            0,
            0});
            gridFormatRule8.Rule = formatConditionRuleTopBottom2;
            this.gridView1.FormatRules.Add(gridFormatRule5);
            this.gridView1.FormatRules.Add(gridFormatRule6);
            this.gridView1.FormatRules.Add(gridFormatRule7);
            this.gridView1.FormatRules.Add(gridFormatRule8);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsClipboard.AllowHtmlFormat = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.gridView1.OptionsClipboard.CopyCollapsedData = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMarketShare, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.propertyGridControl);
            this.layoutControl1.Controls.Add(this.simpleButtonCopyPaste);
            this.layoutControl1.Controls.Add(this.richEditControl);
            this.layoutControl1.Controls.Add(this.spreadsheetControl1);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(267, 62, 925, 789);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(921, 562);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            this.layoutControl1.GroupExpandChanged += new DevExpress.XtraLayout.Utils.LayoutGroupEventHandler(this.layoutControl1_GroupExpandChanged);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.webBrowser1);
            this.panelControl1.Location = new System.Drawing.Point(461, 392);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(417, 168);
            this.panelControl1.TabIndex = 19;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(2, 2);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(413, 164);
            this.webBrowser1.TabIndex = 13;
            // 
            // propertyGridControl
            // 
            this.propertyGridControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.propertyGridControl.Location = new System.Drawing.Point(894, 2);
            this.propertyGridControl.MinimumSize = new System.Drawing.Size(300, 0);
            this.propertyGridControl.Name = "propertyGridControl";
            this.propertyGridControl.OptionsView.ShowRootCategories = false;
            this.propertyGridControl.Size = new System.Drawing.Size(343, 553);
            this.propertyGridControl.TabIndex = 17;
            // 
            // simpleButtonCopyPaste
            // 
            this.simpleButtonCopyPaste.Location = new System.Drawing.Point(2, 2);
            this.simpleButtonCopyPaste.Name = "simpleButtonCopyPaste";
            this.simpleButtonCopyPaste.Size = new System.Drawing.Size(443, 22);
            this.simpleButtonCopyPaste.StyleController = this.layoutControl1;
            this.simpleButtonCopyPaste.TabIndex = 15;
            this.simpleButtonCopyPaste.Text = "Copy and Paste";
            this.simpleButtonCopyPaste.Click += new System.EventHandler(this.simpleButtonCopyPaste_Click);
            // 
            // richEditControl
            // 
            this.richEditControl.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Draft;
            this.richEditControl.EnableToolTips = true;
            this.richEditControl.Location = new System.Drawing.Point(461, 205);
            this.richEditControl.Name = "richEditControl";
            this.richEditControl.Options.Bookmarks.ConflictNameResolution = DevExpress.XtraRichEdit.ConflictNameAction.Keep;
            this.richEditControl.Options.Export.PlainText.ExportFinalParagraphMark = DevExpress.XtraRichEdit.Export.PlainText.ExportFinalParagraphMark.Never;
            this.richEditControl.Options.Fields.UpdateFieldsInTextBoxes = false;
            this.richEditControl.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditControl.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditControl.Size = new System.Drawing.Size(417, 167);
            this.richEditControl.TabIndex = 12;
            this.richEditControl.Views.DraftView.Padding = new System.Windows.Forms.Padding(17, 16, 0, 0);
            // 
            // spreadsheetControl1
            // 
            this.spreadsheetControl1.Location = new System.Drawing.Point(461, 18);
            this.spreadsheetControl1.Name = "spreadsheetControl1";
            this.spreadsheetControl1.Options.TabSelector.Visibility = DevExpress.XtraSpreadsheet.SpreadsheetElementVisibility.Hidden;
            this.spreadsheetControl1.Options.View.Charts.Antialiasing = DevExpress.XtraSpreadsheet.DocumentCapability.Enabled;
            this.spreadsheetControl1.Options.View.Charts.TextAntialiasing = DevExpress.XtraSpreadsheet.DocumentCapability.Enabled;
            this.spreadsheetControl1.Options.View.ShowColumnHeaders = false;
            this.spreadsheetControl1.Options.View.ShowPrintArea = false;
            this.spreadsheetControl1.Options.View.ShowRowHeaders = false;
            this.spreadsheetControl1.Size = new System.Drawing.Size(417, 167);
            this.spreadsheetControl1.TabIndex = 4;
            this.spreadsheetControl1.Text = "spreadsheetControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.richEditLCI,
            this.copyPasteButtonLCI,
            this.gridViewPropertiesLayoutGroup,
            this.splitterItemProperties,
            this.webBrowserLCI,
            this.gridControlLCI,
            this.splitterItemGrid});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(921, 562);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.spreadsheetControl1;
            this.layoutControlItem2.Location = new System.Drawing.Point(459, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(421, 187);
            this.layoutControlItem2.Text = "Excel format";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(78, 13);
            // 
            // richEditLCI
            // 
            this.richEditLCI.Control = this.richEditControl;
            this.richEditLCI.Location = new System.Drawing.Point(459, 187);
            this.richEditLCI.Name = "richEditLCI";
            this.richEditLCI.Size = new System.Drawing.Size(421, 187);
            this.richEditLCI.Text = "Rich text format";
            this.richEditLCI.TextLocation = DevExpress.Utils.Locations.Top;
            this.richEditLCI.TextSize = new System.Drawing.Size(78, 13);
            // 
            // copyPasteButtonLCI
            // 
            this.copyPasteButtonLCI.Control = this.simpleButtonCopyPaste;
            this.copyPasteButtonLCI.Location = new System.Drawing.Point(0, 0);
            this.copyPasteButtonLCI.Name = "copyPasteButtonLCI";
            this.copyPasteButtonLCI.Size = new System.Drawing.Size(447, 26);
            this.copyPasteButtonLCI.TextSize = new System.Drawing.Size(0, 0);
            this.copyPasteButtonLCI.TextVisible = false;
            // 
            // gridViewPropertiesLayoutGroup
            // 
            this.gridViewPropertiesLayoutGroup.ExpandButtonVisible = true;
            this.gridViewPropertiesLayoutGroup.Expanded = false;
            this.gridViewPropertiesLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8});
            this.gridViewPropertiesLayoutGroup.Location = new System.Drawing.Point(892, 0);
            this.gridViewPropertiesLayoutGroup.Name = "gridViewPropertiesLayoutGroup";
            this.gridViewPropertiesLayoutGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.gridViewPropertiesLayoutGroup.Size = new System.Drawing.Size(29, 562);
            this.gridViewPropertiesLayoutGroup.Text = "Clipboard Options";
            this.gridViewPropertiesLayoutGroup.TextLocation = DevExpress.Utils.Locations.Right;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.propertyGridControl;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(347, 557);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // splitterItemProperties
            // 
            this.splitterItemProperties.AllowHotTrack = true;
            this.splitterItemProperties.Location = new System.Drawing.Point(880, 0);
            this.splitterItemProperties.Name = "splitterItemProperties";
            this.splitterItemProperties.Size = new System.Drawing.Size(12, 562);
            this.splitterItemProperties.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // webBrowserLCI
            // 
            this.webBrowserLCI.Control = this.panelControl1;
            this.webBrowserLCI.Location = new System.Drawing.Point(459, 374);
            this.webBrowserLCI.Name = "webBrowserLCI";
            this.webBrowserLCI.Size = new System.Drawing.Size(421, 188);
            this.webBrowserLCI.Text = "HTML format";
            this.webBrowserLCI.TextLocation = DevExpress.Utils.Locations.Top;
            this.webBrowserLCI.TextSize = new System.Drawing.Size(78, 13);
            // 
            // gridControlLCI
            // 
            this.gridControlLCI.Control = this.gridControl1;
            this.gridControlLCI.Location = new System.Drawing.Point(0, 26);
            this.gridControlLCI.Name = "gridControlLCI";
            this.gridControlLCI.Size = new System.Drawing.Size(447, 536);
            this.gridControlLCI.TextSize = new System.Drawing.Size(0, 0);
            this.gridControlLCI.TextVisible = false;
            // 
            // splitterItemGrid
            // 
            this.splitterItemGrid.AllowHotTrack = true;
            this.splitterItemGrid.Location = new System.Drawing.Point(447, 0);
            this.splitterItemGrid.Name = "splitterItemGrid";
            this.splitterItemGrid.Size = new System.Drawing.Size(12, 562);
            // 
            // ClipboardTutorial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "ClipboardTutorial";
            this.Size = new System.Drawing.Size(921, 562);
            this.Load += new System.EventHandler(this.ClipboardTutorial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditLCI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyPasteButtonLCI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPropertiesLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItemProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webBrowserLCI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLCI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItemGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ClipboardGridControl gridControl1;
        private Views.Grid.GridView gridView1;
        private Columns.GridColumn colState;
        private Columns.GridColumn colSales;
        private Columns.GridColumn colProfit;
        private Columns.GridColumn colSalesVsTarget;
        private Columns.GridColumn colMarketShare;
        private Columns.GridColumn colCustomersSatisfaction;
        private XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private XtraLayout.LayoutControl layoutControl1;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private XtraLayout.LayoutControlItem gridControlLCI;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private XtraRichEdit.RichEditControl richEditControl;
        private XtraSpreadsheet.SpreadsheetControl spreadsheetControl1;
        private XtraLayout.LayoutControlItem layoutControlItem2;
        private XtraLayout.LayoutControlItem richEditLCI;
        private XtraLayout.SplitterItem splitterItemGrid;
        private XtraEditors.SimpleButton simpleButtonCopyPaste;
        private XtraLayout.LayoutControlItem copyPasteButtonLCI;
        private XtraVerticalGrid.PropertyGridControl propertyGridControl;
        private XtraLayout.LayoutControlGroup gridViewPropertiesLayoutGroup;
        private XtraLayout.LayoutControlItem layoutControlItem8;
        private XtraLayout.SplitterItem splitterItemProperties;
        private XtraEditors.PanelControl panelControl1;
        private XtraLayout.LayoutControlItem webBrowserLCI;
    }
}
