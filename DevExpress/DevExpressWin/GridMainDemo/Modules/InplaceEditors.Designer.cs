namespace DevExpress.XtraGrid.Demos {
    partial class InplaceEditors {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InplaceEditors));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridEditorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridEditorValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemSpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTimeEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.repositoryItemComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemImageComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imcSmall = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemMRUEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMRUEdit();
            this.repositoryItemCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPictureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemRadioGroup = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemPopupContainerEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControl = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridPopup = new DevExpress.XtraGrid.GridControl();
            this.dataTableLookUp = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.gridView1Popup = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colclnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colclnDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageEdit = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemMemoExEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemProgressBar = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCalcEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.repositoryItemColorEdit = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.repositoryItemFontEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.repositoryItemRangeTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemRangeTrackBar();
            this.repositoryItemCheckedComboBoxEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.dataSet1 = new System.Data.DataSet();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitter1 = new DevExpress.XtraEditors.SplitterControl();
            this.gcProperties = new DevExpress.XtraEditors.GroupControl();
            this.xtraPropertyGrid1 = new DevExpress.DXperience.Demos.XtraPropertyGrid();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imcSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMRUEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl)).BeginInit();
            this.popupContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1Popup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRangeTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcProperties)).BeginInit();
            this.gcProperties.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit,
            this.repositoryItemButtonEdit,
            this.repositoryItemSpinEdit,
            this.repositoryItemTimeEdit,
            this.repositoryItemComboBox,
            this.repositoryItemImageComboBox,
            this.repositoryItemMRUEdit,
            this.repositoryItemCheckEdit,
            this.repositoryItemPictureEdit,
            this.repositoryItemMemoEdit,
            this.repositoryItemRadioGroup,
            this.repositoryItemHyperLinkEdit,
            this.repositoryItemPopupContainerEdit,
            this.repositoryItemImageEdit,
            this.repositoryItemMemoExEdit,
            this.repositoryItemProgressBar,
            this.repositoryItemDateEdit,
            this.repositoryItemCalcEdit,
            this.repositoryItemColorEdit,
            this.repositoryItemLookUpEdit,
            this.repositoryItemTrackBar1,
            this.repositoryItemFontEdit1,
            this.repositoryItemRangeTrackBar1,
            this.repositoryItemCheckedComboBoxEdit1});
            this.gridControl1.Size = new System.Drawing.Size(593, 428);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridEditorName,
            this.gridEditorValue});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // gridEditorName
            // 
            this.gridEditorName.Caption = "Editor Name";
            this.gridEditorName.FieldName = "Name";
            this.gridEditorName.Name = "gridEditorName";
            this.gridEditorName.OptionsColumn.AllowEdit = false;
            this.gridEditorName.OptionsColumn.AllowFocus = false;
            this.gridEditorName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridEditorName.OptionsColumn.ReadOnly = true;
            this.gridEditorName.OptionsFilter.AllowAutoFilter = false;
            this.gridEditorName.OptionsFilter.AllowFilter = false;
            this.gridEditorName.Visible = true;
            this.gridEditorName.VisibleIndex = 0;
            this.gridEditorName.Width = 150;
            // 
            // gridEditorValue
            // 
            this.gridEditorValue.Caption = "Value";
            this.gridEditorValue.FieldName = "Value";
            this.gridEditorValue.Name = "gridEditorValue";
            this.gridEditorValue.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridEditorValue.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridEditorValue.OptionsFilter.AllowAutoFilter = false;
            this.gridEditorValue.OptionsFilter.AllowFilter = false;
            this.gridEditorValue.Visible = true;
            this.gridEditorValue.VisibleIndex = 1;
            this.gridEditorValue.Width = 234;
            // 
            // repositoryItemTextEdit
            // 
            this.repositoryItemTextEdit.AutoHeight = false;
            this.repositoryItemTextEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemTextEdit.Name = "repositoryItemTextEdit";
            // 
            // repositoryItemButtonEdit
            // 
            this.repositoryItemButtonEdit.AutoHeight = false;
            this.repositoryItemButtonEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit.Name = "repositoryItemButtonEdit";
            this.repositoryItemButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit_ButtonClick);
            // 
            // repositoryItemSpinEdit
            // 
            this.repositoryItemSpinEdit.AutoHeight = false;
            this.repositoryItemSpinEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemSpinEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit.Name = "repositoryItemSpinEdit";
            // 
            // repositoryItemTimeEdit
            // 
            this.repositoryItemTimeEdit.AutoHeight = false;
            this.repositoryItemTimeEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemTimeEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit.Name = "repositoryItemTimeEdit";
            // 
            // repositoryItemComboBox
            // 
            this.repositoryItemComboBox.AutoHeight = false;
            this.repositoryItemComboBox.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox.DropDownRows = 15;
            this.repositoryItemComboBox.Items.AddRange(new object[] {
            "Afghanistan",
            "Albania",
            "Algeria",
            "American Samoa",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antarctica",
            "Antigua and Barbuda",
            "Argentina",
            "Armenia",
            "Aruba",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia and Herzegovina",
            "Botswana",
            "Bouvet Island",
            "Brazil",
            "British Indian Ocean Territories",
            "Brunei Darussalam",
            "Bulgaria",
            "Burkina Faso",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China, People\'\'s Republic of",
            "Christmas Island",
            "Cocos Islands",
            "Columbia",
            "Comoros",
            "Congo",
            "Cook Islands",
            "Costa Rica",
            "Cote D\'\'ivoire",
            "Croatia",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "East Timor",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Falkland Islands",
            "Faroe Islands",
            "Fiji",
            "Finland",
            "France",
            "France, Metropolitan",
            "French Guiana",
            "French Polynesia",
            "French Southern Territories",
            "FYROM",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Gibraltar",
            "Greece",
            "Greenland",
            "Grenada",
            "Guadeloupe",
            "Guam",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Heard Island And Mcdonald Islands",
            "Honduras",
            "Hong Kong",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Ireland",
            "Israel",
            "Italy",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kiribati",
            "Korea, Democratic Peoples Republic of",
            "Korea, Republic of",
            "Kuwait",
            "Kyrgyzstan",
            "Lao Peoples Democratic Republic",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libyan Arab Jamahiriya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Macau",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Micronesia",
            "Moldova",
            "Monaco",
            "Mongolia",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Myanmar",
            "Namibia",
            "Nauru",
            "Nepal",
            "Netherlands",
            "Netherlands Antilles",
            "New Caledonia",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Niue",
            "Norfolk Island",
            "Northern Mariana Islands",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Pitcairn",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Reunion",
            "Romania",
            "Russian Federation",
            "Rwanda",
            "Saint Helena",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Pierre and Miquelon",
            "Saint Vincent and The Grenadines",
            "Samoa",
            "San Marino",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "South Georgia and Sandwich Islands",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Suriname",
            "Svalbard and JanMayen",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syrian Arab Republic",
            "Taiwan",
            "Tajikistan",
            "Tanzania",
            "Thailand",
            "Togo",
            "Tokelau",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks and Caicos Islands",
            "Tuvalu",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United Kingdom",
            "United States",
            "Uruguay",
            "Uzbekistan",
            "Vanuatu",
            "Vatican City State",
            "Venezuela",
            "Vietnam",
            "Virgin Islands (British)",
            "Virgin Islands (U.S.)",
            "Wallis And Futuna Islands",
            "Western Sahara",
            "Yemen",
            "Yugoslavia",
            "Zaire",
            "Zambia",
            "Zimbabwe",
            "Guernsey",
            "Malaga",
            "Denmark",
            "Jersey"});
            this.repositoryItemComboBox.Name = "repositoryItemComboBox";
            // 
            // repositoryItemImageComboBox
            // 
            this.repositoryItemImageComboBox.AutoHeight = false;
            this.repositoryItemImageComboBox.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemImageComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox.DropDownRows = 12;
            this.repositoryItemImageComboBox.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Time", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Design", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mail", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pie", 3, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Home", 4, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tag", 5, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Contact", 6, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Database", 7, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Task", 8, 8),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Video", 9, 9)});
            this.repositoryItemImageComboBox.Name = "repositoryItemImageComboBox";
            this.repositoryItemImageComboBox.SmallImages = this.imcSmall;
            // 
            // imcSmall
            // 
            this.imcSmall.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imcSmall.ImageStream")));
            this.imcSmall.InsertGalleryImage("time_16x16.png", "images/scheduling/time_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/time_16x16.png"), 0);
            this.imcSmall.Images.SetKeyName(0, "time_16x16.png");
            this.imcSmall.InsertGalleryImage("design_16x16.png", "images/miscellaneous/design_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/design_16x16.png"), 1);
            this.imcSmall.Images.SetKeyName(1, "design_16x16.png");
            this.imcSmall.InsertGalleryImage("mail_16x16.png", "images/mail/mail_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/mail_16x16.png"), 2);
            this.imcSmall.Images.SetKeyName(2, "mail_16x16.png");
            this.imcSmall.InsertGalleryImage("pie_16x16.png", "images/chart/pie_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/chart/pie_16x16.png"), 3);
            this.imcSmall.Images.SetKeyName(3, "pie_16x16.png");
            this.imcSmall.InsertGalleryImage("home_16x16.png", "images/navigation/home_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/home_16x16.png"), 4);
            this.imcSmall.Images.SetKeyName(4, "home_16x16.png");
            this.imcSmall.InsertGalleryImage("tag_16x16.png", "images/programming/tag_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/programming/tag_16x16.png"), 5);
            this.imcSmall.Images.SetKeyName(5, "tag_16x16.png");
            this.imcSmall.InsertGalleryImage("contact_16x16.png", "images/mail/contact_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/contact_16x16.png"), 6);
            this.imcSmall.Images.SetKeyName(6, "contact_16x16.png");
            this.imcSmall.InsertGalleryImage("database_16x16.png", "images/data/database_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/data/database_16x16.png"), 7);
            this.imcSmall.Images.SetKeyName(7, "database_16x16.png");
            this.imcSmall.InsertGalleryImage("task_16x16.png", "images/tasks/task_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/tasks/task_16x16.png"), 8);
            this.imcSmall.Images.SetKeyName(8, "task_16x16.png");
            this.imcSmall.InsertGalleryImage("video_16x16.png", "images/miscellaneous/video_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/video_16x16.png"), 9);
            this.imcSmall.Images.SetKeyName(9, "video_16x16.png");
            // 
            // repositoryItemMRUEdit
            // 
            this.repositoryItemMRUEdit.AutoHeight = false;
            this.repositoryItemMRUEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemMRUEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemMRUEdit.Items.AddRange(new object[] {
            "White",
            "Black",
            "Blue",
            "Green"});
            this.repositoryItemMRUEdit.Name = "repositoryItemMRUEdit";
            this.repositoryItemMRUEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemMRUEdit_ButtonClick);
            // 
            // repositoryItemCheckEdit
            // 
            this.repositoryItemCheckEdit.AutoHeight = false;
            this.repositoryItemCheckEdit.Caption = "Check";
            this.repositoryItemCheckEdit.Name = "repositoryItemCheckEdit";
            // 
            // repositoryItemPictureEdit
            // 
            this.repositoryItemPictureEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemPictureEdit.Name = "repositoryItemPictureEdit";
            // 
            // repositoryItemMemoEdit
            // 
            this.repositoryItemMemoEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemMemoEdit.Name = "repositoryItemMemoEdit";
            // 
            // repositoryItemRadioGroup
            // 
            this.repositoryItemRadioGroup.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemRadioGroup.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Male"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Female")});
            this.repositoryItemRadioGroup.Name = "repositoryItemRadioGroup";
            // 
            // repositoryItemHyperLinkEdit
            // 
            this.repositoryItemHyperLinkEdit.AutoHeight = false;
            this.repositoryItemHyperLinkEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemHyperLinkEdit.Name = "repositoryItemHyperLinkEdit";
            // 
            // repositoryItemPopupContainerEdit
            // 
            this.repositoryItemPopupContainerEdit.AutoHeight = false;
            this.repositoryItemPopupContainerEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemPopupContainerEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit.CloseOnOuterMouseClick = false;
            this.repositoryItemPopupContainerEdit.Name = "repositoryItemPopupContainerEdit";
            this.repositoryItemPopupContainerEdit.PopupControl = this.popupContainerControl;
            this.repositoryItemPopupContainerEdit.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit_QueryResultValue);
            this.repositoryItemPopupContainerEdit.QueryDisplayText += new DevExpress.XtraEditors.Controls.QueryDisplayTextEventHandler(this.repositoryItemPopupContainerEdit_QueryDisplayText);
            this.repositoryItemPopupContainerEdit.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.repositoryItemPopupContainerEdit_QueryPopUp);
            // 
            // popupContainerControl
            // 
            this.popupContainerControl.Controls.Add(this.gridPopup);
            this.popupContainerControl.Location = new System.Drawing.Point(48, 24);
            this.popupContainerControl.Name = "popupContainerControl";
            this.popupContainerControl.Size = new System.Drawing.Size(344, 256);
            this.popupContainerControl.TabIndex = 4;
            // 
            // gridPopup
            // 
            this.gridPopup.DataSource = this.dataTableLookUp;
            this.gridPopup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPopup.Location = new System.Drawing.Point(0, 0);
            this.gridPopup.MainView = this.gridView1Popup;
            this.gridPopup.Name = "gridPopup";
            this.gridPopup.Size = new System.Drawing.Size(344, 256);
            this.gridPopup.TabIndex = 1;
            this.gridPopup.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1Popup});
            this.gridPopup.BindingContextChanged += new System.EventHandler(this.gridPopup_BindingContextChanged);
            // 
            // dataTableLookUp
            // 
            this.dataTableLookUp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3});
            this.dataTableLookUp.TableName = "TableLookUp";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Id";
            this.dataColumn1.ColumnName = "clnId";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Name";
            this.dataColumn2.ColumnName = "clnName";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "Department";
            this.dataColumn3.ColumnName = "clnDepartment";
            // 
            // gridView1Popup
            // 
            this.gridView1Popup.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colclnName,
            this.colclnDepartment});
            this.gridView1Popup.GridControl = this.gridPopup;
            this.gridView1Popup.GroupCount = 1;
            this.gridView1Popup.Name = "gridView1Popup";
            this.gridView1Popup.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView1Popup.OptionsBehavior.Editable = false;
            this.gridView1Popup.OptionsSelection.InvertSelection = true;
            this.gridView1Popup.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colclnDepartment, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1Popup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView1Popup_KeyDown);
            this.gridView1Popup.DoubleClick += new System.EventHandler(this.gridView1Popup_DoubleClick);
            this.gridView1Popup.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // colclnName
            // 
            this.colclnName.Caption = "Name";
            this.colclnName.FieldName = "clnName";
            this.colclnName.Name = "colclnName";
            this.colclnName.Visible = true;
            this.colclnName.VisibleIndex = 0;
            // 
            // colclnDepartment
            // 
            this.colclnDepartment.Caption = "Department";
            this.colclnDepartment.FieldName = "clnDepartment";
            this.colclnDepartment.Name = "colclnDepartment";
            // 
            // repositoryItemImageEdit
            // 
            this.repositoryItemImageEdit.AutoHeight = false;
            this.repositoryItemImageEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemImageEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit.Name = "repositoryItemImageEdit";
            this.repositoryItemImageEdit.PopupFormSize = new System.Drawing.Size(250, 150);
            // 
            // repositoryItemMemoExEdit
            // 
            this.repositoryItemMemoExEdit.AutoHeight = false;
            this.repositoryItemMemoExEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemMemoExEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit.Name = "repositoryItemMemoExEdit";
            this.repositoryItemMemoExEdit.PopupFormSize = new System.Drawing.Size(250, 150);
            // 
            // repositoryItemProgressBar
            // 
            this.repositoryItemProgressBar.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemProgressBar.Name = "repositoryItemProgressBar";
            // 
            // repositoryItemDateEdit
            // 
            this.repositoryItemDateEdit.AutoHeight = false;
            this.repositoryItemDateEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit.Name = "repositoryItemDateEdit";
            // 
            // repositoryItemCalcEdit
            // 
            this.repositoryItemCalcEdit.AutoHeight = false;
            this.repositoryItemCalcEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemCalcEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit.Name = "repositoryItemCalcEdit";
            // 
            // repositoryItemColorEdit
            // 
            this.repositoryItemColorEdit.AutoHeight = false;
            this.repositoryItemColorEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemColorEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit.Name = "repositoryItemColorEdit";
            // 
            // repositoryItemLookUpEdit
            // 
            this.repositoryItemLookUpEdit.AutoHeight = false;
            this.repositoryItemLookUpEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("clnName", "Name", 160, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("clnDepartment", "Department", 140, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpEdit.DataSource = this.dataTableLookUp;
            this.repositoryItemLookUpEdit.DisplayMember = "clnName";
            this.repositoryItemLookUpEdit.Name = "repositoryItemLookUpEdit";
            this.repositoryItemLookUpEdit.PopupWidth = 290;
            this.repositoryItemLookUpEdit.ValueMember = "clnId";
            // 
            // repositoryItemTrackBar1
            // 
            this.repositoryItemTrackBar1.LabelAppearance.Options.UseTextOptions = true;
            this.repositoryItemTrackBar1.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTrackBar1.Name = "repositoryItemTrackBar1";
            // 
            // repositoryItemFontEdit1
            // 
            this.repositoryItemFontEdit1.AutoHeight = false;
            this.repositoryItemFontEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemFontEdit1.Name = "repositoryItemFontEdit1";
            // 
            // repositoryItemRangeTrackBar1
            // 
            this.repositoryItemRangeTrackBar1.LabelAppearance.Options.UseTextOptions = true;
            this.repositoryItemRangeTrackBar1.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemRangeTrackBar1.Name = "repositoryItemRangeTrackBar1";
            // 
            // repositoryItemCheckedComboBoxEdit1
            // 
            this.repositoryItemCheckedComboBoxEdit1.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit1.Name = "repositoryItemCheckedComboBoxEdit1";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableLookUp});
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(304, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(176, 224);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(593, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 428);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // gcProperties
            // 
            this.gcProperties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.gcProperties.Appearance.Options.UseBackColor = true;
            this.gcProperties.Controls.Add(this.xtraPropertyGrid1);
            this.gcProperties.Dock = System.Windows.Forms.DockStyle.Right;
            this.gcProperties.Location = new System.Drawing.Point(598, 0);
            this.gcProperties.Name = "gcProperties";
            this.gcProperties.Padding = new System.Windows.Forms.Padding(1);
            this.gcProperties.Size = new System.Drawing.Size(405, 428);
            this.gcProperties.TabIndex = 7;
            this.gcProperties.Text = "Properties:";
            // 
            // xtraPropertyGrid1
            // 
            this.xtraPropertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraPropertyGrid1.Location = new System.Drawing.Point(3, 22);
            this.xtraPropertyGrid1.Name = "xtraPropertyGrid1";
            this.xtraPropertyGrid1.Size = new System.Drawing.Size(399, 403);
            this.xtraPropertyGrid1.TabIndex = 0;
            // 
            // InplaceEditors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.popupContainerControl);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.gcProperties);
            this.Name = "InplaceEditors";
            this.Size = new System.Drawing.Size(1003, 428);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imcSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMRUEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl)).EndInit();
            this.popupContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1Popup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRangeTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcProperties)).EndInit();
            this.gcProperties.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Columns.GridColumn gridEditorName;
        private DevExpress.XtraGrid.Columns.GridColumn gridEditorValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemMRUEdit repositoryItemMRUEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit;
        private System.ComponentModel.IContainer components = null;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dataTableLookUp;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colclnName;
        private DevExpress.XtraGrid.Columns.GridColumn colclnDepartment;
        private DevExpress.XtraGrid.GridControl gridPopup;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1Popup;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl;
        private DevExpress.XtraEditors.SplitterControl splitter1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl gcProperties;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar repositoryItemTrackBar1;
        private DevExpress.DXperience.Demos.XtraPropertyGrid xtraPropertyGrid1;
        private DevExpress.XtraEditors.Repository.RepositoryItemFontEdit repositoryItemFontEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRangeTrackBar repositoryItemRangeTrackBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit1;
        private Utils.ImageCollection imcSmall;
    }
}
