﻿namespace DevExpress.XtraGrid.Demos {
    partial class EditFormOptions {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.icbShowUpdateCancelPanel = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbShowOnF2Key = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbShowOnEnterKey = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbShowOnDoubleClick = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbBindingMode = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbActionOnModifiedRowChange = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.sePopupEditFormWidth = new DevExpress.XtraEditors.SpinEdit();
            this.teFormCaptionFormat = new DevExpress.XtraEditors.TextEdit();
            this.icbEditingMode = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciOK = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbShowUpdateCancelPanel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbShowOnF2Key.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbShowOnEnterKey.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbShowOnDoubleClick.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbBindingMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbActionOnModifiedRowChange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sePopupEditFormWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFormCaptionFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbEditingMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.icbShowUpdateCancelPanel);
            this.layoutControl1.Controls.Add(this.icbShowOnF2Key);
            this.layoutControl1.Controls.Add(this.icbShowOnEnterKey);
            this.layoutControl1.Controls.Add(this.icbShowOnDoubleClick);
            this.layoutControl1.Controls.Add(this.icbBindingMode);
            this.layoutControl1.Controls.Add(this.icbActionOnModifiedRowChange);
            this.layoutControl1.Controls.Add(this.sePopupEditFormWidth);
            this.layoutControl1.Controls.Add(this.teFormCaptionFormat);
            this.layoutControl1.Controls.Add(this.icbEditingMode);
            this.layoutControl1.Controls.Add(this.simpleButton2);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(10, 10);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(396, 176, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(368, 298);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // icbShowUpdateCancelPanel
            // 
            this.icbShowUpdateCancelPanel.Location = new System.Drawing.Point(163, 216);
            this.icbShowUpdateCancelPanel.Name = "icbShowUpdateCancelPanel";
            this.icbShowUpdateCancelPanel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbShowUpdateCancelPanel.Size = new System.Drawing.Size(203, 20);
            this.icbShowUpdateCancelPanel.StyleController = this.layoutControl1;
            this.icbShowUpdateCancelPanel.TabIndex = 15;
            // 
            // icbShowOnF2Key
            // 
            this.icbShowOnF2Key.Location = new System.Drawing.Point(163, 192);
            this.icbShowOnF2Key.Name = "icbShowOnF2Key";
            this.icbShowOnF2Key.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbShowOnF2Key.Size = new System.Drawing.Size(203, 20);
            this.icbShowOnF2Key.StyleController = this.layoutControl1;
            this.icbShowOnF2Key.TabIndex = 14;
            // 
            // icbShowOnEnterKey
            // 
            this.icbShowOnEnterKey.Location = new System.Drawing.Point(163, 168);
            this.icbShowOnEnterKey.Name = "icbShowOnEnterKey";
            this.icbShowOnEnterKey.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbShowOnEnterKey.Size = new System.Drawing.Size(203, 20);
            this.icbShowOnEnterKey.StyleController = this.layoutControl1;
            this.icbShowOnEnterKey.TabIndex = 13;
            // 
            // icbShowOnDoubleClick
            // 
            this.icbShowOnDoubleClick.Location = new System.Drawing.Point(163, 144);
            this.icbShowOnDoubleClick.Name = "icbShowOnDoubleClick";
            this.icbShowOnDoubleClick.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbShowOnDoubleClick.Size = new System.Drawing.Size(203, 20);
            this.icbShowOnDoubleClick.StyleController = this.layoutControl1;
            this.icbShowOnDoubleClick.TabIndex = 12;
            // 
            // icbBindingMode
            // 
            this.icbBindingMode.Location = new System.Drawing.Point(163, 120);
            this.icbBindingMode.Name = "icbBindingMode";
            this.icbBindingMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbBindingMode.Size = new System.Drawing.Size(203, 20);
            this.icbBindingMode.StyleController = this.layoutControl1;
            this.icbBindingMode.TabIndex = 11;
            // 
            // icbActionOnModifiedRowChange
            // 
            this.icbActionOnModifiedRowChange.Location = new System.Drawing.Point(163, 96);
            this.icbActionOnModifiedRowChange.Name = "icbActionOnModifiedRowChange";
            this.icbActionOnModifiedRowChange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbActionOnModifiedRowChange.Size = new System.Drawing.Size(203, 20);
            this.icbActionOnModifiedRowChange.StyleController = this.layoutControl1;
            this.icbActionOnModifiedRowChange.TabIndex = 10;
            // 
            // sePopupEditFormWidth
            // 
            this.sePopupEditFormWidth.EditValue = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.sePopupEditFormWidth.Location = new System.Drawing.Point(163, 72);
            this.sePopupEditFormWidth.Name = "sePopupEditFormWidth";
            this.sePopupEditFormWidth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sePopupEditFormWidth.Properties.IsFloatValue = false;
            this.sePopupEditFormWidth.Properties.Mask.EditMask = "N00";
            this.sePopupEditFormWidth.Properties.MaxValue = new decimal(new int[] {
            1200,
            0,
            0,
            0});
            this.sePopupEditFormWidth.Properties.MinValue = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.sePopupEditFormWidth.Size = new System.Drawing.Size(203, 20);
            this.sePopupEditFormWidth.StyleController = this.layoutControl1;
            this.sePopupEditFormWidth.TabIndex = 9;
            // 
            // teFormCaptionFormat
            // 
            this.teFormCaptionFormat.Location = new System.Drawing.Point(163, 48);
            this.teFormCaptionFormat.Name = "teFormCaptionFormat";
            this.teFormCaptionFormat.Size = new System.Drawing.Size(203, 20);
            this.teFormCaptionFormat.StyleController = this.layoutControl1;
            this.teFormCaptionFormat.TabIndex = 8;
            // 
            // icbEditingMode
            // 
            this.icbEditingMode.Location = new System.Drawing.Point(163, 2);
            this.icbEditingMode.Name = "icbEditingMode";
            this.icbEditingMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbEditingMode.Size = new System.Drawing.Size(203, 20);
            this.icbEditingMode.StyleController = this.layoutControl1;
            this.icbEditingMode.TabIndex = 7;
            // 
            // simpleButton2
            // 
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Location = new System.Drawing.Point(278, 274);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(88, 22);
            this.simpleButton2.StyleController = this.layoutControl1;
            this.simpleButton2.TabIndex = 6;
            this.simpleButton2.Text = "&Cancel";
            // 
            // simpleButton1
            // 
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton1.Location = new System.Drawing.Point(184, 274);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(84, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 5;
            this.simpleButton1.Text = "&OK";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.lciOK,
            this.lciCancel,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.simpleSeparator1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem4,
            this.emptySpaceItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(368, 298);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 238);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(368, 34);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciOK
            // 
            this.lciOK.Control = this.simpleButton1;
            this.lciOK.CustomizationFormText = "layoutControlItem2";
            this.lciOK.Location = new System.Drawing.Point(182, 272);
            this.lciOK.MaxSize = new System.Drawing.Size(88, 26);
            this.lciOK.MinSize = new System.Drawing.Size(88, 26);
            this.lciOK.Name = "lciOK";
            this.lciOK.Size = new System.Drawing.Size(88, 26);
            this.lciOK.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciOK.Text = "lciOK";
            this.lciOK.TextSize = new System.Drawing.Size(0, 0);
            this.lciOK.TextToControlDistance = 0;
            this.lciOK.TextVisible = false;
            // 
            // lciCancel
            // 
            this.lciCancel.Control = this.simpleButton2;
            this.lciCancel.CustomizationFormText = "layoutControlItem3";
            this.lciCancel.Location = new System.Drawing.Point(276, 272);
            this.lciCancel.MaxSize = new System.Drawing.Size(92, 26);
            this.lciCancel.MinSize = new System.Drawing.Size(92, 26);
            this.lciCancel.Name = "lciCancel";
            this.lciCancel.Size = new System.Drawing.Size(92, 26);
            this.lciCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCancel.Text = "lciCancel";
            this.lciCancel.TextSize = new System.Drawing.Size(0, 0);
            this.lciCancel.TextToControlDistance = 0;
            this.lciCancel.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 272);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(182, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(270, 272);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(6, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(6, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(6, 26);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AllowHtmlStringInCaption = true;
            this.layoutControlItem1.Control = this.icbEditingMode;
            this.layoutControlItem1.CustomizationFormText = "<b>Editing Mode</b>:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem1.Text = "<b>Editing Mode</b>:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.teFormCaptionFormat;
            this.layoutControlItem2.CustomizationFormText = "Form Caption Format:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem2.Text = "Form Caption Format:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(158, 13);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 34);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(368, 2);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.sePopupEditFormWidth;
            this.layoutControlItem3.CustomizationFormText = "Popup Edit Form Width:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 70);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem3.Text = "Popup Edit Form Width:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.icbActionOnModifiedRowChange;
            this.layoutControlItem4.CustomizationFormText = "Action On Modified Row Change:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 94);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem4.Text = "Action On Modified Row Change:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.icbBindingMode;
            this.layoutControlItem5.CustomizationFormText = "Binding Mode:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 118);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem5.Text = "Binding Mode:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.icbShowOnDoubleClick;
            this.layoutControlItem6.CustomizationFormText = "Show On Double Click:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 142);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem6.Text = "Show On Double Click:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.icbShowOnEnterKey;
            this.layoutControlItem7.CustomizationFormText = "Show On Enter Key:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 166);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem7.Text = "Show On Enter Key:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.icbShowOnF2Key;
            this.layoutControlItem8.CustomizationFormText = "Show On F2 Key:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 190);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem8.Text = "Show On F2 Key:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.icbShowUpdateCancelPanel;
            this.layoutControlItem9.CustomizationFormText = "Show Update Cancel Panel:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 214);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem9.Text = "Show Update Cancel Panel:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(158, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(368, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 36);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(368, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EditFormOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButton2;
            this.ClientSize = new System.Drawing.Size(388, 318);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(350, 300);
            this.Name = "EditFormOptions";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Form Options";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.icbShowUpdateCancelPanel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbShowOnF2Key.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbShowOnEnterKey.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbShowOnDoubleClick.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbBindingMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbActionOnModifiedRowChange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sePopupEditFormWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFormCaptionFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbEditingMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private XtraLayout.LayoutControl layoutControl1;
        private XtraEditors.SimpleButton simpleButton2;
        private XtraEditors.SimpleButton simpleButton1;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private XtraLayout.EmptySpaceItem emptySpaceItem1;
        private XtraLayout.LayoutControlItem lciOK;
        private XtraLayout.LayoutControlItem lciCancel;
        private XtraLayout.EmptySpaceItem emptySpaceItem2;
        private XtraLayout.EmptySpaceItem emptySpaceItem3;
        private XtraEditors.ImageComboBoxEdit icbShowUpdateCancelPanel;
        private XtraEditors.ImageComboBoxEdit icbShowOnF2Key;
        private XtraEditors.ImageComboBoxEdit icbShowOnEnterKey;
        private XtraEditors.ImageComboBoxEdit icbShowOnDoubleClick;
        private XtraEditors.ImageComboBoxEdit icbBindingMode;
        private XtraEditors.ImageComboBoxEdit icbActionOnModifiedRowChange;
        private XtraEditors.SpinEdit sePopupEditFormWidth;
        private XtraEditors.TextEdit teFormCaptionFormat;
        private XtraEditors.ImageComboBoxEdit icbEditingMode;
        private XtraLayout.LayoutControlItem layoutControlItem1;
        private XtraLayout.LayoutControlItem layoutControlItem2;
        private XtraLayout.SimpleSeparator simpleSeparator1;
        private XtraLayout.LayoutControlItem layoutControlItem3;
        private XtraLayout.LayoutControlItem layoutControlItem4;
        private XtraLayout.LayoutControlItem layoutControlItem5;
        private XtraLayout.LayoutControlItem layoutControlItem6;
        private XtraLayout.LayoutControlItem layoutControlItem7;
        private XtraLayout.LayoutControlItem layoutControlItem8;
        private XtraLayout.LayoutControlItem layoutControlItem9;
        private XtraLayout.EmptySpaceItem emptySpaceItem4;
        private XtraLayout.EmptySpaceItem emptySpaceItem5;
    }
}
