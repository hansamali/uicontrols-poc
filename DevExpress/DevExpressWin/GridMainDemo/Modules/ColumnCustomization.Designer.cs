namespace DevExpress.XtraGrid.Demos {
    partial class ColumnCustomization {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ColumnCustomization));
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colFirstName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLastName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCountry = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRegion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPostalCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAddress = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colBirthDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHireDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTitle = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTitleOfCourtesy = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHomePhone = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colExtension = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPhoto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNotes = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colOrderID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUnitPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colQuantity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colDiscount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colProductName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDiscontinued = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQuantityPerUnit = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colReorderLevel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUnitsInStock = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUnitsOnOrder = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colIcon = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCategoryName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCustomers_CompanyName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colContactTitle = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colContactName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCustomers_Phone = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFax = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkEdit3
            // 
            resources.ApplyResources(this.checkEdit3, "checkEdit3");
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = resources.GetString("checkEdit3.Properties.Caption");
            this.checkEdit3.StyleController = this.layoutControl1;
            this.checkEdit3.CheckedChanged += new System.EventHandler(this.checkEdit3_CheckedChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.checkEdit4);
            this.layoutControl1.Controls.Add(this.checkEdit3);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.checkEdit1);
            this.layoutControl1.Controls.Add(this.checkEdit2);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1032, 180, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            // 
            // checkEdit4
            // 
            resources.ApplyResources(this.checkEdit4, "checkEdit4");
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = resources.GetString("checkEdit4.Properties.Caption");
            this.checkEdit4.StyleController = this.layoutControl1;
            this.checkEdit4.CheckedChanged += new System.EventHandler(this.checkEdit4_CheckedChanged);
            // 
            // simpleButton1
            // 
            resources.ApplyResources(this.simpleButton1, "simpleButton1");
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // checkEdit1
            // 
            resources.ApplyResources(this.checkEdit1, "checkEdit1");
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = resources.GetString("checkEdit1.Properties.Caption");
            this.checkEdit1.StyleController = this.layoutControl1;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // checkEdit2
            // 
            resources.ApplyResources(this.checkEdit2, "checkEdit2");
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = resources.GetString("checkEdit2.Properties.Caption");
            this.checkEdit2.StyleController = this.layoutControl1;
            this.checkEdit2.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(237, 505);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(217, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkEdit2;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(217, 23);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.checkEdit1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 57);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(217, 23);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkEdit3;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(217, 23);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(217, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 126);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(217, 359);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkEdit4;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 103);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(217, 23);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // gridControl1
            // 
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemMemoEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand1,
            this.gridBand8});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colOrderID,
            this.colUnitPrice,
            this.colQuantity,
            this.colDiscount,
            this.colProductName,
            this.colCategoryName,
            this.colQuantityPerUnit,
            this.colReorderLevel,
            this.colUnitsInStock,
            this.colUnitsOnOrder,
            this.colDiscontinued,
            this.colIcon,
            this.colFirstName,
            this.colLastName,
            this.colTitle,
            this.colTitleOfCourtesy,
            this.colBirthDate,
            this.colHireDate,
            this.colAddress,
            this.colCity,
            this.colCountry,
            this.colPostalCode,
            this.colRegion,
            this.colNotes,
            this.colHomePhone,
            this.colExtension,
            this.colPhoto,
            this.colCustomers_CompanyName,
            this.colContactName,
            this.colContactTitle,
            this.colCustomers_Phone,
            this.colFax});
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsCustomization.AllowChangeColumnParent = true;
            this.advBandedGridView1.OptionsCustomization.CustomizationFormSearchBoxVisible = true;
            this.advBandedGridView1.OptionsView.AllowGlyphSkinning = true;
            this.advBandedGridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.RowSeparatorHeight = 1;
            this.advBandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrderID, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.advBandedGridView1.ShowCustomizationForm += new System.EventHandler(this.advBandedGridView1_ShowCustomizationForm);
            this.advBandedGridView1.HideCustomizationForm += new System.EventHandler(this.advBandedGridView1_HideCustomizationForm);
            this.advBandedGridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // gridBand4
            // 
            resources.ApplyResources(this.gridBand4, "gridBand4");
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand5,
            this.gridBand6,
            this.gridBand7});
            this.gridBand4.VisibleIndex = 0;
            // 
            // gridBand5
            // 
            resources.ApplyResources(this.gridBand5, "gridBand5");
            this.gridBand5.Columns.Add(this.colFirstName);
            this.gridBand5.Columns.Add(this.colLastName);
            this.gridBand5.VisibleIndex = 0;
            // 
            // colFirstName
            // 
            this.colFirstName.FieldName = "FirstName";
            this.colFirstName.Name = "colFirstName";
            resources.ApplyResources(this.colFirstName, "colFirstName");
            // 
            // colLastName
            // 
            this.colLastName.FieldName = "LastName";
            this.colLastName.Name = "colLastName";
            this.colLastName.RowIndex = 1;
            resources.ApplyResources(this.colLastName, "colLastName");
            // 
            // gridBand6
            // 
            resources.ApplyResources(this.gridBand6, "gridBand6");
            this.gridBand6.Columns.Add(this.colCountry);
            this.gridBand6.Columns.Add(this.colCity);
            this.gridBand6.Columns.Add(this.colRegion);
            this.gridBand6.Columns.Add(this.colPostalCode);
            this.gridBand6.Columns.Add(this.colAddress);
            this.gridBand6.VisibleIndex = 1;
            // 
            // colCountry
            // 
            resources.ApplyResources(this.colCountry, "colCountry");
            this.colCountry.FieldName = "Employees.Country";
            this.colCountry.Name = "colCountry";
            // 
            // colCity
            // 
            resources.ApplyResources(this.colCity, "colCity");
            this.colCity.FieldName = "Employees.City";
            this.colCity.Name = "colCity";
            // 
            // colRegion
            // 
            resources.ApplyResources(this.colRegion, "colRegion");
            this.colRegion.FieldName = "Employees.Region";
            this.colRegion.Name = "colRegion";
            // 
            // colPostalCode
            // 
            resources.ApplyResources(this.colPostalCode, "colPostalCode");
            this.colPostalCode.FieldName = "Employees.PostalCode";
            this.colPostalCode.Name = "colPostalCode";
            // 
            // colAddress
            // 
            resources.ApplyResources(this.colAddress, "colAddress");
            this.colAddress.FieldName = "Employees.Address";
            this.colAddress.Name = "colAddress";
            this.colAddress.RowIndex = 1;
            // 
            // gridBand7
            // 
            resources.ApplyResources(this.gridBand7, "gridBand7");
            this.gridBand7.Columns.Add(this.colBirthDate);
            this.gridBand7.Columns.Add(this.colHireDate);
            this.gridBand7.Columns.Add(this.colTitle);
            this.gridBand7.Columns.Add(this.colTitleOfCourtesy);
            this.gridBand7.Columns.Add(this.colHomePhone);
            this.gridBand7.Columns.Add(this.colExtension);
            this.gridBand7.Columns.Add(this.colPhoto);
            this.gridBand7.Columns.Add(this.colNotes);
            this.gridBand7.Image = ((System.Drawing.Image)(resources.GetObject("gridBand7.Image")));
            this.gridBand7.VisibleIndex = -1;
            // 
            // colBirthDate
            // 
            this.colBirthDate.FieldName = "BirthDate";
            this.colBirthDate.Name = "colBirthDate";
            // 
            // colHireDate
            // 
            this.colHireDate.FieldName = "HireDate";
            this.colHireDate.Image = ((System.Drawing.Image)(resources.GetObject("colHireDate.Image")));
            this.colHireDate.Name = "colHireDate";
            // 
            // colTitle
            // 
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            // 
            // colTitleOfCourtesy
            // 
            this.colTitleOfCourtesy.FieldName = "TitleOfCourtesy";
            this.colTitleOfCourtesy.Name = "colTitleOfCourtesy";
            // 
            // colHomePhone
            // 
            this.colHomePhone.FieldName = "HomePhone";
            this.colHomePhone.Name = "colHomePhone";
            // 
            // colExtension
            // 
            this.colExtension.FieldName = "Extension";
            this.colExtension.Name = "colExtension";
            // 
            // colPhoto
            // 
            this.colPhoto.FieldName = "Photo";
            this.colPhoto.Name = "colPhoto";
            this.colPhoto.RowCount = 2;
            resources.ApplyResources(this.colPhoto, "colPhoto");
            // 
            // colNotes
            // 
            this.colNotes.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.RowCount = 2;
            resources.ApplyResources(this.colNotes, "colNotes");
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // gridBand1
            // 
            resources.ApplyResources(this.gridBand1, "gridBand1");
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand2});
            this.gridBand1.Image = ((System.Drawing.Image)(resources.GetObject("gridBand1.Image")));
            this.gridBand1.VisibleIndex = 1;
            // 
            // gridBand3
            // 
            resources.ApplyResources(this.gridBand3, "gridBand3");
            this.gridBand3.Columns.Add(this.colOrderID);
            this.gridBand3.Columns.Add(this.colUnitPrice);
            this.gridBand3.Columns.Add(this.colQuantity);
            this.gridBand3.Columns.Add(this.colDiscount);
            this.gridBand3.VisibleIndex = 0;
            // 
            // colOrderID
            // 
            resources.ApplyResources(this.colOrderID, "colOrderID");
            this.colOrderID.FieldName = "Order Details.OrderID";
            this.colOrderID.Name = "colOrderID";
            // 
            // colUnitPrice
            // 
            resources.ApplyResources(this.colUnitPrice, "colUnitPrice");
            this.colUnitPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colUnitPrice.FieldName = "Order Details.UnitPrice";
            this.colUnitPrice.Name = "colUnitPrice";
            // 
            // repositoryItemTextEdit1
            // 
            resources.ApplyResources(this.repositoryItemTextEdit1, "repositoryItemTextEdit1");
            this.repositoryItemTextEdit1.Mask.EditMask = resources.GetString("repositoryItemTextEdit1.Mask.EditMask");
            this.repositoryItemTextEdit1.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("repositoryItemTextEdit1.Mask.MaskType")));
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat")));
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colQuantity
            // 
            this.colQuantity.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.RowIndex = 1;
            resources.ApplyResources(this.colQuantity, "colQuantity");
            // 
            // repositoryItemSpinEdit1
            // 
            resources.ApplyResources(this.repositoryItemSpinEdit1, "repositoryItemSpinEdit1");
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemSpinEdit1.Buttons"))))});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = resources.GetString("repositoryItemSpinEdit1.Mask.EditMask");
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // colDiscount
            // 
            this.colDiscount.ColumnEdit = this.repositoryItemTextEdit2;
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.RowIndex = 1;
            resources.ApplyResources(this.colDiscount, "colDiscount");
            // 
            // repositoryItemTextEdit2
            // 
            resources.ApplyResources(this.repositoryItemTextEdit2, "repositoryItemTextEdit2");
            this.repositoryItemTextEdit2.Mask.EditMask = resources.GetString("repositoryItemTextEdit2.Mask.EditMask");
            this.repositoryItemTextEdit2.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("repositoryItemTextEdit2.Mask.MaskType")));
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat")));
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridBand2
            // 
            resources.ApplyResources(this.gridBand2, "gridBand2");
            this.gridBand2.Columns.Add(this.colProductName);
            this.gridBand2.Columns.Add(this.colDiscontinued);
            this.gridBand2.Columns.Add(this.colQuantityPerUnit);
            this.gridBand2.Columns.Add(this.colReorderLevel);
            this.gridBand2.Columns.Add(this.colUnitsInStock);
            this.gridBand2.Columns.Add(this.colUnitsOnOrder);
            this.gridBand2.Columns.Add(this.colIcon);
            this.gridBand2.Columns.Add(this.colCategoryName);
            this.gridBand2.VisibleIndex = 1;
            // 
            // colProductName
            // 
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            resources.ApplyResources(this.colProductName, "colProductName");
            // 
            // colDiscontinued
            // 
            this.colDiscontinued.FieldName = "Discontinued";
            this.colDiscontinued.Image = ((System.Drawing.Image)(resources.GetObject("colDiscontinued.Image")));
            this.colDiscontinued.Name = "colDiscontinued";
            // 
            // colQuantityPerUnit
            // 
            this.colQuantityPerUnit.FieldName = "QuantityPerUnit";
            this.colQuantityPerUnit.Name = "colQuantityPerUnit";
            // 
            // colReorderLevel
            // 
            this.colReorderLevel.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colReorderLevel.FieldName = "ReorderLevel";
            this.colReorderLevel.Name = "colReorderLevel";
            // 
            // colUnitsInStock
            // 
            this.colUnitsInStock.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colUnitsInStock.FieldName = "UnitsInStock";
            this.colUnitsInStock.Name = "colUnitsInStock";
            // 
            // colUnitsOnOrder
            // 
            this.colUnitsOnOrder.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colUnitsOnOrder.FieldName = "UnitsOnOrder";
            this.colUnitsOnOrder.Name = "colUnitsOnOrder";
            // 
            // colIcon
            // 
            resources.ApplyResources(this.colIcon, "colIcon");
            this.colIcon.FieldName = "Icon_17";
            this.colIcon.Image = ((System.Drawing.Image)(resources.GetObject("colIcon.Image")));
            this.colIcon.Name = "colIcon";
            this.colIcon.OptionsColumn.ShowCaption = false;
            this.colIcon.OptionsFilter.AllowFilter = false;
            this.colIcon.RowIndex = 1;
            // 
            // colCategoryName
            // 
            this.colCategoryName.FieldName = "CategoryName";
            this.colCategoryName.Name = "colCategoryName";
            this.colCategoryName.RowIndex = 1;
            resources.ApplyResources(this.colCategoryName, "colCategoryName");
            // 
            // gridBand8
            // 
            resources.ApplyResources(this.gridBand8, "gridBand8");
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand9,
            this.gridBand10});
            this.gridBand8.VisibleIndex = -1;
            // 
            // gridBand9
            // 
            resources.ApplyResources(this.gridBand9, "gridBand9");
            this.gridBand9.Columns.Add(this.colCustomers_CompanyName);
            this.gridBand9.Columns.Add(this.colContactTitle);
            this.gridBand9.Columns.Add(this.colContactName);
            this.gridBand9.VisibleIndex = -1;
            // 
            // colCustomers_CompanyName
            // 
            resources.ApplyResources(this.colCustomers_CompanyName, "colCustomers_CompanyName");
            this.colCustomers_CompanyName.FieldName = "Customers.CompanyName";
            this.colCustomers_CompanyName.Name = "colCustomers_CompanyName";
            // 
            // colContactTitle
            // 
            this.colContactTitle.FieldName = "ContactTitle";
            this.colContactTitle.Name = "colContactTitle";
            this.colContactTitle.RowIndex = 1;
            resources.ApplyResources(this.colContactTitle, "colContactTitle");
            // 
            // colContactName
            // 
            this.colContactName.FieldName = "ContactName";
            this.colContactName.Name = "colContactName";
            this.colContactName.RowIndex = 1;
            resources.ApplyResources(this.colContactName, "colContactName");
            // 
            // gridBand10
            // 
            resources.ApplyResources(this.gridBand10, "gridBand10");
            this.gridBand10.Columns.Add(this.colCustomers_Phone);
            this.gridBand10.Columns.Add(this.colFax);
            this.gridBand10.VisibleIndex = -1;
            // 
            // colCustomers_Phone
            // 
            resources.ApplyResources(this.colCustomers_Phone, "colCustomers_Phone");
            this.colCustomers_Phone.FieldName = "Customers.Phone";
            this.colCustomers_Phone.Name = "colCustomers_Phone";
            // 
            // colFax
            // 
            this.colFax.FieldName = "Fax";
            this.colFax.Name = "colFax";
            this.colFax.RowIndex = 1;
            resources.ApplyResources(this.colFax, "colFax");
            // 
            // navigationPane1
            // 
            this.navigationPane1.Controls.Add(this.navigationPage1);
            resources.ApplyResources(this.navigationPane1, "navigationPane1");
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AppearanceCaption.Options.UseTextOptions = true;
            this.navigationPane1.PageProperties.AppearanceCaption.TextOptions.Trimming = Utils.Trimming.EllipsisCharacter;
            this.navigationPane1.PageProperties.ShowExpandButton = false;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPage[] {
            this.navigationPage1});
            this.navigationPane1.RegularSize = new System.Drawing.Size(287, 565);
            this.navigationPane1.SelectedPage = this.navigationPage1;
            this.navigationPane1.SelectedPageIndex = 0;
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "Options";
            this.navigationPage1.Controls.Add(this.layoutControl1);
            this.navigationPage1.Image = ((System.Drawing.Image)(resources.GetObject("navigationPage1.Image")));
            this.navigationPage1.Name = "navigationPage1";
            resources.ApplyResources(this.navigationPage1, "navigationPage1");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gridControl1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // ColumnCustomization
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.navigationPane1);
            this.Name = "ColumnCustomization";
            this.Load += new System.EventHandler(this.ColumnCustomization_Load);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.navigationPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private System.ComponentModel.IContainer components = null;
        private XtraLayout.LayoutControl layoutControl1;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private XtraLayout.LayoutControlItem layoutControlItem1;
        private XtraLayout.LayoutControlItem layoutControlItem3;
        private XtraLayout.LayoutControlItem layoutControlItem2;
        private XtraLayout.LayoutControlItem layoutControlItem4;
        private XtraLayout.EmptySpaceItem emptySpaceItem1;
        private XtraLayout.EmptySpaceItem emptySpaceItem2;
        private Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private Views.BandedGrid.BandedGridColumn colUnitPrice;
        private Views.BandedGrid.BandedGridColumn colOrderID;
        private Views.BandedGrid.BandedGridColumn colQuantity;
        private Views.BandedGrid.BandedGridColumn colDiscount;
        private Views.BandedGrid.BandedGridColumn colProductName;
        private Views.BandedGrid.BandedGridColumn colCategoryName;
        private Views.BandedGrid.BandedGridColumn colDiscontinued;
        private Views.BandedGrid.BandedGridColumn colQuantityPerUnit;
        private Views.BandedGrid.BandedGridColumn colReorderLevel;
        private Views.BandedGrid.BandedGridColumn colUnitsInStock;
        private Views.BandedGrid.BandedGridColumn colUnitsOnOrder;
        private Views.BandedGrid.BandedGridColumn colIcon;
        private XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private Views.BandedGrid.BandedGridColumn colFirstName;
        private Views.BandedGrid.BandedGridColumn colLastName;
        private Views.BandedGrid.BandedGridColumn colCountry;
        private Views.BandedGrid.BandedGridColumn colRegion;
        private Views.BandedGrid.BandedGridColumn colCity;
        private Views.BandedGrid.BandedGridColumn colAddress;
        private Views.BandedGrid.BandedGridColumn colPostalCode;
        private Views.BandedGrid.BandedGridColumn colBirthDate;
        private Views.BandedGrid.BandedGridColumn colHireDate;
        private Views.BandedGrid.BandedGridColumn colTitle;
        private Views.BandedGrid.BandedGridColumn colTitleOfCourtesy;
        private Views.BandedGrid.BandedGridColumn colHomePhone;
        private Views.BandedGrid.BandedGridColumn colExtension;
        private Views.BandedGrid.BandedGridColumn colNotes;
        private Views.BandedGrid.BandedGridColumn colPhoto;
        private XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private Views.BandedGrid.BandedGridColumn colCustomers_CompanyName;
        private Views.BandedGrid.BandedGridColumn colContactName;
        private Views.BandedGrid.BandedGridColumn colContactTitle;
        private Views.BandedGrid.BandedGridColumn colCustomers_Phone;
        private Views.BandedGrid.BandedGridColumn colFax;
        private Views.BandedGrid.GridBand gridBand4;
        private Views.BandedGrid.GridBand gridBand5;
        private Views.BandedGrid.GridBand gridBand6;
        private Views.BandedGrid.GridBand gridBand7;
        private Views.BandedGrid.GridBand gridBand1;
        private Views.BandedGrid.GridBand gridBand3;
        private Views.BandedGrid.GridBand gridBand2;
        private Views.BandedGrid.GridBand gridBand8;
        private Views.BandedGrid.GridBand gridBand9;
        private Views.BandedGrid.GridBand gridBand10;
        private XtraEditors.CheckEdit checkEdit4;
        private XtraLayout.LayoutControlItem layoutControlItem5;
        private XtraBars.Navigation.NavigationPane navigationPane1;
        private XtraBars.Navigation.NavigationPage navigationPage1;
        private System.Windows.Forms.Panel panel1;
    }
}
