namespace DevExpress.XtraGrid.Demos {
    partial class FixedBands {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FixedBands));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dataTable = new System.Data.DataTable();
            this.dcMaker = new System.Data.DataColumn();
            this.dcYear1992 = new System.Data.DataColumn();
            this.dcYear1993 = new System.Data.DataColumn();
            this.dcYear1994 = new System.Data.DataColumn();
            this.dcYear1995 = new System.Data.DataColumn();
            this.dcYear1996 = new System.Data.DataColumn();
            this.dcYear1997 = new System.Data.DataColumn();
            this.dcYear1998 = new System.Data.DataColumn();
            this.dcYear1999 = new System.Data.DataColumn();
            this.dcYear2000 = new System.Data.DataColumn();
            this.gcTotal = new System.Data.DataColumn();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gbManufacture = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.grColumnMaker = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbYear = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.grColumnYear1992 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.grColumnYear1993 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.grColumnYear1994 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.grColumnYear1995 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.grColumnYear1996 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.grColumnYear1997 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.grColumnYear1998 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.grColumnYear1999 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.grColumnYear2000 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbTotals = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.grColumnUnitSold = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.dataSet = new System.Data.DataSet();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.icbFixedStyle = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbBand = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.dsContacts1 = new DevExpress.XtraGrid.Demos.dsContacts();
            this.bandedGridView2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCustomerName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colJanuary = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colFebruary = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMarch = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colApril = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMay = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colJune = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colJuly = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAugust = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSeptember = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOctober = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNovember = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDecember = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colRowSum = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRowAvr = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cbShowMode = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.numFixedLineWidth = new DevExpress.XtraEditors.SpinEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbFixedStyle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbBand.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsContacts1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbShowMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFixedLineWidth.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.dataTable;
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1});
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // dataTable
            // 
            this.dataTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dcMaker,
            this.dcYear1992,
            this.dcYear1993,
            this.dcYear1994,
            this.dcYear1995,
            this.dcYear1996,
            this.dcYear1997,
            this.dcYear1998,
            this.dcYear1999,
            this.dcYear2000,
            this.gcTotal});
            this.dataTable.TableName = "Table";
            // 
            // dcMaker
            // 
            this.dcMaker.ColumnName = "Maker";
            // 
            // dcYear1992
            // 
            this.dcYear1992.ColumnName = "Year1992";
            this.dcYear1992.DataType = typeof(int);
            // 
            // dcYear1993
            // 
            this.dcYear1993.ColumnName = "Year1993";
            this.dcYear1993.DataType = typeof(int);
            // 
            // dcYear1994
            // 
            this.dcYear1994.ColumnName = "Year1994";
            this.dcYear1994.DataType = typeof(int);
            // 
            // dcYear1995
            // 
            this.dcYear1995.ColumnName = "Year1995";
            this.dcYear1995.DataType = typeof(int);
            // 
            // dcYear1996
            // 
            this.dcYear1996.ColumnName = "Year1996";
            this.dcYear1996.DataType = typeof(int);
            // 
            // dcYear1997
            // 
            this.dcYear1997.ColumnName = "Year1997";
            this.dcYear1997.DataType = typeof(int);
            // 
            // dcYear1998
            // 
            this.dcYear1998.ColumnName = "Year1998";
            this.dcYear1998.DataType = typeof(int);
            // 
            // dcYear1999
            // 
            this.dcYear1999.ColumnName = "Year1999";
            this.dcYear1999.DataType = typeof(int);
            // 
            // dcYear2000
            // 
            this.dcYear2000.ColumnName = "Year2000";
            this.dcYear2000.DataType = typeof(int);
            // 
            // gcTotal
            // 
            this.gcTotal.ColumnName = "Total";
            this.gcTotal.DataType = typeof(int);
            this.gcTotal.ReadOnly = true;
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbManufacture,
            this.gbYear,
            this.gbTotals});
            this.bandedGridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.grColumnMaker,
            this.grColumnYear1992,
            this.grColumnYear1993,
            this.grColumnYear1994,
            this.grColumnYear1995,
            this.grColumnYear1996,
            this.grColumnYear1997,
            this.grColumnYear1998,
            this.grColumnYear1999,
            this.grColumnYear2000,
            this.grColumnUnitSold});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Images = this.imageCollection1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridView1.OptionsCustomization.AllowFilter = false;
            this.bandedGridView1.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridView1.OptionsPrint.AutoWidth = false;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.OptionsView.ShowIndicator = false;
            this.bandedGridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // gbManufacture
            // 
            resources.ApplyResources(this.gbManufacture, "gbManufacture");
            this.gbManufacture.Columns.Add(this.grColumnMaker);
            this.gbManufacture.OptionsBand.AllowHotTrack = false;
            this.gbManufacture.OptionsBand.AllowMove = false;
            this.gbManufacture.OptionsBand.AllowPress = false;
            this.gbManufacture.OptionsBand.ShowInCustomizationForm = false;
            this.gbManufacture.VisibleIndex = 0;
            // 
            // grColumnMaker
            // 
            this.grColumnMaker.AppearanceCell.BackColor = ((System.Drawing.Color)(resources.GetObject("grColumnMaker.AppearanceCell.BackColor")));
            this.grColumnMaker.AppearanceCell.ForeColor = ((System.Drawing.Color)(resources.GetObject("grColumnMaker.AppearanceCell.ForeColor")));
            this.grColumnMaker.AppearanceCell.Options.UseBackColor = true;
            this.grColumnMaker.AppearanceCell.Options.UseForeColor = true;
            resources.ApplyResources(this.grColumnMaker, "grColumnMaker");
            this.grColumnMaker.FieldName = "Maker";
            this.grColumnMaker.Name = "grColumnMaker";
            // 
            // gbYear
            // 
            resources.ApplyResources(this.gbYear, "gbYear");
            this.gbYear.Columns.Add(this.grColumnYear1992);
            this.gbYear.Columns.Add(this.grColumnYear1993);
            this.gbYear.Columns.Add(this.grColumnYear1994);
            this.gbYear.Columns.Add(this.grColumnYear1995);
            this.gbYear.Columns.Add(this.grColumnYear1996);
            this.gbYear.Columns.Add(this.grColumnYear1997);
            this.gbYear.Columns.Add(this.grColumnYear1998);
            this.gbYear.Columns.Add(this.grColumnYear1999);
            this.gbYear.Columns.Add(this.grColumnYear2000);
            this.gbYear.OptionsBand.AllowHotTrack = false;
            this.gbYear.OptionsBand.AllowMove = false;
            this.gbYear.OptionsBand.AllowPress = false;
            this.gbYear.OptionsBand.ShowInCustomizationForm = false;
            this.gbYear.VisibleIndex = 1;
            // 
            // grColumnYear1992
            // 
            resources.ApplyResources(this.grColumnYear1992, "grColumnYear1992");
            this.grColumnYear1992.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear1992.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear1992.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear1992.FieldName = "Year1992";
            this.grColumnYear1992.Name = "grColumnYear1992";
            this.grColumnYear1992.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear1992.Summary"))), resources.GetString("grColumnYear1992.Summary1"), resources.GetString("grColumnYear1992.Summary2"))});
            // 
            // repositoryItemSpinEdit1
            // 
            resources.ApplyResources(this.repositoryItemSpinEdit1, "repositoryItemSpinEdit1");
            this.repositoryItemSpinEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = resources.GetString("repositoryItemSpinEdit1.Mask.EditMask");
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // grColumnYear1993
            // 
            resources.ApplyResources(this.grColumnYear1993, "grColumnYear1993");
            this.grColumnYear1993.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear1993.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear1993.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear1993.FieldName = "Year1993";
            this.grColumnYear1993.Name = "grColumnYear1993";
            this.grColumnYear1993.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear1993.Summary"))), resources.GetString("grColumnYear1993.Summary1"), resources.GetString("grColumnYear1993.Summary2"))});
            // 
            // grColumnYear1994
            // 
            resources.ApplyResources(this.grColumnYear1994, "grColumnYear1994");
            this.grColumnYear1994.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear1994.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear1994.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear1994.FieldName = "Year1994";
            this.grColumnYear1994.Name = "grColumnYear1994";
            this.grColumnYear1994.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear1994.Summary"))), resources.GetString("grColumnYear1994.Summary1"), resources.GetString("grColumnYear1994.Summary2"))});
            // 
            // grColumnYear1995
            // 
            resources.ApplyResources(this.grColumnYear1995, "grColumnYear1995");
            this.grColumnYear1995.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear1995.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear1995.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear1995.FieldName = "Year1995";
            this.grColumnYear1995.Name = "grColumnYear1995";
            this.grColumnYear1995.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear1995.Summary"))), resources.GetString("grColumnYear1995.Summary1"), resources.GetString("grColumnYear1995.Summary2"))});
            // 
            // grColumnYear1996
            // 
            resources.ApplyResources(this.grColumnYear1996, "grColumnYear1996");
            this.grColumnYear1996.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear1996.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear1996.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear1996.FieldName = "Year1996";
            this.grColumnYear1996.Name = "grColumnYear1996";
            this.grColumnYear1996.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear1996.Summary"))), resources.GetString("grColumnYear1996.Summary1"), resources.GetString("grColumnYear1996.Summary2"))});
            // 
            // grColumnYear1997
            // 
            resources.ApplyResources(this.grColumnYear1997, "grColumnYear1997");
            this.grColumnYear1997.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear1997.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear1997.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear1997.FieldName = "Year1997";
            this.grColumnYear1997.Name = "grColumnYear1997";
            this.grColumnYear1997.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear1997.Summary"))), resources.GetString("grColumnYear1997.Summary1"), resources.GetString("grColumnYear1997.Summary2"))});
            // 
            // grColumnYear1998
            // 
            resources.ApplyResources(this.grColumnYear1998, "grColumnYear1998");
            this.grColumnYear1998.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear1998.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear1998.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear1998.FieldName = "Year1998";
            this.grColumnYear1998.Name = "grColumnYear1998";
            this.grColumnYear1998.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear1998.Summary"))), resources.GetString("grColumnYear1998.Summary1"), resources.GetString("grColumnYear1998.Summary2"))});
            // 
            // grColumnYear1999
            // 
            resources.ApplyResources(this.grColumnYear1999, "grColumnYear1999");
            this.grColumnYear1999.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear1999.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear1999.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear1999.FieldName = "Year1999";
            this.grColumnYear1999.Name = "grColumnYear1999";
            this.grColumnYear1999.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear1999.Summary"))), resources.GetString("grColumnYear1999.Summary1"), resources.GetString("grColumnYear1999.Summary2"))});
            // 
            // grColumnYear2000
            // 
            resources.ApplyResources(this.grColumnYear2000, "grColumnYear2000");
            this.grColumnYear2000.ColumnEdit = this.repositoryItemSpinEdit1;
            this.grColumnYear2000.DisplayFormat.FormatString = "### ### ###";
            this.grColumnYear2000.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnYear2000.FieldName = "Year2000";
            this.grColumnYear2000.Name = "grColumnYear2000";
            this.grColumnYear2000.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnYear2000.Summary"))), resources.GetString("grColumnYear2000.Summary1"), resources.GetString("grColumnYear2000.Summary2"))});
            // 
            // gbTotals
            // 
            resources.ApplyResources(this.gbTotals, "gbTotals");
            this.gbTotals.Columns.Add(this.grColumnUnitSold);
            this.gbTotals.OptionsBand.AllowHotTrack = false;
            this.gbTotals.OptionsBand.AllowMove = false;
            this.gbTotals.OptionsBand.AllowPress = false;
            this.gbTotals.OptionsBand.ShowInCustomizationForm = false;
            this.gbTotals.VisibleIndex = 2;
            // 
            // grColumnUnitSold
            // 
            this.grColumnUnitSold.AppearanceCell.BackColor = ((System.Drawing.Color)(resources.GetObject("grColumnUnitSold.AppearanceCell.BackColor")));
            this.grColumnUnitSold.AppearanceCell.ForeColor = ((System.Drawing.Color)(resources.GetObject("grColumnUnitSold.AppearanceCell.ForeColor")));
            this.grColumnUnitSold.AppearanceCell.Options.UseBackColor = true;
            this.grColumnUnitSold.AppearanceCell.Options.UseForeColor = true;
            resources.ApplyResources(this.grColumnUnitSold, "grColumnUnitSold");
            this.grColumnUnitSold.DisplayFormat.FormatString = "### ### ### ###";
            this.grColumnUnitSold.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.grColumnUnitSold.FieldName = "Total";
            this.grColumnUnitSold.Name = "grColumnUnitSold";
            this.grColumnUnitSold.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("grColumnUnitSold.Summary"))), resources.GetString("grColumnUnitSold.Summary1"), resources.GetString("grColumnUnitSold.Summary2"))});
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "totals");
            // 
            // dataSet
            // 
            this.dataSet.DataSetName = "NewDataSet";
            this.dataSet.Locale = new System.Globalization.CultureInfo("en-US");
            this.dataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable});
            // 
            // xtraTabControl1
            // 
            resources.ApplyResources(this.xtraTabControl1, "xtraTabControl1");
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.panelControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            resources.ApplyResources(this.xtraTabPage1, "xtraTabPage1");
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.icbFixedStyle);
            this.panelControl2.Controls.Add(this.icbBand);
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Controls.Add(this.label4);
            this.panelControl2.Controls.Add(this.labelControl1);
            resources.ApplyResources(this.panelControl2, "panelControl2");
            this.panelControl2.Name = "panelControl2";
            // 
            // icbFixedStyle
            // 
            resources.ApplyResources(this.icbFixedStyle, "icbFixedStyle");
            this.icbFixedStyle.Name = "icbFixedStyle";
            this.icbFixedStyle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("icbFixedStyle.Properties.Buttons"))))});
            this.icbFixedStyle.SelectedIndexChanged += new System.EventHandler(this.icbFixedStyle_SelectedIndexChanged);
            // 
            // icbBand
            // 
            resources.ApplyResources(this.icbBand, "icbBand");
            this.icbBand.Name = "icbBand";
            this.icbBand.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("icbBand.Properties.Buttons"))))});
            this.icbBand.Properties.DropDownRows = 15;
            this.icbBand.SelectedIndexChanged += new System.EventHandler(this.icbBand_SelectedIndexChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // labelControl1
            // 
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl1.LineVisible = true;
            this.labelControl1.Name = "labelControl1";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Controls.Add(this.panelControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            resources.ApplyResources(this.xtraTabPage2, "xtraTabPage2");
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.dsContacts1.CustomersPayment;
            resources.ApplyResources(this.gridControl2, "gridControl2");
            this.gridControl2.MainView = this.bandedGridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit2});
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView2});
            // 
            // dsContacts1
            // 
            this.dsContacts1.DataSetName = "dsContacts";
            this.dsContacts1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsContacts1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bandedGridView2
            // 
            this.bandedGridView2.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand5,
            this.gridBand6});
            this.bandedGridView2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.bandedGridView2.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colID,
            this.colCustomerName,
            this.colYear,
            this.colJanuary,
            this.colFebruary,
            this.colMarch,
            this.colApril,
            this.colMay,
            this.colJune,
            this.colJuly,
            this.colAugust,
            this.colSeptember,
            this.colOctober,
            this.colNovember,
            this.colDecember,
            this.colRowSum,
            this.colRowAvr});
            this.bandedGridView2.GridControl = this.gridControl2;
            this.bandedGridView2.GroupCount = 1;
            this.bandedGridView2.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary"))), resources.GetString("bandedGridView2.GroupSummary1"), this.colJanuary, resources.GetString("bandedGridView2.GroupSummary2")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary3"))), resources.GetString("bandedGridView2.GroupSummary4"), this.colFebruary, resources.GetString("bandedGridView2.GroupSummary5")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary6"))), resources.GetString("bandedGridView2.GroupSummary7"), this.colMarch, resources.GetString("bandedGridView2.GroupSummary8")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary9"))), resources.GetString("bandedGridView2.GroupSummary10"), this.colApril, resources.GetString("bandedGridView2.GroupSummary11")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary12"))), resources.GetString("bandedGridView2.GroupSummary13"), this.colMay, resources.GetString("bandedGridView2.GroupSummary14")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary15"))), resources.GetString("bandedGridView2.GroupSummary16"), this.colJune, resources.GetString("bandedGridView2.GroupSummary17")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary18"))), resources.GetString("bandedGridView2.GroupSummary19"), this.colJuly, resources.GetString("bandedGridView2.GroupSummary20")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary21"))), resources.GetString("bandedGridView2.GroupSummary22"), this.colAugust, resources.GetString("bandedGridView2.GroupSummary23")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary24"))), resources.GetString("bandedGridView2.GroupSummary25"), this.colSeptember, resources.GetString("bandedGridView2.GroupSummary26")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary27"))), resources.GetString("bandedGridView2.GroupSummary28"), this.colOctober, resources.GetString("bandedGridView2.GroupSummary29")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary30"))), resources.GetString("bandedGridView2.GroupSummary31"), this.colNovember, resources.GetString("bandedGridView2.GroupSummary32")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary33"))), resources.GetString("bandedGridView2.GroupSummary34"), this.colDecember, resources.GetString("bandedGridView2.GroupSummary35")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary36"))), resources.GetString("bandedGridView2.GroupSummary37"), this.colRowSum, resources.GetString("bandedGridView2.GroupSummary38")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary39"))), resources.GetString("bandedGridView2.GroupSummary40"), this.colRowAvr, resources.GetString("bandedGridView2.GroupSummary41")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("bandedGridView2.GroupSummary42"))), resources.GetString("bandedGridView2.GroupSummary43"), ((DevExpress.XtraGrid.Columns.GridColumn)(resources.GetObject("bandedGridView2.GroupSummary44"))), resources.GetString("bandedGridView2.GroupSummary45"))});
            this.bandedGridView2.HorzScrollStep = 25;
            this.bandedGridView2.Name = "bandedGridView2";
            this.bandedGridView2.OptionsPrint.AutoWidth = false;
            this.bandedGridView2.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView2.OptionsView.ShowFooter = true;
            this.bandedGridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colYear, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.bandedGridView2.CustomDrawRowFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.bandedGridView2_CustomDrawFooterCell);
            this.bandedGridView2.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.bandedGridView2_CustomDrawFooterCell);
            this.bandedGridView2.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // gridBand4
            // 
            resources.ApplyResources(this.gridBand4, "gridBand4");
            this.gridBand4.Columns.Add(this.colCustomerName);
            this.gridBand4.Columns.Add(this.colYear);
            this.gridBand4.VisibleIndex = 0;
            // 
            // colCustomerName
            // 
            this.colCustomerName.AppearanceCell.BackColor = ((System.Drawing.Color)(resources.GetObject("colCustomerName.AppearanceCell.BackColor")));
            this.colCustomerName.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colCustomerName.AppearanceCell.Font")));
            this.colCustomerName.AppearanceCell.ForeColor = ((System.Drawing.Color)(resources.GetObject("colCustomerName.AppearanceCell.ForeColor")));
            this.colCustomerName.AppearanceCell.Options.UseBackColor = true;
            this.colCustomerName.AppearanceCell.Options.UseFont = true;
            this.colCustomerName.AppearanceCell.Options.UseForeColor = true;
            resources.ApplyResources(this.colCustomerName, "colCustomerName");
            this.colCustomerName.FieldName = "Customer Name";
            this.colCustomerName.Name = "colCustomerName";
            this.colCustomerName.OptionsColumn.AllowEdit = false;
            this.colCustomerName.OptionsColumn.AllowFocus = false;
            this.colCustomerName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colCustomerName.Summary"))))});
            // 
            // colYear
            // 
            this.colYear.AppearanceCell.BackColor = ((System.Drawing.Color)(resources.GetObject("colYear.AppearanceCell.BackColor")));
            this.colYear.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colYear.AppearanceCell.Font")));
            this.colYear.AppearanceCell.ForeColor = ((System.Drawing.Color)(resources.GetObject("colYear.AppearanceCell.ForeColor")));
            this.colYear.AppearanceCell.Options.UseBackColor = true;
            this.colYear.AppearanceCell.Options.UseFont = true;
            this.colYear.AppearanceCell.Options.UseForeColor = true;
            resources.ApplyResources(this.colYear, "colYear");
            this.colYear.FieldName = "Year";
            this.colYear.Name = "colYear";
            this.colYear.OptionsColumn.AllowFocus = false;
            this.colYear.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colYear.OptionsColumn.AllowMove = false;
            // 
            // gridBand5
            // 
            resources.ApplyResources(this.gridBand5, "gridBand5");
            this.gridBand5.Columns.Add(this.colID);
            this.gridBand5.Columns.Add(this.colJanuary);
            this.gridBand5.Columns.Add(this.colFebruary);
            this.gridBand5.Columns.Add(this.colMarch);
            this.gridBand5.Columns.Add(this.colApril);
            this.gridBand5.Columns.Add(this.colMay);
            this.gridBand5.Columns.Add(this.colJune);
            this.gridBand5.Columns.Add(this.colJuly);
            this.gridBand5.Columns.Add(this.colAugust);
            this.gridBand5.Columns.Add(this.colSeptember);
            this.gridBand5.Columns.Add(this.colOctober);
            this.gridBand5.Columns.Add(this.colNovember);
            this.gridBand5.Columns.Add(this.colDecember);
            this.gridBand5.VisibleIndex = 1;
            // 
            // colID
            // 
            resources.ApplyResources(this.colID, "colID");
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colJanuary
            // 
            resources.ApplyResources(this.colJanuary, "colJanuary");
            this.colJanuary.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colJanuary.DisplayFormat.FormatString = "c";
            this.colJanuary.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colJanuary.FieldName = "January";
            this.colJanuary.Name = "colJanuary";
            this.colJanuary.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colJanuary.Summary"))), resources.GetString("colJanuary.Summary1"), resources.GetString("colJanuary.Summary2"))});
            // 
            // repositoryItemSpinEdit2
            // 
            resources.ApplyResources(this.repositoryItemSpinEdit2, "repositoryItemSpinEdit2");
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // colFebruary
            // 
            resources.ApplyResources(this.colFebruary, "colFebruary");
            this.colFebruary.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colFebruary.DisplayFormat.FormatString = "c";
            this.colFebruary.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFebruary.FieldName = "February";
            this.colFebruary.Name = "colFebruary";
            this.colFebruary.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colFebruary.Summary"))), resources.GetString("colFebruary.Summary1"), resources.GetString("colFebruary.Summary2"))});
            // 
            // colMarch
            // 
            resources.ApplyResources(this.colMarch, "colMarch");
            this.colMarch.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colMarch.DisplayFormat.FormatString = "c";
            this.colMarch.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMarch.FieldName = "March";
            this.colMarch.Name = "colMarch";
            this.colMarch.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colMarch.Summary"))), resources.GetString("colMarch.Summary1"), resources.GetString("colMarch.Summary2"))});
            // 
            // colApril
            // 
            resources.ApplyResources(this.colApril, "colApril");
            this.colApril.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colApril.DisplayFormat.FormatString = "c";
            this.colApril.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colApril.FieldName = "April";
            this.colApril.Name = "colApril";
            this.colApril.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colApril.Summary"))), resources.GetString("colApril.Summary1"), resources.GetString("colApril.Summary2"))});
            // 
            // colMay
            // 
            resources.ApplyResources(this.colMay, "colMay");
            this.colMay.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colMay.DisplayFormat.FormatString = "c";
            this.colMay.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMay.FieldName = "May";
            this.colMay.Name = "colMay";
            this.colMay.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colMay.Summary"))), resources.GetString("colMay.Summary1"), resources.GetString("colMay.Summary2"))});
            // 
            // colJune
            // 
            resources.ApplyResources(this.colJune, "colJune");
            this.colJune.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colJune.DisplayFormat.FormatString = "c";
            this.colJune.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colJune.FieldName = "June";
            this.colJune.Name = "colJune";
            this.colJune.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colJune.Summary"))), resources.GetString("colJune.Summary1"), resources.GetString("colJune.Summary2"))});
            // 
            // colJuly
            // 
            resources.ApplyResources(this.colJuly, "colJuly");
            this.colJuly.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colJuly.DisplayFormat.FormatString = "c";
            this.colJuly.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colJuly.FieldName = "July";
            this.colJuly.Name = "colJuly";
            this.colJuly.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colJuly.Summary"))), resources.GetString("colJuly.Summary1"), resources.GetString("colJuly.Summary2"))});
            // 
            // colAugust
            // 
            resources.ApplyResources(this.colAugust, "colAugust");
            this.colAugust.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colAugust.DisplayFormat.FormatString = "c";
            this.colAugust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAugust.FieldName = "August";
            this.colAugust.Name = "colAugust";
            this.colAugust.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colAugust.Summary"))), resources.GetString("colAugust.Summary1"), resources.GetString("colAugust.Summary2"))});
            // 
            // colSeptember
            // 
            resources.ApplyResources(this.colSeptember, "colSeptember");
            this.colSeptember.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colSeptember.DisplayFormat.FormatString = "c";
            this.colSeptember.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSeptember.FieldName = "September";
            this.colSeptember.Name = "colSeptember";
            this.colSeptember.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colSeptember.Summary"))), resources.GetString("colSeptember.Summary1"), resources.GetString("colSeptember.Summary2"))});
            // 
            // colOctober
            // 
            resources.ApplyResources(this.colOctober, "colOctober");
            this.colOctober.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colOctober.DisplayFormat.FormatString = "c";
            this.colOctober.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOctober.FieldName = "October";
            this.colOctober.Name = "colOctober";
            this.colOctober.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colOctober.Summary"))), resources.GetString("colOctober.Summary1"), resources.GetString("colOctober.Summary2"))});
            // 
            // colNovember
            // 
            resources.ApplyResources(this.colNovember, "colNovember");
            this.colNovember.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colNovember.DisplayFormat.FormatString = "c";
            this.colNovember.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNovember.FieldName = "November";
            this.colNovember.Name = "colNovember";
            this.colNovember.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colNovember.Summary"))), resources.GetString("colNovember.Summary1"), resources.GetString("colNovember.Summary2"))});
            // 
            // colDecember
            // 
            resources.ApplyResources(this.colDecember, "colDecember");
            this.colDecember.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colDecember.DisplayFormat.FormatString = "c";
            this.colDecember.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDecember.FieldName = "December";
            this.colDecember.Name = "colDecember";
            this.colDecember.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colDecember.Summary"))), resources.GetString("colDecember.Summary1"), resources.GetString("colDecember.Summary2"))});
            // 
            // gridBand6
            // 
            resources.ApplyResources(this.gridBand6, "gridBand6");
            this.gridBand6.Columns.Add(this.colRowSum);
            this.gridBand6.Columns.Add(this.colRowAvr);
            this.gridBand6.VisibleIndex = 2;
            // 
            // colRowSum
            // 
            this.colRowSum.AppearanceCell.BackColor = ((System.Drawing.Color)(resources.GetObject("colRowSum.AppearanceCell.BackColor")));
            this.colRowSum.AppearanceCell.ForeColor = ((System.Drawing.Color)(resources.GetObject("colRowSum.AppearanceCell.ForeColor")));
            this.colRowSum.AppearanceCell.Options.UseBackColor = true;
            this.colRowSum.AppearanceCell.Options.UseForeColor = true;
            resources.ApplyResources(this.colRowSum, "colRowSum");
            this.colRowSum.DisplayFormat.FormatString = "c";
            this.colRowSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRowSum.FieldName = "RowSum";
            this.colRowSum.Name = "colRowSum";
            this.colRowSum.OptionsColumn.AllowEdit = false;
            this.colRowSum.OptionsColumn.AllowFocus = false;
            this.colRowSum.OptionsFilter.AllowFilter = false;
            this.colRowSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colRowSum.Summary"))), resources.GetString("colRowSum.Summary1"), resources.GetString("colRowSum.Summary2"))});
            // 
            // colRowAvr
            // 
            this.colRowAvr.AppearanceCell.BackColor = ((System.Drawing.Color)(resources.GetObject("colRowAvr.AppearanceCell.BackColor")));
            this.colRowAvr.AppearanceCell.ForeColor = ((System.Drawing.Color)(resources.GetObject("colRowAvr.AppearanceCell.ForeColor")));
            this.colRowAvr.AppearanceCell.Options.UseBackColor = true;
            this.colRowAvr.AppearanceCell.Options.UseForeColor = true;
            resources.ApplyResources(this.colRowAvr, "colRowAvr");
            this.colRowAvr.DisplayFormat.FormatString = "c";
            this.colRowAvr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRowAvr.FieldName = "RowAvr";
            this.colRowAvr.Name = "colRowAvr";
            this.colRowAvr.OptionsColumn.AllowEdit = false;
            this.colRowAvr.OptionsColumn.AllowFocus = false;
            this.colRowAvr.OptionsFilter.AllowFilter = false;
            this.colRowAvr.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colRowAvr.Summary"))), resources.GetString("colRowAvr.Summary1"), resources.GetString("colRowAvr.Summary2"))});
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cbShowMode);
            this.panelControl1.Controls.Add(this.numFixedLineWidth);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label3);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            // 
            // labelControl2
            // 
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl2.LineVisible = true;
            this.labelControl2.Name = "labelControl2";
            // 
            // cbShowMode
            // 
            resources.ApplyResources(this.cbShowMode, "cbShowMode");
            this.cbShowMode.Name = "cbShowMode";
            this.cbShowMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cbShowMode.Properties.Buttons"))))});
            this.cbShowMode.SelectedIndexChanged += new System.EventHandler(this.cbShowMode_SelectedIndexChanged);
            // 
            // numFixedLineWidth
            // 
            resources.ApplyResources(this.numFixedLineWidth, "numFixedLineWidth");
            this.numFixedLineWidth.Name = "numFixedLineWidth";
            this.numFixedLineWidth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.numFixedLineWidth.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.numFixedLineWidth.Properties.IsFloatValue = false;
            this.numFixedLineWidth.Properties.Mask.EditMask = resources.GetString("numFixedLineWidth.Properties.Mask.EditMask");
            this.numFixedLineWidth.Properties.MaxValue = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numFixedLineWidth.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFixedLineWidth.EditValueChanged += new System.EventHandler(this.numFixedLineWidth_ValueChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // FixedBands
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "FixedBands";
            this.Load += new System.EventHandler(this.FixedBands_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbFixedStyle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbBand.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsContacts1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbShowMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFixedLineWidth.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbManufacture;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnMaker;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear1992;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear1993;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear1994;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear1995;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear1996;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear1997;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear1998;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear1999;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnYear2000;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grColumnUnitSold;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbYear;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbTotals;
        private System.Data.DataSet dataSet;
        private System.Data.DataTable dataTable;
        private System.Data.DataColumn dcMaker;
        private System.Data.DataColumn dcYear1992;
        private System.Data.DataColumn dcYear1993;
        private System.Data.DataColumn dcYear1994;
        private System.Data.DataColumn dcYear1995;
        private System.Data.DataColumn dcYear1996;
        private System.Data.DataColumn dcYear1997;
        private System.Data.DataColumn dcYear1998;
        private System.Data.DataColumn dcYear1999;
        private System.Data.DataColumn dcYear2000;
        private System.Data.DataColumn gcTotal;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRowSum;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFebruary;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAugust;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMarch;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRowAvr;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCustomerName;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colJanuary;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colApril;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMay;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colJune;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colJuly;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSeptember;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOctober;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNovember;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDecember;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Demos.dsContacts dsContacts1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraEditors.SpinEdit numFixedLineWidth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.ComponentModel.IContainer components = null;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbShowMode;
        private XtraEditors.PanelControl panelControl2;
        private XtraEditors.LabelControl labelControl1;
        private XtraEditors.LabelControl labelControl2;
        private XtraEditors.ImageComboBoxEdit icbFixedStyle;
        private XtraEditors.ImageComboBoxEdit icbBand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private Utils.ImageCollection imageCollection1;
    }
}
