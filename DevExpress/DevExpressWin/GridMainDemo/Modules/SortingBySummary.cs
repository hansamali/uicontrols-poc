using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Drawing;
using DevExpress.Utils;
using DevExpress.Utils.Paint;

namespace DevExpress.XtraGrid.Demos {
    /// <summary>
    /// Summary description for SortingBySummary.
    /// </summary>
    public partial class SortingBySummary : TutorialControl {
        public SortingBySummary() {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
            TutorialInfo.WhatsThisCodeFile = "CS\\GridMainDemo\\Modules\\SortingBySummary.cs";
            TutorialInfo.WhatsThisXMLFile = "DevExpress.XtraGrid.Demos.CodeInfo.SortingBySummary.xml";
            simpleButton1.Visible = false;// !LocalizationHelper.IsJapanese;
            InitNWindData();
            // TODO: Add any initialization after the InitForm call

            //<gridControl1>
            /*
            ~Note: the following properties are set at design time and listed here only for demonstration purposes.
            gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Product Name", null, "(Product: Count {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Order Sum", null, "(Orders: Sum {0:c})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Average, "Unit Price", null, "(Price: Avg {0:c})")});
            */
            //</gridControl1>
            

        }
        ColorsObject colorsSortValue = new ColorsObject(Color.RoyalBlue, Color.Empty);
        public override DevExpress.XtraGrid.Views.Base.BaseView ExportView { get { return gridView1; } }
        private void SortingBySummary_Load(object sender, System.EventArgs e) {
            gridControl1.DataSource = GroupIntervalData.CreateDataTable(500, false);
            cbeSorting.SelectedIndex = 4;
        }
        protected override void InitMDBData(string connectionString) {
            DemoHelper.AddCategoryImagesToEdit(connectionString, repositoryItemImageComboBox1);
        }
        //<cbeSorting>
        GridColumn GroupColumn { get { return gridColumn5; } }
        int CurrentSummaryItemIndex { get { return (int)cbeSorting.SelectedIndex / 2 - 1; } }
        ColumnSortOrder CurrentSortOrder { get { return cbeSorting.SelectedIndex % 2 == 0 ? ColumnSortOrder.Ascending : ColumnSortOrder.Descending; } }

        private void cbeSorting_SelectedIndexChanged(object sender, System.EventArgs e) {
            gridView1.BeginSort();
            try {
                gridView1.GroupSummarySortInfo.Clear();
                GroupColumn.SortOrder = CurrentSortOrder;
                if(CurrentSummaryItemIndex >= 0)
                    gridView1.GroupSummarySortInfo.Add(
                        gridView1.GroupSummary[CurrentSummaryItemIndex], CurrentSortOrder, GroupColumn);
            }
            finally {
                gridView1.EndSort();
            }
        }
        //</cbeSorting>
        
        int[] GetCurrentRange(string text) {
            string[] rangeString = new string[] { "Count", "Sum", "Avg" };
            int ret1 = 0;
            int ret2 = 0;
            if(CurrentSummaryItemIndex < 0)
                ret2 = text.IndexOf("(") - 1;
            else {
                ret1 = text.IndexOf(rangeString[CurrentSummaryItemIndex]) +
                    rangeString[CurrentSummaryItemIndex].Length + 1;
                ret2 = text.IndexOf(")", ret1) - ret1;
            }
            return new int[] { ret1, ret2 };
        }

        private void gridView1_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e) {
            if(LocalizationHelper.IsJapanese) return;
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            GridGroupRowPainter painter = e.Painter as GridGroupRowPainter;
            if(info == null || painter == null) return;
            if(info.RowHandle == gridView1.FocusedRowHandle) return;

            e.DefaultDraw();

            AppearanceObject appObject = painter.GetGroupColorAppearance(info);
            MultiColorDrawStringParams mcds = new MultiColorDrawStringParams(appObject);
            mcds.Text = gridView1.GetGroupRowDisplayText(info.RowHandle);
            CharacterRangeWithFormat crwf0 = new CharacterRangeWithFormat(
                0, mcds.Text.Length, appObject.GetForeColor(), appObject.GetBackColor());
            int[] range = GetCurrentRange(mcds.Text);
            CharacterRangeWithFormat crwf1 = new CharacterRangeWithFormat(
                range[0], range[1], colorsSortValue.ForeColor, colorsSortValue.BackColor);
            mcds.Ranges = new CharacterRangeWithFormat[] { crwf0, crwf1 };
            Rectangle r = painter.GetGroupClientBounds(info);
            r.X += info.ButtonBounds.Width + 7;
            r.Width -= (info.ButtonBounds.Width + 7);
            mcds.Bounds = r;

            e.Cache.Paint.MultiColorDrawString(e.Cache, mcds);
            e.Handled = true;
        }

        #region Edit Sorting Value style
        private void simpleButton1_Click(object sender, System.EventArgs e) {
            DevExpress.XtraEditors.XtraForm frm = new DevExpress.XtraEditors.XtraForm();
            frm.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            frm.StartPosition = FormStartPosition.Manual;
            frm.Text = Properties.Resources.EditStyle;
            frm.Location = this.PointToScreen(new Point(simpleButton1.Left + simpleButton1.Width / 2 - frm.Width, panelControl1.Top + simpleButton1.Top + simpleButton1.Height / 2));
            DevExpress.DXperience.Demos.XtraPropertyGrid grid = new DevExpress.DXperience.Demos.XtraPropertyGrid();
            grid.ShowDescription = false;
            grid.ShowCategories = false;
            grid.PropertyGrid.SelectedObject = colorsSortValue;
            grid.PropertyGrid.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(propertyValueChanged);
            grid.Dock = DockStyle.Fill;
            frm.Controls.Add(grid);
            frm.ShowDialog();
        }

        void propertyValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e) {
            gridView1.LayoutChanged();
        }
        #endregion

        private void gridView1_GridMenuItemClick(object sender, DevExpress.XtraGrid.Views.Grid.GridMenuItemClickEventArgs e) {
            if(e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Column) return;
            if(e.SummaryItem == null) return;
            int index = -1;
            if(e.DXMenuItem.Caption.IndexOf("Product") > -1) index = 2;
            if(e.DXMenuItem.Caption.IndexOf("Order Sum") > -1) index = 4;
            if(e.DXMenuItem.Caption.IndexOf("Price") > -1) index = 6;
            if(SortOrder.Descending.ToString() == e.SummaryFormat) index += 1;
            cbeSorting.SelectedIndex = -1;
            cbeSorting.SelectedIndex = index;
            e.Handled = true;
        }
    }
}
