﻿namespace DevExpress.XtraGrid.Demos {
    partial class SplitPresentation {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplitPresentation));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.sbRemoveSplit = new DevExpress.XtraEditors.SimpleButton();
            this.sbSplit = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBeds = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaths = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouseSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLotSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFeatures = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearBuilt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhoto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.lcInfo = new DevExpress.XtraEditors.LabelControl();
            this.editInterestRate = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.editTermOfLoan = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.sbCalc = new DevExpress.XtraEditors.SimpleButton();
            this.editStart = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.editLoanAmount = new DevExpress.XtraEditors.SpinEdit();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcMonth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gcBalance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPayment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCalc = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciInfo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editInterestRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editTermOfLoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editLoanAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.sbRemoveSplit);
            this.layoutControl1.Controls.Add(this.sbSplit);
            this.layoutControl1.Controls.Add(this.gridSplitContainer2);
            this.layoutControl1.Controls.Add(this.lcInfo);
            this.layoutControl1.Controls.Add(this.editInterestRate);
            this.layoutControl1.Controls.Add(this.editTermOfLoan);
            this.layoutControl1.Controls.Add(this.sbCalc);
            this.layoutControl1.Controls.Add(this.editStart);
            this.layoutControl1.Controls.Add(this.editLoanAmount);
            this.layoutControl1.Controls.Add(this.gridSplitContainer1);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(695, 284, 450, 350);
            this.layoutControl1.OptionsView.UseParentAutoScaleFactor = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            // 
            // sbRemoveSplit
            // 
            resources.ApplyResources(this.sbRemoveSplit, "sbRemoveSplit");
            this.sbRemoveSplit.Name = "sbRemoveSplit";
            this.sbRemoveSplit.StyleController = this.layoutControl1;
            this.sbRemoveSplit.Click += new System.EventHandler(this.sbRemoveSplit_Click);
            // 
            // sbSplit
            // 
            resources.ApplyResources(this.sbSplit, "sbSplit");
            this.sbSplit.Name = "sbSplit";
            this.sbSplit.StyleController = this.layoutControl1;
            this.sbSplit.Click += new System.EventHandler(this.sbSplit_Click);
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Horizontal = true;
            resources.ApplyResources(this.gridSplitContainer2, "gridSplitContainer2");
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.SplitterPosition = 320;
            this.gridSplitContainer2.SplitViewShown += new System.EventHandler(this.gridSplitContainer2_SplitViewShown);
            this.gridSplitContainer2.SplitViewHidden += new System.EventHandler(this.gridSplitContainer2_SplitViewHidden);
            // 
            // gridControl2
            // 
            resources.ApplyResources(this.gridControl2, "gridControl2");
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colAddress,
            this.colBeds,
            this.colBaths,
            this.colHouseSize,
            this.colLotSize,
            this.colPrice,
            this.colFeatures,
            this.colYearBuilt,
            this.colType,
            this.colStatus,
            this.colPhoto});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 60;
            this.gridView2.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // colID
            // 
            resources.ApplyResources(this.colID, "colID");
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowFocus = false;
            // 
            // colAddress
            // 
            resources.ApplyResources(this.colAddress, "colAddress");
            this.colAddress.FieldName = "Address";
            this.colAddress.Name = "colAddress";
            this.colAddress.OptionsColumn.AllowFocus = false;
            // 
            // colBeds
            // 
            resources.ApplyResources(this.colBeds, "colBeds");
            this.colBeds.FieldName = "Beds";
            this.colBeds.Name = "colBeds";
            this.colBeds.OptionsColumn.AllowFocus = false;
            // 
            // colBaths
            // 
            resources.ApplyResources(this.colBaths, "colBaths");
            this.colBaths.FieldName = "Baths";
            this.colBaths.Name = "colBaths";
            this.colBaths.OptionsColumn.AllowFocus = false;
            // 
            // colHouseSize
            // 
            resources.ApplyResources(this.colHouseSize, "colHouseSize");
            this.colHouseSize.ColumnEdit = this.repositoryItemTextEdit2;
            this.colHouseSize.FieldName = "HouseSize";
            this.colHouseSize.Name = "colHouseSize";
            this.colHouseSize.OptionsColumn.AllowFocus = false;
            // 
            // repositoryItemTextEdit2
            // 
            resources.ApplyResources(this.repositoryItemTextEdit2, "repositoryItemTextEdit2");
            this.repositoryItemTextEdit2.DisplayFormat.FormatString = "{0:n} Sq Ft";
            this.repositoryItemTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit2.Mask.EditMask = resources.GetString("repositoryItemTextEdit2.Mask.EditMask");
            this.repositoryItemTextEdit2.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("repositoryItemTextEdit2.Mask.MaskType")));
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colLotSize
            // 
            resources.ApplyResources(this.colLotSize, "colLotSize");
            this.colLotSize.DisplayFormat.FormatString = "{0} Acres";
            this.colLotSize.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLotSize.FieldName = "LotSize";
            this.colLotSize.Name = "colLotSize";
            this.colLotSize.OptionsColumn.AllowFocus = false;
            // 
            // colPrice
            // 
            resources.ApplyResources(this.colPrice, "colPrice");
            this.colPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.OptionsColumn.AllowFocus = false;
            // 
            // repositoryItemTextEdit1
            // 
            resources.ApplyResources(this.repositoryItemTextEdit1, "repositoryItemTextEdit1");
            this.repositoryItemTextEdit1.Mask.EditMask = resources.GetString("repositoryItemTextEdit1.Mask.EditMask");
            this.repositoryItemTextEdit1.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("repositoryItemTextEdit1.Mask.MaskType")));
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat")));
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colFeatures
            // 
            resources.ApplyResources(this.colFeatures, "colFeatures");
            this.colFeatures.FieldName = "Features";
            this.colFeatures.Name = "colFeatures";
            this.colFeatures.OptionsColumn.AllowFocus = false;
            // 
            // colYearBuilt
            // 
            resources.ApplyResources(this.colYearBuilt, "colYearBuilt");
            this.colYearBuilt.FieldName = "YearBuilt";
            this.colYearBuilt.Name = "colYearBuilt";
            this.colYearBuilt.OptionsColumn.AllowFocus = false;
            // 
            // colType
            // 
            resources.ApplyResources(this.colType, "colType");
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            this.colType.OptionsColumn.AllowFocus = false;
            // 
            // colStatus
            // 
            resources.ApplyResources(this.colStatus, "colStatus");
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowFocus = false;
            // 
            // colPhoto
            // 
            resources.ApplyResources(this.colPhoto, "colPhoto");
            this.colPhoto.ColumnEdit = this.repositoryItemPictureEdit1;
            this.colPhoto.FieldName = "Photo";
            this.colPhoto.Name = "colPhoto";
            this.colPhoto.OptionsColumn.AllowFocus = false;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // lcInfo
            // 
            this.lcInfo.AllowHtmlString = true;
            this.lcInfo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcInfo.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lcInfo.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources(this.lcInfo, "lcInfo");
            this.lcInfo.Name = "lcInfo";
            this.lcInfo.StyleController = this.layoutControl1;
            // 
            // editInterestRate
            // 
            resources.ApplyResources(this.editInterestRate, "editInterestRate");
            this.editInterestRate.Name = "editInterestRate";
            this.editInterestRate.Properties.Appearance.Options.UseTextOptions = true;
            this.editInterestRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.editInterestRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("editInterestRate.Properties.Buttons"))))});
            this.editInterestRate.Properties.DropDownRows = 12;
            this.editInterestRate.StyleController = this.layoutControl1;
            // 
            // editTermOfLoan
            // 
            resources.ApplyResources(this.editTermOfLoan, "editTermOfLoan");
            this.editTermOfLoan.Name = "editTermOfLoan";
            this.editTermOfLoan.Properties.Appearance.Options.UseTextOptions = true;
            this.editTermOfLoan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.editTermOfLoan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("editTermOfLoan.Properties.Buttons"))))});
            this.editTermOfLoan.StyleController = this.layoutControl1;
            // 
            // sbCalc
            // 
            resources.ApplyResources(this.sbCalc, "sbCalc");
            this.sbCalc.Name = "sbCalc";
            this.sbCalc.StyleController = this.layoutControl1;
            this.sbCalc.Click += new System.EventHandler(this.sbCalc_Click);
            // 
            // editStart
            // 
            resources.ApplyResources(this.editStart, "editStart");
            this.editStart.Name = "editStart";
            this.editStart.Properties.Appearance.Options.UseTextOptions = true;
            this.editStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.editStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("editStart.Properties.Buttons"))))});
            this.editStart.StyleController = this.layoutControl1;
            // 
            // editLoanAmount
            // 
            resources.ApplyResources(this.editLoanAmount, "editLoanAmount");
            this.editLoanAmount.Name = "editLoanAmount";
            this.editLoanAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.editLoanAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.editLoanAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.editLoanAmount.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.editLoanAmount.Properties.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.editLoanAmount.Properties.Mask.EditMask = resources.GetString("editLoanAmount.Properties.Mask.EditMask");
            this.editLoanAmount.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("editLoanAmount.Properties.Mask.UseMaskAsDisplayFormat")));
            this.editLoanAmount.Properties.MaxValue = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.editLoanAmount.Properties.MinValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.editLoanAmount.StyleController = this.layoutControl1;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            resources.ApplyResources(this.gridSplitContainer1, "gridSplitContainer1");
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            // 
            // gridControl1
            // 
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.Row.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.Row.Font")));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcMonth,
            this.gcBalance,
            this.gcPayment});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(this.gridView1_CustomDrawColumnHeader);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gridView1_CustomDrawFooterCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.gridView1_CustomSummaryCalculate);
            this.gridView1.ShowFilterPopupCheckedListBox += new DevExpress.XtraGrid.Views.Grid.FilterPopupCheckedListBoxEventHandler(this.gridView1_ShowFilterPopupCheckedListBox);
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // gcMonth
            // 
            this.gcMonth.AppearanceCell.Options.UseTextOptions = true;
            this.gcMonth.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            resources.ApplyResources(this.gcMonth, "gcMonth");
            this.gcMonth.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gcMonth.FieldName = "MonthNumber";
            this.gcMonth.Name = "gcMonth";
            this.gcMonth.OptionsColumn.AllowFocus = false;
            this.gcMonth.OptionsColumn.FixedWidth = true;
            this.gcMonth.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            resources.ApplyResources(this.repositoryItemButtonEdit1, "repositoryItemButtonEdit1");
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // gcBalance
            // 
            resources.ApplyResources(this.gcBalance, "gcBalance");
            this.gcBalance.DisplayFormat.FormatString = "c0";
            this.gcBalance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcBalance.FieldName = "Balance";
            this.gcBalance.Name = "gcBalance";
            this.gcBalance.OptionsColumn.AllowFocus = false;
            // 
            // gcPayment
            // 
            resources.ApplyResources(this.gcPayment, "gcPayment");
            this.gcPayment.DisplayFormat.FormatString = "c";
            this.gcPayment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcPayment.FieldName = "MonthlyPayment";
            this.gcPayment.MinWidth = 200;
            this.gcPayment.Name = "gcPayment";
            this.gcPayment.OptionsColumn.AllowFocus = false;
            this.gcPayment.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gcPayment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcPayment.OptionsFilter.AllowAutoFilter = false;
            this.gcPayment.OptionsFilter.AllowFilter = false;
            this.gcPayment.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("gcPayment.Summary"))), resources.GetString("gcPayment.Summary1"), resources.GetString("gcPayment.Summary2"))});
            // 
            // layoutControlGroup1
            // 
            resources.ApplyResources(this.layoutControlGroup1, "layoutControlGroup1");
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(875, 699);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            resources.ApplyResources(this.tabbedControlGroup1, "tabbedControlGroup1");
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(875, 699);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup3
            // 
            resources.ApplyResources(this.layoutControlGroup3, "layoutControlGroup3");
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(851, 653);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridSplitContainer2;
            resources.ApplyResources(this.layoutControlItem4, "layoutControlItem4");
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(851, 621);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.sbSplit;
            resources.ApplyResources(this.layoutControlItem5, "layoutControlItem5");
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 8);
            this.layoutControlItem5.Size = new System.Drawing.Size(123, 32);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.sbRemoveSplit;
            resources.ApplyResources(this.layoutControlItem6, "layoutControlItem6");
            this.layoutControlItem6.Location = new System.Drawing.Point(123, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(126, 32);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            resources.ApplyResources(this.emptySpaceItem1, "emptySpaceItem1");
            this.emptySpaceItem1.Location = new System.Drawing.Point(249, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(602, 32);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            resources.ApplyResources(this.layoutControlGroup2, "layoutControlGroup2");
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.lciCalc,
            this.lciInfo,
            this.layoutControlItem1,
            this.layoutControlItem8});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(851, 653);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.editInterestRate;
            resources.ApplyResources(this.layoutControlItem2, "layoutControlItem2");
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 4);
            this.layoutControlItem2.Size = new System.Drawing.Size(214, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.editTermOfLoan;
            resources.ApplyResources(this.layoutControlItem3, "layoutControlItem3");
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 4);
            this.layoutControlItem3.Size = new System.Drawing.Size(214, 26);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.editStart;
            resources.ApplyResources(this.layoutControlItem7, "layoutControlItem7");
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(214, 24);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(69, 13);
            // 
            // lciCalc
            // 
            this.lciCalc.Control = this.sbCalc;
            resources.ApplyResources(this.lciCalc, "lciCalc");
            this.lciCalc.Location = new System.Drawing.Point(0, 102);
            this.lciCalc.Name = "lciCalc";
            this.lciCalc.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 12, 24);
            this.lciCalc.Size = new System.Drawing.Size(214, 58);
            this.lciCalc.TextSize = new System.Drawing.Size(0, 0);
            this.lciCalc.TextToControlDistance = 0;
            this.lciCalc.TextVisible = false;
            // 
            // lciInfo
            // 
            this.lciInfo.Control = this.lcInfo;
            resources.ApplyResources(this.lciInfo, "lciInfo");
            this.lciInfo.Location = new System.Drawing.Point(0, 160);
            this.lciInfo.MinSize = new System.Drawing.Size(14, 17);
            this.lciInfo.Name = "lciInfo";
            this.lciInfo.Size = new System.Drawing.Size(214, 493);
            this.lciInfo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciInfo.TextSize = new System.Drawing.Size(0, 0);
            this.lciInfo.TextToControlDistance = 0;
            this.lciInfo.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editLoanAmount;
            resources.ApplyResources(this.layoutControlItem1, "layoutControlItem1");
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 4);
            this.layoutControlItem1.Size = new System.Drawing.Size(214, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gridSplitContainer1;
            resources.ApplyResources(this.layoutControlItem8, "layoutControlItem8");
            this.layoutControlItem8.Location = new System.Drawing.Point(214, 0);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
            this.layoutControlItem8.Size = new System.Drawing.Size(637, 653);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // SplitPresentation
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "SplitPresentation";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editInterestRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editTermOfLoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editLoanAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LabelControl lcInfo;
        private DevExpress.XtraEditors.ImageComboBoxEdit editInterestRate;
        private DevExpress.XtraEditors.ImageComboBoxEdit editTermOfLoan;
        private DevExpress.XtraEditors.SimpleButton sbCalc;
        private GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gcMonth;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gcBalance;
        private DevExpress.XtraGrid.Columns.GridColumn gcPayment;
        private DevExpress.XtraEditors.ImageComboBoxEdit editStart;
        private DevExpress.XtraEditors.SpinEdit editLoanAmount;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem lciCalc;
        private DevExpress.XtraLayout.LayoutControlItem lciInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private GridSplitContainer gridSplitContainer1;
        private GridSplitContainer gridSplitContainer2;
        private GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colBeds;
        private DevExpress.XtraGrid.Columns.GridColumn colBaths;
        private DevExpress.XtraGrid.Columns.GridColumn colHouseSize;
        private DevExpress.XtraGrid.Columns.GridColumn colLotSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colFeatures;
        private DevExpress.XtraGrid.Columns.GridColumn colYearBuilt;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPhoto;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.SimpleButton sbRemoveSplit;
        private DevExpress.XtraEditors.SimpleButton sbSplit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
