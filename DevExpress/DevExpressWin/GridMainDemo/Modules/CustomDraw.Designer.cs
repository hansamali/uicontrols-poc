namespace DevExpress.XtraGrid.Demos {
    partial class CustomDraw {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomDraw));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.cardView1 = new DevExpress.XtraGrid.Views.Card.CardView();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colUnitPrice1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dsNWindProducts1 = new DevExpress.XtraGrid.Demos.dsNWindProducts();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colProductName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCategoryID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imlCategories = new System.Windows.Forms.ImageList(this.components);
            this.colProductID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSupplierID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colUnitPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQuantityPerUnit = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colReorderLevel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDiscontinued = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUnitsOnOrder = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUnitsInStock = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.imlIndicator = new System.Windows.Forms.ImageList(this.components);
            this.toolBar1 = new System.Windows.Forms.ToolBar();
            this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton3 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton4 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton5 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton6 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton7 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton8 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton9 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton10 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton11 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton12 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton13 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton14 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton15 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton16 = new System.Windows.Forms.ToolBarButton();
            this.imlToolBar = new System.Windows.Forms.ImageList(this.components);
            this.imlCustomDrawImages = new System.Windows.Forms.ImageList(this.components);
            this.imlFilterShapes = new System.Windows.Forms.ImageList(this.components);
            this.imlSortShapes = new System.Windows.Forms.ImageList(this.components);
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLink = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colPicture = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colPlatform = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsNWindProducts1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cardView1
            // 
            this.cardView1.Appearance.Card.BackColor = System.Drawing.Color.Aquamarine;
            this.cardView1.Appearance.Card.Options.UseBackColor = true;
            this.cardView1.Appearance.CardButton.Font = new System.Drawing.Font("Arial", 8.25F);
            this.cardView1.Appearance.CardButton.Options.UseFont = true;
            this.cardView1.Appearance.CardCaption.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.cardView1.Appearance.CardCaption.ForeColor = System.Drawing.Color.Black;
            this.cardView1.Appearance.CardCaption.Options.UseFont = true;
            this.cardView1.Appearance.CardCaption.Options.UseForeColor = true;
            this.cardView1.Appearance.EmptySpace.BackColor = System.Drawing.Color.Transparent;
            this.cardView1.Appearance.EmptySpace.Font = new System.Drawing.Font("Arial", 8.25F);
            this.cardView1.Appearance.EmptySpace.Options.UseBackColor = true;
            this.cardView1.Appearance.EmptySpace.Options.UseFont = true;
            this.cardView1.Appearance.FieldCaption.BackColor = System.Drawing.Color.Aquamarine;
            this.cardView1.Appearance.FieldCaption.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.cardView1.Appearance.FieldCaption.Options.UseBackColor = true;
            this.cardView1.Appearance.FieldCaption.Options.UseFont = true;
            this.cardView1.Appearance.FieldValue.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.cardView1.Appearance.FieldValue.Font = new System.Drawing.Font("Arial", 8.25F);
            this.cardView1.Appearance.FieldValue.Options.UseBackColor = true;
            this.cardView1.Appearance.FieldValue.Options.UseFont = true;
            this.cardView1.Appearance.FocusedCardCaption.BorderColor = System.Drawing.Color.Navy;
            this.cardView1.Appearance.FocusedCardCaption.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.cardView1.Appearance.FocusedCardCaption.ForeColor = System.Drawing.Color.White;
            this.cardView1.Appearance.FocusedCardCaption.Options.UseBorderColor = true;
            this.cardView1.Appearance.FocusedCardCaption.Options.UseFont = true;
            this.cardView1.Appearance.FocusedCardCaption.Options.UseForeColor = true;
            this.cardView1.Appearance.HideSelectionCardCaption.ForeColor = System.Drawing.Color.White;
            this.cardView1.Appearance.HideSelectionCardCaption.Options.UseForeColor = true;
            this.cardView1.Appearance.SeparatorLine.Font = new System.Drawing.Font("Arial", 8.25F);
            this.cardView1.Appearance.SeparatorLine.Options.UseFont = true;
            this.cardView1.CardCaptionFormat = "Order ID: {1}";
            this.cardView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOrderID,
            this.colQuantity,
            this.colUnitPrice1,
            this.colDiscount,
            this.colSubTotal});
            this.cardView1.DetailHeight = 150;
            this.cardView1.FocusedCardTopFieldIndex = 0;
            this.cardView1.GridControl = this.gridControl1;
            this.cardView1.Name = "cardView1";
            this.cardView1.OptionsView.ShowCardExpandButton = false;
            this.cardView1.OptionsView.ShowQuickCustomizeButton = false;
            this.cardView1.CustomDrawCardFieldValue += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.cardView1_CustomDrawCardFieldValue);
            this.cardView1.CustomDrawCardCaption += new DevExpress.XtraGrid.Views.Card.CardCaptionCustomDrawEventHandler(this.cardView1_CustomDrawCardCaption);
            this.cardView1.CustomDrawCardFieldCaption += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.cardView1_CustomDrawCardFieldCaption);
            // 
            // colOrderID
            // 
            this.colOrderID.Caption = "Order ID";
            this.colOrderID.FieldName = "OrderID";
            this.colOrderID.Name = "colOrderID";
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Quantity";
            this.colQuantity.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 1;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // colUnitPrice1
            // 
            this.colUnitPrice1.Caption = "Unit Price";
            this.colUnitPrice1.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colUnitPrice1.DisplayFormat.FormatString = "c";
            this.colUnitPrice1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUnitPrice1.FieldName = "UnitPrice";
            this.colUnitPrice1.Name = "colUnitPrice1";
            this.colUnitPrice1.Visible = true;
            this.colUnitPrice1.VisibleIndex = 0;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colDiscount
            // 
            this.colDiscount.Caption = "Discount";
            this.colDiscount.DisplayFormat.FormatString = "p";
            this.colDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.Visible = true;
            this.colDiscount.VisibleIndex = 2;
            // 
            // colSubTotal
            // 
            this.colSubTotal.Caption = "Sub Total";
            this.colSubTotal.DisplayFormat.FormatString = "c";
            this.colSubTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSubTotal.FieldName = "SubTotal";
            this.colSubTotal.Name = "colSubTotal";
            this.colSubTotal.OptionsColumn.AllowEdit = false;
            this.colSubTotal.OptionsColumn.ReadOnly = true;
            this.colSubTotal.Visible = true;
            this.colSubTotal.VisibleIndex = 3;
            // 
            // gridControl1
            // 
            this.gridControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl1.BackgroundImage")));
            this.gridControl1.DataSource = this.dsNWindProducts1.Products;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Name = "";
            gridLevelNode1.LevelTemplate = this.cardView1;
            gridLevelNode1.RelationName = "ProductsOrder_x0020_Details";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 32);
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemCalcEdit1,
            this.repositoryItemSpinEdit1});
            this.gridControl1.Size = new System.Drawing.Size(711, 458);
            this.gridControl1.TabIndex = 11;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1,
            this.cardView1});
            // 
            // dsNWindProducts1
            // 
            this.dsNWindProducts1.DataSetName = "dsNWindProducts";
            this.dsNWindProducts1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsNWindProducts1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Appearance.BandPanel.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.advBandedGridView1.Appearance.BandPanel.Options.UseFont = true;
            this.advBandedGridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.WhiteSmoke;
            this.advBandedGridView1.Appearance.EvenRow.Font = new System.Drawing.Font("Arial", 8.25F);
            this.advBandedGridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.advBandedGridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.EvenRow.Options.UseFont = true;
            this.advBandedGridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("Arial", 8.25F);
            this.advBandedGridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.advBandedGridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.DarkBlue;
            this.advBandedGridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.SteelBlue;
            this.advBandedGridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Arial", 8.25F);
            this.advBandedGridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.advBandedGridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.advBandedGridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Bold);
            this.advBandedGridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.advBandedGridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Bold);
            this.advBandedGridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.advBandedGridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.DimGray;
            this.advBandedGridView1.Appearance.HideSelectionRow.BackColor2 = System.Drawing.Color.Silver;
            this.advBandedGridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Arial", 8.25F);
            this.advBandedGridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.advBandedGridView1.Appearance.HideSelectionRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.advBandedGridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.advBandedGridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.DarkGray;
            this.advBandedGridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.OddRow.BackColor = System.Drawing.Color.AntiqueWhite;
            this.advBandedGridView1.Appearance.OddRow.BackColor2 = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.OddRow.Font = new System.Drawing.Font("Arial", 8.25F);
            this.advBandedGridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.OddRow.Options.UseFont = true;
            this.advBandedGridView1.Appearance.Row.Font = new System.Drawing.Font("Arial", 8.25F);
            this.advBandedGridView1.Appearance.Row.Options.UseFont = true;
            this.advBandedGridView1.Appearance.VertLine.BackColor = System.Drawing.Color.DarkGray;
            this.advBandedGridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colProductID,
            this.colProductName,
            this.colSupplierID,
            this.colCategoryID,
            this.colQuantityPerUnit,
            this.colUnitPrice,
            this.colUnitsInStock,
            this.colUnitsOnOrder,
            this.colReorderLevel,
            this.colDiscontinued});
            styleFormatCondition1.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Strikeout);
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.ControlDark;
            styleFormatCondition1.Appearance.Options.UseFont = true;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colDiscontinued;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = true;
            this.advBandedGridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.GroupCount = 1;
            this.advBandedGridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "ProductID", null, "")});
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsDetail.AllowZoomDetail = false;
            this.advBandedGridView1.OptionsDetail.ShowDetailTabs = false;
            this.advBandedGridView1.OptionsNavigation.UseAdvVertNavigation = false;
            this.advBandedGridView1.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.advBandedGridView1.OptionsView.GroupDrawMode = DevExpress.XtraGrid.Views.Grid.GroupDrawMode.Standard;
            this.advBandedGridView1.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.Button;
            this.advBandedGridView1.OptionsView.ShowFooter = true;
            this.advBandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSupplierID, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDiscontinued, DevExpress.Data.ColumnSortOrder.Descending)});
            this.advBandedGridView1.CustomDrawGroupPanel += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.advBandedGridView1_CustomDrawGroupPanel);
            this.advBandedGridView1.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.advBandedGridView1_CustomDrawFooterCell);
            this.advBandedGridView1.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.advBandedGridView1_CustomDrawGroupRow);
            this.advBandedGridView1.GroupLevelStyle += new DevExpress.XtraGrid.Views.Grid.GroupLevelStyleEventHandler(this.advBandedGridView1_GroupLevelStyle);
            this.advBandedGridView1.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.advBandedGridView1_CustomDrawRowIndicator);
            this.advBandedGridView1.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.advBandedGridView1_FocusedColumnChanged);
            this.advBandedGridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.advBandedGridView1_CustomDrawCell);
            this.advBandedGridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.advBandedGridView1_RowCellStyle);
            this.advBandedGridView1.CustomDrawFilterPanel += new DevExpress.XtraGrid.Views.Base.CustomDrawObjectEventHandler(this.advBandedGridView1_CustomDrawFilterPanel);
            this.advBandedGridView1.CustomDrawBandHeader += new DevExpress.XtraGrid.Views.BandedGrid.BandHeaderCustomDrawEventHandler(this.advBandedGridView1_CustomDrawBandHeader);
            this.advBandedGridView1.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(this.advBandedGridView1_CustomDrawColumnHeader);
            this.advBandedGridView1.CustomDrawFooter += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.advBandedGridView1_CustomDrawFooter);
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Product";
            this.gridBand1.Columns.Add(this.colProductName);
            this.gridBand1.Columns.Add(this.colCategoryID);
            this.gridBand1.Columns.Add(this.colProductID);
            this.gridBand1.Columns.Add(this.colSupplierID);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 300;
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Product Name";
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.colProductName.Visible = true;
            this.colProductName.Width = 300;
            // 
            // colCategoryID
            // 
            this.colCategoryID.Caption = "Category";
            this.colCategoryID.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCategoryID.FieldName = "CategoryID";
            this.colCategoryID.Name = "colCategoryID";
            this.colCategoryID.RowIndex = 1;
            this.colCategoryID.Visible = true;
            this.colCategoryID.Width = 151;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Beverages", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Condiments", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Confections", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dairy Products", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Grains/Cereals", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Meat/Poultry", 6, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Produce", 7, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Seafood", 8, 7)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.imlCategories;
            // 
            // imlCategories
            // 
            this.imlCategories.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlCategories.ImageStream")));
            this.imlCategories.TransparentColor = System.Drawing.Color.Magenta;
            this.imlCategories.Images.SetKeyName(0, "");
            this.imlCategories.Images.SetKeyName(1, "");
            this.imlCategories.Images.SetKeyName(2, "");
            this.imlCategories.Images.SetKeyName(3, "");
            this.imlCategories.Images.SetKeyName(4, "");
            this.imlCategories.Images.SetKeyName(5, "");
            this.imlCategories.Images.SetKeyName(6, "");
            this.imlCategories.Images.SetKeyName(7, "");
            // 
            // colProductID
            // 
            this.colProductID.Caption = "Product ID";
            this.colProductID.FieldName = "ProductID";
            this.colProductID.Name = "colProductID";
            this.colProductID.Width = 59;
            // 
            // colSupplierID
            // 
            this.colSupplierID.Caption = "Supplier";
            this.colSupplierID.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.Name = "colSupplierID";
            this.colSupplierID.RowIndex = 1;
            this.colSupplierID.Visible = true;
            this.colSupplierID.Width = 149;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyName", "Company Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)});
            this.repositoryItemLookUpEdit1.DataSource = this.dsNWindProducts1.Suppliers;
            this.repositoryItemLookUpEdit1.DisplayMember = "CompanyName";
            this.repositoryItemLookUpEdit1.DropDownRows = 10;
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.PopupWidth = 200;
            this.repositoryItemLookUpEdit1.ValueMember = "SupplierID";
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Price";
            this.gridBand2.Columns.Add(this.colUnitPrice);
            this.gridBand2.Columns.Add(this.colQuantityPerUnit);
            this.gridBand2.Columns.Add(this.colReorderLevel);
            this.gridBand2.Columns.Add(this.colDiscontinued);
            this.gridBand2.Columns.Add(this.colUnitsOnOrder);
            this.gridBand2.Columns.Add(this.colUnitsInStock);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 412;
            // 
            // colUnitPrice
            // 
            this.colUnitPrice.Caption = "Unit Price";
            this.colUnitPrice.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colUnitPrice.DisplayFormat.FormatString = "c";
            this.colUnitPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUnitPrice.FieldName = "UnitPrice";
            this.colUnitPrice.Name = "colUnitPrice";
            this.colUnitPrice.SummaryItem.DisplayFormat = "SUM={0:c}";
            this.colUnitPrice.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colUnitPrice.Visible = true;
            this.colUnitPrice.Width = 135;
            // 
            // colQuantityPerUnit
            // 
            this.colQuantityPerUnit.Caption = "Quantity Per Unit";
            this.colQuantityPerUnit.FieldName = "QuantityPerUnit";
            this.colQuantityPerUnit.Name = "colQuantityPerUnit";
            this.colQuantityPerUnit.Visible = true;
            this.colQuantityPerUnit.Width = 127;
            // 
            // colReorderLevel
            // 
            this.colReorderLevel.Caption = "Reorder Level";
            this.colReorderLevel.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colReorderLevel.FieldName = "ReorderLevel";
            this.colReorderLevel.Name = "colReorderLevel";
            this.colReorderLevel.RowIndex = 1;
            this.colReorderLevel.SummaryItem.DisplayFormat = "MAX={0}";
            this.colReorderLevel.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Max;
            this.colReorderLevel.Visible = true;
            this.colReorderLevel.Width = 135;
            // 
            // colDiscontinued
            // 
            this.colDiscontinued.Caption = "Discontinued";
            this.colDiscontinued.FieldName = "Discontinued";
            this.colDiscontinued.Name = "colDiscontinued";
            this.colDiscontinued.Visible = true;
            this.colDiscontinued.Width = 150;
            // 
            // colUnitsOnOrder
            // 
            this.colUnitsOnOrder.Caption = "Units On Order";
            this.colUnitsOnOrder.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colUnitsOnOrder.FieldName = "UnitsOnOrder";
            this.colUnitsOnOrder.Name = "colUnitsOnOrder";
            this.colUnitsOnOrder.RowIndex = 1;
            this.colUnitsOnOrder.Visible = true;
            this.colUnitsOnOrder.Width = 127;
            // 
            // colUnitsInStock
            // 
            this.colUnitsInStock.Caption = "Units In Stock";
            this.colUnitsInStock.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colUnitsInStock.FieldName = "UnitsInStock";
            this.colUnitsInStock.Name = "colUnitsInStock";
            this.colUnitsInStock.RowIndex = 1;
            this.colUnitsInStock.SummaryItem.DisplayFormat = "AVG={0:#.##}";
            this.colUnitsInStock.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Average;
            this.colUnitsInStock.Visible = true;
            this.colUnitsInStock.Width = 150;
            // 
            // imlIndicator
            // 
            this.imlIndicator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlIndicator.ImageStream")));
            this.imlIndicator.TransparentColor = System.Drawing.Color.Magenta;
            this.imlIndicator.Images.SetKeyName(0, "");
            this.imlIndicator.Images.SetKeyName(1, "");
            this.imlIndicator.Images.SetKeyName(2, "");
            this.imlIndicator.Images.SetKeyName(3, "");
            this.imlIndicator.Images.SetKeyName(4, "");
            this.imlIndicator.Images.SetKeyName(5, "");
            // 
            // toolBar1
            // 
            this.toolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.toolBarButton1,
            this.toolBarButton2,
            this.toolBarButton3,
            this.toolBarButton4,
            this.toolBarButton5,
            this.toolBarButton6,
            this.toolBarButton7,
            this.toolBarButton8,
            this.toolBarButton9,
            this.toolBarButton10,
            this.toolBarButton11,
            this.toolBarButton12,
            this.toolBarButton13,
            this.toolBarButton14,
            this.toolBarButton15,
            this.toolBarButton16});
            this.toolBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBar1.DropDownArrows = true;
            this.toolBar1.ImageList = this.imlToolBar;
            this.toolBar1.Location = new System.Drawing.Point(0, 0);
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.ShowToolTips = true;
            this.toolBar1.Size = new System.Drawing.Size(711, 28);
            this.toolBar1.TabIndex = 10;
            this.toolBar1.Wrappable = false;
            this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // toolBarButton1
            // 
            this.toolBarButton1.ImageIndex = 0;
            this.toolBarButton1.Name = "toolBarButton1";
            this.toolBarButton1.Pushed = true;
            this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton1.ToolTipText = "Custom Draw Group Panel";
            // 
            // toolBarButton2
            // 
            this.toolBarButton2.ImageIndex = 1;
            this.toolBarButton2.Name = "toolBarButton2";
            this.toolBarButton2.Pushed = true;
            this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton2.ToolTipText = "Custom Draw Band Header";
            // 
            // toolBarButton3
            // 
            this.toolBarButton3.ImageIndex = 2;
            this.toolBarButton3.Name = "toolBarButton3";
            this.toolBarButton3.Pushed = true;
            this.toolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton3.ToolTipText = "Custom Draw Column Header";
            // 
            // toolBarButton4
            // 
            this.toolBarButton4.ImageIndex = 3;
            this.toolBarButton4.Name = "toolBarButton4";
            this.toolBarButton4.Pushed = true;
            this.toolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton4.ToolTipText = "Custom Draw Sorted Shapes and Filter Buttons";
            // 
            // toolBarButton5
            // 
            this.toolBarButton5.ImageIndex = 4;
            this.toolBarButton5.Name = "toolBarButton5";
            this.toolBarButton5.Pushed = true;
            this.toolBarButton5.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton5.ToolTipText = "Custom Draw Row Indicator";
            // 
            // toolBarButton6
            // 
            this.toolBarButton6.ImageIndex = 5;
            this.toolBarButton6.Name = "toolBarButton6";
            this.toolBarButton6.Pushed = true;
            this.toolBarButton6.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton6.ToolTipText = "Custom Draw Group Row";
            // 
            // toolBarButton7
            // 
            this.toolBarButton7.ImageIndex = 6;
            this.toolBarButton7.Name = "toolBarButton7";
            this.toolBarButton7.Pushed = true;
            this.toolBarButton7.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton7.ToolTipText = "Custom Draw Footer";
            // 
            // toolBarButton8
            // 
            this.toolBarButton8.ImageIndex = 7;
            this.toolBarButton8.Name = "toolBarButton8";
            this.toolBarButton8.Pushed = true;
            this.toolBarButton8.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton8.ToolTipText = "Custom Draw Footer Cell";
            // 
            // toolBarButton9
            // 
            this.toolBarButton9.ImageIndex = 8;
            this.toolBarButton9.Name = "toolBarButton9";
            this.toolBarButton9.Pushed = true;
            this.toolBarButton9.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton9.ToolTipText = "Custom Draw Cell";
            // 
            // toolBarButton10
            // 
            this.toolBarButton10.ImageIndex = 9;
            this.toolBarButton10.Name = "toolBarButton10";
            this.toolBarButton10.Pushed = true;
            this.toolBarButton10.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton10.ToolTipText = "Custom Draw Filter Panel";
            // 
            // toolBarButton11
            // 
            this.toolBarButton11.Name = "toolBarButton11";
            this.toolBarButton11.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // toolBarButton12
            // 
            this.toolBarButton12.ImageIndex = 10;
            this.toolBarButton12.Name = "toolBarButton12";
            this.toolBarButton12.Pushed = true;
            this.toolBarButton12.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton12.ToolTipText = "Custom Draw Card Caption";
            // 
            // toolBarButton13
            // 
            this.toolBarButton13.ImageIndex = 11;
            this.toolBarButton13.Name = "toolBarButton13";
            this.toolBarButton13.Pushed = true;
            this.toolBarButton13.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton13.ToolTipText = "Custom Draw Card Field Caption";
            // 
            // toolBarButton14
            // 
            this.toolBarButton14.ImageIndex = 12;
            this.toolBarButton14.Name = "toolBarButton14";
            this.toolBarButton14.Pushed = true;
            this.toolBarButton14.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton14.ToolTipText = "Custom Draw Card Field Value";
            // 
            // toolBarButton15
            // 
            this.toolBarButton15.Name = "toolBarButton15";
            this.toolBarButton15.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // toolBarButton16
            // 
            this.toolBarButton16.ImageIndex = 13;
            this.toolBarButton16.Name = "toolBarButton16";
            this.toolBarButton16.Pushed = true;
            this.toolBarButton16.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.toolBarButton16.ToolTipText = "Draw BackGround";
            // 
            // imlToolBar
            // 
            this.imlToolBar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolBar.ImageStream")));
            this.imlToolBar.TransparentColor = System.Drawing.Color.Magenta;
            this.imlToolBar.Images.SetKeyName(0, "");
            this.imlToolBar.Images.SetKeyName(1, "");
            this.imlToolBar.Images.SetKeyName(2, "");
            this.imlToolBar.Images.SetKeyName(3, "");
            this.imlToolBar.Images.SetKeyName(4, "");
            this.imlToolBar.Images.SetKeyName(5, "");
            this.imlToolBar.Images.SetKeyName(6, "");
            this.imlToolBar.Images.SetKeyName(7, "");
            this.imlToolBar.Images.SetKeyName(8, "");
            this.imlToolBar.Images.SetKeyName(9, "");
            this.imlToolBar.Images.SetKeyName(10, "");
            this.imlToolBar.Images.SetKeyName(11, "");
            this.imlToolBar.Images.SetKeyName(12, "");
            this.imlToolBar.Images.SetKeyName(13, "");
            // 
            // imlCustomDrawImages
            // 
            this.imlCustomDrawImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlCustomDrawImages.ImageStream")));
            this.imlCustomDrawImages.TransparentColor = System.Drawing.Color.Magenta;
            this.imlCustomDrawImages.Images.SetKeyName(0, "");
            this.imlCustomDrawImages.Images.SetKeyName(1, "");
            this.imlCustomDrawImages.Images.SetKeyName(2, "");
            this.imlCustomDrawImages.Images.SetKeyName(3, "");
            // 
            // imlFilterShapes
            // 
            this.imlFilterShapes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlFilterShapes.ImageStream")));
            this.imlFilterShapes.TransparentColor = System.Drawing.Color.Magenta;
            this.imlFilterShapes.Images.SetKeyName(0, "");
            this.imlFilterShapes.Images.SetKeyName(1, "");
            this.imlFilterShapes.Images.SetKeyName(2, "");
            this.imlFilterShapes.Images.SetKeyName(3, "");
            this.imlFilterShapes.Images.SetKeyName(4, "");
            this.imlFilterShapes.Images.SetKeyName(5, "");
            // 
            // imlSortShapes
            // 
            this.imlSortShapes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlSortShapes.ImageStream")));
            this.imlSortShapes.TransparentColor = System.Drawing.Color.Magenta;
            this.imlSortShapes.Images.SetKeyName(0, "");
            this.imlSortShapes.Images.SetKeyName(1, "");
            // 
            // gridControl2
            // 
            this.gridControl2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl2.BackgroundImage")));
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Name = "";
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl2.MainView = this.gridView1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemSpinEdit2,
            this.repositoryItemImageComboBox2,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl2.Size = new System.Drawing.Size(711, 490);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLink,
            this.colPicture,
            this.colPlatform,
            this.colPrice,
            this.colProduct});
            this.gridView1.GridControl = this.gridControl2;
            this.gridView1.GroupCount = 1;
            this.gridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Price", this.colPrice, "SUM={0:c}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "ProductName", this.colProduct, "")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.UseTabKey = false;
            this.gridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView1.OptionsView.ShowPreview = true;
            this.gridView1.PaintStyleName = "MixedXP";
            this.gridView1.PreviewFieldName = "Description";
            this.gridView1.RowHeight = 48;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPlatform, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gridView1_CustomDrawRowIndicator);
            this.gridView1.CustomDrawRowFooter += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridView1_CustomDrawRowFooter);
            this.gridView1.CustomDrawRowPreview += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridView1_CustomDrawRowPreview);
            this.gridView1.CustomDrawRowFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gridView1_CustomDrawRowFooterCell);
            this.gridView1.CustomDrawFilterPanel += new DevExpress.XtraGrid.Views.Base.CustomDrawObjectEventHandler(this.gridView1_CustomDrawFilterPanel);
            this.gridView1.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(this.gridView1_CustomDrawColumnHeader);
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            // 
            // colLink
            // 
            this.colLink.Caption = "Link";
            this.colLink.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLink.FieldName = "Link";
            this.colLink.Name = "colLink";
            this.colLink.OptionsColumn.ReadOnly = true;
            this.colLink.Visible = true;
            this.colLink.VisibleIndex = 3;
            this.colLink.Width = 162;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            // 
            // colPicture
            // 
            this.colPicture.Caption = "Picture";
            this.colPicture.ColumnEdit = this.repositoryItemPictureEdit1;
            this.colPicture.FieldName = "Picture";
            this.colPicture.Name = "colPicture";
            this.colPicture.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colPicture.OptionsColumn.AllowSize = false;
            this.colPicture.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPicture.OptionsColumn.FixedWidth = true;
            this.colPicture.OptionsFilter.AllowFilter = false;
            this.colPicture.Visible = true;
            this.colPicture.VisibleIndex = 1;
            this.colPicture.Width = 220;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // colPlatform
            // 
            this.colPlatform.Caption = "Platform";
            this.colPlatform.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colPlatform.FieldName = "Platform";
            this.colPlatform.Name = "colPlatform";
            this.colPlatform.Width = 100;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("VCL", 0, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("ActiveX", 1, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".NET", 2, -1)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colPrice
            // 
            this.colPrice.Caption = "Price";
            this.colPrice.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colPrice.DisplayFormat.FormatString = "c";
            this.colPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.Visible = true;
            this.colPrice.VisibleIndex = 2;
            this.colPrice.Width = 101;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // colProduct
            // 
            this.colProduct.Caption = "ProductName";
            this.colProduct.FieldName = "ProductName";
            this.colProduct.Name = "colProduct";
            this.colProduct.Visible = true;
            this.colProduct.VisibleIndex = 0;
            this.colProduct.Width = 154;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 73);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 70);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FilterPanel", 50);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 70);
            this.xtraGridBlending1.GridControl = this.gridControl2;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(720, 521);
            this.xtraTabControl1.TabIndex = 13;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            this.xtraTabControl1.Text = "xtraTabControl1";
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.panel1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(711, 490);
            this.xtraTabPage1.Text = "Custom Draw (full features)";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toolBar1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(711, 32);
            this.panel1.TabIndex = 12;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(711, 490);
            this.xtraTabPage2.Text = "Custom Draw (mix example)";
            // 
            // CustomDraw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "CustomDraw";
            this.Size = new System.Drawing.Size(720, 521);
            this.Load += new System.EventHandler(this.CustomDraw_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsNWindProducts1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.xtraTabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolBar toolBar1;
        private System.Windows.Forms.ToolBarButton toolBarButton1;
        private System.Windows.Forms.ToolBarButton toolBarButton2;
        private System.Windows.Forms.ToolBarButton toolBarButton3;
        private System.Windows.Forms.ToolBarButton toolBarButton4;
        private System.Windows.Forms.ToolBarButton toolBarButton5;
        private System.Windows.Forms.ImageList imlToolBar;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colProductName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCategoryID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colProductID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSupplierID;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUnitPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQuantityPerUnit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colReorderLevel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDiscontinued;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUnitsOnOrder;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUnitsInStock;
        private dsNWindProducts dsNWindProducts1;
        private DevExpress.XtraGrid.Views.Card.CardView cardView1;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitPrice1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubTotal;
        private System.Windows.Forms.ImageList imlIndicator;
        private System.Windows.Forms.ImageList imlCategories;
        private System.Windows.Forms.ImageList imlCustomDrawImages;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private System.Windows.Forms.ImageList imlFilterShapes;
        private System.Windows.Forms.ImageList imlSortShapes;
        private System.Windows.Forms.ToolBarButton toolBarButton6;
        private System.Windows.Forms.ToolBarButton toolBarButton7;
        private System.Windows.Forms.ToolBarButton toolBarButton8;
        private System.Windows.Forms.ToolBarButton toolBarButton9;
        private System.Windows.Forms.ToolBarButton toolBarButton10;
        private System.Windows.Forms.ToolBarButton toolBarButton11;
        private System.Windows.Forms.ToolBarButton toolBarButton12;
        private System.Windows.Forms.ToolBarButton toolBarButton13;
        private System.Windows.Forms.ToolBarButton toolBarButton14;
        private System.Windows.Forms.ToolBarButton toolBarButton15;
        private System.Windows.Forms.ToolBarButton toolBarButton16;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colLink;
        private DevExpress.XtraGrid.Columns.GridColumn colPicture;
        private DevExpress.XtraGrid.Columns.GridColumn colPlatform;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colProduct;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private System.ComponentModel.IContainer components;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private System.Windows.Forms.Panel panel1;
    }
}
