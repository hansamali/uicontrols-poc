namespace DevExpress.XtraGrid.Demos {
    partial class ViewStyles {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewStyles));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet1 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet1 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon1 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon2 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon3 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet2 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet2 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon4 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon5 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon6 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet3 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet3 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon7 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon8 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon9 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet4 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet4 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon10 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon11 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon12 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule7 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRule2ColorScale formatConditionRule2ColorScale1 = new DevExpress.XtraEditors.FormatConditionRule2ColorScale();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule8 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule9 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet5 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet5 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon13 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon14 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon15 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule10 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleIconSet formatConditionRuleIconSet6 = new DevExpress.XtraEditors.FormatConditionRuleIconSet();
            DevExpress.XtraEditors.FormatConditionIconSet formatConditionIconSet6 = new DevExpress.XtraEditors.FormatConditionIconSet();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon16 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon17 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            DevExpress.XtraEditors.FormatConditionIconSetIcon formatConditionIconSetIcon18 = new DevExpress.XtraEditors.FormatConditionIconSetIcon();
            this.bgcInStock = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcMPGCity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcMPGHighway = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gcInStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMPGCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMPGHighway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bcInStock = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcMPGCity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcMPGHighway = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chAdvBanded = new DevExpress.XtraEditors.CheckButton();
            this.chBanded = new DevExpress.XtraEditors.CheckButton();
            this.chCard = new DevExpress.XtraEditors.CheckButton();
            this.chGrid = new DevExpress.XtraEditors.CheckButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gbMain = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcTrademark = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcModification = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcLogo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcDescription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gbPerfomance = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcHorsepower = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcTorque = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcDoors = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcCylinders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcTransmissionType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcTransmissionSpeeds = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbImage = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcPhoto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbInfo = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.pgcCategory = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcBodyStyle = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcDeliveryDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTrademark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcModification = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDoors = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcBodyStyle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcCylinders = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcHorsepower = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTorque = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTransmissionSpeeds = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTransmissionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcImage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPhoto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcLogo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcTrademark = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcModification = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcCategory = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcBodyStyle = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcHorsepower = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcTorque = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcPhoto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcDescription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcDoors = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcCylinders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcTransmissionSpeeds = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcTransmissionType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcImage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcDeliveryDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.cardView1 = new DevExpress.XtraGrid.Views.Card.CardView();
            this.colTrademark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModification = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBodyStyle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhoto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInStock = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).BeginInit();
            this.SuspendLayout();
            // 
            // bgcInStock
            // 
            this.bgcInStock.FieldName = "InStock";
            this.bgcInStock.Name = "bgcInStock";
            // 
            // bgcMPGCity
            // 
            resources.ApplyResources(this.bgcMPGCity, "bgcMPGCity");
            this.bgcMPGCity.FieldName = "MPGCity";
            this.bgcMPGCity.Name = "bgcMPGCity";
            this.bgcMPGCity.RowIndex = 1;
            // 
            // bgcMPGHighway
            // 
            resources.ApplyResources(this.bgcMPGHighway, "bgcMPGHighway");
            this.bgcMPGHighway.FieldName = "MPGHighway";
            this.bgcMPGHighway.Name = "bgcMPGHighway";
            this.bgcMPGHighway.RowIndex = 1;
            // 
            // gcInStock
            // 
            this.gcInStock.FieldName = "InStock";
            this.gcInStock.Name = "gcInStock";
            resources.ApplyResources(this.gcInStock, "gcInStock");
            // 
            // gcMPGCity
            // 
            this.gcMPGCity.FieldName = "MPGCity";
            this.gcMPGCity.Name = "gcMPGCity";
            resources.ApplyResources(this.gcMPGCity, "gcMPGCity");
            // 
            // gcMPGHighway
            // 
            this.gcMPGHighway.FieldName = "MPGHighway";
            this.gcMPGHighway.Name = "gcMPGHighway";
            resources.ApplyResources(this.gcMPGHighway, "gcMPGHighway");
            // 
            // gcPrice
            // 
            this.gcPrice.FieldName = "Price";
            this.gcPrice.Name = "gcPrice";
            resources.ApplyResources(this.gcPrice, "gcPrice");
            // 
            // bcInStock
            // 
            this.bcInStock.FieldName = "InStock";
            this.bcInStock.Name = "bcInStock";
            resources.ApplyResources(this.bcInStock, "bcInStock");
            // 
            // bcMPGCity
            // 
            this.bcMPGCity.FieldName = "MPGCity";
            this.bcMPGCity.Name = "bcMPGCity";
            resources.ApplyResources(this.bcMPGCity, "bcMPGCity");
            // 
            // bcMPGHighway
            // 
            this.bcMPGHighway.FieldName = "MPGHighway";
            this.bcMPGHighway.Name = "bcMPGHighway";
            resources.ApplyResources(this.bcMPGHighway, "bcMPGHighway");
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AllowFocused = false;
            resources.ApplyResources(this.repositoryItemImageComboBox3, "repositoryItemImageComboBox3");
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemImageComboBox3.Buttons"))))});
            this.repositoryItemImageComboBox3.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(resources.GetString("repositoryItemImageComboBox3.Items"), ((object)(resources.GetObject("repositoryItemImageComboBox3.Items1"))), ((int)(resources.GetObject("repositoryItemImageComboBox3.Items2")))),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(resources.GetString("repositoryItemImageComboBox3.Items3"), ((object)(resources.GetObject("repositoryItemImageComboBox3.Items4"))), ((int)(resources.GetObject("repositoryItemImageComboBox3.Items5")))),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(resources.GetString("repositoryItemImageComboBox3.Items6"), ((object)(resources.GetObject("repositoryItemImageComboBox3.Items7"))), ((int)(resources.GetObject("repositoryItemImageComboBox3.Items8"))))});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AllowFocused = false;
            resources.ApplyResources(this.repositoryItemSpinEdit3, "repositoryItemSpinEdit3");
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "GridView.png");
            this.imageList1.Images.SetKeyName(1, "CardView.png");
            this.imageList1.Images.SetKeyName(2, "BandView.png");
            this.imageList1.Images.SetKeyName(3, "ABandView.png");
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.chAdvBanded);
            this.panelControl1.Controls.Add(this.chBanded);
            this.panelControl1.Controls.Add(this.chCard);
            this.panelControl1.Controls.Add(this.chGrid);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            // 
            // chAdvBanded
            // 
            this.chAdvBanded.AllowFocus = false;
            this.chAdvBanded.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.chAdvBanded.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("chAdvBanded.Appearance.FontStyleDelta")));
            this.chAdvBanded.Appearance.Options.UseFont = true;
            this.chAdvBanded.Appearance.Options.UseTextOptions = true;
            this.chAdvBanded.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chAdvBanded.Checked = true;
            this.chAdvBanded.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chAdvBanded.ImageIndex = 3;
            this.chAdvBanded.ImageList = this.imageList1;
            resources.ApplyResources(this.chAdvBanded, "chAdvBanded");
            this.chAdvBanded.Name = "chAdvBanded";
            this.chAdvBanded.Tag = "Advanced Banded GridView";
            this.chAdvBanded.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // chBanded
            // 
            this.chBanded.AllowFocus = false;
            this.chBanded.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.chBanded.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("chBanded.Appearance.FontStyleDelta")));
            this.chBanded.Appearance.Options.UseFont = true;
            this.chBanded.Appearance.Options.UseTextOptions = true;
            this.chBanded.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chBanded.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chBanded.ImageIndex = 2;
            this.chBanded.ImageList = this.imageList1;
            resources.ApplyResources(this.chBanded, "chBanded");
            this.chBanded.Name = "chBanded";
            this.chBanded.Tag = "Banded GridView";
            this.chBanded.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // chCard
            // 
            this.chCard.AllowFocus = false;
            this.chCard.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("chCard.Appearance.FontStyleDelta")));
            this.chCard.Appearance.Options.UseFont = true;
            this.chCard.Appearance.Options.UseTextOptions = true;
            this.chCard.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chCard.ImageIndex = 1;
            this.chCard.ImageList = this.imageList1;
            resources.ApplyResources(this.chCard, "chCard");
            this.chCard.Name = "chCard";
            this.chCard.Tag = "CardView";
            this.chCard.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // chGrid
            // 
            this.chGrid.AllowFocus = false;
            this.chGrid.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("chGrid.Appearance.FontStyleDelta")));
            this.chGrid.Appearance.Options.UseFont = true;
            this.chGrid.Appearance.Options.UseTextOptions = true;
            this.chGrid.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chGrid.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chGrid.ImageIndex = 0;
            this.chGrid.ImageList = this.imageList1;
            resources.ApplyResources(this.chGrid, "chGrid");
            this.chGrid.Name = "chGrid";
            this.chGrid.Tag = "GridView";
            this.chGrid.CheckedChanged += new System.EventHandler(this.ch_CheckedChanged);
            // 
            // gridControl1
            // 
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemPictureEdit1,
            this.repositoryItemMemoEdit1,
            this.repositoryItemImageEdit1});
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1,
            this.gridView1,
            this.bandedGridView1,
            this.cardView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbMain,
            this.gbPerfomance,
            this.gbImage,
            this.gbInfo});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bgcID,
            this.bgcTrademark,
            this.bgcName,
            this.bgcModification,
            this.pgcCategory,
            this.bgcPrice,
            this.bgcMPGCity,
            this.bgcMPGHighway,
            this.bgcDoors,
            this.bgcBodyStyle,
            this.bgcCylinders,
            this.bgcHorsepower,
            this.bgcTorque,
            this.bgcDescription,
            this.bgcPhoto,
            this.bgcTransmissionSpeeds,
            this.bgcTransmissionType,
            this.bgcDeliveryDate,
            this.bgcInStock,
            this.bgcLogo});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.bgcInStock;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor")));
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.NotEqual;
            formatConditionRuleValue1.PredefinedName = "Strikeout Text";
            formatConditionRuleValue1.Value1 = true;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule2.Column = this.bgcMPGCity;
            gridFormatRule2.Name = "Format1";
            formatConditionIconSet1.CategoryName = "Directional";
            formatConditionIconSetIcon1.PredefinedName = "Arrows3_1.png";
            formatConditionIconSetIcon1.Value = new decimal(new int[] {
            67,
            0,
            0,
            0});
            formatConditionIconSetIcon1.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon2.PredefinedName = "Arrows3_2.png";
            formatConditionIconSetIcon2.Value = new decimal(new int[] {
            33,
            0,
            0,
            0});
            formatConditionIconSetIcon2.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon3.PredefinedName = "Arrows3_3.png";
            formatConditionIconSetIcon3.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSet1.Icons.Add(formatConditionIconSetIcon1);
            formatConditionIconSet1.Icons.Add(formatConditionIconSetIcon2);
            formatConditionIconSet1.Icons.Add(formatConditionIconSetIcon3);
            formatConditionIconSet1.Name = "Arrows3Colored";
            formatConditionIconSet1.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleIconSet1.IconSet = formatConditionIconSet1;
            gridFormatRule2.Rule = formatConditionRuleIconSet1;
            gridFormatRule3.Column = this.bgcMPGHighway;
            gridFormatRule3.Name = "Format2";
            formatConditionIconSet2.CategoryName = "Directional";
            formatConditionIconSetIcon4.PredefinedName = "Arrows3_1.png";
            formatConditionIconSetIcon4.Value = new decimal(new int[] {
            67,
            0,
            0,
            0});
            formatConditionIconSetIcon4.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon5.PredefinedName = "Arrows3_2.png";
            formatConditionIconSetIcon5.Value = new decimal(new int[] {
            33,
            0,
            0,
            0});
            formatConditionIconSetIcon5.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon6.PredefinedName = "Arrows3_3.png";
            formatConditionIconSetIcon6.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSet2.Icons.Add(formatConditionIconSetIcon4);
            formatConditionIconSet2.Icons.Add(formatConditionIconSetIcon5);
            formatConditionIconSet2.Icons.Add(formatConditionIconSetIcon6);
            formatConditionIconSet2.Name = "Arrows3Colored";
            formatConditionIconSet2.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleIconSet2.IconSet = formatConditionIconSet2;
            gridFormatRule3.Rule = formatConditionRuleIconSet2;
            this.advBandedGridView1.FormatRules.Add(gridFormatRule1);
            this.advBandedGridView1.FormatRules.Add(gridFormatRule2);
            this.advBandedGridView1.FormatRules.Add(gridFormatRule3);
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.GroupCount = 1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.advBandedGridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.advBandedGridView1.OptionsCustomization.AllowChangeColumnParent = true;
            this.advBandedGridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.advBandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.bgcBodyStyle, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gbMain
            // 
            resources.ApplyResources(this.gbMain, "gbMain");
            this.gbMain.Columns.Add(this.bgcID);
            this.gbMain.Columns.Add(this.bgcTrademark);
            this.gbMain.Columns.Add(this.bgcName);
            this.gbMain.Columns.Add(this.bgcModification);
            this.gbMain.Columns.Add(this.bgcLogo);
            this.gbMain.Columns.Add(this.bgcDescription);
            this.gbMain.VisibleIndex = 0;
            // 
            // bgcID
            // 
            this.bgcID.FieldName = "ID";
            this.bgcID.Name = "bgcID";
            // 
            // bgcTrademark
            // 
            resources.ApplyResources(this.bgcTrademark, "bgcTrademark");
            this.bgcTrademark.FieldName = "Trademark";
            this.bgcTrademark.MinWidth = 80;
            this.bgcTrademark.Name = "bgcTrademark";
            this.bgcTrademark.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // bgcName
            // 
            this.bgcName.FieldName = "Name";
            this.bgcName.Name = "bgcName";
            resources.ApplyResources(this.bgcName, "bgcName");
            // 
            // bgcModification
            // 
            this.bgcModification.FieldName = "Modification";
            this.bgcModification.Name = "bgcModification";
            resources.ApplyResources(this.bgcModification, "bgcModification");
            // 
            // bgcLogo
            // 
            this.bgcLogo.FieldName = "TrademarkImage";
            this.bgcLogo.Name = "bgcLogo";
            this.bgcLogo.RowCount = 2;
            this.bgcLogo.RowIndex = 1;
            resources.ApplyResources(this.bgcLogo, "bgcLogo");
            // 
            // bgcDescription
            // 
            this.bgcDescription.ColumnEdit = this.repositoryItemMemoEdit1;
            this.bgcDescription.FieldName = "Description";
            this.bgcDescription.Image = ((System.Drawing.Image)(resources.GetObject("bgcDescription.Image")));
            this.bgcDescription.Name = "bgcDescription";
            this.bgcDescription.OptionsFilter.AllowFilter = false;
            this.bgcDescription.RowCount = 3;
            this.bgcDescription.RowIndex = 1;
            resources.ApplyResources(this.bgcDescription, "bgcDescription");
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // gbPerfomance
            // 
            resources.ApplyResources(this.gbPerfomance, "gbPerfomance");
            this.gbPerfomance.Columns.Add(this.bgcHorsepower);
            this.gbPerfomance.Columns.Add(this.bgcTorque);
            this.gbPerfomance.Columns.Add(this.bgcMPGCity);
            this.gbPerfomance.Columns.Add(this.bgcMPGHighway);
            this.gbPerfomance.Columns.Add(this.bgcDoors);
            this.gbPerfomance.Columns.Add(this.bgcCylinders);
            this.gbPerfomance.Columns.Add(this.bgcTransmissionType);
            this.gbPerfomance.Columns.Add(this.bgcTransmissionSpeeds);
            this.gbPerfomance.Image = ((System.Drawing.Image)(resources.GetObject("gbPerfomance.Image")));
            this.gbPerfomance.VisibleIndex = 1;
            // 
            // bgcHorsepower
            // 
            this.bgcHorsepower.FieldName = "Horsepower";
            this.bgcHorsepower.Name = "bgcHorsepower";
            resources.ApplyResources(this.bgcHorsepower, "bgcHorsepower");
            // 
            // bgcTorque
            // 
            this.bgcTorque.FieldName = "Torque";
            this.bgcTorque.Name = "bgcTorque";
            resources.ApplyResources(this.bgcTorque, "bgcTorque");
            // 
            // bgcDoors
            // 
            this.bgcDoors.FieldName = "Doors";
            this.bgcDoors.Name = "bgcDoors";
            this.bgcDoors.RowIndex = 2;
            resources.ApplyResources(this.bgcDoors, "bgcDoors");
            // 
            // bgcCylinders
            // 
            this.bgcCylinders.FieldName = "Cylinders";
            this.bgcCylinders.Name = "bgcCylinders";
            this.bgcCylinders.RowIndex = 2;
            resources.ApplyResources(this.bgcCylinders, "bgcCylinders");
            // 
            // bgcTransmissionType
            // 
            this.bgcTransmissionType.FieldName = "TransmissionType";
            this.bgcTransmissionType.Name = "bgcTransmissionType";
            this.bgcTransmissionType.OptionsColumn.ShowInCustomizationForm = false;
            this.bgcTransmissionType.RowIndex = 3;
            resources.ApplyResources(this.bgcTransmissionType, "bgcTransmissionType");
            // 
            // bgcTransmissionSpeeds
            // 
            this.bgcTransmissionSpeeds.FieldName = "TransmissionSpeeds";
            this.bgcTransmissionSpeeds.Name = "bgcTransmissionSpeeds";
            this.bgcTransmissionSpeeds.OptionsColumn.ShowInCustomizationForm = false;
            this.bgcTransmissionSpeeds.RowIndex = 3;
            resources.ApplyResources(this.bgcTransmissionSpeeds, "bgcTransmissionSpeeds");
            // 
            // gbImage
            // 
            resources.ApplyResources(this.gbImage, "gbImage");
            this.gbImage.Columns.Add(this.bgcPhoto);
            this.gbImage.VisibleIndex = 2;
            // 
            // bgcPhoto
            // 
            this.bgcPhoto.FieldName = "Photo";
            this.bgcPhoto.Name = "bgcPhoto";
            this.bgcPhoto.RowCount = 4;
            resources.ApplyResources(this.bgcPhoto, "bgcPhoto");
            // 
            // gbInfo
            // 
            resources.ApplyResources(this.gbInfo, "gbInfo");
            this.gbInfo.Columns.Add(this.pgcCategory);
            this.gbInfo.Columns.Add(this.bgcBodyStyle);
            this.gbInfo.Columns.Add(this.bgcPrice);
            this.gbInfo.VisibleIndex = -1;
            // 
            // pgcCategory
            // 
            resources.ApplyResources(this.pgcCategory, "pgcCategory");
            this.pgcCategory.FieldName = "Category";
            this.pgcCategory.Name = "pgcCategory";
            this.pgcCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // bgcBodyStyle
            // 
            resources.ApplyResources(this.bgcBodyStyle, "bgcBodyStyle");
            this.bgcBodyStyle.FieldName = "BodyStyle";
            this.bgcBodyStyle.Name = "bgcBodyStyle";
            this.bgcBodyStyle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // bgcPrice
            // 
            this.bgcPrice.FieldName = "Price";
            this.bgcPrice.Name = "bgcPrice";
            resources.ApplyResources(this.bgcPrice, "bgcPrice");
            // 
            // bgcDeliveryDate
            // 
            this.bgcDeliveryDate.FieldName = "DeliveryDate";
            this.bgcDeliveryDate.Name = "bgcDeliveryDate";
            // 
            // repositoryItemMemoExEdit1
            // 
            resources.ApplyResources(this.repositoryItemMemoExEdit1, "repositoryItemMemoExEdit1");
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemMemoExEdit1.Buttons"))))});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.PopupFormSize = new System.Drawing.Size(300, 150);
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.CustomHeight = 80;
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.PictureInterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // repositoryItemImageEdit1
            // 
            resources.ApplyResources(this.repositoryItemImageEdit1, "repositoryItemImageEdit1");
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemImageEdit1.Buttons"))))});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            this.repositoryItemImageEdit1.PopupFormSize = new System.Drawing.Size(300, 200);
            this.repositoryItemImageEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcID,
            this.gcTrademark,
            this.gcName,
            this.gcModification,
            this.gcCategory,
            this.gcPrice,
            this.gcMPGCity,
            this.gcMPGHighway,
            this.gcDoors,
            this.gcBodyStyle,
            this.gcCylinders,
            this.gcHorsepower,
            this.gcTorque,
            this.gcTransmissionSpeeds,
            this.gcTransmissionType,
            this.gcDescription,
            this.gcImage,
            this.gcDeliveryDate,
            this.gcInStock,
            this.gcPhoto});
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.gcInStock;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor1")));
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.NotEqual;
            formatConditionRuleValue2.PredefinedName = "Strikeout Text";
            formatConditionRuleValue2.Value1 = true;
            gridFormatRule4.Rule = formatConditionRuleValue2;
            gridFormatRule5.Column = this.gcMPGCity;
            gridFormatRule5.Name = "Format1";
            formatConditionIconSet3.CategoryName = "Directional";
            formatConditionIconSetIcon7.PredefinedName = "Arrows3_1.png";
            formatConditionIconSetIcon7.Value = new decimal(new int[] {
            67,
            0,
            0,
            0});
            formatConditionIconSetIcon7.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon8.PredefinedName = "Arrows3_2.png";
            formatConditionIconSetIcon8.Value = new decimal(new int[] {
            33,
            0,
            0,
            0});
            formatConditionIconSetIcon8.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon9.PredefinedName = "Arrows3_3.png";
            formatConditionIconSetIcon9.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSet3.Icons.Add(formatConditionIconSetIcon7);
            formatConditionIconSet3.Icons.Add(formatConditionIconSetIcon8);
            formatConditionIconSet3.Icons.Add(formatConditionIconSetIcon9);
            formatConditionIconSet3.Name = "Arrows3Colored";
            formatConditionIconSet3.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleIconSet3.IconSet = formatConditionIconSet3;
            gridFormatRule5.Rule = formatConditionRuleIconSet3;
            gridFormatRule6.Column = this.gcMPGHighway;
            gridFormatRule6.Name = "Format2";
            formatConditionIconSet4.CategoryName = "Directional";
            formatConditionIconSetIcon10.PredefinedName = "Arrows3_1.png";
            formatConditionIconSetIcon10.Value = new decimal(new int[] {
            67,
            0,
            0,
            0});
            formatConditionIconSetIcon10.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon11.PredefinedName = "Arrows3_2.png";
            formatConditionIconSetIcon11.Value = new decimal(new int[] {
            33,
            0,
            0,
            0});
            formatConditionIconSetIcon11.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon12.PredefinedName = "Arrows3_3.png";
            formatConditionIconSetIcon12.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSet4.Icons.Add(formatConditionIconSetIcon10);
            formatConditionIconSet4.Icons.Add(formatConditionIconSetIcon11);
            formatConditionIconSet4.Icons.Add(formatConditionIconSetIcon12);
            formatConditionIconSet4.Name = "Arrows3Colored";
            formatConditionIconSet4.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleIconSet4.IconSet = formatConditionIconSet4;
            gridFormatRule6.Rule = formatConditionRuleIconSet4;
            gridFormatRule7.Column = this.gcPrice;
            gridFormatRule7.Name = "Format3";
            formatConditionRule2ColorScale1.PredefinedName = "White, Red";
            gridFormatRule7.Rule = formatConditionRule2ColorScale1;
            this.gridView1.FormatRules.Add(gridFormatRule4);
            this.gridView1.FormatRules.Add(gridFormatRule5);
            this.gridView1.FormatRules.Add(gridFormatRule6);
            this.gridView1.FormatRules.Add(gridFormatRule7);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.PreviewFieldName = "Description";
            this.gridView1.RowHeight = 27;
            this.gridView1.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView1_DragObjectOver);
            // 
            // gcID
            // 
            this.gcID.FieldName = "ID";
            this.gcID.Name = "gcID";
            // 
            // gcTrademark
            // 
            resources.ApplyResources(this.gcTrademark, "gcTrademark");
            this.gcTrademark.FieldName = "Trademark";
            this.gcTrademark.Name = "gcTrademark";
            this.gcTrademark.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gcName
            // 
            resources.ApplyResources(this.gcName, "gcName");
            this.gcName.FieldName = "Name";
            this.gcName.Name = "gcName";
            // 
            // gcModification
            // 
            this.gcModification.FieldName = "Modification";
            this.gcModification.Name = "gcModification";
            resources.ApplyResources(this.gcModification, "gcModification");
            // 
            // gcCategory
            // 
            this.gcCategory.FieldName = "Category";
            this.gcCategory.Name = "gcCategory";
            this.gcCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            resources.ApplyResources(this.gcCategory, "gcCategory");
            // 
            // gcDoors
            // 
            this.gcDoors.FieldName = "Doors";
            this.gcDoors.Name = "gcDoors";
            resources.ApplyResources(this.gcDoors, "gcDoors");
            // 
            // gcBodyStyle
            // 
            this.gcBodyStyle.FieldName = "BodyStyle";
            this.gcBodyStyle.Name = "gcBodyStyle";
            this.gcBodyStyle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            resources.ApplyResources(this.gcBodyStyle, "gcBodyStyle");
            // 
            // gcCylinders
            // 
            this.gcCylinders.FieldName = "Cylinders";
            this.gcCylinders.Name = "gcCylinders";
            resources.ApplyResources(this.gcCylinders, "gcCylinders");
            // 
            // gcHorsepower
            // 
            this.gcHorsepower.FieldName = "Horsepower";
            this.gcHorsepower.Name = "gcHorsepower";
            resources.ApplyResources(this.gcHorsepower, "gcHorsepower");
            // 
            // gcTorque
            // 
            this.gcTorque.FieldName = "Torque";
            this.gcTorque.Name = "gcTorque";
            resources.ApplyResources(this.gcTorque, "gcTorque");
            // 
            // gcTransmissionSpeeds
            // 
            this.gcTransmissionSpeeds.FieldName = "TransmissionSpeeds";
            this.gcTransmissionSpeeds.Name = "gcTransmissionSpeeds";
            resources.ApplyResources(this.gcTransmissionSpeeds, "gcTransmissionSpeeds");
            // 
            // gcTransmissionType
            // 
            this.gcTransmissionType.FieldName = "TransmissionType";
            this.gcTransmissionType.Name = "gcTransmissionType";
            resources.ApplyResources(this.gcTransmissionType, "gcTransmissionType");
            // 
            // gcDescription
            // 
            this.gcDescription.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gcDescription.FieldName = "Description";
            this.gcDescription.Name = "gcDescription";
            this.gcDescription.OptionsFilter.AllowFilter = false;
            resources.ApplyResources(this.gcDescription, "gcDescription");
            // 
            // gcImage
            // 
            this.gcImage.FieldName = "Image";
            resources.ApplyResources(this.gcImage, "gcImage");
            this.gcImage.Image = ((System.Drawing.Image)(resources.GetObject("gcImage.Image")));
            this.gcImage.Name = "gcImage";
            this.gcImage.OptionsColumn.AllowFocus = false;
            this.gcImage.OptionsColumn.AllowMove = false;
            this.gcImage.OptionsFilter.AllowFilter = false;
            // 
            // gcDeliveryDate
            // 
            this.gcDeliveryDate.FieldName = "DeliveryDate";
            this.gcDeliveryDate.Name = "gcDeliveryDate";
            resources.ApplyResources(this.gcDeliveryDate, "gcDeliveryDate");
            // 
            // gcPhoto
            // 
            this.gcPhoto.ColumnEdit = this.repositoryItemImageEdit1;
            this.gcPhoto.FieldName = "Photo";
            this.gcPhoto.Name = "gcPhoto";
            resources.ApplyResources(this.gcPhoto, "gcPhoto");
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand4,
            this.gridBand5});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bcID,
            this.bcTrademark,
            this.bcName,
            this.bcModification,
            this.bcCategory,
            this.bcPrice,
            this.bcMPGCity,
            this.bcMPGHighway,
            this.bcDoors,
            this.bcBodyStyle,
            this.bcCylinders,
            this.bcHorsepower,
            this.bcTorque,
            this.bcTransmissionSpeeds,
            this.bcTransmissionType,
            this.bcDescription,
            this.bcImage,
            this.bcPhoto,
            this.bcDeliveryDate,
            this.bcInStock,
            this.bcLogo});
            gridFormatRule8.ApplyToRow = true;
            gridFormatRule8.Column = this.bcInStock;
            gridFormatRule8.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor2")));
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.NotEqual;
            formatConditionRuleValue3.PredefinedName = "Strikeout Text";
            formatConditionRuleValue3.Value1 = true;
            gridFormatRule8.Rule = formatConditionRuleValue3;
            gridFormatRule9.Column = this.bcMPGCity;
            gridFormatRule9.Name = "Format1";
            formatConditionIconSet5.CategoryName = "Directional";
            formatConditionIconSetIcon13.PredefinedName = "Arrows3_1.png";
            formatConditionIconSetIcon13.Value = new decimal(new int[] {
            67,
            0,
            0,
            0});
            formatConditionIconSetIcon13.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon14.PredefinedName = "Arrows3_2.png";
            formatConditionIconSetIcon14.Value = new decimal(new int[] {
            33,
            0,
            0,
            0});
            formatConditionIconSetIcon14.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon15.PredefinedName = "Arrows3_3.png";
            formatConditionIconSetIcon15.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSet5.Icons.Add(formatConditionIconSetIcon13);
            formatConditionIconSet5.Icons.Add(formatConditionIconSetIcon14);
            formatConditionIconSet5.Icons.Add(formatConditionIconSetIcon15);
            formatConditionIconSet5.Name = "Arrows3Colored";
            formatConditionIconSet5.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleIconSet5.IconSet = formatConditionIconSet5;
            gridFormatRule9.Rule = formatConditionRuleIconSet5;
            gridFormatRule10.Column = this.bcMPGHighway;
            gridFormatRule10.Name = "Format2";
            formatConditionIconSet6.CategoryName = "Directional";
            formatConditionIconSetIcon16.PredefinedName = "Arrows3_1.png";
            formatConditionIconSetIcon16.Value = new decimal(new int[] {
            67,
            0,
            0,
            0});
            formatConditionIconSetIcon16.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon17.PredefinedName = "Arrows3_2.png";
            formatConditionIconSetIcon17.Value = new decimal(new int[] {
            33,
            0,
            0,
            0});
            formatConditionIconSetIcon17.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSetIcon18.PredefinedName = "Arrows3_3.png";
            formatConditionIconSetIcon18.ValueComparison = DevExpress.XtraEditors.FormatConditionComparisonType.GreaterOrEqual;
            formatConditionIconSet6.Icons.Add(formatConditionIconSetIcon16);
            formatConditionIconSet6.Icons.Add(formatConditionIconSetIcon17);
            formatConditionIconSet6.Icons.Add(formatConditionIconSetIcon18);
            formatConditionIconSet6.Name = "Arrows3Colored";
            formatConditionIconSet6.ValueType = DevExpress.XtraEditors.FormatConditionValueType.Percent;
            formatConditionRuleIconSet6.IconSet = formatConditionIconSet6;
            gridFormatRule10.Rule = formatConditionRuleIconSet6;
            this.bandedGridView1.FormatRules.Add(gridFormatRule8);
            this.bandedGridView1.FormatRules.Add(gridFormatRule9);
            this.bandedGridView1.FormatRules.Add(gridFormatRule10);
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.GroupCount = 1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.bandedGridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.bandedGridView1.OptionsCustomization.AllowChangeColumnParent = true;
            this.bandedGridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.bandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.bcCategory, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridBand1
            // 
            resources.ApplyResources(this.gridBand1, "gridBand1");
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3});
            this.gridBand1.VisibleIndex = 0;
            // 
            // gridBand2
            // 
            resources.ApplyResources(this.gridBand2, "gridBand2");
            this.gridBand2.Columns.Add(this.bcLogo);
            this.gridBand2.Columns.Add(this.bcTrademark);
            this.gridBand2.Columns.Add(this.bcName);
            this.gridBand2.Columns.Add(this.bcModification);
            this.gridBand2.VisibleIndex = 0;
            // 
            // bcLogo
            // 
            resources.ApplyResources(this.bcLogo, "bcLogo");
            this.bcLogo.FieldName = "TrademarkImage";
            this.bcLogo.Image = ((System.Drawing.Image)(resources.GetObject("bcLogo.Image")));
            this.bcLogo.Name = "bcLogo";
            this.bcLogo.OptionsColumn.AllowFocus = false;
            this.bcLogo.OptionsColumn.ReadOnly = true;
            this.bcLogo.OptionsFilter.AllowFilter = false;
            // 
            // bcTrademark
            // 
            this.bcTrademark.FieldName = "Trademark";
            this.bcTrademark.Name = "bcTrademark";
            this.bcTrademark.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            resources.ApplyResources(this.bcTrademark, "bcTrademark");
            // 
            // bcName
            // 
            this.bcName.FieldName = "Name";
            this.bcName.Name = "bcName";
            resources.ApplyResources(this.bcName, "bcName");
            // 
            // bcModification
            // 
            this.bcModification.FieldName = "Modification";
            this.bcModification.Name = "bcModification";
            resources.ApplyResources(this.bcModification, "bcModification");
            // 
            // gridBand3
            // 
            resources.ApplyResources(this.gridBand3, "gridBand3");
            this.gridBand3.Columns.Add(this.bcCategory);
            this.gridBand3.Columns.Add(this.bcBodyStyle);
            this.gridBand3.VisibleIndex = 1;
            // 
            // bcCategory
            // 
            this.bcCategory.FieldName = "Category";
            this.bcCategory.Name = "bcCategory";
            this.bcCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            resources.ApplyResources(this.bcCategory, "bcCategory");
            // 
            // bcBodyStyle
            // 
            this.bcBodyStyle.FieldName = "BodyStyle";
            this.bcBodyStyle.Name = "bcBodyStyle";
            this.bcBodyStyle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            resources.ApplyResources(this.bcBodyStyle, "bcBodyStyle");
            // 
            // gridBand4
            // 
            resources.ApplyResources(this.gridBand4, "gridBand4");
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand6,
            this.gridBand7});
            this.gridBand4.VisibleIndex = 1;
            // 
            // gridBand6
            // 
            resources.ApplyResources(this.gridBand6, "gridBand6");
            this.gridBand6.Columns.Add(this.bcMPGCity);
            this.gridBand6.Columns.Add(this.bcMPGHighway);
            this.gridBand6.VisibleIndex = 0;
            // 
            // gridBand7
            // 
            resources.ApplyResources(this.gridBand7, "gridBand7");
            this.gridBand7.Columns.Add(this.bcHorsepower);
            this.gridBand7.Columns.Add(this.bcTorque);
            this.gridBand7.VisibleIndex = 1;
            // 
            // bcHorsepower
            // 
            this.bcHorsepower.FieldName = "Horsepower";
            this.bcHorsepower.Name = "bcHorsepower";
            resources.ApplyResources(this.bcHorsepower, "bcHorsepower");
            // 
            // bcTorque
            // 
            this.bcTorque.FieldName = "Torque";
            this.bcTorque.Name = "bcTorque";
            resources.ApplyResources(this.bcTorque, "bcTorque");
            // 
            // gridBand5
            // 
            resources.ApplyResources(this.gridBand5, "gridBand5");
            this.gridBand5.Columns.Add(this.bcPhoto);
            this.gridBand5.Columns.Add(this.bcDescription);
            this.gridBand5.Image = ((System.Drawing.Image)(resources.GetObject("gridBand5.Image")));
            this.gridBand5.VisibleIndex = 2;
            // 
            // bcPhoto
            // 
            this.bcPhoto.ColumnEdit = this.repositoryItemImageEdit1;
            this.bcPhoto.FieldName = "Photo";
            this.bcPhoto.Name = "bcPhoto";
            resources.ApplyResources(this.bcPhoto, "bcPhoto");
            // 
            // bcDescription
            // 
            this.bcDescription.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.bcDescription.FieldName = "Description";
            this.bcDescription.Name = "bcDescription";
            this.bcDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.bcDescription.OptionsFilter.AllowFilter = false;
            resources.ApplyResources(this.bcDescription, "bcDescription");
            // 
            // bcID
            // 
            this.bcID.FieldName = "ID";
            this.bcID.Name = "bcID";
            resources.ApplyResources(this.bcID, "bcID");
            // 
            // bcPrice
            // 
            this.bcPrice.FieldName = "Price";
            this.bcPrice.Name = "bcPrice";
            resources.ApplyResources(this.bcPrice, "bcPrice");
            // 
            // bcDoors
            // 
            this.bcDoors.FieldName = "Doors";
            this.bcDoors.Name = "bcDoors";
            resources.ApplyResources(this.bcDoors, "bcDoors");
            // 
            // bcCylinders
            // 
            this.bcCylinders.FieldName = "Cylinders";
            this.bcCylinders.Name = "bcCylinders";
            resources.ApplyResources(this.bcCylinders, "bcCylinders");
            // 
            // bcTransmissionSpeeds
            // 
            this.bcTransmissionSpeeds.FieldName = "TransmissionSpeeds";
            this.bcTransmissionSpeeds.Name = "bcTransmissionSpeeds";
            resources.ApplyResources(this.bcTransmissionSpeeds, "bcTransmissionSpeeds");
            // 
            // bcTransmissionType
            // 
            this.bcTransmissionType.FieldName = "TransmissionType";
            this.bcTransmissionType.Name = "bcTransmissionType";
            resources.ApplyResources(this.bcTransmissionType, "bcTransmissionType");
            // 
            // bcImage
            // 
            this.bcImage.FieldName = "Image";
            this.bcImage.Name = "bcImage";
            resources.ApplyResources(this.bcImage, "bcImage");
            // 
            // bcDeliveryDate
            // 
            this.bcDeliveryDate.FieldName = "DeliveryDate";
            this.bcDeliveryDate.Name = "bcDeliveryDate";
            resources.ApplyResources(this.bcDeliveryDate, "bcDeliveryDate");
            // 
            // cardView1
            // 
            this.cardView1.CardWidth = 255;
            this.cardView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTrademark,
            this.colName,
            this.colModification,
            this.colCategory,
            this.colBodyStyle,
            this.colPhoto,
            this.colDescription,
            this.colDeliveryDate,
            this.colPrice,
            this.colInStock});
            this.cardView1.FocusedCardTopFieldIndex = 0;
            this.cardView1.GridControl = this.gridControl1;
            this.cardView1.Name = "cardView1";
            this.cardView1.OptionsBehavior.FieldAutoHeight = true;
            this.cardView1.CustomCardCaptionImage += new DevExpress.XtraGrid.Views.Card.CardCaptionImageEventHandler(this.cardView1_CustomCardCaptionImage);
            this.cardView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.cardView1_ValidatingEditor);
            // 
            // colTrademark
            // 
            this.colTrademark.FieldName = "Trademark";
            this.colTrademark.Name = "colTrademark";
            resources.ApplyResources(this.colTrademark, "colTrademark");
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            resources.ApplyResources(this.colName, "colName");
            // 
            // colModification
            // 
            this.colModification.FieldName = "Modification";
            this.colModification.Name = "colModification";
            resources.ApplyResources(this.colModification, "colModification");
            // 
            // colCategory
            // 
            this.colCategory.FieldName = "Category";
            this.colCategory.Name = "colCategory";
            resources.ApplyResources(this.colCategory, "colCategory");
            // 
            // colBodyStyle
            // 
            this.colBodyStyle.FieldName = "BodyStyle";
            this.colBodyStyle.Name = "colBodyStyle";
            resources.ApplyResources(this.colBodyStyle, "colBodyStyle");
            // 
            // colPhoto
            // 
            this.colPhoto.ColumnEdit = this.repositoryItemPictureEdit1;
            this.colPhoto.FieldName = "Photo";
            this.colPhoto.Name = "colPhoto";
            resources.ApplyResources(this.colPhoto, "colPhoto");
            // 
            // colDescription
            // 
            this.colDescription.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            resources.ApplyResources(this.colDescription, "colDescription");
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.FieldName = "DeliveryDate";
            this.colDeliveryDate.Name = "colDeliveryDate";
            // 
            // colPrice
            // 
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            resources.ApplyResources(this.colPrice, "colPrice");
            // 
            // colInStock
            // 
            this.colInStock.FieldName = "InStock";
            this.colInStock.Name = "colInStock";
            resources.ApplyResources(this.colInStock, "colInStock");
            // 
            // ViewStyles
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "ViewStyles";
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.IContainer components;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckButton chAdvBanded;
        private DevExpress.XtraEditors.CheckButton chBanded;
        private DevExpress.XtraEditors.CheckButton chCard;
        private DevExpress.XtraEditors.CheckButton chGrid;
        private GridControl gridControl1;
        private Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private Views.Grid.GridView gridView1;
        private Views.Card.CardView cardView1;
        private Views.BandedGrid.BandedGridView bandedGridView1;
        private Views.BandedGrid.GridBand gridBand1;
        private Columns.GridColumn colTrademark;
        private Columns.GridColumn colName;
        private Columns.GridColumn colModification;
        private Columns.GridColumn colCategory;
        private Columns.GridColumn colBodyStyle;
        private Columns.GridColumn colPhoto;
        private XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private Columns.GridColumn colDescription;
        private XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private Columns.GridColumn colDeliveryDate;
        private Columns.GridColumn colPrice;
        private Columns.GridColumn colInStock;
        private Views.BandedGrid.GridBand gbMain;
        private Views.BandedGrid.BandedGridColumn bgcID;
        private Views.BandedGrid.BandedGridColumn bgcLogo;
        private XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private Views.BandedGrid.GridBand gbPerfomance;
        private Views.BandedGrid.BandedGridColumn bgcHorsepower;
        private Views.BandedGrid.BandedGridColumn bgcTorque;
        private Views.BandedGrid.BandedGridColumn bgcMPGCity;
        private Views.BandedGrid.BandedGridColumn bgcMPGHighway;
        private Views.BandedGrid.BandedGridColumn bgcDoors;
        private Views.BandedGrid.BandedGridColumn bgcCylinders;
        private Views.BandedGrid.GridBand gbInfo;
        private Views.BandedGrid.GridBand gbImage;
        private Views.BandedGrid.BandedGridColumn bgcTransmissionSpeeds;
        private Views.BandedGrid.BandedGridColumn bgcTransmissionType;
        private Views.BandedGrid.BandedGridColumn bgcTrademark;
        private Views.BandedGrid.BandedGridColumn bgcName;
        private Views.BandedGrid.BandedGridColumn bgcModification;
        private Views.BandedGrid.BandedGridColumn bgcDescription;
        private Views.BandedGrid.BandedGridColumn bgcPhoto;
        private Views.BandedGrid.BandedGridColumn pgcCategory;
        private Views.BandedGrid.BandedGridColumn bgcBodyStyle;
        private Views.BandedGrid.BandedGridColumn bgcPrice;
        private Views.BandedGrid.BandedGridColumn bgcDeliveryDate;
        private Views.BandedGrid.BandedGridColumn bgcInStock;
        private Views.BandedGrid.GridBand gridBand2;
        private Views.BandedGrid.BandedGridColumn bcLogo;
        private Views.BandedGrid.BandedGridColumn bcTrademark;
        private Views.BandedGrid.BandedGridColumn bcName;
        private Views.BandedGrid.BandedGridColumn bcModification;
        private Views.BandedGrid.GridBand gridBand3;
        private Views.BandedGrid.BandedGridColumn bcCategory;
        private Views.BandedGrid.BandedGridColumn bcBodyStyle;
        private Views.BandedGrid.GridBand gridBand4;
        private Views.BandedGrid.GridBand gridBand6;
        private Views.BandedGrid.BandedGridColumn bcMPGCity;
        private Views.BandedGrid.BandedGridColumn bcMPGHighway;
        private Views.BandedGrid.GridBand gridBand7;
        private Views.BandedGrid.BandedGridColumn bcHorsepower;
        private Views.BandedGrid.BandedGridColumn bcTorque;
        private Views.BandedGrid.GridBand gridBand5;
        private Views.BandedGrid.BandedGridColumn bcPhoto;
        private Views.BandedGrid.BandedGridColumn bcDescription;
        private Views.BandedGrid.BandedGridColumn bcID;
        private Views.BandedGrid.BandedGridColumn bcPrice;
        private Views.BandedGrid.BandedGridColumn bcDoors;
        private Views.BandedGrid.BandedGridColumn bcCylinders;
        private Views.BandedGrid.BandedGridColumn bcTransmissionSpeeds;
        private Views.BandedGrid.BandedGridColumn bcTransmissionType;
        private Views.BandedGrid.BandedGridColumn bcImage;
        private Views.BandedGrid.BandedGridColumn bcDeliveryDate;
        private Views.BandedGrid.BandedGridColumn bcInStock;
        private XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private Columns.GridColumn gcID;
        private Columns.GridColumn gcTrademark;
        private Columns.GridColumn gcName;
        private Columns.GridColumn gcModification;
        private Columns.GridColumn gcCategory;
        private Columns.GridColumn gcPrice;
        private Columns.GridColumn gcMPGCity;
        private Columns.GridColumn gcMPGHighway;
        private Columns.GridColumn gcDoors;
        private Columns.GridColumn gcBodyStyle;
        private Columns.GridColumn gcCylinders;
        private Columns.GridColumn gcHorsepower;
        private Columns.GridColumn gcTorque;
        private Columns.GridColumn gcTransmissionSpeeds;
        private Columns.GridColumn gcTransmissionType;
        private Columns.GridColumn gcDescription;
        private Columns.GridColumn gcImage;
        private Columns.GridColumn gcDeliveryDate;
        private Columns.GridColumn gcInStock;
        private Columns.GridColumn gcPhoto;
    }
}
