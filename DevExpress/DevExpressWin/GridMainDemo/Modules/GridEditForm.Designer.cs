﻿namespace DevExpress.XtraGrid.Demos {
    partial class GridEditForm {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GridEditForm));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.dsVehicles1 = new DevExpress.XtraGrid.Demos.dsVehicles();
            this.persistentRepository1 = new DevExpress.XtraEditors.Repository.PersistentRepository(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraGrid.Demos.RepositoryItemGridLookUpEditWithGlyph();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLogo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gbImage = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colImage1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbMain = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colID1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTrademarkID1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colName1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colModification1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCategoryID1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBodyStyleID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gbPerfomance = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDoors = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHorsepower = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTorque = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCylinders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMPGCity1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMPGHighway1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbInfo = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPhoto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.colDescription1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTransmissionSpeeds = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTransmissionType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrademarkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModification = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMPGCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMPGHighway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicles1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.gridControl2);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1169, 687);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "Model";
            this.gridControl2.DataSource = this.dsVehicles1;
            this.gridControl2.ExternalRepository = this.persistentRepository1;
            this.gridControl2.Location = new System.Drawing.Point(14, 36);
            this.gridControl2.MainView = this.advBandedGridView1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemLookUpEdit2});
            this.gridControl2.Size = new System.Drawing.Size(1141, 637);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // dsVehicles1
            // 
            this.dsVehicles1.DataSetName = "dsVehicles";
            this.dsVehicles1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // persistentRepository1
            // 
            this.persistentRepository1.Items.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemMemoEdit1,
            this.repositoryItemPictureEdit2});
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.PopupFormMinSize = new System.Drawing.Size(400, 150);
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.repositoryItemLookUpEdit1.DisplayMember = "Name";
            this.repositoryItemLookUpEdit1.DropDownRows = 4;
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ShowFooter = false;
            this.repositoryItemLookUpEdit1.ShowHeader = false;
            this.repositoryItemLookUpEdit1.ValueMember = "ID";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DataSource = this.dsVehicles1;
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Name";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1});
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            this.repositoryItemGridLookUpEdit1.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.PictureInterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLogo,
            this.colTName});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.RowAutoHeight = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowColumnHeaders = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colLogo
            // 
            this.colLogo.ColumnEdit = this.repositoryItemPictureEdit1;
            this.colLogo.FieldName = "Logo";
            this.colLogo.Name = "colLogo";
            this.colLogo.Visible = true;
            this.colLogo.VisibleIndex = 0;
            // 
            // colTName
            // 
            this.colTName.FieldName = "Name";
            this.colTName.Name = "colTName";
            this.colTName.Visible = true;
            this.colTName.VisibleIndex = 1;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.PictureInterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            this.repositoryItemPictureEdit2.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbImage,
            this.gbMain,
            this.gbPerfomance,
            this.gbInfo});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colID1,
            this.colTrademarkID1,
            this.colName1,
            this.colModification1,
            this.colCategoryID1,
            this.colPrice,
            this.colMPGCity1,
            this.colMPGHighway1,
            this.colDoors,
            this.colBodyStyleID,
            this.colCylinders,
            this.colHorsepower,
            this.colTorque,
            this.colDescription1,
            this.colImage1,
            this.colPhoto,
            this.colTransmissionSpeeds,
            this.colTransmissionType});
            this.advBandedGridView1.GridControl = this.gridControl2;
            this.advBandedGridView1.GroupCount = 1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.advBandedGridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.advBandedGridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.advBandedGridView1.OptionsDetail.EnableMasterViewMode = false;
            this.advBandedGridView1.OptionsEditForm.FormCaptionFormat = "{Name} {Modification}";
            this.advBandedGridView1.OptionsEditForm.PopupEditFormWidth = 900;
            this.advBandedGridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.advBandedGridView1.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBodyStyleID, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gbImage
            // 
            this.gbImage.Caption = "Image";
            this.gbImage.Columns.Add(this.colImage1);
            this.gbImage.Name = "gbImage";
            this.gbImage.VisibleIndex = 0;
            this.gbImage.Width = 109;
            // 
            // colImage1
            // 
            this.colImage1.ColumnEdit = this.repositoryItemPictureEdit2;
            this.colImage1.FieldName = "Image";
            this.colImage1.Name = "colImage1";
            this.colImage1.RowCount = 2;
            this.colImage1.Visible = true;
            this.colImage1.Width = 109;
            // 
            // gbMain
            // 
            this.gbMain.Caption = "Main";
            this.gbMain.Columns.Add(this.colID1);
            this.gbMain.Columns.Add(this.colTrademarkID1);
            this.gbMain.Columns.Add(this.colName1);
            this.gbMain.Columns.Add(this.colModification1);
            this.gbMain.Columns.Add(this.colCategoryID1);
            this.gbMain.Columns.Add(this.colBodyStyleID);
            this.gbMain.Columns.Add(this.colPrice);
            this.gbMain.Name = "gbMain";
            this.gbMain.VisibleIndex = 1;
            this.gbMain.Width = 472;
            // 
            // colID1
            // 
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            // 
            // colTrademarkID1
            // 
            this.colTrademarkID1.Caption = "Trademark";
            this.colTrademarkID1.ColumnEdit = this.repositoryItemGridLookUpEdit1;
            this.colTrademarkID1.FieldName = "TrademarkID";
            this.colTrademarkID1.Image = ((System.Drawing.Image)(resources.GetObject("colTrademarkID1.Image")));
            this.colTrademarkID1.MinWidth = 80;
            this.colTrademarkID1.Name = "colTrademarkID1";
            this.colTrademarkID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTrademarkID1.Visible = true;
            this.colTrademarkID1.Width = 159;
            // 
            // colName1
            // 
            this.colName1.FieldName = "Name";
            this.colName1.Name = "colName1";
            this.colName1.Visible = true;
            this.colName1.Width = 167;
            // 
            // colModification1
            // 
            this.colModification1.FieldName = "Modification";
            this.colModification1.Name = "colModification1";
            this.colModification1.Visible = true;
            this.colModification1.Width = 146;
            // 
            // colCategoryID1
            // 
            this.colCategoryID1.Caption = "Category";
            this.colCategoryID1.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colCategoryID1.FieldName = "CategoryID";
            this.colCategoryID1.Name = "colCategoryID1";
            this.colCategoryID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCategoryID1.RowIndex = 1;
            this.colCategoryID1.Visible = true;
            this.colCategoryID1.Width = 159;
            // 
            // colBodyStyleID
            // 
            this.colBodyStyleID.Caption = "Body Style";
            this.colBodyStyleID.ColumnEdit = this.repositoryItemLookUpEdit2;
            this.colBodyStyleID.FieldName = "BodyStyleID";
            this.colBodyStyleID.Name = "colBodyStyleID";
            this.colBodyStyleID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBodyStyleID.RowIndex = 1;
            this.colBodyStyleID.Visible = true;
            this.colBodyStyleID.Width = 167;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.repositoryItemLookUpEdit2.DisplayMember = "Name";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.ShowFooter = false;
            this.repositoryItemLookUpEdit2.ShowHeader = false;
            this.repositoryItemLookUpEdit2.ValueMember = "ID";
            // 
            // colPrice
            // 
            this.colPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.RowIndex = 1;
            this.colPrice.Visible = true;
            this.colPrice.Width = 146;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gbPerfomance
            // 
            this.gbPerfomance.Caption = "Performance";
            this.gbPerfomance.Columns.Add(this.colDoors);
            this.gbPerfomance.Columns.Add(this.colHorsepower);
            this.gbPerfomance.Columns.Add(this.colTorque);
            this.gbPerfomance.Columns.Add(this.colCylinders);
            this.gbPerfomance.Columns.Add(this.colMPGCity1);
            this.gbPerfomance.Columns.Add(this.colMPGHighway1);
            this.gbPerfomance.Image = ((System.Drawing.Image)(resources.GetObject("gbPerfomance.Image")));
            this.gbPerfomance.Name = "gbPerfomance";
            this.gbPerfomance.VisibleIndex = 2;
            this.gbPerfomance.Width = 384;
            // 
            // colDoors
            // 
            this.colDoors.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colDoors.FieldName = "Doors";
            this.colDoors.Name = "colDoors";
            this.colDoors.Visible = true;
            this.colDoors.Width = 125;
            // 
            // colHorsepower
            // 
            this.colHorsepower.FieldName = "Horsepower";
            this.colHorsepower.Name = "colHorsepower";
            this.colHorsepower.Visible = true;
            this.colHorsepower.Width = 125;
            // 
            // colTorque
            // 
            this.colTorque.FieldName = "Torque";
            this.colTorque.Name = "colTorque";
            this.colTorque.Visible = true;
            this.colTorque.Width = 134;
            // 
            // colCylinders
            // 
            this.colCylinders.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colCylinders.FieldName = "Cylinders";
            this.colCylinders.Name = "colCylinders";
            this.colCylinders.RowIndex = 1;
            this.colCylinders.Visible = true;
            this.colCylinders.Width = 125;
            // 
            // colMPGCity1
            // 
            this.colMPGCity1.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colMPGCity1.FieldName = "MPG City";
            this.colMPGCity1.Name = "colMPGCity1";
            this.colMPGCity1.RowIndex = 1;
            this.colMPGCity1.Visible = true;
            this.colMPGCity1.Width = 125;
            // 
            // colMPGHighway1
            // 
            this.colMPGHighway1.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colMPGHighway1.FieldName = "MPG Highway";
            this.colMPGHighway1.Name = "colMPGHighway1";
            this.colMPGHighway1.RowIndex = 1;
            this.colMPGHighway1.Visible = true;
            this.colMPGHighway1.Width = 134;
            // 
            // gbInfo
            // 
            this.gbInfo.Caption = "Info";
            this.gbInfo.Columns.Add(this.colPhoto);
            this.gbInfo.Columns.Add(this.colDescription1);
            this.gbInfo.Name = "gbInfo";
            this.gbInfo.Visible = false;
            this.gbInfo.VisibleIndex = -1;
            this.gbInfo.Width = 138;
            // 
            // colPhoto
            // 
            this.colPhoto.ColumnEdit = this.repositoryItemImageEdit1;
            this.colPhoto.FieldName = "Photo";
            this.colPhoto.Name = "colPhoto";
            this.colPhoto.Visible = true;
            this.colPhoto.Width = 138;
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            this.repositoryItemImageEdit1.PopupFormMinSize = new System.Drawing.Size(400, 250);
            // 
            // colDescription1
            // 
            this.colDescription1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsFilter.AllowFilter = false;
            this.colDescription1.RowIndex = 1;
            this.colDescription1.Visible = true;
            this.colDescription1.Width = 138;
            // 
            // colTransmissionSpeeds
            // 
            this.colTransmissionSpeeds.FieldName = "Transmission Speeds";
            this.colTransmissionSpeeds.Name = "colTransmissionSpeeds";
            this.colTransmissionSpeeds.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colTransmissionType
            // 
            this.colTransmissionType.FieldName = "Transmission Type";
            this.colTransmissionType.Name = "colTransmissionType";
            this.colTransmissionType.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Model";
            this.gridControl1.DataSource = this.dsVehicles1;
            this.gridControl1.ExternalRepository = this.persistentRepository1;
            this.gridControl1.Location = new System.Drawing.Point(14, 36);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1141, 637);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colImage,
            this.colTrademarkID,
            this.colName,
            this.colModification,
            this.colCategoryID,
            this.colMPGCity,
            this.colMPGHighway,
            this.colDescription});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            this.gridView1.OptionsEditForm.FormCaptionFormat = "{Name} {Modification}";
            this.gridView1.OptionsEditForm.PopupEditFormWidth = 900;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomRowCellEditForEditing += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEditForEditing);
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            // 
            // colImage
            // 
            this.colImage.ColumnEdit = this.repositoryItemPictureEdit2;
            this.colImage.FieldName = "Photo";
            this.colImage.Name = "colImage";
            this.colImage.OptionsEditForm.CaptionLocation = DevExpress.XtraGrid.EditForm.EditFormColumnCaptionLocation.Near;
            this.colImage.OptionsEditForm.RowSpan = 3;
            this.colImage.OptionsEditForm.UseEditorColRowSpan = false;
            this.colImage.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.colImage.Width = 109;
            // 
            // colTrademarkID
            // 
            this.colTrademarkID.Caption = "Trademark";
            this.colTrademarkID.ColumnEdit = this.repositoryItemGridLookUpEdit1;
            this.colTrademarkID.FieldName = "TrademarkID";
            this.colTrademarkID.Image = ((System.Drawing.Image)(resources.GetObject("colTrademarkID.Image")));
            this.colTrademarkID.MinWidth = 80;
            this.colTrademarkID.Name = "colTrademarkID";
            this.colTrademarkID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTrademarkID.Visible = true;
            this.colTrademarkID.VisibleIndex = 0;
            this.colTrademarkID.Width = 175;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 211;
            // 
            // colModification
            // 
            this.colModification.FieldName = "Modification";
            this.colModification.Name = "colModification";
            this.colModification.Visible = true;
            this.colModification.VisibleIndex = 2;
            this.colModification.Width = 186;
            // 
            // colCategoryID
            // 
            this.colCategoryID.Caption = "Category";
            this.colCategoryID.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colCategoryID.FieldName = "CategoryID";
            this.colCategoryID.Name = "colCategoryID";
            this.colCategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCategoryID.Visible = true;
            this.colCategoryID.VisibleIndex = 3;
            this.colCategoryID.Width = 166;
            // 
            // colMPGCity
            // 
            this.colMPGCity.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colMPGCity.FieldName = "MPG City";
            this.colMPGCity.Name = "colMPGCity";
            this.colMPGCity.Visible = true;
            this.colMPGCity.VisibleIndex = 4;
            this.colMPGCity.Width = 111;
            // 
            // colMPGHighway
            // 
            this.colMPGHighway.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colMPGHighway.FieldName = "MPG Highway";
            this.colMPGHighway.Name = "colMPGHighway";
            this.colMPGHighway.Visible = true;
            this.colMPGHighway.VisibleIndex = 5;
            this.colMPGHighway.Width = 111;
            // 
            // colDescription
            // 
            this.colDescription.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsEditForm.CaptionLocation = DevExpress.XtraGrid.EditForm.EditFormColumnCaptionLocation.Near;
            this.colDescription.OptionsEditForm.ColumnSpan = 3;
            this.colDescription.OptionsEditForm.RowSpan = 3;
            this.colDescription.OptionsEditForm.StartNewRow = true;
            this.colDescription.OptionsEditForm.UseEditorColRowSpan = false;
            this.colDescription.OptionsFilter.AllowFilter = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 6;
            this.colDescription.Width = 143;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1169, 687);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1169, 687);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1145, 641);
            this.layoutControlGroup2.Text = "Simple Edit Form";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1145, 641);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1145, 641);
            this.layoutControlGroup3.Text = "Advanced Edit Form";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl2;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1145, 641);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // GridEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "GridEditForm";
            this.Size = new System.Drawing.Size(1169, 687);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicles1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private XtraLayout.LayoutControl layoutControl1;
        private GridControl gridControl1;
        private Views.Grid.GridView gridView1;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private XtraLayout.LayoutControlGroup layoutControlGroup2;
        private XtraLayout.LayoutControlItem layoutControlItem1;
        private GridControl gridControl2;
        private XtraLayout.LayoutControlGroup layoutControlGroup3;
        private XtraLayout.LayoutControlItem layoutControlItem2;
        private dsVehicles dsVehicles1;
        private Columns.GridColumn colID;
        private Columns.GridColumn colTrademarkID;
        private Columns.GridColumn colName;
        private Columns.GridColumn colModification;
        private Columns.GridColumn colCategoryID;
        private Columns.GridColumn colMPGCity;
        private Columns.GridColumn colMPGHighway;
        private Columns.GridColumn colDescription;
        private Columns.GridColumn colImage;
        private XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private RepositoryItemGridLookUpEditWithGlyph repositoryItemGridLookUpEdit1;
        private Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private Columns.GridColumn colLogo;
        private Columns.GridColumn colTName;
        private XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private Views.BandedGrid.BandedGridColumn colImage1;
        private Views.BandedGrid.BandedGridColumn colID1;
        private Views.BandedGrid.BandedGridColumn colTrademarkID1;
        private Views.BandedGrid.BandedGridColumn colName1;
        private Views.BandedGrid.BandedGridColumn colModification1;
        private Views.BandedGrid.BandedGridColumn colPhoto;
        private Views.BandedGrid.BandedGridColumn colCategoryID1;
        private Views.BandedGrid.BandedGridColumn colBodyStyleID;
        private Views.BandedGrid.BandedGridColumn colPrice;
        private Views.BandedGrid.BandedGridColumn colDescription1;
        private Views.BandedGrid.BandedGridColumn colDoors;
        private Views.BandedGrid.BandedGridColumn colHorsepower;
        private Views.BandedGrid.BandedGridColumn colTorque;
        private Views.BandedGrid.BandedGridColumn colCylinders;
        private Views.BandedGrid.BandedGridColumn colMPGCity1;
        private Views.BandedGrid.BandedGridColumn colMPGHighway1;
        private Views.BandedGrid.GridBand gbImage;
        private Views.BandedGrid.GridBand gbMain;
        private Views.BandedGrid.GridBand gbPerfomance;
        private Views.BandedGrid.GridBand gbInfo;
        private XtraEditors.Repository.PersistentRepository persistentRepository1;
        private XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private Views.BandedGrid.BandedGridColumn colTransmissionSpeeds;
        private Views.BandedGrid.BandedGridColumn colTransmissionType;

    }
}
