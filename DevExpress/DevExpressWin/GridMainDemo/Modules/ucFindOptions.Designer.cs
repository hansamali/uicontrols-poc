namespace DevExpress.XtraGrid.Demos {
    partial class ucFindOptions {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucFindOptions));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ceSearchInPreview = new DevExpress.XtraEditors.CheckEdit();
            this.ceShowFindButton = new DevExpress.XtraEditors.CheckEdit();
            this.icbFindMode = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.seFindDelay = new DevExpress.XtraEditors.SpinEdit();
            this.ceShowCloseButton = new DevExpress.XtraEditors.CheckEdit();
            this.ceShowClearButton = new DevExpress.XtraEditors.CheckEdit();
            this.ceHighlightFindResults = new DevExpress.XtraEditors.CheckEdit();
            this.ceClearFind = new DevExpress.XtraEditors.CheckEdit();
            this.ceAlwaysVisible = new DevExpress.XtraEditors.CheckEdit();
            this.ceAllowFindFilter = new DevExpress.XtraEditors.CheckEdit();
            this.cbFindFilterColumns = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceSearchInPreview.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowFindButton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbFindMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFindDelay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowCloseButton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowClearButton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceHighlightFindResults.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceClearFind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAlwaysVisible.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllowFindFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFindFilterColumns.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.ceSearchInPreview);
            this.layoutControl1.Controls.Add(this.ceShowFindButton);
            this.layoutControl1.Controls.Add(this.icbFindMode);
            this.layoutControl1.Controls.Add(this.seFindDelay);
            this.layoutControl1.Controls.Add(this.ceShowCloseButton);
            this.layoutControl1.Controls.Add(this.ceShowClearButton);
            this.layoutControl1.Controls.Add(this.ceHighlightFindResults);
            this.layoutControl1.Controls.Add(this.ceClearFind);
            this.layoutControl1.Controls.Add(this.ceAlwaysVisible);
            this.layoutControl1.Controls.Add(this.ceAllowFindFilter);
            this.layoutControl1.Controls.Add(this.cbFindFilterColumns);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.UseParentAutoScaleFactor = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            // 
            // ceSearchInPreview
            // 
            resources.ApplyResources(this.ceSearchInPreview, "ceSearchInPreview");
            this.ceSearchInPreview.Name = "ceSearchInPreview";
            this.ceSearchInPreview.Properties.Caption = resources.GetString("ceSearchInPreview.Properties.Caption");
            this.ceSearchInPreview.StyleController = this.layoutControl1;
            this.ceSearchInPreview.CheckedChanged += new System.EventHandler(this.ceSearchInPreview_CheckedChanged);
            // 
            // ceShowFindButton
            // 
            resources.ApplyResources(this.ceShowFindButton, "ceShowFindButton");
            this.ceShowFindButton.Name = "ceShowFindButton";
            this.ceShowFindButton.Properties.Caption = resources.GetString("ceShowFindButton.Properties.Caption");
            this.ceShowFindButton.StyleController = this.layoutControl1;
            this.ceShowFindButton.CheckedChanged += new System.EventHandler(this.ceShowFindButton_CheckedChanged);
            // 
            // icbFindMode
            // 
            resources.ApplyResources(this.icbFindMode, "icbFindMode");
            this.icbFindMode.Name = "icbFindMode";
            this.icbFindMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("icbFindMode.Properties.Buttons"))))});
            this.icbFindMode.StyleController = this.layoutControl1;
            this.icbFindMode.SelectedIndexChanged += new System.EventHandler(this.icbFindMode_SelectedIndexChanged);
            // 
            // seFindDelay
            // 
            resources.ApplyResources(this.seFindDelay, "seFindDelay");
            this.seFindDelay.Name = "seFindDelay";
            this.seFindDelay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seFindDelay.Properties.IsFloatValue = false;
            this.seFindDelay.Properties.Mask.EditMask = resources.GetString("seFindDelay.Properties.Mask.EditMask");
            this.seFindDelay.Properties.MaxValue = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.seFindDelay.Properties.MinValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.seFindDelay.StyleController = this.layoutControl1;
            this.seFindDelay.EditValueChanged += new System.EventHandler(this.seFindDelay_EditValueChanged);
            // 
            // ceShowCloseButton
            // 
            resources.ApplyResources(this.ceShowCloseButton, "ceShowCloseButton");
            this.ceShowCloseButton.Name = "ceShowCloseButton";
            this.ceShowCloseButton.Properties.Caption = resources.GetString("ceShowCloseButton.Properties.Caption");
            this.ceShowCloseButton.StyleController = this.layoutControl1;
            this.ceShowCloseButton.CheckedChanged += new System.EventHandler(this.ceShowCloseButton_CheckedChanged);
            // 
            // ceShowClearButton
            // 
            resources.ApplyResources(this.ceShowClearButton, "ceShowClearButton");
            this.ceShowClearButton.Name = "ceShowClearButton";
            this.ceShowClearButton.Properties.Caption = resources.GetString("ceShowClearButton.Properties.Caption");
            this.ceShowClearButton.StyleController = this.layoutControl1;
            this.ceShowClearButton.CheckedChanged += new System.EventHandler(this.ceShowClearButton_CheckedChanged);
            // 
            // ceHighlightFindResults
            // 
            resources.ApplyResources(this.ceHighlightFindResults, "ceHighlightFindResults");
            this.ceHighlightFindResults.Name = "ceHighlightFindResults";
            this.ceHighlightFindResults.Properties.Caption = resources.GetString("ceHighlightFindResults.Properties.Caption");
            this.ceHighlightFindResults.StyleController = this.layoutControl1;
            this.ceHighlightFindResults.CheckedChanged += new System.EventHandler(this.ceHighlightFindResults_CheckedChanged);
            // 
            // ceClearFind
            // 
            resources.ApplyResources(this.ceClearFind, "ceClearFind");
            this.ceClearFind.Name = "ceClearFind";
            this.ceClearFind.Properties.Caption = resources.GetString("ceClearFind.Properties.Caption");
            this.ceClearFind.StyleController = this.layoutControl1;
            this.ceClearFind.CheckedChanged += new System.EventHandler(this.ceClearFind_CheckedChanged);
            // 
            // ceAlwaysVisible
            // 
            resources.ApplyResources(this.ceAlwaysVisible, "ceAlwaysVisible");
            this.ceAlwaysVisible.Name = "ceAlwaysVisible";
            this.ceAlwaysVisible.Properties.Caption = resources.GetString("ceAlwaysVisible.Properties.Caption");
            this.ceAlwaysVisible.StyleController = this.layoutControl1;
            this.ceAlwaysVisible.CheckedChanged += new System.EventHandler(this.ceAlwaysVisible_CheckedChanged);
            // 
            // ceAllowFindFilter
            // 
            resources.ApplyResources(this.ceAllowFindFilter, "ceAllowFindFilter");
            this.ceAllowFindFilter.Name = "ceAllowFindFilter";
            this.ceAllowFindFilter.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.ceAllowFindFilter.Properties.Caption = resources.GetString("ceAllowFindFilter.Properties.Caption");
            this.ceAllowFindFilter.StyleController = this.layoutControl1;
            this.ceAllowFindFilter.CheckedChanged += new System.EventHandler(this.ceAllowFindFilter_CheckedChanged);
            // 
            // cbFindFilterColumns
            // 
            resources.ApplyResources(this.cbFindFilterColumns, "cbFindFilterColumns");
            this.cbFindFilterColumns.Name = "cbFindFilterColumns";
            this.cbFindFilterColumns.Properties.DropDownRows = 25;
            this.cbFindFilterColumns.StyleController = this.layoutControl1;
            this.cbFindFilterColumns.EditValueChanged += new System.EventHandler(this.cbFindFilterColumns_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            resources.ApplyResources(this.layoutControlGroup1, "layoutControlGroup1");
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(225, 356);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.ceAllowFindFilter;
            resources.ApplyResources(this.layoutControlItem1, "layoutControlItem1");
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ceAlwaysVisible;
            resources.ApplyResources(this.layoutControlItem2, "layoutControlItem2");
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.ceClearFind;
            resources.ApplyResources(this.layoutControlItem3, "layoutControlItem3");
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.seFindDelay;
            resources.ApplyResources(this.layoutControlItem6, "layoutControlItem6");
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 204);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.cbFindFilterColumns;
            resources.ApplyResources(this.layoutControlItem7, "layoutControlItem7");
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 228);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.icbFindMode;
            resources.ApplyResources(this.layoutControlItem8, "layoutControlItem8");
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 252);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.ceHighlightFindResults;
            resources.ApplyResources(this.layoutControlItem4, "layoutControlItem4");
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 69);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.ceShowCloseButton;
            resources.ApplyResources(this.layoutControlItem5, "layoutControlItem5");
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.ceShowClearButton;
            resources.ApplyResources(this.layoutControlItem9, "layoutControlItem9");
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 125);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.ceShowFindButton;
            resources.ApplyResources(this.layoutControlItem10, "layoutControlItem10");
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 148);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.ceSearchInPreview;
            resources.ApplyResources(this.layoutControlItem11, "layoutControlItem11");
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 171);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(225, 23);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 276);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(225, 80);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 92);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(225, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 194);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(225, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ucFindOptions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "ucFindOptions";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceSearchInPreview.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowFindButton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbFindMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFindDelay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowCloseButton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowClearButton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceHighlightFindResults.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceClearFind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAlwaysVisible.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllowFindFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFindFilterColumns.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.CheckEdit ceAlwaysVisible;
        private DevExpress.XtraEditors.CheckEdit ceAllowFindFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit ceClearFind;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.CheckEdit ceShowCloseButton;
        private DevExpress.XtraEditors.CheckEdit ceHighlightFindResults;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SpinEdit seFindDelay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbFindMode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.CheckEdit ceSearchInPreview;
        private DevExpress.XtraEditors.CheckEdit ceShowFindButton;
        private DevExpress.XtraEditors.CheckEdit ceShowClearButton;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbFindFilterColumns;
        private XtraLayout.EmptySpaceItem emptySpaceItem1;
        private XtraLayout.EmptySpaceItem emptySpaceItem2;
        private XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}
