namespace DevExpress.XtraGrid.Demos {
    partial class FilterControl {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing) {
                if(components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FilterControl));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.filterControl1 = new DevExpress.XtraEditors.FilterControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumnUnitPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridColumnQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.ceToolTips = new DevExpress.XtraEditors.CheckEdit();
            this.ceOperandTypeIcon = new DevExpress.XtraEditors.CheckEdit();
            this.ceGroupCommandsIcon = new DevExpress.XtraEditors.CheckEdit();
            this.seSeparatorHeight = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.seLevelIndent = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.ceValue = new DevExpress.XtraEditors.ColorEdit();
            this.ceOperator = new DevExpress.XtraEditors.ColorEdit();
            this.ceGroupOperator = new DevExpress.XtraEditors.ColorEdit();
            this.ceFieldName = new DevExpress.XtraEditors.ColorEdit();
            this.ceEmptyValue = new DevExpress.XtraEditors.ColorEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbApply = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.filterControl2 = new DevExpress.XtraEditors.FilterControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.sbReset = new DevExpress.XtraEditors.SimpleButton();
            this.sbApplyFilter2 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSubject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnImplemented = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.gridColumnSuspended = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceToolTips.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOperandTypeIcon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGroupCommandsIcon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seSeparatorHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seLevelIndent.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOperator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGroupOperator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceFieldName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceEmptyValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CaptionImageUri.Uri = "";
            resources.ApplyResources(this.splitContainerControl1, "splitContainerControl1");
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.filterControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.xtraTabControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl1);
            resources.ApplyResources(this.splitContainerControl1.Panel1, "splitContainerControl1.Panel1");
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl1);
            resources.ApplyResources(this.splitContainerControl1.Panel2, "splitContainerControl1.Panel2");
            this.splitContainerControl1.SplitterPosition = 215;
            // 
            // filterControl1
            // 
            this.filterControl1.Cursor = System.Windows.Forms.Cursors.Arrow;
            resources.ApplyResources(this.filterControl1, "filterControl1");
            this.filterControl1.Name = "filterControl1";
            this.filterControl1.SourceControl = this.gridControl1;
            // 
            // gridControl1
            // 
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemCalcEdit1,
            this.repositoryItemSpinEdit1});
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnOrder,
            this.gridColumnProduct,
            this.gridColumnUnitPrice,
            this.gridColumnQuantity,
            this.gridColumnDiscount});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("gridView1.GroupSummary"))), resources.GetString("gridView1.GroupSummary1"), ((DevExpress.XtraGrid.Columns.GridColumn)(resources.GetObject("gridView1.GroupSummary2"))), resources.GetString("gridView1.GroupSummary3"))});
            this.gridView1.Images = this.imageList1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // gridColumnOrder
            // 
            resources.ApplyResources(this.gridColumnOrder, "gridColumnOrder");
            this.gridColumnOrder.FieldName = "OrderID";
            this.gridColumnOrder.Name = "gridColumnOrder";
            // 
            // gridColumnProduct
            // 
            resources.ApplyResources(this.gridColumnProduct, "gridColumnProduct");
            this.gridColumnProduct.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.gridColumnProduct.FieldName = "ProductID";
            this.gridColumnProduct.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumnProduct.Image = ((System.Drawing.Image)(resources.GetObject("gridColumnProduct.Image")));
            this.gridColumnProduct.Name = "gridColumnProduct";
            // 
            // repositoryItemLookUpEdit1
            // 
            resources.ApplyResources(this.repositoryItemLookUpEdit1, "repositoryItemLookUpEdit1");
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemLookUpEdit1.Buttons"))))});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repositoryItemLookUpEdit1.Columns"), resources.GetString("repositoryItemLookUpEdit1.Columns1"))});
            this.repositoryItemLookUpEdit1.DisplayMember = "ProductName";
            this.repositoryItemLookUpEdit1.DropDownRows = 10;
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.PopupWidth = 220;
            this.repositoryItemLookUpEdit1.ValueMember = "ProductID";
            // 
            // gridColumnUnitPrice
            // 
            resources.ApplyResources(this.gridColumnUnitPrice, "gridColumnUnitPrice");
            this.gridColumnUnitPrice.ColumnEdit = this.repositoryItemCalcEdit1;
            this.gridColumnUnitPrice.DisplayFormat.FormatString = "c";
            this.gridColumnUnitPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnUnitPrice.FieldName = "UnitPrice";
            this.gridColumnUnitPrice.Name = "gridColumnUnitPrice";
            // 
            // repositoryItemCalcEdit1
            // 
            resources.ApplyResources(this.repositoryItemCalcEdit1, "repositoryItemCalcEdit1");
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemCalcEdit1.Buttons"))))});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // gridColumnQuantity
            // 
            resources.ApplyResources(this.gridColumnQuantity, "gridColumnQuantity");
            this.gridColumnQuantity.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnQuantity.FieldName = "Quantity";
            this.gridColumnQuantity.Name = "gridColumnQuantity";
            // 
            // repositoryItemSpinEdit1
            // 
            resources.ApplyResources(this.repositoryItemSpinEdit1, "repositoryItemSpinEdit1");
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = resources.GetString("repositoryItemSpinEdit1.Mask.EditMask");
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnDiscount
            // 
            resources.ApplyResources(this.gridColumnDiscount, "gridColumnDiscount");
            this.gridColumnDiscount.DisplayFormat.FormatString = "p";
            this.gridColumnDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnDiscount.FieldName = "Discount";
            this.gridColumnDiscount.Name = "gridColumnDiscount";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.panelControl2, "panelControl2");
            this.panelControl2.Name = "panelControl2";
            // 
            // xtraTabControl2
            // 
            resources.ApplyResources(this.xtraTabControl2, "xtraTabControl2");
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.ceToolTips);
            this.xtraTabPage3.Controls.Add(this.ceOperandTypeIcon);
            this.xtraTabPage3.Controls.Add(this.ceGroupCommandsIcon);
            this.xtraTabPage3.Controls.Add(this.seSeparatorHeight);
            this.xtraTabPage3.Controls.Add(this.labelControl7);
            this.xtraTabPage3.Controls.Add(this.seLevelIndent);
            this.xtraTabPage3.Controls.Add(this.labelControl6);
            this.xtraTabPage3.Name = "xtraTabPage3";
            resources.ApplyResources(this.xtraTabPage3, "xtraTabPage3");
            // 
            // ceToolTips
            // 
            resources.ApplyResources(this.ceToolTips, "ceToolTips");
            this.ceToolTips.Name = "ceToolTips";
            this.ceToolTips.Properties.Caption = resources.GetString("ceToolTips.Properties.Caption");
            this.ceToolTips.CheckedChanged += new System.EventHandler(this.ceToolTips_CheckedChanged);
            // 
            // ceOperandTypeIcon
            // 
            resources.ApplyResources(this.ceOperandTypeIcon, "ceOperandTypeIcon");
            this.ceOperandTypeIcon.Name = "ceOperandTypeIcon";
            this.ceOperandTypeIcon.Properties.Caption = resources.GetString("ceOperandTypeIcon.Properties.Caption");
            this.ceOperandTypeIcon.CheckedChanged += new System.EventHandler(this.ceOperandTypeIcon_CheckedChanged);
            // 
            // ceGroupCommandsIcon
            // 
            resources.ApplyResources(this.ceGroupCommandsIcon, "ceGroupCommandsIcon");
            this.ceGroupCommandsIcon.Name = "ceGroupCommandsIcon";
            this.ceGroupCommandsIcon.Properties.Caption = resources.GetString("ceGroupCommandsIcon.Properties.Caption");
            this.ceGroupCommandsIcon.CheckedChanged += new System.EventHandler(this.ceGroupCommandsIcon_CheckedChanged);
            // 
            // seSeparatorHeight
            // 
            resources.ApplyResources(this.seSeparatorHeight, "seSeparatorHeight");
            this.seSeparatorHeight.Name = "seSeparatorHeight";
            this.seSeparatorHeight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seSeparatorHeight.Properties.IsFloatValue = false;
            this.seSeparatorHeight.Properties.Mask.EditMask = resources.GetString("seSeparatorHeight.Properties.Mask.EditMask");
            this.seSeparatorHeight.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.seSeparatorHeight.EditValueChanged += new System.EventHandler(this.seSeparatorHeight_EditValueChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.labelControl7, "labelControl7");
            this.labelControl7.Name = "labelControl7";
            // 
            // seLevelIndent
            // 
            resources.ApplyResources(this.seLevelIndent, "seLevelIndent");
            this.seLevelIndent.Name = "seLevelIndent";
            this.seLevelIndent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seLevelIndent.Properties.IsFloatValue = false;
            this.seLevelIndent.Properties.Mask.EditMask = resources.GetString("seLevelIndent.Properties.Mask.EditMask");
            this.seLevelIndent.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.seLevelIndent.Properties.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.seLevelIndent.EditValueChanged += new System.EventHandler(this.seLevelIndent_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.labelControl6, "labelControl6");
            this.labelControl6.Name = "labelControl6";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.ceValue);
            this.xtraTabPage4.Controls.Add(this.ceOperator);
            this.xtraTabPage4.Controls.Add(this.ceGroupOperator);
            this.xtraTabPage4.Controls.Add(this.ceFieldName);
            this.xtraTabPage4.Controls.Add(this.ceEmptyValue);
            this.xtraTabPage4.Controls.Add(this.labelControl5);
            this.xtraTabPage4.Controls.Add(this.labelControl4);
            this.xtraTabPage4.Controls.Add(this.labelControl3);
            this.xtraTabPage4.Controls.Add(this.labelControl2);
            this.xtraTabPage4.Controls.Add(this.labelControl1);
            this.xtraTabPage4.Name = "xtraTabPage4";
            resources.ApplyResources(this.xtraTabPage4, "xtraTabPage4");
            // 
            // ceValue
            // 
            resources.ApplyResources(this.ceValue, "ceValue");
            this.ceValue.Name = "ceValue";
            this.ceValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ceValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("ceValue.Properties.Buttons"))))});
            this.ceValue.EditValueChanged += new System.EventHandler(this.ceValue_EditValueChanged);
            // 
            // ceOperator
            // 
            resources.ApplyResources(this.ceOperator, "ceOperator");
            this.ceOperator.Name = "ceOperator";
            this.ceOperator.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ceOperator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("ceOperator.Properties.Buttons"))))});
            this.ceOperator.EditValueChanged += new System.EventHandler(this.ceOperator_EditValueChanged);
            // 
            // ceGroupOperator
            // 
            resources.ApplyResources(this.ceGroupOperator, "ceGroupOperator");
            this.ceGroupOperator.Name = "ceGroupOperator";
            this.ceGroupOperator.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ceGroupOperator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("ceGroupOperator.Properties.Buttons"))))});
            this.ceGroupOperator.EditValueChanged += new System.EventHandler(this.ceGroupOperator_EditValueChanged);
            // 
            // ceFieldName
            // 
            resources.ApplyResources(this.ceFieldName, "ceFieldName");
            this.ceFieldName.Name = "ceFieldName";
            this.ceFieldName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ceFieldName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("ceFieldName.Properties.Buttons"))))});
            this.ceFieldName.EditValueChanged += new System.EventHandler(this.ceFieldName_EditValueChanged);
            // 
            // ceEmptyValue
            // 
            resources.ApplyResources(this.ceEmptyValue, "ceEmptyValue");
            this.ceEmptyValue.Name = "ceEmptyValue";
            this.ceEmptyValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ceEmptyValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("ceEmptyValue.Properties.Buttons"))))});
            this.ceEmptyValue.EditValueChanged += new System.EventHandler(this.ceEmptyValue_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.labelControl5, "labelControl5");
            this.labelControl5.Name = "labelControl5";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.labelControl4, "labelControl4");
            this.labelControl4.Name = "labelControl4";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.labelControl3, "labelControl3");
            this.labelControl3.Name = "labelControl3";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Name = "labelControl2";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Name = "labelControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.sbApply);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            // 
            // sbApply
            // 
            resources.ApplyResources(this.sbApply, "sbApply");
            this.sbApply.Name = "sbApply";
            this.sbApply.Click += new System.EventHandler(this.sbApply_Click);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CaptionImageUri.Uri = "";
            resources.ApplyResources(this.splitContainerControl2, "splitContainerControl2");
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.filterControl2);
            this.splitContainerControl2.Panel1.Controls.Add(this.panelControl3);
            resources.ApplyResources(this.splitContainerControl2.Panel1, "splitContainerControl2.Panel1");
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl2);
            resources.ApplyResources(this.splitContainerControl2.Panel2, "splitContainerControl2.Panel2");
            this.splitContainerControl2.SplitterPosition = 161;
            // 
            // filterControl2
            // 
            resources.ApplyResources(this.filterControl2, "filterControl2");
            this.filterControl2.Name = "filterControl2";
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.sbReset);
            this.panelControl3.Controls.Add(this.sbApplyFilter2);
            resources.ApplyResources(this.panelControl3, "panelControl3");
            this.panelControl3.Name = "panelControl3";
            // 
            // sbReset
            // 
            resources.ApplyResources(this.sbReset, "sbReset");
            this.sbReset.Name = "sbReset";
            this.sbReset.Click += new System.EventHandler(this.sbReset_Click);
            // 
            // sbApplyFilter2
            // 
            resources.ApplyResources(this.sbApplyFilter2, "sbApplyFilter2");
            this.sbApplyFilter2.Name = "sbApplyFilter2";
            this.sbApplyFilter2.Click += new System.EventHandler(this.sbApplyFilter2_Click);
            // 
            // gridControl2
            // 
            resources.ApplyResources(this.gridControl2, "gridControl2");
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1});
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnID,
            this.gridColumnSubject,
            this.gridColumnImplemented,
            this.gridColumnSuspended});
            styleFormatCondition1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font")));
            styleFormatCondition1.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor")));
            styleFormatCondition1.Appearance.Options.UseFont = true;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumnSuspended;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = true;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // gridColumnID
            // 
            resources.ApplyResources(this.gridColumnID, "gridColumnID");
            this.gridColumnID.FieldName = "ID";
            this.gridColumnID.Name = "gridColumnID";
            // 
            // gridColumnSubject
            // 
            resources.ApplyResources(this.gridColumnSubject, "gridColumnSubject");
            this.gridColumnSubject.FieldName = "Subject";
            this.gridColumnSubject.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumnSubject.Name = "gridColumnSubject";
            // 
            // gridColumnImplemented
            // 
            resources.ApplyResources(this.gridColumnImplemented, "gridColumnImplemented");
            this.gridColumnImplemented.ColumnEdit = this.repositoryItemProgressBar1;
            this.gridColumnImplemented.FieldName = "Implemented";
            this.gridColumnImplemented.Name = "gridColumnImplemented";
            this.gridColumnImplemented.OptionsColumn.AllowFocus = false;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.ShowTitle = true;
            // 
            // gridColumnSuspended
            // 
            resources.ApplyResources(this.gridColumnSuspended, "gridColumnSuspended");
            this.gridColumnSuspended.FieldName = "Suspended";
            this.gridColumnSuspended.Name = "gridColumnSuspended";
            // 
            // xtraTabControl1
            // 
            resources.ApplyResources(this.xtraTabControl1, "xtraTabControl1");
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            resources.ApplyResources(this.xtraTabPage1, "xtraTabPage1");
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            resources.ApplyResources(this.xtraTabPage2, "xtraTabPage2");
            // 
            // FilterControl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "FilterControl";
            this.Load += new System.EventHandler(this.FilterControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceToolTips.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOperandTypeIcon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGroupCommandsIcon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seSeparatorHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seLevelIndent.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOperator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGroupOperator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceFieldName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceEmptyValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.FilterControl filterControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrder;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnProduct;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUnitPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDiscount;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton sbApply;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.ColorEdit ceEmptyValue;
        private DevExpress.XtraEditors.ColorEdit ceFieldName;
        private DevExpress.XtraEditors.ColorEdit ceGroupOperator;
        private DevExpress.XtraEditors.ColorEdit ceOperator;
        private DevExpress.XtraEditors.ColorEdit ceValue;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SpinEdit seLevelIndent;
        private DevExpress.XtraEditors.SpinEdit seSeparatorHeight;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.CheckEdit ceGroupCommandsIcon;
        private DevExpress.XtraEditors.CheckEdit ceOperandTypeIcon;
        private DevExpress.XtraEditors.CheckEdit ceToolTips;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.FilterControl filterControl2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSubject;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnImplemented;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSuspended;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton sbApplyFilter2;
        private DevExpress.XtraEditors.SimpleButton sbReset;
        private System.ComponentModel.IContainer components;
    }
}
