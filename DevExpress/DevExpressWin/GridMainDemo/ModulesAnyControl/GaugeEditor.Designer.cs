﻿namespace DevExpress.XtraGrid.Demos {
    partial class GaugeEditor {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GaugeEditor));
            DevExpress.XtraGauges.Core.Model.ScaleLabel scaleLabel1 = new DevExpress.XtraGauges.Core.Model.ScaleLabel();
            this.colTask = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewField_colTask = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cpHighPriority = new DevExpress.XtraEditors.ColorPickEdit();
            this.cpMediumPriority = new DevExpress.XtraEditors.ColorPickEdit();
            this.cpOverDue = new DevExpress.XtraEditors.ColorPickEdit();
            this.cpActive = new DevExpress.XtraEditors.ColorPickEdit();
            this.cpLowPriority = new DevExpress.XtraEditors.ColorPickEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.layoutView1 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.colCategory = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colCategory = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colPercentComplete = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colPercentComplete = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colDueDate = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colDueDate = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.gaugeColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colPriority = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.item1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.circularGauge1 = new DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge();
            this.imageIndicatorComponent1 = new DevExpress.XtraGauges.Win.Base.ImageIndicatorComponent();
            this.arcScaleRangeBarComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent();
            this.arcScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cpHighPriority.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpMediumPriority.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpOverDue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpLowPriority.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPercentComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageIndicatorComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleRangeBarComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            this.SuspendLayout();
            // 
            // colTask
            // 
            this.colTask.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.colTask.AppearanceCell.Options.UseFont = true;
            this.colTask.AppearanceCell.Options.UseTextOptions = true;
            this.colTask.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colTask.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTask.Caption = "Task";
            this.colTask.ColumnEdit = this.repositoryItemMemoEdit2;
            this.colTask.FieldName = "TaskName";
            this.colTask.LayoutViewField = this.layoutViewField_colTask;
            this.colTask.Name = "colTask";
            this.colTask.OptionsColumn.AllowEdit = false;
            this.colTask.OptionsColumn.AllowFocus = false;
            this.colTask.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colTask.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colTask.OptionsFilter.AllowAutoFilter = false;
            this.colTask.OptionsFilter.AllowFilter = false;
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // layoutViewField_colTask
            // 
            this.layoutViewField_colTask.EditorPreferredWidth = 158;
            this.layoutViewField_colTask.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colTask.MaxSize = new System.Drawing.Size(0, 104);
            this.layoutViewField_colTask.MinSize = new System.Drawing.Size(41, 104);
            this.layoutViewField_colTask.Name = "layoutViewField_colTask";
            this.layoutViewField_colTask.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_colTask.OptionsCustomization.AllowDrop = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_colTask.Padding = new DevExpress.XtraLayout.Utils.Padding(17, 2, 12, 2);
            this.layoutViewField_colTask.Size = new System.Drawing.Size(179, 104);
            this.layoutViewField_colTask.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colTask.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colTask.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colTask.TextVisible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cpHighPriority);
            this.layoutControl1.Controls.Add(this.cpMediumPriority);
            this.layoutControl1.Controls.Add(this.cpOverDue);
            this.layoutControl1.Controls.Add(this.cpActive);
            this.layoutControl1.Controls.Add(this.cpLowPriority);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.gaugeControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(214, 153, 1514, 777);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1204, 622);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cpHighPriority
            // 
            this.cpHighPriority.EditValue = System.Drawing.Color.Empty;
            this.cpHighPriority.Location = new System.Drawing.Point(1019, 192);
            this.cpHighPriority.Name = "cpHighPriority";
            this.cpHighPriority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cpHighPriority.Size = new System.Drawing.Size(161, 20);
            this.cpHighPriority.StyleController = this.layoutControl1;
            this.cpHighPriority.TabIndex = 14;
            // 
            // cpMediumPriority
            // 
            this.cpMediumPriority.EditValue = System.Drawing.Color.Empty;
            this.cpMediumPriority.Location = new System.Drawing.Point(1019, 168);
            this.cpMediumPriority.Name = "cpMediumPriority";
            this.cpMediumPriority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cpMediumPriority.Size = new System.Drawing.Size(161, 20);
            this.cpMediumPriority.StyleController = this.layoutControl1;
            this.cpMediumPriority.TabIndex = 13;
            // 
            // cpOverDue
            // 
            this.cpOverDue.EditValue = System.Drawing.Color.Empty;
            this.cpOverDue.Location = new System.Drawing.Point(1019, 67);
            this.cpOverDue.Name = "cpOverDue";
            this.cpOverDue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cpOverDue.Size = new System.Drawing.Size(161, 20);
            this.cpOverDue.StyleController = this.layoutControl1;
            this.cpOverDue.TabIndex = 12;
            // 
            // cpActive
            // 
            this.cpActive.EditValue = System.Drawing.Color.Empty;
            this.cpActive.Location = new System.Drawing.Point(1019, 43);
            this.cpActive.Name = "cpActive";
            this.cpActive.Properties.AutomaticColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.cpActive.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cpActive.Size = new System.Drawing.Size(161, 20);
            this.cpActive.StyleController = this.layoutControl1;
            this.cpActive.TabIndex = 6;
            // 
            // cpLowPriority
            // 
            this.cpLowPriority.EditValue = System.Drawing.Color.Empty;
            this.cpLowPriority.Location = new System.Drawing.Point(1019, 144);
            this.cpLowPriority.Name = "cpLowPriority";
            this.cpLowPriority.Properties.AutomaticColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(174)))), ((int)(((byte)(85)))));
            this.cpLowPriority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cpLowPriority.Size = new System.Drawing.Size(161, 20);
            this.cpLowPriority.StyleController = this.layoutControl1;
            this.cpLowPriority.TabIndex = 5;
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Location = new System.Drawing.Point(12, 12);
            this.gridControl1.MainView = this.layoutView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.repositoryItemMemoEdit2});
            this.gridControl1.Size = new System.Drawing.Size(852, 359);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView1});
            // 
            // layoutView1
            // 
            this.layoutView1.Appearance.FieldCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.layoutView1.Appearance.FieldCaption.Options.UseFont = true;
            this.layoutView1.Appearance.FieldCaption.Options.UseTextOptions = true;
            this.layoutView1.Appearance.FieldCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.layoutView1.CardMinSize = new System.Drawing.Size(342, 167);
            this.layoutView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.colTask,
            this.colCategory,
            this.colPercentComplete,
            this.colDueDate,
            this.gaugeColumn,
            this.colPriority});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colTask;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            formatConditionRuleValue1.Appearance.Options.UseFont = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.NotEqual;
            formatConditionRuleValue1.Expression = "Len([TaskName]) != 0";
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.layoutView1.FormatRules.Add(gridFormatRule1);
            this.layoutView1.GridControl = this.gridControl1;
            this.layoutView1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colPercentComplete,
            this.layoutViewField_layoutViewColumn1_1});
            this.layoutView1.Name = "layoutView1";
            this.layoutView1.OptionsBehavior.AllowExpandCollapse = false;
            this.layoutView1.OptionsCustomization.ShowGroupCardCaptions = false;
            this.layoutView1.OptionsCustomization.ShowGroupLayout = false;
            this.layoutView1.OptionsCustomization.ShowGroupLayoutTreeView = false;
            this.layoutView1.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.AlignGlobal;
            this.layoutView1.OptionsItemText.TextToControlDistance = 10;
            this.layoutView1.OptionsView.AllowHotTrackFields = false;
            this.layoutView1.OptionsView.CardsAlignment = DevExpress.XtraGrid.Views.Layout.CardsAlignment.Far;
            this.layoutView1.OptionsView.ShowHeaderPanel = false;
            this.layoutView1.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.MultiRow;
            this.layoutView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCategory, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.layoutView1.TemplateCard = this.layoutViewCard1;
            this.layoutView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.layoutView1_CustomUnboundColumnData);
            // 
            // colCategory
            // 
            this.colCategory.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colCategory.AppearanceHeader.ForeColor = System.Drawing.Color.DarkGray;
            this.colCategory.AppearanceHeader.Options.UseFont = true;
            this.colCategory.AppearanceHeader.Options.UseForeColor = true;
            this.colCategory.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategory.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory.Caption = "CATEGORY";
            this.colCategory.FieldName = "CategoryName";
            this.colCategory.LayoutViewField = this.layoutViewField_colCategory;
            this.colCategory.Name = "colCategory";
            this.colCategory.OptionsColumn.AllowEdit = false;
            this.colCategory.OptionsColumn.AllowFocus = false;
            this.colCategory.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colCategory.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCategory.OptionsFilter.AllowAutoFilter = false;
            this.colCategory.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_colCategory
            // 
            this.layoutViewField_colCategory.EditorPreferredWidth = 92;
            this.layoutViewField_colCategory.Location = new System.Drawing.Point(0, 104);
            this.layoutViewField_colCategory.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutViewField_colCategory.MinSize = new System.Drawing.Size(107, 22);
            this.layoutViewField_colCategory.Name = "layoutViewField_colCategory";
            this.layoutViewField_colCategory.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_colCategory.OptionsCustomization.AllowDrop = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_colCategory.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 2, 0, 0);
            this.layoutViewField_colCategory.Size = new System.Drawing.Size(179, 22);
            this.layoutViewField_colCategory.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colCategory.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colCategory.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField_colCategory.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutViewField_colCategory.TextSize = new System.Drawing.Size(58, 13);
            this.layoutViewField_colCategory.TextToControlDistance = 5;
            // 
            // colPercentComplete
            // 
            this.colPercentComplete.Caption = "% Complete";
            this.colPercentComplete.FieldName = "PercentComplete";
            this.colPercentComplete.LayoutViewField = this.layoutViewField_colPercentComplete;
            this.colPercentComplete.Name = "colPercentComplete";
            this.colPercentComplete.OptionsColumn.AllowEdit = false;
            this.colPercentComplete.OptionsColumn.AllowFocus = false;
            this.colPercentComplete.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colPercentComplete.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPercentComplete.OptionsFilter.AllowAutoFilter = false;
            this.colPercentComplete.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_colPercentComplete
            // 
            this.layoutViewField_colPercentComplete.EditorPreferredWidth = 136;
            this.layoutViewField_colPercentComplete.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colPercentComplete.Name = "layoutViewField_colPercentComplete";
            this.layoutViewField_colPercentComplete.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_colPercentComplete.OptionsCustomization.AllowDrop = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_colPercentComplete.Size = new System.Drawing.Size(363, 161);
            this.layoutViewField_colPercentComplete.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colPercentComplete.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_colPercentComplete.TextVisible = false;
            // 
            // colDueDate
            // 
            this.colDueDate.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colDueDate.AppearanceHeader.ForeColor = System.Drawing.Color.DarkGray;
            this.colDueDate.AppearanceHeader.Options.UseFont = true;
            this.colDueDate.AppearanceHeader.Options.UseForeColor = true;
            this.colDueDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDueDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDueDate.Caption = "DUE DATE";
            this.colDueDate.FieldName = "DueDate";
            this.colDueDate.LayoutViewField = this.layoutViewField_colDueDate;
            this.colDueDate.Name = "colDueDate";
            this.colDueDate.OptionsColumn.AllowEdit = false;
            this.colDueDate.OptionsColumn.AllowFocus = false;
            this.colDueDate.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colDueDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDueDate.OptionsFilter.AllowAutoFilter = false;
            this.colDueDate.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_colDueDate
            // 
            this.layoutViewField_colDueDate.EditorPreferredWidth = 92;
            this.layoutViewField_colDueDate.Location = new System.Drawing.Point(0, 126);
            this.layoutViewField_colDueDate.MaxSize = new System.Drawing.Size(0, 35);
            this.layoutViewField_colDueDate.MinSize = new System.Drawing.Size(107, 35);
            this.layoutViewField_colDueDate.Name = "layoutViewField_colDueDate";
            this.layoutViewField_colDueDate.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_colDueDate.OptionsCustomization.AllowDrop = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_colDueDate.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 2, 0, 13);
            this.layoutViewField_colDueDate.Size = new System.Drawing.Size(179, 35);
            this.layoutViewField_colDueDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_colDueDate.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_colDueDate.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutViewField_colDueDate.TextSize = new System.Drawing.Size(53, 13);
            // 
            // gaugeColumn
            // 
            this.gaugeColumn.Caption = "layoutViewColumn1";
            this.gaugeColumn.FieldName = "gaugeColumn";
            this.gaugeColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this.gaugeColumn.Name = "gaugeColumn";
            this.gaugeColumn.OptionsColumn.AllowEdit = false;
            this.gaugeColumn.OptionsColumn.AllowFocus = false;
            this.gaugeColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gaugeColumn.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gaugeColumn.OptionsFilter.AllowAutoFilter = false;
            this.gaugeColumn.OptionsFilter.AllowFilter = false;
            this.gaugeColumn.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 153;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(179, 8);
            this.layoutViewField_layoutViewColumn1.MaxSize = new System.Drawing.Size(157, 153);
            this.layoutViewField_layoutViewColumn1.MinSize = new System.Drawing.Size(157, 153);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_layoutViewColumn1.OptionsCustomization.AllowDrop = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(157, 153);
            this.layoutViewField_layoutViewColumn1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn1.TextVisible = false;
            // 
            // colPriority
            // 
            this.colPriority.Caption = "Priority";
            this.colPriority.FieldName = "Priority";
            this.colPriority.LayoutViewField = this.layoutViewField_layoutViewColumn1_1;
            this.colPriority.Name = "colPriority";
            // 
            // layoutViewField_layoutViewColumn1_1
            // 
            this.layoutViewField_layoutViewColumn1_1.EditorPreferredWidth = 10;
            this.layoutViewField_layoutViewColumn1_1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn1_1.Name = "layoutViewField_layoutViewColumn1_1";
            this.layoutViewField_layoutViewColumn1_1.Size = new System.Drawing.Size(363, 161);
            this.layoutViewField_layoutViewColumn1_1.TextSize = new System.Drawing.Size(109, 20);
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "TemplateCard";
            this.layoutViewCard1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colCategory,
            this.layoutViewField_colDueDate,
            this.layoutViewField_layoutViewColumn1,
            this.layoutViewField_colTask,
            this.item1});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Disable;
            this.layoutViewCard1.OptionsCustomization.AllowDrop = DevExpress.XtraLayout.ItemDragDropMode.Disable;
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 10;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewCard1.Text = "TemplateCard";
            this.layoutViewCard1.TextVisible = false;
            // 
            // item1
            // 
            this.item1.AllowHotTrack = false;
            this.item1.CustomizationFormText = "item1";
            this.item1.Location = new System.Drawing.Point(179, 0);
            this.item1.MaxSize = new System.Drawing.Size(0, 8);
            this.item1.MinSize = new System.Drawing.Size(104, 8);
            this.item1.Name = "item1";
            this.item1.Size = new System.Drawing.Size(157, 8);
            this.item1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item1.Text = "item1";
            this.item1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl1.ColorScheme.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.gaugeControl1.ColorScheme.TargetElements = ((DevExpress.XtraGauges.Core.Base.TargetElement)((DevExpress.XtraGauges.Core.Base.TargetElement.RangeBar | DevExpress.XtraGauges.Core.Base.TargetElement.Label)));
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.circularGauge1});
            this.gaugeControl1.Location = new System.Drawing.Point(606, 375);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(258, 235);
            this.gaugeControl1.TabIndex = 1;
            // 
            // circularGauge1
            // 
            this.circularGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 246, 223);
            this.circularGauge1.Images.AddRange(new DevExpress.XtraGauges.Win.Base.ImageIndicatorComponent[] {
            this.imageIndicatorComponent1});
            this.circularGauge1.Name = "circularGauge1";
            this.circularGauge1.RangeBars.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent[] {
            this.arcScaleRangeBarComponent1});
            this.circularGauge1.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent[] {
            this.arcScaleComponent1});
            // 
            // imageIndicatorComponent1
            // 
            this.imageIndicatorComponent1.AllowImageSkinning = true;
            this.imageIndicatorComponent1.Image = ((System.Drawing.Image)(resources.GetObject("imageIndicatorComponent1.Image")));
            this.imageIndicatorComponent1.ImageLayoutMode = DevExpress.XtraGauges.Core.Drawing.ImageLayoutMode.Stretch;
            this.imageIndicatorComponent1.Name = "circularGauge1_ImageIndicator1";
            this.imageIndicatorComponent1.Position = new DevExpress.XtraGauges.Core.Base.PointF2D(135F, 125F);
            this.imageIndicatorComponent1.Size = new System.Drawing.SizeF(74F, 74F);
            this.imageIndicatorComponent1.ZOrder = -1001;
            // 
            // arcScaleRangeBarComponent1
            // 
            this.arcScaleRangeBarComponent1.AppearanceRangeBar.BackgroundBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#CBCBCB");
            this.arcScaleRangeBarComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleRangeBarComponent1.EndOffset = 1F;
            this.arcScaleRangeBarComponent1.Name = "circularGauge3_RangeBar2";
            this.arcScaleRangeBarComponent1.RoundedCaps = true;
            this.arcScaleRangeBarComponent1.ShowBackground = true;
            this.arcScaleRangeBarComponent1.StartOffset = 84F;
            this.arcScaleRangeBarComponent1.ZOrder = -10;
            // 
            // arcScaleComponent1
            // 
            this.arcScaleComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponent1.EndAngle = -45F;
            scaleLabel1.AppearanceText.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold);
            scaleLabel1.FormatString = "{1}%";
            scaleLabel1.Name = "Label0";
            scaleLabel1.Position = new DevExpress.XtraGauges.Core.Base.PointF2D(183F, 217F);
            scaleLabel1.Size = new System.Drawing.SizeF(100F, 40F);
            scaleLabel1.Text = "%";
            this.arcScaleComponent1.Labels.AddRange(new DevExpress.XtraGauges.Core.Model.ILabel[] {
            scaleLabel1});
            this.arcScaleComponent1.MajorTickCount = 0;
            this.arcScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent1.MajorTickmark.ShapeOffset = -14F;
            this.arcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style16_1;
            this.arcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent1.MaxValue = 100F;
            this.arcScaleComponent1.MinorTickCount = 0;
            this.arcScaleComponent1.MinorTickmark.ShapeOffset = -7F;
            this.arcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style16_2;
            this.arcScaleComponent1.Name = "scale1";
            this.arcScaleComponent1.StartAngle = -270F;
            this.arcScaleComponent1.Value = 1F;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlGroup2,
            this.emptySpaceItem3,
            this.emptySpaceItem2,
            this.layoutControlGroup5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1204, 622);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(856, 363);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 363);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(856, 239);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(475, 239);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gaugeControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(475, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(223, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(381, 239);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "EmbeddedGaugeControl";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(856, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(20, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(20, 602);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(876, 216);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(308, 386);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Task priority";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 101);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(308, 115);
            this.layoutControlGroup4.Text = "Task priority";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.cpLowPriority;
            this.layoutControlItem3.CustomizationFormText = "Low priority:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(173, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(284, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Low priority:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.cpMediumPriority;
            this.layoutControlItem7.CustomizationFormText = "Medium priority:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(173, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(284, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Medium priority:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.cpHighPriority;
            this.layoutControlItem8.CustomizationFormText = "High priotity:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(173, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(284, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "High priotity:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 91);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(104, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(308, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Task status";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutControlGroup3.OptionsCustomization.AllowDrop = DevExpress.XtraLayout.ItemDragDropMode.Allow;
            this.layoutControlGroup3.Size = new System.Drawing.Size(308, 91);
            this.layoutControlGroup3.Text = "Task status";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.cpActive;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(173, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(284, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Active:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.cpOverDue;
            this.layoutControlItem5.CustomizationFormText = "Over due:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(173, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(284, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Over due:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.emptySpaceItem4,
            this.layoutControlGroup4});
            this.layoutControlGroup5.Location = new System.Drawing.Point(876, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(308, 216);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // GaugeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "GaugeEditor";
            this.Size = new System.Drawing.Size(1204, 622);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cpHighPriority.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpMediumPriority.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpOverDue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpLowPriority.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPercentComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageIndicatorComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleRangeBarComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private XtraLayout.LayoutControl layoutControl1;
        private XtraGauges.Win.GaugeControl gaugeControl1;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private GridControl gridControl1;
        private Views.Layout.LayoutView layoutView1;
        private Columns.LayoutViewColumn colTask;
        private Columns.LayoutViewColumn colCategory;
        private Columns.LayoutViewColumn colPercentComplete;
        private Columns.LayoutViewColumn colDueDate;
        private XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private Columns.LayoutViewColumn gaugeColumn;
        private Columns.LayoutViewColumn colPriority;
        private XtraGauges.Win.Gauges.Circular.CircularGauge circularGauge1;
        private XtraGauges.Win.Base.ImageIndicatorComponent imageIndicatorComponent1;
        private XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent arcScaleRangeBarComponent1;
        private XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent1;
        private XtraEditors.ColorPickEdit cpActive;
        private XtraEditors.ColorPickEdit cpLowPriority;
        private XtraLayout.LayoutControlItem layoutControlItem2;
        private XtraLayout.LayoutControlItem layoutControlItem1;
        private XtraLayout.EmptySpaceItem emptySpaceItem1;
        private XtraLayout.LayoutControlGroup layoutControlGroup2;
        private XtraEditors.ColorPickEdit cpHighPriority;
        private XtraEditors.ColorPickEdit cpMediumPriority;
        private XtraEditors.ColorPickEdit cpOverDue;
        private XtraLayout.EmptySpaceItem emptySpaceItem3;
        private Views.Layout.LayoutViewField layoutViewField_colTask;
        private Views.Layout.LayoutViewField layoutViewField_colCategory;
        private Views.Layout.LayoutViewField layoutViewField_colPercentComplete;
        private Views.Layout.LayoutViewField layoutViewField_colDueDate;
        private Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
        private Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_1;
        private Views.Layout.LayoutViewCard layoutViewCard1;
        private XtraLayout.EmptySpaceItem item1;
        private XtraLayout.EmptySpaceItem emptySpaceItem2;
        private XtraLayout.LayoutControlGroup layoutControlGroup5;
        private XtraLayout.LayoutControlGroup layoutControlGroup3;
        private XtraLayout.LayoutControlItem layoutControlItem4;
        private XtraLayout.LayoutControlItem layoutControlItem5;
        private XtraLayout.EmptySpaceItem emptySpaceItem4;
        private XtraLayout.LayoutControlGroup layoutControlGroup4;
        private XtraLayout.LayoutControlItem layoutControlItem3;
        private XtraLayout.LayoutControlItem layoutControlItem7;
        private XtraLayout.LayoutControlItem layoutControlItem8;


    }
}
