﻿#pragma checksum "..\..\..\Layouts\Window1.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "462BB62EA43EBD122370EB89F491DF5A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DevExpress.Core;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Core.ConditionalFormatting;
using DevExpress.Xpf.Core.DataSources;
using DevExpress.Xpf.Core.Serialization;
using DevExpress.Xpf.Core.ServerMode;
using DevExpress.Xpf.DXBinding;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Editors.DataPager;
using DevExpress.Xpf.Editors.DateNavigator;
using DevExpress.Xpf.Editors.ExpressionEditor;
using DevExpress.Xpf.Editors.Filtering;
using DevExpress.Xpf.Editors.Flyout;
using DevExpress.Xpf.Editors.Popups;
using DevExpress.Xpf.Editors.Popups.Calendar;
using DevExpress.Xpf.Editors.RangeControl;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Editors.Settings.Extension;
using DevExpress.Xpf.Editors.Validation;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DockingDemo {
    
    
    /// <summary>
    /// Window1
    /// </summary>
    public partial class Window1 : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem bCut;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem bCopy;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem bPaste;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem bSelectAll;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem bUndo;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem bRedo;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem bClose;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarSubItem smEdit;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarSubItem bsFile;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.Bar childMenu;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.Bar childBar;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Layouts\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Editors.TextEdit textEdit;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DockingDemo;component/layouts/window1.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Layouts\Window1.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.bCut = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 2:
            this.bCopy = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 3:
            this.bPaste = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 4:
            this.bSelectAll = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 5:
            this.bUndo = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 6:
            this.bRedo = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 7:
            this.bClose = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            
            #line 23 "..\..\..\Layouts\Window1.xaml"
            this.bClose.ItemClick += new DevExpress.Xpf.Bars.ItemClickEventHandler(this.bClose_ItemClick);
            
            #line default
            #line hidden
            return;
            case 8:
            this.smEdit = ((DevExpress.Xpf.Bars.BarSubItem)(target));
            return;
            case 9:
            this.bsFile = ((DevExpress.Xpf.Bars.BarSubItem)(target));
            return;
            case 10:
            this.childMenu = ((DevExpress.Xpf.Bars.Bar)(target));
            return;
            case 11:
            this.childBar = ((DevExpress.Xpf.Bars.Bar)(target));
            return;
            case 12:
            this.textEdit = ((DevExpress.Xpf.Editors.TextEdit)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

