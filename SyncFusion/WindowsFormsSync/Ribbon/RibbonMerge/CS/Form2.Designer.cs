#region Copyright Syncfusion Inc. 2001 - 2016
// Copyright Syncfusion Inc. 2001 - 2016. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace RibbonControlMerging
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Grid.GridBaseStyle gridBaseStyle5 = new Syncfusion.Windows.Forms.Grid.GridBaseStyle();
            Syncfusion.Windows.Forms.Grid.GridBaseStyle gridBaseStyle6 = new Syncfusion.Windows.Forms.Grid.GridBaseStyle();
            Syncfusion.Windows.Forms.Grid.GridBaseStyle gridBaseStyle7 = new Syncfusion.Windows.Forms.Grid.GridBaseStyle();
            Syncfusion.Windows.Forms.Grid.GridBaseStyle gridBaseStyle8 = new Syncfusion.Windows.Forms.Grid.GridBaseStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.gridControl1 = new Syncfusion.Windows.Forms.Grid.GridControl();
            this.DataToolStripEx = new Syncfusion.Windows.Forms.Tools.RibbonPanelMergeContainer();
            this.toolStripEx1 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.HyperLinkBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx2 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.GroupBtn = new System.Windows.Forms.ToolStripButton();
            this.UngroupBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx3 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.DataBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx4 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.NameManagerBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx5 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.ViewToolStripEx = new Syncfusion.Windows.Forms.Tools.RibbonPanelMergeContainer();
            this.themesToolStripExt = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.NewCommentsBtn = new System.Windows.Forms.ToolStripButton();
            this.DeleteBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx7 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.SheetBtn = new System.Windows.Forms.ToolStripButton();
            this.WorkbookBtn = new System.Windows.Forms.ToolStripButton();
            this.superAccelerator1 = new Syncfusion.Windows.Forms.Tools.SuperAccelerator(this);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            this.DataToolStripEx.SuspendLayout();
            this.toolStripEx1.SuspendLayout();
            this.toolStripEx2.SuspendLayout();
            this.toolStripEx3.SuspendLayout();
            this.toolStripEx4.SuspendLayout();
            this.toolStripEx5.SuspendLayout();
            this.ViewToolStripEx.SuspendLayout();
            this.themesToolStripExt.SuspendLayout();
            this.toolStripEx7.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.AlphaBlendSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(94)))), ((int)(((byte)(171)))), ((int)(((byte)(222)))));
            gridBaseStyle5.Name = "Header";
            gridBaseStyle5.StyleInfo.Borders.Bottom = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.None);
            gridBaseStyle5.StyleInfo.Borders.Left = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.None);
            gridBaseStyle5.StyleInfo.Borders.Right = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.None);
            gridBaseStyle5.StyleInfo.Borders.Top = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.None);
            gridBaseStyle5.StyleInfo.CellType = "Header";
            gridBaseStyle5.StyleInfo.Font.Bold = true;
            gridBaseStyle5.StyleInfo.Interior = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(199)))), ((int)(((byte)(184))))), System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(234)))), ((int)(((byte)(216))))));
            gridBaseStyle5.StyleInfo.VerticalAlignment = Syncfusion.Windows.Forms.Grid.GridVerticalAlignment.Middle;
            gridBaseStyle6.Name = "Standard";
            gridBaseStyle6.StyleInfo.Font.Facename = "Tahoma";
            gridBaseStyle6.StyleInfo.Interior = new Syncfusion.Drawing.BrushInfo(System.Drawing.SystemColors.Window);
            gridBaseStyle7.Name = "Column Header";
            gridBaseStyle7.StyleInfo.BaseStyle = "Header";
            gridBaseStyle7.StyleInfo.HorizontalAlignment = Syncfusion.Windows.Forms.Grid.GridHorizontalAlignment.Center;
            gridBaseStyle8.Name = "Row Header";
            gridBaseStyle8.StyleInfo.BaseStyle = "Header";
            gridBaseStyle8.StyleInfo.HorizontalAlignment = Syncfusion.Windows.Forms.Grid.GridHorizontalAlignment.Left;
            gridBaseStyle8.StyleInfo.Interior = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Horizontal, System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(199)))), ((int)(((byte)(184))))), System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(234)))), ((int)(((byte)(216))))));
            this.gridControl1.BaseStylesMap.AddRange(new Syncfusion.Windows.Forms.Grid.GridBaseStyle[] {
            gridBaseStyle5,
            gridBaseStyle6,
            gridBaseStyle7,
            gridBaseStyle8});
            this.gridControl1.ColCount = 25;
            this.gridControl1.ColWidthEntries.AddRange(new Syncfusion.Windows.Forms.Grid.GridColWidth[] {
            new Syncfusion.Windows.Forms.Grid.GridColWidth(0, 35)});
            this.gridControl1.DefaultGridBorderStyle = Syncfusion.Windows.Forms.Grid.GridBorderStyle.Solid;
            this.gridControl1.DefaultRowHeight = 20;
            this.gridControl1.GridOfficeScrollBars = Syncfusion.Windows.Forms.OfficeScrollBars.Metro;
            this.gridControl1.Location = new System.Drawing.Point(234, 221);
            this.gridControl1.MetroScrollBars = true;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Properties.ForceImmediateRepaint = false;
            this.gridControl1.Properties.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(212)))), ((int)(((byte)(212)))));
            this.gridControl1.Properties.MarkColHeader = false;
            this.gridControl1.Properties.MarkRowHeader = false;
            this.gridControl1.RowCount = 50;
            this.gridControl1.RowHeightEntries.AddRange(new Syncfusion.Windows.Forms.Grid.GridRowHeight[] {
            new Syncfusion.Windows.Forms.Grid.GridRowHeight(0, 29)});
            this.gridControl1.SerializeCellsBehavior = Syncfusion.Windows.Forms.Grid.GridSerializeCellsBehavior.SerializeAsRangeStylesIntoCode;
            this.gridControl1.ShowCurrentCellBorderBehavior = Syncfusion.Windows.Forms.Grid.GridShowCurrentCellBorder.AlwaysVisible;
            this.gridControl1.Size = new System.Drawing.Size(321, 189);
            this.gridControl1.SmartSizeBox = false;
            this.gridControl1.TabIndex = 2;
            this.gridControl1.Text = "gridControl1";
            this.gridControl1.ThemesEnabled = true;
            this.gridControl1.UseRightToLeftCompatibleTextBox = true;
            // 
            // DataToolStripEx
            // 
            this.DataToolStripEx.CaptionAlignment = Syncfusion.Windows.Forms.Tools.CaptionAlignment.Center;
            this.DataToolStripEx.CaptionTextStyle = Syncfusion.Windows.Forms.Tools.CaptionTextStyle.Plain;
            this.DataToolStripEx.Controls.Add(this.toolStripEx1);
            this.DataToolStripEx.Controls.Add(this.toolStripEx2);
            this.DataToolStripEx.Controls.Add(this.toolStripEx3);
            this.DataToolStripEx.Controls.Add(this.toolStripEx4);
            this.DataToolStripEx.Controls.Add(this.toolStripEx5);
            this.DataToolStripEx.Location = new System.Drawing.Point(5, 118);
            this.DataToolStripEx.Name = "DataToolStripEx";
            this.DataToolStripEx.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Blue;
            this.DataToolStripEx.ScrollPosition = 0;
            this.DataToolStripEx.Size = new System.Drawing.Size(483, 103);
            this.DataToolStripEx.TabIndex = 0;
            this.DataToolStripEx.Text = "DATA";
            // 
            // toolStripEx1
            // 
            this.toolStripEx1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx1.Image = null;
            this.toolStripEx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HyperLinkBtn});
            this.toolStripEx1.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx1.Name = "toolStripEx1";
            this.toolStripEx1.Office12Mode = false;
            this.toolStripEx1.Padding = new System.Windows.Forms.Padding(10, 10, 1, 0);
            this.toolStripEx1.Size = new System.Drawing.Size(75, 97);
            this.toolStripEx1.TabIndex = 7;
            this.toolStripEx1.Text = "Tables";
            // 
            // HyperLinkBtn
            // 
            this.HyperLinkBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.HyperLinkBtn.Image = ((System.Drawing.Image)(resources.GetObject("HyperLinkBtn.Image")));
            this.HyperLinkBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.HyperLinkBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HyperLinkBtn.Name = "HyperLinkBtn";
            this.HyperLinkBtn.Size = new System.Drawing.Size(75, 106);
            this.HyperLinkBtn.Text = "HyperLink";
            this.HyperLinkBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx2
            // 
            this.superAccelerator1.SetCollapsedDropDownAccelerator(this.toolStripEx2, null);
            this.toolStripEx2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx2.Image = null;
            this.toolStripEx2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GroupBtn,
            this.UngroupBtn});
            this.toolStripEx2.Location = new System.Drawing.Point(93, 1);
            this.toolStripEx2.Name = "toolStripEx2";
            this.toolStripEx2.Office12Mode = false;
            this.toolStripEx2.Padding = new System.Windows.Forms.Padding(0, 12, 1, 0);
            this.toolStripEx2.Size = new System.Drawing.Size(123, 121);
            this.toolStripEx2.TabIndex = 6;
            this.toolStripEx2.Text = "Outline";
            // 
            // GroupBtn
            // 
            this.GroupBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.GroupBtn.Image = ((System.Drawing.Image)(resources.GetObject("GroupBtn.Image")));
            this.GroupBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.GroupBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.GroupBtn.Name = "GroupBtn";
            this.GroupBtn.Size = new System.Drawing.Size(52, 106);
            this.GroupBtn.Text = "Group";
            this.GroupBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // UngroupBtn
            // 
            this.UngroupBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.UngroupBtn.Image = ((System.Drawing.Image)(resources.GetObject("UngroupBtn.Image")));
            this.UngroupBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.UngroupBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UngroupBtn.Name = "UngroupBtn";
            this.UngroupBtn.Size = new System.Drawing.Size(68, 106);
            this.UngroupBtn.Text = "Ungroup";
            this.UngroupBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx3
            // 
            this.superAccelerator1.SetCollapsedDropDownAccelerator(this.toolStripEx3, null);
            this.toolStripEx3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx3.Image = null;
            this.toolStripEx3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DataBtn});
            this.toolStripEx3.Location = new System.Drawing.Point(218, 1);
            this.toolStripEx3.Name = "toolStripEx3";
            this.toolStripEx3.Office12Mode = false;
            this.toolStripEx3.Padding = new System.Windows.Forms.Padding(4, 0, 1, 0);
            this.toolStripEx3.Size = new System.Drawing.Size(47, 121);
            this.toolStripEx3.TabIndex = 5;
            this.toolStripEx3.Text = "Data";
            // 
            // DataBtn
            // 
            this.DataBtn.AutoSize = false;
            this.DataBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.DataBtn.Image = ((System.Drawing.Image)(resources.GetObject("DataBtn.Image")));
            this.DataBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.DataBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DataBtn.Name = "DataBtn";
            this.DataBtn.Size = new System.Drawing.Size(40, 60);
            this.DataBtn.Text = "Data\r";
            this.DataBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx4
            // 
            this.toolStripEx4.AutoSize = false;
            this.superAccelerator1.SetCollapsedDropDownAccelerator(this.toolStripEx4, null);
            this.toolStripEx4.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx4.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx4.Image = null;
            this.toolStripEx4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NameManagerBtn});
            this.toolStripEx4.Location = new System.Drawing.Point(267, 1);
            this.toolStripEx4.Name = "toolStripEx4";
            this.toolStripEx4.Office12Mode = false;
            this.toolStripEx4.Padding = new System.Windows.Forms.Padding(0, 10, 1, 0);
            this.toolStripEx4.Size = new System.Drawing.Size(119, 121);
            this.toolStripEx4.TabIndex = 4;
            this.toolStripEx4.Text = "Formulas";
            // 
            // NameManagerBtn
            // 
            this.NameManagerBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.NameManagerBtn.Image = ((System.Drawing.Image)(resources.GetObject("NameManagerBtn.Image")));
            this.NameManagerBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.NameManagerBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NameManagerBtn.Name = "NameManagerBtn";
            this.NameManagerBtn.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.NameManagerBtn.Size = new System.Drawing.Size(78, 108);
            this.NameManagerBtn.Text = "Name\r\nManager";
            this.NameManagerBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx5
            // 
            this.toolStripEx5.AutoSize = false;
            this.superAccelerator1.SetCollapsedDropDownAccelerator(this.toolStripEx5, null);
            this.toolStripEx5.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx5.Image = null;
            this.toolStripEx5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStripEx5.Location = new System.Drawing.Point(388, 1);
            this.toolStripEx5.Name = "toolStripEx5";
            this.toolStripEx5.Office12Mode = false;
            this.toolStripEx5.Padding = new System.Windows.Forms.Padding(0, 10, 1, 0);
            this.toolStripEx5.Size = new System.Drawing.Size(187, 121);
            this.toolStripEx5.TabIndex = 8;
            this.toolStripEx5.Text = "Close";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.toolStripButton1.Image = global::RibbonMerging.Properties.Resources.close;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Padding = new System.Windows.Forms.Padding(10, 15, 0, 0);
            this.toolStripButton1.Size = new System.Drawing.Size(103, 108);
            this.toolStripButton1.Text = "\r\nClose \r\nMergeSheets";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // ViewToolStripEx
            // 
            this.ViewToolStripEx.CaptionAlignment = Syncfusion.Windows.Forms.Tools.CaptionAlignment.Center;
            this.ViewToolStripEx.CaptionTextStyle = Syncfusion.Windows.Forms.Tools.CaptionTextStyle.Plain;
            this.ViewToolStripEx.Controls.Add(this.themesToolStripExt);
            this.ViewToolStripEx.Controls.Add(this.toolStripEx7);
            this.ViewToolStripEx.Location = new System.Drawing.Point(9, 11);
            this.ViewToolStripEx.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.ViewToolStripEx.Name = "ViewToolStripEx";
            this.ViewToolStripEx.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Blue;
            this.ViewToolStripEx.ScrollPosition = 0;
            this.ViewToolStripEx.Size = new System.Drawing.Size(692, 127);
            this.ViewToolStripEx.TabIndex = 1;
            this.ViewToolStripEx.Text = "VIEW";
            // 
            // themesToolStripExt
            // 
            this.superAccelerator1.SetCollapsedDropDownAccelerator(this.themesToolStripExt, null);
            this.themesToolStripExt.DefaultDropDownDirection = System.Windows.Forms.ToolStripDropDownDirection.BelowRight;
            this.themesToolStripExt.Dock = System.Windows.Forms.DockStyle.None;
            this.themesToolStripExt.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.themesToolStripExt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.themesToolStripExt.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.themesToolStripExt.Image = ((System.Drawing.Image)(resources.GetObject("themesToolStripExt.Image")));
            this.themesToolStripExt.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewCommentsBtn,
            this.DeleteBtn});
            this.themesToolStripExt.Location = new System.Drawing.Point(0, 1);
            this.themesToolStripExt.Name = "themesToolStripExt";
            this.themesToolStripExt.Office12Mode = false;
            this.themesToolStripExt.Padding = new System.Windows.Forms.Padding(0, 12, 1, 0);
            this.themesToolStripExt.ShowLauncher = false;
            this.themesToolStripExt.Size = new System.Drawing.Size(135, 121);
            this.themesToolStripExt.TabIndex = 5;
            this.themesToolStripExt.Text = "Comments";
            // 
            // NewCommentsBtn
            // 
            this.NewCommentsBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewCommentsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.NewCommentsBtn.Image = ((System.Drawing.Image)(resources.GetObject("NewCommentsBtn.Image")));
            this.NewCommentsBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.NewCommentsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewCommentsBtn.Name = "NewCommentsBtn";
            this.NewCommentsBtn.Size = new System.Drawing.Size(80, 106);
            this.NewCommentsBtn.Text = "New \r\nComments";
            this.NewCommentsBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.DeleteBtn.Image = ((System.Drawing.Image)(resources.GetObject("DeleteBtn.Image")));
            this.DeleteBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.DeleteBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(52, 106);
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx7
            // 
            this.superAccelerator1.SetCollapsedDropDownAccelerator(this.toolStripEx7, null);
            this.toolStripEx7.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx7.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx7.Image = null;
            this.toolStripEx7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SheetBtn,
            this.WorkbookBtn});
            this.toolStripEx7.Location = new System.Drawing.Point(137, 1);
            this.toolStripEx7.Name = "toolStripEx7";
            this.toolStripEx7.Office12Mode = false;
            this.toolStripEx7.Padding = new System.Windows.Forms.Padding(0, 12, 1, 0);
            this.toolStripEx7.Size = new System.Drawing.Size(136, 121);
            this.toolStripEx7.TabIndex = 4;
            this.toolStripEx7.Text = "Sheets";
            // 
            // SheetBtn
            // 
            this.SheetBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SheetBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.SheetBtn.Image = ((System.Drawing.Image)(resources.GetObject("SheetBtn.Image")));
            this.SheetBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SheetBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SheetBtn.Name = "SheetBtn";
            this.SheetBtn.Size = new System.Drawing.Size(57, 106);
            this.SheetBtn.Text = "Protect\r\nSheet";
            this.SheetBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // WorkbookBtn
            // 
            this.WorkbookBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkbookBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.WorkbookBtn.Image = ((System.Drawing.Image)(resources.GetObject("WorkbookBtn.Image")));
            this.WorkbookBtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.WorkbookBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.WorkbookBtn.Name = "WorkbookBtn";
            this.WorkbookBtn.Size = new System.Drawing.Size(76, 106);
            this.WorkbookBtn.Text = "Protect\r\nWorkbook";
            this.WorkbookBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 544);
            this.Controls.Add(this.DataToolStripEx);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.ViewToolStripEx);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form2";
            this.Text = "Book 1";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            this.DataToolStripEx.ResumeLayout(false);
            this.DataToolStripEx.PerformLayout();
            this.toolStripEx1.ResumeLayout(false);
            this.toolStripEx1.PerformLayout();
            this.toolStripEx2.ResumeLayout(false);
            this.toolStripEx2.PerformLayout();
            this.toolStripEx3.ResumeLayout(false);
            this.toolStripEx3.PerformLayout();
            this.toolStripEx4.ResumeLayout(false);
            this.toolStripEx4.PerformLayout();
            this.toolStripEx5.ResumeLayout(false);
            this.toolStripEx5.PerformLayout();
            this.ViewToolStripEx.ResumeLayout(false);
            this.ViewToolStripEx.PerformLayout();
            this.themesToolStripExt.ResumeLayout(false);
            this.themesToolStripExt.PerformLayout();
            this.toolStripEx7.ResumeLayout(false);
            this.toolStripEx7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Syncfusion.Windows.Forms.Tools.RibbonPanelMergeContainer DataToolStripEx;
        public Syncfusion.Windows.Forms.Tools.RibbonPanelMergeContainer ViewToolStripEx;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx1;
        public System.Windows.Forms.ToolStripButton HyperLinkBtn;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx2;
        public System.Windows.Forms.ToolStripButton GroupBtn;
        public System.Windows.Forms.ToolStripButton UngroupBtn;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx3;
        public System.Windows.Forms.ToolStripButton DataBtn;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx4;
        public System.Windows.Forms.ToolStripButton NameManagerBtn;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx themesToolStripExt;
        public System.Windows.Forms.ToolStripButton NewCommentsBtn;
        public System.Windows.Forms.ToolStripButton DeleteBtn;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx7;
        public System.Windows.Forms.ToolStripButton SheetBtn;
        public System.Windows.Forms.ToolStripButton WorkbookBtn;
        public Syncfusion.Windows.Forms.Grid.GridControl gridControl1;
        private Syncfusion.Windows.Forms.Tools.SuperAccelerator superAccelerator1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx5;
    }
}