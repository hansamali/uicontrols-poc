#region Copyright Syncfusion Inc. 2001 - 2016
// Copyright Syncfusion Inc. 2001 - 2016. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System.Drawing;
namespace OutlookDemo_2010
{
    partial class OutlookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public Syncfusion.Windows.Forms.Tools.TreeNodeAdv treeNodeAdv4;
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Syncfusion.Windows.Forms.BannerTextInfo bannerTextInfo1 = new Syncfusion.Windows.Forms.BannerTextInfo();
            Syncfusion.Windows.Forms.Tools.TreeNodeAdv treeNodeAdv1 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdv();
            Syncfusion.Windows.Forms.Tools.TreeNodeAdv treeNodeAdv2 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdv();
            Syncfusion.Windows.Forms.Tools.TreeNodeAdv treeNodeAdv3 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdv();
            Syncfusion.Windows.Forms.Tools.TreeNodeAdv treeNodeAdv4 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdv();
            Syncfusion.Windows.Forms.Tools.TreeNodeAdv treeNodeAdv5 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdv();
            Syncfusion.Windows.Forms.Tools.TreeNodeAdv treeNodeAdv6 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdv();
            Syncfusion.Windows.Forms.Tools.TreeNodeAdv treeNodeAdv7 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdv();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OutlookForm));
            this.ribbonControlAdv1 = new Syncfusion.Windows.Forms.Tools.RibbonControlAdv();
            this.backStageView1 = new Syncfusion.Windows.Forms.BackStageView(this.components);
            this.backStage1 = new Syncfusion.Windows.Forms.BackStage();
            this.backStageTab2 = new Syncfusion.Windows.Forms.BackStageTab();
            this.panel16 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.backStageTab1 = new Syncfusion.Windows.Forms.BackStageTab();
            this.backStageTab3 = new Syncfusion.Windows.Forms.BackStageTab();
            this.backStageTab4 = new Syncfusion.Windows.Forms.BackStageTab();
            this.backStageTab5 = new Syncfusion.Windows.Forms.BackStageTab();
            this.backStageTab6 = new Syncfusion.Windows.Forms.BackStageTab();
            this.backStageTab7 = new Syncfusion.Windows.Forms.BackStageTab();
            this.backStageButton1 = new Syncfusion.Windows.Forms.BackStageButton();
            this.toolStripTabItem1 = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx1 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripPanelItem13 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.newmail = new System.Windows.Forms.ToolStripButton();
            this.newmailitems = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx2 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripPanelItem1 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.IgnoreButton = new System.Windows.Forms.ToolStripButton();
            this.CleanUpSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.JunkSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripPanelItem12 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx3 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripPanelItem11 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.replybutton = new System.Windows.Forms.ToolStripSplitButton();
            this.replyall = new System.Windows.Forms.ToolStripSplitButton();
            this.forward = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripPanelItem2 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.MeetingButton = new System.Windows.Forms.ToolStripButton();
            this.MoreButton = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx4 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripPanelItem10 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.movetn = new System.Windows.Forms.ToolStripSplitButton();
            this.rules = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx5 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripPanelItem9 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.followupsptbtn = new System.Windows.Forms.ToolStripSplitButton();
            this.readunread = new System.Windows.Forms.ToolStripSplitButton();
            this.categorize = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx6 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripPanelItem3 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.AddressBookButton = new System.Windows.Forms.ToolStripButton();
            this.FilterEmailButton = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripTabItem2 = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx7 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripPanelItem4 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.UpdateFolderButton = new System.Windows.Forms.ToolStripButton();
            this.SendAllButton = new System.Windows.Forms.ToolStripButton();
            this.SendReceiveGroupsSplit = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx8 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx9 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripPanelItem5 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.toolStripSplitButton2 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSplitButton5 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSplitButton6 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx10 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripTabItem3 = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx11 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton25 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx12 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripTabItem5 = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripPanelItem6 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx13 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton28 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton27 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton26 = new System.Windows.Forms.ToolStripButton();
            this.toolStripPanelItem7 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton15 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSplitButton7 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx14 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx15 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton16 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx16 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton19 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton18 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton17 = new System.Windows.Forms.ToolStripButton();
            this.toolStripTabItem9 = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx17 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripSplitButton12 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripButton21 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton20 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx18 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripPanelItem8 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.toolStripCheckBox1 = new Syncfusion.Windows.Forms.Tools.ToolStripCheckBox();
            this.toolStripSplitButton8 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx19 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripSplitButton11 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSplitButton10 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSplitButton9 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx20 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripSplitButton13 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripEx21 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton22 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton24 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton23 = new System.Windows.Forms.ToolStripButton();
            this.splitContainerAdv1 = new Syncfusion.Windows.Forms.Tools.SplitContainerAdv();
            this.panel1 = new System.Windows.Forms.Panel();
            this.treeViewAdv2 = new Syncfusion.Windows.Forms.Tools.TreeViewAdv();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pointerControl1 = new OutlookDemo_2010.PointerControl();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pointerControl7 = new OutlookDemo_2010.PointerControl();
            this.pointerControl6 = new OutlookDemo_2010.PointerControl();
            this.pointerControl5 = new OutlookDemo_2010.PointerControl();
            this.pointerControl4 = new OutlookDemo_2010.PointerControl();
            this.pointerControl3 = new OutlookDemo_2010.PointerControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.InnerSplitterContainer = new Syncfusion.Windows.Forms.Tools.SplitContainerAdv();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.gridGroupingControl1 = new Syncfusion.Windows.Forms.Grid.Grouping.GridGroupingControl();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.outlookSearchBox1 = new OutlookDemo_2010.OutlookSearchBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.MessageRichTextBox = new System.Windows.Forms.RichTextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Tolabel = new System.Windows.Forms.Label();
            this.Maillabel = new System.Windows.Forms.Label();
            this.DateTimeLabel = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.userImage = new System.Windows.Forms.PictureBox();
            this.statusStripEx1 = new Syncfusion.Windows.Forms.Tools.StatusStripEx();
            this.statusStripLabel1 = new Syncfusion.Windows.Forms.Tools.StatusStripLabel();
            this.statusStripLabel2 = new Syncfusion.Windows.Forms.Tools.StatusStripLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.trackBarItem1 = new Syncfusion.Windows.Forms.Tools.TrackBarItem();
            this.HiddenPanel = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pointerControl2 = new OutlookDemo_2010.PointerControl();
            this.splashControl1 = new Syncfusion.Windows.Forms.Tools.SplashControl();
            this.bannerTextProvider1 = new Syncfusion.Windows.Forms.BannerTextProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlAdv1)).BeginInit();
            this.ribbonControlAdv1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backStage1)).BeginInit();
            this.backStage1.SuspendLayout();
            this.backStageTab2.SuspendLayout();
            this.panel16.SuspendLayout();
            this.toolStripTabItem1.Panel.SuspendLayout();
            this.toolStripEx1.SuspendLayout();
            this.toolStripEx2.SuspendLayout();
            this.toolStripEx3.SuspendLayout();
            this.toolStripEx4.SuspendLayout();
            this.toolStripEx5.SuspendLayout();
            this.toolStripEx6.SuspendLayout();
            this.toolStripTabItem2.Panel.SuspendLayout();
            this.toolStripEx7.SuspendLayout();
            this.toolStripEx8.SuspendLayout();
            this.toolStripEx9.SuspendLayout();
            this.toolStripEx10.SuspendLayout();
            this.toolStripTabItem3.Panel.SuspendLayout();
            this.toolStripEx11.SuspendLayout();
            this.toolStripEx12.SuspendLayout();
            this.toolStripEx13.SuspendLayout();
            this.toolStripEx14.SuspendLayout();
            this.toolStripEx15.SuspendLayout();
            this.toolStripEx16.SuspendLayout();
            this.toolStripTabItem9.Panel.SuspendLayout();
            this.toolStripEx17.SuspendLayout();
            this.toolStripEx18.SuspendLayout();
            this.toolStripEx19.SuspendLayout();
            this.toolStripEx20.SuspendLayout();
            this.toolStripEx21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAdv1)).BeginInit();
            this.splitContainerAdv1.Panel1.SuspendLayout();
            this.splitContainerAdv1.Panel2.SuspendLayout();
            this.splitContainerAdv1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeViewAdv2)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InnerSplitterContainer)).BeginInit();
            this.InnerSplitterContainer.Panel1.SuspendLayout();
            this.InnerSplitterContainer.Panel2.SuspendLayout();
            this.InnerSplitterContainer.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridGroupingControl1)).BeginInit();
            this.panel13.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userImage)).BeginInit();
            this.statusStripEx1.SuspendLayout();
            this.HiddenPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControlAdv1
            // 
            this.ribbonControlAdv1.BackStageView = this.backStageView1;
            this.ribbonControlAdv1.CaptionFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControlAdv1.Header.AddMainItem(toolStripTabItem1);
            this.ribbonControlAdv1.Header.AddMainItem(toolStripTabItem2);
            this.ribbonControlAdv1.Header.AddMainItem(toolStripTabItem3);
            this.ribbonControlAdv1.Header.AddMainItem(toolStripTabItem9);
            this.ribbonControlAdv1.HideMenuButtonToolTip = false;
            this.ribbonControlAdv1.Location = new System.Drawing.Point(1, 1);
            this.ribbonControlAdv1.MaximizeToolTip = "Maximize Ribbon";
            this.ribbonControlAdv1.MenuButtonEnabled = true;
            this.ribbonControlAdv1.MenuButtonFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControlAdv1.MenuButtonText = "FILE";
            this.ribbonControlAdv1.MenuButtonWidth = 56;
            this.ribbonControlAdv1.MenuColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(114)))), ((int)(((byte)(198)))));
            this.ribbonControlAdv1.MinimizeToolTip = "Minimize Ribbon";
            this.ribbonControlAdv1.Name = "ribbonControlAdv1";
            this.ribbonControlAdv1.Office2013ColorScheme = Syncfusion.Windows.Forms.Tools.Office2013ColorScheme.White;
            this.ribbonControlAdv1.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Silver;
            // 
            // ribbonControlAdv1.OfficeMenu
            // 
            this.ribbonControlAdv1.OfficeMenu.Name = "OfficeMenu";
            this.ribbonControlAdv1.OfficeMenu.ShowItemToolTips = true;
            this.ribbonControlAdv1.OfficeMenu.Size = new System.Drawing.Size(12, 65);
            this.ribbonControlAdv1.OverFlowButtonToolTip = "Show DropDown";
            this.ribbonControlAdv1.QuickPanelImageLayout = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ribbonControlAdv1.QuickPanelVisible = false;
            this.ribbonControlAdv1.RibbonHeaderImage = Syncfusion.Windows.Forms.Tools.RibbonHeaderImage.Floweral;
            this.ribbonControlAdv1.RibbonStyle = Syncfusion.Windows.Forms.Tools.RibbonStyle.Office2013;
            this.ribbonControlAdv1.SelectedTab = this.toolStripTabItem2;
            this.ribbonControlAdv1.Show2010CustomizeQuickItemDialog = false;
            this.ribbonControlAdv1.ShowRibbonDisplayOptionButton = true;
            this.ribbonControlAdv1.Size = new System.Drawing.Size(1040, 153);
            this.ribbonControlAdv1.SystemText.QuickAccessDialogDropDownName = "Start menu";
            this.ribbonControlAdv1.TabIndex = 0;
            this.ribbonControlAdv1.Text = "f";
            this.ribbonControlAdv1.TitleAlignment = Syncfusion.Windows.Forms.Tools.TextAlignment.Center;
            this.ribbonControlAdv1.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.ribbonControlAdv1.TouchMode = true;
            // 
            // backStageView1
            // 
            this.backStageView1.BackStage = this.backStage1;
            this.backStageView1.HostControl = null;
            this.backStageView1.HostForm = this;
            // 
            // backStage1
            // 
            this.backStage1.AllowDrop = true;
            this.backStage1.BeforeTouchSize = new System.Drawing.Size(1035, 587);
            this.backStage1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.backStage1.Controls.Add(this.backStageTab2);
            this.backStage1.Controls.Add(this.backStageTab1);
            this.backStage1.Controls.Add(this.backStageTab3);
            this.backStage1.Controls.Add(this.backStageTab4);
            this.backStage1.Controls.Add(this.backStageTab5);
            this.backStage1.Controls.Add(this.backStageTab6);
            this.backStage1.Controls.Add(this.backStageTab7);
            this.backStage1.Controls.Add(this.backStageButton1);
            this.backStage1.ItemSize = new System.Drawing.Size(138, 40);
            this.backStage1.Location = new System.Drawing.Point(0, 0);
            this.backStage1.Name = "backStage1";
            this.backStage1.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Silver;
            this.backStage1.Size = new System.Drawing.Size(1035, 587);
            this.backStage1.TabIndex = 1;
            this.backStage1.Visible = false;
            // 
            // backStageTab2
            // 
            this.backStageTab2.Accelerator = "";
            this.backStageTab2.BackColor = System.Drawing.Color.White;
            this.backStageTab2.BackgroundImage = global::OutlookDemo_2010.Properties.Resources._new;
            this.backStageTab2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.backStageTab2.Controls.Add(this.panel16);
            this.backStageTab2.Image = null;
            this.backStageTab2.ImageSize = new System.Drawing.Size(16, 16);
            this.backStageTab2.Location = new System.Drawing.Point(137, 0);
            this.backStageTab2.Name = "backStageTab2";
            this.backStageTab2.Position = new System.Drawing.Point(0, 0);
            this.backStageTab2.ShowCloseButton = true;
            this.backStageTab2.Size = new System.Drawing.Size(898, 587);
            this.backStageTab2.TabIndex = 4;
            this.backStageTab2.Text = "Open/Export";
            this.backStageTab2.ThemesEnabled = false;
            // 
            // panel16
            // 
            this.panel16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel16.Controls.Add(this.button5);
            this.panel16.Controls.Add(this.button4);
            this.panel16.Controls.Add(this.button3);
            this.panel16.Controls.Add(this.button2);
            this.panel16.Controls.Add(this.button1);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(898, 587);
            this.panel16.TabIndex = 0;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.Open;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(41, 42);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(228, 74);
            this.button5.TabIndex = 4;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.otheruser;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(196)))), ((int)(((byte)(224)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(41, 382);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(280, 78);
            this.button4.TabIndex = 3;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.importandexport;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(196)))), ((int)(((byte)(224)))));
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(41, 297);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(268, 64);
            this.button3.TabIndex = 2;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.openoutlookdatafile;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(196)))), ((int)(((byte)(224)))));
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(41, 208);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(280, 67);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.opencalender;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(196)))), ((int)(((byte)(224)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(41, 127);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(289, 67);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // backStageTab1
            // 
            this.backStageTab1.Accelerator = "";
            this.backStageTab1.BackColor = System.Drawing.Color.White;
            this.backStageTab1.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.info1;
            this.backStageTab1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.backStageTab1.Image = null;
            this.backStageTab1.ImageSize = new System.Drawing.Size(16, 16);
            this.backStageTab1.Location = new System.Drawing.Point(137, 0);
            this.backStageTab1.Name = "backStageTab1";
            this.backStageTab1.Position = new System.Drawing.Point(0, 0);
            this.backStageTab1.ShowCloseButton = true;
            this.backStageTab1.Size = new System.Drawing.Size(898, 587);
            this.backStageTab1.TabIndex = 3;
            this.backStageTab1.Text = "Info";
            this.backStageTab1.ThemesEnabled = false;
            // 
            // backStageTab3
            // 
            this.backStageTab3.Accelerator = "";
            this.backStageTab3.BackColor = System.Drawing.Color.White;
            this.backStageTab3.Image = null;
            this.backStageTab3.ImageSize = new System.Drawing.Size(16, 16);
            this.backStageTab3.Location = new System.Drawing.Point(137, 0);
            this.backStageTab3.Name = "backStageTab3";
            this.backStageTab3.Position = new System.Drawing.Point(0, 0);
            this.backStageTab3.ShowCloseButton = true;
            this.backStageTab3.Size = new System.Drawing.Size(898, 587);
            this.backStageTab3.TabIndex = 5;
            this.backStageTab3.Text = "Save As";
            this.backStageTab3.ThemesEnabled = false;
            // 
            // backStageTab4
            // 
            this.backStageTab4.Accelerator = "";
            this.backStageTab4.BackColor = System.Drawing.Color.White;
            this.backStageTab4.Image = null;
            this.backStageTab4.ImageSize = new System.Drawing.Size(16, 16);
            this.backStageTab4.Location = new System.Drawing.Point(137, 0);
            this.backStageTab4.Name = "backStageTab4";
            this.backStageTab4.Position = new System.Drawing.Point(0, 0);
            this.backStageTab4.ShowCloseButton = true;
            this.backStageTab4.Size = new System.Drawing.Size(898, 587);
            this.backStageTab4.TabIndex = 6;
            this.backStageTab4.Text = "Save Attachments";
            this.backStageTab4.ThemesEnabled = false;
            // 
            // backStageTab5
            // 
            this.backStageTab5.Accelerator = "";
            this.backStageTab5.BackColor = System.Drawing.Color.White;
            this.backStageTab5.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.print;
            this.backStageTab5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.backStageTab5.Image = null;
            this.backStageTab5.ImageSize = new System.Drawing.Size(16, 16);
            this.backStageTab5.Location = new System.Drawing.Point(137, 0);
            this.backStageTab5.Name = "backStageTab5";
            this.backStageTab5.Position = new System.Drawing.Point(0, 0);
            this.backStageTab5.ShowCloseButton = true;
            this.backStageTab5.Size = new System.Drawing.Size(898, 587);
            this.backStageTab5.TabIndex = 7;
            this.backStageTab5.Text = "Print";
            this.backStageTab5.ThemesEnabled = false;
            // 
            // backStageTab6
            // 
            this.backStageTab6.Accelerator = "";
            this.backStageTab6.BackColor = System.Drawing.Color.White;
            this.backStageTab6.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.officeaccount;
            this.backStageTab6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.backStageTab6.Image = null;
            this.backStageTab6.ImageSize = new System.Drawing.Size(16, 16);
            this.backStageTab6.Location = new System.Drawing.Point(137, 0);
            this.backStageTab6.Name = "backStageTab6";
            this.backStageTab6.Position = new System.Drawing.Point(0, 0);
            this.backStageTab6.ShowCloseButton = true;
            this.backStageTab6.Size = new System.Drawing.Size(898, 587);
            this.backStageTab6.TabIndex = 8;
            this.backStageTab6.Text = "Office Accounts";
            this.backStageTab6.ThemesEnabled = false;
            // 
            // backStageTab7
            // 
            this.backStageTab7.Accelerator = "";
            this.backStageTab7.BackColor = System.Drawing.Color.White;
            this.backStageTab7.Image = null;
            this.backStageTab7.ImageSize = new System.Drawing.Size(16, 16);
            this.backStageTab7.Location = new System.Drawing.Point(137, 0);
            this.backStageTab7.Name = "backStageTab7";
            this.backStageTab7.Position = new System.Drawing.Point(0, 0);
            this.backStageTab7.ShowCloseButton = true;
            this.backStageTab7.Size = new System.Drawing.Size(898, 587);
            this.backStageTab7.TabIndex = 9;
            this.backStageTab7.Text = "Options";
            this.backStageTab7.ThemesEnabled = false;
            // 
            // backStageButton1
            // 
            this.backStageButton1.Accelerator = "";
            this.backStageButton1.BackColor = System.Drawing.Color.Transparent;
            this.backStageButton1.BeforeTouchSize = new System.Drawing.Size(75, 23);
            this.backStageButton1.IsBackStageButton = false;
            this.backStageButton1.Location = new System.Drawing.Point(0, 275);
            this.backStageButton1.Name = "backStageButton1";
            this.backStageButton1.Size = new System.Drawing.Size(110, 25);
            this.backStageButton1.TabIndex = 10;
            this.backStageButton1.Text = "Exit";
            this.backStageButton1.Click += new System.EventHandler(this.backStageButton1_Click);
            // 
            // toolStripTabItem1
            // 
            this.toolStripTabItem1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripTabItem1.Name = "toolStripTabItem1";
            // 
            // ribbonControlAdv1.ribbonPanel1
            // 
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx1);
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx2);
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx3);
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx4);
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx5);
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx6);
            this.toolStripTabItem1.Panel.LauncherStyle = Syncfusion.Windows.Forms.Tools.LauncherStyle.Metro;
            this.toolStripTabItem1.Panel.Name = "ribbonPanel1";
            this.toolStripTabItem1.Panel.ScrollPosition = 0;
            this.toolStripTabItem1.Panel.TabIndex = 2;
            this.toolStripTabItem1.Panel.Text = "HOME";
            this.toolStripTabItem1.Position = 0;
            this.toolStripTabItem1.Size = new System.Drawing.Size(46, 19);
            this.toolStripTabItem1.Tag = "1";
            this.toolStripTabItem1.Text = "HOME";
            // 
            // toolStripEx1
            // 
            this.toolStripEx1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx1.Image = global::OutlookDemo_2010.Properties.Resources.New_Mail;
            this.toolStripEx1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripEx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPanelItem13});
            this.toolStripEx1.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx1.Name = "toolStripEx1";
            this.toolStripEx1.Office12Mode = false;
            this.toolStripEx1.Size = new System.Drawing.Size(108, 84);
            this.toolStripEx1.TabIndex = 0;
            this.toolStripEx1.Text = "New";
            // 
            // toolStripPanelItem13
            // 
            this.toolStripPanelItem13.CausesValidation = false;
            this.toolStripPanelItem13.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem13.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newmail,
            this.newmailitems});
            this.toolStripPanelItem13.Name = "toolStripPanelItem13";
            this.toolStripPanelItem13.RowCount = 1;
            this.toolStripPanelItem13.Size = new System.Drawing.Size(99, 70);
            this.toolStripPanelItem13.Text = "toolStripPanelItem13";
            this.toolStripPanelItem13.Transparent = true;
            // 
            // newmail
            // 
            this.newmail.Image = global::OutlookDemo_2010.Properties.Resources.New_Mail;
            this.newmail.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.newmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newmail.Name = "newmail";
            this.newmail.Size = new System.Drawing.Size(39, 69);
            this.newmail.Text = "New \r\nMail";
            this.newmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // newmailitems
            // 
            this.newmailitems.Image = global::OutlookDemo_2010.Properties.Resources.New_item;
            this.newmailitems.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.newmailitems.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newmailitems.Name = "newmailitems";
            this.newmailitems.Size = new System.Drawing.Size(56, 74);
            this.newmailitems.Text = "New\r\nItems";
            this.newmailitems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx2
            // 
            this.toolStripEx2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx2.Image = global::OutlookDemo_2010.Properties.Resources.Delete1;
            this.toolStripEx2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPanelItem1,
            this.toolStripPanelItem12});
            this.toolStripEx2.Location = new System.Drawing.Point(110, 1);
            this.toolStripEx2.Name = "toolStripEx2";
            this.toolStripEx2.Office12Mode = false;
            this.toolStripEx2.Size = new System.Drawing.Size(150, 84);
            this.toolStripEx2.TabIndex = 1;
            this.toolStripEx2.Text = "Delete";
            // 
            // toolStripPanelItem1
            // 
            this.toolStripPanelItem1.CausesValidation = false;
            this.toolStripPanelItem1.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IgnoreButton,
            this.CleanUpSplitButton,
            this.JunkSplitButton});
            this.toolStripPanelItem1.Name = "toolStripPanelItem1";
            this.toolStripPanelItem1.Size = new System.Drawing.Size(91, 70);
            this.toolStripPanelItem1.Text = "toolStripPanelItem1";
            this.toolStripPanelItem1.Transparent = true;
            // 
            // IgnoreButton
            // 
            this.IgnoreButton.Image = global::OutlookDemo_2010.Properties.Resources.Ignore;
            this.IgnoreButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IgnoreButton.Name = "IgnoreButton";
            this.IgnoreButton.Size = new System.Drawing.Size(61, 20);
            this.IgnoreButton.Text = "Ignore";
            // 
            // CleanUpSplitButton
            // 
            this.CleanUpSplitButton.Image = global::OutlookDemo_2010.Properties.Resources.clean_up2;
            this.CleanUpSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CleanUpSplitButton.Name = "CleanUpSplitButton";
            this.CleanUpSplitButton.Size = new System.Drawing.Size(87, 20);
            this.CleanUpSplitButton.Text = "Clean Up";
            // 
            // JunkSplitButton
            // 
            this.JunkSplitButton.Image = global::OutlookDemo_2010.Properties.Resources.Junk;
            this.JunkSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.JunkSplitButton.Name = "JunkSplitButton";
            this.JunkSplitButton.Size = new System.Drawing.Size(63, 20);
            this.JunkSplitButton.Text = "Junk";
            // 
            // toolStripPanelItem12
            // 
            this.toolStripPanelItem12.CausesValidation = false;
            this.toolStripPanelItem12.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem12.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2});
            this.toolStripPanelItem12.Name = "toolStripPanelItem12";
            this.toolStripPanelItem12.Size = new System.Drawing.Size(48, 70);
            this.toolStripPanelItem12.Text = "toolStripPanelItem12";
            this.toolStripPanelItem12.Transparent = true;
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::OutlookDemo_2010.Properties.Resources.Delete1;
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(44, 59);
            this.toolStripButton2.Text = "Delete";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx3
            // 
            this.toolStripEx3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx3.Image = global::OutlookDemo_2010.Properties.Resources.Replay;
            this.toolStripEx3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPanelItem11,
            this.toolStripPanelItem2});
            this.toolStripEx3.Location = new System.Drawing.Point(262, 1);
            this.toolStripEx3.Name = "toolStripEx3";
            this.toolStripEx3.Office12Mode = false;
            this.toolStripEx3.Size = new System.Drawing.Size(288, 84);
            this.toolStripEx3.TabIndex = 2;
            this.toolStripEx3.Text = "Respond";
            // 
            // toolStripPanelItem11
            // 
            this.toolStripPanelItem11.AutoSize = false;
            this.toolStripPanelItem11.CausesValidation = false;
            this.toolStripPanelItem11.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem11.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.replybutton,
            this.replyall,
            this.forward});
            this.toolStripPanelItem11.Name = "toolStripPanelItem11";
            this.toolStripPanelItem11.RowCount = 1;
            this.toolStripPanelItem11.Size = new System.Drawing.Size(202, 80);
            this.toolStripPanelItem11.Text = "toolStripPanelItem11";
            this.toolStripPanelItem11.Transparent = true;
            // 
            // replybutton
            // 
            this.replybutton.Image = global::OutlookDemo_2010.Properties.Resources.Replay;
            this.replybutton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.replybutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.replybutton.Name = "replybutton";
            this.replybutton.Size = new System.Drawing.Size(63, 66);
            this.replybutton.Text = "Reply";
            this.replybutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // replyall
            // 
            this.replyall.Image = global::OutlookDemo_2010.Properties.Resources.Replay_All;
            this.replyall.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.replyall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.replyall.Name = "replyall";
            this.replyall.Size = new System.Drawing.Size(69, 66);
            this.replyall.Text = "Reply All";
            this.replyall.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // forward
            // 
            this.forward.Image = global::OutlookDemo_2010.Properties.Resources.Forward2;
            this.forward.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.forward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.forward.Name = "forward";
            this.forward.Size = new System.Drawing.Size(66, 66);
            this.forward.Text = "Forward";
            this.forward.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripPanelItem2
            // 
            this.toolStripPanelItem2.AutoSize = false;
            this.toolStripPanelItem2.CausesValidation = false;
            this.toolStripPanelItem2.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MeetingButton,
            this.MoreButton});
            this.toolStripPanelItem2.Name = "toolStripPanelItem2";
            this.toolStripPanelItem2.Size = new System.Drawing.Size(75, 80);
            this.toolStripPanelItem2.Text = "toolStripPanelItem1";
            this.toolStripPanelItem2.Transparent = true;
            // 
            // MeetingButton
            // 
            this.MeetingButton.Image = global::OutlookDemo_2010.Properties.Resources.Meeting;
            this.MeetingButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MeetingButton.Name = "MeetingButton";
            this.MeetingButton.Size = new System.Drawing.Size(71, 20);
            this.MeetingButton.Text = "Meeting";
            // 
            // MoreButton
            // 
            this.MoreButton.Image = global::OutlookDemo_2010.Properties.Resources.more;
            this.MoreButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MoreButton.Name = "MoreButton";
            this.MoreButton.Size = new System.Drawing.Size(67, 20);
            this.MoreButton.Text = "More";
            // 
            // toolStripEx4
            // 
            this.toolStripEx4.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx4.Image = global::OutlookDemo_2010.Properties.Resources.move;
            this.toolStripEx4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPanelItem10});
            this.toolStripEx4.Location = new System.Drawing.Point(552, 1);
            this.toolStripEx4.Name = "toolStripEx4";
            this.toolStripEx4.Office12Mode = false;
            this.toolStripEx4.Size = new System.Drawing.Size(141, 84);
            this.toolStripEx4.TabIndex = 3;
            this.toolStripEx4.Text = "Move";
            // 
            // toolStripPanelItem10
            // 
            this.toolStripPanelItem10.CausesValidation = false;
            this.toolStripPanelItem10.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem10.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movetn,
            this.rules});
            this.toolStripPanelItem10.Name = "toolStripPanelItem10";
            this.toolStripPanelItem10.RowCount = 1;
            this.toolStripPanelItem10.Size = new System.Drawing.Size(132, 70);
            this.toolStripPanelItem10.Text = "toolStripPanelItem10";
            this.toolStripPanelItem10.Transparent = true;
            // 
            // movetn
            // 
            this.movetn.Image = global::OutlookDemo_2010.Properties.Resources.move;
            this.movetn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.movetn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.movetn.Name = "movetn";
            this.movetn.Size = new System.Drawing.Size(63, 66);
            this.movetn.Text = "Move";
            this.movetn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // rules
            // 
            this.rules.Image = global::OutlookDemo_2010.Properties.Resources.Rules;
            this.rules.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.rules.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rules.Name = "rules";
            this.rules.Size = new System.Drawing.Size(65, 68);
            this.rules.Text = "Rules";
            this.rules.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx5
            // 
            this.toolStripEx5.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx5.Image = global::OutlookDemo_2010.Properties.Resources.follow_up;
            this.toolStripEx5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPanelItem9});
            this.toolStripEx5.Location = new System.Drawing.Point(695, 1);
            this.toolStripEx5.Name = "toolStripEx5";
            this.toolStripEx5.Office12Mode = false;
            this.toolStripEx5.Size = new System.Drawing.Size(260, 84);
            this.toolStripEx5.TabIndex = 4;
            this.toolStripEx5.Text = "Tags";
            // 
            // toolStripPanelItem9
            // 
            this.toolStripPanelItem9.AutoSize = false;
            this.toolStripPanelItem9.CausesValidation = false;
            this.toolStripPanelItem9.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.followupsptbtn,
            this.readunread,
            this.categorize});
            this.toolStripPanelItem9.Name = "toolStripPanelItem9";
            this.toolStripPanelItem9.RowCount = 1;
            this.toolStripPanelItem9.Size = new System.Drawing.Size(251, 80);
            this.toolStripPanelItem9.Text = "toolStripPanelItem9";
            this.toolStripPanelItem9.Transparent = true;
            // 
            // followupsptbtn
            // 
            this.followupsptbtn.Image = global::OutlookDemo_2010.Properties.Resources.follow_up;
            this.followupsptbtn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.followupsptbtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.followupsptbtn.Name = "followupsptbtn";
            this.followupsptbtn.Size = new System.Drawing.Size(76, 59);
            this.followupsptbtn.Text = "Follow Up";
            this.followupsptbtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // readunread
            // 
            this.readunread.Image = global::OutlookDemo_2010.Properties.Resources.Read_unread;
            this.readunread.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.readunread.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.readunread.Name = "readunread";
            this.readunread.Size = new System.Drawing.Size(92, 64);
            this.readunread.Text = "Unread/Read";
            this.readunread.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // categorize
            // 
            this.categorize.Image = global::OutlookDemo_2010.Properties.Resources.catgeroize;
            this.categorize.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.categorize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.categorize.Name = "categorize";
            this.categorize.Size = new System.Drawing.Size(79, 59);
            this.categorize.Text = "Categorize";
            this.categorize.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx6
            // 
            this.toolStripEx6.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx6.Image = global::OutlookDemo_2010.Properties.Resources.Address_Book;
            this.toolStripEx6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPanelItem3});
            this.toolStripEx6.Location = new System.Drawing.Point(957, 1);
            this.toolStripEx6.Name = "toolStripEx6";
            this.toolStripEx6.Office12Mode = false;
            this.toolStripEx6.Size = new System.Drawing.Size(115, 84);
            this.toolStripEx6.TabIndex = 5;
            this.toolStripEx6.Text = "Find";
            // 
            // toolStripPanelItem3
            // 
            this.toolStripPanelItem3.CausesValidation = false;
            this.toolStripPanelItem3.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.AddressBookButton,
            this.FilterEmailButton});
            this.toolStripPanelItem3.Name = "toolStripPanelItem3";
            this.toolStripPanelItem3.Size = new System.Drawing.Size(106, 70);
            this.toolStripPanelItem3.Text = "Find";
            this.toolStripPanelItem3.Transparent = true;
            // 
            // toolStripTextBox1
            // 
            bannerTextInfo1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bannerTextInfo1.Text = "Search";
            this.bannerTextProvider1.SetBannerText(this.toolStripTextBox1, bannerTextInfo1);
            this.toolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            // 
            // AddressBookButton
            // 
            this.AddressBookButton.Image = global::OutlookDemo_2010.Properties.Resources.Address_Book;
            this.AddressBookButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddressBookButton.Name = "AddressBookButton";
            this.AddressBookButton.Size = new System.Drawing.Size(99, 20);
            this.AddressBookButton.Text = "Address Book";
            // 
            // FilterEmailButton
            // 
            this.FilterEmailButton.Image = global::OutlookDemo_2010.Properties.Resources.Filter_Email;
            this.FilterEmailButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FilterEmailButton.Name = "FilterEmailButton";
            this.FilterEmailButton.Size = new System.Drawing.Size(97, 20);
            this.FilterEmailButton.Text = "Filter Email";
            // 
            // toolStripTabItem2
            // 
            this.toolStripTabItem2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripTabItem2.Name = "toolStripTabItem2";
            // 
            // ribbonControlAdv1.ribbonPanel2
            // 
            this.toolStripTabItem2.Panel.Controls.Add(this.toolStripEx7);
            this.toolStripTabItem2.Panel.Controls.Add(this.toolStripEx8);
            this.toolStripTabItem2.Panel.Controls.Add(this.toolStripEx9);
            this.toolStripTabItem2.Panel.Controls.Add(this.toolStripEx10);
            this.toolStripTabItem2.Panel.Name = "ribbonPanel2";
            this.toolStripTabItem2.Panel.ScrollPosition = 0;
            this.toolStripTabItem2.Panel.TabIndex = 3;
            this.toolStripTabItem2.Panel.Text = "SEND/RECEIVE";
            this.toolStripTabItem2.Position = 1;
            this.toolStripTabItem2.Size = new System.Drawing.Size(87, 19);
            this.toolStripTabItem2.Tag = "2";
            this.toolStripTabItem2.Text = "SEND/RECEIVE";
            // 
            // toolStripEx7
            // 
            this.toolStripEx7.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx7.Image = null;
            this.toolStripEx7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripPanelItem4});
            this.toolStripEx7.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx7.Name = "toolStripEx7";
            this.toolStripEx7.Office12Mode = false;
            this.toolStripEx7.Size = new System.Drawing.Size(242, 93);
            this.toolStripEx7.TabIndex = 0;
            this.toolStripEx7.Text = "Send and Receive";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton3.Image = global::OutlookDemo_2010.Properties.Resources.Send_and_recive_all_folder;
            this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(85, 76);
            this.toolStripButton3.Text = "Send/Receive \r\nAll Folders";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripPanelItem4
            // 
            this.toolStripPanelItem4.AutoSize = false;
            this.toolStripPanelItem4.CausesValidation = false;
            this.toolStripPanelItem4.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UpdateFolderButton,
            this.SendAllButton,
            this.SendReceiveGroupsSplit});
            this.toolStripPanelItem4.Name = "toolStripPanelItem4";
            this.toolStripPanelItem4.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripPanelItem4.Size = new System.Drawing.Size(148, 75);
            this.toolStripPanelItem4.Text = "toolStripPanelItem1";
            this.toolStripPanelItem4.Transparent = true;
            // 
            // UpdateFolderButton
            // 
            this.UpdateFolderButton.Image = global::OutlookDemo_2010.Properties.Resources.updated_folder;
            this.UpdateFolderButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.UpdateFolderButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UpdateFolderButton.Name = "UpdateFolderButton";
            this.UpdateFolderButton.Size = new System.Drawing.Size(101, 20);
            this.UpdateFolderButton.Text = "Update Folder";
            // 
            // SendAllButton
            // 
            this.SendAllButton.Image = global::OutlookDemo_2010.Properties.Resources.Send_All;
            this.SendAllButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SendAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SendAllButton.Name = "SendAllButton";
            this.SendAllButton.Size = new System.Drawing.Size(86, 30);
            this.SendAllButton.Text = "Send All";
            // 
            // SendReceiveGroupsSplit
            // 
            this.SendReceiveGroupsSplit.AutoSize = false;
            this.SendReceiveGroupsSplit.Image = global::OutlookDemo_2010.Properties.Resources.Send_Receive_group;
            this.SendReceiveGroupsSplit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SendReceiveGroupsSplit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SendReceiveGroupsSplit.Name = "SendReceiveGroupsSplit";
            this.SendReceiveGroupsSplit.Size = new System.Drawing.Size(148, 18);
            this.SendReceiveGroupsSplit.Text = "Send/ReceiveGroups";
            // 
            // toolStripEx8
            // 
            this.toolStripEx8.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx8.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx8.Image = null;
            this.toolStripEx8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton5,
            this.toolStripButton4});
            this.toolStripEx8.Location = new System.Drawing.Point(244, 1);
            this.toolStripEx8.Name = "toolStripEx8";
            this.toolStripEx8.Office12Mode = false;
            this.toolStripEx8.Size = new System.Drawing.Size(123, 93);
            this.toolStripEx8.TabIndex = 1;
            this.toolStripEx8.Text = "Download";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton5.Image = global::OutlookDemo_2010.Properties.Resources.Show_Progress;
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(56, 76);
            this.toolStripButton5.Text = "Show \r\nProgress";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.AutoSize = false;
            this.toolStripButton4.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton4.Image = global::OutlookDemo_2010.Properties.Resources.Cancel_All;
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(60, 70);
            this.toolStripButton4.Text = "Cancel\r\nAll";
            this.toolStripButton4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx9
            // 
            this.toolStripEx9.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx9.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx9.Image = null;
            this.toolStripEx9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton8,
            this.toolStripPanelItem5});
            this.toolStripEx9.Location = new System.Drawing.Point(373, 1);
            this.toolStripEx9.Name = "toolStripEx9";
            this.toolStripEx9.Office12Mode = false;
            this.toolStripEx9.Size = new System.Drawing.Size(280, 90);
            this.toolStripEx9.TabIndex = 2;
            this.toolStripEx9.Text = "Server";
            this.toolStripEx9.Visible = false;
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(68, 73);
            this.toolStripButton8.Text = "Download \r\nHeaders";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripPanelItem5
            // 
            this.toolStripPanelItem5.CausesValidation = false;
            this.toolStripPanelItem5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripPanelItem5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton2,
            this.toolStripSplitButton5,
            this.toolStripSplitButton6});
            this.toolStripPanelItem5.Name = "toolStripPanelItem5";
            this.toolStripPanelItem5.Size = new System.Drawing.Size(172, 76);
            this.toolStripPanelItem5.Text = "toolStripPanelItem1";
            this.toolStripPanelItem5.Transparent = true;
            // 
            // toolStripSplitButton2
            // 
            this.toolStripSplitButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton2.Image")));
            this.toolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton2.Name = "toolStripSplitButton2";
            this.toolStripSplitButton2.Size = new System.Drawing.Size(137, 20);
            this.toolStripSplitButton2.Text = "Mark to Download";
            // 
            // toolStripSplitButton5
            // 
            this.toolStripSplitButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton5.Image")));
            this.toolStripSplitButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton5.Name = "toolStripSplitButton5";
            this.toolStripSplitButton5.Size = new System.Drawing.Size(155, 20);
            this.toolStripSplitButton5.Text = "UnMark To Download";
            // 
            // toolStripSplitButton6
            // 
            this.toolStripSplitButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton6.Image")));
            this.toolStripSplitButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton6.Name = "toolStripSplitButton6";
            this.toolStripSplitButton6.Size = new System.Drawing.Size(168, 20);
            this.toolStripSplitButton6.Text = "Process Marked Headers";
            // 
            // toolStripEx10
            // 
            this.toolStripEx10.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx10.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx10.Image = null;
            this.toolStripEx10.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton6,
            this.toolStripButton7});
            this.toolStripEx10.Location = new System.Drawing.Point(399, 1);
            this.toolStripEx10.Name = "toolStripEx10";
            this.toolStripEx10.Office12Mode = false;
            this.toolStripEx10.Size = new System.Drawing.Size(182, 101);
            this.toolStripEx10.TabIndex = 3;
            this.toolStripEx10.Text = "Preferences";
            this.toolStripEx10.Visible = false;
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(72, 84);
            this.toolStripButton6.Text = "Download\r\nPreferences";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(47, 84);
            this.toolStripButton7.Text = "Work \r\nOffline";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripTabItem3
            // 
            this.toolStripTabItem3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripTabItem3.ForeColor = System.Drawing.Color.Black;
            this.toolStripTabItem3.Name = "toolStripTabItem3";
            // 
            // ribbonControlAdv1.ribbonPanel3
            // 
            this.toolStripTabItem3.Panel.Controls.Add(this.toolStripEx11);
            this.toolStripTabItem3.Panel.Controls.Add(this.toolStripEx12);
            this.toolStripTabItem3.Panel.Controls.Add(this.toolStripEx13);
            this.toolStripTabItem3.Panel.Controls.Add(this.toolStripEx14);
            this.toolStripTabItem3.Panel.Controls.Add(this.toolStripEx15);
            this.toolStripTabItem3.Panel.Controls.Add(this.toolStripEx16);
            this.toolStripTabItem3.Panel.Name = "ribbonPanel3";
            this.toolStripTabItem3.Panel.ScrollPosition = 0;
            this.toolStripTabItem3.Panel.TabIndex = 4;
            this.toolStripTabItem3.Panel.Text = "FOLDER";
            this.toolStripTabItem3.Position = 2;
            this.toolStripTabItem3.Size = new System.Drawing.Size(54, 19);
            this.toolStripTabItem3.Tag = "3";
            this.toolStripTabItem3.Text = "FOLDER";
            // 
            // toolStripEx11
            // 
            this.toolStripEx11.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx11.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx11.Image = null;
            this.toolStripEx11.ImageScalingSize = new System.Drawing.Size(35, 35);
            this.toolStripEx11.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton25,
            this.toolStripButton9});
            this.toolStripEx11.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx11.Name = "toolStripEx11";
            this.toolStripEx11.Office12Mode = false;
            this.toolStripEx11.Size = new System.Drawing.Size(124, 84);
            this.toolStripEx11.TabIndex = 0;
            this.toolStripEx11.Text = "New";
            // 
            // toolStripButton25
            // 
            this.toolStripButton25.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton25.Image = global::OutlookDemo_2010.Properties.Resources.New_folder;
            this.toolStripButton25.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton25.Name = "toolStripButton25";
            this.toolStripButton25.Size = new System.Drawing.Size(44, 67);
            this.toolStripButton25.Text = "New\r\nFolder";
            this.toolStripButton25.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton9.Image = global::OutlookDemo_2010.Properties.Resources.New_search_folder;
            this.toolStripButton9.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(73, 67);
            this.toolStripButton9.Text = "New Search\r\nFolder";
            this.toolStripButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx12
            // 
            this.toolStripEx12.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx12.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx12.Image = null;
            this.toolStripEx12.ImageScalingSize = new System.Drawing.Size(35, 35);
            this.toolStripEx12.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTabItem5,
            this.toolStripPanelItem6});
            this.toolStripEx12.Location = new System.Drawing.Point(126, 1);
            this.toolStripEx12.Name = "toolStripEx12";
            this.toolStripEx12.Office12Mode = false;
            this.toolStripEx12.Size = new System.Drawing.Size(163, 84);
            this.toolStripEx12.TabIndex = 1;
            this.toolStripEx12.Text = "Action";
            // 
            // toolStripTabItem5
            // 
            this.toolStripTabItem5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripTabItem5.ForeColor = System.Drawing.Color.Black;
            this.toolStripTabItem5.Image = global::OutlookDemo_2010.Properties.Resources.Rename_folder;
            this.toolStripTabItem5.Name = "toolStripTabItem5";
            // 
            // 
            // 
            this.toolStripTabItem5.Panel.Name = "";
            this.toolStripTabItem5.Panel.ScrollPosition = 0;
            this.toolStripTabItem5.Panel.TabIndex = 0;
            this.toolStripTabItem5.Panel.Text = "Rename\r\nFolder";
            this.toolStripTabItem5.Position = -1;
            this.toolStripTabItem5.Size = new System.Drawing.Size(54, 67);
            this.toolStripTabItem5.Tag = "2";
            this.toolStripTabItem5.Text = "Rename\r\nFolder";
            this.toolStripTabItem5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripPanelItem6
            // 
            this.toolStripPanelItem6.CausesValidation = false;
            this.toolStripPanelItem6.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripButton13});
            this.toolStripPanelItem6.Name = "toolStripPanelItem6";
            this.toolStripPanelItem6.Size = new System.Drawing.Size(100, 70);
            this.toolStripPanelItem6.Text = "toolStripPanelItem1";
            this.toolStripPanelItem6.Transparent = true;
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.Image = global::OutlookDemo_2010.Properties.Resources.Rules;
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(91, 20);
            this.toolStripButton10.Text = "Copy Folder";
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.Image = global::OutlookDemo_2010.Properties.Resources.move;
            this.toolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Size = new System.Drawing.Size(93, 20);
            this.toolStripButton11.Text = "Move Folder";
            // 
            // toolStripButton13
            // 
            this.toolStripButton13.Image = global::OutlookDemo_2010.Properties.Resources.Delete1;
            this.toolStripButton13.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton13.Name = "toolStripButton13";
            this.toolStripButton13.Size = new System.Drawing.Size(96, 20);
            this.toolStripButton13.Text = "Delete Folder";
            // 
            // toolStripEx13
            // 
            this.toolStripEx13.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx13.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx13.Image = null;
            this.toolStripEx13.ImageScalingSize = new System.Drawing.Size(35, 35);
            this.toolStripEx13.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton28,
            this.toolStripButton27,
            this.toolStripButton26,
            this.toolStripPanelItem7});
            this.toolStripEx13.Location = new System.Drawing.Point(291, 1);
            this.toolStripEx13.Name = "toolStripEx13";
            this.toolStripEx13.Office12Mode = false;
            this.toolStripEx13.Size = new System.Drawing.Size(364, 84);
            this.toolStripEx13.TabIndex = 2;
            this.toolStripEx13.Text = "Clean Up";
            // 
            // toolStripButton28
            // 
            this.toolStripButton28.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton28.Image = global::OutlookDemo_2010.Properties.Resources.Mark_all_as_read;
            this.toolStripButton28.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton28.Name = "toolStripButton28";
            this.toolStripButton28.Size = new System.Drawing.Size(55, 67);
            this.toolStripButton28.Text = "Mark All\r\nAs Read";
            this.toolStripButton28.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton27
            // 
            this.toolStripButton27.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton27.Image = global::OutlookDemo_2010.Properties.Resources.Run_rules_now2;
            this.toolStripButton27.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton27.Name = "toolStripButton27";
            this.toolStripButton27.Size = new System.Drawing.Size(63, 67);
            this.toolStripButton27.Text = "Run Rules\r\nNow";
            this.toolStripButton27.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton26
            // 
            this.toolStripButton26.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton26.Image = global::OutlookDemo_2010.Properties.Resources.Show_all_A_to_Z;
            this.toolStripButton26.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton26.Name = "toolStripButton26";
            this.toolStripButton26.Size = new System.Drawing.Size(74, 67);
            this.toolStripButton26.Text = "Show All\r\nFrom A to Z";
            this.toolStripButton26.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripPanelItem7
            // 
            this.toolStripPanelItem7.CausesValidation = false;
            this.toolStripPanelItem7.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton14,
            this.toolStripButton15,
            this.toolStripSplitButton7});
            this.toolStripPanelItem7.Name = "toolStripPanelItem7";
            this.toolStripPanelItem7.Size = new System.Drawing.Size(163, 70);
            this.toolStripPanelItem7.Text = "toolStripPanelItem1";
            this.toolStripPanelItem7.Transparent = true;
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.Image = global::OutlookDemo_2010.Properties.Resources.clean_up2;
            this.toolStripButton14.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton14.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton14.Name = "toolStripButton14";
            this.toolStripButton14.Size = new System.Drawing.Size(118, 27);
            this.toolStripButton14.Text = "Clean Up Folder";
            // 
            // toolStripButton15
            // 
            this.toolStripButton15.Image = global::OutlookDemo_2010.Properties.Resources.Delete1;
            this.toolStripButton15.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton15.Name = "toolStripButton15";
            this.toolStripButton15.Size = new System.Drawing.Size(77, 20);
            this.toolStripButton15.Text = "Delete All";
            // 
            // toolStripSplitButton7
            // 
            this.toolStripSplitButton7.Image = global::OutlookDemo_2010.Properties.Resources.To_manger;
            this.toolStripSplitButton7.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton7.Name = "toolStripSplitButton7";
            this.toolStripSplitButton7.Size = new System.Drawing.Size(159, 28);
            this.toolStripSplitButton7.Text = "Recover Deleted Item";
            // 
            // toolStripEx14
            // 
            this.toolStripEx14.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx14.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx14.Image = null;
            this.toolStripEx14.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton12});
            this.toolStripEx14.Location = new System.Drawing.Point(657, 1);
            this.toolStripEx14.Name = "toolStripEx14";
            this.toolStripEx14.Office12Mode = false;
            this.toolStripEx14.Size = new System.Drawing.Size(72, 84);
            this.toolStripEx14.TabIndex = 3;
            this.toolStripEx14.Text = "Favorites";
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton12.Image = global::OutlookDemo_2010.Properties.Resources.show_in_favorites;
            this.toolStripButton12.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton12.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.Size = new System.Drawing.Size(65, 67);
            this.toolStripButton12.Text = "Show In\r\nFavourites";
            this.toolStripButton12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx15
            // 
            this.toolStripEx15.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx15.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx15.Image = null;
            this.toolStripEx15.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton16});
            this.toolStripEx15.Location = new System.Drawing.Point(731, 1);
            this.toolStripEx15.Name = "toolStripEx15";
            this.toolStripEx15.Office12Mode = false;
            this.toolStripEx15.Size = new System.Drawing.Size(62, 84);
            this.toolStripEx15.TabIndex = 4;
            this.toolStripEx15.Text = "Online View";
            // 
            // toolStripButton16
            // 
            this.toolStripButton16.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton16.Image = global::OutlookDemo_2010.Properties.Resources.View_in_server;
            this.toolStripButton16.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton16.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton16.Name = "toolStripButton16";
            this.toolStripButton16.Size = new System.Drawing.Size(55, 67);
            this.toolStripButton16.Text = "View On\r\nServer";
            this.toolStripButton16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx16
            // 
            this.toolStripEx16.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx16.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx16.Image = null;
            this.toolStripEx16.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton19,
            this.toolStripButton18,
            this.toolStripButton17});
            this.toolStripEx16.Location = new System.Drawing.Point(795, 1);
            this.toolStripEx16.Name = "toolStripEx16";
            this.toolStripEx16.Office12Mode = false;
            this.toolStripEx16.Size = new System.Drawing.Size(248, 84);
            this.toolStripEx16.TabIndex = 5;
            this.toolStripEx16.Text = "Properties";
            // 
            // toolStripButton19
            // 
            this.toolStripButton19.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton19.Image = global::OutlookDemo_2010.Properties.Resources.Auto_archive_Setting;
            this.toolStripButton19.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton19.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton19.Name = "toolStripButton19";
            this.toolStripButton19.Size = new System.Drawing.Size(86, 67);
            this.toolStripButton19.Text = "Auto Archeive\r\nSettings";
            this.toolStripButton19.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton18
            // 
            this.toolStripButton18.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton18.Image = global::OutlookDemo_2010.Properties.Resources.Folder_permission;
            this.toolStripButton18.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton18.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton18.Name = "toolStripButton18";
            this.toolStripButton18.Size = new System.Drawing.Size(74, 67);
            this.toolStripButton18.Text = "Folder\r\nPermissions";
            this.toolStripButton18.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton17
            // 
            this.toolStripButton17.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton17.Image = global::OutlookDemo_2010.Properties.Resources.Folder_properties;
            this.toolStripButton17.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton17.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton17.Name = "toolStripButton17";
            this.toolStripButton17.Size = new System.Drawing.Size(64, 67);
            this.toolStripButton17.Text = "Folder\r\nProperties";
            this.toolStripButton17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripTabItem9
            // 
            this.toolStripTabItem9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripTabItem9.ForeColor = System.Drawing.Color.Black;
            this.toolStripTabItem9.Name = "toolStripTabItem9";
            // 
            // ribbonControlAdv1.ribbonPanel4
            // 
            this.toolStripTabItem9.Panel.Controls.Add(this.toolStripEx17);
            this.toolStripTabItem9.Panel.Controls.Add(this.toolStripEx18);
            this.toolStripTabItem9.Panel.Controls.Add(this.toolStripEx19);
            this.toolStripTabItem9.Panel.Controls.Add(this.toolStripEx20);
            this.toolStripTabItem9.Panel.Controls.Add(this.toolStripEx21);
            this.toolStripTabItem9.Panel.Name = "ribbonPanel4";
            this.toolStripTabItem9.Panel.ScrollPosition = 0;
            this.toolStripTabItem9.Panel.TabIndex = 5;
            this.toolStripTabItem9.Panel.Text = "VIEW";
            this.toolStripTabItem9.Position = 3;
            this.toolStripTabItem9.Size = new System.Drawing.Size(40, 19);
            this.toolStripTabItem9.Tag = "9";
            this.toolStripTabItem9.Text = "VIEW";
            // 
            // toolStripEx17
            // 
            this.toolStripEx17.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx17.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx17.Image = null;
            this.toolStripEx17.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton12,
            this.toolStripButton21,
            this.toolStripButton20});
            this.toolStripEx17.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx17.Name = "toolStripEx17";
            this.toolStripEx17.Office12Mode = false;
            this.toolStripEx17.Size = new System.Drawing.Size(170, 84);
            this.toolStripEx17.TabIndex = 0;
            this.toolStripEx17.Text = "Current View";
            // 
            // toolStripSplitButton12
            // 
            this.toolStripSplitButton12.ForeColor = System.Drawing.Color.Black;
            this.toolStripSplitButton12.Image = global::OutlookDemo_2010.Properties.Resources.Change_View;
            this.toolStripSplitButton12.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton12.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton12.Name = "toolStripSplitButton12";
            this.toolStripSplitButton12.Size = new System.Drawing.Size(64, 67);
            this.toolStripSplitButton12.Text = "Change\r\nView";
            this.toolStripSplitButton12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton21
            // 
            this.toolStripButton21.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton21.Image = global::OutlookDemo_2010.Properties.Resources.View_Setting;
            this.toolStripButton21.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton21.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton21.Name = "toolStripButton21";
            this.toolStripButton21.Size = new System.Drawing.Size(53, 67);
            this.toolStripButton21.Text = "View\r\nSettings";
            this.toolStripButton21.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton20
            // 
            this.toolStripButton20.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton20.Image = global::OutlookDemo_2010.Properties.Resources.Reset_View;
            this.toolStripButton20.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton20.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton20.Name = "toolStripButton20";
            this.toolStripButton20.Size = new System.Drawing.Size(46, 67);
            this.toolStripButton20.Text = "Reset\r\nView";
            this.toolStripButton20.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx18
            // 
            this.toolStripEx18.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx18.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx18.Image = null;
            this.toolStripEx18.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPanelItem8});
            this.toolStripEx18.Location = new System.Drawing.Point(172, 1);
            this.toolStripEx18.Name = "toolStripEx18";
            this.toolStripEx18.Office12Mode = false;
            this.toolStripEx18.Size = new System.Drawing.Size(167, 84);
            this.toolStripEx18.TabIndex = 1;
            this.toolStripEx18.Text = "Messages";
            // 
            // toolStripPanelItem8
            // 
            this.toolStripPanelItem8.CausesValidation = false;
            this.toolStripPanelItem8.ForeColor = System.Drawing.Color.Black;
            this.toolStripPanelItem8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripCheckBox1,
            this.toolStripSplitButton8});
            this.toolStripPanelItem8.Name = "toolStripPanelItem8";
            this.toolStripPanelItem8.Size = new System.Drawing.Size(158, 70);
            this.toolStripPanelItem8.Text = "toolStripPanelItem8";
            this.toolStripPanelItem8.Transparent = true;
            // 
            // toolStripCheckBox1
            // 
            this.toolStripCheckBox1.Name = "toolStripCheckBox1";
            this.toolStripCheckBox1.Size = new System.Drawing.Size(144, 19);
            this.toolStripCheckBox1.Text = "Show As Conversation";
            // 
            // toolStripSplitButton8
            // 
            this.toolStripSplitButton8.Enabled = false;
            this.toolStripSplitButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton8.Image")));
            this.toolStripSplitButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton8.Name = "toolStripSplitButton8";
            this.toolStripSplitButton8.Size = new System.Drawing.Size(154, 20);
            this.toolStripSplitButton8.Text = "Conversation Settings";
            // 
            // toolStripEx19
            // 
            this.toolStripEx19.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx19.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx19.Image = null;
            this.toolStripEx19.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton11,
            this.toolStripSplitButton10,
            this.toolStripSplitButton9});
            this.toolStripEx19.Location = new System.Drawing.Point(341, 1);
            this.toolStripEx19.Name = "toolStripEx19";
            this.toolStripEx19.Office12Mode = false;
            this.toolStripEx19.Size = new System.Drawing.Size(207, 84);
            this.toolStripEx19.TabIndex = 2;
            this.toolStripEx19.Text = "Layout";
            // 
            // toolStripSplitButton11
            // 
            this.toolStripSplitButton11.ForeColor = System.Drawing.Color.Black;
            this.toolStripSplitButton11.Image = global::OutlookDemo_2010.Properties.Resources.Folder_Pane;
            this.toolStripSplitButton11.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton11.Name = "toolStripSplitButton11";
            this.toolStripSplitButton11.Size = new System.Drawing.Size(64, 67);
            this.toolStripSplitButton11.Text = "Folder \r\npane";
            this.toolStripSplitButton11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSplitButton10
            // 
            this.toolStripSplitButton10.ForeColor = System.Drawing.Color.Black;
            this.toolStripSplitButton10.Image = global::OutlookDemo_2010.Properties.Resources.Reading_Pane;
            this.toolStripSplitButton10.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton10.Name = "toolStripSplitButton10";
            this.toolStripSplitButton10.Size = new System.Drawing.Size(68, 67);
            this.toolStripSplitButton10.Text = "Reading\r\nPane";
            this.toolStripSplitButton10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSplitButton9
            // 
            this.toolStripSplitButton9.ForeColor = System.Drawing.Color.Black;
            this.toolStripSplitButton9.Image = global::OutlookDemo_2010.Properties.Resources.To_Do_Bar;
            this.toolStripSplitButton9.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton9.Name = "toolStripSplitButton9";
            this.toolStripSplitButton9.Size = new System.Drawing.Size(68, 67);
            this.toolStripSplitButton9.Text = "To-Do\r\nBar";
            this.toolStripSplitButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx20
            // 
            this.toolStripEx20.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx20.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx20.Image = null;
            this.toolStripEx20.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton13});
            this.toolStripEx20.Location = new System.Drawing.Point(550, 1);
            this.toolStripEx20.Name = "toolStripEx20";
            this.toolStripEx20.Office12Mode = false;
            this.toolStripEx20.Size = new System.Drawing.Size(69, 84);
            this.toolStripEx20.TabIndex = 3;
            this.toolStripEx20.Text = "People Pane";
            // 
            // toolStripSplitButton13
            // 
            this.toolStripSplitButton13.ForeColor = System.Drawing.Color.Black;
            this.toolStripSplitButton13.Image = global::OutlookDemo_2010.Properties.Resources.Message_Priview;
            this.toolStripSplitButton13.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton13.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton13.Name = "toolStripSplitButton13";
            this.toolStripSplitButton13.Size = new System.Drawing.Size(62, 67);
            this.toolStripSplitButton13.Text = "People \r\nPane";
            this.toolStripSplitButton13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx21
            // 
            this.toolStripEx21.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.toolStripEx21.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx21.Image = null;
            this.toolStripEx21.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton22,
            this.toolStripButton24,
            this.toolStripButton23});
            this.toolStripEx21.Location = new System.Drawing.Point(621, 1);
            this.toolStripEx21.Name = "toolStripEx21";
            this.toolStripEx21.Office12Mode = false;
            this.toolStripEx21.Size = new System.Drawing.Size(214, 84);
            this.toolStripEx21.TabIndex = 4;
            this.toolStripEx21.Text = "Window";
            // 
            // toolStripButton22
            // 
            this.toolStripButton22.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton22.Image = global::OutlookDemo_2010.Properties.Resources.Reminder_Windows;
            this.toolStripButton22.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton22.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton22.Name = "toolStripButton22";
            this.toolStripButton22.Size = new System.Drawing.Size(68, 67);
            this.toolStripButton22.Text = "Remainder\r\nWindow";
            this.toolStripButton22.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton24
            // 
            this.toolStripButton24.AutoSize = false;
            this.toolStripButton24.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton24.Image = global::OutlookDemo_2010.Properties.Resources.Open_in_New_Window;
            this.toolStripButton24.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolStripButton24.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton24.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton24.Name = "toolStripButton24";
            this.toolStripButton24.Size = new System.Drawing.Size(82, 75);
            this.toolStripButton24.Text = "Open In\r\nNew Window";
            this.toolStripButton24.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripButton24.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton23
            // 
            this.toolStripButton23.AutoSize = false;
            this.toolStripButton23.ForeColor = System.Drawing.Color.Black;
            this.toolStripButton23.Image = global::OutlookDemo_2010.Properties.Resources.Close_All_Item;
            this.toolStripButton23.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton23.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton23.Name = "toolStripButton23";
            this.toolStripButton23.Size = new System.Drawing.Size(57, 75);
            this.toolStripButton23.Text = "Close All\r\nItems";
            this.toolStripButton23.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // splitContainerAdv1
            // 
            this.splitContainerAdv1.BackColor = System.Drawing.Color.White;
            this.splitContainerAdv1.BeforeTouchSize = 7;
            this.splitContainerAdv1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerAdv1.Location = new System.Drawing.Point(1, 153);
            this.splitContainerAdv1.Name = "splitContainerAdv1";
            // 
            // splitContainerAdv1.Panel1
            // 
            this.splitContainerAdv1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainerAdv1.Panel2
            // 
            this.splitContainerAdv1.Panel2.Controls.Add(this.InnerSplitterContainer);
            this.splitContainerAdv1.Size = new System.Drawing.Size(1036, 463);
            this.splitContainerAdv1.SplitterDistance = 205;
            this.splitContainerAdv1.TabIndex = 3;
            this.splitContainerAdv1.Text = "splitContainerAdv1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.treeViewAdv2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(205, 463);
            this.panel1.TabIndex = 2;
            // 
            // treeViewAdv2
            // 
            this.treeViewAdv2.BackColor = System.Drawing.Color.White;
            this.treeViewAdv2.BeforeTouchSize = new System.Drawing.Size(205, 389);
            this.treeViewAdv2.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.treeViewAdv2.BorderColor = System.Drawing.Color.Transparent;
            this.treeViewAdv2.BorderSides = System.Windows.Forms.Border3DSide.Middle;
            this.treeViewAdv2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeViewAdv2.CanSelectDisabledNode = false;
            this.treeViewAdv2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewAdv2.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.treeViewAdv2.GutterSpace = 10;
            // 
            // 
            // 
            this.treeViewAdv2.HelpTextControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeViewAdv2.HelpTextControl.Location = new System.Drawing.Point(0, 0);
            this.treeViewAdv2.HelpTextControl.Name = "helpText";
            this.treeViewAdv2.HelpTextControl.Size = new System.Drawing.Size(49, 15);
            this.treeViewAdv2.HelpTextControl.TabIndex = 0;
            this.treeViewAdv2.HelpTextControl.Text = "help text";
            this.treeViewAdv2.ItemHeight = 24;
            this.treeViewAdv2.Location = new System.Drawing.Point(0, 21);
            this.treeViewAdv2.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(165)))), ((int)(((byte)(220)))));
            this.treeViewAdv2.Name = "treeViewAdv2";
            treeNodeAdv1.ChildStyle.EnsureDefaultOptionedChild = true;
            treeNodeAdv1.EnsureDefaultOptionedChild = true;
            treeNodeAdv1.Expanded = true;
            treeNodeAdv1.Height = 30;
            treeNodeAdv1.MultiLine = true;
            treeNodeAdv2.ChildStyle.EnsureDefaultOptionedChild = true;
            treeNodeAdv2.EnsureDefaultOptionedChild = true;
            treeNodeAdv2.MultiLine = true;
            treeNodeAdv2.Optioned = true;
            treeNodeAdv2.PlusMinusSize = new System.Drawing.Size(9, 9);
            treeNodeAdv2.ShowLine = true;
            treeNodeAdv2.Text = "Inbox";
            treeNodeAdv3.ChildStyle.EnsureDefaultOptionedChild = true;
            treeNodeAdv3.EnsureDefaultOptionedChild = true;
            treeNodeAdv3.MultiLine = true;
            treeNodeAdv3.PlusMinusSize = new System.Drawing.Size(9, 9);
            treeNodeAdv3.ShowLine = true;
            treeNodeAdv3.Text = "Sent Items";
            treeNodeAdv4.ChildStyle.EnsureDefaultOptionedChild = true;
            treeNodeAdv4.EnsureDefaultOptionedChild = true;
            treeNodeAdv4.MultiLine = true;
            treeNodeAdv4.PlusMinusSize = new System.Drawing.Size(9, 9);
            treeNodeAdv4.ShowLine = true;
            treeNodeAdv4.Text = "Drafts";
            treeNodeAdv5.ChildStyle.EnsureDefaultOptionedChild = true;
            treeNodeAdv5.EnsureDefaultOptionedChild = true;
            treeNodeAdv5.MultiLine = true;
            treeNodeAdv5.PlusMinusSize = new System.Drawing.Size(9, 9);
            treeNodeAdv5.ShowLine = true;
            treeNodeAdv5.Text = "Deleted Items";
            treeNodeAdv6.ChildStyle.EnsureDefaultOptionedChild = true;
            treeNodeAdv6.EnsureDefaultOptionedChild = true;
            treeNodeAdv6.MultiLine = true;
            treeNodeAdv6.PlusMinusSize = new System.Drawing.Size(9, 9);
            treeNodeAdv6.ShowLine = true;
            treeNodeAdv6.Text = "Junk E-Mail";
            treeNodeAdv7.ChildStyle.EnsureDefaultOptionedChild = true;
            treeNodeAdv7.EnsureDefaultOptionedChild = true;
            treeNodeAdv7.MultiLine = true;
            treeNodeAdv7.PlusMinusSize = new System.Drawing.Size(9, 9);
            treeNodeAdv7.ShowLine = true;
            treeNodeAdv7.Text = "Outbox";
            treeNodeAdv1.Nodes.AddRange(new Syncfusion.Windows.Forms.Tools.TreeNodeAdv[] {
            treeNodeAdv2,
            treeNodeAdv3,
            treeNodeAdv4,
            treeNodeAdv5,
            treeNodeAdv6,
            treeNodeAdv7});
            treeNodeAdv1.PlusMinusSize = new System.Drawing.Size(9, 9);
            treeNodeAdv1.ShowLine = true;
            treeNodeAdv1.Text = "customer@support.com";
            this.treeViewAdv2.Nodes.AddRange(new Syncfusion.Windows.Forms.Tools.TreeNodeAdv[] {
            treeNodeAdv1});
            this.treeViewAdv2.SelectedNodeBackground = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(165)))), ((int)(((byte)(220))))));
            this.treeViewAdv2.ShowFocusRect = false;
            this.treeViewAdv2.ShowLines = false;
            this.treeViewAdv2.Size = new System.Drawing.Size(205, 389);
            this.treeViewAdv2.Style = Syncfusion.Windows.Forms.Tools.TreeStyle.Metro;
            this.treeViewAdv2.TabIndex = 7;
            this.treeViewAdv2.Text = "treeViewAdv2";
            // 
            // 
            // 
            this.treeViewAdv2.ToolTipControl.BackColor = System.Drawing.SystemColors.Info;
            this.treeViewAdv2.ToolTipControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeViewAdv2.ToolTipControl.Location = new System.Drawing.Point(0, 0);
            this.treeViewAdv2.ToolTipControl.Name = "toolTip";
            this.treeViewAdv2.ToolTipControl.Size = new System.Drawing.Size(41, 15);
            this.treeViewAdv2.ToolTipControl.TabIndex = 1;
            this.treeViewAdv2.ToolTipControl.Text = "toolTip";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pointerControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(205, 21);
            this.panel2.TabIndex = 6;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(228)))), ((int)(((byte)(228)))));
            this.panel9.Controls.Add(this.pointerControl7);
            this.panel9.Controls.Add(this.pointerControl6);
            this.panel9.Controls.Add(this.pointerControl5);
            this.panel9.Controls.Add(this.pointerControl4);
            this.panel9.Controls.Add(this.pointerControl3);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 410);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(205, 53);
            this.panel9.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(205, 463);
            this.panel3.TabIndex = 0;
            // 
            // InnerSplitterContainer
            // 
            this.InnerSplitterContainer.BackColor = System.Drawing.Color.White;
            this.InnerSplitterContainer.BeforeTouchSize = 7;
            this.InnerSplitterContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InnerSplitterContainer.Location = new System.Drawing.Point(0, 0);
            this.InnerSplitterContainer.Name = "InnerSplitterContainer";
            // 
            // InnerSplitterContainer.Panel1
            // 
            this.InnerSplitterContainer.Panel1.Controls.Add(this.panel12);
            this.InnerSplitterContainer.Panel1.Controls.Add(this.panel10);
            this.InnerSplitterContainer.Panel1.Controls.Add(this.panel8);
            // 
            // InnerSplitterContainer.Panel2
            // 
            this.InnerSplitterContainer.Panel2.Controls.Add(this.panel7);
            this.InnerSplitterContainer.Panel2.Controls.Add(this.panel5);
            this.InnerSplitterContainer.Size = new System.Drawing.Size(824, 463);
            this.InnerSplitterContainer.SplitterDistance = 308;
            this.InnerSplitterContainer.TabIndex = 0;
            this.InnerSplitterContainer.Text = "splitContainerAdv2";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(212)))), ((int)(((byte)(212)))));
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(307, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1, 463);
            this.panel12.TabIndex = 4;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(212)))), ((int)(((byte)(212)))));
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1, 463);
            this.panel10.TabIndex = 3;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel11);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(308, 463);
            this.panel8.TabIndex = 2;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.DarkGray;
            this.panel11.Controls.Add(this.panel14);
            this.panel11.Controls.Add(this.panel13);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(308, 463);
            this.panel11.TabIndex = 1;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.gridGroupingControl1);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(0, 66);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(308, 397);
            this.panel14.TabIndex = 4;
            // 
            // gridGroupingControl1
            // 
            this.gridGroupingControl1.BackColor = System.Drawing.SystemColors.Window;
            this.gridGroupingControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridGroupingControl1.FreezeCaption = false;
            this.gridGroupingControl1.GridOfficeScrollBars = Syncfusion.Windows.Forms.OfficeScrollBars.Metro;
            this.gridGroupingControl1.GridVisualStyles = Syncfusion.Windows.Forms.GridVisualStyles.Metro;
            this.gridGroupingControl1.Location = new System.Drawing.Point(0, 0);
            this.gridGroupingControl1.Name = "gridGroupingControl1";
            this.gridGroupingControl1.Size = new System.Drawing.Size(308, 397);
            this.gridGroupingControl1.TabIndex = 4;
            this.gridGroupingControl1.TableDescriptor.AllowNew = false;
            this.gridGroupingControl1.TableDescriptor.TableOptions.CaptionRowHeight = 29;
            this.gridGroupingControl1.TableDescriptor.TableOptions.ColumnHeaderRowHeight = 25;
            this.gridGroupingControl1.TableDescriptor.TableOptions.RecordRowHeight = 25;
            this.gridGroupingControl1.Text = "gridGroupingControl1";
            this.gridGroupingControl1.TopLevelGroupOptions.ShowColumnHeaders = false;
            this.gridGroupingControl1.VersionInfo = "12.1400.0.43";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.panel15);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(308, 66);
            this.panel13.TabIndex = 3;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.outlookSearchBox1);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(308, 66);
            this.panel15.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.MessageRichTextBox);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 101);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(509, 362);
            this.panel7.TabIndex = 2;
            // 
            // MessageRichTextBox
            // 
            this.MessageRichTextBox.BackColor = System.Drawing.Color.White;
            this.MessageRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MessageRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessageRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.MessageRichTextBox.Name = "MessageRichTextBox";
            this.MessageRichTextBox.ReadOnly = true;
            this.MessageRichTextBox.Size = new System.Drawing.Size(509, 362);
            this.MessageRichTextBox.TabIndex = 0;
            this.MessageRichTextBox.Text = "";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.Tolabel);
            this.panel5.Controls.Add(this.Maillabel);
            this.panel5.Controls.Add(this.DateTimeLabel);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(509, 101);
            this.panel5.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label2.Location = new System.Drawing.Point(144, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "To:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label1.Location = new System.Drawing.Point(144, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "From: ";
            // 
            // Tolabel
            // 
            this.Tolabel.AutoSize = true;
            this.Tolabel.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.Tolabel.Location = new System.Drawing.Point(201, 62);
            this.Tolabel.Name = "Tolabel";
            this.Tolabel.Size = new System.Drawing.Size(76, 13);
            this.Tolabel.TabIndex = 4;
            this.Tolabel.Text = "JacksonSmith";
            // 
            // Maillabel
            // 
            this.Maillabel.AutoSize = true;
            this.Maillabel.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.Maillabel.Location = new System.Drawing.Point(201, 37);
            this.Maillabel.Name = "Maillabel";
            this.Maillabel.Size = new System.Drawing.Size(37, 13);
            this.Maillabel.TabIndex = 3;
            this.Maillabel.Text = "Karter";
            // 
            // DateTimeLabel
            // 
            this.DateTimeLabel.AutoSize = true;
            this.DateTimeLabel.Location = new System.Drawing.Point(144, 10);
            this.DateTimeLabel.Name = "DateTimeLabel";
            this.DateTimeLabel.Size = new System.Drawing.Size(115, 13);
            this.DateTimeLabel.TabIndex = 2;
            this.DateTimeLabel.Text = "Friday 5/6/14 2.10 PM";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.userImage);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(108, 101);
            this.panel6.TabIndex = 1;
            // 
            // userImage
            // 
            this.userImage.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.dp;
            this.userImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userImage.Location = new System.Drawing.Point(13, 13);
            this.userImage.Name = "userImage";
            this.userImage.Size = new System.Drawing.Size(81, 75);
            this.userImage.TabIndex = 0;
            this.userImage.TabStop = false;
            // 
            // statusStripEx1
            // 
            this.statusStripEx1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.statusStripEx1.BeforeTouchSize = new System.Drawing.Size(1036, 23);
            this.statusStripEx1.Dock = Syncfusion.Windows.Forms.Tools.DockStyleEx.Bottom;
            this.statusStripEx1.Font = new System.Drawing.Font("Segoe UI", 9.55F);
            this.statusStripEx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusStripLabel1,
            this.statusStripLabel2,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel1,
            this.trackBarItem1});
            this.statusStripEx1.Location = new System.Drawing.Point(1, 616);
            this.statusStripEx1.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.statusStripEx1.Name = "statusStripEx1";
            this.statusStripEx1.Size = new System.Drawing.Size(1036, 23);
            this.statusStripEx1.TabIndex = 3;
            this.statusStripEx1.Text = "statusStripEx1";
            // 
            // statusStripLabel1
            // 
            this.statusStripLabel1.ForeColor = System.Drawing.Color.White;
            this.statusStripLabel1.Margin = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.statusStripLabel1.Name = "statusStripLabel1";
            this.statusStripLabel1.Size = new System.Drawing.Size(62, 17);
            this.statusStripLabel1.Text = "ITEMS 15";
            // 
            // statusStripLabel2
            // 
            this.statusStripLabel2.ForeColor = System.Drawing.Color.White;
            this.statusStripLabel2.Margin = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.statusStripLabel2.Name = "statusStripLabel2";
            this.statusStripLabel2.Size = new System.Drawing.Size(70, 17);
            this.statusStripLabel2.Text = "UNREAD 2";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(188, 17);
            this.toolStripStatusLabel2.Text = "ALL FOLDERS ARE UP TO DATE";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(250, 17);
            this.toolStripStatusLabel1.Text = "CONNECTED TO MICROSOFT EXCHANGE";
            // 
            // trackBarItem1
            // 
            this.trackBarItem1.Maximum = 10;
            this.trackBarItem1.Name = "trackBarItem1";
            this.trackBarItem1.Size = new System.Drawing.Size(250, 20);
            this.trackBarItem1.Text = "100%";
            this.trackBarItem1.Value = 5;
            // 
            // HiddenPanel
            // 
            this.HiddenPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.HiddenPanel.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.All_folder_01;
            this.HiddenPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HiddenPanel.Controls.Add(this.panel4);
            this.HiddenPanel.Controls.Add(this.pointerControl2);
            this.HiddenPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.HiddenPanel.Location = new System.Drawing.Point(1, 153);
            this.HiddenPanel.Name = "HiddenPanel";
            this.HiddenPanel.Size = new System.Drawing.Size(45, 463);
            this.HiddenPanel.TabIndex = 4;
            this.HiddenPanel.Visible = false;
            this.HiddenPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.HiddenPanel_MouseDown);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel4.Location = new System.Drawing.Point(46, 420);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 79);
            this.panel4.TabIndex = 3;
            // 
            // splashControl1
            // 
            this.splashControl1.HideHostForm = true;
            this.splashControl1.HostForm = this;
            this.splashControl1.SplashImage = global::OutlookDemo_2010.Properties.Resources.Splash;
            this.splashControl1.TimerInterval = 2000;
            // 
            // pointerControl2
            // 
            this.pointerControl2.BackColor = System.Drawing.Color.Transparent;
            this.pointerControl2.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.icon_arrow_ryt;
            this.pointerControl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pointerControl2.Location = new System.Drawing.Point(12, 4);
            this.pointerControl2.Margin = new System.Windows.Forms.Padding(4);
            this.pointerControl2.Name = "pointerControl2";
            this.pointerControl2.Size = new System.Drawing.Size(15, 21);
            this.pointerControl2.TabIndex = 5;
            this.pointerControl2.Click += new System.EventHandler(this.pointerControl2_Click);
            // 
            // pointerControl1
            // 
            this.pointerControl1.BackColor = System.Drawing.Color.Transparent;
            this.pointerControl1.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.icon_arrow12;
            this.pointerControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pointerControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pointerControl1.Location = new System.Drawing.Point(188, 0);
            this.pointerControl1.Margin = new System.Windows.Forms.Padding(4);
            this.pointerControl1.Name = "pointerControl1";
            this.pointerControl1.Size = new System.Drawing.Size(17, 21);
            this.pointerControl1.TabIndex = 7;
            this.pointerControl1.Click += new System.EventHandler(this.pointerControl1_Click_2);
            // 
            // pointerControl7
            // 
            this.pointerControl7.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.icons_5;
            this.pointerControl7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pointerControl7.Location = new System.Drawing.Point(206, 16);
            this.pointerControl7.Margin = new System.Windows.Forms.Padding(4);
            this.pointerControl7.Name = "pointerControl7";
            this.pointerControl7.Size = new System.Drawing.Size(29, 23);
            this.pointerControl7.TabIndex = 4;
            // 
            // pointerControl6
            // 
            this.pointerControl6.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.icons_4;
            this.pointerControl6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pointerControl6.Location = new System.Drawing.Point(163, 17);
            this.pointerControl6.Margin = new System.Windows.Forms.Padding(4);
            this.pointerControl6.Name = "pointerControl6";
            this.pointerControl6.Size = new System.Drawing.Size(29, 23);
            this.pointerControl6.TabIndex = 3;
            // 
            // pointerControl5
            // 
            this.pointerControl5.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.icons_3;
            this.pointerControl5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pointerControl5.Location = new System.Drawing.Point(116, 16);
            this.pointerControl5.Margin = new System.Windows.Forms.Padding(4);
            this.pointerControl5.Name = "pointerControl5";
            this.pointerControl5.Size = new System.Drawing.Size(29, 23);
            this.pointerControl5.TabIndex = 2;
            // 
            // pointerControl4
            // 
            this.pointerControl4.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.icon2;
            this.pointerControl4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pointerControl4.Location = new System.Drawing.Point(69, 16);
            this.pointerControl4.Margin = new System.Windows.Forms.Padding(4);
            this.pointerControl4.Name = "pointerControl4";
            this.pointerControl4.Size = new System.Drawing.Size(29, 23);
            this.pointerControl4.TabIndex = 1;
            // 
            // pointerControl3
            // 
            this.pointerControl3.BackgroundImage = global::OutlookDemo_2010.Properties.Resources.icons;
            this.pointerControl3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pointerControl3.Location = new System.Drawing.Point(27, 16);
            this.pointerControl3.Margin = new System.Windows.Forms.Padding(4);
            this.pointerControl3.Name = "pointerControl3";
            this.pointerControl3.Size = new System.Drawing.Size(29, 23);
            this.pointerControl3.TabIndex = 0;
            // 
            // outlookSearchBox1
            // 
            this.outlookSearchBox1.BackColor = System.Drawing.Color.White;
            this.outlookSearchBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outlookSearchBox1.Label1Clicked = false;
            this.outlookSearchBox1.Label2Clicked = false;
            this.outlookSearchBox1.Location = new System.Drawing.Point(0, 0);
            this.outlookSearchBox1.Margin = new System.Windows.Forms.Padding(4);
            this.outlookSearchBox1.Name = "outlookSearchBox1";
            this.outlookSearchBox1.SearchString = "";
            this.outlookSearchBox1.Size = new System.Drawing.Size(308, 66);
            this.outlookSearchBox1.TabIndex = 2;
            // 
            // OutlookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Borders = new System.Windows.Forms.Padding(0);
            this.ClientSize = new System.Drawing.Size(1038, 639);
            this.ColorScheme = Syncfusion.Windows.Forms.Tools.RibbonForm.ColorSchemeType.Silver;
            this.Controls.Add(this.backStage1);
            this.Controls.Add(this.HiddenPanel);
            this.Controls.Add(this.splitContainerAdv1);
            this.Controls.Add(this.ribbonControlAdv1);
            this.Controls.Add(this.statusStripEx1);
            this.HelpButtonImage = ((System.Drawing.Image)(resources.GetObject("$this.HelpButtonImage")));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OutlookForm";
            this.Padding = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.Text = "Outlook 2013 Demo";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlAdv1)).EndInit();
            this.ribbonControlAdv1.ResumeLayout(false);
            this.ribbonControlAdv1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backStage1)).EndInit();
            this.backStage1.ResumeLayout(false);
            this.backStageTab2.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.toolStripTabItem1.Panel.ResumeLayout(false);
            this.toolStripTabItem1.Panel.PerformLayout();
            this.toolStripEx1.ResumeLayout(false);
            this.toolStripEx1.PerformLayout();
            this.toolStripEx2.ResumeLayout(false);
            this.toolStripEx2.PerformLayout();
            this.toolStripEx3.ResumeLayout(false);
            this.toolStripEx3.PerformLayout();
            this.toolStripEx4.ResumeLayout(false);
            this.toolStripEx4.PerformLayout();
            this.toolStripEx5.ResumeLayout(false);
            this.toolStripEx5.PerformLayout();
            this.toolStripEx6.ResumeLayout(false);
            this.toolStripEx6.PerformLayout();
            this.toolStripTabItem2.Panel.ResumeLayout(false);
            this.toolStripTabItem2.Panel.PerformLayout();
            this.toolStripEx7.ResumeLayout(false);
            this.toolStripEx7.PerformLayout();
            this.toolStripEx8.ResumeLayout(false);
            this.toolStripEx8.PerformLayout();
            this.toolStripEx9.ResumeLayout(false);
            this.toolStripEx9.PerformLayout();
            this.toolStripEx10.ResumeLayout(false);
            this.toolStripEx10.PerformLayout();
            this.toolStripTabItem3.Panel.ResumeLayout(false);
            this.toolStripTabItem3.Panel.PerformLayout();
            this.toolStripEx11.ResumeLayout(false);
            this.toolStripEx11.PerformLayout();
            this.toolStripEx12.ResumeLayout(false);
            this.toolStripEx12.PerformLayout();
            this.toolStripEx13.ResumeLayout(false);
            this.toolStripEx13.PerformLayout();
            this.toolStripEx14.ResumeLayout(false);
            this.toolStripEx14.PerformLayout();
            this.toolStripEx15.ResumeLayout(false);
            this.toolStripEx15.PerformLayout();
            this.toolStripEx16.ResumeLayout(false);
            this.toolStripEx16.PerformLayout();
            this.toolStripTabItem9.Panel.ResumeLayout(false);
            this.toolStripTabItem9.Panel.PerformLayout();
            this.toolStripEx17.ResumeLayout(false);
            this.toolStripEx17.PerformLayout();
            this.toolStripEx18.ResumeLayout(false);
            this.toolStripEx18.PerformLayout();
            this.toolStripEx19.ResumeLayout(false);
            this.toolStripEx19.PerformLayout();
            this.toolStripEx20.ResumeLayout(false);
            this.toolStripEx20.PerformLayout();
            this.toolStripEx21.ResumeLayout(false);
            this.toolStripEx21.PerformLayout();
            this.splitContainerAdv1.Panel1.ResumeLayout(false);
            this.splitContainerAdv1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAdv1)).EndInit();
            this.splitContainerAdv1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeViewAdv2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.InnerSplitterContainer.Panel1.ResumeLayout(false);
            this.InnerSplitterContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InnerSplitterContainer)).EndInit();
            this.InnerSplitterContainer.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridGroupingControl1)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userImage)).EndInit();
            this.statusStripEx1.ResumeLayout(false);
            this.statusStripEx1.PerformLayout();
            this.HiddenPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.RibbonControlAdv ribbonControlAdv1;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem toolStripTabItem1;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx1;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx2;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem1;
        private System.Windows.Forms.ToolStripButton IgnoreButton;
        private System.Windows.Forms.ToolStripSplitButton CleanUpSplitButton;
        private System.Windows.Forms.ToolStripSplitButton JunkSplitButton;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx3;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem2;
        private System.Windows.Forms.ToolStripButton MeetingButton;
        private System.Windows.Forms.ToolStripSplitButton MoreButton;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem toolStripTabItem3;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx11;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx12;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem toolStripTabItem5;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem6;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx4;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx5;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx6;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem3;
        private System.Windows.Forms.ToolStripButton AddressBookButton;
        private System.Windows.Forms.ToolStripSplitButton FilterEmailButton;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem toolStripTabItem2;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx7;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem4;
        private System.Windows.Forms.ToolStripButton UpdateFolderButton;
        private System.Windows.Forms.ToolStripButton SendAllButton;
        private System.Windows.Forms.ToolStripSplitButton SendReceiveGroupsSplit;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx8;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx9;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem5;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton2;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton5;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton6;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx10;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem toolStripTabItem9;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx17;
        private System.Windows.Forms.ToolStripButton toolStripButton21;
        private System.Windows.Forms.ToolStripButton toolStripButton20;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx13;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem7;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.ToolStripButton toolStripButton15;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton7;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx14;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx15;
        private System.Windows.Forms.ToolStripButton toolStripButton16;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx16;
        private System.Windows.Forms.ToolStripButton toolStripButton19;
        private System.Windows.Forms.ToolStripButton toolStripButton18;
        private System.Windows.Forms.ToolStripButton toolStripButton17;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton12;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx18;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem8;
        private Syncfusion.Windows.Forms.Tools.ToolStripCheckBox toolStripCheckBox1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton8;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx19;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton11;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton10;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton9;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx20;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton13;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx21;
        private System.Windows.Forms.ToolStripButton toolStripButton22;
        private System.Windows.Forms.ToolStripButton toolStripButton24;
        private System.Windows.Forms.ToolStripButton toolStripButton23;
        private Syncfusion.Windows.Forms.BackStageView backStageView1;
        private Syncfusion.Windows.Forms.BackStage backStage1;
        private Syncfusion.Windows.Forms.BackStageTab backStageTab1;
        private Syncfusion.Windows.Forms.BackStageTab backStageTab3;
        private Syncfusion.Windows.Forms.BackStageTab backStageTab4;
        private Syncfusion.Windows.Forms.BackStageTab backStageTab5;
        private Syncfusion.Windows.Forms.BackStageTab backStageTab6;
        private Syncfusion.Windows.Forms.BackStageTab backStageTab7;
        private Syncfusion.Windows.Forms.BackStageButton backStageButton1;
        private Syncfusion.Windows.Forms.Tools.SplitContainerAdv splitContainerAdv1;
        private System.Windows.Forms.Panel HiddenPanel;
        private Syncfusion.Windows.Forms.Tools.SplitContainerAdv InnerSplitterContainer;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label DateTimeLabel;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox userImage;
        private System.Windows.Forms.Label Tolabel;
        private System.Windows.Forms.Label Maillabel;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RichTextBox MessageRichTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private PointerControl pointerControl2;
        private System.Windows.Forms.Panel panel4;
        private Syncfusion.Windows.Forms.Tools.StatusStripEx statusStripEx1;
        private Syncfusion.Windows.Forms.Tools.StatusStripLabel statusStripLabel1;
        private Syncfusion.Windows.Forms.Tools.StatusStripLabel statusStripLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private Syncfusion.Windows.Forms.Tools.TrackBarItem trackBarItem1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel9;
        private PointerControl pointerControl7;
        private PointerControl pointerControl6;
        private PointerControl pointerControl5;
        private PointerControl pointerControl4;
        private PointerControl pointerControl3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private PointerControl pointerControl1;
        private Syncfusion.Windows.Forms.Tools.TreeViewAdv treeViewAdv2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ToolStripButton toolStripButton25;
        private System.Windows.Forms.ToolStripButton toolStripButton28;
        private System.Windows.Forms.ToolStripButton toolStripButton27;
        private System.Windows.Forms.ToolStripButton toolStripButton26;
        private Syncfusion.Windows.Forms.Tools.SplashControl splashControl1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private Syncfusion.Windows.Forms.Grid.Grouping.GridGroupingControl gridGroupingControl1;
        private System.Windows.Forms.Panel panel15;
        private OutlookSearchBox outlookSearchBox1;
        private Syncfusion.Windows.Forms.BannerTextProvider bannerTextProvider1;
        private Syncfusion.Windows.Forms.BackStageTab backStageTab2;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem13;
        private System.Windows.Forms.ToolStripButton newmail;
        private System.Windows.Forms.ToolStripSplitButton newmailitems;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem12;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem11;
        private System.Windows.Forms.ToolStripSplitButton replybutton;
        private System.Windows.Forms.ToolStripSplitButton replyall;
        private System.Windows.Forms.ToolStripSplitButton forward;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem10;
        private System.Windows.Forms.ToolStripSplitButton movetn;
        private System.Windows.Forms.ToolStripSplitButton rules;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem9;
        private System.Windows.Forms.ToolStripSplitButton followupsptbtn;
        private System.Windows.Forms.ToolStripSplitButton readunread;
        private System.Windows.Forms.ToolStripSplitButton categorize;
    }
}

