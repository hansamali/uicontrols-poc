#region Copyright Syncfusion Inc. 2001 - 2016
// Copyright Syncfusion Inc. 2001 - 2016. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Syncfusion.Windows.Forms.Tools;
using Syncfusion.Grouping;
using Syncfusion.Windows.Forms.Grid;
using System.Collections;
using Syncfusion.Windows.Forms.Grid.Grouping;
using Syncfusion.Windows.Forms;

namespace OutlookDemo_2010
{
    public partial class OutlookForm : RibbonForm
    {
        int mailSplitterDistance = 0;
        int readerSplitterDistance = 0;
        Label lbl;
        Label draftLbl;
        private SuperAccelerator SuperAccelerator1;

        public OutlookForm()
        {
            InitializeComponent();
            lbl = new Label();
            lbl.Text = "2";
            lbl.ForeColor = Color.DeepSkyBlue;
            draftLbl = new Label();
            draftLbl.Text = "15";
            draftLbl.ForeColor = Color.DeepSkyBlue;
            lbl.AutoSize = true;
            draftLbl.AutoSize = true;
            this.ribbonControlAdv1.MenuButtonVisible = true;
            this.ribbonControlAdv1.SelectedTab = this.toolStripTabItem1;
            this.SuperAccelerator();
            using (Graphics g = this.CreateGraphics())
            {
                if (g.DpiX >= 120)
                {
                    this.ribbonControlAdv1.Size = new System.Drawing.Size(351, 163);
                    this.ribbonControlAdv1.MinimumSize = new Size(0, 200);
                }
                else
                {
                    this.ribbonControlAdv1.Size = new System.Drawing.Size(351, 153);
                }
            }
            lbl.Font = new Font("Segoe UI", 11, FontStyle.Regular);
            draftLbl.Font = new Font("Segoe UI", 11, FontStyle.Regular);
            //this.ribbonControlAdv1.Header.AddQuickItem(new Syncfusion.Windows.Forms.Tools.QuickButtonReflectable(toolStripButton2));
            this.treeViewAdv2.FullRowSelect = true;
            this.treeViewAdv2.SelectedNode = this.treeViewAdv2.Nodes[0];
            this.treeViewAdv2.SelectedNodeBackground = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(225, 225, 225));
            this.treeViewAdv2.SelectedNodeForeColor = Color.Black;
            this.treeViewAdv2.MouseUp += new MouseEventHandler(treeViewAdv1_MouseUp);
            this.treeViewAdv2.MouseMove += new MouseEventHandler(treeViewAdv1_MouseMove);
            this.treeViewAdv2.NodeMouseClick += new TreeNodeAdvMouseClickArgs(treeViewAdv1_NodeMouseClick);
            this.splitContainerAdv1.Panel1.Size = new Size(600, this.splitContainerAdv1.Panel1.Size.Height);
            this.treeViewAdv2.MouseLeave += new EventHandler(treeViewAdv1_MouseLeave);
            ds = new DataSet();
            ReadXml(ds, "data.xml");
            this.gridGroupingControl1.DataSource = ds.Tables[0];
            this.gridGroupingControl1.TableDescriptor.GroupedColumns.Add("Today");
            this.gridGroupingControl1.TableDescriptor.TopLevelGroupOptions.ShowCaption = false;
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("ContactTitle");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("Address");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("City");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("PostalCode");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("To");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("Greetings");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("ContactName");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("ContactID");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("Phone");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("CompanyName");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("Message");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("Time");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("Date");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("Day");
            this.gridGroupingControl1.TableDescriptor.VisibleColumns.Remove("Size");

            this.gridGroupingControl1.TableDescriptor.TableOptions.ShowRowHeader = false;
            this.gridGroupingControl1.Table.ExpandAllGroups();
            this.gridGroupingControl1.ChildGroupOptions.CaptionText = this.gridGroupingControl1.TableDescriptor.GroupedColumns[0].Name.ToString();
            this.gridGroupingControl1.ShowRowHeaders = false;
            this.gridGroupingControl1.ShowColumnHeaders = false;
            this.gridGroupingControl1.TopLevelGroupOptions.ShowAddNewRecordAfterDetails = false;
            this.gridGroupingControl1.TableDescriptor.AllowEdit = false;
            this.gridGroupingControl1.TableDescriptor.AllowNew = false;
            this.gridGroupingControl1.TopLevelGroupOptions.ShowCaption = false;
            this.gridGroupingControl1.TableControlCurrentCellStartEditing += new Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlCancelEventHandler(gridGroupingControl1_TableControlCurrentCellStartEditing);
            this.gridGroupingControl1.Table.DefaultRecordRowHeight = 57;
            this.gridGroupingControl1.Table.ExpandAllGroups();
            this.gridGroupingControl1.TableOptions.ListBoxSelectionCurrentCellOptions = Syncfusion.Windows.Forms.Grid.Grouping.GridListBoxSelectionCurrentCellOptions.None;
            this.gridGroupingControl1.TableOptions.ListBoxSelectionMode = SelectionMode.One;
            this.gridGroupingControl1.QueryCellStyleInfo += new Syncfusion.Windows.Forms.Grid.Grouping.GridTableCellStyleInfoEventHandler(gridGroupingControl1_QueryCellStyleInfo);
            this.gridGroupingControl1.ActivateCurrentCellBehavior = Syncfusion.Windows.Forms.Grid.GridCellActivateAction.None;
            this.gridGroupingControl1.TableModel.CellModels.Add("OutlookHeaderCell", new OutlookCell.OutlookHeaderCellModel(this.gridGroupingControl1.TableModel));
            this.gridGroupingControl1.TableModel.QueryColWidth += new Syncfusion.Windows.Forms.Grid.GridRowColSizeEventHandler(TableModel_QueryColWidth);
            this.gridGroupingControl1.TableControlCellClick += new Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlCellClickEventHandler(gridGroupingControl1_TableControlCellClick);
            this.setMessageTextBoxText();
            this.gridGroupingControl1.TableOptions.SelectionBackColor =  ColorTranslator.FromHtml("#CDE6F7");
            this.gridGroupingControl1.TableOptions.ListBoxSelectionOutlineBorder = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.Standard);
            this.gridGroupingControl1.TableControlCellMouseHover += new Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlCellMouseEventHandler(gridGroupingControl1_TableControlCellMouseHover);
            this.gridGroupingControl1.TableModel.QueryRowHeight += new GridRowColSizeEventHandler(TableModel_QueryRowHeight);
            this.gridGroupingControl1.TableControl.MouseWheel += new MouseEventHandler(TableControl_MouseWheel);
            this.gridGroupingControl1.TableControlDrawCellDisplayText += new Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlDrawCellDisplayTextEventHandler(gridGroupingControl1_TableControlDrawCellDisplayText);
            this.gridGroupingControl1.TableControl.ScrollbarsVisibleChanged += new EventHandler(TableControl_ScrollbarsVisibleChanged);
            this.gridGroupingControl1.TableControlDrawCell += new Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlDrawCellEventHandler(gridGroupingControl1_TableControlDrawCell);
            this.gridGroupingControl1.DefaultGridBorderStyle = GridBorderStyle.None;
            this.gridGroupingControl1.BorderStyle = BorderStyle.None;
            this.InnerSplitterContainer.SplitterDistance = this.outlookSearchBox1.Width - 40;
            mailSplitterDistance = this.splitContainerAdv1.SplitterDistance;
            readerSplitterDistance = this.InnerSplitterContainer.SplitterDistance;
            this.splitContainerAdv1.SplitterMoved += new Syncfusion.Windows.Forms.Tools.Events.SplitterMoveEventHandler(splitContainerAdv1_SplitterMoved);
            this.InnerSplitterContainer.SplitterMoved += new Syncfusion.Windows.Forms.Tools.Events.SplitterMoveEventHandler(InnerSplitterContainer_SplitterMoved);
            this.statusStripEx1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(114)))), ((int)(((byte)(198)))));
            this.gridGroupingControl1.TableOptions.ListBoxSelectionCurrentCellOptions = GridListBoxSelectionCurrentCellOptions.HideCurrentCell;
            this.gridGroupingControl1.TableOptions.SelectionBackColor = Color.White;//ColorTranslator.FromHtml("#CDE6F7");
            
            this.gridGroupingControl1.Appearance.GroupCaptionPlusMinusCell.BackColor = ColorTranslator.FromHtml("#F0F0F0");
            this.gridGroupingControl1.TableControlPrepareViewStyleInfo += new GridTableControlPrepareViewStyleInfoEventHandler(gridGroupingControl1_TableControlPrepareViewStyleInfo);
            this.gridGroupingControl1.TableControlCellMouseHoverEnter+=new GridTableControlCellMouseEventHandler(gridGroupingControl1_TableControlCellMouseHoverEnter);
            this.gridGroupingControl1.TableControlCellMouseHoverLeave+=new GridTableControlCellMouseEventHandler(gridGroupingControl1_TableControlCellMouseHoverLeave);
            this.gridGroupingControl1.TableControl.VScrollPixelPosChanging += new GridScrollPositionChangingEventHandler(TableControl_VScrollPixelPosChanging);
            this.splashControl1.SplashControlPanel.BorderType = SplashBorderType.None;
            this.splashControl1.ShowDialogSplash(this);
            this.splashControl1.AutoMode = false;
            this.splashControl1.HostForm = this;
            this.gridGroupingControl1.TableControlCellDrawn += new GridTableControlDrawCellEventHandler(gridGroupingControl1_TableControlCellDrawn);
            this.InnerSplitterContainer.SplitterMoving += new Syncfusion.Windows.Forms.Tools.Events.SplitterMoveEventHandler(InnerSplitterContainer_SplitterMoving);
            dt = ds.Tables[0];
            this.gridGroupingControl1.TableModel.QueryColWidth+=new GridRowColSizeEventHandler(TableModel_QueryColWidth);
            this.gridGroupingControl1.TableControlCellMouseHover+=new GridTableControlCellMouseEventHandler(gridGroupingControl1_TableControlCellMouseHover);
            this.gridGroupingControl1.TableControl.CellClick += new GridCellClickEventHandler(TableControl_CellClick);
            this.gridGroupingControl1.VisibleChanged += new EventHandler(gridGroupingControl1_VisibleChanged);
            this.gridGroupingControl1.AllowProportionalColumnSizing = true;
            this.gridGroupingControl1.TableControl.Refresh();
            this.label2.Click += new EventHandler(label2_Click);
            this.label1.Click += new EventHandler(label1_Click);
            foreach (Control ctrl in this.outlookSearchBox1.Controls)
            {
                if (ctrl is Label)
                {
                    if ((ctrl as Label).Text.ToLower() == "unread")
                    {
                        (ctrl as Label).ForeColor = Color.FromArgb(68, 68, 68);
                    }
                    else if ((ctrl as Label).Text.ToLower() == "all")
                    {
                        (ctrl as Label).ForeColor = Color.FromArgb(58, 187, 246);
                        outlookSearchBox1.Label1Clicked = true;
                    }
                    ((Label)ctrl).Click += new EventHandler(OutlookForm_Click);
                }
            }
            unreadMessage.Add(1);
            unreadMessage.Add(2);
            unreadMessage.Add(3);
            lbl.BackColor = Color.Transparent;
            draftLbl.BackColor = Color.Transparent;
            this.treeViewAdv2.Nodes[0].Nodes[0].CustomControl = lbl;
            this.treeViewAdv2.BackColor = Color.FromArgb(252,252,252);
            this.panel9.BackColor = Color.FromArgb(252, 252, 252);
            this.panel2.BackColor = Color.FromArgb(252, 252, 252);
            this.splitContainerAdv1.BackColor = Color.FromArgb(252, 252, 252);
            this.toolStripTextBox1.BorderStyle = BorderStyle.FixedSingle;
            this.toolStripTextBox1.MouseDown += new MouseEventHandler(toolStripTextBox1_MouseDown);
            this.toolStripTextBox1.LostFocus += new EventHandler(toolStripTextBox1_LostFocus);
            this.toolStripTextBox1.Text = "Search";
            this.toolStripTextBox1.ForeColor = Color.Gray;
            this.ribbonControlAdv1.QuickPanelVisible = true;
            this.ribbonControlAdv1.TouchMode = false;
            this.Padding = new Padding(3);
            this.gridGroupingControl1.TableControl.HScrollBar.Enabled = false;
            this.gridGroupingControl1.TableControl.HScrollBehavior = GridScrollbarMode.Disabled;
            this.gridGroupingControl1.Table.DefaultRecordRowHeight = RowHeightOnScaling();
            this.backStage1.VisibleChanged += new EventHandler(backStage1_VisibleChanged);
            this.ribbonControlAdv1.ShowRibbonDisplayOptionButton = false;
            using (Graphics g = this.CreateGraphics())
            {
                if (g.DpiX >= 120)
                {
                    this.treeViewAdv2.Size = new Size(210, 232);
                    this.Size = new Size(700, 500);
                    this.outlookSearchBox1.Controls[4].Width += 20;
                }
            }
            this.ribbonControlAdv1.RibbonHeaderImage = RibbonHeaderImage.None;
            this.ribbonControlAdv1.MenuButtonClick += new EventHandler(ribbonControlAdv1_MenuButtonClick);
            this.Load += new EventHandler(OutlookForm_Load);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.splitContainerAdv1.IsSplitterFixed = true;
            this.InnerSplitterContainer.SplitterDistance = this.outlookSearchBox1.Controls[3].Location.X + this.outlookSearchBox1.Controls[3].Width + 10;
            this.gridGroupingControl1.SizeChanged += new EventHandler(gridGroupingControl1_SizeChanged);
            this.Shown += new EventHandler(OutlookForm_Shown);
            screenBounds = Screen.PrimaryScreen.Bounds;
            this.MessageRichTextBox.MouseEnter += new EventHandler(MessageRichTextBox_MouseEnter);
            this.gridGroupingControl1.TableControl.VScrollBehavior = GridScrollbarMode.Enabled;
            this.gridGroupingControl1.TableControlCellMouseHoverLeave += new GridTableControlCellMouseEventHandler(gridGroupingControl1_TableControlCellMouseHoverLeave);
            this.gridGroupingControl1.TableControlKeyDown += new GridTableControlKeyEventHandler(gridGroupingControl1_TableControlKeyDown);
            this.gridGroupingControl1.TableControl.CurrentCellMoving += new GridCurrentCellMovingEventHandler(TableControl_CurrentCellMoving);


            foreach (ToolStripTabItem items in this.ribbonControlAdv1.Header.MainItems)
            {
                foreach (ToolStripEx item in items.Panel.Controls)
                {
                    item.LauncherClick += Item_LauncherClick;
                }
            }
        }

        /// <summary>
        /// This event raises when the launcher is clicked
        /// </summary>
        /// <param name="sender">Instance of the Object</param>
        /// <param name="e">Contains data for the source</param>
        private void Item_LauncherClick(object sender, EventArgs e)
        {
          
            MessageBox.Show("Launcher is clicked", "Launcher");
        }

        private void SuperAccelerator()
        {
            //SuperAccelerator
            this.SuperAccelerator1 = new SuperAccelerator(this);
            this.SuperAccelerator1.Appearance = Syncfusion.Windows.Forms.Tools.Appearance.Advanced;
            this.SuperAccelerator1.BackColor = Color.Black;
            this.SuperAccelerator1.ForeColor = Color.White;
            this.ribbonControlAdv1.SuperAccelerator = this.SuperAccelerator1;
            this.SuperAccelerator1.SetAccelerator(toolStripTabItem1, "F");
            this.SuperAccelerator1.SetMenuButtonAccelerator(this.ribbonControlAdv1, "F");
            this.SuperAccelerator1.SetAccelerator(this.toolStripTabItem1, "H");
            this.SuperAccelerator1.SetAccelerator(this.newmail, "N");
            this.SuperAccelerator1.SetAccelerator(this.newmailitems, "I");
            this.SuperAccelerator1.SetAccelerator(this.IgnoreButton, "X");
            this.SuperAccelerator1.SetAccelerator(this.CleanUpSplitButton, "C");
            this.SuperAccelerator1.SetAccelerator(this.JunkSplitButton, "J");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton2, "D");
            this.SuperAccelerator1.SetAccelerator(this.replybutton, "RP");
            this.SuperAccelerator1.SetAccelerator(this.replyall, "RA");
            this.SuperAccelerator1.SetAccelerator(this.forward, "FW");
            this.SuperAccelerator1.SetAccelerator(this.MeetingButton, "MR");
            this.SuperAccelerator1.SetAccelerator(this.MoreButton, "ME");
            this.SuperAccelerator1.SetAccelerator(this.movetn, "MV");
            this.SuperAccelerator1.SetAccelerator(this.rules, "RR");
            this.SuperAccelerator1.SetAccelerator(this.readunread, "W");
            this.SuperAccelerator1.SetAccelerator(this.categorize, "G");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton16, "V");
            this.SuperAccelerator1.SetAccelerator(this.toolStripTextBox1, "FC");
            this.SuperAccelerator1.SetAccelerator(this.AddressBookButton, "AB");
            this.SuperAccelerator1.SetAccelerator(this.FilterEmailButton, "L");
            this.SuperAccelerator1.SetAccelerator(this.toolStripTabItem2, "S");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton3, "S");
            this.SuperAccelerator1.SetAccelerator(this.UpdateFolderButton, "D");
            this.SuperAccelerator1.SetAccelerator(this.SendAllButton, "A");
            this.SuperAccelerator1.SetAccelerator(this.SendReceiveGroupsSplit, "G");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton5, "P");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton4, "C");
            this.SuperAccelerator1.SetAccelerator(this.followupsptbtn, "U");
            this.SuperAccelerator1.SetAccelerator(this.toolStripTabItem2, "S");
            this.SuperAccelerator1.SetAccelerator(this.toolStripTabItem3, "O");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton25, "N");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton9, "SF");
            this.SuperAccelerator1.SetAccelerator(this.toolStripTabItem5, "RN");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton10, "CF");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton11, "MV");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton13, "DF");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton28, "MA");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton27, "RR");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton26, "H");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton14, "CU");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton15, "DA");
            this.SuperAccelerator1.SetAccelerator(this.toolStripSplitButton7, "RD");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton12, "FA");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton16, "V");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton19, "A");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton18, "PP");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton17, "FP");
            this.SuperAccelerator1.SetAccelerator(this.toolStripTabItem9, "V");
            this.SuperAccelerator1.SetAccelerator(this.toolStripSplitButton12, "CV");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton21, "V");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton20, "X");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton23, "CA");
            this.SuperAccelerator1.SetAccelerator(this.toolStripCheckBox1, "GC");
            this.SuperAccelerator1.SetAccelerator(this.toolStripSplitButton8, "CS");
            this.SuperAccelerator1.SetAccelerator(this.toolStripSplitButton11, "F");
            this.SuperAccelerator1.SetAccelerator(this.toolStripSplitButton10, "PN");
            this.SuperAccelerator1.SetAccelerator(this.toolStripSplitButton9, "B");
            this.SuperAccelerator1.SetAccelerator(this.toolStripSplitButton13, "PP");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton22, "M");
            this.SuperAccelerator1.SetAccelerator(this.toolStripButton24, "ON");
            this.SuperAccelerator1.SetAccelerator(this.backStageTab2, "O");
            this.SuperAccelerator1.SetAccelerator(this.backStageTab1, "I");
            this.SuperAccelerator1.SetAccelerator(this.backStageTab3, "A");
            this.SuperAccelerator1.SetAccelerator(this.backStageTab4, "M");
            this.SuperAccelerator1.SetAccelerator(this.backStageTab5, "P");
            this.SuperAccelerator1.SetAccelerator(this.backStageTab6, "D");
            this.SuperAccelerator1.SetAccelerator(this.backStageTab7, "X");
            this.SuperAccelerator1.SetAccelerator(this.backStageButton1, "X");

            ToolStripButton btn = new ToolStripButton(OutlookDemo_2010.Properties.Resources.New_Mail);
            this.SuperAccelerator1.SetAccelerator(btn, "1");
            this.ribbonControlAdv1.Header.AddQuickItem(btn);
            ToolStripButton btn1 = new ToolStripButton(OutlookDemo_2010.Properties.Resources.Delete1);
            this.SuperAccelerator1.SetAccelerator(btn1, "2");
            this.ribbonControlAdv1.Header.AddQuickItem(btn1);
        }

        void TableControl_CurrentCellMoving(object sender, GridCurrentCellMovingEventArgs e)
        {
            GridCurrentCell cc = this.gridGroupingControl1.TableControl.GetNestedCurrentCell();
            GridTableCellStyleInfo tableStyle = this.gridGroupingControl1.TableControl.GetTableViewStyleInfo(cc.RowIndex, cc.ColIndex);
            if (tableStyle != null)
            {
                if (cc.ColIndex == 0)
                {
                    if (isUpArrowPressed)
                    {
                        GridTableCellStyleInfo nextCellStyle = this.gridGroupingControl1.TableControl.GetTableViewStyleInfo(cc.RowIndex - 1, cc.ColIndex);
                        if (nextCellStyle.TableCellIdentity.TableCellType == GridTableCellType.GroupCaptionRowHeaderCell && isMoving)
                        {
                            isMoving = false;
                            e.Cancel = true;
                            if (cc.RowIndex - 2 >= 1)
                                cc.MoveTo(cc.RowIndex - 2, cc.ColIndex);
                        }
                    }
                }
            }
        }
        bool isMoving = false;
        bool isUpArrowPressed = false;
        void gridGroupingControl1_TableControlKeyDown(object sender, GridTableControlKeyEventArgs e)
        {
            isMoving = true;
            isUpArrowPressed = false;
            if (e.Inner.KeyCode == Keys.Up)
                isUpArrowPressed = true;
        }

        void MessageRichTextBox_MouseEnter(object sender, EventArgs e)
        {
            //this.gridGroupingControl1.TableControl.Refresh();
        }
        Rectangle screenBounds = new Rectangle();
        void OutlookForm_Shown(object sender, EventArgs e)
        {
            //this.gridGroupingControl1.TableControl.Refresh();
        }

        void gridGroupingControl1_SizeChanged(object sender, EventArgs e)
        {
            //this.gridGroupingControl1.TableControl.Refresh();
        }


        void OutlookForm_Load(object sender, EventArgs e)
        {
            using (Graphics g = this.CreateGraphics())
            { 
                if (Screen.PrimaryScreen.Bounds.Size == new Size(1920, 1080))
                {
                    this.InnerSplitterContainer.SplitterDistance = 192;
                }
                else if (Screen.PrimaryScreen.Bounds.Size == new Size(1440, 900))
                {
                    this.InnerSplitterContainer.SplitterDistance = this.outlookSearchBox1.Controls[3].Location.X + this.outlookSearchBox1.Controls[3].Width;
                }
                if (g.DpiX >= 120)
                {
                    this.InnerSplitterContainer.SplitterDistance = this.outlookSearchBox1.Width / 2 + 45;
                }
            }

            this.WindowState = FormWindowState.Maximized;
        }
       
        void ribbonControlAdv1_MenuButtonClick(object sender, EventArgs e)
        {
            //this.SuspendLayout();
        }

        void backStage1_VisibleChanged(object sender, EventArgs e)
        {
            //this.ribbonControlAdv1.Size = new Size(0, 163);
            //this.ResumeLayout();
        }

        private int padding = 5;
        private bool isDpiAware = false;
        private int degreeOfPercentage = 100;
        /// <summary>
        /// When DPI is greater than 100 then the DefaultRowHeight will be set based on the font size.
        /// </summary>
        /// <returns>The Height Value</returns>
        internal int RowHeightOnScaling()
        {
            if (this.gridGroupingControl1.TableModel.ActiveGridView != null)
                using (Graphics graph = this.gridGroupingControl1.TableModel.ActiveGridView.CreateGraphics())
                {
                    if (graph.DpiY > 100)
                    {
                        degreeOfPercentage = (int)graph.DpiY - 100;
                        isDpiAware = true;
                        return 60;
                    }
                }
            return this.gridGroupingControl1.Table.DefaultRecordRowHeight;
        }

        

        void toolStripTextBox1_LostFocus(object sender, EventArgs e)
        {
            this.toolStripTextBox1.Text = "Search";
        }

        

        void toolStripTextBox1_MouseDown(object sender, MouseEventArgs e)
        {
            this.toolStripTextBox1.Text = "";
        }

      

        void OutlookForm_Click(object sender, EventArgs e)
        {
            if (((Label)sender).Text == "Unread")
            {
                isUnreadClicked = true;
                this.gridGroupingControl1.TableControl.Refresh();
            }
            if (((Label)sender).Text == "All")
            {
                isUnreadClicked = false;
                this.gridGroupingControl1.TableControl.Refresh();
            }
        }

        void label1_Click(object sender, EventArgs e)
        {
            
        }

        void label2_Click(object sender, EventArgs e)
        {
        }
        bool isUnreadClicked = false;
        void gridGroupingControl1_VisibleChanged(object sender, EventArgs e)
        {
            this.gridGroupingControl1.Invalidate(true);
        }
        int selectedRow = 0;
        void TableControl_CellClick(object sender, GridCellClickEventArgs e)
        {
            Element el = this.gridGroupingControl1.Table.GetInnerMostCurrentElement();
            GridTable table;
               if (el != null)
               {
                   table = el.ParentTable as GridTable;
                   GridTableControl tableControl = this.gridGroupingControl1.GetTableControl(table.TableDescriptor.Name);
                   GridCurrentCell cc = tableControl.CurrentCell;
                   GridTableCellStyleInfo style = table.GetTableCellStyle(cc.RowIndex, cc.ColIndex);
                   GridRecord rec = el as GridRecord;                 
               }
               if (this.gridGroupingControl1.TableControl.Table.CurrentRecord != null)
               {
                   selectedRow = this.gridGroupingControl1.TableControl.Table.CurrentRecord.Id;
               }
            GridTableCellStyleInfo styleInfo = this.gridGroupingControl1.TableModel[e.RowIndex, e.ColIndex];            
            int rowIndex = e.RowIndex;
            int colIndex = e.ColIndex;
            Rectangle rect = this.gridGroupingControl1.TableControl.RangeInfoToRectangle(GridRangeInfo.Cell(rowIndex, colIndex));
            rect.Width = 50;
            if (unreadMessage != null)
            {
                if (unreadMessage.Contains(rowIndex))
                {
                    unreadMessage.Remove(rowIndex);
                    if (el != null)
                    {
                        table = el.ParentTable as GridTable;
                        if (table.CurrentRecord != null)
                        {
                            int temp = el.ParentGroup.Records.Count;
                            for (int i = 0; i < el.ParentGroup.Records.Count; i++)
                            {
                                if (el.ParentGroup.Records[i] == table.CurrentRecord)
                                {
                                    temp = i;
                                    break;
                                }
                            }
                            int ids = table.CurrentRecord.GetRowIndex() - temp - 1;
                            if (unreadMessage.Contains(ids))
                                unreadMessage.Remove(ids);
                        }
                    }
                    int rest = 0;
                    if (int.TryParse(this.lbl.Text, out rest))
                    {
                        lbl.Text = (int.Parse(this.lbl.Text) - 1).ToString();
                        if (lbl.Text == "0")
                            lbl.Text = "";
                    }
                }
            }
            if (rect.Contains(e.MouseEventArgs.Location))
            {
                if (unreadMessage.Contains(rowIndex))
                {
                    unreadMessage.Remove(rowIndex);
                    lbl.Text = (int.Parse(this.lbl.Text) - 1).ToString();
                    if (lbl.Text == "0")
                        lbl.Text = string.Empty;
                }
                else
                {
                    unreadMessage.Add(rowIndex);
                    if (this.gridGroupingControl1.TableControl.Table.CurrentRecord != null)
                    {
                        if (el != null)
                        {
                            table = el.ParentTable as GridTable;
                            if (table.CurrentRecord != null)
                            {
                                int temp = el.ParentGroup.Records.Count;
                                for(int i=0;i<el.ParentGroup.Records.Count;i++)
                                {
                                    if (el.ParentGroup.Records[i] == table.CurrentRecord)
                                    {
                                        temp = i;
                                        break;
                                    }
                                }
                                int ids = table.CurrentRecord.GetRowIndex() - temp - 1;
                                unreadMessage.Add(ids);
                            }
                        }
                    }
                    string text = this.lbl.Text;
                    if (text == string.Empty)
                        text = "0";
                    lbl.Text = (int.Parse(text) + 1).ToString();
                }
            }
            if (styleInfo.CellType == "OutlookHeaderCell")
            {
                if (categoryRect.X < e.MouseEventArgs.Location.X && (categoryRect.X + categoryRect.Width) > e.MouseEventArgs.Location.X)
                {
                    if (category.Contains(rowIndex))
                    {
                        category.Remove(rowIndex);
                    }
                    else
                    {
                        category.Add(rowIndex);
                    }
                }
                if (closeImage.X < e.MouseEventArgs.Location.X && (closeImage.X + closeImage.Width) > e.MouseEventArgs.Location.X)
                {
                    if (this.gridGroupingControl1.TableControl.Table.CurrentRecord != null)
                        this.gridGroupingControl1.TableControl.Table.CurrentRecord.Delete();
                    this.MessageRichTextBox.Clear();
                }
            }
            if (isUnreadClicked)
                this.gridGroupingControl1.TableControl.Refresh();
        }
        List<int> category = new List<int>();

        void InnerSplitterContainer_SplitterMoving(object sender, Syncfusion.Windows.Forms.Tools.Events.SplitterMoveEventArgs e)
        {
            
        }
        DataSet ds;
        DataTable dt;
        public List<int> unreadMessage = new List<int>();
        int rowIndex = 0;
        void gridGroupingControl1_TableControlCellDrawn(object sender, GridTableControlDrawCellEventArgs e)
        {
            int ht = 20;
            if (degreeOfPercentage != 100)
                ht = ht + ht * degreeOfPercentage / 100 + 2;
            if (e.Inner.Style.CellType == "OutlookHeaderCell")
            {
                rowIndex = e.Inner.RowIndex;
                Graphics g = e.Inner.Graphics;
                Rectangle clRect = e.Inner.Bounds;
                int result = 0;
                if (int.TryParse(e.Inner.Style.ValueMember, out result))
                {
                    if (!unreadMessage.Contains(rowIndex))
                        unreadMessage.Add(rowIndex);
                }

                Rectangle firstDrawString = new Rectangle(clRect.X + 2, clRect.Y + 1, clRect.Width - 110, ht);
                Rectangle secondDrawString = new Rectangle(clRect.X + 2, clRect.Y + 22, clRect.Width - 110, ht-4);
                Rectangle thirdDrawString = new Rectangle(clRect.X + 2, clRect.Y + 38, clRect.Width - 110, ht-6);
                Rectangle fourthDrawString = new Rectangle(clRect.Width - 90, clRect.Y + 22, 100, ht-6);
                Rectangle fifthDrawString = new Rectangle();
                Font firstFont;
                Font secondFont;
                Font thirdFont;
                Font fourthFont;
                using (Graphics g1 = Graphics.FromImage(new Bitmap(10,10)))
                {
                    if (g1.DpiX > 120)
                    {
                        firstFont = new Font("Segoe UI", 7.5f);
                        secondFont = new Font("Segoe UI", 6f);
                        thirdFont = new Font("Segoe UI", 6f);
                        fourthFont = new Font("Segoe UI", 6f);
                    }
                    else
                    {
                        firstFont = new Font("Segoe UI", 11.5f);
                        secondFont = new Font("Segoe UI", 9f);
                        thirdFont = new Font("Segoe UI", 8f);
                        fourthFont = new Font("Segoe UI", 8f);
                    }
                }
                SolidBrush firstBrush = new SolidBrush(ColorTranslator.FromHtml("#0E0E0E"));
                SolidBrush secondBrush = new SolidBrush(ColorTranslator.FromHtml("#0E0E0E"));
                SolidBrush thirdBrush = new SolidBrush(ColorTranslator.FromHtml("#0E0E0E"));
                SolidBrush fourthBrush = new SolidBrush(ColorTranslator.FromHtml("#0E0E0E"));
                string firstString = "Customer Support";
                string secondString = "Please schedule the meeting on tomorrow";
                string thirdString = "<http.customersupport.com>";
                string fourthString = "11.58 AM";
                closeImage = new Rectangle(clRect.X + clRect.Width - 25, clRect.Y + 20, 20, 20);

                if (e.Inner.RowIndex < dt.Rows.Count + 2)
                {
                    firstString = dt.Rows[e.Inner.RowIndex - 2].ItemArray[2].ToString();
                    secondString = dt.Rows[e.Inner.RowIndex - 2].ItemArray[3].ToString();
                    thirdString = dt.Rows[e.Inner.RowIndex - 2].ItemArray[5].ToString();
                    if (char.IsNumber(dt.Rows[e.Inner.RowIndex - 2].ItemArray[12].ToString(), 0))
                    {
                        fourthString = "    " + dt.Rows[e.Inner.RowIndex - 2].ItemArray[12].ToString();
                    }
                    else
                        fourthString = dt.Rows[e.Inner.RowIndex - 2].ItemArray[12].ToString();
                }

                if (e.TableControl.Table.CurrentRecord != null)
                {
                    if (e.TableControl.Table.CurrentRecord.GetRowIndex() == e.Inner.RowIndex)
                    {
                        //Pen blackPen = new Pen(Color.Black, 1);
                        //blackPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                        Rectangle paintRect = new Rectangle(e.Inner.Bounds.X, e.Inner.Bounds.Y + 1, e.Inner.Bounds.Width, e.Inner.Bounds.Height - 2);
                        //g.DrawRectangle(blackPen, paintRect);
                        g.FillRectangle(new SolidBrush(Color.FromArgb(205, 230, 247)), paintRect);
                    }
                }

                if (unreadMessage.Contains(rowIndex))
                {
                    using (Graphics g1 = Graphics.FromImage(new Bitmap(10,10)))
                    {
                        if (g1.DpiX > 120)
                        {
                            firstFont = new Font("Segoe UI", 7.55f);
                            secondFont = new Font("Segoe UI", 6f, FontStyle.Bold);// Semilight
                            fourthFont = new Font("Segoe UI", 6f, FontStyle.Bold);
                        }
                        else
                        {
                            firstFont = new Font("Segoe UI", 11.55f);
                            secondFont = new Font("Segoe UI", 9f, FontStyle.Bold);// Semilight
                            fourthFont = new Font("Segoe UI", 9f, FontStyle.Bold);
                        }
                    }
                    firstBrush = new SolidBrush(Color.Black);
                    secondBrush = new SolidBrush(ColorTranslator.FromHtml("#006FC4"));
                    g.FillRectangle(secondBrush, clRect.X, clRect.Y + 1, 3, clRect.Height - 2);
                    if (IsColWidthChanged)
                    {
                        firstBrush = new SolidBrush(ColorTranslator.FromHtml("#006FC4"));
                    }
                }
                if (IsColWidthChanged && screenBounds.Width < 1500)
                {
                    firstDrawString = new Rectangle(clRect.X + 2, clRect.Y + 6, clRect.Width * 20 / 100, ht);
                    secondDrawString = new Rectangle(clRect.Width * 20 / 100 + 12, clRect.Y + 6, clRect.Width * 30 / 100, ht);
                    thirdDrawString = new Rectangle(clRect.X + 2, clRect.Y + 28, clRect.Width * 80 / 100, ht-2);
                    fourthDrawString = new Rectangle(clRect.Width * 50 / 100 + 12, clRect.Y + 6, clRect.Width * 20 / 100, ht-6);
                    fifthDrawString = new Rectangle(clRect.Width * 70 / 100 + 12, clRect.Y + 6, clRect.Width * 10 / 100, ht-4);
                    using (Graphics g1 = Graphics.FromImage(new Bitmap(10, 10)))
                    {
                        if (g1.DpiX > 120)
                        {
                            firstFont = new Font("Segoe UI", 6.25f);
                            secondFont = new Font("Segoe UI", 6.25f);
                            thirdFont = new Font("Segoe UI", 6.25f);
                            fourthFont = new Font("Segoe UI", 6.25f);
                        }
                        else
                        {
                            firstFont = new Font("Segoe UI", 9.25f);
                            secondFont = new Font("Segoe UI", 9.25f);
                            thirdFont = new Font("Segoe UI", 9.25f);
                            fourthFont = new Font("Segoe UI", 9.25f);
                        }
                    }
                    if (unreadMessage.Contains(rowIndex))
                    {
                        using (Graphics g1 = Graphics.FromImage(new Bitmap(10,10)))
                        {
                            if (g1.DpiX > 120)
                            {
                                firstFont = new Font("Segoe UI", 6.25f, FontStyle.Bold);
                                secondFont = new Font("Segoe UI", 6.25f, FontStyle.Bold);
                                fourthFont = new Font("Segoe UI", 6.25f, FontStyle.Bold);
                            }
                            else
                            {
                                firstFont = new Font("Segoe UI", 9.25f, FontStyle.Bold);
                                secondFont = new Font("Segoe UI", 9.25f, FontStyle.Bold);
                                fourthFont = new Font("Segoe UI", 9.25f, FontStyle.Bold);
                            }
                        }
                    }
                    string fifthString = "54KB";
                    if (e.Inner.RowIndex < dt.Rows.Count + 2)
                        fifthString = dt.Rows[e.Inner.RowIndex - 2].ItemArray[15].ToString();
                    g.DrawString(fifthString, fourthFont, secondBrush, fifthDrawString);
                    categoryRect = new Rectangle(clRect.Width * 80 / 100 + 6, clRect.Y + 6, 8, ht-6);
                    g.DrawRectangle(new Pen(Color.LightGray), categoryRect);
                    if (category.Contains(rowIndex))
                    {
                        g.FillRectangle(new SolidBrush(Color.FromArgb(124, 206, 110)), categoryRect);
                        Rectangle sixthDrawString = new Rectangle(clRect.Width * 80 / 100 + 6 + categoryRect.Width, clRect.Y + 6, 90, ht-6);
                        g.DrawRectangle(new Pen(Color.FromArgb(57, 125, 42)), categoryRect);
                        string sixthString = "Green..";
                        g.DrawString(sixthString, firstFont, firstBrush, sixthDrawString);
                    }
                    //
                }
                g.DrawString(firstString, firstFont, firstBrush, firstDrawString);
                g.DrawString(secondString, secondFont, secondBrush, secondDrawString);
                g.DrawString(thirdString, thirdFont, thirdBrush, thirdDrawString);
                g.DrawString(fourthString, fourthFont, secondBrush, fourthDrawString);
            }
        }
        Rectangle closeImage = new Rectangle();
        Rectangle categoryRect = new Rectangle();
        void TableControl_VScrollPixelPosChanging(object sender, GridScrollPositionChangingEventArgs e)
        {
            this.gridGroupingControl1.TableControl.Refresh();
        }

        void gridGroupingControl1_TableControlPrepareViewStyleInfo(object sender, GridTableControlPrepareViewStyleInfoEventArgs e)
        {
        }


        #region "Selection BackColor Customization"

        int currentRowIndex = 0, currentColIndex = 0;
        Hashtable selectedRows = new Hashtable();
        Hashtable selectionIndents = new Hashtable();

        void gridGroupingControl1_TableControlCellMouseHoverLeave(object sender, GridTableControlCellMouseEventArgs e)
        {
            GridTableCellStyleInfo styleInfo = e.TableControl.GetTableViewStyleInfo(e.Inner.RowIndex, e.Inner.ColIndex);
            if (styleInfo.TableCellIdentity!=null  && styleInfo.TableCellIdentity.TableCellType.ToString() != "Static")
            {
                if (e.Inner.ColIndex > 1)
                {
                    MouseHoverCheck(e.Inner.RowIndex, e.Inner.ColIndex, false);
                    this.gridGroupingControl1.TableControl.RefreshRange(GridRangeInfo.Row(e.Inner.RowIndex), GridRangeOptions.None);
                }
            }
        }

        void gridGroupingControl1_TableControlCellMouseHoverEnter(object sender, GridTableControlCellMouseEventArgs e)
        {
            GridTableCellStyleInfo styleInfo = e.TableControl.GetTableViewStyleInfo(e.Inner.RowIndex, e.Inner.ColIndex);
            if (styleInfo.TableCellIdentity.TableCellType.ToString() != "Static")
            {
                if (e.Inner.ColIndex > 1 &&e.Inner.ColIndex<=2)
                {
                    MouseHoverCheck(e.Inner.RowIndex, e.Inner.ColIndex, true);
                    currentRowIndex = e.Inner.RowIndex;
                    currentColIndex = e.Inner.ColIndex;
                    this.gridGroupingControl1.TableControl.RefreshRange(GridRangeInfo.Row(e.Inner.RowIndex), GridRangeOptions.None);
                }
            }
            this.gridGroupingControl1.TableControl.Refresh();
        }

        private void MouseHoverCheck(int row, int col, bool isHover)
        {
            if (col > 1)
            {
                GridTableCellStyleInfoIdentity id = this.gridGroupingControl1.TableControl.GetTableViewStyleInfo(row, col).TableCellIdentity;
                if (id.DisplayElement.IsRecord())
                {
                    int key = id.DisplayElement.GetRecord().Id;
                    selectedRows.Clear();
                    selectedRows.Add(key, isHover);
                    selectionIndents.Clear();
                    selectionIndents.Add(col - 2, isHover);
                    this.gridGroupingControl1.TableControl.RefreshRange(GridRangeInfo.Row(row));
                }
                if (this.gridGroupingControl1.TableControl.Selections.Count > 0)
                    this.gridGroupingControl1.TableControl.Selections.Clear();
            }
        }

        #endregion

        void InnerSplitterContainer_SplitterMoved(object sender, Syncfusion.Windows.Forms.Tools.Events.SplitterMoveEventArgs e)
        {
            readerSplitterDistance = this.InnerSplitterContainer.SplitterIncrement;
        }

        void splitContainerAdv1_SplitterMoved(object sender, Syncfusion.Windows.Forms.Tools.Events.SplitterMoveEventArgs e)
        {
            outlookSearchBox1.Width += 10;// (int)e.NewSplitPosition.X - (int)e.OldSplitPosition.X;
            mailSplitterDistance = this.splitContainerAdv1.SplitterDistance;
            if (this.splitContainerAdv1.SplitterDistance > this.HiddenPanel.Width)
            {
                this.HiddenPanel.Visible = false;
            }
        }


        void gridGroupingControl1_TableControlDrawCell(object sender, Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlDrawCellEventArgs e)
        {
        }

        void gridGroupingControl1_TableControlDrawCellDisplayText(object sender, Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlDrawCellDisplayTextEventArgs e)
        {
            if (this.gridGroupingControl1.TableModel[e.Inner.RowIndex, e.Inner.ColIndex].CellType == "IndentCell")
            {
                using (Graphics g = this.gridGroupingControl1.TableControl.CreateGridGraphics())
                {
                }
            }
        }

        void TableControl_ScrollbarsVisibleChanged(object sender, EventArgs e)
        {
            this.gridGroupingControl1.TableModel.Refresh();
        }

        void TableControl_MouseWheel(object sender, MouseEventArgs e)
        {
            this.gridGroupingControl1.TableModel.Refresh();
            this.gridGroupingControl1.Refresh();
        }

        void TableModel_QueryRowHeight(object sender, GridRowColSizeEventArgs e)
        {
            if (this.gridGroupingControl1.TableModel[e.Index, 2].CellType == "Static")
            {
                e.Size = 22;
                e.Handled = true;
            }
            if (isUnreadClicked)
            {
                if (!unreadMessage.Contains(e.Index))
                {
                    e.Size = 0;
                    e.Handled = true;
                }
            }
            if (emptyData)
            {
                e.Size = 0;
                e.Handled = true;
            }
            if (sentItems)
            {
                if (e.Index > 6)
                {
                    e.Size = 0;
                    e.Handled = true;
                }
            }
            //if (this.gridGroupingControl1.TableModel[e.Index, 2].CellType == "OutlookHeaderCell")
            //{
            //    if (IsColWidthChanged)
            //    {
            //        e.Size = 40;
            //    }
            //    else
            //        e.Size = 57;
            //    e.Handled = true;
            //}
        }
        int moveRowIndex = 0, moveColIndex = 0;
        bool hoveredFlag, hoveredClose = false;
        void gridGroupingControl1_TableControlCellMouseHover(object sender, Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlCellMouseEventArgs e)
        {
            int rowIndex, colIndex;
            rowIndex = e.Inner.RowIndex;
            colIndex = e.Inner.ColIndex;
            moveRowIndex = rowIndex;
            moveColIndex = colIndex;
            int result = 0;
            GridStyleInfo style = this.gridGroupingControl1.TableModel[rowIndex, colIndex];
            if (int.TryParse(style.ValueMember, out result))
            {
                if (!unreadMessage.Contains(rowIndex))
                    unreadMessage.Add(rowIndex);
            }
            if (style.CellType == "IndentCell")
            {
                this.gridGroupingControl1.TableControl.Refresh();
            }
            //
            //
            //
            if (this.gridGroupingControl1.TableModel[e.Inner.RowIndex, e.Inner.ColIndex].CellType == "IndentCell"
                || this.gridGroupingControl1.TableModel[e.Inner.RowIndex, e.Inner.ColIndex].CellType == "OutlookHeaderCell")
            {
                indentCellHovered = true;
                indentRI = e.Inner.RowIndex;
                indentCI = e.Inner.ColIndex;
                this.gridGroupingControl1.TableModel[e.Inner.RowIndex, e.Inner.ColIndex].BackColor = ColorTranslator.FromHtml("#E6F2FA");
            }
            else
                indentCellHovered = false;
            if (e.Inner.ColIndex == 3)
            {
                e.TableControl.RefreshRange(GridRangeInfo.Cols(2,2));
            }
        }
        bool indentCellHovered = false;
        int indentRI, indentCI = 0;
        private bool IsColWidthChanged = false;
        int dist = 30;
        void TableModel_QueryColWidth(object sender, Syncfusion.Windows.Forms.Grid.GridRowColSizeEventArgs e)
        {
            if (e.Index == 2)
            {
                if (screenBounds.Width > 1500)
                    dist = 40;
                e.Size = this.outlookSearchBox1.Width - dist;
                e.Handled = true;
            }
            if (e.Index == 3)
            {
                e.Size = 100;
                e.Handled = true;
            }
            if (e.Size > 400)
            {
                IsColWidthChanged = true;
            }
            else
            {
                IsColWidthChanged = false; 
            }
        }

        private void setMessageTextBoxText()
        {
            this.DateTimeLabel.Text = DateTime.Now.ToString();
            this.Tolabel.Text = "Katrina";
            this.Maillabel.Text = "John Peter";
            string rtf2 =
                @"{\rtf1\ansi" +
                // font table
                @"{\fonttbl" +
                @"\f0\fswiss Segoe UI;}" +
                @"\highlight\ql\f0\f0\fs20   " + "\\plain\\par" +
                @"\highlight1\ql\cf0\f0\fs20 {    Hi }" + "Katrina" + "," + "\\plain\\par" + Environment.NewLine +
                @"\highlight1\ql\f0\f2\fs20 " + "\\plain\\par" +
                // third line
                @"\highlight1\ql\cf0\f0\fs20     " + "Your appointment has been schedulded on today @ 9.00Am" + "." + "\\plain\\par" +
                @"\highlight\ql\f0\f0\fs20   " + "\\plain\\par" +
                @"\highlight1\ql\cf0\f0\fs20     " + "Thanks" + "," + "\\plain\\par" +
                @"\highlight1\ql\cf0\f0\fs20     " + "John" + "." + "\\plain\\par" +
                // closing bracket
                @"}";

            // Use display to show the content..
            this.MessageRichTextBox.Rtf = rtf2;
        }

        void gridGroupingControl1_QueryCellStyleInfo(object sender, Syncfusion.Windows.Forms.Grid.Grouping.GridTableCellStyleInfoEventArgs e)
        {
            if (e.Style.CellType == "TextBox")
                e.Style.CellType = "OutlookHeaderCell";
            if (e.Style.CellType == "OutlookHeaderCell")
                e.Style.Text = "";
            if (e.Style.CellType == "Header")
                e.Style.CellType = GridCellTypeName.Static;
            e.Style.Borders.All = GridBorder.Empty;
            if (e.Style.CellType == GridCellTypeName.Static)
            {
                Font newFont = new Font(e.Style.GdipFont.Name, e.Style.GdipFont.Size, FontStyle.Bold);
                e.Style.Font = new GridFontInfo(newFont);
                e.Style.VerticalAlignment = GridVerticalAlignment.Middle;
                e.Style.HorizontalAlignment = GridHorizontalAlignment.Left;
            }
            //selection
            if (e.TableCellIdentity.Column != null && e.TableCellIdentity.DisplayElement.IsRecord() && selectedRows != null && selectedRows.Count > 0)
            {
                int key = e.TableCellIdentity.DisplayElement.GetRecord().Id;
                if (selectedRows.Contains(key) && (bool)selectedRows[key])
                {
                    e.Style.BackColor = Color.FromArgb(230, 242, 250);// ColorTranslator.FromHtml("#CDE6F7");
                    e.Style.TextColor = Color.White;
                    hoveredRowIndex = e.TableCellIdentity.RowIndex;
                }
            }

            this.gridGroupingControl1.TableControl.Selections.Clear();

            if (e.Style.TableCellIdentity.TableCellType == GridTableCellType.GroupCaptionCell)
            {
                GridCaptionRow capRow = e.Style.TableCellIdentity.DisplayElement as GridCaptionRow;
                Group g = capRow.ParentGroup;
                if (!g.IsTopLevelGroup)
                {
                    e.Style.CellValue = g.Category.ToString().Substring(2);
                }
            }           
        }
        int hoveredRowIndex = -1;
        void gridGroupingControl1_TableControlCurrentCellStartEditing(object sender, Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlCancelEventArgs e)
        {
            e.Inner.Cancel = false;
        }

        void treeViewAdv1_MouseLeave(object sender, EventArgs e)
        {
            //if ((sender as TreeViewAdv) == this.treeViewAdv1)
            //{
            //    this.treeViewAdv1.Nodes[0].Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(0, 255, 255, 255));
            //    foreach (TreeNodeAdv item in this.treeViewAdv1.Nodes[0].Nodes)
            //    {
            //        item.Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(0, 255, 255, 255));
            //    }
            //}
            if ((sender as TreeViewAdv) == this.treeViewAdv2)
            {
                this.treeViewAdv2.Nodes[0].Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(0, 255, 255, 255));
                foreach (TreeNodeAdv item in this.treeViewAdv2.Nodes[0].Nodes)
                {
                    item.Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(0, 255, 255, 255));
                }
            }
        }

        void ReadXml(DataSet ds, string xmlFileName)
        {
            for (int n = 0; n < 10; n++)
            {
                if (System.IO.File.Exists(xmlFileName))
                {
                    ds.ReadXml(xmlFileName);
                    break;
                }
                xmlFileName = @"..\" + xmlFileName;
            }
        }

        void treeViewAdv1_MouseMove(object sender, MouseEventArgs e)
        {
            //if ((sender as TreeViewAdv) == this.treeViewAdv1)
            //{
            //    this.treeViewAdv1.Nodes[0].Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(0, 255, 255, 255));
            //    foreach (TreeNodeAdv item in this.treeViewAdv1.Nodes[0].Nodes)
            //    {
            //        item.Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(0, 255, 255, 255));
            //    }
            //}
            if ((sender as TreeViewAdv) == this.treeViewAdv2)
            {
                this.treeViewAdv2.Nodes[0].Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(0, 255, 255, 255));
                foreach (TreeNodeAdv item in this.treeViewAdv2.Nodes[0].Nodes)
                {
                    item.Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(0, 255, 255, 255));
                }
                if (this.treeViewAdv2.GetNodeAtPoint(new Point(e.X, e.Y)) != null)
                {
                    TreeNodeAdv node = this.treeViewAdv2.GetNodeAtPoint(new Point(e.X, e.Y));
                    node.Background = new Syncfusion.Drawing.BrushInfo(Color.FromArgb(205, 230, 247));
                }
            }
        }

        void treeViewAdv1_MouseUp(object sender, MouseEventArgs e)
        {
            //if ((sender as TreeViewAdv) == this.treeViewAdv1)
            //{
            //    if (this.treeViewAdv1.Nodes[0] != this.treeViewAdv1.SelectedNode)
            //        this.treeViewAdv1.Nodes[0].Font = new Font(this.treeViewAdv1.Nodes[0].Font.FontFamily, this.treeViewAdv1.Nodes[0].Font.Size, FontStyle.Regular);
            //    foreach (TreeNodeAdv item in this.treeViewAdv1.Nodes[0].Nodes)
            //    {
            //        if (item != this.treeViewAdv1.SelectedNode)
            //        {
            //            item.Font = new Font(item.Font.FontFamily, item.Font.Size, FontStyle.Regular);
            //        }
            //    }
            //}
            if ((sender as TreeViewAdv) == this.treeViewAdv2)
            {
                if (this.treeViewAdv2.Nodes[0] != this.treeViewAdv2.SelectedNode)
                    this.treeViewAdv2.Nodes[0].Font = new Font(this.treeViewAdv2.Nodes[0].Font.FontFamily, this.treeViewAdv2.Nodes[0].Font.Size, FontStyle.Regular);
                foreach (TreeNodeAdv item in this.treeViewAdv2.Nodes[0].Nodes)
                {
                    if (item != this.treeViewAdv2.SelectedNode)
                    {
                        item.Font = new Font(item.Font.FontFamily, item.Font.Size, FontStyle.Regular);
                    }
                }
            }
        }
        bool emptyData = false;
        bool sentItems = false;
        private void treeViewAdv1_NodeMouseClick(object sender, TreeViewAdvMouseClickEventArgs e)
        {
            sentItems = false;
            if (e.Node.Text == "Sent Items")
            {
                emptyData = false;
                sentItems = true;
            }
            if (e.Node.Text == "Deleted Items" || e.Node.Text == "Junk E-Mail" || e.Node.Text == "Outbox" ||
                e.Node.Text == "Drafts")
            {
                emptyData = true;
            }
            else if (e.Node.Text == "Inbox")
            {
                emptyData = false;
            }
            this.gridGroupingControl1.TableControl.Refresh();
            e.Node.Font = new Font(e.Node.Font.FontFamily, e.Node.Font.Size, FontStyle.Bold);
        }

        

        private void gridGroupingControl1_TableControlCellClick(object sender, Syncfusion.Windows.Forms.Grid.Grouping.GridTableControlCellClickEventArgs e)
        {
            if (e.Inner.ColIndex == 1)
            {
                this.gridGroupingControl1.TableModel[e.Inner.RowIndex, e.Inner.ColIndex + 1].ValueMember = e.Inner.RowIndex.ToString();
            }
            //use the Nested display elements to know the selection is of record or caption
            int row = 0;
            row = e.Inner.RowIndex;
            this.MessageRichTextBox.Clear();
            this.panel5.Visible = true;
            if (this.gridGroupingControl1.Table.NestedDisplayElements.Count > e.Inner.RowIndex)
            {
                // If the selection is of caption hide the reading pane
                if (this.gridGroupingControl1.Table.NestedDisplayElements[row].IsCaption())
                {
                    this.panel5.Visible = false;
                    this.MessageRichTextBox.Clear();
                }
                if (this.gridGroupingControl1.Table.NestedDisplayElements[row].IsRecord())
                {
                    Record rec = this.gridGroupingControl1.Table.CurrentRecord;
                    this.DateTimeLabel.Text = DateTime.Now.ToString();
                    this.Tolabel.Text = rec["To"].ToString();
                    this.Maillabel.Text = rec["ContactName"].ToString();
                    string rtf2 =
                        @"{\rtf1\ansi" +
                        // font table
                        @"{\fonttbl" +
                        @"\f0\fswiss Segoe UI;}" +
                        @"\highlight\ql\f0\f0\fs20   " + "\\plain\\par" +
                        @"\highlight1\ql\cf0\f0\fs20 {    Hi }" + rec["To"] + "," + "\\plain\\par" + Environment.NewLine +
                        @"\highlight1\ql\f0\f2\fs20 " + "\\plain\\par" +
                        // third line
                        @"\highlight1\ql\cf0\f0\fs20     " + rec["Message"] + "." + "\\plain\\par" +
                        @"\highlight\ql\f0\f0\fs20   " + "\\plain\\par" +
                        @"\highlight1\ql\cf0\f0\fs20     " + "Thanks" + "," + "\\plain\\par" +
                        @"\highlight1\ql\cf0\f0\fs20     " + rec["ContactName"] + "." + "\\plain\\par" +
                        // closing bracket
                        @"}";

                    // Use display to show the content..
                    this.MessageRichTextBox.Rtf = rtf2;
                }
            }
        }

        private void backStageButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.ribbonControlAdv1.TouchMode = true;
        }

        private void unreadlabel_MouseDown(object sender, MouseEventArgs e)
        {
            //Alllabel.ForeColor = Color.Black;
            (sender as Label).ForeColor = Color.FromArgb(58, 187, 246);
        }

        private void unreadlabel_MouseEnter(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.FromArgb(58, 187, 246);
        }

        private void unreadlabel_MouseLeave(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.Black;
        }

        private void Alllabel_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void Alllabel_MouseEnter(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.FromArgb(58, 187, 246);
        }

        
        private void pointerControl2_Click(object sender, EventArgs e)
        {
            this.splitContainerAdv1.Panel1Collapsed = false;
            this.HiddenPanel.Visible = false;            
            this.splitContainerAdv1.SplitterDistance = 226;
        }

        private void pointerControl1_Click_2(object sender, EventArgs e)
        {          
            this.HiddenPanel.Visible = true;        
            this.splitContainerAdv1.SplitterDistance = this.HiddenPanel.Width;      
        }

        private void HiddenPanel_MouseDown(object sender, MouseEventArgs e)
        {           
            this.HiddenPanel.Visible = false;       
            this.splitContainerAdv1.SplitterDistance = 226;
        }
    }
}
