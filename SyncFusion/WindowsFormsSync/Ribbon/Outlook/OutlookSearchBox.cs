#region Copyright Syncfusion Inc. 2001 - 2016
// Copyright Syncfusion Inc. 2001 - 2016. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OutlookDemo_2010
{
    public partial class OutlookSearchBox : UserControl
    {
        bool isLabel1Hit = false;
        bool isLabel2Hit = false;
        public OutlookSearchBox()
        {
            InitializeComponent();
           
            this.buttonEdit2.TextBox.Text = "By Date";
            this.buttonEdit3.TextBox.Text = "Newest";
            this.buttonEdit1.TextBox.ForeColor = Color.FromArgb(68, 68, 68);
            this.buttonEdit2.TextBox.ForeColor = Color.FromArgb(68, 68, 68);
            this.buttonEdit3.TextBox.ForeColor = Color.FromArgb(68, 68, 68);
            this.buttonEditChildButton1.BackColor = Color.White;
            this.buttonEditChildButton2.BackColor = Color.White;
            this.buttonEditChildButton3.BackColor = Color.White;
            this.buttonEdit2.MetroColor = Color.White;
            this.buttonEdit3.MetroColor = Color.White;
            this.buttonEdit2.TextBox.BackColor = Color.White;
            this.buttonEdit3.TextBox.BackColor = Color.White;
            this.buttonEdit3.TextBox.Enabled = false;
            this.buttonEdit2.TextBox.Enabled = false;
            this.label1.ForeColor = Color.FromArgb(58, 187, 246);
            this.Invalidate();
            this.buttonEdit1.TextBox.ForeColor = Color.Gray;
            this.comboBoxAdv1.ForeColor = Color.FromArgb(68, 68, 68);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            using (Graphics g = this.CreateGraphics())
            {
                if (g.DpiX > 96)
                {
                    this.buttonEdit2.Location = new Point(this.buttonEdit2.Location.X + 40, this.buttonEdit2.Location.Y);
                    this.buttonEdit3.Location = new Point(this.buttonEdit3.Location.X + 40, this.buttonEdit3.Location.Y);
                }
            }
        }


        private string m_SearchString = string.Empty;

        public string SearchString
        {
            get { return m_SearchString; }
            set { m_SearchString = value; }
        }



        private bool m_Label1Clicked = false;

        public bool Label1Clicked
        {
            get { return m_Label1Clicked; }
            set
            {
                m_Label1Clicked = value;
                label1.ForeColor = Color.FromArgb(58, 187, 246);
                label2.ForeColor = Color.FromArgb(68, 68, 68);
            }
        }

        private bool m_Label2Clicked = false;
        public bool Label2Clicked
        {
            get { return m_Label2Clicked; }
            set
            {
                m_Label2Clicked = value;
                label1.ForeColor = Color.FromArgb(68, 68, 68);
                label2.ForeColor = Color.FromArgb(58, 187, 246);
            }
        }

        // All

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.FromArgb(58, 187, 246);
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            if (!this.Label1Clicked)
                (sender as Label).ForeColor = Color.FromArgb(68, 68, 68);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Label2Clicked = false;
            Label1Clicked = true;
        }
        
        // Unread

        private void label2_Click(object sender, EventArgs e)
        {
            Label1Clicked = false;
            Label2Clicked = true;
        }

        private void label2_MouseEnter(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.FromArgb(58, 187, 246);
        }

        private void label2_MouseLeave(object sender, EventArgs e)
        {
            if (!Label2Clicked)
                (sender as Label).ForeColor = Color.FromArgb(68, 68, 68);
        }
    }
}
