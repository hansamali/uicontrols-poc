﻿#pragma checksum "..\..\TimeConsumingView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "02DE6E4C5B251A10CD22A94187FAB458"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SampleLayout.WPF;
using Syncfusion;
using Syncfusion.Windows;
using Syncfusion.Windows.Chart;
using Syncfusion.Windows.Shared;
using Syncfusion.Windows.Tools;
using Syncfusion.Windows.Tools.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using TabControlExtDemo;


namespace TabControlExtDemo {
    
    
    /// <summary>
    /// TimeConsumingView
    /// </summary>
    public partial class TimeConsumingView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\TimeConsumingView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox reload;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\TimeConsumingView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Syncfusion.Windows.Chart.Chart Chart1;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\TimeConsumingView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Syncfusion.Windows.Chart.ChartSeries series1;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\TimeConsumingView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Syncfusion.Windows.Chart.ChartSeries series2;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\TimeConsumingView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Syncfusion.Windows.Chart.Chart Chart2;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\TimeConsumingView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Syncfusion.Windows.Chart.ChartSeries series3;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\TimeConsumingView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Syncfusion.Windows.Chart.ChartSeries series4;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TabControlExtDemo;component/timeconsumingview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\TimeConsumingView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.reload = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 2:
            this.Chart1 = ((Syncfusion.Windows.Chart.Chart)(target));
            return;
            case 3:
            this.series1 = ((Syncfusion.Windows.Chart.ChartSeries)(target));
            return;
            case 4:
            this.series2 = ((Syncfusion.Windows.Chart.ChartSeries)(target));
            return;
            case 5:
            this.Chart2 = ((Syncfusion.Windows.Chart.Chart)(target));
            return;
            case 6:
            this.series3 = ((Syncfusion.Windows.Chart.ChartSeries)(target));
            return;
            case 7:
            this.series4 = ((Syncfusion.Windows.Chart.ChartSeries)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

