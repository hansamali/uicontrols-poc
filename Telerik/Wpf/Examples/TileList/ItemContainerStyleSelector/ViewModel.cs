﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Telerik.Windows.Examples.TileList.ItemContainerStyleSelector
{
	public class ViewModel
	{
		Northwind northwind;
		public Northwind Northwind
		{
			get
			{
				if (northwind == null)
				{
					northwind = new Northwind();
				}

				return northwind;
			}
		}

		IEnumerable<Foods> foods;
		public IEnumerable<Foods> Foods
		{
			get
			{
				if (foods == null)
				{
					foods = this.Northwind.FoodsCollection;
				}

				return foods;
			}
		}
	}
}
