﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace Telerik.Windows.Examples.TileList.ItemContainerStyleSelector
{
	public class TileStyleSelector : StyleSelector
	{
		public override Style SelectStyle(object item, System.Windows.DependencyObject container)
		{
			Foods food = item as Foods;
			switch (food.FoodCategory)
			{
				case "DISHES":
					return this.DishesCategoryStyle;
				case "SEAFOOD":
					return this.SeafoodCategoryStyle;
				case "BEVERAGES":
					return this.BeveragesStyle;
				default:
					return this.DishesCategoryStyle;
			}
		}
		public Style DishesCategoryStyle { get; set; }
		public Style SeafoodCategoryStyle { get; set; }
		public Style BeveragesStyle { get; set; }
	}
}
