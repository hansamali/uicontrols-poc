﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.QuickStart.Common.Helpers;
using Telerik.Windows.Examples.TileList.Selection;

namespace Telerik.Windows.Examples
{
	public class TileListExample : UserControl
	{
		public TileListExample()
		{
			this.Loaded += this.OnLoaded;
		}

		protected virtual void OnLoaded(object sender, RoutedEventArgs e)
		{
			if (ConfigurationPanel != null)
			{
				ConfigurationPanel.DataContext = this.ChildrenOfType<RadTileList>().FirstOrDefault();
			}
		}

		protected Panel ConfigurationPanel
		{
			get
			{
				return Telerik.Windows.Controls.QuickStart.QuickStart.GetConfigurationPanel(this);
			}
		}
	}
}
