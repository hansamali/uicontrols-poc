﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Telerik.Windows.Examples.TileList.BindingItemsSource
{
    public class ViewModel
    {
        Northwind northwind;
        public Northwind Northwind
        {
            get
            {
                if (northwind == null)
                {
                    northwind = new Northwind();
                }

                return northwind;
            }
        }

        IEnumerable<Employee> employees;
        public IEnumerable<Employee> Employees
        {
            get
            {
                if (employees == null)
                {
                    employees = this.Northwind.EmployeesCollection;
                }

                return employees;
            }
        }

        IEnumerable<TileReorderMode> reorderModes;

        public IEnumerable<TileReorderMode> ReorderModes
        {
            get
            {
                if (this.reorderModes == null)
                {
                    this.reorderModes = new List<TileReorderMode>()
					{ 
						TileReorderMode.BetweenGroups,
                        TileReorderMode.InGroup, 
                        TileReorderMode.None
					};
                }

                return this.reorderModes;
            }
        }

        IEnumerable<Visibility> visibilityModes;

        public IEnumerable<Visibility> VisibilityModes
        {
            get
            {
                if (this.visibilityModes == null)
                {
                    this.visibilityModes = new List<Visibility>()
					{ 
						Visibility.Visible,
                        Visibility.Collapsed
					};
                }

                return this.visibilityModes;
            }
        }

        IEnumerable<SelectionMouseButton> mouseButtonSelectionModes;

        public IEnumerable<SelectionMouseButton> MouseButtonSelectionModes
        {
            get
            {
                if (this.mouseButtonSelectionModes == null)
                {
                    this.mouseButtonSelectionModes = new List<SelectionMouseButton>()
					{ 
						SelectionMouseButton.Left,
                        SelectionMouseButton.Right,
                        SelectionMouseButton.Left | SelectionMouseButton.Right
					};
                }

                return this.mouseButtonSelectionModes;
            }
        }

        IEnumerable<VerticalAlignment> verticalTilesAlignments;

        public IEnumerable<VerticalAlignment> VerticalTilesAlignments
        {
            get
            {
                if (this.verticalTilesAlignments == null)
                {
                    this.verticalTilesAlignments = new List<VerticalAlignment>()
					{ 
						VerticalAlignment.Center,
                        VerticalAlignment.Stretch,
                         VerticalAlignment.Top,
                        VerticalAlignment.Bottom                                              
					};
                }

                return this.verticalTilesAlignments;
            }
        }
    }
}
