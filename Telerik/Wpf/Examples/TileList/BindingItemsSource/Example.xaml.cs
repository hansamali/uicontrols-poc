﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace Telerik.Windows.Examples.TileList.BindingItemsSource
{
    public partial class Example : UserControl
    {
        public Example()
        {
            InitializeComponent();           
        }

        private void OnAutoGeneratingTile(object sender, AutoGeneratingTileEventArgs e)
        {
            var employeePosition = (e.Tile.DataContext as Employee).Title;

            switch (employeePosition)
            {
                case "Sales Representative":
                    e.Tile.Group.DisplayIndex = 0;
                    break;                         
                case "Sales Manager":
                    e.Tile.Group.DisplayIndex = 1;
                    break;
                case "Vice President, Sales":
                    e.Tile.Group.DisplayIndex = 2;
                    break;

            }
        }
    }
}