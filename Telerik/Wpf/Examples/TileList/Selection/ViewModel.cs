﻿using System;
using System.Collections.Generic;
#if WPF
using System.Windows.Controls;
#endif
#if SILVERLIGHT
using Telerik.Windows.Controls;
#endif
namespace Telerik.Windows.Examples.TileList.Selection
{
	public class ViewModel
	{
		IEnumerable<SelectionMode> modes;

		public IEnumerable<SelectionMode> Modes
		{
			get
			{
				if (modes == null)
				{
					modes = new List<SelectionMode>()
					{ 
						SelectionMode.Single,
						SelectionMode.Multiple, 
						SelectionMode.Extended 
					};
				}

				return modes;
			}
		}

		Northwind northwind;
        public Northwind Northwind
        {
            get
            {
                if (northwind == null)
                {
                    northwind = new Northwind();
                }

                return northwind;
            }
        }

        IEnumerable<Foods> foods;
        public IEnumerable<Foods> Foods
        {
            get
            {
                if (foods == null)
                {
                    foods = this.Northwind.FoodsCollection;
                }

                return foods;
            }
        }
	}
}
