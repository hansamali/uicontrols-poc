﻿using System.Windows;
using System.Windows.Controls;

namespace Telerik.Windows.Examples.LayoutControl.Theming
{
    public partial class Example : UserControl
    {
        public Example()
        {
            this.InitializeComponent();
			this.toolBox.LayoutControl = this.xLayoutControl;
			this.Unloaded += this.Example_Unloaded;
		}

		private void Example_Unloaded(object sender, RoutedEventArgs e)
		{
			this.toolBox.IsOpen = false;
		}
	}
}
