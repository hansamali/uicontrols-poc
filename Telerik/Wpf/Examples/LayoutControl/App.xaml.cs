﻿using System;
using System.Windows;

namespace Telerik.Windows.Examples.LayoutControl
{
    public partial class App : Application
    {
        public App()
        {
            this.StartupUri = new Uri("FirstLook/Example.xaml", UriKind.Relative);
        }
    }
}
