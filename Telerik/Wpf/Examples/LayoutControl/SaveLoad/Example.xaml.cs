﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.LayoutControl;
using Telerik.Windows.DragDrop;

namespace Telerik.Windows.Examples.LayoutControl.SaveLoad
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Example : UserControl
    {
        private string savedXMLString;
        Dictionary<string, FrameworkElement> removedElements = new Dictionary<string, FrameworkElement>();

        public Example()
        {
            InitializeComponent();
            this.layoutControl.ElementLoading += this.LayoutControlElementLoading;
            this.layoutControl.ElementSaving += this.LayoutControlElementSaving;
			this.toolBox.LayoutControl = this.layoutControl;
			this.Unloaded += this.Example_Unloaded;
		}

		private void Example_Unloaded(object sender, RoutedEventArgs e)
		{
			this.toolBox.IsOpen = false;
		}

		private void LayoutControlElementSaving(object sender, Controls.LayoutControl.Serialization.LayoutElementSavingEventArgs e)
        {
            var groupBase = e.Element as LayoutControlGroupBase;
            if (groupBase != null && string.IsNullOrEmpty(RadLayoutControl.GetSerializationId(groupBase)))
            {
                // Set automatic id to groups which do not have serialization id.
                RadLayoutControl.SetSerializationId(groupBase, "group_" + groupBase.GetHashCode());
            }
        }

        private void LayoutControlElementLoading(object sender, Controls.LayoutControl.Serialization.LayoutElementLoadingEventArgs e)
        {
            if (e.Element == null)
            {
                string id = e.Info["SerializationId"] as string;
                if (!string.IsNullOrEmpty(id) && removedElements.ContainsKey(id))
                {
                    e.Element = removedElements[id];
                }
            }
        }

        private void LayoutControlSelectionChanged(object sender, LayoutControlSelectionChangedEventArgs e)
        {
            if (e.OldItem != null)
            {
                FrameworkElement element = e.OldItem as FrameworkElement;
                if (element != null && element.Parent == null)
                {
                    // Storing references to the deleted items.
                    this.StoreReferencesToDeletedElements(element);
                }
            }

            bool isDragDropEnabled = !(e.NewItem is UserControl) && 
                                     !(e.NewItem is LayoutControlTabGroup) &&
                                     !(e.NewItem is LayoutControlTabGroupItem);
            DragDropManager.SetAllowDrag(this.layoutControl, isDragDropEnabled);
        }

        /// <summary>
        /// This is done in order to support saving, deleting and later restoring of deleted groups / usercontrols.
        /// If new application session is opened between save and load, other approach should be used - for example code to recreate the groups / usercontrols.
        /// </summary>
        /// <param name="element"></param>
        private void StoreReferencesToDeletedElements(FrameworkElement element)
        {
            // Element is deleted via manipulation control. We save reference to it for further restoring on Load.
            // We also need to store all ids of the child elements deleted with their parent.
            string id = RadLayoutControl.GetSerializationId(element);
            if (!string.IsNullOrEmpty(id))
            {
                this.removedElements[id] = element;
            }

            LayoutControlGroupBase group = element as LayoutControlGroupBase;
            if (group != null)
            {
                group.Items.OfType<FrameworkElement>().ToList().ForEach(x => this.StoreReferencesToDeletedElements(x));
                group.Items.Clear();
            }
        }

        private void OnSaveButtonClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.savedXMLString = this.layoutControl.SaveToXmlString();
        }

        private void OnLoadButtonClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.layoutControl.LoadFromXmlString(this.savedXMLString);
            this.removedElements.Clear();
        }
    }
}
