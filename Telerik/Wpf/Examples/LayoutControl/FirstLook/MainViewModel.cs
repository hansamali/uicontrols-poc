﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using Telerik.Windows.Controls.Map;
using Telerik.Windows.Examples.LayoutControl.FirstLook.Models;

namespace Telerik.Windows.Examples.LayoutControl.FirstLook
{
    public class MainViewModel
    {
        //first tab data
        public Person PersonData { get; set; }

        public ObservableCollection<DailyActivity> ActivityData { get; set; }

        //second tab data
        public DailyCaloriesItem FirstDayCaloriesData { get; set; }

        public DailyCaloriesItem SecondDayCaloriesData { get; set; }

        //third taba data 
        public ObservableCollection<RunningItem> RunningData { get; set; }

        public MainViewModel()
        {
            this.FillPersonData();

            this.ActivityData = new ObservableCollection<DailyActivity>();
            this.FillActivityData();

            this.FillDailyCaloriesData();

            this.RunningData = new ObservableCollection<RunningItem>();
            this.FillRunningData();
        }

        private void FillPersonData()
        {
            this.PersonData = new Person()
            {
                Name = "Samantha Johnson Smith",
                Age = 27,
                Birthday = "September 24",
                Height = 177,
                Weight = 65,
                Allergies = "Peanuts, Pets, Soy",
                ActivityLevel = "Medium",
                Country = "USA",
                City = "Boston",
                Region = "Boston",
                Street = "LambertAvenue 45",
                PostCode = 13548,
                Phone = "555 666 888",
                AvatarPath = "../Images/avatar.png"
            };
        }

        private void FillActivityData()
        {
            this.ActivityData.Add(new DailyActivity()
            {
                Activity = new ObservableCollection<Activitytem>()
                 {
                     new Activitytem()
                     {
                          Steps = 7458,
                          Label = new Label()
                          {
                              Steps = "7458 steps today",
                              Date = "February 12"
                          },
                          StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD4DF32"))
                     },
                     new Activitytem()
                     {
                         Steps = 4200,
                         Label = new Label(),
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6D4D4"))
                     }
                 }
            });
            this.ActivityData.Add(new DailyActivity()
            {
                Activity = new ObservableCollection<Activitytem>()
                 {
                     new Activitytem()
                     {
                          Steps = 9235,
                          Label = new Label()
                          {
                              Steps = "9235 steps today",
                              Date = "February 11"
                          },
                          StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF8EC441"))
                     },
                     new Activitytem()
                     {
                         Steps = 4200,
                         Label = new Label(),
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6D4D4"))
                     },
                 }
            });
            this.ActivityData.Add(new DailyActivity()
            {
                Activity = new ObservableCollection<Activitytem>()
                 {
                     new Activitytem()
                     {
                         Steps = 2657,
                         Label = new Label()
                         {
                             Steps = "2657 steps today",
                             Date = "February 10"
                         },
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFF59700"))
                     },
                     new Activitytem()
                     {
                         Steps = 10200,
                         Label = new Label(),
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6D4D4"))
                     }
                 }
            });
            this.ActivityData.Add(new DailyActivity()
            {
                Activity = new ObservableCollection<Activitytem>()
                 {
                     new Activitytem()
                     {
                         Steps = 11234,
                         Label = new Label()
                         {
                             Steps = "11234 steps today",
                             Date = "February 09"
                         },
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFF59700"))
                     },
                     new Activitytem()
                     {
                         Steps = 1800,
                         Label = new Label(),
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6D4D4"))
                     }
                 }
            });
            this.ActivityData.Add(new DailyActivity()
            {
                Activity = new ObservableCollection<Activitytem>()
                 {
                     new Activitytem()
                     {
                         Steps = 7458,
                         Label = new Label()
                         {
                             Steps = "7458 steps today",
                             Date = "February 08"
                         },
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFF59700"))
                     },
                     new Activitytem()
                     {
                         Steps = 4200,
                         Label = new Label(),
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6D4D4"))
                     }
                 }
            });
            this.ActivityData.Add(new DailyActivity()
            {
                Activity = new ObservableCollection<Activitytem>()
                 {
                     new Activitytem()
                     {
                         Steps = 7470,
                         Label = new Label()
                         {
                             Steps = "7470 steps today",
                             Date = "February 07"
                         },
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFF59700"))
                     },
                     new Activitytem()
                     {
                         Steps = 4200,
                         Label = new Label(),
                         StepsFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6D4D4"))
                     }
                 }
            });
        }

        private void FillDailyCaloriesData()
        {
            this.FirstDayCaloriesData = new DailyCaloriesItem()
            {
                Date = "February 12",
                Calories = 1790,
                MaxCalories = 1900,
                MaxScaleValue = 2300,
                MaxCaloriesFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6D4D4")),
                Images = new ObservableCollection<DailyCaloriesImageItem>()
                {
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal1.png",
                        Calories = 300,
                        Time = "08:25"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal2.png",
                        Calories = 415,
                        Time = "12:00"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal3.png",
                        Calories = 120,
                        Time = "12:00"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal4.png",
                        Calories = 235,
                        Time = "16:30"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal5.png",
                        Calories = 520,
                        Time = "21:15"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal6.png",
                        Calories = 200,
                        Time = "21:15"
                    }
                }
            };

            this.SecondDayCaloriesData = new DailyCaloriesItem()
            {
                Date = "February 11",
                Calories = 1830,
                MaxCalories = 1900,
                MaxScaleValue = 2300,
                MaxCaloriesFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6D4D4")),
                Images = new ObservableCollection<DailyCaloriesImageItem>()
                {
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal7.png",
                        Calories = 350                     ,
                        Time = "08:15"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal8.png",
                        Calories = 175,
                        Time = "12:00"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal9.png",
                        Calories = 430,
                        Time = "12:00"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal10.png",
                        Calories = 200,
                        Time = "16:30"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal11.png",
                        Calories = 475,
                        Time = "20:30"
                    },
                    new DailyCaloriesImageItem()
                    {
                        ImageUri = "../../Images/meal12.png",
                        Calories = 200,
                        Time = "20:30"
                    }
                }
            };
        }

        private void FillRunningData()
        {
            this.RunningData.Add(new RunningItem()
            {
                Date = "February 12",
                Distance = 6.45,
                Time = "54:36",
                Calories = 415,
                Pace = "6:15",
                Walking = "8:36",
                StartLocation = new Location(40.791780924978923, -73.962836265564093),
                EndLocation = new Location(40.78895417167741, -73.956313133239874)
            });
            this.RunningData.Add(new RunningItem()
            {
                Date = "February 11",
                Distance = 11.05,
                Time = "54:36",
                Calories = 415,
                Pace = "6:15",
                Walking = "8:36",
                StartLocation = new Location(40.787167081643332, -73.956313133239874),
                EndLocation = new Location(40.784210154336236, -73.958630561828741)
            });
            this.RunningData.Add(new RunningItem()
            {
                Date = "February 10",
                Distance = 5.13,
                Time = "54:36",
                Calories = 415,
                Pace = "6:15",
                Walking = "8:36",
                StartLocation = new Location(40.785412437365679, -73.9533090591432),
                EndLocation = new Location(40.782617908177741, -73.955454826355108)
            });
            this.RunningData.Add(new RunningItem()
            {
                Date = "February 09",
                Distance = 6.08,
                Time = "54:36",
                Calories = 415,
                Pace = "6:15",
                Walking = "8:36",
                StartLocation = new Location(40.781253095374595, -73.960003852844366),
                EndLocation = new Location(40.778555882839818, -73.953266143798956)
            });
            this.RunningData.Add(new RunningItem()
            {
                Date = "February 08",
                Distance = 6.45,
                Time = "54:36",
                Calories = 415,
                Pace = "6:15",
                Walking = "8:36",
                StartLocation = new Location(40.791780924978923, -73.962836265564093),
                EndLocation = new Location(40.78895417167741, -73.956313133239874)
            });
            this.RunningData.Add(new RunningItem()
            {
                Date = "February 07",
                Distance = 11.05,
                Time = "54:36",
                Calories = 415,
                Pace = "6:15",
                Walking = "8:36",
                StartLocation = new Location(40.787167081643332, -73.956313133239874),
                EndLocation = new Location(40.784210154336236, -73.958630561828741)
            });
            this.RunningData.Add(new RunningItem()
            {
                Date = "February 06",
                Distance = 11.05,
                Time = "54:36",
                Calories = 415,
                Pace = "6:15",
                Walking = "8:36",
                StartLocation = new Location(40.787167081643332, -73.956313133239874),
                EndLocation = new Location(40.784210154336236, -73.958630561828741)
            });
            this.RunningData.Add(new RunningItem()
            {
                Date = "February 05",
                Distance = 11.05,
                Time = "54:36",
                Calories = 415,
                Pace = "6:15",
                Walking = "8:36",
                StartLocation = new Location(40.787167081643332, -73.956313133239874),
                EndLocation = new Location(40.784210154336236, -73.958630561828741)
            });
        }
    }
}
