﻿namespace Telerik.Windows.Examples.LayoutControl.FirstLook.Models
{
    public class Person
    {
        public int Age { get; set; }

        public int PostCode { get; set; }

        public int Height { get; set; }

        public int Weight { get; set; }

        public string Name { get; set; }

        public string Birthday { get; set; }

        public string Allergies { get; set; }

        public string ActivityLevel { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string Street { get; set; }

        public string Phone { get; set; }

        public string AvatarPath { get; set; }
    }
}
