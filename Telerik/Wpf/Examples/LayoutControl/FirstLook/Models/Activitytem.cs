﻿using System.Windows.Media;

namespace Telerik.Windows.Examples.LayoutControl.FirstLook.Models
{
    public class Activitytem
    {
        public int Steps { get; set; }

        public Label Label { get; set; }

        public Brush StepsFill { get; set; }
    }
}
