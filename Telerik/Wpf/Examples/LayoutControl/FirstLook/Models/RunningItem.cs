﻿using Telerik.Windows.Controls.Map;

namespace Telerik.Windows.Examples.LayoutControl.FirstLook.Models
{
    public class RunningItem
    {
        public int Calories { get; set; }

        public double Distance { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string Pace { get; set; }

        public string Walking { get; set; }

        public Location StartLocation { get; set; }

        public Location EndLocation { get; set; }
    }
}
