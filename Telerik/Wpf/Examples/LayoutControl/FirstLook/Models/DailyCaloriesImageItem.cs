﻿namespace Telerik.Windows.Examples.LayoutControl.FirstLook.Models
{
    public class DailyCaloriesImageItem
    {
        public string ImageUri { get; set; }

        public string Time { get; set; }

        public int Calories { get; set; }
    }
}
