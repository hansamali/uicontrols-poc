﻿using System.Collections.ObjectModel;

namespace Telerik.Windows.Examples.LayoutControl.FirstLook.Models
{
    public class DailyActivity
    {
        public ObservableCollection<Activitytem> Activity { get; set; }

        public DailyActivity()
        {
            this.Activity = new ObservableCollection<Activitytem>();
        }
    }
}
