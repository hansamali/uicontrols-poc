﻿using System.Collections.ObjectModel;
using System.Windows.Media;

namespace Telerik.Windows.Examples.LayoutControl.FirstLook.Models
{
    public class DailyCaloriesItem
    {
        public DailyCaloriesItem()
        {
            this.Images = new ObservableCollection<DailyCaloriesImageItem>();
        }

        public string Date { get; set; }

        public int Calories { get; set; }

        public int MaxCalories { get; set; }

        public int MaxScaleValue { get; set; }

        public Brush MaxCaloriesFill { get; set; }

        public ObservableCollection<DailyCaloriesImageItem> Images { get; set; }
    }
}
