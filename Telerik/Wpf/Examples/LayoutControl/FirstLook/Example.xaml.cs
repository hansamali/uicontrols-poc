﻿using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.LayoutControl;
using Telerik.Windows.Controls.Map;
using Telerik.Windows.DragDrop;
using Telerik.Windows.Examples.LayoutControl.FirstLook.Models;

namespace Telerik.Windows.Examples.LayoutControl.FirstLook
{
    public partial class Example : UserControl
    {
        private BingRouteProvider routeProvider;
        private LocationCollection currentRouteLocations;

        public Example()
        {
            this.InitializeComponent();

            this.InitializeRoutingComponents();
            this.RunningDataList.SelectedIndex = 0;
			this.toolBox.LayoutControl = this.LayoutControl;
			this.Unloaded += this.Example_Unloaded;
		}

		private void Example_Unloaded(object sender, RoutedEventArgs e)
		{
			this.toolBox.IsOpen = false;
		}

		private void RadMap_Loaded(object sender, RoutedEventArgs e)
        {
            this.BringBestView();
        }

        private void RunningDataList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RunningItem dataContext = this.RunningDataList.SelectedItem as RunningItem;

            if (dataContext != null)
            {
                this.currentRouteLocations.Clear();
                this.currentRouteLocations.Add(dataContext.StartLocation);
                this.currentRouteLocations.Add(dataContext.EndLocation);
                this.FindRoute();
            }
        }

        private void RouteProvider_RoutingCompleted(object sender, RoutingCompletedEventArgs e)
        {
            RouteResponse routeResponse = e.Response as RouteResponse;

            if (routeResponse != null && routeResponse.Error == null)
            {
                if (routeResponse.Result != null && routeResponse.Result.RoutePath != null)
                {
                    PolylineData routeLine = new PolylineData()
                    {
                        Points = routeResponse.Result.RoutePath.Points,
                        ShapeFill = new MapShapeFill()
                        {
                            Stroke = new SolidColorBrush(Colors.Red),
                            StrokeThickness = 2
                        }
                    };

                    this.routeLayer.Items.Add(routeLine);
                    this.BringBestView();
                }
            }
        }

        private void InitializeRoutingComponents()
        {
            this.currentRouteLocations = new LocationCollection();
            this.routeProvider = new BingRouteProvider();
            this.routeProvider.ApplicationId = "AqaPuZWytKRUA8Nm5nqvXHWGL8BDCXvK8onCl2PkC581Zp3T_fYAQBiwIphJbRAK";

            this.routeProvider.MapControl = this.radMap;
            this.routeProvider.RoutingCompleted += this.RouteProvider_RoutingCompleted;

            this.wayPointsLayer.ItemsSource = this.currentRouteLocations;
        }

        private void FindRoute()
        {
            this.routeLayer.Items.Clear();

            RouteRequest routeRequest = new RouteRequest();
            routeRequest.Culture = new CultureInfo("en-US");
            routeRequest.Options.RoutePathType = RoutePathType.Points;

            if (this.currentRouteLocations.Count > 1)
            {
                foreach (Location location in this.currentRouteLocations)
                {
                    routeRequest.Waypoints.Add(location);
                }

                this.routeProvider.CalculateRouteAsync(routeRequest);
            }
        }

        private void BringBestView()
        {
            LocationRect bestViewRect = this.wayPointsLayer.GetBestView(this.routeLayer.Items);
            this.radMap.SetView(bestViewRect);
        }

		private void LayoutControl_SelectionChanged(object sender, Controls.LayoutControl.LayoutControlSelectionChangedEventArgs e)
		{
			var isDragDropEnabled = true;
			var tabgroupItem = e.NewItem as LayoutControlTabGroupItem;
			if (tabgroupItem != null && tabgroupItem.ParentOfType<LayoutControlGroupBase>() == this.RootLayoutTabGroup)
			{
				isDragDropEnabled = false;
			}
			DragDropManager.SetAllowDrag(this.LayoutControl, isDragDropEnabled);
		}
	}
}
