﻿#pragma checksum "..\..\..\..\TopElementPathFraction\Example.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F8786C668FF834E3C6A2769C95372694"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Behaviors;
using Telerik.Windows.Controls.BulletGraph;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.Gauge;
using Telerik.Windows.Controls.HeatMap;
using Telerik.Windows.Controls.LayoutControl;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Map;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.QuickStart;
using Telerik.Windows.Controls.RadialMenu;
using Telerik.Windows.Controls.Sparklines;
using Telerik.Windows.Controls.TimeBar;
using Telerik.Windows.Controls.Timeline;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeMap;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Controls.Wizard;
using Telerik.Windows.Data;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Examples;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace Telerik.Windows.Examples.Carousel.TopElementPathFraction {
    
    
    /// <summary>
    /// Example
    /// </summary>
    public partial class Example : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadCarouselPanel Panel;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.PathStop scaleCenterPoint;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.PathStop opacityLeftPoint;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.PathStop opacityCenterPoint;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.PathStop opacityRightPoint;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadSlider Slider;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MoveForward;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MoveBackward;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NextPage;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\..\..\TopElementPathFraction\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PreviousPage;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Carousel;component/topelementpathfraction/example.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\TopElementPathFraction\Example.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Panel = ((Telerik.Windows.Controls.RadCarouselPanel)(target));
            return;
            case 2:
            this.scaleCenterPoint = ((Telerik.Windows.Controls.PathStop)(target));
            return;
            case 3:
            this.opacityLeftPoint = ((Telerik.Windows.Controls.PathStop)(target));
            return;
            case 4:
            this.opacityCenterPoint = ((Telerik.Windows.Controls.PathStop)(target));
            return;
            case 5:
            this.opacityRightPoint = ((Telerik.Windows.Controls.PathStop)(target));
            return;
            case 6:
            this.Slider = ((Telerik.Windows.Controls.RadSlider)(target));
            
            #line 137 "..\..\..\..\TopElementPathFraction\Example.xaml"
            this.Slider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.Slider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.MoveForward = ((System.Windows.Controls.Button)(target));
            
            #line 142 "..\..\..\..\TopElementPathFraction\Example.xaml"
            this.MoveForward.Click += new System.Windows.RoutedEventHandler(this.MoveForward_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.MoveBackward = ((System.Windows.Controls.Button)(target));
            
            #line 145 "..\..\..\..\TopElementPathFraction\Example.xaml"
            this.MoveBackward.Click += new System.Windows.RoutedEventHandler(this.MoveBackward_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.NextPage = ((System.Windows.Controls.Button)(target));
            
            #line 148 "..\..\..\..\TopElementPathFraction\Example.xaml"
            this.NextPage.Click += new System.Windows.RoutedEventHandler(this.NextPage_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.PreviousPage = ((System.Windows.Controls.Button)(target));
            
            #line 151 "..\..\..\..\TopElementPathFraction\Example.xaml"
            this.PreviousPage.Click += new System.Windows.RoutedEventHandler(this.PreviousPage_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

