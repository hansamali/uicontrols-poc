﻿using System;
using System.Linq;
using System.Windows;

namespace Telerik.Windows.Examples.SpreadStreamProcessing
{
    public partial class App : Application
    {
        public App()
        {
            this.StartupUri = new Uri("/SpreadStreamProcessing;component/LargeDocumentExport/Example.xaml", UriKind.RelativeOrAbsolute);
        }
    }
}
