﻿using System;
using System.Linq;

namespace Telerik.Windows.Examples.SpreadStreamProcessing.LargeDocumentExport
{
    public enum Shipping
    {
        Express,
        OneDay,
        TwoDays,
        Regular
    }
}
