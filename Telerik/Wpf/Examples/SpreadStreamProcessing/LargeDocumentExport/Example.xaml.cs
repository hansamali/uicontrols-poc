﻿using System;
using System.Linq;
using System.Windows.Controls;

namespace Telerik.Windows.Examples.SpreadStreamProcessing.LargeDocumentExport
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Example : UserControl
    {
        public Example()
        {
            InitializeComponent();
        }
    }
}
