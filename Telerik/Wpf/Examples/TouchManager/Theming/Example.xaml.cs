﻿using System.Windows.Controls;
using System.Windows.Media;
using Telerik.Windows.Controls.QuickStart.Common.Helpers;
using Telerik.Windows.Input.Touch;

namespace Telerik.Windows.Examples.TouchManager.Theming
{
    public partial class Example : UserControl
    {
        public Example()
        {
            this.InitializeComponent();
            Input.Touch.TouchManager.AddTapAndHoldEventHandler(this.TouchElement, this.OnTapAndHold);
            this.ApplyThemeSpecificSettings();
        }

        public string CurrentTheme
        {
            get
            {
                return ApplicationThemeManager.GetInstance().CurrentTheme;
            }
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationThemeManager.GetInstance().ThemeChanged += this.Example_ThemeChanged;
        }

        private void UserControl_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationThemeManager.GetInstance().ThemeChanged -= this.Example_ThemeChanged;
        }

        private void Example_ThemeChanged(object sender, System.EventArgs e)
        {
            this.ApplyThemeSpecificSettings();
        }

        private void ApplyThemeSpecificSettings()
        {
            switch (this.CurrentTheme)
            {
                case "Expression_Dark":
                case "VisualStudio2013_Dark":
                case "Green_Dark":
                    this.ConditionTextBlock.Foreground = new SolidColorBrush(Colors.White);
                    this.ButtonPath.Stroke = new SolidColorBrush(Colors.White);
                    break;
                default:
                    this.ConditionTextBlock.Foreground = new SolidColorBrush(Colors.Black);
                    this.ButtonPath.Stroke = new SolidColorBrush(Colors.Black);
                    break;
            }
        }

        private void OnTapAndHold(object sender, TouchEventArgs args)
        {
            
        }
    }
}
