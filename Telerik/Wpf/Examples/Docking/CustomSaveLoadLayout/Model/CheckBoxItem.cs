﻿using Telerik.Windows.Controls;

namespace Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.Model
{
	public class CheckBoxItem : ViewModelBase
	{
		private string itemName;
		private bool shouldPropertyBeSaved;
		private bool shouldPropertyBeLoaded;

		public string ItemName
		{
			get
			{
				return this.itemName;
			}

			set
			{
				if (this.itemName != value)
				{
					this.itemName = value;
					this.OnPropertyChanged(() => this.ItemName);
				}
			}
		}

		public bool ShouldPropertyBeSaved
		{
			get
			{
				return this.shouldPropertyBeSaved;
			}

			set
			{
				if (this.shouldPropertyBeSaved != value)
				{
					this.shouldPropertyBeSaved = value;
					this.OnPropertyChanged(() => this.ShouldPropertyBeSaved);
				}
			}
		}

		public bool ShouldPropertyBeLoaded
		{
			get
			{
				return this.shouldPropertyBeLoaded;
			}

			set
			{
				if (this.shouldPropertyBeLoaded != value)
				{
					this.shouldPropertyBeLoaded = value;
					this.OnPropertyChanged(() => this.ShouldPropertyBeLoaded);
				}
			}
		}
	}
}
