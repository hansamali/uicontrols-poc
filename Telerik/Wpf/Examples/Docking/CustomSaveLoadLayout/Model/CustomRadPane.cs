﻿using Telerik.Windows.Controls;
using System.Windows;

namespace Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.Model
{
	public class CustomRadPane : RadPane
	{
		public static readonly DependencyProperty CustomPropertyProperty =
			DependencyProperty.Register("CustomProperty", typeof(string), typeof(CustomRadPane), new PropertyMetadata(null));

		public string CustomProperty
		{
			get { return (string)GetValue(CustomPropertyProperty); }
			set { SetValue(CustomPropertyProperty, value); }
		}
	}
}
