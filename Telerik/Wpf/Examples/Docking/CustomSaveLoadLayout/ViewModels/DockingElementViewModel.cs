﻿using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.Model;

namespace Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.ViewModels
{
	public abstract class DockingElementViewModel : ViewModelBase
	{
		private bool isExcludedFromLayout;
		private string contentHeader;

		public ObservableCollection<CheckBoxItem> Properties { get; set; }

		public bool IsExcludedFromLayout
		{
			get
			{
				return this.isExcludedFromLayout;
			}
			set
			{
				if (this.isExcludedFromLayout != value)
				{
					this.isExcludedFromLayout = value;
					this.OnPropertyChanged(() => this.IsExcludedFromLayout);
				}
			}
		}

		public string ContentHeader
		{
			get
			{
				return this.contentHeader;
			}

			set
			{
				if (this.contentHeader != value)
				{
					this.contentHeader = value;
					this.OnPropertyChanged(() => this.ContentHeader);
				}
			}
		}
	}
}
