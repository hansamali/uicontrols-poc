﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.Model;

namespace Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.ViewModels
{
	public class PaneViewModel : DockingElementViewModel
	{
		public PaneViewModel(IEnumerable<CheckBoxItem> properties, string header)
		{
			this.Properties = new ObservableCollection<CheckBoxItem>(properties);
			this.ContentHeader = header;
		}
	}
}
