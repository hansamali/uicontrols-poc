﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.Model;

namespace Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.ViewModels
{
	public class PaneGroupViewModel : DockingElementViewModel
	{
		public PaneGroupViewModel(IEnumerable<CheckBoxItem> properties)
		{
			this.Properties = new ObservableCollection<CheckBoxItem>(properties);
			this.ContentHeader = "PANE GROUP PROPERTIES";
		}
	}
}
