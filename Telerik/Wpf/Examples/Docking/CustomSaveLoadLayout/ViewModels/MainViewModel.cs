﻿using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.Model;

namespace Telerik.Windows.Examples.Docking.CustomSaveLoadLayout.ViewModels
{
	public class MainViewModel : ViewModelBase
	{
		private ObservableCollection<CheckBoxItem> paneProperties;
		private ObservableCollection<CheckBoxItem> customPaneProperties;

		public MainViewModel()
		{
			this.paneProperties = this.GeneratePaneProperties();
			this.customPaneProperties = new ObservableCollection<CheckBoxItem>(this.GeneratePaneProperties()) 
			{ 
				new CheckBoxItem(){ ItemName = "CustomProperty" }
			};

			this.PaneViewModel = new PaneViewModel(this.paneProperties, "BOTTOM PANE PROPERTIES");
			this.CustomPaneViewModel = new PaneViewModel(this.customPaneProperties, "CUSTOM PANE PROPERTIES");
			this.PaneGroupViewModel = new PaneGroupViewModel(this.GeneratePaneGroupProperties());
			this.CustomPaneGroupDataViewModel = new PaneGroupViewModel(this.GeneratePaneGroupProperties());
			this.BottomPaneGroupViewModel = new PaneGroupViewModel(this.GeneratePaneGroupProperties());
		}

		public PaneViewModel PaneViewModel { get; set; }
		public PaneViewModel CustomPaneViewModel { get; set; }
		public PaneGroupViewModel PaneGroupViewModel { get; set; }
		public PaneGroupViewModel CustomPaneGroupDataViewModel { get; set; }
		public PaneGroupViewModel BottomPaneGroupViewModel { get; set; }

		public ObservableCollection<CheckBoxItem> GeneratePaneProperties()
		{
			var paneProperties = new ObservableCollection<CheckBoxItem>();

			paneProperties.Add(new CheckBoxItem() { ItemName = "Header", ShouldPropertyBeSaved = true, ShouldPropertyBeLoaded = true });
			paneProperties.Add(new CheckBoxItem() { ItemName = "PaneHeaderVisibility" });
			paneProperties.Add(new CheckBoxItem() { ItemName = "IsPinned", ShouldPropertyBeSaved = true, ShouldPropertyBeLoaded = true });

			return paneProperties;
		}

		public ObservableCollection<CheckBoxItem> GeneratePaneGroupProperties()
		{
			var paneGroupProperties = new ObservableCollection<CheckBoxItem>();

			paneGroupProperties.Add(new CheckBoxItem() { ItemName = "RelativeSize" });
			paneGroupProperties.Add(new CheckBoxItem() { ItemName = "TabStripPlacement" });
			paneGroupProperties.Add(new CheckBoxItem() { ItemName = "SelectedIndex", ShouldPropertyBeSaved = true, ShouldPropertyBeLoaded = true });

			return paneGroupProperties;
		}
	}
}
