﻿using System.Windows.Controls;

namespace Telerik.Windows.Examples.Docking.CustomSaveLoadLayout
{
	/// <summary>
	/// Interaction logic for PaneContent.xaml
	/// </summary>
	public partial class PaneContent : UserControl
	{
		public PaneContent()
		{
			InitializeComponent();
		}
	}
}
