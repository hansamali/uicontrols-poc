﻿using System;
using System.Collections;
using System.IO;
using Telerik.Windows.Controls;

namespace Telerik.Windows.Examples.ChartView3D.Common
{
    public abstract class DataSourceViewModelBase : ViewModelBase
    {
        protected virtual string WpfPath
        {
            get
            {
                return "/ChartView3D;component/DataSources/{0}";
            }
        }

        protected virtual void GetData(string fileName)
        {
            Uri uri = new Uri(string.Format(this.WpfPath, fileName), UriKind.RelativeOrAbsolute);
            System.Windows.Resources.StreamResourceInfo streamInfo = System.Windows.Application.GetResourceStream(uri);
            using (StreamReader fileReader = new StreamReader(streamInfo.Stream))
            {
                this.GetDataCompleted(this.ParseData(fileReader));
            }

        }

        protected abstract void GetDataCompleted(IEnumerable data);

        protected abstract IEnumerable ParseData(TextReader dataReader);
    }
}

