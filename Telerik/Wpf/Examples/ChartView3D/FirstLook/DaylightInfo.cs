﻿using System;

namespace Telerik.Windows.Examples.ChartView3D.FirstLook
{
    public class DaylightInfo
    {
        public double DayLengthInHours { get; set; }
        public string Latitude { get; set; }
        public DateTime Date { get; set; }
    }
}
