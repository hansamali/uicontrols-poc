﻿using System;
using System.Linq;
using System.Windows.Data;
using System.Windows.Media;

namespace Telerik.Windows.Examples.ChartView3D.FirstLook
{
    public class GradientStopCollectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            GradientStopCollection absoluteStops = (GradientStopCollection)value;
            GradientStopCollection relativeStops = new GradientStopCollection();

            double min = absoluteStops.Min(x => x.Offset);
            double max = absoluteStops.Max(x => x.Offset);
            double range = max - min;

            foreach (GradientStop stop in absoluteStops)
            {                
                double normalizedPosition = (stop.Offset - min) / range;
                relativeStops.Add(new GradientStop(stop.Color, normalizedPosition));
            }

            return relativeStops;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
