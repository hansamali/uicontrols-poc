﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Telerik.Charting;
using Telerik.Windows.Controls.ChartView;

namespace Telerik.Windows.Examples.ChartView3D.FirstLook
{
    public class CustomDefaultMaterialSelector : MaterialSelector
    {
        private static SpecularMaterial SpecularMaterial;
        private Dictionary<Color, Material> colorToMaterialDict = new Dictionary<Color, Material>();

        static CustomDefaultMaterialSelector()
        {
            SolidColorBrush specularMaterialBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#BF444444"));
            SpecularMaterial = new SpecularMaterial(specularMaterialBrush, 30);
            SpecularMaterial.Freeze();
        }

        public GradientStopCollection GradientStops { get; set; }       

        public override Material SelectMaterial(object context)
        {
            var dataPoint = (XyzDataPoint3D)context;
            var dataItem = dataPoint.DataItem as DaylightInfo;
            if (dataItem != null)
            {
                SolidColorBrush brush = GetBrush(dataItem.DayLengthInHours);
                Material material = this.GetOrCreateMaterial(brush);
                return material;
            }

            return base.SelectMaterial(context);
        }
        
        private Material GetOrCreateMaterial(SolidColorBrush brush)
        {
            // Reuse and freeze material for better performance.
            Material material;
            if (!this.colorToMaterialDict.TryGetValue(brush.Color, out material))
            {
                MaterialGroup materialGroup = new MaterialGroup();
                materialGroup.Children.Add(new DiffuseMaterial(brush));
                materialGroup.Children.Add(SpecularMaterial);
                materialGroup.Freeze();

                this.colorToMaterialDict[brush.Color] = materialGroup;
                material = materialGroup;
            }

            return material;
        }

        private SolidColorBrush GetBrush(double absoluteOffset)
        {            
            GradientStop currentStop;
            GradientStop nextStop;

            for (int i = 0; i < this.GradientStops.Count - 1; i++)
            {
                currentStop = this.GradientStops[i];
                nextStop = this.GradientStops[i + 1];
                if ((currentStop.Offset <= absoluteOffset) && (absoluteOffset <= nextStop.Offset))
                {
                    double normalizedPosition = (absoluteOffset - currentStop.Offset) / (nextStop.Offset - currentStop.Offset);
                    Color color = MixColors(currentStop.Color, nextStop.Color, normalizedPosition);
                    return new SolidColorBrush(color);
                }
            }
            return null;
        }

        private static Color MixColors(Color startColor, Color endColor, double position)
        {
            byte alpha = Convert.ToByte(startColor.A + (position * (endColor.A - startColor.A)));
            byte red = Convert.ToByte(startColor.R + (position * (endColor.R - startColor.R)));
            byte green = Convert.ToByte(startColor.G + (position * (endColor.G - startColor.G)));
            byte blue = Convert.ToByte(startColor.B + (position * (endColor.B - startColor.B)));

            return new Color { A = alpha, R = red, G = green, B = blue, };
        }
    }
}
