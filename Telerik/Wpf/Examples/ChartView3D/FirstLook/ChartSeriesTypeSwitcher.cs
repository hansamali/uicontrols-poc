﻿using System.Windows;
using System.Windows.Media;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ChartView;

namespace Telerik.Windows.Examples.ChartView3D.FirstLook
{
    public static class ChartSeriesTypeSwitcher
    {
        private static SurfaceSeries3DValueGradientColorizer SurfaceSeriesColorizer;
        private static CustomDefaultMaterialSelector MaterialSelector;

        public static GradientStopCollection GradientStops;

        static ChartSeriesTypeSwitcher()
        {
            GradientStops = new GradientStopCollection()
            {
                new GradientStop((Color)ColorConverter.ConvertFromString("#007FFF"), 2),
                new GradientStop((Color)ColorConverter.ConvertFromString("#00D4FF"), 4),
                new GradientStop((Color)ColorConverter.ConvertFromString("#00FFFF"), 6),
                new GradientStop((Color)ColorConverter.ConvertFromString("#00FF7F"), 8),
                new GradientStop((Color)ColorConverter.ConvertFromString("#AAFF2A"), 10),
                new GradientStop((Color)ColorConverter.ConvertFromString("#D4FF00"), 12),
                new GradientStop((Color)ColorConverter.ConvertFromString("#FFFF00"), 14),
                new GradientStop((Color)ColorConverter.ConvertFromString("#FFD400"), 16),
                new GradientStop((Color)ColorConverter.ConvertFromString("#FFAA00"), 18),
                new GradientStop((Color)ColorConverter.ConvertFromString("#FF7F00"), 20),
                new GradientStop((Color)ColorConverter.ConvertFromString("#FF5500"), 22),
                new GradientStop((Color)ColorConverter.ConvertFromString("#FF0000"), 24),
            };

            MaterialSelector = new CustomDefaultMaterialSelector() { GradientStops = GradientStops };            
            SurfaceSeriesColorizer = new SurfaceSeries3DValueGradientColorizer() { IsAbsolute = true, GradientStops = GradientStops };
        }

        public static readonly DependencyProperty SeriesTypeProperty = 
            DependencyProperty.RegisterAttached(
            "SeriesType",
            typeof(SeriesType), 
            typeof(ChartSeriesTypeSwitcher),
            new PropertyMetadata(OnSeriesTypeChanged));    

        public static SeriesType GetSeriesType(DependencyObject obj)
        {
            return (SeriesType)obj.GetValue(SeriesTypeProperty);
        }

        public static void SetSeriesType(DependencyObject obj, SeriesType value)
        {
            obj.SetValue(SeriesTypeProperty, value);
        }

        private static void OnSeriesTypeChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            RadCartesianChart3D chart = (RadCartesianChart3D)target;
            chart.Series.Clear();
            if (e.NewValue != null)
            {
                XyzSeries3D series = GetSeries(chart, (SeriesType)e.NewValue);
                chart.Series.Add(series);
            }                
        }

        private static XyzSeries3D GetSeries(RadCartesianChart3D chart, SeriesType seriesType)
        {
            XyzSeries3D series = null;
            switch (seriesType)
            {
                case SeriesType.Bar:
                    series = new BarSeries3D();
                    break;
                case SeriesType.Point:
                    series = new PointSeries3D();
                    break;
                case SeriesType.Surface:
                    series = new SurfaceSeries3D() { Colorizer = SurfaceSeriesColorizer };                    
                    break;
            }               
                        
            series.DefaultVisualMaterialSelector = MaterialSelector;
            series.XValueBinding = new PropertyNameDataPointBinding("Date");
            series.YValueBinding = new PropertyNameDataPointBinding("Latitude");
            series.ZValueBinding = new PropertyNameDataPointBinding("DayLengthInHours");
            series.SetBinding(XyzSeries3D.ItemsSourceProperty, "DaylightInfos");
            
            return series;
        }
    }
}
