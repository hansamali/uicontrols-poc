﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using Telerik.Windows.Controls;
using Telerik.Windows.Examples.ChartView3D.Common;

namespace Telerik.Windows.Examples.ChartView3D.FirstLook
{
    public class DaylightViewModel : DataSourceViewModelBase
    {
        private SeriesType selectedSeriesType;

        public SeriesType SelectedSeriesType
        {
            get
            {
                return this.selectedSeriesType;
            }
            set
            {
                if (this.selectedSeriesType != value)
                {
                    this.selectedSeriesType = value;
                    OnPropertyChanged("SelectedSeriesType");
                }
            }
        }

        public ObservableCollection<DaylightInfo> DaylightInfos 
        { 
            get; 
            set; 
        }

        public DelegateCommand ChangeSelectedSeriesTypeCommand 
        { 
            get; 
            set;
        }

        public DaylightViewModel()
        {
            this.GetData("DaylightData.csv");            
            this.SelectedSeriesType = SeriesType.Surface;
            this.ChangeSelectedSeriesTypeCommand = new DelegateCommand(OnChangeSelectedSeriesTypeExecuted);
        }

        private void OnChangeSelectedSeriesTypeExecuted(object parameter)
        {
            if (parameter != null)
            {
                var type = (SeriesType)parameter;
                this.SelectedSeriesType = type;
            }
        }

        protected override void GetDataCompleted(IEnumerable data)
        {
            this.DaylightInfos = data as ObservableCollection<DaylightInfo>;
        }

        protected override IEnumerable ParseData(TextReader dataReader)
        {
            ObservableCollection<DaylightInfo> chartData = new ObservableCollection<DaylightInfo>();

            string line = String.Empty;
            while ((line = dataReader.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line))
                    continue;

                string[] data = line.Split(',');

                DaylightInfo dataItem = new DaylightInfo();
                dataItem.Latitude = data[0];
                dataItem.Date = DateTime.FromOADate(double.Parse(data[1], CultureInfo.InvariantCulture));
                dataItem.DayLengthInHours = double.Parse(data[2], CultureInfo.InvariantCulture);
                chartData.Add(dataItem);
            }

            return chartData;
        }
    }
}
