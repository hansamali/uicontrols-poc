﻿using System.Collections.Generic;
using System.Windows.Media.Media3D;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ChartView;

namespace Telerik.Windows.Examples.ChartView3D.Appearance
{
    public class PalettesViewModel : ViewModelBase
    {
        private Dictionary<string, MaterialCollection> palettes;
        private MaterialCollection selectedPalette;
        
        public MaterialCollection SelectedPalette
        {
            get
            {
                return this.selectedPalette;
            }
            set
            {
                if (this.selectedPalette != value)
                {
                    this.selectedPalette = value;
                    OnPropertyChanged("SelectedPalette");
                }
            }
        }

        public Dictionary<string, MaterialCollection> Palettes 
        {
            get
            {
                return this.palettes;
            }
        }

        public PalettesViewModel()
        {
            this.palettes = new Dictionary<string, MaterialCollection>();
            this.palettes.Add("Arctic", Chart3DPalettes.Arctic);
            this.palettes.Add("Autumn", Chart3DPalettes.Autumn);
            this.palettes.Add("Cold", Chart3DPalettes.Cold);
            this.palettes.Add("Flower", Chart3DPalettes.Flower);
            this.palettes.Add("Forest", Chart3DPalettes.Forest);
            this.palettes.Add("Grayscale", Chart3DPalettes.Grayscale);
            this.palettes.Add("Green", Chart3DPalettes.Green);
            this.palettes.Add("Ground", Chart3DPalettes.Ground);
            this.palettes.Add("Lilac", Chart3DPalettes.Lilac);
            this.palettes.Add("Natural", Chart3DPalettes.Natural);
            this.palettes.Add("Office2013", Chart3DPalettes.Office2013);
            this.palettes.Add("Office2016", Chart3DPalettes.Office2016);
            this.palettes.Add("Pastel", Chart3DPalettes.Pastel);
            this.palettes.Add("Rainbow", Chart3DPalettes.Rainbow);
            this.palettes.Add("Spring", Chart3DPalettes.Spring);
            this.palettes.Add("Summer", Chart3DPalettes.Summer);
            this.palettes.Add("VS2013", Chart3DPalettes.VisualStudio2013);
            this.palettes.Add("Warm", Chart3DPalettes.Warm);
            this.palettes.Add("Windows8", Chart3DPalettes.Windows8);

            this.SelectedPalette = this.Palettes["Office2016"];
        }
    }
}
