﻿using System.Windows.Controls;
using Telerik.Windows.Controls.Pivot;
using Telerik.Windows.Controls.Spreadsheet.Controls;

namespace Telerik.Windows.Examples.PivotGrid.Selection
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Example : UserControl
    {
        public Example()
        {
            InitializeComponent();

            RadSpreadsheetSheetSelector.SetIsSheetSelectorVisible(this.radSpreadsheet, false);
        }
    }
}
