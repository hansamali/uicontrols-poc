﻿#pragma checksum "..\..\..\..\FirstLook\Example.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "447CC75C038EFDDA039A584D33428AAE"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Behaviors;
using Telerik.Windows.Controls.BulletGraph;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.Gauge;
using Telerik.Windows.Controls.HeatMap;
using Telerik.Windows.Controls.LayoutControl;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Map;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.QuickStart;
using Telerik.Windows.Controls.RadialMenu;
using Telerik.Windows.Controls.Sparklines;
using Telerik.Windows.Controls.TimeBar;
using Telerik.Windows.Controls.Timeline;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeMap;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Controls.Wizard;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Examples.Barcode.Gallery;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace Telerik.Windows.Examples.Barcode.FirstLook {
    
    
    /// <summary>
    /// Example
    /// </summary>
    public partial class Example : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadBook RadBook1;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Code1Grid;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadBarcodeUPCA Barcode1;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Code2Grid;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadBarcodeUPCA Barcode2;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Code3Grid;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadBarcodeUPCA Barcode3;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Code4Grid;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadBarcodeUPCA Barcode4;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Code5Grid;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadBarcodeUPCA Barcode5;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Code6Grid;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadBarcodeUPCA Barcode6;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Barcode;component/firstlook/example.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\FirstLook\Example.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.RadBook1 = ((Telerik.Windows.Controls.RadBook)(target));
            return;
            case 3:
            this.Code1Grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.Barcode1 = ((Telerik.Windows.Controls.RadBarcodeUPCA)(target));
            return;
            case 5:
            this.Code2Grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.Barcode2 = ((Telerik.Windows.Controls.RadBarcodeUPCA)(target));
            return;
            case 7:
            this.Code3Grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.Barcode3 = ((Telerik.Windows.Controls.RadBarcodeUPCA)(target));
            return;
            case 9:
            this.Code4Grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.Barcode4 = ((Telerik.Windows.Controls.RadBarcodeUPCA)(target));
            return;
            case 11:
            this.Code5Grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 12:
            this.Barcode5 = ((Telerik.Windows.Controls.RadBarcodeUPCA)(target));
            return;
            case 13:
            this.Code6Grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 14:
            this.Barcode6 = ((Telerik.Windows.Controls.RadBarcodeUPCA)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

