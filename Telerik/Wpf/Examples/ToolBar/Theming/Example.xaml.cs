using System;
using System.Windows.Media;

namespace Telerik.Windows.Examples.ToolBar.Theming
{
	public partial class Example : System.Windows.Controls.UserControl
	{
		public Example()
		{
			InitializeComponent();
#if SILVERLIGHT
            envelope.Fill = new SolidColorBrush(Colors.White);
#endif
        }        
	}
}
