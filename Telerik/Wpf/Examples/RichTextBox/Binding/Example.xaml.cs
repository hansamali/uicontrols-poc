﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.QuickStart.Common.Helpers;
using Telerik.Windows.Documents.FormatProviders.Xaml;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.Examples.RichTextBox.Extensibility;

namespace Telerik.Windows.Examples.RichTextBox.Binding
{
    public partial class Example : System.Windows.Controls.UserControl
    {
        private const string SampleDocumentPath = "SampleData/RadRichTextBoxOverview.xaml";

        static Example()
        {
            // This method is used only to work around limitations for using MEF in Examples.
            ExamplesMefCatalogManager.ChangeDefaultMefCatalog();
        }

        public Example()
        {
            InitializeComponent();
            this.ApplyThemeSpecificChanges();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            using (Stream stream = Application.GetResourceStream(GetResourceUri(SampleDocumentPath)).Stream)
            {
                this.richTextBox.Document = new XamlFormatProvider().Import(stream);
                this.richTextBox.Document.LayoutMode = DocumentLayoutMode.Flow;
            }
        }

        private Uri GetResourceUri(string resource)
        {
            AssemblyName assemblyName = new AssemblyName(this.GetType().Assembly.FullName);
            string resourcePath = "/" + assemblyName.Name + ";component/" + resource;
            Uri resourceUri = new Uri(resourcePath, UriKind.Relative);
            return resourceUri;
        }

        private void ApplyThemeSpecificChanges()
        {
            this.fontSizeComboBox.Width = ApplicationThemeManager.GetInstance().CurrentTheme == "Windows8Touch" ? 70 : 45;
            IconSources.ChangeIconsSet(IconsSet.Modern);
        }
    }
}