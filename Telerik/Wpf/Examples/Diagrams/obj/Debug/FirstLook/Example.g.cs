﻿#pragma checksum "..\..\..\FirstLook\Example.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "DA9D8BE69F5A43B566832535F87C751B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Charting;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Behaviors;
using Telerik.Windows.Controls.BulletGraph;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.ChartView;
using Telerik.Windows.Controls.Data.PropertyGrid;
using Telerik.Windows.Controls.Diagrams;
using Telerik.Windows.Controls.Diagrams.Extensions;
using Telerik.Windows.Controls.Diagrams.Primitives;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.Gauge;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.HeatMap;
using Telerik.Windows.Controls.LayoutControl;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Map;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.QuickStart;
using Telerik.Windows.Controls.RadialMenu;
using Telerik.Windows.Controls.RibbonView;
using Telerik.Windows.Controls.Sparklines;
using Telerik.Windows.Controls.TimeBar;
using Telerik.Windows.Controls.Timeline;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeMap;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Controls.Wizard;
using Telerik.Windows.Data;
using Telerik.Windows.Diagrams.Core;
using Telerik.Windows.Documents.FormatProviders.Html;
using Telerik.Windows.Documents.FormatProviders.OpenXml.Docx;
using Telerik.Windows.Documents.FormatProviders.Rtf;
using Telerik.Windows.Documents.FormatProviders.Txt;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.Documents.UI;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Examples.Diagrams.Common;
using Telerik.Windows.Examples.Diagrams.FirstLook;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace Telerik.Windows.Examples.Diagrams.FirstLook {
    
    
    /// <summary>
    /// Example
    /// </summary>
    public partial class Example : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Examples.Diagrams.FirstLook.Example root;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonView Ribbon;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox SamplesList;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton NewButton;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton OpenButton;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton SaveButton;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton PasteButton;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton CutButton;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton CopyButton;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton DeleteButton;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonRadioButton TextButton;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonRadioButton PathButton;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonRadioButton PencilButton;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonRadioButton PointerButton;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonRadioButton ConnectionButton;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton GroupButton;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton UngroupButton;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton BackwardButton;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton ForwardButton;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton ToFrontButton;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton ToBackButton;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton AlignLeftButton;
        
        #line default
        #line hidden
        
        
        #line 189 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton AlignBottomButton;
        
        #line default
        #line hidden
        
        
        #line 192 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton AlignTopButton;
        
        #line default
        #line hidden
        
        
        #line 195 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton AlignRightButton;
        
        #line default
        #line hidden
        
        
        #line 200 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton LayoutButton;
        
        #line default
        #line hidden
        
        
        #line 209 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonDropDownButton BackgroundColorButton;
        
        #line default
        #line hidden
        
        
        #line 220 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonToggleButton ShowGridToggle;
        
        #line default
        #line hidden
        
        
        #line 225 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonDropDownButton GridColorButton;
        
        #line default
        #line hidden
        
        
        #line 244 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadNumericUpDown CellWidthSpinner;
        
        #line default
        #line hidden
        
        
        #line 250 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadNumericUpDown CellHeightSpinner;
        
        #line default
        #line hidden
        
        
        #line 292 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadNumericUpDown ZoomSpinner;
        
        #line default
        #line hidden
        
        
        #line 297 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonButton FitToWindowButton;
        
        #line default
        #line hidden
        
        
        #line 313 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRibbonComboBox SelectionModeCombo;
        
        #line default
        #line hidden
        
        
        #line 329 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadDiagramToolbox toolBox;
        
        #line default
        #line hidden
        
        
        #line 330 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadDiagram diagram;
        
        #line default
        #line hidden
        
        
        #line 345 "..\..\..\FirstLook\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadListBox SamplesList1;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Diagrams;component/firstlook/example.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\FirstLook\Example.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.root = ((Telerik.Windows.Examples.Diagrams.FirstLook.Example)(target));
            return;
            case 2:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.Ribbon = ((Telerik.Windows.Controls.RadRibbonView)(target));
            return;
            case 4:
            this.SamplesList = ((System.Windows.Controls.ListBox)(target));
            
            #line 39 "..\..\..\FirstLook\Example.xaml"
            this.SamplesList.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.SamplesList_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.NewButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 6:
            this.OpenButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 7:
            this.SaveButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 8:
            
            #line 62 "..\..\..\FirstLook\Example.xaml"
            ((System.Windows.Controls.ListBox)(target)).SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.UndoSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 71 "..\..\..\FirstLook\Example.xaml"
            ((System.Windows.Controls.ListBox)(target)).SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.RedoSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.PasteButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 11:
            this.CutButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 12:
            this.CopyButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 13:
            this.DeleteButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 14:
            this.TextButton = ((Telerik.Windows.Controls.RadRibbonRadioButton)(target));
            
            #line 108 "..\..\..\FirstLook\Example.xaml"
            this.TextButton.Checked += new System.Windows.RoutedEventHandler(this.OnToolChecked);
            
            #line default
            #line hidden
            return;
            case 15:
            this.PathButton = ((Telerik.Windows.Controls.RadRibbonRadioButton)(target));
            
            #line 112 "..\..\..\FirstLook\Example.xaml"
            this.PathButton.Checked += new System.Windows.RoutedEventHandler(this.OnToolChecked);
            
            #line default
            #line hidden
            return;
            case 16:
            this.PencilButton = ((Telerik.Windows.Controls.RadRibbonRadioButton)(target));
            
            #line 117 "..\..\..\FirstLook\Example.xaml"
            this.PencilButton.Checked += new System.Windows.RoutedEventHandler(this.OnToolChecked);
            
            #line default
            #line hidden
            return;
            case 17:
            this.PointerButton = ((Telerik.Windows.Controls.RadRibbonRadioButton)(target));
            
            #line 122 "..\..\..\FirstLook\Example.xaml"
            this.PointerButton.Checked += new System.Windows.RoutedEventHandler(this.OnToolChecked);
            
            #line default
            #line hidden
            return;
            case 18:
            this.ConnectionButton = ((Telerik.Windows.Controls.RadRibbonRadioButton)(target));
            
            #line 127 "..\..\..\FirstLook\Example.xaml"
            this.ConnectionButton.Checked += new System.Windows.RoutedEventHandler(this.OnToolChecked);
            
            #line default
            #line hidden
            return;
            case 19:
            
            #line 139 "..\..\..\FirstLook\Example.xaml"
            ((Telerik.Windows.Controls.RadRibbonButton)(target)).Click += new System.Windows.RoutedEventHandler(this.OnExportToHtmlClick);
            
            #line default
            #line hidden
            return;
            case 20:
            this.GroupButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 21:
            this.UngroupButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 22:
            this.BackwardButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 23:
            this.ForwardButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 24:
            this.ToFrontButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 25:
            this.ToBackButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 26:
            this.AlignLeftButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 27:
            this.AlignBottomButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 28:
            this.AlignTopButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 29:
            this.AlignRightButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 30:
            this.LayoutButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            
            #line 200 "..\..\..\FirstLook\Example.xaml"
            this.LayoutButton.Click += new System.Windows.RoutedEventHandler(this.LayoutButton_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.BackgroundColorButton = ((Telerik.Windows.Controls.RadRibbonDropDownButton)(target));
            return;
            case 32:
            
            #line 215 "..\..\..\FirstLook\Example.xaml"
            ((Telerik.Windows.Controls.RadColorSelector)(target)).SelectedColorChanged += new System.EventHandler(this.RadColorSelector_SelectedColorChanged);
            
            #line default
            #line hidden
            return;
            case 33:
            this.ShowGridToggle = ((Telerik.Windows.Controls.RadRibbonToggleButton)(target));
            return;
            case 34:
            this.GridColorButton = ((Telerik.Windows.Controls.RadRibbonDropDownButton)(target));
            return;
            case 35:
            
            #line 230 "..\..\..\FirstLook\Example.xaml"
            ((Telerik.Windows.Controls.RadColorSelector)(target)).SelectedColorChanged += new System.EventHandler(this.GridColorSelectorOnSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 36:
            this.CellWidthSpinner = ((Telerik.Windows.Controls.RadNumericUpDown)(target));
            
            #line 246 "..\..\..\FirstLook\Example.xaml"
            this.CellWidthSpinner.ValueChanged += new System.EventHandler<Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs>(this.CellWidthSpinner_OnValueChanged);
            
            #line default
            #line hidden
            return;
            case 37:
            this.CellHeightSpinner = ((Telerik.Windows.Controls.RadNumericUpDown)(target));
            
            #line 252 "..\..\..\FirstLook\Example.xaml"
            this.CellHeightSpinner.ValueChanged += new System.EventHandler<Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs>(this.CellHeightSpinner_OnValueChanged);
            
            #line default
            #line hidden
            return;
            case 38:
            this.ZoomSpinner = ((Telerik.Windows.Controls.RadNumericUpDown)(target));
            
            #line 294 "..\..\..\FirstLook\Example.xaml"
            this.ZoomSpinner.ValueChanged += new System.EventHandler<Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs>(this.ZoomSpinner_OnValueChanged);
            
            #line default
            #line hidden
            return;
            case 39:
            this.FitToWindowButton = ((Telerik.Windows.Controls.RadRibbonButton)(target));
            return;
            case 40:
            this.SelectionModeCombo = ((Telerik.Windows.Controls.RadRibbonComboBox)(target));
            return;
            case 41:
            this.toolBox = ((Telerik.Windows.Controls.Diagrams.Extensions.RadDiagramToolbox)(target));
            return;
            case 42:
            this.diagram = ((Telerik.Windows.Controls.RadDiagram)(target));
            return;
            case 43:
            this.SamplesList1 = ((Telerik.Windows.Controls.RadListBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

