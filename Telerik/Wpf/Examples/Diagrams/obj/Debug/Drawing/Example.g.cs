﻿#pragma checksum "..\..\..\Drawing\Example.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F80AF6EDB9B1A90D55B0BB5FFF05BB68"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Charting;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Behaviors;
using Telerik.Windows.Controls.BulletGraph;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.ChartView;
using Telerik.Windows.Controls.Data.PropertyGrid;
using Telerik.Windows.Controls.Diagrams;
using Telerik.Windows.Controls.Diagrams.Extensions;
using Telerik.Windows.Controls.Diagrams.Primitives;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.Gauge;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.HeatMap;
using Telerik.Windows.Controls.LayoutControl;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Map;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.QuickStart;
using Telerik.Windows.Controls.RadialMenu;
using Telerik.Windows.Controls.RibbonView;
using Telerik.Windows.Controls.Sparklines;
using Telerik.Windows.Controls.TimeBar;
using Telerik.Windows.Controls.Timeline;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeMap;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Controls.Wizard;
using Telerik.Windows.Data;
using Telerik.Windows.Documents.FormatProviders.Html;
using Telerik.Windows.Documents.FormatProviders.OpenXml.Docx;
using Telerik.Windows.Documents.FormatProviders.Rtf;
using Telerik.Windows.Documents.FormatProviders.Txt;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.Documents.UI;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace Telerik.Windows.Examples.Diagrams.Drawing {
    
    
    /// <summary>
    /// Example
    /// </summary>
    public partial class Example : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 21 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox isShapeClosedCheckBox;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadDiagram diagram;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadListBox toolbox;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryRadioButton pointerToolButton;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryRadioButton pathToolButton;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryRadioButton pencilToolButton;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryRadioButton textToolButton;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryDropDownButton strokeButton;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox strokeBrushesGallery;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryDropDownButton fillButton;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox fillBrushesGallery;
        
        #line default
        #line hidden
        
        
        #line 192 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryDropDownButton strokeThicknessButton;
        
        #line default
        #line hidden
        
        
        #line 196 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox strokeThicknessListBox;
        
        #line default
        #line hidden
        
        
        #line 217 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryDropDownButton fillRuleButton;
        
        #line default
        #line hidden
        
        
        #line 220 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox fillRuleListBox;
        
        #line default
        #line hidden
        
        
        #line 310 "..\..\..\Drawing\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryButton importImageButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Diagrams;component/drawing/example.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Drawing\Example.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\..\Drawing\Example.xaml"
            ((Telerik.Windows.Examples.Diagrams.Drawing.Example)(target)).Unloaded += new System.Windows.RoutedEventHandler(this.ExampleUnloaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.isShapeClosedCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 3:
            this.diagram = ((Telerik.Windows.Controls.RadDiagram)(target));
            
            #line 37 "..\..\..\Drawing\Example.xaml"
            this.diagram.ShapeDeserialized += new System.EventHandler<Telerik.Windows.Controls.Diagrams.ShapeSerializationRoutedEventArgs>(this.DiagramShapeDeserialized);
            
            #line default
            #line hidden
            return;
            case 4:
            this.toolbox = ((Telerik.Windows.Controls.RadListBox)(target));
            return;
            case 5:
            this.pointerToolButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryRadioButton)(target));
            
            #line 96 "..\..\..\Drawing\Example.xaml"
            this.pointerToolButton.Click += new System.Windows.RoutedEventHandler(this.PointerToolButtonClick);
            
            #line default
            #line hidden
            return;
            case 6:
            this.pathToolButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryRadioButton)(target));
            
            #line 100 "..\..\..\Drawing\Example.xaml"
            this.pathToolButton.Click += new System.Windows.RoutedEventHandler(this.PathToolButtonClick);
            
            #line default
            #line hidden
            return;
            case 7:
            this.pencilToolButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryRadioButton)(target));
            
            #line 106 "..\..\..\Drawing\Example.xaml"
            this.pencilToolButton.Click += new System.Windows.RoutedEventHandler(this.PencilToolButtonClick);
            
            #line default
            #line hidden
            return;
            case 8:
            this.textToolButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryRadioButton)(target));
            
            #line 111 "..\..\..\Drawing\Example.xaml"
            this.textToolButton.Click += new System.Windows.RoutedEventHandler(this.TextToolButtonClicked);
            
            #line default
            #line hidden
            return;
            case 9:
            this.strokeButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryDropDownButton)(target));
            return;
            case 10:
            this.strokeBrushesGallery = ((System.Windows.Controls.ListBox)(target));
            
            #line 133 "..\..\..\Drawing\Example.xaml"
            this.strokeBrushesGallery.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.StrokeBrushesGalleryMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 11:
            this.fillButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryDropDownButton)(target));
            return;
            case 12:
            this.fillBrushesGallery = ((System.Windows.Controls.ListBox)(target));
            
            #line 170 "..\..\..\Drawing\Example.xaml"
            this.fillBrushesGallery.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.FillBrushesGalleryMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 13:
            this.strokeThicknessButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryDropDownButton)(target));
            return;
            case 14:
            this.strokeThicknessListBox = ((System.Windows.Controls.ListBox)(target));
            
            #line 202 "..\..\..\Drawing\Example.xaml"
            this.strokeThicknessListBox.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.StrokeThicknessListBoxMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 15:
            this.fillRuleButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryDropDownButton)(target));
            return;
            case 16:
            this.fillRuleListBox = ((System.Windows.Controls.ListBox)(target));
            
            #line 225 "..\..\..\Drawing\Example.xaml"
            this.fillRuleListBox.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.FillRuleListBoxMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 17:
            this.importImageButton = ((Telerik.Windows.Controls.Diagrams.Extensions.RadGeometryButton)(target));
            
            #line 310 "..\..\..\Drawing\Example.xaml"
            this.importImageButton.Click += new System.Windows.RoutedEventHandler(this.ImportImageButtonClick);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

