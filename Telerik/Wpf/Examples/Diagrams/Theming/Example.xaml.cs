using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Collections.Generic;
using Telerik.Windows.Controls.QuickStart.Common.Helpers;


namespace Telerik.Windows.Examples.Diagrams.Theming
{
    public partial class Example : UserControl
    {
        private List<string> darkThemes;

        public string CurrentTheme
        {
            get
            {
                return ApplicationThemeManager.GetInstance().CurrentTheme;
            }
        }

        public Example()
        {
            InitializeComponent();
            
            this.AddThemes();
            this.ApplyThemeSpecificSettings();
        }

        private void AddThemes()
        {
            this.darkThemes = new List<string>();

            this.darkThemes.Add("Green_Dark");
            this.darkThemes.Add("VisualStudio2013_Dark");
            this.darkThemes.Add("Expression_Dark");
        }

        private void Example_ThemeChanged(object sender, System.EventArgs e)
        {
            this.ApplyThemeSpecificSettings();
        }

        private void ApplyThemeSpecificSettings()
        {
            if (this.darkThemes.Contains(this.CurrentTheme))
            {
                IconSources.ChangeIconsSet(IconsSet.Dark);
            }
            else
            {
                IconSources.ChangeIconsSet(IconsSet.Light);
            }
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationThemeManager.GetInstance().ThemeChanged += this.Example_ThemeChanged;
        }

        private void UserControl_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationThemeManager.GetInstance().ThemeChanged -= this.Example_ThemeChanged;
        }
    }
}
