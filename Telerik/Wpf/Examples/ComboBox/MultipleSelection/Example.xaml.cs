﻿using System.Windows;
using System.Windows.Controls;

namespace Telerik.Windows.Examples.ComboBox.MultipleSelection
{
    public partial class Example : UserControl
    {
        public Example()
        {
            InitializeComponent();
        }

        private void MultipleSelectionBoxTemplateCheckBoxClick(object sender, System.Windows.RoutedEventArgs e)
        {
            this.radComboBox.MultipleSelectionBoxTemplate = (sender as CheckBox).IsChecked.Value ? this.Resources["MultipleSelectionBoxTemplate"] as DataTemplate : null;
        }

        private void RefreshItemTemplateSelectedCheckBoxClick(object sender, System.Windows.RoutedEventArgs e)
        {
            if ((sender as CheckBox).IsChecked.Value)
            {
                this.radComboBox.ItemTemplate = this.Resources["CheckBoxItemTemplate"] as DataTemplate;
            }
            else
            {
                this.radComboBox.ItemTemplate = this.Resources["NormalItemTemplate"] as DataTemplate;
            }
        }

        private void OnComboBoxLoaded(object sender, RoutedEventArgs e)
        {
            this.radComboBox.SelectAll();
        }
    }
}