﻿using System.Collections.ObjectModel;
using Telerik.Windows.Controls;

namespace Telerik.Windows.Examples.ComboBox.FirstLook
{
    public class Technology : ViewModelBase
    {
        private int id;
        private string displayName;
        private ObservableCollection<Category> categories;
        private bool isSelected;

        public Technology()
        {
            this.categories = new ObservableCollection<Category>();
        }

        public int ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                if (this.isSelected != value)
                {
                    this.isSelected = value;
                    this.OnPropertyChanged(() => this.IsSelected);
                }
            }
        }

        public string DisplayName
        {
            get
            {
                return this.displayName;
            }
            set
            {
                this.displayName = value;
            }
        }

        public ObservableCollection<Category> Categories
        {
            get
            {
                return this.categories;
            }
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Technology)
            {
                return (obj as Technology).ID == this.ID;
            }
            return false;
        }
    }
}