﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Telerik.Windows.Examples.ComboBox.FirstLook
{
    public class Store : ViewModelBase
    {
        private string sortBy;
        public ReadOnlyObservableCollection<string> sortFields;
        private CollectionViewSource filteredBooks;
        private CollectionViewSource filteredCategories;
        private Technology selectedTechnology;
        private Category selectedCategory;
        private ObservableBookCollection books;
        private ObservableCollection<Technology> technologies;
        private ObservableCollection<Category> categories;

        public Store()
        {
            this.books = new ObservableBookCollection();
            this.technologies = new ObservableCollection<Technology>();
            this.categories = new ObservableCollection<Category>();

            this.filteredBooks = new CollectionViewSource();
            this.filteredBooks.Source = this.Books;
            this.filteredBooks.Filter += FilterBooks;

            this.filteredCategories = new CollectionViewSource();
            this.filteredCategories.Source = this.Categories;
            this.filteredCategories.Filter += FilterCategories;

            ObservableCollection<string> sortFields = new ObservableCollection<string>();
            sortFields.Add("Price");
            sortFields.Add("PublishDate");
            sortFields.Add("Rating");
            this.sortFields = new ReadOnlyObservableCollection<string>(sortFields);

            this.RefreshBooksCommand = new DelegateCommand(this.OnRefreshBooksCommandExecuted);
        }

        public ICommand RefreshBooksCommand { get; set; }

        public string SortBy
        {
            get
            {
                return this.sortBy;
            }
            set
            {
                if (this.sortBy != value)
                {
                    this.sortBy = value;
                    this.OnPropertyChanged("SortBy");
                    this.FilteredBooks.SortDescriptions.Clear();
                    if (this.SortBy != null)
                    {
                        this.FilteredBooks.SortDescriptions.Add(new SortDescription(SortBy, ListSortDirection.Descending));
                    }
                }
            }
        }

        public ReadOnlyObservableCollection<string> SortFields
        {
            get
            {
                return this.sortFields;
            }
        }

        public CollectionViewSource FilteredBooks
        {
            get
            {
                return this.filteredBooks;
            }
        }

        public CollectionViewSource FilteredCategories
        {
            get
            {
                return this.filteredCategories;
            }
        }

        public Technology SelectedTechnology
        {
            get
            {
                return this.selectedTechnology;
            }
            set
            {
                if (this.selectedTechnology != value)
                {
                    this.selectedTechnology = value;
                    this.OnPropertyChanged("SelectedTechnology");
                }
            }
        }

        public Category SelectedCategory
        {
            get
            {
                return this.selectedCategory;
            }
            set
            {
                if (this.selectedCategory != value)
                {
                    this.selectedCategory = value;
                    this.OnPropertyChanged("SelectedCategory");
                }
            }
        }

        public ObservableBookCollection Books
        {
            get
            {
                return this.books;
            }
        }

        public ObservableCollection<Technology> Technologies
        {
            get
            {
                return this.technologies;
            }
        }

        public ObservableCollection<Category> Categories
        {
            get
            {
                return this.categories;
            }
        }

        private void OnRefreshBooksCommandExecuted(object obj)
        {
            this.FilteredBooks.View.Refresh();
        }

        private void FilterCategories(object sender, FilterEventArgs e)
        {
            Category b = e.Item as Category;
            if (b != null)
            {
                e.Accepted = (this.SelectedTechnology == null) || this.SelectedTechnology.Categories.Contains(b);
            }
        }

        private void FilterBooks(object sender, FilterEventArgs e)
        {
            Book b = e.Item as Book;
            if (b != null)
            {
                var technologyMatch = this.SelectedTechnology == null || this.Technologies.Any(t => t.IsSelected && b.Technology.Equals(t));
                var categoryMatch = this.SelectedCategory == null || this.Categories.Any(c => c.IsSelected && b.Category.Equals(c));
                e.Accepted = technologyMatch && categoryMatch;
            }
        }
    }
}