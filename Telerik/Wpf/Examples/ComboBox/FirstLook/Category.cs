﻿using Telerik.Windows.Controls;

namespace Telerik.Windows.Examples.ComboBox.FirstLook
{
    public class Category : ViewModelBase
    {
        private int id;
        private string displayName;
        private bool isSelected;

        public int ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public string DisplayName
        {
            get
            {
                return this.displayName;
            }
            set
            {
                this.displayName = value;
            }
        }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                if (this.isSelected != value)
                {
                    this.isSelected = value;
                    this.OnPropertyChanged(() => this.IsSelected);
                }
            }
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Category)
            {
                return (obj as Category).ID == this.ID;
            }
            return false;
        }
    }
}