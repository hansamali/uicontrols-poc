﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls.Map;
using Telerik.Windows.Examples.Map.FirstLook.ViewModels;

namespace Telerik.Windows.Examples.Map.FirstLook
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Example : UserControl
    {
        double pivotLongitude;
        MainViewModel model;
        const double LongitudeWidth = 360;

        public Example()
        {
            InitializeComponent();
            this.bingProvider.ApplicationId = BingMapHelper.VEKey;
        }
        private void MapLoaded(object sender, RoutedEventArgs e)
        {
            this.pivotLongitude = this.map.Center.Longitude;
            this.model = new MainViewModel();
            this.DataContext = this.model;
            this.map.CenterChanged += (o, ee) => this.RefreshTrip(this.map.Center.Longitude);
            this.map.ZoomChanged += this.Map_ZoomChanged;

            this.InitializeFlightsLayer();
        }

        private void Map_ZoomChanged(object sender, EventArgs e)
        {
            DataTemplate citiesTemplate = this.map.ZoomLevel >= 4 ?
                this.rootGrid.Resources["cityLabeTemplate"] as DataTemplate :
                this.rootGrid.Resources["cityPushPinTemplate"] as DataTemplate;

            if (this.citiesLayer.ItemTemplate != citiesTemplate)
            {
                this.citiesLayer.ItemTemplate = citiesTemplate;
            }
        }

        private void InitializeFlightsLayer()
        {
            foreach (TripPartModel trip in this.model.TripParts)
            {
                QuadraticBezierSegmentData bezier = new QuadraticBezierSegmentData() { Point1 = trip.MiddleLocation, Point2 = trip.ArrivalLocation };
                PathFigureData data = new PathFigureData() { StartPoint = trip.DepartureLocation };
                data.Segments.Add(bezier);

                PathGeometryData pathGeometry = new PathGeometryData();
                pathGeometry.Figures.Add(data);

                PathData path = new PathData();
                path.Data = pathGeometry;
                this.flightsLayer.Items.Add(path);
            }
        }

        internal void RefreshTrip(double longitude)
        {
            double dist = Math.Abs(longitude - this.pivotLongitude);
            if (dist < LongitudeWidth)
            {
                return;
            }

            double offset = longitude < this.pivotLongitude ? -LongitudeWidth : LongitudeWidth;
            this.pivotLongitude += offset;

            this.model.ShiftCities(offset);
            this.ShiftRoutes(offset);
        }

        private void ShiftRoutes(double offset)
        {
            foreach (PathData path in this.flightsLayer.Items)
            {
                PathGeometryData data = path.Data as PathGeometryData;
                PathFigureData figure = data.Figures[0];
                QuadraticBezierSegmentData bezier = figure.Segments[0] as QuadraticBezierSegmentData;

                figure.StartPoint = new Location(figure.StartPoint.Latitude, figure.StartPoint.Longitude + offset);
                bezier.Point2 = new Location(bezier.Point2.Latitude, bezier.Point2.Longitude + offset);
                bezier.Point1 = TripPartModel.CalculateMiddleLocation(figure.StartPoint, bezier.Point2, 8);
            }
        }
    }
}
