﻿using System;
using System.Linq;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Map;

namespace Telerik.Windows.Examples.Map.FirstLook
{
    public class TripCityModel : ViewModelBase, INotifyLocationChanged
    {
        public string City { get; set; }
        public int TripIndex { get; set; }
        public string ImagePath {get;set;}

        private Location location;
        public Location Location
        {
            get
            {
                return this.location;
            }
            set
            {
                if (this.location== null || this.location.Latitude != value.Latitude || this.location.Longitude != value.Longitude)
                {
                    Location oldValue = this.location;
                    this.location = value;
                    this.OnPropertyChanged("Location");
                    this.OnLocationChanged(oldValue, value);
                }
            }
        }

        public TripCityModel(string city, Location loc, int tripIndex, bool isFlightStart)
        {
            this.City = city;
            this.Location = loc;
            this.TripIndex = tripIndex;

            string busOrPlane = isFlightStart ? "plane_16" : "bus_16";
            this.ImagePath = string.Format("../Images/{0}.png", busOrPlane); 
        }

        public static TripCityModel Copy(TripCityModel from, Location newLocation)
        {
            TripCityModel newModel = new TripCityModel(from.City, newLocation, from.TripIndex, true);
            newModel.ImagePath = from.ImagePath;
            return newModel;
        }

        public event EventHandler<LocationChangedEventArgs> LocationChanged;

        private void OnLocationChanged(Location oldValue, Location newValue)
        {
            if (this.LocationChanged != null)
            {
                this.LocationChanged(this, new LocationChangedEventArgs(oldValue, newValue));
            }
        }
    }
}
