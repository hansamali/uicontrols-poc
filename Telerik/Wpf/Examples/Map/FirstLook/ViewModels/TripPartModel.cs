﻿using System;
using System.Linq;
using Telerik.Windows.Controls.Map;

namespace Telerik.Windows.Examples.Map.FirstLook
{
    public class TripPartModel
    {
        public TripPartModel(Location start, Location end)
        {
            this.DepartureLocation = start;
            this.ArrivalLocation = end;
        }

        public Location DepartureLocation { get; set; }
        public Location ArrivalLocation { get; set; }
        public Location MiddleLocation
        {
            get
            {
                return CalculateMiddleLocation(this.DepartureLocation, this.ArrivalLocation, 8);
            }
        }

        public static Location CalculateMiddleLocation(Location start, Location end, double tension)
        {
            Location controlPoint = new Location();
            controlPoint.Longitude = (start.Longitude + end.Longitude) / 2;
            controlPoint.Latitude = Math.Max(start.Latitude, end.Latitude) +
                                    Math.Abs(start.Longitude - end.Longitude) / tension;
            return controlPoint;
        }
    }
}
