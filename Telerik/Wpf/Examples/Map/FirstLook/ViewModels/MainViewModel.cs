﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Telerik.Windows.Controls.Map;

namespace Telerik.Windows.Examples.Map.FirstLook.ViewModels
{
    public class MainViewModel
    {
        private readonly ObservableCollection<TripCityModel> uniqueCities = new ObservableCollection<TripCityModel>()
        {
            new TripCityModel("Seattle", new Location(47.6149425,-122.4059451), 1, true),
            new TripCityModel("New York", new Location(40.7058254,-74.1180854), 2, true),
            new TripCityModel("London", new Location(51.5287718,-0.2416795), 3, true),
            new TripCityModel("Paris", new Location(48.8589507,2.2775177), 4, false),
            new TripCityModel("Berlin", new Location(52.5076682,13.286064), 5, true),
            new TripCityModel("Istanbul", new Location(41.0054958,28.8720976), 6, true),
            new TripCityModel("Cairo", new Location(30.0595581,31.223445), 7, true),
            new TripCityModel("Mumbai", new Location(19.0830943,72.741121), 8, false),
            new TripCityModel("New Delhi", new Location(28.5274228,77.1389454), 9, true),
            new TripCityModel("Bangkok", new Location(13.7248945,100.4930278), 10, false),
            new TripCityModel("Hanoi", new Location(21.0227358,105.819454), 11, true),
            new TripCityModel("Tokyo", new Location(35.6734622,139.6403485), 12, true),
        };

        public MainViewModel()
        {
            this.InitializeCities();
            this.InitializeTripParts();             
        }

        public ObservableCollection<TripPartModel> TripParts { get; set; }
        public ObservableCollection<TripCityModel> Cities { get; set; }

        internal void ShiftCities(double offset)
        {
            foreach (TripCityModel cityModel in this.Cities)
            {
                Location newLocation = new Location(cityModel.Location.Latitude, cityModel.Location.Longitude + offset);
                cityModel.Location = newLocation;
            }
        }

        private void InitializeCities()
        {
            this.Cities = new ObservableCollection<TripCityModel>();

            for (int k = -360; k <= 720; k += 360)
            {
                for (int i = 0; i < this.uniqueCities.Count; i++)
                {
                    TripCityModel originalCity = this.uniqueCities[i];
                    TripCityModel city = k == 0 ?
                        originalCity :
                        TripCityModel.Copy(originalCity, new Location(originalCity.Location.Latitude, originalCity.Location.Longitude + k));

                    this.Cities.Add(city);
                }
            }
        }

        private void InitializeTripParts()
        {
            this.TripParts = new ObservableCollection<TripPartModel>();

            int length = this.Cities.Count;
            for (int i = 0; i < length - 1; i++)
            {
                TripCityModel start = this.Cities[i];
                TripCityModel end = this.Cities[i + 1];
                this.TripParts.Add(new TripPartModel(start.Location, end.Location));
            } 
        }
    }
}
