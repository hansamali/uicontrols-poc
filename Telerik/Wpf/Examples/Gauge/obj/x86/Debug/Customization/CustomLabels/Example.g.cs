﻿#pragma checksum "..\..\..\..\..\Customization\CustomLabels\Example.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "B7299995E5ECCEB66177428491BCDB78"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Charting;
using Telerik.Examples.Gauge;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Behaviors;
using Telerik.Windows.Controls.BulletGraph;
using Telerik.Windows.Controls.ChartView;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.Gauge;
using Telerik.Windows.Controls.HeatMap;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Map;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.QuickStart;
using Telerik.Windows.Controls.Sparklines;
using Telerik.Windows.Controls.TimeBar;
using Telerik.Windows.Controls.Timeline;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeMap;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace Telerik.Windows.Examples.Gauge.Customization.CustomLabels {
    
    
    /// <summary>
    /// Example
    /// </summary>
    public partial class Example : Telerik.Examples.Gauge.DynamicBasePage, System.Windows.Markup.IComponentConnector {
        
        
        #line 70 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadRadialGauge radGauge;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Gauge.RadialScale radialScale;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.Gauge.Marker marker;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox labelLocation;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton relative;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton absolute;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadNumericUpDown labelOffset;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox foregroundCombo;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox fontSizeCombo;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox fontFamilyCombo;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox labelRotationMode;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox labelFormat;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox labelTemplate;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Gauge;component/customization/customlabels/example.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.radGauge = ((Telerik.Windows.Controls.RadRadialGauge)(target));
            return;
            case 3:
            this.radialScale = ((Telerik.Windows.Controls.Gauge.RadialScale)(target));
            return;
            case 4:
            this.marker = ((Telerik.Windows.Controls.Gauge.Marker)(target));
            return;
            case 5:
            this.labelLocation = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 6:
            this.relative = ((System.Windows.Controls.RadioButton)(target));
            
            #line 130 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
            this.relative.Checked += new System.Windows.RoutedEventHandler(this.OffsetModeChanged);
            
            #line default
            #line hidden
            
            #line 131 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
            this.relative.Unchecked += new System.Windows.RoutedEventHandler(this.OffsetModeChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.absolute = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 8:
            this.labelOffset = ((Telerik.Windows.Controls.RadNumericUpDown)(target));
            
            #line 145 "..\..\..\..\..\Customization\CustomLabels\Example.xaml"
            this.labelOffset.ValueChanged += new System.EventHandler<Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs>(this.OffsetChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            this.foregroundCombo = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 10:
            this.fontSizeCombo = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 11:
            this.fontFamilyCombo = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 12:
            this.labelRotationMode = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 13:
            this.labelFormat = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 14:
            this.labelTemplate = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

