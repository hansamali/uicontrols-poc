﻿using System.Windows;
using System.Windows.Controls;

namespace Telerik.Windows.Examples.PasswordBox.Configurator
{
    public partial class Example : UserControl
    {
        private DataTemplate defaultTemplate;

        public Example()
        {
            InitializeComponent();
            this.defaultTemplate = this.radPasswordBox.ShowPasswordButtonContentTemplate;
        }

        private void OnRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            var selectedOption = (sender as RadioButton).Content.ToString();
            var content = string.Empty;
            var contentTemplate = this.Resources[selectedOption + "Template"] as DataTemplate;

            switch (selectedOption)
            {
                case "Text":
                    content = "Show";
                    break;
                case "Path":
                    content = "F1 M36,0 C52.3,0 66,13.6 66,30 L66,36 L72,36 L72,96 L0,96 L0,36 L6,36 L6,30 C6,13.6 19.6,0 36,0 z M14.6,30.6 L14.6,36 L57.3,36 L57.3,30.6 C57.3,19.7 46.8,9.3 36,9.3 C25.1,9.3 14.6,19.7 14.6,30.6 z M44,82 L38.5,65.5 C41.7,64.5 44,61.5 44,58 C44,53.5 40.4,50 36,50 C31.5,50 28,53.5 28,58 C28,61.5 30.2,64.5 33.4,65.5 L28,82 L44,82 z";
                    break;
                case "Image":
                    content = @"../Images/star.png";
                    break;
                case "Default":
                default:
                    contentTemplate = this.defaultTemplate;
                    break;
            }

            if (this.radPasswordBox != null)
            {
                this.radPasswordBox.ShowPasswordButtonContentTemplate = contentTemplate;
                this.radPasswordBox.ShowPasswordButtonContent = content;
            }
        }
    }
}
