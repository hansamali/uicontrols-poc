﻿using System.Windows;
using System.Windows.Controls;

namespace Telerik.Windows.Examples.PasswordBox.FirstLook
{
    public class ContentTemplateSelector: DataTemplateSelector
    {
        public DataTemplate SignInTemplate { get; set; }
        public DataTemplate SignOutTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var isCurrentlyLogged = (bool?)item;
            if (isCurrentlyLogged != null)
            {
                return isCurrentlyLogged.Value ? this.SignOutTemplate : this.SignInTemplate;
            }

            return base.SelectTemplate(item, container);
        }
    }
}
