﻿using System;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace Telerik.Windows.Examples.PasswordBox.FirstLook
{
    public class ViewModel : ViewModelBase
    {
        private bool isBusy;
        private bool? isCurrentlyLogged;
        private DispatcherTimer timer;

        public ViewModel()
        {
            this.IsCurrentlySignedIn = false;
            this.SignInCommand = new DelegateCommand(this.OnSignInCommandExecuted);
            this.SignOutCommand = new DelegateCommand(this.OnSignOutCommandExecuted);
            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromSeconds(2);
        }

        public ICommand SignInCommand { get; set; }
        public ICommand SignOutCommand { get; set; }

        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }

            set
            {
                if (this.isBusy != value)
                {
                    this.isBusy = value;
                    this.OnPropertyChanged(() => this.IsBusy);
                }
            }
        }

        public bool? IsCurrentlySignedIn
        {
            get
            {
                return this.isCurrentlyLogged;
            }

            set
            {
                if (this.isCurrentlyLogged != value)
                {
                    this.isCurrentlyLogged = value;
                    this.OnPropertyChanged(() => this.IsCurrentlySignedIn);
                }
            }
        }

        private void OnSignInCommandExecuted(object obj)
        {
            this.IsCurrentlySignedIn = null;
            this.IsBusy = true;
            this.timer.Tick -= this.OnTimerTick;
            this.timer.Tick += this.OnTimerTick;
            this.timer.Start();
        }

        private void OnSignOutCommandExecuted(object obj)
        {
            this.IsCurrentlySignedIn = false;
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            this.IsBusy = false;
            this.timer.Stop();
            this.timer.Tick -= this.OnTimerTick;
            this.IsCurrentlySignedIn = true;
        }
    }
}
