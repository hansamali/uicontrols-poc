﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Telerik.Windows.Examples.PasswordBox.FirstLook
{
    public class FocusHelper
    {
        private static void OnEnsureFocusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var newValue = (bool?)e.NewValue;
            if (newValue != null && !newValue.Value)
            {
                var control = d as Control;
                control.Dispatcher.BeginInvoke(new Action(() =>
                {
                    control.Focus();
                }));
            }
        }

        public static bool GetEnsureFocus(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnsureFocusProperty);
        }

        public static void SetEnsureFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(EnsureFocusProperty, value);
        }

        public static readonly DependencyProperty EnsureFocusProperty =
            DependencyProperty.RegisterAttached(
            "EnsureFocus",
            typeof(bool?),
            typeof(FocusHelper),
            new PropertyMetadata(OnEnsureFocusChanged));
    }
}
