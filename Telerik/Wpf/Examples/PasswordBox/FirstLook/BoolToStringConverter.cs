﻿using System;
using System.Windows.Data;

namespace Telerik.Windows.Examples.PasswordBox.FirstLook
{
    public class BoolToStringConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value as bool? ?? true) ? string.Empty : "Locked";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
