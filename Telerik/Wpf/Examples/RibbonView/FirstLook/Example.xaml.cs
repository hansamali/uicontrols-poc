using System.Windows;
using System.Windows.Media;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.QuickStart.Common.Helpers;

namespace Telerik.Windows.Examples.RibbonView.FirstLook
{
	public partial class Example : System.Windows.Controls.UserControl
	{	
		public Example()
		{
			InitializeComponent();
            ApplyThemeSpecificSettings();
			this.Loaded += UserControl_Loaded;
			this.Unloaded += UserControl_Unloaded;
		}

		private void RibbonGroup_LaunchDialog(object sender, RadRoutedEventArgs e)
		{
			RadWindow.Alert("Show Group Options");
		}
		
		private void TextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			this.ChartTools.IsActive = false;
		}

		private void ChartImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			this.ChartTools.IsActive = true;
#if WPF
            if (ApplicationThemeManager.GetInstance().CurrentTheme.Equals("Office2016"))
            {
              this.ChartTools.Color = new SolidColorBrush(Telerik.Windows.Controls.Office2016Palette.Palette.AccentPressedColor);
		    }
#endif
		}
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
            ApplicationThemeManager.GetInstance().ThemeChanged += this.Example_ThemeChanged;
		}

		private void Example_ThemeChanged(object sender, System.EventArgs e)
		{
            ApplyThemeSpecificSettings();
		}

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            ApplicationThemeManager.GetInstance().ThemeChanged -= this.Example_ThemeChanged;
        }

        private void ApplyThemeSpecificSettings()
        {
#if WPF            
            switch (ApplicationThemeManager.GetInstance().CurrentTheme)
			{
				case "Green_Light":
				case "Green_Dark":
					this.ChartTools.Color = new SolidColorBrush(Telerik.Windows.Controls.GreenPalette.Palette.AccentLowColor);
					break;
                case "Office2016":
                    this.ChartTools.Color = new SolidColorBrush(Telerik.Windows.Controls.Office2016Palette.Palette.AccentPressedColor);
                    break;
				default:
					this.ChartTools.Color = new SolidColorBrush { Color = Color.FromArgb(0xFF, 0xFF, 0x64, 0x64) };
					break;
			}
#endif
            switch (ApplicationThemeManager.GetInstance().CurrentTheme)
            {
                case "Expression_Dark":
                case "VisualStudio2013_Dark":
                case "Green_Dark":
                    IconSources.ChangeIconsSet(IconsSet.Dark);
                    break;
                case "Green_Light":
                case "Office2013":
                case "Office2016":
                case "Office2013_LightGray":
                case "Office2013_DarkGray":
                case "VisualStudio2013":
                case "VisualStudio2013_Blue":
                case "Windows8":
                case "Windows8Touch":
                    IconSources.ChangeIconsSet(IconsSet.Modern);
                    break;
                default:
                    IconSources.ChangeIconsSet(IconsSet.Light);
                    break;
            }
        }
	}
}
