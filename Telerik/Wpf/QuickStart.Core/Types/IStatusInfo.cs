﻿namespace Telerik.Windows.QuickStart
{
	public interface IStatusInfo
	{
		Enums.StatusMode Status { get; }
		Enums.StatusMode TouchStatus { get; }
	}
}
