﻿using System.Windows;
using System.Windows.Controls;

namespace Telerik.Windows.QuickStart
{
    public class ThemeStatusTemplateSelector: DataTemplateSelector
    {
        public DataTemplate NewTemplate { get; set; }
        public DataTemplate NormalTemplate { get; set; }

        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            if (item.ToString().Contains("Office2016"))
            {
                return this.NewTemplate;
            }

            return this.NormalTemplate;
        }
    }
}
