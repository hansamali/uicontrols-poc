﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Telerik.Examples.WinControls.ChartView.InverseAxes
{
    public class WellDrillData
    {
        public DateTime Date { get; set; }
        public double Depth { get; set; }
    }
}
