﻿using System;
using System.Collections.Generic;

namespace Telerik.Examples.WinControls.ChartView.Printing
{
    public class LineSeriesDataModel
    {
        private IEnumerable<LineAreaSeriesData> s1;
        private IEnumerable<LineAreaSeriesData> s2;
        private IEnumerable<LineAreaSeriesData> s3;
        private IEnumerable<LineAreaSeriesData> s4;
        private IEnumerable<LineAreaSeriesData> s5;
        private IEnumerable<LineAreaSeriesData> s6;
        private IEnumerable<LineAreaSeriesData> s7;
        private IEnumerable<LineAreaSeriesData> s8;
        private IEnumerable<LineAreaSeriesData> s9;
        private IEnumerable<LineAreaSeriesData> s10;
        private IEnumerable<LineAreaSeriesData> s11;
        private IEnumerable<LineAreaSeriesData> s12;

        public IEnumerable<LineAreaSeriesData> S1
        {
            get
            {
                if (this.s1 == null)
                {
                    this.s1 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(30, "May"),
                        new LineAreaSeriesData(20, "Jun"),
                        new LineAreaSeriesData(60, "Jul"),
                        new LineAreaSeriesData(110, "Aug"),
                        new LineAreaSeriesData(150, "Sep"),
                        new LineAreaSeriesData(200, "Oct"),
                        new LineAreaSeriesData(160, "Nov"),
                        new LineAreaSeriesData(150, "Dec"),
                        new LineAreaSeriesData(100, "Jan")
                    };
                }

                return this.s1;
            }
        }

        public IEnumerable<LineAreaSeriesData> S2
        {
            get
            {
                if (this.s2 == null)
                {
                    this.s2 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(70, "May"),
                        new LineAreaSeriesData(60, "Jun"),
                        new LineAreaSeriesData(100, "Jul"),
                        new LineAreaSeriesData(50, "Aug"),
                        new LineAreaSeriesData(60, "Sep"),
                        new LineAreaSeriesData(80, "Oct"),
                        new LineAreaSeriesData(100, "Nov"),
                        new LineAreaSeriesData(80, "Dec"),
                        new LineAreaSeriesData(50, "Jan")
                    };
                }

                return this.s2;
            }
        }

        public IEnumerable<LineAreaSeriesData> S3
        {
            get
            {
                if (this.s3 == null)
                {
                    this.s3 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(150, "May"),
                        new LineAreaSeriesData(170, "Jun"),
                        new LineAreaSeriesData(160, "Jul"),
                        new LineAreaSeriesData(110, "Aug"),
                        new LineAreaSeriesData(200, "Sep"),
                        new LineAreaSeriesData(130, "Oct"),
                        new LineAreaSeriesData(210, "Nov"),
                        new LineAreaSeriesData(210, "Dec"),
                        new LineAreaSeriesData(210, "Jan")
                    };
                }

                return this.s3;
            }
        }

        public IEnumerable<LineAreaSeriesData> S4
        {
            get
            {
                if (this.s4 == null)
                {
                    this.s4 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(190, "May"),
                        new LineAreaSeriesData(210, "Jun"),
                        new LineAreaSeriesData(200, "Jul"),
                        new LineAreaSeriesData(230, "Aug"),
                        new LineAreaSeriesData(230, "Sep"),
                        new LineAreaSeriesData(300, "Oct"),
                        new LineAreaSeriesData(290, "Nov"),
                        new LineAreaSeriesData(280, "Dec"),
                        new LineAreaSeriesData(270, "Jan")
                    };
                }

                return this.s4;
            }
        }

        public IEnumerable<LineAreaSeriesData> S5
        {
            get
            {
                if (this.s5 == null)
                {
                    this.s5 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(280, "May"),
                        new LineAreaSeriesData(260, "Jun"),
                        new LineAreaSeriesData(270, "Jul"),
                        new LineAreaSeriesData(310, "Aug"),
                        new LineAreaSeriesData(270, "Sep"),
                        new LineAreaSeriesData(260, "Oct"),
                        new LineAreaSeriesData(230, "Nov"),
                        new LineAreaSeriesData(250, "Dec"),
                        new LineAreaSeriesData(260, "Jan")
                    };
                }

                return this.s5;
            }
        }

        public IEnumerable<LineAreaSeriesData> S6
        {
            get
            {
                if (this.s6 == null)
                {
                    this.s6 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(330, "May"),
                        new LineAreaSeriesData(320, "Jun"),
                        new LineAreaSeriesData(380, "Jul"),
                        new LineAreaSeriesData(330, "Aug"),
                        new LineAreaSeriesData(330, "Sep"),
                        new LineAreaSeriesData(330, "Oct"),
                        new LineAreaSeriesData(350, "Nov"),
                        new LineAreaSeriesData(340, "Dec"),
                        new LineAreaSeriesData(380, "Jan")
                    };
                }

                return this.s6;
            }
        }

        public IEnumerable<LineAreaSeriesData> S7
        {
            get
            {
                if (this.s7 == null)
                {
                    this.s7 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(420, "May"),
                        new LineAreaSeriesData(410, "Jun"),
                        new LineAreaSeriesData(420, "Jul"),
                        new LineAreaSeriesData(500, "Aug"),
                        new LineAreaSeriesData(450, "Sep"),
                        new LineAreaSeriesData(440, "Oct"),
                        new LineAreaSeriesData(380, "Nov"),
                        new LineAreaSeriesData(420, "Dec"),
                        new LineAreaSeriesData(450, "Jan")
                    };
                }

                return this.s7;
            }
        }

        public IEnumerable<LineAreaSeriesData> S8
        {
            get
            {
                if (this.s8 == null)
                {
                    this.s8 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(370, "May"),
                        new LineAreaSeriesData(360, "Jun"),
                        new LineAreaSeriesData(350, "Jul"),
                        new LineAreaSeriesData(370, "Aug"),
                        new LineAreaSeriesData(380, "Sep"),
                        new LineAreaSeriesData(340, "Oct"),
                        new LineAreaSeriesData(320, "Nov"),
                        new LineAreaSeriesData(310, "Dec"),
                        new LineAreaSeriesData(310, "Jan")
                    };
                }

                return this.s8;
            }
        }

        public IEnumerable<LineAreaSeriesData> S9
        {
            get
            {
                if (this.s9 == null)
                {
                    this.s9 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(240, "May"),
                        new LineAreaSeriesData(220, "Jun"),
                        new LineAreaSeriesData(260, "Jul"),
                        new LineAreaSeriesData(280, "Aug"),
                        new LineAreaSeriesData(220, "Sep"),
                        new LineAreaSeriesData(230, "Oct"),
                        new LineAreaSeriesData(250, "Nov"),
                        new LineAreaSeriesData(210, "Dec"),
                        new LineAreaSeriesData(250, "Jan")
                    };
                }

                return this.s9;
            }
        }

        public IEnumerable<LineAreaSeriesData> S10
        {
            get
            {
                if (this.s10 == null)
                {
                    this.s10 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(250, "May"),
                        new LineAreaSeriesData(240, "Jun"),
                        new LineAreaSeriesData(260, "Jul"),
                        new LineAreaSeriesData(290, "Aug"),
                        new LineAreaSeriesData(320, "Sep"),
                        new LineAreaSeriesData(360, "Oct"),
                        new LineAreaSeriesData(350, "Nov"),
                        new LineAreaSeriesData(320, "Dec"),
                        new LineAreaSeriesData(330, "Jan")
                    };
                }

                return this.s10;
            }
        }

        public IEnumerable<LineAreaSeriesData> S11
        {
            get
            {
                if (this.s11 == null)
                {
                    this.s11 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(130, "May"),
                        new LineAreaSeriesData(160, "Jun"),
                        new LineAreaSeriesData(170, "Jul"),
                        new LineAreaSeriesData(190, "Aug"),
                        new LineAreaSeriesData(100, "Sep"),
                        new LineAreaSeriesData(60, "Oct"),
                        new LineAreaSeriesData(90, "Nov"),
                        new LineAreaSeriesData(140, "Dec"),
                        new LineAreaSeriesData(110, "Jan")
                    };
                }

                return this.s11;
            }
        }

        public IEnumerable<LineAreaSeriesData> S12
        {
            get
            {
                if (this.s12 == null)
                {
                    this.s12 = new List<LineAreaSeriesData>() {
                        new LineAreaSeriesData(330, "May"),
                        new LineAreaSeriesData(360, "Jun"),
                        new LineAreaSeriesData(320, "Jul"),
                        new LineAreaSeriesData(370, "Aug"),
                        new LineAreaSeriesData(280, "Sep"),
                        new LineAreaSeriesData(230, "Oct"),
                        new LineAreaSeriesData(290, "Nov"),
                        new LineAreaSeriesData(210, "Dec"),
                        new LineAreaSeriesData(290, "Jan")
                    };
                }

                return this.s12;
            }
        }

        public IEnumerable<LineAreaSeriesData> GetData(int index)
        {
            if (index == 0)
            {
                return this.S1;
            }

            if (index == 1)
            {
                return this.S2;
            }

            if (index == 2)
            {
                return this.S3;
            }

            if (index == 3)
            {
                return this.S4;
            }

            if (index == 4)
            {
                return this.S5;
            }

            if (index == 5)
            {
                return this.S6;
            }

            if (index == 6)
            {
                return this.S7;
            }

            if (index == 7)
            {
                return this.S8;
            }

            if (index == 8)
            {
                return this.S9;
            }

            if (index == 9)
            {
                return this.S10;
            }

            if (index == 10)
            {
                return this.S11;
            }

            if (index == 11)
            {
                return this.S12;
            }

            return null;
        }

        public string GetLegendText(int index)
        {
            switch (index)
            {
                case 0:
                    return "Bills";
                case 1:
                    return "Car";
                case 2:
                    return "Entertainment";
                case 3:
                    return "Food & Drink";
                case 4:
                    return "Gifts";
                case 5:
                    return "Groceries";
                case 6:
                    return "Hobbies";
                case 7:
                    return "Rent";
                case 8:
                    return "Savings";
                case 9:
                    return "Shopping";
                case 10:
                    return "Travel";
                case 11:
                    return "Utilities";
                default:
                    return "Missing product ID";
            }
        }
    }
}
