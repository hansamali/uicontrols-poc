﻿using Telerik.WinControls.UI;
namespace Telerik.Examples.WinControls.ChartView.RadarAnnotations
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radChartView1 = new Telerik.WinControls.UI.RadChartView();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radCheckBox30N = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox60N = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox30S = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox60S = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radCheckBoxSouthernSolstice = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxSouthwardEquinox = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxNorthernSolstice = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxNorthwardEquinox = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.radCheckBoxWinter = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxAutumn = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxSummer = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxSpring = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBoxWorkHours = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox30N)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox60N)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox30S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox60S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxSouthernSolstice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxSouthwardEquinox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxNorthernSolstice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxNorthwardEquinox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxWinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAutumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxSummer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxSpring)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxWorkHours)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox4);
            this.settingsPanel.Controls.Add(this.radGroupBox3);
            this.settingsPanel.Controls.Add(this.radGroupBox2);
            this.settingsPanel.Controls.Add(this.radLabel1);
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(1564, 3);
            this.settingsPanel.Size = new System.Drawing.Size(304, 832);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox3, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox4, 0);
            // 
            // radChartView1
            // 
            this.radChartView1.AreaType = Telerik.WinControls.UI.ChartAreaType.Polar;
            this.radChartView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radChartView1.Location = new System.Drawing.Point(0, 0);
            this.radChartView1.Name = "radChartView1";
            this.radChartView1.Size = new System.Drawing.Size(1871, 1086);
            this.radChartView1.TabIndex = 2;
            this.radChartView1.Text = "radChartView1";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.radCheckBox30N);
            this.radGroupBox1.Controls.Add(this.radCheckBox60N);
            this.radGroupBox1.Controls.Add(this.radCheckBox30S);
            this.radGroupBox1.Controls.Add(this.radCheckBox60S);
            this.radGroupBox1.HeaderText = "Latitude";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 66);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(284, 123);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Latitude";
            // 
            // radCheckBox30N
            // 
            this.radCheckBox30N.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBox30N.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox30N.Location = new System.Drawing.Point(5, 94);
            this.radCheckBox30N.Name = "radCheckBox30N";
            this.radCheckBox30N.Size = new System.Drawing.Size(48, 18);
            this.radCheckBox30N.TabIndex = 0;
            this.radCheckBox30N.Text = "30°N";
            this.radCheckBox30N.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadCheckmark)(this.radCheckBox30N.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            // 
            // radCheckBox60N
            // 
            this.radCheckBox60N.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBox60N.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox60N.Location = new System.Drawing.Point(5, 70);
            this.radCheckBox60N.Name = "radCheckBox60N";
            this.radCheckBox60N.Size = new System.Drawing.Size(48, 18);
            this.radCheckBox60N.TabIndex = 0;
            this.radCheckBox60N.Text = "60°N";
            this.radCheckBox60N.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadCheckmark)(this.radCheckBox60N.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            // 
            // radCheckBox30S
            // 
            this.radCheckBox30S.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBox30S.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox30S.Location = new System.Drawing.Point(5, 46);
            this.radCheckBox30S.Name = "radCheckBox30S";
            this.radCheckBox30S.Size = new System.Drawing.Size(46, 18);
            this.radCheckBox30S.TabIndex = 0;
            this.radCheckBox30S.Text = "30°S";
            this.radCheckBox30S.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadCheckmark)(this.radCheckBox30S.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            // 
            // radCheckBox60S
            // 
            this.radCheckBox60S.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBox60S.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox60S.Location = new System.Drawing.Point(5, 22);
            this.radCheckBox60S.Name = "radCheckBox60S";
            this.radCheckBox60S.Size = new System.Drawing.Size(46, 18);
            this.radCheckBox60S.TabIndex = 0;
            this.radCheckBox60S.Text = "60°S";
            this.radCheckBox60S.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadCheckmark)(this.radCheckBox60S.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(10, 32);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(150, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Year-round hours of daylight";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.radCheckBoxSouthernSolstice);
            this.radGroupBox2.Controls.Add(this.radCheckBoxSouthwardEquinox);
            this.radGroupBox2.Controls.Add(this.radCheckBoxNorthernSolstice);
            this.radGroupBox2.Controls.Add(this.radCheckBoxNorthwardEquinox);
            this.radGroupBox2.HeaderText = "Solstices and equinoxes";
            this.radGroupBox2.Location = new System.Drawing.Point(10, 195);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(284, 147);
            this.radGroupBox2.TabIndex = 2;
            this.radGroupBox2.Text = "Solstices and equinoxes";
            // 
            // radCheckBoxSouthernSolstice
            // 
            this.radCheckBoxSouthernSolstice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxSouthernSolstice.Location = new System.Drawing.Point(5, 118);
            this.radCheckBoxSouthernSolstice.Name = "radCheckBoxSouthernSolstice";
            this.radCheckBoxSouthernSolstice.Size = new System.Drawing.Size(107, 18);
            this.radCheckBoxSouthernSolstice.TabIndex = 1;
            this.radCheckBoxSouthernSolstice.Text = "Southern Solstice";
            // 
            // radCheckBoxSouthwardEquinox
            // 
            this.radCheckBoxSouthwardEquinox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxSouthwardEquinox.Location = new System.Drawing.Point(5, 94);
            this.radCheckBoxSouthwardEquinox.Name = "radCheckBoxSouthwardEquinox";
            this.radCheckBoxSouthwardEquinox.Size = new System.Drawing.Size(117, 18);
            this.radCheckBoxSouthwardEquinox.TabIndex = 2;
            this.radCheckBoxSouthwardEquinox.Text = "Southward Equinox";
            // 
            // radCheckBoxNorthernSolstice
            // 
            this.radCheckBoxNorthernSolstice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxNorthernSolstice.Location = new System.Drawing.Point(5, 70);
            this.radCheckBoxNorthernSolstice.Name = "radCheckBoxNorthernSolstice";
            this.radCheckBoxNorthernSolstice.Size = new System.Drawing.Size(107, 18);
            this.radCheckBoxNorthernSolstice.TabIndex = 3;
            this.radCheckBoxNorthernSolstice.Text = "Northern Solstice";
            // 
            // radCheckBoxNorthwardEquinox
            // 
            this.radCheckBoxNorthwardEquinox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxNorthwardEquinox.Location = new System.Drawing.Point(5, 46);
            this.radCheckBoxNorthwardEquinox.Name = "radCheckBoxNorthwardEquinox";
            this.radCheckBoxNorthwardEquinox.Size = new System.Drawing.Size(117, 18);
            this.radCheckBoxNorthwardEquinox.TabIndex = 4;
            this.radCheckBoxNorthwardEquinox.Text = "Northward Equinox";
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox3.Controls.Add(this.radLabel3);
            this.radGroupBox3.Controls.Add(this.radCheckBoxWinter);
            this.radGroupBox3.Controls.Add(this.radCheckBoxAutumn);
            this.radGroupBox3.Controls.Add(this.radCheckBoxSummer);
            this.radGroupBox3.Controls.Add(this.radCheckBoxSpring);
            this.radGroupBox3.HeaderText = "Seasons";
            this.radGroupBox3.Location = new System.Drawing.Point(10, 348);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(284, 146);
            this.radGroupBox3.TabIndex = 2;
            this.radGroupBox3.Text = "Seasons";
            // 
            // radCheckBoxWinter
            // 
            this.radCheckBoxWinter.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxWinter.Location = new System.Drawing.Point(5, 117);
            this.radCheckBoxWinter.Name = "radCheckBoxWinter";
            this.radCheckBoxWinter.Size = new System.Drawing.Size(54, 18);
            this.radCheckBoxWinter.TabIndex = 1;
            this.radCheckBoxWinter.Text = "Winter";
            // 
            // radCheckBoxAutumn
            // 
            this.radCheckBoxAutumn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxAutumn.Location = new System.Drawing.Point(5, 93);
            this.radCheckBoxAutumn.Name = "radCheckBoxAutumn";
            this.radCheckBoxAutumn.Size = new System.Drawing.Size(60, 18);
            this.radCheckBoxAutumn.TabIndex = 2;
            this.radCheckBoxAutumn.Text = "Autumn";
            // 
            // radCheckBoxSummer
            // 
            this.radCheckBoxSummer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxSummer.Location = new System.Drawing.Point(5, 69);
            this.radCheckBoxSummer.Name = "radCheckBoxSummer";
            this.radCheckBoxSummer.Size = new System.Drawing.Size(62, 18);
            this.radCheckBoxSummer.TabIndex = 3;
            this.radCheckBoxSummer.Text = "Summer";
            // 
            // radCheckBoxSpring
            // 
            this.radCheckBoxSpring.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxSpring.Location = new System.Drawing.Point(5, 45);
            this.radCheckBoxSpring.Name = "radCheckBoxSpring";
            this.radCheckBoxSpring.Size = new System.Drawing.Size(53, 18);
            this.radCheckBoxSpring.TabIndex = 4;
            this.radCheckBoxSpring.Text = "Spring";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(5, 22);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(164, 18);
            this.radLabel2.TabIndex = 5;
            this.radLabel2.Text = "Radial axis grid line annotations";
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(5, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(171, 18);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "Radial axis plot band annotations";
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox4.Controls.Add(this.radLabel4);
            this.radGroupBox4.Controls.Add(this.radCheckBoxWorkHours);
            this.radGroupBox4.HeaderText = "Work hours";
            this.radGroupBox4.Location = new System.Drawing.Point(10, 501);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(284, 79);
            this.radGroupBox4.TabIndex = 3;
            this.radGroupBox4.Text = "Work hours";
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel4.Location = new System.Drawing.Point(5, 21);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(161, 18);
            this.radLabel4.TabIndex = 5;
            this.radLabel4.Text = "Polar axis plot band annotation";
            // 
            // radCheckBoxWorkHours
            // 
            this.radCheckBoxWorkHours.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxWorkHours.Location = new System.Drawing.Point(5, 45);
            this.radCheckBoxWorkHours.Name = "radCheckBoxWorkHours";
            this.radCheckBoxWorkHours.Size = new System.Drawing.Size(78, 18);
            this.radCheckBoxWorkHours.TabIndex = 4;
            this.radCheckBoxWorkHours.Text = "Work hours";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScrollMinSize = new System.Drawing.Size(680, 420);
            this.Controls.Add(this.radChartView1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1881, 1096);
            this.Controls.SetChildIndex(this.radChartView1, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox30N)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox60N)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox30S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox60S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxSouthernSolstice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxSouthwardEquinox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxNorthernSolstice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxNorthwardEquinox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxWinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAutumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxSummer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxSpring)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxWorkHours)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private RadGroupBox radGroupBox3;
        private RadCheckBox radCheckBoxWinter;
        private RadCheckBox radCheckBoxAutumn;
        private RadCheckBox radCheckBoxSummer;
        private RadCheckBox radCheckBoxSpring;
        private RadGroupBox radGroupBox2;
        private RadCheckBox radCheckBoxSouthernSolstice;
        private RadCheckBox radCheckBoxSouthwardEquinox;
        private RadCheckBox radCheckBoxNorthernSolstice;
        private RadCheckBox radCheckBoxNorthwardEquinox;
        private RadLabel radLabel1;
        private RadGroupBox radGroupBox1;
        private RadCheckBox radCheckBox30N;
        private RadCheckBox radCheckBox60N;
        private RadCheckBox radCheckBox30S;
        private RadCheckBox radCheckBox60S;
        private RadChartView radChartView1;
        private RadGroupBox radGroupBox4;
        private RadLabel radLabel4;
        private RadCheckBox radCheckBoxWorkHours;
        private RadLabel radLabel3;
        private RadLabel radLabel2;

    }
}