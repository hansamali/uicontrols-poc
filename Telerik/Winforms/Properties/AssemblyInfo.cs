﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Demo Application - Telerik UI for WinForms")]

[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(Telerik.WinControls.VersionNumber.CompanyName)]
[assembly: AssemblyProduct(Telerik.WinControls.VersionNumber.ProductName)]
[assembly: AssemblyCopyright(Telerik.WinControls.VersionNumber.CopyrightText)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

#if NET2

[assembly: CLSCompliant(true)]

#endif

#if NET4

[assembly: CLSCompliant(false)]

#endif


// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("15243bcc-c612-44fa-a5d2-f936ca1f71e0")]
[assembly: System.Resources.NeutralResourcesLanguage("en-US", System.Resources.UltimateResourceFallbackLocation.MainAssembly)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion(Telerik.WinControls.VersionNumber.Number)]
[assembly: AssemblyFileVersion(Telerik.WinControls.VersionNumber.Number)]
