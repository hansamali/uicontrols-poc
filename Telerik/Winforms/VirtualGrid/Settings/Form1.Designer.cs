﻿namespace Telerik.Examples.WinControls.VirtualGrid.Settings
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radVirtualGrid1 = new Telerik.WinControls.UI.RadVirtualGrid();
            this.northwindDataSet = new Telerik.Examples.WinControls.DataSources.NorthwindDataSet();
            this.employeesTableAdapter = new Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.EmployeesTableAdapter();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorHeaderRowHeight = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorNewRowHeight = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorDataRowHeight = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorColumnWidth = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorCellSpacing = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorRowSpacing = new Telerik.WinControls.UI.RadSpinEditor();
            this.radCheckBoxShowHeaderRow = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxShowNewRow = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxShowFilterRow = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxFitColumns = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxAlternatingRowColors = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxHotTracking = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorFilterRowHeight = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radVirtualGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorHeaderRowHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorNewRowHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorDataRowHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorColumnWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorCellSpacing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorRowSpacing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxShowHeaderRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxShowNewRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxShowFilterRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFitColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAlternatingRowColors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxHotTracking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorFilterRowHeight)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radCheckBoxHotTracking);
            this.settingsPanel.Controls.Add(this.radCheckBoxAlternatingRowColors);
            this.settingsPanel.Controls.Add(this.radCheckBoxFitColumns);
            this.settingsPanel.Controls.Add(this.radCheckBoxShowFilterRow);
            this.settingsPanel.Controls.Add(this.radCheckBoxShowNewRow);
            this.settingsPanel.Controls.Add(this.radCheckBoxShowHeaderRow);
            this.settingsPanel.Controls.Add(this.radSpinEditorRowSpacing);
            this.settingsPanel.Controls.Add(this.radSpinEditorCellSpacing);
            this.settingsPanel.Controls.Add(this.radSpinEditorColumnWidth);
            this.settingsPanel.Controls.Add(this.radSpinEditorDataRowHeight);
            this.settingsPanel.Controls.Add(this.radSpinEditorFilterRowHeight);
            this.settingsPanel.Controls.Add(this.radSpinEditorNewRowHeight);
            this.settingsPanel.Controls.Add(this.radSpinEditorHeaderRowHeight);
            this.settingsPanel.Controls.Add(this.radLabel6);
            this.settingsPanel.Controls.Add(this.radLabel5);
            this.settingsPanel.Controls.Add(this.radLabel4);
            this.settingsPanel.Controls.Add(this.radLabel3);
            this.settingsPanel.Controls.Add(this.radLabel7);
            this.settingsPanel.Controls.Add(this.radLabel2);
            this.settingsPanel.Controls.Add(this.radLabel1);
            this.settingsPanel.Location = new System.Drawing.Point(955, 11);
            this.settingsPanel.Size = new System.Drawing.Size(230, 615);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel7, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel3, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel4, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel5, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel6, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorHeaderRowHeight, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorNewRowHeight, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorFilterRowHeight, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorDataRowHeight, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorColumnWidth, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorCellSpacing, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorRowSpacing, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxShowHeaderRow, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxShowNewRow, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxShowFilterRow, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxFitColumns, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxAlternatingRowColors, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxHotTracking, 0);
            // 
            // radVirtualGrid1
            // 
            this.radVirtualGrid1.AllowCut = false;
            this.radVirtualGrid1.AllowDelete = false;
            this.radVirtualGrid1.AllowEdit = false;
            this.radVirtualGrid1.AllowPaste = false;
            this.radVirtualGrid1.AllowSorting = false;
            this.radVirtualGrid1.Location = new System.Drawing.Point(0, 0);
            this.radVirtualGrid1.Name = "radVirtualGrid1";
            this.radVirtualGrid1.Size = new System.Drawing.Size(800, 600);
            this.radVirtualGrid1.StandardTab = false;
            this.radVirtualGrid1.TabIndex = 0;
            this.radVirtualGrid1.Text = "radVirtualGrid1";
            this.radVirtualGrid1.CellValueNeeded += new Telerik.WinControls.UI.VirtualGridCellValueNeededEventHandler(this.radVirtualGrid1_CellValueNeeded);
            this.radVirtualGrid1.CellFormatting += new Telerik.WinControls.UI.VirtualGridCellElementEventHandler(this.radVirtualGrid1_CellFormatting);
            // 
            // northwindDataSet
            // 
            this.northwindDataSet.DataSetName = "NorthwindDataSet";
            this.northwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // employeesTableAdapter
            // 
            this.employeesTableAdapter.ClearBeforeFill = true;
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(10, 33);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(98, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Header row height";
            // 
            // radSpinEditorHeaderRowHeight
            // 
            this.radSpinEditorHeaderRowHeight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorHeaderRowHeight.Location = new System.Drawing.Point(10, 58);
            this.radSpinEditorHeaderRowHeight.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.radSpinEditorHeaderRowHeight.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.radSpinEditorHeaderRowHeight.Name = "radSpinEditorHeaderRowHeight";
            this.radSpinEditorHeaderRowHeight.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorHeaderRowHeight.TabIndex = 2;
            this.radSpinEditorHeaderRowHeight.TabStop = false;
            this.radSpinEditorHeaderRowHeight.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(10, 84);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(85, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "New row height";
            // 
            // radSpinEditorNewRowHeight
            // 
            this.radSpinEditorNewRowHeight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorNewRowHeight.Location = new System.Drawing.Point(10, 109);
            this.radSpinEditorNewRowHeight.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.radSpinEditorNewRowHeight.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.radSpinEditorNewRowHeight.Name = "radSpinEditorNewRowHeight";
            this.radSpinEditorNewRowHeight.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorNewRowHeight.TabIndex = 2;
            this.radSpinEditorNewRowHeight.TabStop = false;
            this.radSpinEditorNewRowHeight.Value = new decimal(new int[] {
            24,
            0,
            0,
            0});
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(10, 186);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(62, 18);
            this.radLabel3.TabIndex = 1;
            this.radLabel3.Text = "Row height";
            // 
            // radSpinEditorDataRowHeight
            // 
            this.radSpinEditorDataRowHeight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorDataRowHeight.Location = new System.Drawing.Point(10, 211);
            this.radSpinEditorDataRowHeight.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.radSpinEditorDataRowHeight.Name = "radSpinEditorDataRowHeight";
            this.radSpinEditorDataRowHeight.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorDataRowHeight.TabIndex = 2;
            this.radSpinEditorDataRowHeight.TabStop = false;
            this.radSpinEditorDataRowHeight.Value = new decimal(new int[] {
            24,
            0,
            0,
            0});
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel4.Location = new System.Drawing.Point(10, 237);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(75, 18);
            this.radLabel4.TabIndex = 1;
            this.radLabel4.Text = "Column width";
            // 
            // radSpinEditorColumnWidth
            // 
            this.radSpinEditorColumnWidth.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorColumnWidth.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.radSpinEditorColumnWidth.Location = new System.Drawing.Point(10, 262);
            this.radSpinEditorColumnWidth.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.radSpinEditorColumnWidth.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.radSpinEditorColumnWidth.Name = "radSpinEditorColumnWidth";
            this.radSpinEditorColumnWidth.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorColumnWidth.TabIndex = 2;
            this.radSpinEditorColumnWidth.TabStop = false;
            this.radSpinEditorColumnWidth.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // radLabel5
            // 
            this.radLabel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel5.Location = new System.Drawing.Point(10, 288);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(65, 18);
            this.radLabel5.TabIndex = 1;
            this.radLabel5.Text = "Cell spacing";
            // 
            // radSpinEditorCellSpacing
            // 
            this.radSpinEditorCellSpacing.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorCellSpacing.Location = new System.Drawing.Point(10, 313);
            this.radSpinEditorCellSpacing.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.radSpinEditorCellSpacing.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.radSpinEditorCellSpacing.Name = "radSpinEditorCellSpacing";
            this.radSpinEditorCellSpacing.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorCellSpacing.TabIndex = 2;
            this.radSpinEditorCellSpacing.TabStop = false;
            this.radSpinEditorCellSpacing.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // radLabel6
            // 
            this.radLabel6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel6.Location = new System.Drawing.Point(10, 339);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(68, 18);
            this.radLabel6.TabIndex = 1;
            this.radLabel6.Text = "Row spacing";
            // 
            // radSpinEditorRowSpacing
            // 
            this.radSpinEditorRowSpacing.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorRowSpacing.Location = new System.Drawing.Point(10, 364);
            this.radSpinEditorRowSpacing.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.radSpinEditorRowSpacing.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.radSpinEditorRowSpacing.Name = "radSpinEditorRowSpacing";
            this.radSpinEditorRowSpacing.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorRowSpacing.TabIndex = 2;
            this.radSpinEditorRowSpacing.TabStop = false;
            this.radSpinEditorRowSpacing.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // radCheckBoxShowHeaderRow
            // 
            this.radCheckBoxShowHeaderRow.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxShowHeaderRow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBoxShowHeaderRow.Location = new System.Drawing.Point(10, 402);
            this.radCheckBoxShowHeaderRow.Name = "radCheckBoxShowHeaderRow";
            this.radCheckBoxShowHeaderRow.Size = new System.Drawing.Size(138, 18);
            this.radCheckBoxShowHeaderRow.TabIndex = 3;
            this.radCheckBoxShowHeaderRow.Text = "Should column headers";
            this.radCheckBoxShowHeaderRow.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxShowNewRow
            // 
            this.radCheckBoxShowNewRow.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxShowNewRow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBoxShowNewRow.Location = new System.Drawing.Point(10, 426);
            this.radCheckBoxShowNewRow.Name = "radCheckBoxShowNewRow";
            this.radCheckBoxShowNewRow.Size = new System.Drawing.Size(93, 18);
            this.radCheckBoxShowNewRow.TabIndex = 3;
            this.radCheckBoxShowNewRow.Text = "Show new row";
            this.radCheckBoxShowNewRow.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxShowFilterRow
            // 
            this.radCheckBoxShowFilterRow.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxShowFilterRow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBoxShowFilterRow.Location = new System.Drawing.Point(10, 450);
            this.radCheckBoxShowFilterRow.Name = "radCheckBoxShowFilterRow";
            this.radCheckBoxShowFilterRow.Size = new System.Drawing.Size(95, 18);
            this.radCheckBoxShowFilterRow.TabIndex = 3;
            this.radCheckBoxShowFilterRow.Text = "Show filter row";
            this.radCheckBoxShowFilterRow.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxFitColumns
            // 
            this.radCheckBoxFitColumns.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxFitColumns.Location = new System.Drawing.Point(10, 474);
            this.radCheckBoxFitColumns.Name = "radCheckBoxFitColumns";
            this.radCheckBoxFitColumns.Size = new System.Drawing.Size(77, 18);
            this.radCheckBoxFitColumns.TabIndex = 3;
            this.radCheckBoxFitColumns.Text = "Fit columns";
            // 
            // radCheckBoxAlternatingRowColors
            // 
            this.radCheckBoxAlternatingRowColors.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxAlternatingRowColors.Location = new System.Drawing.Point(10, 498);
            this.radCheckBoxAlternatingRowColors.Name = "radCheckBoxAlternatingRowColors";
            this.radCheckBoxAlternatingRowColors.Size = new System.Drawing.Size(131, 18);
            this.radCheckBoxAlternatingRowColors.TabIndex = 3;
            this.radCheckBoxAlternatingRowColors.Text = "Alternating row colors";
            // 
            // radCheckBoxHotTracking
            // 
            this.radCheckBoxHotTracking.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxHotTracking.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBoxHotTracking.Location = new System.Drawing.Point(10, 522);
            this.radCheckBoxHotTracking.Name = "radCheckBoxHotTracking";
            this.radCheckBoxHotTracking.Size = new System.Drawing.Size(117, 18);
            this.radCheckBoxHotTracking.TabIndex = 3;
            this.radCheckBoxHotTracking.Text = "Enable hot tracking";
            this.radCheckBoxHotTracking.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel7.Location = new System.Drawing.Point(10, 135);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(87, 18);
            this.radLabel7.TabIndex = 1;
            this.radLabel7.Text = "Filter row height";
            // 
            // radSpinEditorFilterRowHeight
            // 
            this.radSpinEditorFilterRowHeight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorFilterRowHeight.Location = new System.Drawing.Point(10, 160);
            this.radSpinEditorFilterRowHeight.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.radSpinEditorFilterRowHeight.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.radSpinEditorFilterRowHeight.Name = "radSpinEditorFilterRowHeight";
            this.radSpinEditorFilterRowHeight.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorFilterRowHeight.TabIndex = 2;
            this.radSpinEditorFilterRowHeight.TabStop = false;
            this.radSpinEditorFilterRowHeight.Value = new decimal(new int[] {
            26,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radVirtualGrid1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1529, 1013);
            this.Controls.SetChildIndex(this.radVirtualGrid1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radVirtualGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorHeaderRowHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorNewRowHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorDataRowHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorColumnWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorCellSpacing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorRowSpacing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxShowHeaderRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxShowNewRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxShowFilterRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFitColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAlternatingRowColors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxHotTracking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorFilterRowHeight)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadVirtualGrid radVirtualGrid1;
        private DataSources.NorthwindDataSet northwindDataSet;
        private Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.EmployeesTableAdapter employeesTableAdapter;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxShowFilterRow;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxShowNewRow;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxShowHeaderRow;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorRowSpacing;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorCellSpacing;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorColumnWidth;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorDataRowHeight;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorNewRowHeight;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorHeaderRowHeight;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxHotTracking;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxAlternatingRowColors;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxFitColumns;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorFilterRowHeight;
        private Telerik.WinControls.UI.RadLabel radLabel7;
    }
}