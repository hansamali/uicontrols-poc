﻿namespace Telerik.Examples.WinControls.Scheduler.MonthView
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.DateTimeInterval dateTimeInterval1 = new Telerik.WinControls.UI.DateTimeInterval();
            Telerik.WinControls.UI.SchedulerDailyPrintStyle schedulerDailyPrintStyle1 = new Telerik.WinControls.UI.SchedulerDailyPrintStyle();
            this.radScheduler1 = new Telerik.WinControls.UI.RadScheduler();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.checkBoxCellsOverflow = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxAppointmentsScrolling = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxEnableWeeksHeader = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxShowWeeksHeader = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxVerticalScroll = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.dropDownWorkWeekEnd = new Telerik.WinControls.UI.RadDropDownList();
            this.dropDownWorkWeekStart = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.spinEditorWeekCount = new Telerik.WinControls.UI.RadSpinEditor();
            this.checkBoxShowWeekend = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxShowFullMonth = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.trackBarColumnSize = new Telerik.WinControls.UI.RadTrackBar();
            this.trackBarRowSize = new Telerik.WinControls.UI.RadTrackBar();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.spinEditorAppointmentSpacing = new Telerik.WinControls.UI.RadSpinEditor();
            this.checkBoxAutoSizeAppointments = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxExactTimeRendering = new Telerik.WinControls.UI.RadCheckBox();
            this.timePickerRulerStart = new Telerik.WinControls.UI.RadTimePicker();
            this.timePickerRulerEnd = new Telerik.WinControls.UI.RadTimePicker();
            this.buttonBackToMonthView = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScheduler1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxCellsOverflow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxAppointmentsScrolling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxEnableWeeksHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowWeeksHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxVerticalScroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownWorkWeekEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownWorkWeekStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorWeekCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowWeekend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowFullMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColumnSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRowSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorAppointmentSpacing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxAutoSizeAppointments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExactTimeRendering)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerRulerStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerRulerEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBackToMonthView)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.buttonBackToMonthView);
            this.settingsPanel.Controls.Add(this.radGroupBox3);
            this.settingsPanel.Controls.Add(this.radGroupBox2);
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(1065, 80);
            this.settingsPanel.Size = new System.Drawing.Size(230, 719);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox3, 0);
            this.settingsPanel.Controls.SetChildIndex(this.buttonBackToMonthView, 0);
            // 
            // radScheduler1
            // 
            dateTimeInterval1.End = new System.DateTime(((long)(0)));
            dateTimeInterval1.Start = new System.DateTime(((long)(0)));
            this.radScheduler1.AccessibleInterval = dateTimeInterval1;
            this.radScheduler1.ActiveViewType = Telerik.WinControls.UI.SchedulerViewType.Month;
            this.radScheduler1.Culture = new System.Globalization.CultureInfo("en-US");
            this.radScheduler1.DataSource = null;
            this.radScheduler1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScheduler1.Location = new System.Drawing.Point(0, 0);
            this.radScheduler1.Name = "radScheduler1";
            schedulerDailyPrintStyle1.AppointmentFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            schedulerDailyPrintStyle1.DateEndRange = new System.DateTime(2014, 6, 15, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.DateHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            schedulerDailyPrintStyle1.DateStartRange = new System.DateTime(2014, 6, 10, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.PageHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold);
            this.radScheduler1.PrintStyle = schedulerDailyPrintStyle1;
            this.radScheduler1.Size = new System.Drawing.Size(1531, 990);
            this.radScheduler1.TabIndex = 2;
            this.radScheduler1.Text = "radScheduler1";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.checkBoxCellsOverflow);
            this.radGroupBox1.Controls.Add(this.checkBoxAppointmentsScrolling);
            this.radGroupBox1.Controls.Add(this.checkBoxEnableWeeksHeader);
            this.radGroupBox1.Controls.Add(this.checkBoxShowWeeksHeader);
            this.radGroupBox1.Controls.Add(this.checkBoxVerticalScroll);
            this.radGroupBox1.HeaderText = "Visual settings";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 33);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(210, 143);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Visual settings";
            // 
            // checkBoxCellsOverflow
            // 
            this.checkBoxCellsOverflow.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxCellsOverflow.Location = new System.Drawing.Point(5, 117);
            this.checkBoxCellsOverflow.Name = "checkBoxCellsOverflow";
            this.checkBoxCellsOverflow.Size = new System.Drawing.Size(161, 18);
            this.checkBoxCellsOverflow.TabIndex = 4;
            this.checkBoxCellsOverflow.Text = "Enable cells overflow button";
            // 
            // checkBoxAppointmentsScrolling
            // 
            this.checkBoxAppointmentsScrolling.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxAppointmentsScrolling.Location = new System.Drawing.Point(5, 93);
            this.checkBoxAppointmentsScrolling.Name = "checkBoxAppointmentsScrolling";
            this.checkBoxAppointmentsScrolling.Size = new System.Drawing.Size(171, 18);
            this.checkBoxAppointmentsScrolling.TabIndex = 3;
            this.checkBoxAppointmentsScrolling.Text = "Enable row scrollbars";
            // 
            // checkBoxEnableWeeksHeader
            // 
            this.checkBoxEnableWeeksHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxEnableWeeksHeader.Location = new System.Drawing.Point(5, 69);
            this.checkBoxEnableWeeksHeader.Name = "checkBoxEnableWeeksHeader";
            this.checkBoxEnableWeeksHeader.Size = new System.Drawing.Size(125, 18);
            this.checkBoxEnableWeeksHeader.TabIndex = 2;
            this.checkBoxEnableWeeksHeader.Text = "Enable weeks header";
            // 
            // checkBoxShowWeeksHeader
            // 
            this.checkBoxShowWeeksHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxShowWeeksHeader.Location = new System.Drawing.Point(5, 45);
            this.checkBoxShowWeeksHeader.Name = "checkBoxShowWeeksHeader";
            this.checkBoxShowWeeksHeader.Size = new System.Drawing.Size(119, 18);
            this.checkBoxShowWeeksHeader.TabIndex = 1;
            this.checkBoxShowWeeksHeader.Text = "Show weeks header";
            // 
            // checkBoxVerticalScroll
            // 
            this.checkBoxVerticalScroll.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxVerticalScroll.Location = new System.Drawing.Point(5, 21);
            this.checkBoxVerticalScroll.Name = "checkBoxVerticalScroll";
            this.checkBoxVerticalScroll.Size = new System.Drawing.Size(132, 18);
            this.checkBoxVerticalScroll.TabIndex = 0;
            this.checkBoxVerticalScroll.Text = "Show vertical scrollbar";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox2.Controls.Add(this.panel2);
            this.radGroupBox2.Controls.Add(this.checkBoxShowWeekend);
            this.radGroupBox2.Controls.Add(this.checkBoxShowFullMonth);
            this.radGroupBox2.HeaderText = "View settings";
            this.radGroupBox2.Location = new System.Drawing.Point(10, 182);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(210, 145);
            this.radGroupBox2.TabIndex = 2;
            this.radGroupBox2.Text = "View settings";
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.Controls.Add(this.radLabel7);
            this.panel2.Controls.Add(this.radLabel8);
            this.panel2.Controls.Add(this.dropDownWorkWeekEnd);
            this.panel2.Controls.Add(this.dropDownWorkWeekStart);
            this.panel2.Controls.Add(this.radLabel6);
            this.panel2.Controls.Add(this.spinEditorWeekCount);
            this.panel2.Location = new System.Drawing.Point(5, 22);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 78);
            this.panel2.TabIndex = 4;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(0, 28);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(100, 18);
            this.radLabel7.TabIndex = 25;
            this.radLabel7.Text = "Working week end";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(0, 3);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(103, 18);
            this.radLabel8.TabIndex = 24;
            this.radLabel8.Text = "Working week start";
            // 
            // dropDownWorkWeekEnd
            // 
            this.dropDownWorkWeekEnd.AllowShowFocusCues = false;
            this.dropDownWorkWeekEnd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dropDownWorkWeekEnd.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.dropDownWorkWeekEnd.Location = new System.Drawing.Point(121, 28);
            this.dropDownWorkWeekEnd.Name = "dropDownWorkWeekEnd";
            this.dropDownWorkWeekEnd.Size = new System.Drawing.Size(78, 20);
            this.dropDownWorkWeekEnd.TabIndex = 23;
            this.dropDownWorkWeekEnd.Text = "radDropDownList3";
            // 
            // dropDownWorkWeekStart
            // 
            this.dropDownWorkWeekStart.AllowShowFocusCues = false;
            this.dropDownWorkWeekStart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dropDownWorkWeekStart.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.dropDownWorkWeekStart.Location = new System.Drawing.Point(121, 2);
            this.dropDownWorkWeekStart.Name = "dropDownWorkWeekStart";
            this.dropDownWorkWeekStart.Size = new System.Drawing.Size(78, 20);
            this.dropDownWorkWeekStart.TabIndex = 22;
            this.dropDownWorkWeekStart.Text = "radDropDownList2";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(0, 55);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(66, 18);
            this.radLabel6.TabIndex = 21;
            this.radLabel6.Text = "Week count";
            // 
            // spinEditorWeekCount
            // 
            this.spinEditorWeekCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorWeekCount.Location = new System.Drawing.Point(121, 54);
            this.spinEditorWeekCount.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.spinEditorWeekCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditorWeekCount.Name = "spinEditorWeekCount";
            this.spinEditorWeekCount.Size = new System.Drawing.Size(78, 20);
            this.spinEditorWeekCount.TabIndex = 20;
            this.spinEditorWeekCount.TabStop = false;
            this.spinEditorWeekCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // checkBoxShowWeekend
            // 
            this.checkBoxShowWeekend.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxShowWeekend.Location = new System.Drawing.Point(5, 121);
            this.checkBoxShowWeekend.Name = "checkBoxShowWeekend";
            this.checkBoxShowWeekend.Size = new System.Drawing.Size(95, 18);
            this.checkBoxShowWeekend.TabIndex = 3;
            this.checkBoxShowWeekend.Text = "Show weekend";
            // 
            // checkBoxShowFullMonth
            // 
            this.checkBoxShowFullMonth.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxShowFullMonth.Location = new System.Drawing.Point(5, 102);
            this.checkBoxShowFullMonth.Name = "checkBoxShowFullMonth";
            this.checkBoxShowFullMonth.Size = new System.Drawing.Size(102, 18);
            this.checkBoxShowFullMonth.TabIndex = 2;
            this.checkBoxShowFullMonth.Text = "Show full month";
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox3.Controls.Add(this.trackBarColumnSize);
            this.radGroupBox3.Controls.Add(this.trackBarRowSize);
            this.radGroupBox3.Controls.Add(this.radLabel3);
            this.radGroupBox3.Controls.Add(this.radLabel2);
            this.radGroupBox3.Controls.Add(this.panel1);
            this.radGroupBox3.Controls.Add(this.checkBoxAutoSizeAppointments);
            this.radGroupBox3.Controls.Add(this.checkBoxExactTimeRendering);
            this.radGroupBox3.HeaderText = "Sizing options";
            this.radGroupBox3.Location = new System.Drawing.Point(10, 333);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(210, 261);
            this.radGroupBox3.TabIndex = 3;
            this.radGroupBox3.Text = "Sizing options";
            // 
            // trackBarColumnSize
            // 
            this.trackBarColumnSize.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.trackBarColumnSize.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.BottomRight;
            this.trackBarColumnSize.LargeTickFrequency = 1;
            this.trackBarColumnSize.Location = new System.Drawing.Point(5, 202);
            this.trackBarColumnSize.Maximum = 10F;
            this.trackBarColumnSize.Minimum = 1F;
            this.trackBarColumnSize.Name = "trackBarColumnSize";
            this.trackBarColumnSize.Size = new System.Drawing.Size(200, 55);
            this.trackBarColumnSize.TabIndex = 6;
            this.trackBarColumnSize.Text = "radTrackBar2";
            this.trackBarColumnSize.Value = 1F;
            // 
            // trackBarRowSize
            // 
            this.trackBarRowSize.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.trackBarRowSize.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.BottomRight;
            this.trackBarRowSize.LargeTickFrequency = 1;
            this.trackBarRowSize.Location = new System.Drawing.Point(5, 114);
            this.trackBarRowSize.Maximum = 10F;
            this.trackBarRowSize.Minimum = 1F;
            this.trackBarRowSize.Name = "trackBarRowSize";
            this.trackBarRowSize.Size = new System.Drawing.Size(200, 55);
            this.trackBarRowSize.TabIndex = 0;
            this.trackBarRowSize.Text = "radTrackBar1";
            this.trackBarRowSize.Value = 1F;
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(5, 178);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(107, 18);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "Current column size:";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(5, 92);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(89, 18);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "Current row size:";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.spinEditorAppointmentSpacing);
            this.panel1.Location = new System.Drawing.Point(5, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 25);
            this.panel1.TabIndex = 3;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(0, 4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(114, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Appointment spacing";
            // 
            // spinEditorAppointmentSpacing
            // 
            this.spinEditorAppointmentSpacing.Location = new System.Drawing.Point(130, 3);
            this.spinEditorAppointmentSpacing.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spinEditorAppointmentSpacing.Name = "spinEditorAppointmentSpacing";
            this.spinEditorAppointmentSpacing.Size = new System.Drawing.Size(67, 20);
            this.spinEditorAppointmentSpacing.TabIndex = 3;
            this.spinEditorAppointmentSpacing.TabStop = false;
            // 
            // checkBoxAutoSizeAppointments
            // 
            this.checkBoxAutoSizeAppointments.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxAutoSizeAppointments.Location = new System.Drawing.Point(5, 40);
            this.checkBoxAutoSizeAppointments.Name = "checkBoxAutoSizeAppointments";
            this.checkBoxAutoSizeAppointments.Size = new System.Drawing.Size(139, 18);
            this.checkBoxAutoSizeAppointments.TabIndex = 1;
            this.checkBoxAutoSizeAppointments.Text = "Auto size appointments";
            // 
            // checkBoxExactTimeRendering
            // 
            this.checkBoxExactTimeRendering.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxExactTimeRendering.Location = new System.Drawing.Point(5, 21);
            this.checkBoxExactTimeRendering.Name = "checkBoxExactTimeRendering";
            this.checkBoxExactTimeRendering.Size = new System.Drawing.Size(123, 18);
            this.checkBoxExactTimeRendering.TabIndex = 0;
            this.checkBoxExactTimeRendering.Text = "Exact time rendering";
            // 
            // timePickerRulerStart
            // 
            this.timePickerRulerStart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.timePickerRulerStart.Location = new System.Drawing.Point(90, 3);
            this.timePickerRulerStart.Name = "timePickerRulerStart";
            this.timePickerRulerStart.Size = new System.Drawing.Size(107, 20);
            this.timePickerRulerStart.TabIndex = 8;
            this.timePickerRulerStart.TabStop = false;
            this.timePickerRulerStart.Text = "radTimePicker1";
            this.timePickerRulerStart.Value = new System.DateTime(2014, 6, 9, 14, 58, 13, 0);
            // 
            // timePickerRulerEnd
            // 
            this.timePickerRulerEnd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.timePickerRulerEnd.Location = new System.Drawing.Point(90, 29);
            this.timePickerRulerEnd.Name = "timePickerRulerEnd";
            this.timePickerRulerEnd.Size = new System.Drawing.Size(107, 20);
            this.timePickerRulerEnd.TabIndex = 9;
            this.timePickerRulerEnd.TabStop = false;
            this.timePickerRulerEnd.Text = "radTimePicker2";
            this.timePickerRulerEnd.Value = new System.DateTime(2014, 6, 9, 14, 58, 13, 0);
            // 
            // buttonBackToMonthView
            // 
            this.buttonBackToMonthView.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonBackToMonthView.Location = new System.Drawing.Point(10, 33);
            this.buttonBackToMonthView.Name = "buttonBackToMonthView";
            this.buttonBackToMonthView.Size = new System.Drawing.Size(210, 24);
            this.buttonBackToMonthView.TabIndex = 4;
            this.buttonBackToMonthView.Text = "Back to Month View";
            this.buttonBackToMonthView.Visible = false;
            this.buttonBackToMonthView.Click += new System.EventHandler(this.buttonBackToMonthView_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radScheduler1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1541, 1000);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radScheduler1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScheduler1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxCellsOverflow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxAppointmentsScrolling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxEnableWeeksHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowWeeksHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxVerticalScroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownWorkWeekEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownWorkWeekStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorWeekCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowWeekend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowFullMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColumnSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRowSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorAppointmentSpacing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxAutoSizeAppointments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExactTimeRendering)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerRulerStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerRulerEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBackToMonthView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadScheduler radScheduler1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorAppointmentSpacing;
        private Telerik.WinControls.UI.RadCheckBox checkBoxAutoSizeAppointments;
        private Telerik.WinControls.UI.RadCheckBox checkBoxExactTimeRendering;
        private Telerik.WinControls.UI.RadCheckBox checkBoxCellsOverflow;
        private Telerik.WinControls.UI.RadCheckBox checkBoxAppointmentsScrolling;
        private Telerik.WinControls.UI.RadCheckBox checkBoxEnableWeeksHeader;
        private Telerik.WinControls.UI.RadCheckBox checkBoxShowWeeksHeader;
        private Telerik.WinControls.UI.RadCheckBox checkBoxVerticalScroll;
        private Telerik.WinControls.UI.RadTrackBar trackBarColumnSize;
        private Telerik.WinControls.UI.RadTrackBar trackBarRowSize;
        private Telerik.WinControls.UI.RadCheckBox checkBoxShowWeekend;
        private Telerik.WinControls.UI.RadCheckBox checkBoxShowFullMonth;
        private Telerik.WinControls.UI.RadTimePicker timePickerRulerStart;
        private Telerik.WinControls.UI.RadTimePicker timePickerRulerEnd;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorWeekCount;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadDropDownList dropDownWorkWeekEnd;
        private Telerik.WinControls.UI.RadDropDownList dropDownWorkWeekStart;
        private Telerik.WinControls.UI.RadButton buttonBackToMonthView;
    }
}