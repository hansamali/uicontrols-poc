﻿namespace Telerik.Examples.WinControls.Scheduler.DayView
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.DateTimeInterval dateTimeInterval1 = new Telerik.WinControls.UI.DateTimeInterval();
            Telerik.WinControls.UI.SchedulerDailyPrintStyle schedulerDailyPrintStyle1 = new Telerik.WinControls.UI.SchedulerDailyPrintStyle();
            this.radScheduler1 = new Telerik.WinControls.UI.RadScheduler();
            this.radSchedulerNavigator1 = new Telerik.WinControls.UI.RadSchedulerNavigator();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.dropDownRangeFactor = new Telerik.WinControls.UI.RadDropDownList();
            this.spinEditorRulerWidth = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinEditorPointerWidth = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinEditorScaleSize = new Telerik.WinControls.UI.RadSpinEditor();
            this.timePickerRulerEnd = new Telerik.WinControls.UI.RadTimePicker();
            this.timePickerRulerStart = new Telerik.WinControls.UI.RadTimePicker();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.dropDownWorkWeekEnd = new Telerik.WinControls.UI.RadDropDownList();
            this.dropDownWorkWeekStart = new Telerik.WinControls.UI.RadDropDownList();
            this.timePickerWorkTimeEnd = new Telerik.WinControls.UI.RadTimePicker();
            this.timePickerWorkTimeStart = new Telerik.WinControls.UI.RadTimePicker();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.spinEditorAllDayMaxHeight = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.spinEditorAllDayHeight = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinEditorAppointmentSpacing = new Telerik.WinControls.UI.RadSpinEditor();
            this.checkBoxExactTimeRendering = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxShowHeader = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxShowAllDayArea = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxShowRuler = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxShowWeekend = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.radTrackBar1 = new Telerik.WinControls.UI.RadTrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScheduler1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSchedulerNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownRangeFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorRulerWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorPointerWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorScaleSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerRulerEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerRulerStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownWorkWeekEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownWorkWeekStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerWorkTimeEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerWorkTimeStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorAllDayMaxHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorAllDayHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorAppointmentSpacing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExactTimeRendering)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowAllDayArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowRuler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowWeekend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox4);
            this.settingsPanel.Controls.Add(this.radGroupBox3);
            this.settingsPanel.Controls.Add(this.radGroupBox2);
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(838, 37);
            this.settingsPanel.Size = new System.Drawing.Size(230, 713);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox3, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox4, 0);
            // 
            // radScheduler1
            // 
            dateTimeInterval1.End = new System.DateTime(((long)(0)));
            dateTimeInterval1.Start = new System.DateTime(((long)(0)));
            this.radScheduler1.AccessibleInterval = dateTimeInterval1;
            this.radScheduler1.Culture = new System.Globalization.CultureInfo("en-US");
            this.radScheduler1.DataSource = null;
            this.radScheduler1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScheduler1.Location = new System.Drawing.Point(0, 77);
            this.radScheduler1.Name = "radScheduler1";
            schedulerDailyPrintStyle1.AppointmentFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            schedulerDailyPrintStyle1.DateEndRange = new System.DateTime(2014, 6, 14, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.DateHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            schedulerDailyPrintStyle1.DateStartRange = new System.DateTime(2014, 6, 9, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.PageHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold);
            this.radScheduler1.PrintStyle = schedulerDailyPrintStyle1;
            this.radScheduler1.Size = new System.Drawing.Size(1487, 913);
            this.radScheduler1.TabIndex = 2;
            this.radScheduler1.Text = "radScheduler1";
            // 
            // radSchedulerNavigator1
            // 
            this.radSchedulerNavigator1.AssociatedScheduler = this.radScheduler1;
            this.radSchedulerNavigator1.DateFormat = "yyyy/MM/dd";
            this.radSchedulerNavigator1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radSchedulerNavigator1.Location = new System.Drawing.Point(0, 0);
            this.radSchedulerNavigator1.Name = "radSchedulerNavigator1";
            this.radSchedulerNavigator1.NavigationStepType = Telerik.WinControls.UI.NavigationStepTypes.Day;
            // 
            // 
            // 
            this.radSchedulerNavigator1.RootElement.StretchVertically = false;
            this.radSchedulerNavigator1.Size = new System.Drawing.Size(1487, 77);
            this.radSchedulerNavigator1.TabIndex = 3;
            this.radSchedulerNavigator1.Text = "radSchedulerNavigator1";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.panel1);
            this.radGroupBox1.HeaderText = "Ruler options";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 40);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(210, 179);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Ruler options";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.dropDownRangeFactor);
            this.panel1.Controls.Add(this.spinEditorRulerWidth);
            this.panel1.Controls.Add(this.spinEditorPointerWidth);
            this.panel1.Controls.Add(this.spinEditorScaleSize);
            this.panel1.Controls.Add(this.timePickerRulerEnd);
            this.panel1.Controls.Add(this.timePickerRulerStart);
            this.panel1.Location = new System.Drawing.Point(5, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 164);
            this.panel1.TabIndex = 9;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(3, 134);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(73, 18);
            this.radLabel6.TabIndex = 19;
            this.radLabel6.Text = "Pointer width";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(3, 108);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(63, 18);
            this.radLabel5.TabIndex = 18;
            this.radLabel5.Text = "Ruler width";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(3, 81);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(70, 18);
            this.radLabel4.TabIndex = 17;
            this.radLabel4.Text = "Range factor";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(3, 56);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(54, 18);
            this.radLabel3.TabIndex = 16;
            this.radLabel3.Text = "Scale size";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(3, 30);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(54, 18);
            this.radLabel2.TabIndex = 15;
            this.radLabel2.Text = "Ruler end";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(57, 18);
            this.radLabel1.TabIndex = 14;
            this.radLabel1.Text = "Ruler start";
            // 
            // dropDownRangeFactor
            // 
            this.dropDownRangeFactor.AllowShowFocusCues = false;
            this.dropDownRangeFactor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dropDownRangeFactor.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.dropDownRangeFactor.Location = new System.Drawing.Point(90, 81);
            this.dropDownRangeFactor.Name = "dropDownRangeFactor";
            this.dropDownRangeFactor.Size = new System.Drawing.Size(107, 20);
            this.dropDownRangeFactor.TabIndex = 13;
            this.dropDownRangeFactor.Text = "radDropDownList1";
            // 
            // spinEditorRulerWidth
            // 
            this.spinEditorRulerWidth.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorRulerWidth.Location = new System.Drawing.Point(90, 107);
            this.spinEditorRulerWidth.Name = "spinEditorRulerWidth";
            this.spinEditorRulerWidth.Size = new System.Drawing.Size(107, 20);
            this.spinEditorRulerWidth.TabIndex = 12;
            this.spinEditorRulerWidth.TabStop = false;
            // 
            // spinEditorPointerWidth
            // 
            this.spinEditorPointerWidth.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorPointerWidth.Location = new System.Drawing.Point(90, 133);
            this.spinEditorPointerWidth.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinEditorPointerWidth.Name = "spinEditorPointerWidth";
            this.spinEditorPointerWidth.Size = new System.Drawing.Size(107, 20);
            this.spinEditorPointerWidth.TabIndex = 11;
            this.spinEditorPointerWidth.TabStop = false;
            // 
            // spinEditorScaleSize
            // 
            this.spinEditorScaleSize.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorScaleSize.Location = new System.Drawing.Point(90, 55);
            this.spinEditorScaleSize.Minimum = new decimal(new int[] {
            21,
            0,
            0,
            0});
            this.spinEditorScaleSize.Name = "spinEditorScaleSize";
            this.spinEditorScaleSize.Size = new System.Drawing.Size(107, 20);
            this.spinEditorScaleSize.TabIndex = 10;
            this.spinEditorScaleSize.TabStop = false;
            this.spinEditorScaleSize.Value = new decimal(new int[] {
            22,
            0,
            0,
            0});
            // 
            // timePickerRulerEnd
            // 
            this.timePickerRulerEnd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.timePickerRulerEnd.Location = new System.Drawing.Point(90, 29);
            this.timePickerRulerEnd.Name = "timePickerRulerEnd";
            this.timePickerRulerEnd.Size = new System.Drawing.Size(107, 20);
            this.timePickerRulerEnd.TabIndex = 9;
            this.timePickerRulerEnd.TabStop = false;
            this.timePickerRulerEnd.Text = "radTimePicker2";
            this.timePickerRulerEnd.Value = new System.DateTime(2014, 6, 9, 14, 58, 13, 0);
            // 
            // timePickerRulerStart
            // 
            this.timePickerRulerStart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.timePickerRulerStart.Location = new System.Drawing.Point(90, 3);
            this.timePickerRulerStart.Name = "timePickerRulerStart";
            this.timePickerRulerStart.Size = new System.Drawing.Size(107, 20);
            this.timePickerRulerStart.TabIndex = 8;
            this.timePickerRulerStart.TabStop = false;
            this.timePickerRulerStart.Text = "radTimePicker1";
            this.timePickerRulerStart.Value = new System.DateTime(2014, 6, 9, 14, 58, 13, 0);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox2.Controls.Add(this.panel2);
            this.radGroupBox2.HeaderText = "Working time options";
            this.radGroupBox2.Location = new System.Drawing.Point(10, 225);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(210, 131);
            this.radGroupBox2.TabIndex = 2;
            this.radGroupBox2.Text = "Working time options";
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.Controls.Add(this.radLabel7);
            this.panel2.Controls.Add(this.radLabel8);
            this.panel2.Controls.Add(this.radLabel9);
            this.panel2.Controls.Add(this.radLabel10);
            this.panel2.Controls.Add(this.dropDownWorkWeekEnd);
            this.panel2.Controls.Add(this.dropDownWorkWeekStart);
            this.panel2.Controls.Add(this.timePickerWorkTimeEnd);
            this.panel2.Controls.Add(this.timePickerWorkTimeStart);
            this.panel2.Location = new System.Drawing.Point(5, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 113);
            this.panel2.TabIndex = 9;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(3, 83);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(100, 18);
            this.radLabel7.TabIndex = 21;
            this.radLabel7.Text = "Working week end";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(3, 58);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(103, 18);
            this.radLabel8.TabIndex = 20;
            this.radLabel8.Text = "Working week start";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(3, 32);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(80, 18);
            this.radLabel9.TabIndex = 19;
            this.radLabel9.Text = "Work time end";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(3, 6);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(84, 18);
            this.radLabel10.TabIndex = 18;
            this.radLabel10.Text = "Work time start";
            // 
            // dropDownWorkWeekEnd
            // 
            this.dropDownWorkWeekEnd.AllowShowFocusCues = false;
            this.dropDownWorkWeekEnd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dropDownWorkWeekEnd.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.dropDownWorkWeekEnd.Location = new System.Drawing.Point(120, 83);
            this.dropDownWorkWeekEnd.Name = "dropDownWorkWeekEnd";
            this.dropDownWorkWeekEnd.Size = new System.Drawing.Size(78, 20);
            this.dropDownWorkWeekEnd.TabIndex = 13;
            this.dropDownWorkWeekEnd.Text = "radDropDownList3";
            // 
            // dropDownWorkWeekStart
            // 
            this.dropDownWorkWeekStart.AllowShowFocusCues = false;
            this.dropDownWorkWeekStart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dropDownWorkWeekStart.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.dropDownWorkWeekStart.Location = new System.Drawing.Point(120, 57);
            this.dropDownWorkWeekStart.Name = "dropDownWorkWeekStart";
            this.dropDownWorkWeekStart.Size = new System.Drawing.Size(78, 20);
            this.dropDownWorkWeekStart.TabIndex = 12;
            this.dropDownWorkWeekStart.Text = "radDropDownList2";
            // 
            // timePickerWorkTimeEnd
            // 
            this.timePickerWorkTimeEnd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.timePickerWorkTimeEnd.Location = new System.Drawing.Point(103, 31);
            this.timePickerWorkTimeEnd.Name = "timePickerWorkTimeEnd";
            this.timePickerWorkTimeEnd.Size = new System.Drawing.Size(95, 20);
            this.timePickerWorkTimeEnd.TabIndex = 11;
            this.timePickerWorkTimeEnd.TabStop = false;
            this.timePickerWorkTimeEnd.Text = "radTimePicker3";
            this.timePickerWorkTimeEnd.Value = new System.DateTime(2014, 6, 9, 14, 58, 13, 0);
            // 
            // timePickerWorkTimeStart
            // 
            this.timePickerWorkTimeStart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.timePickerWorkTimeStart.Location = new System.Drawing.Point(103, 5);
            this.timePickerWorkTimeStart.Name = "timePickerWorkTimeStart";
            this.timePickerWorkTimeStart.Size = new System.Drawing.Size(95, 20);
            this.timePickerWorkTimeStart.TabIndex = 10;
            this.timePickerWorkTimeStart.TabStop = false;
            this.timePickerWorkTimeStart.Text = "radTimePicker4";
            this.timePickerWorkTimeStart.Value = new System.DateTime(2014, 6, 9, 14, 58, 13, 0);
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox3.Controls.Add(this.panel3);
            this.radGroupBox3.Controls.Add(this.checkBoxExactTimeRendering);
            this.radGroupBox3.Controls.Add(this.checkBoxShowHeader);
            this.radGroupBox3.Controls.Add(this.checkBoxShowAllDayArea);
            this.radGroupBox3.Controls.Add(this.checkBoxShowRuler);
            this.radGroupBox3.Controls.Add(this.checkBoxShowWeekend);
            this.radGroupBox3.HeaderText = "General options";
            this.radGroupBox3.Location = new System.Drawing.Point(10, 362);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(210, 227);
            this.radGroupBox3.TabIndex = 3;
            this.radGroupBox3.Text = "General options";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.Controls.Add(this.radLabel13);
            this.panel3.Controls.Add(this.spinEditorAllDayMaxHeight);
            this.panel3.Controls.Add(this.radLabel11);
            this.panel3.Controls.Add(this.radLabel12);
            this.panel3.Controls.Add(this.spinEditorAllDayHeight);
            this.panel3.Controls.Add(this.spinEditorAppointmentSpacing);
            this.panel3.Location = new System.Drawing.Point(5, 141);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 81);
            this.panel3.TabIndex = 9;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(3, 30);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(123, 18);
            this.radLabel13.TabIndex = 19;
            this.radLabel13.Text = "All day area max height";
            // 
            // spinEditorAllDayMaxHeight
            // 
            this.spinEditorAllDayMaxHeight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorAllDayMaxHeight.Location = new System.Drawing.Point(140, 29);
            this.spinEditorAllDayMaxHeight.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.spinEditorAllDayMaxHeight.Name = "spinEditorAllDayMaxHeight";
            this.spinEditorAllDayMaxHeight.Size = new System.Drawing.Size(58, 20);
            this.spinEditorAllDayMaxHeight.TabIndex = 18;
            this.spinEditorAllDayMaxHeight.TabStop = false;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(3, 56);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(119, 18);
            this.radLabel11.TabIndex = 17;
            this.radLabel11.Text = "Appointments spacing";
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(3, 4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(100, 18);
            this.radLabel12.TabIndex = 16;
            this.radLabel12.Text = "All day area height";
            // 
            // spinEditorAllDayHeight
            // 
            this.spinEditorAllDayHeight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorAllDayHeight.Location = new System.Drawing.Point(140, 3);
            this.spinEditorAllDayHeight.Name = "spinEditorAllDayHeight";
            this.spinEditorAllDayHeight.Size = new System.Drawing.Size(58, 20);
            this.spinEditorAllDayHeight.TabIndex = 8;
            this.spinEditorAllDayHeight.TabStop = false;
            // 
            // spinEditorAppointmentSpacing
            // 
            this.spinEditorAppointmentSpacing.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorAppointmentSpacing.Location = new System.Drawing.Point(140, 55);
            this.spinEditorAppointmentSpacing.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.spinEditorAppointmentSpacing.Name = "spinEditorAppointmentSpacing";
            this.spinEditorAppointmentSpacing.Size = new System.Drawing.Size(58, 20);
            this.spinEditorAppointmentSpacing.TabIndex = 7;
            this.spinEditorAppointmentSpacing.TabStop = false;
            // 
            // checkBoxExactTimeRendering
            // 
            this.checkBoxExactTimeRendering.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxExactTimeRendering.Location = new System.Drawing.Point(5, 117);
            this.checkBoxExactTimeRendering.Name = "checkBoxExactTimeRendering";
            this.checkBoxExactTimeRendering.Size = new System.Drawing.Size(123, 18);
            this.checkBoxExactTimeRendering.TabIndex = 4;
            this.checkBoxExactTimeRendering.Text = "Exact time rendering";
            // 
            // checkBoxShowHeader
            // 
            this.checkBoxShowHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxShowHeader.Location = new System.Drawing.Point(5, 93);
            this.checkBoxShowHeader.Name = "checkBoxShowHeader";
            this.checkBoxShowHeader.Size = new System.Drawing.Size(151, 18);
            this.checkBoxShowHeader.TabIndex = 3;
            this.checkBoxShowHeader.Text = "Show the DayView header";
            // 
            // checkBoxShowAllDayArea
            // 
            this.checkBoxShowAllDayArea.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxShowAllDayArea.Location = new System.Drawing.Point(5, 69);
            this.checkBoxShowAllDayArea.Name = "checkBoxShowAllDayArea";
            this.checkBoxShowAllDayArea.Size = new System.Drawing.Size(126, 18);
            this.checkBoxShowAllDayArea.TabIndex = 2;
            this.checkBoxShowAllDayArea.Text = "Show the AllDay area";
            // 
            // checkBoxShowRuler
            // 
            this.checkBoxShowRuler.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxShowRuler.Location = new System.Drawing.Point(5, 45);
            this.checkBoxShowRuler.Name = "checkBoxShowRuler";
            this.checkBoxShowRuler.Size = new System.Drawing.Size(93, 18);
            this.checkBoxShowRuler.TabIndex = 1;
            this.checkBoxShowRuler.Text = "Show the ruler";
            // 
            // checkBoxShowWeekend
            // 
            this.checkBoxShowWeekend.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxShowWeekend.Location = new System.Drawing.Point(5, 21);
            this.checkBoxShowWeekend.Name = "checkBoxShowWeekend";
            this.checkBoxShowWeekend.Size = new System.Drawing.Size(114, 18);
            this.checkBoxShowWeekend.TabIndex = 0;
            this.checkBoxShowWeekend.Text = "Show the weekend";
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox4.Controls.Add(this.radTrackBar1);
            this.radGroupBox4.HeaderText = "Resize the selected column";
            this.radGroupBox4.Location = new System.Drawing.Point(10, 595);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(210, 84);
            this.radGroupBox4.TabIndex = 4;
            this.radGroupBox4.Text = "Resize the selected column";
            // 
            // radTrackBar1
            // 
            this.radTrackBar1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radTrackBar1.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.BottomRight;
            this.radTrackBar1.LargeTickFrequency = 1;
            this.radTrackBar1.Location = new System.Drawing.Point(5, 21);
            this.radTrackBar1.Maximum = 10F;
            this.radTrackBar1.Minimum = 1F;
            this.radTrackBar1.Name = "radTrackBar1";
            this.radTrackBar1.Size = new System.Drawing.Size(200, 55);
            this.radTrackBar1.TabIndex = 0;
            this.radTrackBar1.Text = "radTrackBar1";
            this.radTrackBar1.Value = 1F;
            this.radTrackBar1.ValueChanged += new System.EventHandler(this.radTrackBar1_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radScheduler1);
            this.Controls.Add(this.radSchedulerNavigator1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1497, 1000);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radSchedulerNavigator1, 0);
            this.Controls.SetChildIndex(this.radScheduler1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScheduler1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSchedulerNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownRangeFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorRulerWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorPointerWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorScaleSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerRulerEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerRulerStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownWorkWeekEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownWorkWeekStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerWorkTimeEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePickerWorkTimeStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorAllDayMaxHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorAllDayHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorAppointmentSpacing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExactTimeRendering)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowAllDayArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowRuler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxShowWeekend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadScheduler radScheduler1;
        private Telerik.WinControls.UI.RadSchedulerNavigator radSchedulerNavigator1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadCheckBox checkBoxShowHeader;
        private Telerik.WinControls.UI.RadCheckBox checkBoxShowAllDayArea;
        private Telerik.WinControls.UI.RadCheckBox checkBoxShowRuler;
        private Telerik.WinControls.UI.RadCheckBox checkBoxShowWeekend;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorAllDayHeight;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorAppointmentSpacing;
        private Telerik.WinControls.UI.RadCheckBox checkBoxExactTimeRendering;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadDropDownList dropDownRangeFactor;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorRulerWidth;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorPointerWidth;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorScaleSize;
        private Telerik.WinControls.UI.RadTimePicker timePickerRulerEnd;
        private Telerik.WinControls.UI.RadTimePicker timePickerRulerStart;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadDropDownList dropDownWorkWeekEnd;
        private Telerik.WinControls.UI.RadDropDownList dropDownWorkWeekStart;
        private Telerik.WinControls.UI.RadTimePicker timePickerWorkTimeEnd;
        private Telerik.WinControls.UI.RadTimePicker timePickerWorkTimeStart;
        private System.Windows.Forms.Panel panel3;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorAllDayMaxHeight;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar1;
    }
}