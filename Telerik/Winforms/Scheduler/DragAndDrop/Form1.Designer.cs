﻿namespace Telerik.Examples.WinControls.Scheduler.DragAndDrop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.DateTimeInterval dateTimeInterval2 = new Telerik.WinControls.UI.DateTimeInterval();
            Telerik.WinControls.UI.SchedulerDailyPrintStyle schedulerDailyPrintStyle2 = new Telerik.WinControls.UI.SchedulerDailyPrintStyle();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn10 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn7 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn8 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn11 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn12 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn4 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn7 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn8 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor4 = new Telerik.WinControls.Data.SortDescriptor();
            this.appointmentStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.schedulerDragAndDropDataDataSet = new Telerik.Examples.WinControls.DataSources.SchedulerDragAndDropDataDataSet();
            this.appointmentBackgroundsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radListBox1 = new Telerik.WinControls.UI.RadListControl();
            this.appointmentsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.radScheduler1 = new Telerik.WinControls.UI.RadScheduler();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.appointmentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radSchedulerNavigator1 = new Telerik.WinControls.UI.RadSchedulerNavigator();
            this.appointmentsTableAdapter = new Telerik.Examples.WinControls.DataSources.SchedulerDragAndDropDataDataSetTableAdapters.AppointmentsTableAdapter();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton2 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButtonAnywhere = new Telerik.WinControls.UI.RadRadioButton();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.checkBoxChangeView = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxExceptionDialog = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxValidationDialogs = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxAutoScroll = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxCreateExceptions = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerDragAndDropDataDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentBackgroundsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radListBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScheduler1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSchedulerNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonAnywhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxChangeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExceptionDialog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxValidationDialogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxAutoScroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxCreateExceptions)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox3);
            this.settingsPanel.Controls.Add(this.radGroupBox2);
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(1056, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 581);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox3, 0);
            // 
            // appointmentStatusesBindingSource
            // 
            this.appointmentStatusesBindingSource.DataMember = "AppointmentStatuses";
            this.appointmentStatusesBindingSource.DataSource = this.schedulerDragAndDropDataDataSet;
            // 
            // schedulerDragAndDropDataDataSet
            // 
            this.schedulerDragAndDropDataDataSet.DataSetName = "SchedulerDragAndDropDataDataSet";
            this.schedulerDragAndDropDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // appointmentBackgroundsBindingSource
            // 
            this.appointmentBackgroundsBindingSource.DataMember = "AppointmentBackgrounds";
            this.appointmentBackgroundsBindingSource.DataSource = this.schedulerDragAndDropDataDataSet;
            // 
            // radListBox1
            // 
            this.radListBox1.DataSource = this.appointmentsBindingSource1;
            this.radListBox1.DisplayMember = "Summary";
            this.radListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radListBox1.Location = new System.Drawing.Point(0, 0);
            this.radListBox1.Name = "radListBox1";
            this.radListBox1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.radListBox1.Size = new System.Drawing.Size(466, 232);
            this.radListBox1.TabIndex = 8;
            // 
            // appointmentsBindingSource1
            // 
            this.appointmentsBindingSource1.DataMember = "Appointments";
            this.appointmentsBindingSource1.DataSource = this.schedulerDragAndDropDataDataSet;
            // 
            // radScheduler1
            // 
            dateTimeInterval2.End = new System.DateTime(((long)(0)));
            dateTimeInterval2.Start = new System.DateTime(((long)(0)));
            this.radScheduler1.AccessibleInterval = dateTimeInterval2;
            this.radScheduler1.AllowDrop = true;
            this.radScheduler1.Culture = new System.Globalization.CultureInfo("en-US");
            this.radScheduler1.DataSource = null;
            this.radScheduler1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScheduler1.Location = new System.Drawing.Point(0, 77);
            this.radScheduler1.Name = "radScheduler1";
            schedulerDailyPrintStyle2.AppointmentFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            schedulerDailyPrintStyle2.DateEndRange = new System.DateTime(2014, 6, 11, 0, 0, 0, 0);
            schedulerDailyPrintStyle2.DateHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            schedulerDailyPrintStyle2.DateStartRange = new System.DateTime(2014, 6, 6, 0, 0, 0, 0);
            schedulerDailyPrintStyle2.PageHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold);
            this.radScheduler1.PrintStyle = schedulerDailyPrintStyle2;
            this.radScheduler1.Size = new System.Drawing.Size(1487, 594);
            this.radScheduler1.TabIndex = 6;
            this.radScheduler1.Text = "radScheduler1";
            // 
            // radGridView1
            // 
            this.radGridView1.BackColor = System.Drawing.SystemColors.Control;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            // 
            // radGridView1
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowDeleteRow = false;
            gridViewDecimalColumn10.DataType = typeof(int);
            gridViewDecimalColumn10.FieldName = "ID";
            gridViewDecimalColumn10.HeaderText = "ID";
            gridViewDecimalColumn10.IsAutoGenerated = true;
            gridViewDecimalColumn10.IsVisible = false;
            gridViewDecimalColumn10.Name = "ID";
            gridViewTextBoxColumn13.FieldName = "Summary";
            gridViewTextBoxColumn13.HeaderText = "Summary";
            gridViewTextBoxColumn13.IsAutoGenerated = true;
            gridViewTextBoxColumn13.Name = "Summary";
            gridViewDateTimeColumn7.FieldName = "Start";
            gridViewDateTimeColumn7.HeaderText = "Start";
            gridViewDateTimeColumn7.IsAutoGenerated = true;
            gridViewDateTimeColumn7.Name = "Start";
            gridViewDateTimeColumn8.FieldName = "End";
            gridViewDateTimeColumn8.HeaderText = "End";
            gridViewDateTimeColumn8.IsAutoGenerated = true;
            gridViewDateTimeColumn8.Name = "End";
            gridViewTextBoxColumn14.FieldName = "RecurrenceRule";
            gridViewTextBoxColumn14.HeaderText = "RecurrenceRule";
            gridViewTextBoxColumn14.IsAutoGenerated = true;
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "RecurrenceRule";
            gridViewDecimalColumn11.DataType = typeof(int);
            gridViewDecimalColumn11.FieldName = "MasterEventID";
            gridViewDecimalColumn11.HeaderText = "MasterEventID";
            gridViewDecimalColumn11.IsAutoGenerated = true;
            gridViewDecimalColumn11.IsVisible = false;
            gridViewDecimalColumn11.Name = "MasterEventID";
            gridViewTextBoxColumn15.FieldName = "Location";
            gridViewTextBoxColumn15.HeaderText = "Location";
            gridViewTextBoxColumn15.IsAutoGenerated = true;
            gridViewTextBoxColumn15.Name = "Location";
            gridViewTextBoxColumn16.FieldName = "Description";
            gridViewTextBoxColumn16.HeaderText = "Description";
            gridViewTextBoxColumn16.IsAutoGenerated = true;
            gridViewTextBoxColumn16.MaxWidth = 50;
            gridViewTextBoxColumn16.Name = "Description";
            gridViewTextBoxColumn16.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn16.Width = 5;
            gridViewDecimalColumn12.DataType = typeof(int);
            gridViewDecimalColumn12.FieldName = "ParentID";
            gridViewDecimalColumn12.HeaderText = "ParentID";
            gridViewDecimalColumn12.IsAutoGenerated = true;
            gridViewDecimalColumn12.IsVisible = false;
            gridViewDecimalColumn12.Name = "ParentID";
            gridViewCheckBoxColumn4.FieldName = "Visible";
            gridViewCheckBoxColumn4.HeaderText = "Visible";
            gridViewCheckBoxColumn4.IsAutoGenerated = true;
            gridViewCheckBoxColumn4.IsVisible = false;
            gridViewCheckBoxColumn4.Name = "Visible";
            gridViewComboBoxColumn7.DataSource = this.appointmentStatusesBindingSource;
            gridViewComboBoxColumn7.DataType = typeof(int);
            gridViewComboBoxColumn7.DisplayMember = "Name";
            gridViewComboBoxColumn7.FieldName = "StatusID";
            gridViewComboBoxColumn7.HeaderText = "Status";
            gridViewComboBoxColumn7.Name = "StatusID";
            gridViewComboBoxColumn7.ValueMember = "ID";
            gridViewComboBoxColumn8.DataSource = this.appointmentBackgroundsBindingSource;
            gridViewComboBoxColumn8.DataType = typeof(int);
            gridViewComboBoxColumn8.DisplayMember = "Name";
            gridViewComboBoxColumn8.FieldName = "BackgroundID";
            gridViewComboBoxColumn8.HeaderText = "Background";
            gridViewComboBoxColumn8.Name = "BackgroundID";
            gridViewComboBoxColumn8.ValueMember = "ID";
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn10,
            gridViewTextBoxColumn13,
            gridViewDateTimeColumn7,
            gridViewDateTimeColumn8,
            gridViewTextBoxColumn14,
            gridViewDecimalColumn11,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewDecimalColumn12,
            gridViewCheckBoxColumn4,
            gridViewComboBoxColumn7,
            gridViewComboBoxColumn8});
            this.radGridView1.MasterTemplate.DataSource = this.appointmentsBindingSource;
            sortDescriptor4.PropertyName = "Description";
            this.radGridView1.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor4});
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView1.ShowGroupPanel = false;
            this.radGridView1.Size = new System.Drawing.Size(1017, 232);
            this.radGridView1.TabIndex = 2;
            // 
            // appointmentsBindingSource
            // 
            this.appointmentsBindingSource.DataMember = "Appointments";
            this.appointmentsBindingSource.DataSource = this.schedulerDragAndDropDataDataSet;
            // 
            // radSchedulerNavigator1
            // 
            this.radSchedulerNavigator1.AssociatedScheduler = null;
            this.radSchedulerNavigator1.DateFormat = "yyyy/MM/dd";
            this.radSchedulerNavigator1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radSchedulerNavigator1.Location = new System.Drawing.Point(0, 0);
            this.radSchedulerNavigator1.MinimumSize = new System.Drawing.Size(400, 74);
            this.radSchedulerNavigator1.Name = "radSchedulerNavigator1";
            this.radSchedulerNavigator1.NavigationStepType = Telerik.WinControls.UI.NavigationStepTypes.Day;
            // 
            // 
            // 
            this.radSchedulerNavigator1.RootElement.MinSize = new System.Drawing.Size(400, 74);
            this.radSchedulerNavigator1.RootElement.StretchVertically = false;
            this.radSchedulerNavigator1.Size = new System.Drawing.Size(1487, 77);
            this.radSchedulerNavigator1.TabIndex = 9;
            this.radSchedulerNavigator1.Text = "radSchedulerNavigator1";
            // 
            // appointmentsTableAdapter
            // 
            this.appointmentsTableAdapter.ClearBeforeFill = true;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.radRadioButton2);
            this.radGroupBox1.Controls.Add(this.radRadioButtonAnywhere);
            this.radGroupBox1.HeaderText = "OLE Drag&Drop";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 37);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox1.Size = new System.Drawing.Size(180, 78);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "OLE Drag&Drop";
            // 
            // radRadioButton2
            // 
            this.radRadioButton2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radRadioButton2.Location = new System.Drawing.Point(5, 47);
            this.radRadioButton2.Name = "radRadioButton2";
            this.radRadioButton2.Size = new System.Drawing.Size(111, 18);
            this.radRadioButton2.TabIndex = 1;
            this.radRadioButton2.Text = "Keep original date";
            // 
            // radRadioButtonAnywhere
            // 
            this.radRadioButtonAnywhere.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radRadioButtonAnywhere.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButtonAnywhere.Location = new System.Drawing.Point(5, 23);
            this.radRadioButtonAnywhere.Name = "radRadioButtonAnywhere";
            this.radRadioButtonAnywhere.Size = new System.Drawing.Size(96, 18);
            this.radRadioButtonAnywhere.TabIndex = 0;
            this.radRadioButtonAnywhere.TabStop = true;
            this.radRadioButtonAnywhere.Text = "Drop anywhere";
            this.radRadioButtonAnywhere.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(1487, 907);
            this.radSplitContainer1.TabIndex = 11;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radScheduler1);
            this.splitPanel1.Controls.Add(this.radSchedulerNavigator1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(1487, 671);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2433628F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 110);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 675);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(1487, 232);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2433628F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -110);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer2.Size = new System.Drawing.Size(1487, 232);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            this.radSplitContainer2.Text = "radSplitContainer2";
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.radGridView1);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(1017, 232);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.1857708F, 0F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(100, 0);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.radListBox1);
            this.splitPanel4.Location = new System.Drawing.Point(1021, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel4.Size = new System.Drawing.Size(466, 232);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.1857708F, 0F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(-100, 0);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox2.Controls.Add(this.checkBoxValidationDialogs);
            this.radGroupBox2.Controls.Add(this.checkBoxExceptionDialog);
            this.radGroupBox2.Controls.Add(this.checkBoxChangeView);
            this.radGroupBox2.HeaderText = "AppointmentDraggingBehavior";
            this.radGroupBox2.Location = new System.Drawing.Point(10, 121);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(180, 100);
            this.radGroupBox2.TabIndex = 2;
            this.radGroupBox2.Text = "AppointmentDraggingBehavior";
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox3.Controls.Add(this.checkBoxCreateExceptions);
            this.radGroupBox3.Controls.Add(this.checkBoxAutoScroll);
            this.radGroupBox3.HeaderText = "DragDrop and Resize";
            this.radGroupBox3.Location = new System.Drawing.Point(10, 227);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(180, 72);
            this.radGroupBox3.TabIndex = 2;
            this.radGroupBox3.Text = "DragDrop and Resize";
            // 
            // checkBoxChangeView
            // 
            this.checkBoxChangeView.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxChangeView.Location = new System.Drawing.Point(5, 22);
            this.checkBoxChangeView.Name = "checkBoxChangeView";
            this.checkBoxChangeView.Size = new System.Drawing.Size(126, 18);
            this.checkBoxChangeView.TabIndex = 0;
            this.checkBoxChangeView.Text = "Navigate view on drag";
            this.checkBoxChangeView.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.checkBoxChangeView_ToggleStateChanged);
            // 
            // checkBoxExceptionDialog
            // 
            this.checkBoxExceptionDialog.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxExceptionDialog.Location = new System.Drawing.Point(5, 44);
            this.checkBoxExceptionDialog.Name = "checkBoxExceptionDialog";
            this.checkBoxExceptionDialog.Size = new System.Drawing.Size(139, 18);
            this.checkBoxExceptionDialog.TabIndex = 1;
            this.checkBoxExceptionDialog.Text = "Ask to create exceptions on drag";
            this.checkBoxExceptionDialog.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.checkBoxExceptionDialog_ToggleStateChanged);
            // 
            // checkBoxValidationDialogs
            // 
            this.checkBoxValidationDialogs.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxValidationDialogs.Location = new System.Drawing.Point(5, 68);
            this.checkBoxValidationDialogs.Name = "checkBoxValidationDialogs";
            this.checkBoxValidationDialogs.Size = new System.Drawing.Size(168, 18);
            this.checkBoxValidationDialogs.TabIndex = 2;
            this.checkBoxValidationDialogs.Text = "Occurrence validation dialogs";
            this.checkBoxValidationDialogs.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.checkBoxValidationDialogs_ToggleStateChanged);
            // 
            // checkBoxAutoScroll
            // 
            this.checkBoxAutoScroll.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxAutoScroll.Location = new System.Drawing.Point(5, 21);
            this.checkBoxAutoScroll.Name = "checkBoxAutoScroll";
            this.checkBoxAutoScroll.Size = new System.Drawing.Size(130, 18);
            this.checkBoxAutoScroll.TabIndex = 1;
            this.checkBoxAutoScroll.Text = "AutoScroll in DayView";
            this.checkBoxAutoScroll.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.checkBoxAutoScroll_ToggleStateChanged);
            // 
            // checkBoxCreateExceptions
            // 
            this.checkBoxCreateExceptions.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxCreateExceptions.Location = new System.Drawing.Point(5, 45);
            this.checkBoxCreateExceptions.Name = "checkBoxCreateExceptions";
            this.checkBoxCreateExceptions.Size = new System.Drawing.Size(167, 18);
            this.checkBoxCreateExceptions.TabIndex = 2;
            this.checkBoxCreateExceptions.Text = "Create occurrence exceptions";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radSplitContainer1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1497, 917);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radSplitContainer1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerDragAndDropDataDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentBackgroundsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radListBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScheduler1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSchedulerNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonAnywhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            this.splitPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxChangeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExceptionDialog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxValidationDialogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxAutoScroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxCreateExceptions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadListControl radListBox1;
        private Telerik.WinControls.UI.RadScheduler radScheduler1;
        private Telerik.WinControls.UI.RadSchedulerNavigator radSchedulerNavigator1;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.Examples.WinControls.DataSources.SchedulerDragAndDropDataDataSet schedulerDragAndDropDataDataSet;
        private System.Windows.Forms.BindingSource appointmentsBindingSource;
        private Telerik.Examples.WinControls.DataSources.SchedulerDragAndDropDataDataSetTableAdapters.AppointmentsTableAdapter appointmentsTableAdapter;
        private System.Windows.Forms.BindingSource appointmentStatusesBindingSource;
        private System.Windows.Forms.BindingSource appointmentBackgroundsBindingSource;
        private System.Windows.Forms.BindingSource appointmentsBindingSource1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonAnywhere;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadCheckBox checkBoxValidationDialogs;
        private Telerik.WinControls.UI.RadCheckBox checkBoxExceptionDialog;
        private Telerik.WinControls.UI.RadCheckBox checkBoxChangeView;
        private Telerik.WinControls.UI.RadCheckBox checkBoxCreateExceptions;
        private Telerik.WinControls.UI.RadCheckBox checkBoxAutoScroll;
    }
}
