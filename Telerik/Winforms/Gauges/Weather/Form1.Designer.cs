﻿namespace Telerik.Examples.WinControls.Gauges.Weather
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
       

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radialGaugeTicks6 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.labelSunny = new Telerik.WinControls.UI.RadLabel();
            this.labelFeelsLike = new Telerik.WinControls.UI.RadLabel();
            this.labelDegree = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLinearGauge1 = new Telerik.WinControls.UI.Gauges.RadLinearGauge();
            this.linearGaugeBar1 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeLine1 = new Telerik.WinControls.UI.Gauges.LinearGaugeLine();
            this.linearGaugeTicks1 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.linearGaugeLabels1 = new Telerik.WinControls.UI.Gauges.LinearGaugeLabels();
            this.linearGaugeTicks3 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.linearGaugeLine3 = new Telerik.WinControls.UI.Gauges.LinearGaugeLine();
            this.linearGaugeTicks4 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.linearGaugeNeedleIndicator1 = new Telerik.WinControls.UI.Gauges.LinearGaugeNeedleIndicator();
            this.linearGaugeNeedleIndicator2 = new Telerik.WinControls.UI.Gauges.LinearGaugeNeedleIndicator();
            this.radTrackBar1 = new Telerik.WinControls.UI.RadTrackBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.radLinearGauge2 = new Telerik.WinControls.UI.Gauges.RadLinearGauge();
            this.linearGaugeBar2 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeBar3 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeLine2 = new Telerik.WinControls.UI.Gauges.LinearGaugeLine();
            this.linearGaugeTicks2 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.linearGaugeLabels2 = new Telerik.WinControls.UI.Gauges.LinearGaugeLabels();
            this.linearGaugeTicks5 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.linearGaugeTicks7 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.radLinearGauge3 = new Telerik.WinControls.UI.Gauges.RadLinearGauge();
            this.linearGaugeBar4 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeBar5 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeBar6 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeLine4 = new Telerik.WinControls.UI.Gauges.LinearGaugeLine();
            this.linearGaugeTicks6 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.linearGaugeLabels3 = new Telerik.WinControls.UI.Gauges.LinearGaugeLabels();
            this.linearGaugeNeedleIndicator3 = new Telerik.WinControls.UI.Gauges.LinearGaugeNeedleIndicator();
            this.radLinearGauge4 = new Telerik.WinControls.UI.Gauges.RadLinearGauge();
            this.linearGaugeBar7 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeBar8 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeLine5 = new Telerik.WinControls.UI.Gauges.LinearGaugeLine();
            this.linearGaugeTicks8 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.radLinearGauge5 = new Telerik.WinControls.UI.Gauges.RadLinearGauge();
            this.linearGaugeBar9 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeBar10 = new Telerik.WinControls.UI.Gauges.LinearGaugeBar();
            this.linearGaugeLine6 = new Telerik.WinControls.UI.Gauges.LinearGaugeLine();
            this.linearGaugeTicks9 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.linearGaugeLabels4 = new Telerik.WinControls.UI.Gauges.LinearGaugeLabels();
            this.linearGaugeTicks10 = new Telerik.WinControls.UI.Gauges.LinearGaugeTicks();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.labelRainfall = new Telerik.WinControls.UI.RadLabel();
            this.labelHumidity = new Telerik.WinControls.UI.RadLabel();
            this.labelPressure = new Telerik.WinControls.UI.RadLabel();
            this.labelWindSpeed = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelSunny)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFeelsLike)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDegree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            this.radLabel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelRainfall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHumidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelWindSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1000, 33);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(1000, 199);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(149, 23);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "WEATHER FORECAST";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel10.Location = new System.Drawing.Point(38, 27);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(21, 21);
            this.radLabel10.TabIndex = 2;
            this.radLabel10.Text = "°C";
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel11.Location = new System.Drawing.Point(218, 63);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(247, 41);
            this.radLabel11.TabIndex = 2;
            this.radLabel11.Text = "Manilla, Philippines";
            // 
            // radialGaugeTicks6
            // 
            this.radialGaugeTicks6.AccessibleDescription = "radialGaugeTicks5";
            this.radialGaugeTicks6.AccessibleName = "radialGaugeTicks5";
            this.radialGaugeTicks6.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeTicks6.DrawText = false;
            this.radialGaugeTicks6.Name = "radialGaugeTicks6";
            this.radialGaugeTicks6.Text = "radialGaugeTicks5";
            this.radialGaugeTicks6.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeTicks6.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks6.TickEndIndexVisibleRange = 5F;
            this.radialGaugeTicks6.TicksCount = 18;
            this.radialGaugeTicks6.TicksRadiusPercentage = 103F;
            this.radialGaugeTicks6.TickStartIndexVisibleRange = 5F;
            this.radialGaugeTicks6.TickThickness = 1F;
            // 
            // labelSunny
            // 
            this.labelSunny.AutoSize = false;
            this.labelSunny.BackColor = System.Drawing.Color.Transparent;
            this.labelSunny.Font = new System.Drawing.Font("Segoe UI Light", 32F);
            this.labelSunny.Location = new System.Drawing.Point(212, 184);
            this.labelSunny.Name = "labelSunny";
            this.labelSunny.Size = new System.Drawing.Size(280, 54);
            this.labelSunny.TabIndex = 0;
            this.labelSunny.Text = "Sunny";
            this.labelSunny.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFeelsLike
            // 
            this.labelFeelsLike.AutoSize = false;
            this.labelFeelsLike.BackColor = System.Drawing.Color.Transparent;
            this.labelFeelsLike.Font = new System.Drawing.Font("Segoe UI Semilight", 14F, System.Drawing.FontStyle.Italic);
            this.labelFeelsLike.Location = new System.Drawing.Point(329, 151);
            this.labelFeelsLike.Name = "labelFeelsLike";
            this.labelFeelsLike.Size = new System.Drawing.Size(115, 27);
            this.labelFeelsLike.TabIndex = 0;
            this.labelFeelsLike.Text = "Feels like 24";
            // 
            // labelDegree
            // 
            this.labelDegree.AutoSize = false;
            this.labelDegree.Font = new System.Drawing.Font("Segoe UI Semilight", 46F);
            this.labelDegree.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.labelDegree.Location = new System.Drawing.Point(213, 110);
            this.labelDegree.Name = "labelDegree";
            this.labelDegree.Size = new System.Drawing.Size(91, 70);
            this.labelDegree.TabIndex = 7;
            this.labelDegree.Text = "20";
            this.labelDegree.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Font = new System.Drawing.Font("Segoe UI Semilight", 22F);
            this.radLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.radLabel15.Location = new System.Drawing.Point(305, 110);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(48, 35);
            this.radLabel15.TabIndex = 7;
            this.radLabel15.Text = "°C";
            // 
            // radLinearGauge1
            // 
            this.radLinearGauge1.BackColor = System.Drawing.Color.White;
            this.radLinearGauge1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.linearGaugeBar1,
            this.linearGaugeLine1,
            this.linearGaugeTicks1,
            this.linearGaugeLabels1,
            this.linearGaugeTicks3,
            this.linearGaugeLine3,
            this.linearGaugeTicks4,
            this.linearGaugeNeedleIndicator1,
            this.linearGaugeNeedleIndicator2});
            this.radLinearGauge1.Location = new System.Drawing.Point(15, 44);
            this.radLinearGauge1.Name = "radLinearGauge1";
            this.radLinearGauge1.Padding = new System.Windows.Forms.Padding(0, 5, 40, 5);
            this.radLinearGauge1.RangeEnd = 36F;
            this.radLinearGauge1.Size = new System.Drawing.Size(106, 220);
            this.radLinearGauge1.TabIndex = 0;
            this.radLinearGauge1.Text = "radLinearGauge1";
            this.radLinearGauge1.Value = 23F;
            this.radLinearGauge1.Vertical = true;
            // 
            // linearGaugeBar1
            // 
            this.linearGaugeBar1.AutoSize = false;
            this.linearGaugeBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar1.BindEndRange = true;
            this.linearGaugeBar1.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeBar1.Name = "linearGaugeBar1";
            this.linearGaugeBar1.Offset = 35F;
            this.linearGaugeBar1.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar1.RangeEnd = 23F;
            this.linearGaugeBar1.Width = 15F;
            this.linearGaugeBar1.Width2 = 15F;
            // 
            // linearGaugeLine1
            // 
            this.linearGaugeLine1.AutoSize = false;
            this.linearGaugeLine1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.linearGaugeLine1.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeLine1.Name = "linearGaugeLine1";
            this.linearGaugeLine1.Offset = 35F;
            this.linearGaugeLine1.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeLine1.RangeEnd = 36F;
            this.linearGaugeLine1.Width = 2F;
            // 
            // linearGaugeTicks1
            // 
            this.linearGaugeTicks1.AutoSize = false;
            this.linearGaugeTicks1.Bounds = new System.Drawing.Rectangle(0, 0, 59, 270);
            this.linearGaugeTicks1.Name = "linearGaugeTicks1";
            this.linearGaugeTicks1.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks1.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.linearGaugeTicks1.TicksCount = 3;
            this.linearGaugeTicks1.TicksLenghtPercentage = 4F;
            this.linearGaugeTicks1.TicksLocationPercentage = 33F;
            // 
            // linearGaugeLabels1
            // 
            this.linearGaugeLabels1.AutoSize = false;
            this.linearGaugeLabels1.Bounds = new System.Drawing.Rectangle(0, 0, 66, 211);
            this.linearGaugeLabels1.ForeColor = System.Drawing.Color.Black;
            this.linearGaugeLabels1.LabelLocationPercentage = 15F;
            this.linearGaugeLabels1.LabelsCount = 3;
            this.linearGaugeLabels1.Name = "linearGaugeLabels1";
            this.linearGaugeLabels1.Padding = new System.Windows.Forms.Padding(0);
            // 
            // linearGaugeTicks3
            // 
            this.linearGaugeTicks3.AutoSize = false;
            this.linearGaugeTicks3.Bounds = new System.Drawing.Rectangle(0, 0, 59, 270);
            this.linearGaugeTicks3.Name = "linearGaugeTicks3";
            this.linearGaugeTicks3.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks3.TicksCount = 12;
            this.linearGaugeTicks3.TicksLenghtPercentage = 2F;
            this.linearGaugeTicks3.TicksLocationPercentage = 33F;
            this.linearGaugeTicks3.TickThickness = 1.2F;
            // 
            // linearGaugeLine3
            // 
            this.linearGaugeLine3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.linearGaugeLine3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.linearGaugeLine3.Name = "linearGaugeLine3";
            this.linearGaugeLine3.Offset = 60F;
            this.linearGaugeLine3.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeLine3.RangeEnd = 120F;
            // 
            // linearGaugeTicks4
            // 
            this.linearGaugeTicks4.AutoSize = false;
            this.linearGaugeTicks4.Bounds = new System.Drawing.Rectangle(0, 0, 59, 270);
            this.linearGaugeTicks4.Name = "linearGaugeTicks4";
            this.linearGaugeTicks4.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks4.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.linearGaugeTicks4.TicksCount = 12;
            this.linearGaugeTicks4.TicksLenghtPercentage = 3F;
            this.linearGaugeTicks4.TicksLocationPercentage = 64F;
            // 
            // linearGaugeNeedleIndicator1
            // 
            this.linearGaugeNeedleIndicator1.AutoSize = false;
            this.linearGaugeNeedleIndicator1.BackColor = System.Drawing.Color.Red;
            this.linearGaugeNeedleIndicator1.Bounds = new System.Drawing.Rectangle(0, 0, 63, 270);
            this.linearGaugeNeedleIndicator1.CircleTicks = true;
            this.linearGaugeNeedleIndicator1.Direction = Telerik.WinControls.UI.Gauges.Direction.Left;
            this.linearGaugeNeedleIndicator1.DrawValue = true;
            this.linearGaugeNeedleIndicator1.ForeColor = System.Drawing.Color.Red;
            this.linearGaugeNeedleIndicator1.InnerPointRadiusPercentage = 10F;
            this.linearGaugeNeedleIndicator1.IsFilled = false;
            this.linearGaugeNeedleIndicator1.LenghtPercentage = 5F;
            this.linearGaugeNeedleIndicator1.LineLenght = 40F;
            this.linearGaugeNeedleIndicator1.LocationPercentage = 70F;
            this.linearGaugeNeedleIndicator1.Name = "linearGaugeNeedleIndicator1";
            this.linearGaugeNeedleIndicator1.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeNeedleIndicator1.PointRadiusPercentage = 10F;
            this.linearGaugeNeedleIndicator1.TextOffsetFromIndicator = new System.Drawing.SizeF(2F, -5F);
            this.linearGaugeNeedleIndicator1.Thickness = 2F;
            this.linearGaugeNeedleIndicator1.Value = 30F;
            // 
            // linearGaugeNeedleIndicator2
            // 
            this.linearGaugeNeedleIndicator2.AccessibleDescription = "linearGaugeNeedleIndicator2";
            this.linearGaugeNeedleIndicator2.AccessibleName = "linearGaugeNeedleIndicator2";
            this.linearGaugeNeedleIndicator2.AutoSize = false;
            this.linearGaugeNeedleIndicator2.Bounds = new System.Drawing.Rectangle(0, 0, 66, 270);
            this.linearGaugeNeedleIndicator2.CircleTicks = true;
            this.linearGaugeNeedleIndicator2.Direction = Telerik.WinControls.UI.Gauges.Direction.Left;
            this.linearGaugeNeedleIndicator2.DrawValue = true;
            this.linearGaugeNeedleIndicator2.IsFilled = false;
            this.linearGaugeNeedleIndicator2.LenghtPercentage = 5F;
            this.linearGaugeNeedleIndicator2.LineLenght = 40F;
            this.linearGaugeNeedleIndicator2.LocationPercentage = 70F;
            this.linearGaugeNeedleIndicator2.Name = "linearGaugeNeedleIndicator2";
            this.linearGaugeNeedleIndicator2.Text = "linearGaugeNeedleIndicator2";
            this.linearGaugeNeedleIndicator2.TextOffsetFromIndicator = new System.Drawing.SizeF(2F, -5F);
            this.linearGaugeNeedleIndicator2.Thickness = 2F;
            this.linearGaugeNeedleIndicator2.Value = 18F;
            // 
            // radTrackBar1
            // 
            this.radTrackBar1.LargeTickFrequency = 2;
            this.radTrackBar1.Location = new System.Drawing.Point(745, 33);
            this.radTrackBar1.Maximum = 12F;
            this.radTrackBar1.Name = "radTrackBar1";
            this.radTrackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // 
            // 
            this.radTrackBar1.RootElement.StretchHorizontally = false;
            this.radTrackBar1.RootElement.StretchVertically = true;
            this.radTrackBar1.Size = new System.Drawing.Size(37, 567);
            this.radTrackBar1.TabIndex = 8;
            this.radTrackBar1.Text = "radTrackBar1";
            this.radTrackBar1.TickStyle = Telerik.WinControls.Enumerations.TickStyles.BottomRight;
            this.radTrackBar1.Value = 4F;
            this.radTrackBar1.ValueChanged += new System.EventHandler(this.radTrackBar1_ValueChanged);
            ((Telerik.WinControls.UI.RadTrackBarElement)(this.radTrackBar1.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
            ((Telerik.WinControls.UI.RadTrackBarElement)(this.radTrackBar1.GetChildAt(0))).MinSize = new System.Drawing.Size(0, 100);
            ((Telerik.WinControls.UI.TrackBarLineElement)(this.radTrackBar1.GetChildAt(0).GetChildAt(1).GetChildAt(0).GetChildAt(1))).StretchHorizontally = false;
            ((Telerik.WinControls.UI.TrackBarIndicatorContainerElement)(this.radTrackBar1.GetChildAt(0).GetChildAt(1).GetChildAt(1))).StretchHorizontally = false;
            ((Telerik.WinControls.UI.TrackBarIndicatorElement)(this.radTrackBar1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
            ((Telerik.WinControls.UI.TrackBarRangeElement)(this.radTrackBar1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.TrackBarRangeElement)(this.radTrackBar1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(515, 79);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(164, 135);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // radLabel16
            // 
            this.radLabel16.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel16.Location = new System.Drawing.Point(797, 583);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(44, 24);
            this.radLabel16.TabIndex = 10;
            this.radLabel16.Text = "8 AM";
            // 
            // radLabel17
            // 
            this.radLabel17.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel17.Location = new System.Drawing.Point(798, 489);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(52, 24);
            this.radLabel17.TabIndex = 11;
            this.radLabel17.Text = "10 AM";
            // 
            // radLabel18
            // 
            this.radLabel18.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel18.Location = new System.Drawing.Point(798, 396);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(51, 24);
            this.radLabel18.TabIndex = 12;
            this.radLabel18.Text = "12 PM";
            // 
            // radLabel19
            // 
            this.radLabel19.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel19.Location = new System.Drawing.Point(798, 304);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(43, 24);
            this.radLabel19.TabIndex = 13;
            this.radLabel19.Text = "2 PM";
            // 
            // radLabel20
            // 
            this.radLabel20.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel20.Location = new System.Drawing.Point(798, 211);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(43, 24);
            this.radLabel20.TabIndex = 14;
            this.radLabel20.Text = "4 PM";
            // 
            // radLabel21
            // 
            this.radLabel21.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel21.Location = new System.Drawing.Point(797, 120);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(43, 24);
            this.radLabel21.TabIndex = 15;
            this.radLabel21.Text = "6 PM";
            // 
            // radLinearGauge2
            // 
            this.radLinearGauge2.BackColor = System.Drawing.Color.White;
            this.radLinearGauge2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.linearGaugeBar2,
            this.linearGaugeBar3,
            this.linearGaugeLine2,
            this.linearGaugeTicks2,
            this.linearGaugeLabels2,
            this.linearGaugeTicks5,
            this.linearGaugeTicks7});
            this.radLinearGauge2.Location = new System.Drawing.Point(15, 338);
            this.radLinearGauge2.Name = "radLinearGauge2";
            this.radLinearGauge2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.radLinearGauge2.RangeEnd = 3F;
            this.radLinearGauge2.Size = new System.Drawing.Size(60, 220);
            this.radLinearGauge2.TabIndex = 0;
            this.radLinearGauge2.Text = "radLinearGauge1";
            this.radLinearGauge2.Value = 1.9F;
            this.radLinearGauge2.Vertical = true;
            // 
            // linearGaugeBar2
            // 
            this.linearGaugeBar2.AutoSize = false;
            this.linearGaugeBar2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar2.BindEndRange = true;
            this.linearGaugeBar2.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeBar2.Name = "linearGaugeBar2";
            this.linearGaugeBar2.Offset = 35F;
            this.linearGaugeBar2.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar2.RangeEnd = 1.9F;
            this.linearGaugeBar2.Width = 15F;
            this.linearGaugeBar2.Width2 = 15F;
            // 
            // linearGaugeBar3
            // 
            this.linearGaugeBar3.AutoSize = false;
            this.linearGaugeBar3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(214)))), ((int)(((byte)(214)))));
            this.linearGaugeBar3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(214)))), ((int)(((byte)(214)))));
            this.linearGaugeBar3.BindStartRange = true;
            this.linearGaugeBar3.BindStartRangeOffset = 0.01F;
            this.linearGaugeBar3.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeBar3.Name = "linearGaugeBar3";
            this.linearGaugeBar3.Offset = 35F;
            this.linearGaugeBar3.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar3.RangeEnd = 3F;
            this.linearGaugeBar3.RangeStart = 1.91F;
            this.linearGaugeBar3.Width = 15F;
            this.linearGaugeBar3.Width2 = 15F;
            // 
            // linearGaugeLine2
            // 
            this.linearGaugeLine2.Name = "linearGaugeLine2";
            this.linearGaugeLine2.Offset = 35F;
            this.linearGaugeLine2.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeLine2.RangeEnd = 120F;
            // 
            // linearGaugeTicks2
            // 
            this.linearGaugeTicks2.AutoSize = true;
            this.linearGaugeTicks2.Name = "linearGaugeTicks2";
            this.linearGaugeTicks2.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks2.TicksCount = 3;
            this.linearGaugeTicks2.TicksLenghtPercentage = 9F;
            this.linearGaugeTicks2.TicksLocationPercentage = 30F;
            this.linearGaugeTicks2.TickThickness = 0.5F;
            // 
            // linearGaugeLabels2
            // 
            this.linearGaugeLabels2.AutoSize = false;
            this.linearGaugeLabels2.Bounds = new System.Drawing.Rectangle(0, 0, 60, 210);
            this.linearGaugeLabels2.LabelFontSize = 3F;
            this.linearGaugeLabels2.LabelLocationPercentage = 15F;
            this.linearGaugeLabels2.LabelsCount = 3;
            this.linearGaugeLabels2.Name = "linearGaugeLabels2";
            this.linearGaugeLabels2.Padding = new System.Windows.Forms.Padding(0);
            // 
            // linearGaugeTicks5
            // 
            this.linearGaugeTicks5.AutoSize = false;
            this.linearGaugeTicks5.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeTicks5.Name = "linearGaugeTicks5";
            this.linearGaugeTicks5.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks5.TicksCount = 12;
            this.linearGaugeTicks5.TicksLenghtPercentage = 3F;
            this.linearGaugeTicks5.TicksLocationPercentage = 30F;
            this.linearGaugeTicks5.TickThickness = 0.6F;
            // 
            // linearGaugeTicks7
            // 
            this.linearGaugeTicks7.AccessibleDescription = "linearGaugeTicks7";
            this.linearGaugeTicks7.AccessibleName = "linearGaugeTicks7";
            this.linearGaugeTicks7.AutoSize = false;
            this.linearGaugeTicks7.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeTicks7.Name = "linearGaugeTicks7";
            this.linearGaugeTicks7.Text = "linearGaugeTicks7";
            this.linearGaugeTicks7.TickColor = System.Drawing.Color.White;
            this.linearGaugeTicks7.TicksCount = 13;
            this.linearGaugeTicks7.TicksLenghtPercentage = 15F;
            this.linearGaugeTicks7.TicksLocationPercentage = 55F;
            // 
            // radLinearGauge3
            // 
            this.radLinearGauge3.BackColor = System.Drawing.Color.White;
            this.radLinearGauge3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.linearGaugeBar4,
            this.linearGaugeBar5,
            this.linearGaugeBar6,
            this.linearGaugeLine4,
            this.linearGaugeTicks6,
            this.linearGaugeLabels3,
            this.linearGaugeNeedleIndicator3});
            this.radLinearGauge3.Location = new System.Drawing.Point(197, 338);
            this.radLinearGauge3.Name = "radLinearGauge3";
            this.radLinearGauge3.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.radLinearGauge3.Size = new System.Drawing.Size(50, 220);
            this.radLinearGauge3.TabIndex = 2;
            this.radLinearGauge3.Text = "radLinearGauge2";
            this.radLinearGauge3.Value = 90F;
            this.radLinearGauge3.Vertical = true;
            // 
            // linearGaugeBar4
            // 
            this.linearGaugeBar4.AutoSize = true;
            this.linearGaugeBar4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(231)))), ((int)(((byte)(231)))));
            this.linearGaugeBar4.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.linearGaugeBar4.Name = "linearGaugeBar4";
            this.linearGaugeBar4.Offset = 20F;
            this.linearGaugeBar4.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar4.RangeEnd = 32F;
            this.linearGaugeBar4.Width = 5F;
            this.linearGaugeBar4.Width2 = 20F;
            // 
            // linearGaugeBar5
            // 
            this.linearGaugeBar5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.linearGaugeBar5.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(231)))), ((int)(((byte)(231)))));
            this.linearGaugeBar5.Name = "linearGaugeBar5";
            this.linearGaugeBar5.Offset = 20F;
            this.linearGaugeBar5.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar5.RangeEnd = 66F;
            this.linearGaugeBar5.RangeStart = 33.33F;
            this.linearGaugeBar5.Width = 20F;
            this.linearGaugeBar5.Width2 = 35F;
            // 
            // linearGaugeBar6
            // 
            this.linearGaugeBar6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar6.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.linearGaugeBar6.Name = "linearGaugeBar6";
            this.linearGaugeBar6.Offset = 20F;
            this.linearGaugeBar6.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar6.RangeEnd = 67F;
            this.linearGaugeBar6.RangeStart = 100F;
            this.linearGaugeBar6.Width = 50F;
            this.linearGaugeBar6.Width2 = 35F;
            // 
            // linearGaugeLine4
            // 
            this.linearGaugeLine4.AutoSize = false;
            this.linearGaugeLine4.Bounds = new System.Drawing.Rectangle(0, 0, 15, 285);
            this.linearGaugeLine4.Name = "linearGaugeLine4";
            this.linearGaugeLine4.Offset = 28F;
            this.linearGaugeLine4.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeLine4.RangeEnd = 120F;
            this.linearGaugeLine4.Width = 1.5F;
            // 
            // linearGaugeTicks6
            // 
            this.linearGaugeTicks6.AutoSize = false;
            this.linearGaugeTicks6.Bounds = new System.Drawing.Rectangle(0, 0, 15, 261);
            this.linearGaugeTicks6.Name = "linearGaugeTicks6";
            this.linearGaugeTicks6.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks6.TicksCount = 12;
            this.linearGaugeTicks6.TicksLenghtPercentage = 5F;
            this.linearGaugeTicks6.TicksLocationPercentage = 28F;
            this.linearGaugeTicks6.TickThickness = 0.5F;
            // 
            // linearGaugeLabels3
            // 
            this.linearGaugeLabels3.LabelLocationPercentage = 10F;
            this.linearGaugeLabels3.LabelsCount = 2;
            this.linearGaugeLabels3.Name = "linearGaugeLabels3";
            this.linearGaugeLabels3.Padding = new System.Windows.Forms.Padding(0);
            // 
            // linearGaugeNeedleIndicator3
            // 
            this.linearGaugeNeedleIndicator3.AutoSize = false;
            this.linearGaugeNeedleIndicator3.BindValue = true;
            this.linearGaugeNeedleIndicator3.Bounds = new System.Drawing.Rectangle(0, 0, 11, 223);
            this.linearGaugeNeedleIndicator3.Direction = Telerik.WinControls.UI.Gauges.Direction.Right;
            this.linearGaugeNeedleIndicator3.HorizontalLineWidth = 0;
            this.linearGaugeNeedleIndicator3.InnerPointRadiusPercentage = 10F;
            this.linearGaugeNeedleIndicator3.LocationPercentage = 12F;
            this.linearGaugeNeedleIndicator3.Name = "linearGaugeNeedleIndicator3";
            this.linearGaugeNeedleIndicator3.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeNeedleIndicator3.Value = 90F;
            // 
            // radLinearGauge4
            // 
            this.radLinearGauge4.BackColor = System.Drawing.Color.White;
            this.radLinearGauge4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.linearGaugeBar7,
            this.linearGaugeBar8,
            this.linearGaugeLine5,
            this.linearGaugeTicks8});
            this.radLinearGauge4.Location = new System.Drawing.Point(355, 338);
            this.radLinearGauge4.Name = "radLinearGauge4";
            this.radLinearGauge4.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.radLinearGauge4.RangeEnd = 120F;
            this.radLinearGauge4.Size = new System.Drawing.Size(60, 220);
            this.radLinearGauge4.TabIndex = 0;
            this.radLinearGauge4.Text = "radLinearGauge1";
            this.radLinearGauge4.Value = 70F;
            this.radLinearGauge4.Vertical = true;
            // 
            // linearGaugeBar7
            // 
            this.linearGaugeBar7.AutoSize = false;
            this.linearGaugeBar7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar7.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar7.BindEndRange = true;
            this.linearGaugeBar7.Bounds = new System.Drawing.Rectangle(0, 0, 60, 275);
            this.linearGaugeBar7.Name = "linearGaugeBar7";
            this.linearGaugeBar7.Offset = 35F;
            this.linearGaugeBar7.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar7.RangeEnd = 70F;
            this.linearGaugeBar7.Width = 15F;
            this.linearGaugeBar7.Width2 = 15F;
            // 
            // linearGaugeBar8
            // 
            this.linearGaugeBar8.AutoSize = false;
            this.linearGaugeBar8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(214)))), ((int)(((byte)(214)))));
            this.linearGaugeBar8.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(214)))), ((int)(((byte)(214)))));
            this.linearGaugeBar8.BindStartRange = true;
            this.linearGaugeBar8.BindStartRangeOffset = 1F;
            this.linearGaugeBar8.Bounds = new System.Drawing.Rectangle(0, 0, 60, 275);
            this.linearGaugeBar8.Name = "linearGaugeBar8";
            this.linearGaugeBar8.Offset = 35F;
            this.linearGaugeBar8.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar8.RangeEnd = 120F;
            this.linearGaugeBar8.RangeStart = 71F;
            this.linearGaugeBar8.Width = 15F;
            this.linearGaugeBar8.Width2 = 15F;
            // 
            // linearGaugeLine5
            // 
            this.linearGaugeLine5.Name = "linearGaugeLine5";
            this.linearGaugeLine5.Offset = 35F;
            this.linearGaugeLine5.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeLine5.RangeEnd = 120F;
            this.linearGaugeLine5.RangeStart = 80F;
            // 
            // linearGaugeTicks8
            // 
            this.linearGaugeTicks8.Name = "linearGaugeTicks8";
            this.linearGaugeTicks8.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks8.TickEndIndexVisibleRange = 1F;
            this.linearGaugeTicks8.TicksCount = 3;
            this.linearGaugeTicks8.TicksLenghtPercentage = 15F;
            this.linearGaugeTicks8.TicksLocationPercentage = 35F;
            this.linearGaugeTicks8.TickThickness = 0.7F;
            // 
            // radLinearGauge5
            // 
            this.radLinearGauge5.BackColor = System.Drawing.Color.White;
            this.radLinearGauge5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.linearGaugeBar9,
            this.linearGaugeBar10,
            this.linearGaugeLine6,
            this.linearGaugeTicks9,
            this.linearGaugeLabels4,
            this.linearGaugeTicks10});
            this.radLinearGauge5.Location = new System.Drawing.Point(515, 338);
            this.radLinearGauge5.Name = "radLinearGauge5";
            this.radLinearGauge5.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.radLinearGauge5.RangeEnd = 9F;
            this.radLinearGauge5.Size = new System.Drawing.Size(60, 220);
            this.radLinearGauge5.TabIndex = 0;
            this.radLinearGauge5.Text = "radLinearGauge1";
            this.radLinearGauge5.Value = 6F;
            this.radLinearGauge5.Vertical = true;
            // 
            // linearGaugeBar9
            // 
            this.linearGaugeBar9.AutoSize = false;
            this.linearGaugeBar9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar9.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.linearGaugeBar9.BindEndRange = true;
            this.linearGaugeBar9.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeBar9.Name = "linearGaugeBar9";
            this.linearGaugeBar9.Offset = 35F;
            this.linearGaugeBar9.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar9.RangeEnd = 6F;
            this.linearGaugeBar9.Width = 15F;
            this.linearGaugeBar9.Width2 = 15F;
            // 
            // linearGaugeBar10
            // 
            this.linearGaugeBar10.AutoSize = false;
            this.linearGaugeBar10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(214)))), ((int)(((byte)(214)))));
            this.linearGaugeBar10.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(214)))), ((int)(((byte)(214)))));
            this.linearGaugeBar10.BindStartRange = true;
            this.linearGaugeBar10.BindStartRangeOffset = 0.1F;
            this.linearGaugeBar10.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeBar10.Name = "linearGaugeBar10";
            this.linearGaugeBar10.Offset = 35F;
            this.linearGaugeBar10.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeBar10.RangeEnd = 9F;
            this.linearGaugeBar10.RangeStart = 6.1F;
            this.linearGaugeBar10.Width = 15F;
            this.linearGaugeBar10.Width2 = 15F;
            // 
            // linearGaugeLine6
            // 
            this.linearGaugeLine6.Name = "linearGaugeLine6";
            this.linearGaugeLine6.Offset = 35F;
            this.linearGaugeLine6.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeLine6.RangeEnd = 120F;
            // 
            // linearGaugeTicks9
            // 
            this.linearGaugeTicks9.AutoSize = true;
            this.linearGaugeTicks9.Name = "linearGaugeTicks9";
            this.linearGaugeTicks9.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks9.TicksCount = 3;
            this.linearGaugeTicks9.TicksLenghtPercentage = 9F;
            this.linearGaugeTicks9.TicksLocationPercentage = 30F;
            this.linearGaugeTicks9.TickThickness = 0.5F;
            // 
            // linearGaugeLabels4
            // 
            this.linearGaugeLabels4.AutoSize = false;
            this.linearGaugeLabels4.Bounds = new System.Drawing.Rectangle(0, 0, 60, 210);
            this.linearGaugeLabels4.LabelFontSize = 3F;
            this.linearGaugeLabels4.LabelLocationPercentage = 15F;
            this.linearGaugeLabels4.LabelsCount = 3;
            this.linearGaugeLabels4.Name = "linearGaugeLabels4";
            this.linearGaugeLabels4.Padding = new System.Windows.Forms.Padding(0);
            // 
            // linearGaugeTicks10
            // 
            this.linearGaugeTicks10.AutoSize = false;
            this.linearGaugeTicks10.Bounds = new System.Drawing.Rectangle(0, 0, 60, 270);
            this.linearGaugeTicks10.Name = "linearGaugeTicks10";
            this.linearGaugeTicks10.Padding = new System.Windows.Forms.Padding(0);
            this.linearGaugeTicks10.TicksCount = 12;
            this.linearGaugeTicks10.TicksLenghtPercentage = 3F;
            this.linearGaugeTicks10.TicksLocationPercentage = 30F;
            this.linearGaugeTicks10.TickThickness = 0.6F;
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel2.Location = new System.Drawing.Point(41, 315);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(31, 21);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "mm";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel3.Location = new System.Drawing.Point(215, 315);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(18, 21);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "%";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel4.Location = new System.Drawing.Point(375, 315);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(29, 21);
            this.radLabel4.TabIndex = 2;
            this.radLabel4.Text = "hPa";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel5.Location = new System.Drawing.Point(527, 315);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(30, 21);
            this.radLabel5.TabIndex = 2;
            this.radLabel5.Text = "m/s";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel6.Location = new System.Drawing.Point(342, 334);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(37, 21);
            this.radLabel6.TabIndex = 2;
            this.radLabel6.Text = "1050";
            // 
            // radLabel7
            // 
            this.radLabel7.Controls.Add(this.radLabel8);
            this.radLabel7.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel7.Location = new System.Drawing.Point(342, 404);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(37, 21);
            this.radLabel7.TabIndex = 2;
            this.radLabel7.Text = "1010";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel8.Location = new System.Drawing.Point(-43, 0);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(37, 21);
            this.radLabel8.TabIndex = 2;
            this.radLabel8.Text = "1010";
            // 
            // labelRainfall
            // 
            this.labelRainfall.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.labelRainfall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.labelRainfall.Location = new System.Drawing.Point(42, 585);
            this.labelRainfall.Name = "labelRainfall";
            this.labelRainfall.Size = new System.Drawing.Size(117, 37);
            this.labelRainfall.TabIndex = 16;
            this.labelRainfall.Text = "radLabel9";
            this.labelRainfall.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;            
            // 
            // labelHumidity
            // 
            this.labelHumidity.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.labelHumidity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.labelHumidity.Location = new System.Drawing.Point(218, 585);
            this.labelHumidity.Name = "labelHumidity";
            this.labelHumidity.Size = new System.Drawing.Size(117, 37);
            this.labelHumidity.TabIndex = 16;
            this.labelHumidity.Text = "radLabel9";
            this.labelHumidity.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            
            // 
            // labelPressure
            // 
            this.labelPressure.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.labelPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.labelPressure.Location = new System.Drawing.Point(389, 585);
            this.labelPressure.Name = "labelPressure";
            this.labelPressure.Size = new System.Drawing.Size(117, 37);
            this.labelPressure.TabIndex = 16;
            this.labelPressure.Text = "radLabel9";
            this.labelPressure.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            
            // 
            // labelWindSpeed
            // 
            this.labelWindSpeed.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.labelWindSpeed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.labelWindSpeed.Location = new System.Drawing.Point(548, 585);
            this.labelWindSpeed.Name = "labelWindSpeed";
            this.labelWindSpeed.Size = new System.Drawing.Size(117, 37);
            this.labelWindSpeed.TabIndex = 16;
            this.labelWindSpeed.Text = "radLabel9";
            this.labelWindSpeed.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.radLabel9.Location = new System.Drawing.Point(45, 556);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(56, 23);
            this.radLabel9.TabIndex = 17;
            this.radLabel9.Text = "Rainfall";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.radLabel12.Location = new System.Drawing.Point(220, 558);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(68, 23);
            this.radLabel12.TabIndex = 17;
            this.radLabel12.Text = "Humidity";
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.radLabel13.Location = new System.Drawing.Point(392, 558);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(64, 23);
            this.radLabel13.TabIndex = 17;
            this.radLabel13.Text = "Pressure";
            // 
            // radLabel14
            // 
            this.radLabel14.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.radLabel14.Location = new System.Drawing.Point(551, 558);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(87, 23);
            this.radLabel14.TabIndex = 17;
            this.radLabel14.Text = "Wind speed";
            // 
            // radLabel22
            // 
            this.radLabel22.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel22.Location = new System.Drawing.Point(796, 31);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(43, 24);
            this.radLabel22.TabIndex = 15;
            this.radLabel22.Text = "8 PM";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.radLabel14);
            this.Controls.Add(this.radLabel13);
            this.Controls.Add(this.radLabel12);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.labelWindSpeed);
            this.Controls.Add(this.labelPressure);
            this.Controls.Add(this.labelHumidity);
            this.Controls.Add(this.labelRainfall);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLinearGauge4);
            this.Controls.Add(this.radLinearGauge5);
            this.Controls.Add(this.radLinearGauge3);
            this.Controls.Add(this.radLinearGauge2);
            this.Controls.Add(this.radLabel15);
            this.Controls.Add(this.radLabel22);
            this.Controls.Add(this.radLabel21);
            this.Controls.Add(this.radLabel20);
            this.Controls.Add(this.radLabel19);
            this.Controls.Add(this.radLabel18);
            this.Controls.Add(this.radLabel17);
            this.Controls.Add(this.radLabel16);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.radTrackBar1);
            this.Controls.Add(this.labelSunny);
            this.Controls.Add(this.labelFeelsLike);
            this.Controls.Add(this.radLinearGauge1);
            this.Controls.Add(this.labelDegree);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.radLabel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1347, 695);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Controls.SetChildIndex(this.radLabel1, 0);
            this.Controls.SetChildIndex(this.radLabel10, 0);
            this.Controls.SetChildIndex(this.radLabel2, 0);
            this.Controls.SetChildIndex(this.radLabel3, 0);
            this.Controls.SetChildIndex(this.radLabel4, 0);
            this.Controls.SetChildIndex(this.radLabel5, 0);
            this.Controls.SetChildIndex(this.labelDegree, 0);
            this.Controls.SetChildIndex(this.radLinearGauge1, 0);
            this.Controls.SetChildIndex(this.labelFeelsLike, 0);
            this.Controls.SetChildIndex(this.labelSunny, 0);
            this.Controls.SetChildIndex(this.radTrackBar1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.radLabel16, 0);
            this.Controls.SetChildIndex(this.radLabel17, 0);
            this.Controls.SetChildIndex(this.radLabel18, 0);
            this.Controls.SetChildIndex(this.radLabel19, 0);
            this.Controls.SetChildIndex(this.radLabel20, 0);
            this.Controls.SetChildIndex(this.radLabel21, 0);
            this.Controls.SetChildIndex(this.radLabel22, 0);
            this.Controls.SetChildIndex(this.radLabel15, 0);
            this.Controls.SetChildIndex(this.radLinearGauge2, 0);
            this.Controls.SetChildIndex(this.radLinearGauge3, 0);
            this.Controls.SetChildIndex(this.radLinearGauge5, 0);
            this.Controls.SetChildIndex(this.radLinearGauge4, 0);
            this.Controls.SetChildIndex(this.radLabel6, 0);
            this.Controls.SetChildIndex(this.radLabel7, 0);
            this.Controls.SetChildIndex(this.labelRainfall, 0);
            this.Controls.SetChildIndex(this.labelHumidity, 0);
            this.Controls.SetChildIndex(this.labelPressure, 0);
            this.Controls.SetChildIndex(this.labelWindSpeed, 0);
            this.Controls.SetChildIndex(this.radLabel9, 0);
            this.Controls.SetChildIndex(this.radLabel12, 0);
            this.Controls.SetChildIndex(this.radLabel13, 0);
            this.Controls.SetChildIndex(this.radLabel14, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radLabel11, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelSunny)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFeelsLike)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDegree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLinearGauge5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            this.radLabel7.ResumeLayout(false);
            this.radLabel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelRainfall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHumidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelWindSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks6;
        private Telerik.WinControls.UI.RadLabel labelSunny;
        private Telerik.WinControls.UI.RadLabel labelFeelsLike;
        private Telerik.WinControls.UI.RadLabel labelDegree;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.Gauges.RadLinearGauge radLinearGauge1;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar1;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLine linearGaugeLine1;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks1;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLabels linearGaugeLabels1;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks3;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLine linearGaugeLine3;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks4;
        private Telerik.WinControls.UI.Gauges.LinearGaugeNeedleIndicator linearGaugeNeedleIndicator1;
        private Telerik.WinControls.UI.Gauges.LinearGaugeNeedleIndicator linearGaugeNeedleIndicator2;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.Gauges.RadLinearGauge radLinearGauge2;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar2;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar3;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLine linearGaugeLine2;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks2;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLabels linearGaugeLabels2;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks5;
        private Telerik.WinControls.UI.Gauges.RadLinearGauge radLinearGauge3;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar4;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar5;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar6;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLine linearGaugeLine4;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks6;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLabels linearGaugeLabels3;
        private Telerik.WinControls.UI.Gauges.LinearGaugeNeedleIndicator linearGaugeNeedleIndicator3;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks7;
        private Telerik.WinControls.UI.Gauges.RadLinearGauge radLinearGauge4;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar7;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar8;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLine linearGaugeLine5;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks8;
        private Telerik.WinControls.UI.Gauges.RadLinearGauge radLinearGauge5;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar9;
        private Telerik.WinControls.UI.Gauges.LinearGaugeBar linearGaugeBar10;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLine linearGaugeLine6;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks9;
        private Telerik.WinControls.UI.Gauges.LinearGaugeLabels linearGaugeLabels4;
        private Telerik.WinControls.UI.Gauges.LinearGaugeTicks linearGaugeTicks10;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel labelRainfall;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel labelHumidity;
        private Telerik.WinControls.UI.RadLabel labelPressure;
        private Telerik.WinControls.UI.RadLabel labelWindSpeed;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel22;
    }
}