﻿namespace Telerik.Examples.WinControls.Gauges.Settings
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            this.radRadialGauge1 = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.radialGaugeArc1 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc2 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc3 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeTicks1 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeLabels1 = new Telerik.WinControls.UI.Gauges.RadialGaugeLabels();
            this.radialGaugeNeedle1 = new Telerik.WinControls.UI.Gauges.RadialGaugeNeedle();
            this.spinNeedleLenght = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinArc1Radius = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinArc1Width = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinLabelRadius = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinLabelCount = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinTicksLenght = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinTickTickness = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinTicksCount = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinPointRadiusPercentage = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinInnerPointRadiusPercentage = new Telerik.WinControls.UI.RadSpinEditor();
            this.spinNeedleThickness = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.colorBoxTicks = new Telerik.WinControls.UI.RadColorBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.colorBoxArc = new Telerik.WinControls.UI.RadColorBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadialGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinNeedleLenght)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinArc1Radius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinArc1Width)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinLabelRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinLabelCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTicksLenght)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTickTickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTicksCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPointRadiusPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinInnerPointRadiusPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinNeedleThickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBoxTicks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBoxArc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(732, 8);
            this.settingsPanel.Size = new System.Drawing.Size(230, 755);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(701, 215);
            // 
            // radRadialGauge1
            // 
            this.radRadialGauge1.BackColor = System.Drawing.Color.White;
            this.radRadialGauge1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc1,
            this.radialGaugeArc2,
            this.radialGaugeArc3,
            this.radialGaugeTicks1,
            this.radialGaugeLabels1,
            this.radialGaugeNeedle1});
            this.radRadialGauge1.Location = new System.Drawing.Point(7, 7);
            this.radRadialGauge1.Name = "radRadialGauge1";
            this.radRadialGauge1.RangeEnd = 180D;
            this.radRadialGauge1.Size = new System.Drawing.Size(400, 400);
            this.radRadialGauge1.StartAngle = 130D;
            this.radRadialGauge1.SweepAngle = 280D;
            this.radRadialGauge1.TabIndex = 6;
            this.radRadialGauge1.Text = "radRadialGauge1";
            // 
            // radialGaugeArc1
            // 
            this.radialGaugeArc1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(206)))), ((int)(((byte)(103)))));
            this.radialGaugeArc1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(206)))), ((int)(((byte)(103)))));
            this.radialGaugeArc1.Name = "radialGaugeArc1";
            this.radialGaugeArc1.Radius = 99D;
            this.radialGaugeArc1.RangeEnd = 60D;
            this.radialGaugeArc1.Width = 3D;
            // 
            // radialGaugeArc2
            // 
            this.radialGaugeArc2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeArc2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeArc2.Name = "radialGaugeArc2";
            this.radialGaugeArc2.Radius = 99D;
            this.radialGaugeArc2.RangeEnd = 120D;
            this.radialGaugeArc2.RangeStart = 61D;
            this.radialGaugeArc2.Width = 3D;
            // 
            // radialGaugeArc3
            // 
            this.radialGaugeArc3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.radialGaugeArc3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.radialGaugeArc3.Name = "radialGaugeArc3";
            this.radialGaugeArc3.Radius = 99D;
            this.radialGaugeArc3.RangeEnd = 180D;
            this.radialGaugeArc3.RangeStart = 121D;
            this.radialGaugeArc3.Width = 3D;
            // 
            // radialGaugeTicks1
            // 
            this.radialGaugeTicks1.DrawText = false;
            this.radialGaugeTicks1.Name = "radialGaugeTicks1";
            this.radialGaugeTicks1.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks1.TicksCount = 18;
            this.radialGaugeTicks1.TicksLenghtPercentage = 4F;
            this.radialGaugeTicks1.TicksRadiusPercentage = 83F;
            this.radialGaugeTicks1.TickThickness = 1F;
            // 
            // radialGaugeLabels1
            // 
            this.radialGaugeLabels1.DrawText = false;
            this.radialGaugeLabels1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeLabels1.LabelFontSize = 5F;
            this.radialGaugeLabels1.LabelRadiusPercentage = 68F;
            this.radialGaugeLabels1.LabelsCount = 9;
            this.radialGaugeLabels1.Name = "radialGaugeLabels1";
            // 
            // radialGaugeNeedle1
            // 
            this.radialGaugeNeedle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.radialGaugeNeedle1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.radialGaugeNeedle1.BackLenghtPercentage = 0D;
            this.radialGaugeNeedle1.InnerPointRadiusPercentage = 0D;
            this.radialGaugeNeedle1.LenghtPercentage = 70D;
            this.radialGaugeNeedle1.Name = "radialGaugeNeedle1";
            this.radialGaugeNeedle1.PointRadiusPercentage = 7D;
            this.radialGaugeNeedle1.Thickness = 1.5D;
            this.radialGaugeNeedle1.Value = 130F;
            // 
            // spinNeedleLenght
            // 
            this.spinNeedleLenght.Location = new System.Drawing.Point(426, 51);
            this.spinNeedleLenght.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.spinNeedleLenght.Name = "spinNeedleLenght";
            this.spinNeedleLenght.Size = new System.Drawing.Size(96, 20);
            this.spinNeedleLenght.TabIndex = 1;
            this.spinNeedleLenght.TabStop = false;
            this.spinNeedleLenght.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            this.spinNeedleLenght.ValueChanged += new System.EventHandler(this.spinNeedleLenght_ValueChanged);
            // 
            // spinArc1Radius
            // 
            this.spinArc1Radius.Location = new System.Drawing.Point(537, 451);
            this.spinArc1Radius.Name = "spinArc1Radius";
            this.spinArc1Radius.Size = new System.Drawing.Size(96, 20);
            this.spinArc1Radius.TabIndex = 11;
            this.spinArc1Radius.TabStop = false;
            this.spinArc1Radius.Value = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spinArc1Radius.ValueChanged += new System.EventHandler(this.spinArc1Radius_ValueChanged);
            // 
            // spinArc1Width
            // 
            this.spinArc1Width.Location = new System.Drawing.Point(426, 451);
            this.spinArc1Width.Name = "spinArc1Width";
            this.spinArc1Width.Size = new System.Drawing.Size(96, 20);
            this.spinArc1Width.TabIndex = 11;
            this.spinArc1Width.TabStop = false;
            this.spinArc1Width.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.spinArc1Width.ValueChanged += new System.EventHandler(this.spinArc1Width_ValueChanged);
            // 
            // spinLabelRadius
            // 
            this.spinLabelRadius.Location = new System.Drawing.Point(537, 319);
            this.spinLabelRadius.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.spinLabelRadius.Name = "spinLabelRadius";
            this.spinLabelRadius.Size = new System.Drawing.Size(96, 20);
            this.spinLabelRadius.TabIndex = 10;
            this.spinLabelRadius.TabStop = false;
            this.spinLabelRadius.Value = new decimal(new int[] {
            68,
            0,
            0,
            0});
            this.spinLabelRadius.ValueChanged += new System.EventHandler(this.spinLabelRadius_ValueChanged);
            // 
            // spinLabelCount
            // 
            this.spinLabelCount.Location = new System.Drawing.Point(426, 319);
            this.spinLabelCount.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.spinLabelCount.Name = "spinLabelCount";
            this.spinLabelCount.Size = new System.Drawing.Size(96, 20);
            this.spinLabelCount.TabIndex = 9;
            this.spinLabelCount.TabStop = false;
            this.spinLabelCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinLabelCount.ValueChanged += new System.EventHandler(this.radLabelCount_ValueChanged);
            // 
            // spinTicksLenght
            // 
            this.spinTicksLenght.Location = new System.Drawing.Point(537, 185);
            this.spinTicksLenght.Name = "spinTicksLenght";
            this.spinTicksLenght.Size = new System.Drawing.Size(96, 20);
            this.spinTicksLenght.TabIndex = 8;
            this.spinTicksLenght.TabStop = false;
            this.spinTicksLenght.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.spinTicksLenght.ValueChanged += new System.EventHandler(this.spinTicksLenght_ValueChanged);
            // 
            // spinTickTickness
            // 
            this.spinTickTickness.Location = new System.Drawing.Point(426, 235);
            this.spinTickTickness.Name = "spinTickTickness";
            this.spinTickTickness.Size = new System.Drawing.Size(96, 20);
            this.spinTickTickness.TabIndex = 7;
            this.spinTickTickness.TabStop = false;
            this.spinTickTickness.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinTickTickness.ValueChanged += new System.EventHandler(this.spinTickTickness_ValueChanged);
            // 
            // spinTicksCount
            // 
            this.spinTicksCount.Location = new System.Drawing.Point(426, 185);
            this.spinTicksCount.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.spinTicksCount.Name = "spinTicksCount";
            this.spinTicksCount.Size = new System.Drawing.Size(96, 20);
            this.spinTicksCount.TabIndex = 6;
            this.spinTicksCount.TabStop = false;
            this.spinTicksCount.Value = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.spinTicksCount.ValueChanged += new System.EventHandler(this.spinTicksCount_ValueChanged);
            // 
            // spinPointRadiusPercentage
            // 
            this.spinPointRadiusPercentage.Location = new System.Drawing.Point(537, 101);
            this.spinPointRadiusPercentage.Name = "spinPointRadiusPercentage";
            this.spinPointRadiusPercentage.Size = new System.Drawing.Size(96, 20);
            this.spinPointRadiusPercentage.TabIndex = 5;
            this.spinPointRadiusPercentage.TabStop = false;
            this.spinPointRadiusPercentage.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.spinPointRadiusPercentage.ValueChanged += new System.EventHandler(this.spinPointRadiusPercentage_ValueChanged);
            // 
            // spinInnerPointRadiusPercentage
            // 
            this.spinInnerPointRadiusPercentage.Location = new System.Drawing.Point(537, 51);
            this.spinInnerPointRadiusPercentage.Name = "spinInnerPointRadiusPercentage";
            this.spinInnerPointRadiusPercentage.Size = new System.Drawing.Size(96, 20);
            this.spinInnerPointRadiusPercentage.TabIndex = 4;
            this.spinInnerPointRadiusPercentage.TabStop = false;
            this.spinInnerPointRadiusPercentage.ValueChanged += new System.EventHandler(this.spinInnerPointRadiusPercentage_ValueChanged);
            // 
            // spinNeedleThickness
            // 
            this.spinNeedleThickness.DecimalPlaces = 1;
            this.spinNeedleThickness.Location = new System.Drawing.Point(426, 101);
            this.spinNeedleThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spinNeedleThickness.Name = "spinNeedleThickness";
            this.spinNeedleThickness.Size = new System.Drawing.Size(96, 20);
            this.spinNeedleThickness.TabIndex = 3;
            this.spinNeedleThickness.TabStop = false;
            this.spinNeedleThickness.Value = new decimal(new int[] {
            15,
            0,
            0,
            65536});
            this.spinNeedleThickness.ValueChanged += new System.EventHandler(this.spinNeedleThickness_ValueChanged);
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(534, 165);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(41, 18);
            this.radLabel7.TabIndex = 2;
            this.radLabel7.Text = "Lenght";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(423, 299);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(36, 18);
            this.radLabel8.TabIndex = 2;
            this.radLabel8.Text = "Count";
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(534, 431);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(39, 18);
            this.radLabel13.TabIndex = 2;
            this.radLabel13.Text = "Radius";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(423, 431);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(36, 18);
            this.radLabel10.TabIndex = 2;
            this.radLabel10.Text = "Width";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(423, 215);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(48, 18);
            this.radLabel6.TabIndex = 2;
            this.radLabel6.Text = "Tickness";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(423, 165);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(36, 18);
            this.radLabel5.TabIndex = 2;
            this.radLabel5.Text = "Count";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(534, 299);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(39, 18);
            this.radLabel9.TabIndex = 2;
            this.radLabel9.Text = "Radius";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(534, 82);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(66, 18);
            this.radLabel4.TabIndex = 2;
            this.radLabel4.Text = "Point radius";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(534, 31);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(95, 18);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Inner point radius";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(423, 82);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(48, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Tickness";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(423, 31);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(41, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Length";
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(534, 215);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(33, 18);
            this.radLabel16.TabIndex = 12;
            this.radLabel16.Text = "Color";
            // 
            // colorBoxTicks
            // 
            this.colorBoxTicks.AutoSize = false;
            this.colorBoxTicks.Location = new System.Drawing.Point(537, 235);
            this.colorBoxTicks.Name = "colorBoxTicks";
            this.colorBoxTicks.Size = new System.Drawing.Size(96, 22);
            this.colorBoxTicks.TabIndex = 13;
            this.colorBoxTicks.Text = "radColorBox1";
            this.colorBoxTicks.Value = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.colorBoxTicks.ValueChanged += new System.EventHandler(this.colorBoxTicks_ValueChanged);
            // 
            // radLabel17
            // 
            this.radLabel17.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel17.Location = new System.Drawing.Point(422, 8);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(89, 18);
            this.radLabel17.TabIndex = 14;
            this.radLabel17.Text = "Needle settings";
            // 
            // radLabel18
            // 
            this.radLabel18.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel18.Location = new System.Drawing.Point(423, 140);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(78, 18);
            this.radLabel18.TabIndex = 14;
            this.radLabel18.Text = "Ticks settings";
            // 
            // radLabel19
            // 
            this.radLabel19.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel19.Location = new System.Drawing.Point(423, 272);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(85, 18);
            this.radLabel19.TabIndex = 14;
            this.radLabel19.Text = "Labels settings";
            // 
            // radLabel20
            // 
            this.radLabel20.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel20.Location = new System.Drawing.Point(423, 356);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(74, 18);
            this.radLabel20.TabIndex = 14;
            this.radLabel20.Text = "Arcs settings";
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "Arc 1";
            radListDataItem2.Text = "Arc 2";
            radListDataItem3.Text = "Arc 3";
            this.radDropDownList1.Items.Add(radListDataItem1);
            this.radDropDownList1.Items.Add(radListDataItem2);
            this.radDropDownList1.Items.Add(radListDataItem3);
            this.radDropDownList1.Location = new System.Drawing.Point(426, 403);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(96, 20);
            this.radDropDownList1.TabIndex = 15;
            this.radDropDownList1.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownList1_SelectedIndexChanged);
            // 
            // radLabel21
            // 
            this.radLabel21.Location = new System.Drawing.Point(423, 383);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(55, 18);
            this.radLabel21.TabIndex = 2;
            this.radLabel21.Text = "Select Arc";
            // 
            // colorBoxArc
            // 
            this.colorBoxArc.AutoSize = false;
            this.colorBoxArc.Location = new System.Drawing.Point(537, 403);
            this.colorBoxArc.Name = "colorBoxArc";
            this.colorBoxArc.Size = new System.Drawing.Size(96, 22);
            this.colorBoxArc.TabIndex = 13;
            this.colorBoxArc.Text = "radColorBox1";
            this.colorBoxArc.Value = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.colorBoxArc.ValueChanged += new System.EventHandler(this.colorBoxArc_ValueChanged);
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(534, 383);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(33, 18);
            this.radLabel11.TabIndex = 2;
            this.radLabel11.Text = "Color";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(10, 450);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(190, 24);
            this.radButton1.TabIndex = 16;
            this.radButton1.Text = "Save layout";
            this.radButton1.Click += new System.EventHandler(this.SaveLayout);
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(210, 450);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(190, 24);
            this.radButton2.TabIndex = 17;
            this.radButton2.Text = "Load layout";
            this.radButton2.Click += new System.EventHandler(this.LoadLayout);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.radButton2);
            this.Controls.Add(this.radLabel17);
            this.Controls.Add(this.radDropDownList1);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel20);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radRadialGauge1);
            this.Controls.Add(this.radLabel19);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.spinLabelCount);
            this.Controls.Add(this.radLabel18);
            this.Controls.Add(this.spinLabelRadius);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.spinPointRadiusPercentage);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.colorBoxArc);
            this.Controls.Add(this.radLabel13);
            this.Controls.Add(this.colorBoxTicks);
            this.Controls.Add(this.spinArc1Width);
            this.Controls.Add(this.spinTicksCount);
            this.Controls.Add(this.radLabel21);
            this.Controls.Add(this.radLabel16);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.spinInnerPointRadiusPercentage);
            this.Controls.Add(this.spinNeedleLenght);
            this.Controls.Add(this.spinTickTickness);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.spinNeedleThickness);
            this.Controls.Add(this.spinArc1Radius);
            this.Controls.Add(this.spinTicksLenght);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.radLabel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1502, 654);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Controls.SetChildIndex(this.radLabel1, 0);
            this.Controls.SetChildIndex(this.radLabel10, 0);
            this.Controls.SetChildIndex(this.spinTicksLenght, 0);
            this.Controls.SetChildIndex(this.spinArc1Radius, 0);
            this.Controls.SetChildIndex(this.spinNeedleThickness, 0);
            this.Controls.SetChildIndex(this.radLabel8, 0);
            this.Controls.SetChildIndex(this.spinTickTickness, 0);
            this.Controls.SetChildIndex(this.spinNeedleLenght, 0);
            this.Controls.SetChildIndex(this.spinInnerPointRadiusPercentage, 0);
            this.Controls.SetChildIndex(this.radLabel11, 0);
            this.Controls.SetChildIndex(this.radLabel16, 0);
            this.Controls.SetChildIndex(this.radLabel21, 0);
            this.Controls.SetChildIndex(this.spinTicksCount, 0);
            this.Controls.SetChildIndex(this.spinArc1Width, 0);
            this.Controls.SetChildIndex(this.colorBoxTicks, 0);
            this.Controls.SetChildIndex(this.radLabel13, 0);
            this.Controls.SetChildIndex(this.colorBoxArc, 0);
            this.Controls.SetChildIndex(this.radLabel2, 0);
            this.Controls.SetChildIndex(this.spinPointRadiusPercentage, 0);
            this.Controls.SetChildIndex(this.radLabel4, 0);
            this.Controls.SetChildIndex(this.radLabel9, 0);
            this.Controls.SetChildIndex(this.spinLabelRadius, 0);
            this.Controls.SetChildIndex(this.radLabel18, 0);
            this.Controls.SetChildIndex(this.spinLabelCount, 0);
            this.Controls.SetChildIndex(this.radLabel7, 0);
            this.Controls.SetChildIndex(this.radLabel3, 0);
            this.Controls.SetChildIndex(this.radLabel19, 0);
            this.Controls.SetChildIndex(this.radRadialGauge1, 0);
            this.Controls.SetChildIndex(this.radLabel5, 0);
            this.Controls.SetChildIndex(this.radLabel20, 0);
            this.Controls.SetChildIndex(this.radLabel6, 0);
            this.Controls.SetChildIndex(this.radDropDownList1, 0);
            this.Controls.SetChildIndex(this.radLabel17, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.radButton2, 0);
            this.Controls.SetChildIndex(this.radButton1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadialGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinNeedleLenght)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinArc1Radius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinArc1Width)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinLabelRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinLabelCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTicksLenght)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTickTickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTicksCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPointRadiusPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinInnerPointRadiusPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinNeedleThickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBoxTicks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBoxArc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.Gauges.RadRadialGauge radRadialGauge1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc3;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeLabels radialGaugeLabels1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeNeedle radialGaugeNeedle1;
        private Telerik.WinControls.UI.RadSpinEditor spinNeedleLenght;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadSpinEditor spinNeedleThickness;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.Timer timer1;
        private Telerik.WinControls.UI.RadSpinEditor spinInnerPointRadiusPercentage;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadSpinEditor spinPointRadiusPercentage;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadSpinEditor spinTicksCount;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadSpinEditor spinTickTickness;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadSpinEditor spinLabelCount;
        private Telerik.WinControls.UI.RadSpinEditor spinTicksLenght;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadSpinEditor spinLabelRadius;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadSpinEditor spinArc1Width;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadSpinEditor spinArc1Radius;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadColorBox colorBoxTicks;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadColorBox colorBoxArc;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadButton radButton2;
    }
}