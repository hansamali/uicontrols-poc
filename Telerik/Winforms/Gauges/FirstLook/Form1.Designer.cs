﻿namespace Telerik.Examples.WinControls.Gauges.FirstLook
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
       

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radRadialGauge1 = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.radialGaugeArc1 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc2 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeTicks1 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeTicks2 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeTicks3 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeTicks4 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeTicks5 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeTicks7 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeTicks8 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeTicks9 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radialGaugeLabels1 = new Telerik.WinControls.UI.Gauges.RadialGaugeLabels();
            this.radialGaugeLabels2 = new Telerik.WinControls.UI.Gauges.RadialGaugeLabels();
            this.radialGaugeNeedle2 = new Telerik.WinControls.UI.Gauges.RadialGaugeNeedle();
            this.radialGaugeTicks6 = new Telerik.WinControls.UI.Gauges.RadialGaugeTicks();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radRadialGauge2 = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.radialGaugeArc3 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc4 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeNeedle1 = new Telerik.WinControls.UI.Gauges.RadialGaugeNeedle();
            this.radialGaugeSingleLabel1 = new Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radRadialGauge3 = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.radialGaugeArc5 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc6 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeNeedle3 = new Telerik.WinControls.UI.Gauges.RadialGaugeNeedle();
            this.radialGaugeSingleLabel2 = new Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadialGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadialGauge2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadialGauge3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1000, 33);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(1000, 199);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.radLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(155)))), ((int)(((byte)(86)))));
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(196, 23);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "CALL CENTER MONITORING";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.panel1.Controls.Add(this.radLabel9);
            this.panel1.Controls.Add(this.radLabel8);
            this.panel1.Controls.Add(this.radLabel7);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Location = new System.Drawing.Point(3, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(676, 73);
            this.panel1.TabIndex = 1;
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.radLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(112)))), ((int)(((byte)(165)))));
            this.radLabel9.Location = new System.Drawing.Point(358, 37);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(62, 33);
            this.radLabel9.TabIndex = 1;
            this.radLabel9.Text = "03:37";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.radLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(112)))), ((int)(((byte)(165)))));
            this.radLabel8.Location = new System.Drawing.Point(238, 37);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(62, 33);
            this.radLabel8.TabIndex = 1;
            this.radLabel8.Text = "03:37";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.radLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(112)))), ((int)(((byte)(164)))));
            this.radLabel7.Location = new System.Drawing.Point(134, 37);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(62, 33);
            this.radLabel7.TabIndex = 1;
            this.radLabel7.Text = "03:37";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radLabel7.GetChildAt(0).GetChildAt(2).GetChildAt(1))).TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radLabel7.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(112)))), ((int)(((byte)(165)))));
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.radLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.radLabel6.Location = new System.Drawing.Point(8, 37);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(62, 33);
            this.radLabel6.TabIndex = 1;
            this.radLabel6.Text = "03:37";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radLabel6.GetChildAt(0).GetChildAt(2).GetChildAt(1))).TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radLabel6.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(90)))), ((int)(((byte)(91)))));
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel5.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel5.Location = new System.Drawing.Point(356, 11);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(111, 21);
            this.radLabel5.TabIndex = 0;
            this.radLabel5.Text = "AVG No of Holds";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel4.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel4.Location = new System.Drawing.Point(236, 11);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(100, 21);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "AVG Hold Time";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel3.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel3.Location = new System.Drawing.Point(132, 11);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(91, 21);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Duration Goal";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel2.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel2.Location = new System.Drawing.Point(10, 11);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(114, 21);
            this.radLabel2.TabIndex = 0;
            this.radLabel2.Text = "AVG Handle Time";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.radLabel10.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel10.Location = new System.Drawing.Point(3, 137);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(153, 23);
            this.radLabel10.TabIndex = 2;
            this.radLabel10.Text = "Average Call Duration";
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.radLabel11.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radLabel11.Location = new System.Drawing.Point(239, 137);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(141, 23);
            this.radLabel11.TabIndex = 2;
            this.radLabel11.Text = "Agents\' Productivity";
            // 
            // radRadialGauge1
            // 
            this.radRadialGauge1.BackColor = System.Drawing.Color.White;
            this.radRadialGauge1.CausesValidation = false;
            this.radRadialGauge1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc1,
            this.radialGaugeArc2,
            this.radialGaugeTicks1,
            this.radialGaugeTicks2,
            this.radialGaugeTicks3,
            this.radialGaugeTicks4,
            this.radialGaugeTicks5,
            this.radialGaugeTicks7,
            this.radialGaugeTicks8,
            this.radialGaugeTicks9,
            this.radialGaugeLabels1,
            this.radialGaugeLabels2,
            this.radialGaugeNeedle2});
            this.radRadialGauge1.Location = new System.Drawing.Point(17, 220);
            this.radRadialGauge1.Name = "radRadialGauge1";
            this.radRadialGauge1.RangeEnd = 9D;
            this.radRadialGauge1.Size = new System.Drawing.Size(204, 214);
            this.radRadialGauge1.TabIndex = 1;
            this.radRadialGauge1.Text = "radRadialGauge2";
            this.radRadialGauge1.Value = 6F;
            // 
            // radialGaugeArc1
            // 
            this.radialGaugeArc1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeArc1.BackColor2 = System.Drawing.Color.Black;
            this.radialGaugeArc1.BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeArc1.Name = "radialGaugeArc1";
            this.radialGaugeArc1.RangeEnd = 120D;
            this.radialGaugeArc1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radialGaugeArc1.Width = 1D;
            // 
            // radialGaugeArc2
            // 
            this.radialGaugeArc2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.radialGaugeArc2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.radialGaugeArc2.BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.radialGaugeArc2.Name = "radialGaugeArc2";
            this.radialGaugeArc2.RangeEnd = 180D;
            this.radialGaugeArc2.RangeStart = 120D;
            this.radialGaugeArc2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radialGaugeArc2.Width = 1D;
            // 
            // radialGaugeTicks1
            // 
            this.radialGaugeTicks1.DrawText = false;
            this.radialGaugeTicks1.Name = "radialGaugeTicks1";
            this.radialGaugeTicks1.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks1.TickEndIndexVisibleRange = 11F;
            this.radialGaugeTicks1.TicksCount = 18;
            this.radialGaugeTicks1.TicksRadiusPercentage = 84F;
            this.radialGaugeTicks1.TickThickness = 1F;
            this.radialGaugeTicks1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeTicks2
            // 
            this.radialGaugeTicks2.DrawText = false;
            this.radialGaugeTicks2.Name = "radialGaugeTicks2";
            this.radialGaugeTicks2.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks2.TickEndIndexVisibleRange = 35F;
            this.radialGaugeTicks2.TicksCount = 54;
            this.radialGaugeTicks2.TicksLenghtPercentage = 3F;
            this.radialGaugeTicks2.TickThickness = 1F;
            this.radialGaugeTicks2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeTicks3
            // 
            this.radialGaugeTicks3.DrawText = false;
            this.radialGaugeTicks3.Name = "radialGaugeTicks3";
            this.radialGaugeTicks3.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.radialGaugeTicks3.TicksCount = 18;
            this.radialGaugeTicks3.TicksRadiusPercentage = 84F;
            this.radialGaugeTicks3.TickStartIndexVisibleRange = 12F;
            this.radialGaugeTicks3.TickThickness = 1F;
            this.radialGaugeTicks3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeTicks4
            // 
            this.radialGaugeTicks4.DrawText = false;
            this.radialGaugeTicks4.Name = "radialGaugeTicks4";
            this.radialGaugeTicks4.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.radialGaugeTicks4.TicksCount = 54;
            this.radialGaugeTicks4.TicksLenghtPercentage = 3F;
            this.radialGaugeTicks4.TickStartIndexVisibleRange = 37F;
            this.radialGaugeTicks4.TickThickness = 1F;
            this.radialGaugeTicks4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeTicks5
            // 
            this.radialGaugeTicks5.AccessibleDescription = "radialGaugeTicks5";
            this.radialGaugeTicks5.AccessibleName = "radialGaugeTicks5";
            this.radialGaugeTicks5.DrawText = false;
            this.radialGaugeTicks5.Name = "radialGaugeTicks5";
            this.radialGaugeTicks5.Text = "radialGaugeTicks5";
            this.radialGaugeTicks5.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks5.TickEndIndexVisibleRange = 5F;
            this.radialGaugeTicks5.TicksCount = 18;
            this.radialGaugeTicks5.TicksRadiusPercentage = 103F;
            this.radialGaugeTicks5.TickStartIndexVisibleRange = 5F;
            this.radialGaugeTicks5.TickThickness = 1F;
            this.radialGaugeTicks5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeTicks7
            // 
            this.radialGaugeTicks7.AccessibleDescription = "radialGaugeTicks7";
            this.radialGaugeTicks7.AccessibleName = "radialGaugeTicks7";
            this.radialGaugeTicks7.DrawText = false;
            this.radialGaugeTicks7.Name = "radialGaugeTicks7";
            this.radialGaugeTicks7.Text = "radialGaugeTicks7";
            this.radialGaugeTicks7.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks7.TickEndIndexVisibleRange = 9F;
            this.radialGaugeTicks7.TicksCount = 18;
            this.radialGaugeTicks7.TicksRadiusPercentage = 103F;
            this.radialGaugeTicks7.TickStartIndexVisibleRange = 9F;
            this.radialGaugeTicks7.TickThickness = 1F;
            this.radialGaugeTicks7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeTicks8
            // 
            this.radialGaugeTicks8.AccessibleDescription = "radialGaugeTicks8";
            this.radialGaugeTicks8.AccessibleName = "radialGaugeTicks8";
            this.radialGaugeTicks8.CircleTicks = true;
            this.radialGaugeTicks8.DrawText = false;
            this.radialGaugeTicks8.Name = "radialGaugeTicks8";
            this.radialGaugeTicks8.Text = "radialGaugeTicks8";
            this.radialGaugeTicks8.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks8.TickEndIndexVisibleRange = 5F;
            this.radialGaugeTicks8.TicksCount = 18;
            this.radialGaugeTicks8.TicksLenghtPercentage = 3F;
            this.radialGaugeTicks8.TicksRadiusPercentage = 115F;
            this.radialGaugeTicks8.TickStartIndexVisibleRange = 5F;
            this.radialGaugeTicks8.TickThickness = 1F;
            this.radialGaugeTicks8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeTicks9
            // 
            this.radialGaugeTicks9.AccessibleDescription = "radialGaugeTicks9";
            this.radialGaugeTicks9.AccessibleName = "radialGaugeTicks9";
            this.radialGaugeTicks9.CircleTicks = true;
            this.radialGaugeTicks9.DrawText = false;
            this.radialGaugeTicks9.Name = "radialGaugeTicks9";
            this.radialGaugeTicks9.Text = "radialGaugeTicks9";
            this.radialGaugeTicks9.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks9.TickEndIndexVisibleRange = 9F;
            this.radialGaugeTicks9.TicksCount = 18;
            this.radialGaugeTicks9.TicksLenghtPercentage = 3F;
            this.radialGaugeTicks9.TicksRadiusPercentage = 115F;
            this.radialGaugeTicks9.TickStartIndexVisibleRange = 9F;
            this.radialGaugeTicks9.TickThickness = 1F;
            this.radialGaugeTicks9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeLabels1
            // 
            this.radialGaugeLabels1.DrawText = false;
            this.radialGaugeLabels1.LabelEndVisibleRange = 5F;
            this.radialGaugeLabels1.LabelFontSize = 5F;
            this.radialGaugeLabels1.LabelFormat = "#,##0.00#";
            this.radialGaugeLabels1.LabelRadiusPercentage = 68F;
            this.radialGaugeLabels1.LabelsCount = 9;
            this.radialGaugeLabels1.Name = "radialGaugeLabels1";
            this.radialGaugeLabels1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeLabels2
            // 
            this.radialGaugeLabels2.DrawText = false;
            this.radialGaugeLabels2.ForeColor = System.Drawing.Color.Red;
            this.radialGaugeLabels2.LabelFontSize = 5F;
            this.radialGaugeLabels2.LabelFormat = "#,##0.00#";
            this.radialGaugeLabels2.LabelRadiusPercentage = 68F;
            this.radialGaugeLabels2.LabelsCount = 9;
            this.radialGaugeLabels2.LabelStartVisibleRange = 6F;
            this.radialGaugeLabels2.Name = "radialGaugeLabels2";
            this.radialGaugeLabels2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeNeedle2
            // 
            this.radialGaugeNeedle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.radialGaugeNeedle2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.radialGaugeNeedle2.BackLenghtPercentage = 0D;
            this.radialGaugeNeedle2.BindValue = true;
            this.radialGaugeNeedle2.InnerPointRadiusPercentage = 0D;
            this.radialGaugeNeedle2.LenghtPercentage = 78D;
            this.radialGaugeNeedle2.Name = "radialGaugeNeedle2";
            this.radialGaugeNeedle2.PointRadiusPercentage = 6D;
            this.radialGaugeNeedle2.Thickness = 0.2D;
            this.radialGaugeNeedle2.Value = 6F;
            this.radialGaugeNeedle2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeTicks6
            // 
            this.radialGaugeTicks6.AccessibleDescription = "radialGaugeTicks5";
            this.radialGaugeTicks6.AccessibleName = "radialGaugeTicks5";
            this.radialGaugeTicks6.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeTicks6.DrawText = false;
            this.radialGaugeTicks6.Name = "radialGaugeTicks6";
            this.radialGaugeTicks6.Text = "radialGaugeTicks5";
            this.radialGaugeTicks6.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeTicks6.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.radialGaugeTicks6.TickEndIndexVisibleRange = 5F;
            this.radialGaugeTicks6.TicksCount = 18;
            this.radialGaugeTicks6.TicksRadiusPercentage = 103F;
            this.radialGaugeTicks6.TickStartIndexVisibleRange = 5F;
            this.radialGaugeTicks6.TickThickness = 1F;
            this.radialGaugeTicks6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.radLabel12.Location = new System.Drawing.Point(0, 231);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(68, 31);
            this.radLabel12.TabIndex = 0;
            this.radLabel12.Text = "Industry best practice";
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.radLabel13.Location = new System.Drawing.Point(92, 192);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(68, 31);
            this.radLabel13.TabIndex = 0;
            this.radLabel13.Text = "Company AVG";
            // 
            // radRadialGauge2
            // 
            this.radRadialGauge2.BackColor = System.Drawing.Color.White;
            this.radRadialGauge2.CausesValidation = false;
            this.radRadialGauge2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc3,
            this.radialGaugeArc4,
            this.radialGaugeNeedle1,
            this.radialGaugeSingleLabel1});
            this.radRadialGauge2.Location = new System.Drawing.Point(238, 222);
            this.radRadialGauge2.Name = "radRadialGauge2";
            this.radRadialGauge2.Size = new System.Drawing.Size(202, 212);
            this.radRadialGauge2.StartAngle = 180D;
            this.radRadialGauge2.SweepAngle = 180D;
            this.radRadialGauge2.TabIndex = 6;
            this.radRadialGauge2.Text = "radRadialGauge1";
            this.radRadialGauge2.Value = 50F;
            // 
            // radialGaugeArc3
            // 
            this.radialGaugeArc3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(190)))), ((int)(((byte)(79)))));
            this.radialGaugeArc3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(191)))), ((int)(((byte)(80)))));
            this.radialGaugeArc3.BindEndRange = true;
            this.radialGaugeArc3.Name = "radialGaugeArc3";
            this.radialGaugeArc3.RangeEnd = 50D;
            this.radialGaugeArc3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radialGaugeArc3.Width = 40D;
            // 
            // radialGaugeArc4
            // 
            this.radialGaugeArc4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.radialGaugeArc4.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.radialGaugeArc4.BindStartRange = true;
            this.radialGaugeArc4.Name = "radialGaugeArc4";
            this.radialGaugeArc4.RangeEnd = 100D;
            this.radialGaugeArc4.RangeStart = 50D;
            this.radialGaugeArc4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radialGaugeArc4.Width = 40D;
            // 
            // radialGaugeNeedle1
            // 
            this.radialGaugeNeedle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.radialGaugeNeedle1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.radialGaugeNeedle1.BackLenghtPercentage = 0D;
            this.radialGaugeNeedle1.BindValue = true;
            this.radialGaugeNeedle1.InnerPointRadiusPercentage = 0D;
            this.radialGaugeNeedle1.LenghtPercentage = 94D;
            this.radialGaugeNeedle1.Name = "radialGaugeNeedle1";
            this.radialGaugeNeedle1.Thickness = 0.5D;
            this.radialGaugeNeedle1.Value = 50F;
            this.radialGaugeNeedle1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeSingleLabel1
            // 
            this.radialGaugeSingleLabel1.AccessibleDescription = "radialGaugeSingleLabel1";
            this.radialGaugeSingleLabel1.AccessibleName = "radialGaugeSingleLabel1";
            this.radialGaugeSingleLabel1.BindValue = true;
            this.radialGaugeSingleLabel1.LabelFontSize = 10F;
            this.radialGaugeSingleLabel1.LabelFormat = "#";
            this.radialGaugeSingleLabel1.LabelText = "Text";
            this.radialGaugeSingleLabel1.LocationPercentage = new System.Drawing.SizeF(0F, 0.5F);
            this.radialGaugeSingleLabel1.Name = "radialGaugeSingleLabel1";
            this.radialGaugeSingleLabel1.Text = "radialGaugeSingleLabel1";
            this.radialGaugeSingleLabel1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel14.Location = new System.Drawing.Point(239, 168);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(129, 44);
            this.radLabel14.TabIndex = 7;
            this.radLabel14.Text = "Contacts handled per agent";
            // 
            // radRadialGauge3
            // 
            this.radRadialGauge3.BackColor = System.Drawing.Color.White;
            this.radRadialGauge3.CausesValidation = false;
            this.radRadialGauge3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc5,
            this.radialGaugeArc6,
            this.radialGaugeNeedle3,
            this.radialGaugeSingleLabel2});
            this.radRadialGauge3.Location = new System.Drawing.Point(468, 222);
            this.radRadialGauge3.Name = "radRadialGauge3";
            this.radRadialGauge3.Size = new System.Drawing.Size(211, 212);
            this.radRadialGauge3.StartAngle = 180D;
            this.radRadialGauge3.SweepAngle = 180D;
            this.radRadialGauge3.TabIndex = 6;
            this.radRadialGauge3.Text = "radRadialGauge1";
            this.radRadialGauge3.Value = 50F;
            // 
            // radialGaugeArc5
            // 
            this.radialGaugeArc5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(70)))), ((int)(((byte)(71)))));
            this.radialGaugeArc5.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(70)))), ((int)(((byte)(71)))));
            this.radialGaugeArc5.BindEndRange = true;
            this.radialGaugeArc5.Name = "radialGaugeArc5";
            this.radialGaugeArc5.RangeEnd = 50D;
            this.radialGaugeArc5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radialGaugeArc5.Width = 40D;
            // 
            // radialGaugeArc6
            // 
            this.radialGaugeArc6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.radialGaugeArc6.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.radialGaugeArc6.BindStartRange = true;
            this.radialGaugeArc6.Name = "radialGaugeArc6";
            this.radialGaugeArc6.RangeEnd = 100D;
            this.radialGaugeArc6.RangeStart = 50D;
            this.radialGaugeArc6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radialGaugeArc6.Width = 40D;
            // 
            // radialGaugeNeedle3
            // 
            this.radialGaugeNeedle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.radialGaugeNeedle3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.radialGaugeNeedle3.BackLenghtPercentage = 0D;
            this.radialGaugeNeedle3.BindValue = true;
            this.radialGaugeNeedle3.InnerPointRadiusPercentage = 0D;
            this.radialGaugeNeedle3.LenghtPercentage = 94D;
            this.radialGaugeNeedle3.Name = "radialGaugeNeedle3";
            this.radialGaugeNeedle3.Thickness = 0.5D;
            this.radialGaugeNeedle3.Value = 50F;
            this.radialGaugeNeedle3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radialGaugeSingleLabel2
            // 
            this.radialGaugeSingleLabel2.AccessibleDescription = "radialGaugeSingleLabel2";
            this.radialGaugeSingleLabel2.AccessibleName = "radialGaugeSingleLabel2";
            this.radialGaugeSingleLabel2.BindValue = true;
            this.radialGaugeSingleLabel2.LabelFontSize = 10F;
            this.radialGaugeSingleLabel2.LabelFormat = "#\'%";
            this.radialGaugeSingleLabel2.LabelText = "Text";
            this.radialGaugeSingleLabel2.LocationPercentage = new System.Drawing.SizeF(0F, 0.5F);
            this.radialGaugeSingleLabel2.Name = "radialGaugeSingleLabel2";
            this.radialGaugeSingleLabel2.Text = "radialGaugeSingleLabel2";
            this.radialGaugeSingleLabel2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel15.Location = new System.Drawing.Point(498, 168);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(129, 44);
            this.radLabel15.TabIndex = 7;
            this.radLabel15.Text = "Talk time (%)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radRadialGauge3);
            this.Controls.Add(this.radLabel15);
            this.Controls.Add(this.radLabel14);
            this.Controls.Add(this.radRadialGauge2);
            this.Controls.Add(this.radLabel13);
            this.Controls.Add(this.radLabel12);
            this.Controls.Add(this.radRadialGauge1);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radLabel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1532, 878);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Controls.SetChildIndex(this.radLabel1, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.radLabel10, 0);
            this.Controls.SetChildIndex(this.radLabel11, 0);
            this.Controls.SetChildIndex(this.radRadialGauge1, 0);
            this.Controls.SetChildIndex(this.radLabel12, 0);
            this.Controls.SetChildIndex(this.radLabel13, 0);
            this.Controls.SetChildIndex(this.radRadialGauge2, 0);
            this.Controls.SetChildIndex(this.radLabel14, 0);
            this.Controls.SetChildIndex(this.radLabel15, 0);
            this.Controls.SetChildIndex(this.radRadialGauge3, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadialGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadialGauge2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadialGauge3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge radRadialGauge1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks3;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks4;
        private Telerik.WinControls.UI.Gauges.RadialGaugeLabels radialGaugeLabels1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeLabels radialGaugeLabels2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeNeedle radialGaugeNeedle2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks5;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks6;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks7;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks8;
        private Telerik.WinControls.UI.Gauges.RadialGaugeTicks radialGaugeTicks9;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge radRadialGauge2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc3;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc4;
        private Telerik.WinControls.UI.Gauges.RadialGaugeNeedle radialGaugeNeedle1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel radialGaugeSingleLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge radRadialGauge3;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc5;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc6;
        private Telerik.WinControls.UI.Gauges.RadialGaugeNeedle radialGaugeNeedle3;
        private Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel radialGaugeSingleLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel15;
    }
}