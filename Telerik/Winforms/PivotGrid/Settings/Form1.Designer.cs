﻿namespace Telerik.Examples.WinControls.PivotGrid.Settings
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.rowGrandTotalNone = new Telerik.WinControls.UI.RadRadioButton();
            this.rowGrandTotalLast = new Telerik.WinControls.UI.RadRadioButton();
            this.rowGrandTotalFirst = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.rowSubTotalNone = new Telerik.WinControls.UI.RadRadioButton();
            this.rowSubTotalLast = new Telerik.WinControls.UI.RadRadioButton();
            this.rowSubTotalFirst = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.columnGrandTotalNone = new Telerik.WinControls.UI.RadRadioButton();
            this.columnGrandTotalLast = new Telerik.WinControls.UI.RadRadioButton();
            this.columnGrandTotalFirst = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.columnSubTotalNone = new Telerik.WinControls.UI.RadRadioButton();
            this.columnSubTotalLast = new Telerik.WinControls.UI.RadRadioButton();
            this.columnSubTotalFirst = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox5 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditor1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.aggregatePositionColumns = new Telerik.WinControls.UI.RadRadioButton();
            this.aggregatePositionRows = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox6 = new Telerik.WinControls.UI.RadGroupBox();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rowGrandTotalNone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowGrandTotalLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowGrandTotalFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rowSubTotalNone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowSubTotalLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowSubTotalFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnGrandTotalNone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnGrandTotalLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnGrandTotalFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnSubTotalNone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnSubTotalLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnSubTotalFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox5)).BeginInit();
            this.radGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggregatePositionColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggregatePositionRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox6)).BeginInit();
            this.radGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox6);
            this.settingsPanel.Controls.Add(this.radGroupBox5);
            this.settingsPanel.Controls.Add(this.radGroupBox4);
            this.settingsPanel.Controls.Add(this.radGroupBox3);
            this.settingsPanel.Controls.Add(this.radGroupBox2);
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.settingsPanel.Location = new System.Drawing.Point(1218, 0);
            this.settingsPanel.Size = new System.Drawing.Size(286, 990);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox3, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox4, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox5, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox6, 0);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.rowGrandTotalNone);
            this.radGroupBox1.Controls.Add(this.rowGrandTotalLast);
            this.radGroupBox1.Controls.Add(this.rowGrandTotalFirst);
            this.radGroupBox1.HeaderText = "Rows GrandTotals Position";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 7);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(266, 94);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Rows GrandTotals Position";
            // 
            // rowGrandTotalNone
            // 
            this.rowGrandTotalNone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rowGrandTotalNone.Location = new System.Drawing.Point(5, 70);
            this.rowGrandTotalNone.Name = "rowGrandTotalNone";
            this.rowGrandTotalNone.Size = new System.Drawing.Size(48, 18);
            this.rowGrandTotalNone.TabIndex = 2;
            this.rowGrandTotalNone.Text = "None";
            // 
            // rowGrandTotalLast
            // 
            this.rowGrandTotalLast.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rowGrandTotalLast.Location = new System.Drawing.Point(5, 46);
            this.rowGrandTotalLast.Name = "rowGrandTotalLast";
            this.rowGrandTotalLast.Size = new System.Drawing.Size(40, 18);
            this.rowGrandTotalLast.TabIndex = 1;
            this.rowGrandTotalLast.Text = "Last";
            // 
            // rowGrandTotalFirst
            // 
            this.rowGrandTotalFirst.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rowGrandTotalFirst.Location = new System.Drawing.Point(5, 22);
            this.rowGrandTotalFirst.Name = "rowGrandTotalFirst";
            this.rowGrandTotalFirst.Size = new System.Drawing.Size(41, 18);
            this.rowGrandTotalFirst.TabIndex = 0;
            this.rowGrandTotalFirst.Text = "First";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox2.Controls.Add(this.rowSubTotalNone);
            this.radGroupBox2.Controls.Add(this.rowSubTotalLast);
            this.radGroupBox2.Controls.Add(this.rowSubTotalFirst);
            this.radGroupBox2.HeaderText = "Rows SubTotals Position";
            this.radGroupBox2.Location = new System.Drawing.Point(10, 107);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(266, 93);
            this.radGroupBox2.TabIndex = 2;
            this.radGroupBox2.Text = "Rows SubTotals Position";
            // 
            // rowSubTotalNone
            // 
            this.rowSubTotalNone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rowSubTotalNone.Location = new System.Drawing.Point(5, 69);
            this.rowSubTotalNone.Name = "rowSubTotalNone";
            this.rowSubTotalNone.Size = new System.Drawing.Size(48, 18);
            this.rowSubTotalNone.TabIndex = 5;
            this.rowSubTotalNone.Text = "None";
            // 
            // rowSubTotalLast
            // 
            this.rowSubTotalLast.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rowSubTotalLast.Location = new System.Drawing.Point(5, 45);
            this.rowSubTotalLast.Name = "rowSubTotalLast";
            this.rowSubTotalLast.Size = new System.Drawing.Size(40, 18);
            this.rowSubTotalLast.TabIndex = 4;
            this.rowSubTotalLast.Text = "Last";
            // 
            // rowSubTotalFirst
            // 
            this.rowSubTotalFirst.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rowSubTotalFirst.Location = new System.Drawing.Point(5, 21);
            this.rowSubTotalFirst.Name = "rowSubTotalFirst";
            this.rowSubTotalFirst.Size = new System.Drawing.Size(41, 18);
            this.rowSubTotalFirst.TabIndex = 3;
            this.rowSubTotalFirst.Text = "First";
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox3.Controls.Add(this.columnGrandTotalNone);
            this.radGroupBox3.Controls.Add(this.columnGrandTotalLast);
            this.radGroupBox3.Controls.Add(this.columnGrandTotalFirst);
            this.radGroupBox3.HeaderText = "Columns GrandTotals Position";
            this.radGroupBox3.Location = new System.Drawing.Point(10, 206);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(266, 97);
            this.radGroupBox3.TabIndex = 3;
            this.radGroupBox3.Text = "Columns GrandTotals Position";
            // 
            // columnGrandTotalNone
            // 
            this.columnGrandTotalNone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.columnGrandTotalNone.Location = new System.Drawing.Point(5, 69);
            this.columnGrandTotalNone.Name = "columnGrandTotalNone";
            this.columnGrandTotalNone.Size = new System.Drawing.Size(48, 18);
            this.columnGrandTotalNone.TabIndex = 5;
            this.columnGrandTotalNone.Text = "None";
            // 
            // columnGrandTotalLast
            // 
            this.columnGrandTotalLast.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.columnGrandTotalLast.Location = new System.Drawing.Point(5, 45);
            this.columnGrandTotalLast.Name = "columnGrandTotalLast";
            this.columnGrandTotalLast.Size = new System.Drawing.Size(40, 18);
            this.columnGrandTotalLast.TabIndex = 4;
            this.columnGrandTotalLast.Text = "Last";
            // 
            // columnGrandTotalFirst
            // 
            this.columnGrandTotalFirst.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.columnGrandTotalFirst.Location = new System.Drawing.Point(5, 21);
            this.columnGrandTotalFirst.Name = "columnGrandTotalFirst";
            this.columnGrandTotalFirst.Size = new System.Drawing.Size(41, 18);
            this.columnGrandTotalFirst.TabIndex = 3;
            this.columnGrandTotalFirst.Text = "First";
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox4.Controls.Add(this.columnSubTotalNone);
            this.radGroupBox4.Controls.Add(this.columnSubTotalLast);
            this.radGroupBox4.Controls.Add(this.columnSubTotalFirst);
            this.radGroupBox4.HeaderText = "Columns SubTotals Position";
            this.radGroupBox4.Location = new System.Drawing.Point(10, 309);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(266, 99);
            this.radGroupBox4.TabIndex = 4;
            this.radGroupBox4.Text = "Columns SubTotals Position";
            // 
            // columnSubTotalNone
            // 
            this.columnSubTotalNone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.columnSubTotalNone.Location = new System.Drawing.Point(5, 69);
            this.columnSubTotalNone.Name = "columnSubTotalNone";
            this.columnSubTotalNone.Size = new System.Drawing.Size(48, 18);
            this.columnSubTotalNone.TabIndex = 5;
            this.columnSubTotalNone.Text = "None";
            // 
            // columnSubTotalLast
            // 
            this.columnSubTotalLast.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.columnSubTotalLast.Location = new System.Drawing.Point(5, 45);
            this.columnSubTotalLast.Name = "columnSubTotalLast";
            this.columnSubTotalLast.Size = new System.Drawing.Size(40, 18);
            this.columnSubTotalLast.TabIndex = 4;
            this.columnSubTotalLast.Text = "Last";
            // 
            // columnSubTotalFirst
            // 
            this.columnSubTotalFirst.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.columnSubTotalFirst.Location = new System.Drawing.Point(5, 21);
            this.columnSubTotalFirst.Name = "columnSubTotalFirst";
            this.columnSubTotalFirst.Size = new System.Drawing.Size(41, 18);
            this.columnSubTotalFirst.TabIndex = 3;
            this.columnSubTotalFirst.Text = "First";
            // 
            // radGroupBox5
            // 
            this.radGroupBox5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox5.Controls.Add(this.radLabel1);
            this.radGroupBox5.Controls.Add(this.radSpinEditor1);
            this.radGroupBox5.Controls.Add(this.aggregatePositionColumns);
            this.radGroupBox5.Controls.Add(this.aggregatePositionRows);
            this.radGroupBox5.HeaderText = "Aggregates Position";
            this.radGroupBox5.Location = new System.Drawing.Point(10, 414);
            this.radGroupBox5.Name = "radGroupBox5";
            this.radGroupBox5.Size = new System.Drawing.Size(266, 98);
            this.radGroupBox5.TabIndex = 5;
            this.radGroupBox5.Text = "Aggregates Position";
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(5, 70);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(91, 18);
            this.radLabel1.TabIndex = 6;
            this.radLabel1.Text = "Aggregates level:";
            // 
            // radSpinEditor1
            // 
            this.radSpinEditor1.Location = new System.Drawing.Point(121, 69);
            this.radSpinEditor1.Name = "radSpinEditor1";
            // 
            // 
            // 
            this.radSpinEditor1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor1.Size = new System.Drawing.Size(140, 20);
            this.radSpinEditor1.TabIndex = 5;
            this.radSpinEditor1.TabStop = false;
            this.radSpinEditor1.Tag = "Right";
            // 
            // aggregatePositionColumns
            // 
            this.aggregatePositionColumns.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aggregatePositionColumns.Location = new System.Drawing.Point(5, 45);
            this.aggregatePositionColumns.Name = "aggregatePositionColumns";
            this.aggregatePositionColumns.Size = new System.Drawing.Size(64, 18);
            this.aggregatePositionColumns.TabIndex = 4;
            this.aggregatePositionColumns.Text = "Columns";
            // 
            // aggregatePositionRows
            // 
            this.aggregatePositionRows.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aggregatePositionRows.Location = new System.Drawing.Point(5, 21);
            this.aggregatePositionRows.Name = "aggregatePositionRows";
            this.aggregatePositionRows.Size = new System.Drawing.Size(47, 18);
            this.aggregatePositionRows.TabIndex = 3;
            this.aggregatePositionRows.Text = "Rows";
            // 
            // radGroupBox6
            // 
            this.radGroupBox6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox6.Controls.Add(this.radButton2);
            this.radGroupBox6.Controls.Add(this.radButton1);
            this.radGroupBox6.HeaderText = "Save/Load Layout";
            this.radGroupBox6.Location = new System.Drawing.Point(10, 519);
            this.radGroupBox6.Name = "radGroupBox6";
            this.radGroupBox6.Size = new System.Drawing.Size(266, 100);
            this.radGroupBox6.TabIndex = 6;
            this.radGroupBox6.Text = "Save/Load Layout";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(33, 31);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(205, 24);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "Save";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(33, 61);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(205, 24);
            this.radButton2.TabIndex = 0;
            this.radButton2.Text = "Load";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1514, 1000);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rowGrandTotalNone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowGrandTotalLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowGrandTotalFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rowSubTotalNone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowSubTotalLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowSubTotalFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnGrandTotalNone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnGrandTotalLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnGrandTotalFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnSubTotalNone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnSubTotalLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnSubTotalFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox5)).EndInit();
            this.radGroupBox5.ResumeLayout(false);
            this.radGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggregatePositionColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggregatePositionRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox6)).EndInit();
            this.radGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            this.ResumeLayout(false);

        }

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox5;
        private Telerik.WinControls.UI.RadRadioButton rowGrandTotalFirst;
        private Telerik.WinControls.UI.RadRadioButton rowGrandTotalLast;
        private Telerik.WinControls.UI.RadRadioButton rowGrandTotalNone;
        private Telerik.WinControls.UI.RadRadioButton rowSubTotalNone;
        private Telerik.WinControls.UI.RadRadioButton rowSubTotalLast;
        private Telerik.WinControls.UI.RadRadioButton rowSubTotalFirst;
        private Telerik.WinControls.UI.RadRadioButton columnGrandTotalNone;
        private Telerik.WinControls.UI.RadRadioButton columnGrandTotalLast;
        private Telerik.WinControls.UI.RadRadioButton columnGrandTotalFirst;
        private Telerik.WinControls.UI.RadRadioButton columnSubTotalNone;
        private Telerik.WinControls.UI.RadRadioButton columnSubTotalLast;
        private Telerik.WinControls.UI.RadRadioButton columnSubTotalFirst;
        private Telerik.WinControls.UI.RadRadioButton aggregatePositionColumns;
        private Telerik.WinControls.UI.RadRadioButton aggregatePositionRows;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor1;
        private Telerik.WinControls.UI.RadLabel radLabel1;

        #endregion
        private Telerik.WinControls.UI.RadGroupBox radGroupBox6;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadButton radButton1;
    }
}