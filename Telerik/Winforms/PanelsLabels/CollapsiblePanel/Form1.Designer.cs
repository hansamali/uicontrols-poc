﻿namespace Telerik.Examples.WinControls.PanelsLabels.CollapsiblePanel
{
    partial class Form1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.radPanelDemoHolder = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.personInfoControl4 = new Telerik.Examples.WinControls.PanelsLabels.CollapsiblePanel.PersonInfoControl();
            this.personInfoControl1 = new Telerik.Examples.WinControls.PanelsLabels.CollapsiblePanel.PersonInfoControl();
            this.personInfoControl3 = new Telerik.Examples.WinControls.PanelsLabels.CollapsiblePanel.PersonInfoControl();
            this.personInfoControl2 = new Telerik.Examples.WinControls.PanelsLabels.CollapsiblePanel.PersonInfoControl();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.animationEasingTypeDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.animationTypeDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.rightToLeftCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.verticalHeaderAlignmentDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.horizontalHeaderAlignmentDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.showHeaderLineCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.animationFramesSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.animationIntervalSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.contentSizingModeDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.enableAnimationCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.expandDirectionDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personInfoControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personInfoControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personInfoControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personInfoControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.animationEasingTypeDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationTypeDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightToLeftCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.verticalHeaderAlignmentDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.horizontalHeaderAlignmentDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showHeaderLineCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationFramesSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationIntervalSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contentSizingModeDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableAnimationCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.expandDirectionDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(930, 46);
            this.settingsPanel.Size = new System.Drawing.Size(243, 749);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(1255, 46);
            this.themePanel.Size = new System.Drawing.Size(230, 369);
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radCollapsiblePanel1);
            this.radPanelDemoHolder.Location = new System.Drawing.Point(0, 0);
            this.radPanelDemoHolder.Name = "radPanelDemoHolder";
            this.radPanelDemoHolder.Size = new System.Drawing.Size(750, 450);
            this.radPanelDemoHolder.TabIndex = 2;
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 150, 200);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.BackColor = System.Drawing.Color.White;
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.personInfoControl4);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.personInfoControl1);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.personInfoControl3);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.personInfoControl2);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(748, 422);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(750, 450);
            this.radCollapsiblePanel1.TabIndex = 0;
            this.radCollapsiblePanel1.Text = "radCollapsiblePanel1";
            // 
            // personInfoControl4
            // 
            this.personInfoControl4.Location = new System.Drawing.Point(387, 73);
            this.personInfoControl4.Name = "personInfoControl4";
            this.personInfoControl4.PersonEmail = "Email: BobSmiil@mail.com";
            this.personInfoControl4.PersonImage = ((System.Drawing.Image)(resources.GetObject("personInfoControl4.PersonImage")));
            this.personInfoControl4.PersonName = "Bob Smiil";
            this.personInfoControl4.PersonPhone = "Phone: 333 2334";
            this.personInfoControl4.Size = new System.Drawing.Size(300, 123);
            this.personInfoControl4.TabIndex = 2;
            this.personInfoControl4.Text = "personInfoControl4";
            // 
            // personInfoControl1
            // 
            this.personInfoControl1.Location = new System.Drawing.Point(64, 73);
            this.personInfoControl1.Name = "personInfoControl1";
            this.personInfoControl1.PersonEmail = "Email: BobSmiil@mail.com";
            this.personInfoControl1.PersonImage = ((System.Drawing.Image)(resources.GetObject("personInfoControl1.PersonImage")));
            this.personInfoControl1.PersonName = "Bob Smiil";
            this.personInfoControl1.PersonPhone = "Phone: 333 2334";
            this.personInfoControl1.Size = new System.Drawing.Size(300, 123);
            this.personInfoControl1.TabIndex = 2;
            this.personInfoControl1.Text = "personInfoControl1";
            // 
            // personInfoControl3
            // 
            this.personInfoControl3.Location = new System.Drawing.Point(387, 231);
            this.personInfoControl3.Name = "personInfoControl3";
            this.personInfoControl3.PersonEmail = "Email: BobSmiil@mail.com";
            this.personInfoControl3.PersonImage = ((System.Drawing.Image)(resources.GetObject("personInfoControl3.PersonImage")));
            this.personInfoControl3.PersonName = "Bob Smiil";
            this.personInfoControl3.PersonPhone = "Phone: 333 2334";
            this.personInfoControl3.Size = new System.Drawing.Size(300, 123);
            this.personInfoControl3.TabIndex = 1;
            this.personInfoControl3.Text = "personInfoControl3";
            // 
            // personInfoControl2
            // 
            this.personInfoControl2.Location = new System.Drawing.Point(64, 231);
            this.personInfoControl2.Name = "personInfoControl2";
            this.personInfoControl2.PersonEmail = "Email: BobSmiil@mail.com";
            this.personInfoControl2.PersonImage = ((System.Drawing.Image)(resources.GetObject("personInfoControl2.PersonImage")));
            this.personInfoControl2.PersonName = "Bob Smiil";
            this.personInfoControl2.PersonPhone = "Phone: 333 2334";
            this.personInfoControl2.Size = new System.Drawing.Size(300, 123);
            this.personInfoControl2.TabIndex = 1;
            this.personInfoControl2.Text = "personInfoControl2";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.animationEasingTypeDropDownList);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.animationTypeDropDownList);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.rightToLeftCheckBox);
            this.radGroupBox1.Controls.Add(this.verticalHeaderAlignmentDropDownList);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.horizontalHeaderAlignmentDropDownList);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.showHeaderLineCheckBox);
            this.radGroupBox1.Controls.Add(this.animationFramesSpinEditor);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.animationIntervalSpinEditor);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.contentSizingModeDropDownList);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.enableAnimationCheckBox);
            this.radGroupBox1.Controls.Add(this.expandDirectionDropDownList);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.HeaderText = "Settings";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 32);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(223, 574);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Settings";
            // 
            // animationEasingTypeDropDownList
            // 
            this.animationEasingTypeDropDownList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.animationEasingTypeDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.animationEasingTypeDropDownList.Location = new System.Drawing.Point(5, 151);
            this.animationEasingTypeDropDownList.Name = "animationEasingTypeDropDownList";
            this.animationEasingTypeDropDownList.Size = new System.Drawing.Size(213, 20);
            this.animationEasingTypeDropDownList.TabIndex = 15;
            // 
            // radLabel8
            // 
            this.radLabel8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel8.Location = new System.Drawing.Point(5, 127);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(122, 18);
            this.radLabel8.TabIndex = 14;
            this.radLabel8.Text = "Animation Easing Type:";
            // 
            // animationTypeDropDownList
            // 
            this.animationTypeDropDownList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.animationTypeDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.animationTypeDropDownList.Location = new System.Drawing.Point(5, 97);
            this.animationTypeDropDownList.Name = "animationTypeDropDownList";
            this.animationTypeDropDownList.Size = new System.Drawing.Size(213, 20);
            this.animationTypeDropDownList.TabIndex = 13;
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel7.Location = new System.Drawing.Point(5, 73);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(86, 18);
            this.radLabel7.TabIndex = 12;
            this.radLabel7.Text = "Animation Type:";
            // 
            // rightToLeftCheckBox
            // 
            this.rightToLeftCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rightToLeftCheckBox.Location = new System.Drawing.Point(5, 517);
            this.rightToLeftCheckBox.Name = "rightToLeftCheckBox";
            this.rightToLeftCheckBox.Size = new System.Drawing.Size(78, 18);
            this.rightToLeftCheckBox.TabIndex = 11;
            this.rightToLeftCheckBox.Text = "RightToLeft";
            // 
            // verticalHeaderAlignmentDropDownList
            // 
            this.verticalHeaderAlignmentDropDownList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.verticalHeaderAlignmentDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.verticalHeaderAlignmentDropDownList.Location = new System.Drawing.Point(5, 490);
            this.verticalHeaderAlignmentDropDownList.Name = "verticalHeaderAlignmentDropDownList";
            this.verticalHeaderAlignmentDropDownList.Size = new System.Drawing.Size(213, 20);
            this.verticalHeaderAlignmentDropDownList.TabIndex = 10;
            // 
            // radLabel6
            // 
            this.radLabel6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel6.Location = new System.Drawing.Point(5, 465);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(139, 18);
            this.radLabel6.TabIndex = 9;
            this.radLabel6.Text = "Vertical Header Alignment:";
            // 
            // horizontalHeaderAlignmentDropDownList
            // 
            this.horizontalHeaderAlignmentDropDownList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.horizontalHeaderAlignmentDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.horizontalHeaderAlignmentDropDownList.Location = new System.Drawing.Point(5, 430);
            this.horizontalHeaderAlignmentDropDownList.Name = "horizontalHeaderAlignmentDropDownList";
            this.horizontalHeaderAlignmentDropDownList.Size = new System.Drawing.Size(213, 20);
            this.horizontalHeaderAlignmentDropDownList.TabIndex = 9;
            // 
            // radLabel5
            // 
            this.radLabel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel5.Location = new System.Drawing.Point(5, 405);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(154, 18);
            this.radLabel5.TabIndex = 8;
            this.radLabel5.Text = "Horizontal Header Alignment:";
            // 
            // showHeaderLineCheckBox
            // 
            this.showHeaderLineCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.showHeaderLineCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showHeaderLineCheckBox.Location = new System.Drawing.Point(5, 377);
            this.showHeaderLineCheckBox.Name = "showHeaderLineCheckBox";
            this.showHeaderLineCheckBox.Size = new System.Drawing.Size(113, 18);
            this.showHeaderLineCheckBox.TabIndex = 7;
            this.showHeaderLineCheckBox.Text = "Show Header Line:";
            this.showHeaderLineCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // animationFramesSpinEditor
            // 
            this.animationFramesSpinEditor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.animationFramesSpinEditor.Location = new System.Drawing.Point(5, 339);
            this.animationFramesSpinEditor.Name = "animationFramesSpinEditor";
            this.animationFramesSpinEditor.Size = new System.Drawing.Size(213, 20);
            this.animationFramesSpinEditor.TabIndex = 6;
            this.animationFramesSpinEditor.TabStop = false;
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel4.Location = new System.Drawing.Point(5, 315);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(98, 18);
            this.radLabel4.TabIndex = 6;
            this.radLabel4.Text = "Animation Frames:";
            // 
            // animationIntervalSpinEditor
            // 
            this.animationIntervalSpinEditor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.animationIntervalSpinEditor.Location = new System.Drawing.Point(5, 289);
            this.animationIntervalSpinEditor.Name = "animationIntervalSpinEditor";
            this.animationIntervalSpinEditor.Size = new System.Drawing.Size(213, 20);
            this.animationIntervalSpinEditor.TabIndex = 5;
            this.animationIntervalSpinEditor.TabStop = false;
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(5, 265);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(99, 18);
            this.radLabel3.TabIndex = 4;
            this.radLabel3.Text = "Animation Interval:";
            // 
            // contentSizingModeDropDownList
            // 
            this.contentSizingModeDropDownList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.contentSizingModeDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.contentSizingModeDropDownList.Location = new System.Drawing.Point(5, 205);
            this.contentSizingModeDropDownList.Name = "contentSizingModeDropDownList";
            this.contentSizingModeDropDownList.Size = new System.Drawing.Size(213, 20);
            this.contentSizingModeDropDownList.TabIndex = 3;
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(5, 180);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(113, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Content Sizing Mode:";
            // 
            // enableAnimationCheckBox
            // 
            this.enableAnimationCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.enableAnimationCheckBox.Location = new System.Drawing.Point(5, 240);
            this.enableAnimationCheckBox.Name = "enableAnimationCheckBox";
            this.enableAnimationCheckBox.Size = new System.Drawing.Size(108, 18);
            this.enableAnimationCheckBox.TabIndex = 2;
            this.enableAnimationCheckBox.Text = "Enable Animation";
            // 
            // expandDirectionDropDownList
            // 
            this.expandDirectionDropDownList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.expandDirectionDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.expandDirectionDropDownList.Location = new System.Drawing.Point(5, 47);
            this.expandDirectionDropDownList.Name = "expandDirectionDropDownList";
            this.expandDirectionDropDownList.Size = new System.Drawing.Size(213, 20);
            this.expandDirectionDropDownList.TabIndex = 1;
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(5, 22);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(93, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Expand Direction:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanelDemoHolder);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1409, 808);
            this.Controls.SetChildIndex(this.radPanelDemoHolder, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personInfoControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personInfoControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personInfoControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personInfoControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.animationEasingTypeDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationTypeDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightToLeftCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.verticalHeaderAlignmentDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.horizontalHeaderAlignmentDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showHeaderLineCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationFramesSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationIntervalSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contentSizingModeDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableAnimationCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.expandDirectionDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanelDemoHolder;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDropDownList expandDirectionDropDownList;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private PersonInfoControl personInfoControl3;
        private PersonInfoControl personInfoControl2;
        private PersonInfoControl personInfoControl4;
        private PersonInfoControl personInfoControl1;
        private Telerik.WinControls.UI.RadCheckBox enableAnimationCheckBox;
        private Telerik.WinControls.UI.RadDropDownList contentSizingModeDropDownList;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadSpinEditor animationFramesSpinEditor;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadSpinEditor animationIntervalSpinEditor;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadCheckBox showHeaderLineCheckBox;
        private Telerik.WinControls.UI.RadDropDownList horizontalHeaderAlignmentDropDownList;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList verticalHeaderAlignmentDropDownList;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadCheckBox rightToLeftCheckBox;
        private Telerik.WinControls.UI.RadDropDownList animationTypeDropDownList;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadDropDownList animationEasingTypeDropDownList;
        private Telerik.WinControls.UI.RadLabel radLabel8;
    }
}
