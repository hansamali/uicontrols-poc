using Telerik.WinControls.UI;
namespace Telerik.Examples.WinControls.DropDownListAndListControl.DropDownList.AutoComplete
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.RadListDataItem radListDataItem18 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem19 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem20 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem21 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radGroupBoxSettings = new Telerik.WinControls.UI.RadGroupBox();
            this.radBtnAdd = new Telerik.WinControls.UI.RadButton();
            this.radBtnRemove = new Telerik.WinControls.UI.RadButton();
            this.radTxtText = new Telerik.WinControls.UI.RadTextBox();
            this.radTxtIndex = new Telerik.WinControls.UI.RadTextBox();
            this.radLblText = new Telerik.WinControls.UI.RadLabel();
            this.radLblItemIndex = new Telerik.WinControls.UI.RadLabel();
            this.radCheckCaseSens = new Telerik.WinControls.UI.RadCheckBox();
            this.radLblAutoComplete = new Telerik.WinControls.UI.RadLabel();
            this.radComboAutoCompl = new Telerik.WinControls.UI.RadDropDownList();
            this.radComboDemo = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBoxSettings)).BeginInit();
            this.radGroupBoxSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTxtText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTxtIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblItemIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckCaseSens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblAutoComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboAutoCompl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboDemo)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radComboDemo);
            
            this.radPanelDemoHolder.Size = new System.Drawing.Size(362, 100);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBoxSettings);
            this.settingsPanel.Location = new System.Drawing.Point(1023, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 735);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBoxSettings, 0);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "WizardPicture.bmp");
            this.imageList1.Images.SetKeyName(1, "bulb_on.GIF");
            this.imageList1.Images.SetKeyName(2, "CLSDFOLD.BMP");
            this.imageList1.Images.SetKeyName(3, "test.bmp");
            this.imageList1.Images.SetKeyName(4, "untitled.bmp");
            // 
            // radGroupBoxSettings
            // 
            this.radGroupBoxSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBoxSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBoxSettings.Controls.Add(this.radBtnAdd);
            this.radGroupBoxSettings.Controls.Add(this.radBtnRemove);
            this.radGroupBoxSettings.Controls.Add(this.radTxtText);
            this.radGroupBoxSettings.Controls.Add(this.radTxtIndex);
            this.radGroupBoxSettings.Controls.Add(this.radLblText);
            this.radGroupBoxSettings.Controls.Add(this.radLblItemIndex);
            this.radGroupBoxSettings.Controls.Add(this.radCheckCaseSens);
            this.radGroupBoxSettings.Controls.Add(this.radLblAutoComplete);
            this.radGroupBoxSettings.Controls.Add(this.radComboAutoCompl);
            this.radGroupBoxSettings.FooterText = "";
            
            this.radGroupBoxSettings.HeaderMargin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radGroupBoxSettings.HeaderText = " Settings ";
            this.radGroupBoxSettings.Location = new System.Drawing.Point(10, 6);
            this.radGroupBoxSettings.Name = "radGroupBoxSettings";
            this.radGroupBoxSettings.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBoxSettings.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBoxSettings.Size = new System.Drawing.Size(180, 198);
            this.radGroupBoxSettings.TabIndex = 0;
            this.radGroupBoxSettings.Text = " Settings ";
            // 
            // radBtnAdd
            // 
            this.radBtnAdd.Location = new System.Drawing.Point(7, 165);
            this.radBtnAdd.Name = "radBtnAdd";
            this.radBtnAdd.Size = new System.Drawing.Size(66, 23);
            this.radBtnAdd.TabIndex = 8;
            this.radBtnAdd.Tag = "NotTouch";
            this.radBtnAdd.Text = "Add";
        
            // 
            // radBtnRemove
            // 
            this.radBtnRemove.Location = new System.Drawing.Point(86, 165);
            this.radBtnRemove.Name = "radBtnRemove";
            this.radBtnRemove.Size = new System.Drawing.Size(67, 23);
            this.radBtnRemove.TabIndex = 7;
            this.radBtnRemove.Tag = "NotTouch";
            this.radBtnRemove.Text = "Remove";

            // 
            // radTxtText
            // 
            
            this.radTxtText.Location = new System.Drawing.Point(43, 138);
            this.radTxtText.Name = "radTxtText";
            this.radTxtText.Size = new System.Drawing.Size(110, 20);
            this.radTxtText.TabIndex = 6;
            this.radTxtText.TabStop = false;
            this.radTxtText.Tag = "Right";
          
            // 
            // radTxtIndex
            // 
            
            this.radTxtIndex.Location = new System.Drawing.Point(43, 111);
            this.radTxtIndex.Name = "radTxtIndex";
            this.radTxtIndex.ReadOnly = true;
            this.radTxtIndex.Size = new System.Drawing.Size(109, 20);
            this.radTxtIndex.TabIndex = 5;
            this.radTxtIndex.TabStop = false;
            this.radTxtIndex.Tag = "Right";
            // 
            // radLblText
            // 
            
            this.radLblText.Location = new System.Drawing.Point(13, 141);
            this.radLblText.Name = "radLblText";
            this.radLblText.Size = new System.Drawing.Size(30, 18);
            this.radLblText.TabIndex = 4;
            this.radLblText.Text = "Text:";
            // 
            // radLblItemIndex
            // 
            
            this.radLblItemIndex.Location = new System.Drawing.Point(7, 114);
            this.radLblItemIndex.Name = "radLblItemIndex";
            this.radLblItemIndex.Size = new System.Drawing.Size(36, 18);
            this.radLblItemIndex.TabIndex = 3;
            this.radLblItemIndex.Text = "Index:";
            // 
            // radCheckCaseSens
            // 
            
            this.radCheckCaseSens.Location = new System.Drawing.Point(8, 76);
            this.radCheckCaseSens.Name = "radCheckCaseSens";
            this.radCheckCaseSens.Size = new System.Drawing.Size(98, 33);
            this.radCheckCaseSens.TabIndex = 2;
            this.radCheckCaseSens.Text = "Case sensitive \r\nAutoComplete";
         
            // 
            // radLblAutoComplete
            // 
            
            this.radLblAutoComplete.Location = new System.Drawing.Point(16, 28);
            this.radLblAutoComplete.Name = "radLblAutoComplete";
            this.radLblAutoComplete.Size = new System.Drawing.Size(114, 18);
            this.radLblAutoComplete.TabIndex = 1;
            this.radLblAutoComplete.Text = "AutoComplete Mode:";
            // 
            // radComboAutoCompl
            // 
            this.radComboAutoCompl.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            
            this.radComboAutoCompl.FormatString = "{0}";
            radListDataItem18.Text = "None";
            radListDataItem18.TextWrap = true;
            radListDataItem19.Text = "Suggest";
            radListDataItem19.TextWrap = true;
            radListDataItem20.Text = "Append";
            radListDataItem20.TextWrap = true;
            radListDataItem21.Text = "SuggestAppend";
            radListDataItem21.TextWrap = true;
            this.radComboAutoCompl.Items.Add(radListDataItem18);
            this.radComboAutoCompl.Items.Add(radListDataItem19);
            this.radComboAutoCompl.Items.Add(radListDataItem20);
            this.radComboAutoCompl.Items.Add(radListDataItem21);
            this.radComboAutoCompl.Location = new System.Drawing.Point(13, 49);
            this.radComboAutoCompl.Name = "radComboAutoCompl";
            // 
            // 
            // 
            this.radComboAutoCompl.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboAutoCompl.Size = new System.Drawing.Size(139, 20);
            this.radComboAutoCompl.TabIndex = 0;
            this.radComboAutoCompl.Text = "Select mode:";
         
            // 
            // radComboDemo
            // 
            
            this.radComboDemo.FormatString = "{0}";
            radListDataItem1.Image = global::Telerik.Examples.WinControls.Properties.Resources.BUL;
            radListDataItem1.Text = "Bulgaria";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Image = global::Telerik.Examples.WinControls.Properties.Resources.DE;
            radListDataItem2.Text = "Germany";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Image = global::Telerik.Examples.WinControls.Properties.Resources.FR;
            radListDataItem3.Text = "France";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Image = global::Telerik.Examples.WinControls.Properties.Resources.IN;
            radListDataItem4.Text = "India";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Image = global::Telerik.Examples.WinControls.Properties.Resources.US;
            radListDataItem5.Text = "USA";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Image = global::Telerik.Examples.WinControls.Properties.Resources.CN;
            radListDataItem6.Text = "China";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Image = global::Telerik.Examples.WinControls.Properties.Resources.JP;
            radListDataItem7.Text = "Japan";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Image = global::Telerik.Examples.WinControls.Properties.Resources.NL;
            radListDataItem8.Text = "Netherlands";
            radListDataItem8.TextWrap = true;
            radListDataItem9.Image = global::Telerik.Examples.WinControls.Properties.Resources.BE;
            radListDataItem9.Text = "Belgium";
            radListDataItem9.TextWrap = true;
            radListDataItem10.Image = global::Telerik.Examples.WinControls.Properties.Resources.SP;
            radListDataItem10.Text = "Spain";
            radListDataItem10.TextWrap = true;
            radListDataItem11.Image = global::Telerik.Examples.WinControls.Properties.Resources.PT;
            radListDataItem11.Text = "Portugal";
            radListDataItem11.TextWrap = true;
            radListDataItem12.Image = global::Telerik.Examples.WinControls.Properties.Resources.CH;
            radListDataItem12.Text = "Switzerland";
            radListDataItem12.TextWrap = true;
            radListDataItem13.Image = global::Telerik.Examples.WinControls.Properties.Resources.SE;
            radListDataItem13.Text = "Sweden";
            radListDataItem13.TextWrap = true;
            radListDataItem14.Image = global::Telerik.Examples.WinControls.Properties.Resources.EG;
            radListDataItem14.Text = "Egypt";
            radListDataItem14.TextWrap = true;
            radListDataItem15.Image = global::Telerik.Examples.WinControls.Properties.Resources.CA;
            radListDataItem15.Text = "Canada";
            radListDataItem15.TextWrap = true;
            radListDataItem16.Image = global::Telerik.Examples.WinControls.Properties.Resources.RU;
            radListDataItem16.Text = "Russia";
            radListDataItem16.TextWrap = true;
            radListDataItem17.Image = global::Telerik.Examples.WinControls.Properties.Resources.BR;
            radListDataItem17.Text = "Brazil";
            radListDataItem17.TextWrap = true;
            this.radComboDemo.Items.Add(radListDataItem1);
            this.radComboDemo.Items.Add(radListDataItem2);
            this.radComboDemo.Items.Add(radListDataItem3);
            this.radComboDemo.Items.Add(radListDataItem4);
            this.radComboDemo.Items.Add(radListDataItem5);
            this.radComboDemo.Items.Add(radListDataItem6);
            this.radComboDemo.Items.Add(radListDataItem7);
            this.radComboDemo.Items.Add(radListDataItem8);
            this.radComboDemo.Items.Add(radListDataItem9);
            this.radComboDemo.Items.Add(radListDataItem10);
            this.radComboDemo.Items.Add(radListDataItem11);
            this.radComboDemo.Items.Add(radListDataItem12);
            this.radComboDemo.Items.Add(radListDataItem13);
            this.radComboDemo.Items.Add(radListDataItem14);
            this.radComboDemo.Items.Add(radListDataItem15);
            this.radComboDemo.Items.Add(radListDataItem16);
            this.radComboDemo.Items.Add(radListDataItem17);
            this.radComboDemo.Location = new System.Drawing.Point(0, 0);
            this.radComboDemo.Name = "radComboDemo";
            // 
            // 
            // 
            this.radComboDemo.Size = new System.Drawing.Size(306, 20);
            this.radComboDemo.TabIndex = 0;
            this.radComboDemo.Text = "Choose country:";
            // 
            // Form1
            // 
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2, 35, 2, 4);
            this.Size = new System.Drawing.Size(1142, 516);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBoxSettings)).EndInit();
            this.radGroupBoxSettings.ResumeLayout(false);
            this.radGroupBoxSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTxtText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTxtIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblItemIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckCaseSens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblAutoComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboAutoCompl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboDemo)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.ImageList imageList1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBoxSettings;
        private Telerik.WinControls.UI.RadLabel radLblAutoComplete;
        private Telerik.WinControls.UI.RadDropDownList radComboAutoCompl;
        private Telerik.WinControls.UI.RadButton radBtnAdd;
        private Telerik.WinControls.UI.RadButton radBtnRemove;
        private Telerik.WinControls.UI.RadTextBox radTxtText;
        private Telerik.WinControls.UI.RadTextBox radTxtIndex;
        private Telerik.WinControls.UI.RadLabel radLblText;
        private Telerik.WinControls.UI.RadLabel radLblItemIndex;
        private Telerik.WinControls.UI.RadCheckBox radCheckCaseSens;
        private Telerik.WinControls.UI.RadDropDownList radComboDemo;
	}
}
