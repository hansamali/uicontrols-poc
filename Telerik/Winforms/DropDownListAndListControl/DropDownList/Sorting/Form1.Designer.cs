namespace Telerik.Examples.WinControls.DropDownListAndListControl.DropDownList.Sorting
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.radComboDemo = new Telerik.WinControls.UI.RadDropDownList();
            this.radGroupSettings = new Telerik.WinControls.UI.RadGroupBox();
            this.radBtnAdd = new Telerik.WinControls.UI.RadButton();
            this.radBtnRemove = new Telerik.WinControls.UI.RadButton();
            this.radTxtText = new Telerik.WinControls.UI.RadTextBox();
            this.radTxtIndex = new Telerik.WinControls.UI.RadTextBox();
            this.radLblText = new Telerik.WinControls.UI.RadLabel();
            this.radLblItemIndex = new Telerik.WinControls.UI.RadLabel();
            this.radComboSortMode = new Telerik.WinControls.UI.RadDropDownList();
            this.radLblSortMode = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radComboDemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSettings)).BeginInit();
            this.radGroupSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTxtText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTxtIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblItemIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboSortMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblSortMode)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radComboDemo);
            this.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black;
            this.radPanelDemoHolder.MaximumSize = new System.Drawing.Size(362, 100);
            this.radPanelDemoHolder.MinimumSize = new System.Drawing.Size(362, 100);
            // 
            // 
            // 
            this.radPanelDemoHolder.RootElement.MaxSize = new System.Drawing.Size(362, 100);
            this.radPanelDemoHolder.RootElement.MinSize = new System.Drawing.Size(362, 100);
            this.radPanelDemoHolder.Size = new System.Drawing.Size(362, 100);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupSettings);
            this.settingsPanel.Location = new System.Drawing.Point(1023, 1);
            this.settingsPanel.Size = new System.Drawing.Size(230, 755);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupSettings, 0);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "WizardPicture.bmp");
            this.imageList1.Images.SetKeyName(1, "bulb_on.GIF");
            this.imageList1.Images.SetKeyName(2, "CLSDFOLD.BMP");
            this.imageList1.Images.SetKeyName(3, "test.bmp");
            this.imageList1.Images.SetKeyName(4, "untitled.bmp");
            // 
            // radComboDemo
            // 
            this.radComboDemo.DropDownSizingMode = ((Telerik.WinControls.UI.SizingMode)((Telerik.WinControls.UI.SizingMode.RightBottom | Telerik.WinControls.UI.SizingMode.UpDown)));
            radListDataItem1.Text = "Amsterdam";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Barcelona";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "Bonn";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "Brussels";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "New York";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "London";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "Paris";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "Sofia";
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "Prague";
            radListDataItem9.TextWrap = true;
            radListDataItem10.Text = "Hamburg";
            radListDataItem10.TextWrap = true;
            this.radComboDemo.Items.Add(radListDataItem1);
            this.radComboDemo.Items.Add(radListDataItem2);
            this.radComboDemo.Items.Add(radListDataItem3);
            this.radComboDemo.Items.Add(radListDataItem4);
            this.radComboDemo.Items.Add(radListDataItem5);
            this.radComboDemo.Items.Add(radListDataItem6);
            this.radComboDemo.Items.Add(radListDataItem7);
            this.radComboDemo.Items.Add(radListDataItem8);
            this.radComboDemo.Items.Add(radListDataItem9);
            this.radComboDemo.Items.Add(radListDataItem10);
            this.radComboDemo.Location = new System.Drawing.Point(0, 0);
            this.radComboDemo.Name = "radComboDemo";
            this.radComboDemo.NullText = "Choose City...";
            // 
            // 
            // 
            this.radComboDemo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboDemo.Size = new System.Drawing.Size(306, 20);
            this.radComboDemo.TabIndex = 0;
            // 
            // radGroupSettings
            // 
            this.radGroupSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupSettings.Controls.Add(this.radBtnAdd);
            this.radGroupSettings.Controls.Add(this.radBtnRemove);
            this.radGroupSettings.Controls.Add(this.radTxtText);
            this.radGroupSettings.Controls.Add(this.radTxtIndex);
            this.radGroupSettings.Controls.Add(this.radLblText);
            this.radGroupSettings.Controls.Add(this.radLblItemIndex);
            this.radGroupSettings.Controls.Add(this.radComboSortMode);
            this.radGroupSettings.Controls.Add(this.radLblSortMode);
            this.radGroupSettings.FooterText = "";
            this.radGroupSettings.HeaderMargin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radGroupSettings.HeaderText = " Settings ";
            this.radGroupSettings.Location = new System.Drawing.Point(25, 6);
            this.radGroupSettings.Name = "radGroupSettings";
            this.radGroupSettings.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupSettings.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupSettings.Size = new System.Drawing.Size(180, 228);
            this.radGroupSettings.TabIndex = 0;
            this.radGroupSettings.Text = " Settings ";
            // 
            // radBtnAdd
            // 
            this.radBtnAdd.Location = new System.Drawing.Point(39, 191);
            this.radBtnAdd.Name = "radBtnAdd";
            this.radBtnAdd.Size = new System.Drawing.Size(61, 23);
            this.radBtnAdd.TabIndex = 14;
            this.radBtnAdd.Tag = "NotTouch";
            this.radBtnAdd.Text = "Add";

            // 
            // radBtnRemove
            // 
            this.radBtnRemove.Location = new System.Drawing.Point(106, 191);
            this.radBtnRemove.Name = "radBtnRemove";
            this.radBtnRemove.Size = new System.Drawing.Size(64, 23);
            this.radBtnRemove.TabIndex = 13;
            this.radBtnRemove.Tag = "NotTouch";
            this.radBtnRemove.Text = "Remove";

            // 
            // radTxtText
            // 
            this.radTxtText.Location = new System.Drawing.Point(55, 156);
            this.radTxtText.Name = "radTxtText";
            this.radTxtText.Size = new System.Drawing.Size(94, 20);
            this.radTxtText.TabIndex = 12;
            this.radTxtText.TabStop = false;
            this.radTxtText.Tag = "Right";
      
            // 
            // radTxtIndex
            // 
            this.radTxtIndex.Location = new System.Drawing.Point(55, 120);
            this.radTxtIndex.Name = "radTxtIndex";
            this.radTxtIndex.ReadOnly = true;
            this.radTxtIndex.Size = new System.Drawing.Size(94, 20);
            this.radTxtIndex.TabIndex = 11;
            this.radTxtIndex.TabStop = false;
            this.radTxtIndex.Tag = "Right";
            // 
            // radLblText
            // 
            this.radLblText.Location = new System.Drawing.Point(20, 159);
            this.radLblText.Name = "radLblText";
            this.radLblText.Size = new System.Drawing.Size(30, 18);
            this.radLblText.TabIndex = 10;
            this.radLblText.Text = "Text:";
            // 
            // radLblItemIndex
            // 
            this.radLblItemIndex.Location = new System.Drawing.Point(14, 123);
            this.radLblItemIndex.Name = "radLblItemIndex";
            this.radLblItemIndex.Size = new System.Drawing.Size(36, 18);
            this.radLblItemIndex.TabIndex = 9;
            this.radLblItemIndex.Text = "Index:";
            // 
            // radComboSortMode
            // 
            this.radComboSortMode.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem11.Text = "Ascending";
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "Descending";
            radListDataItem12.TextWrap = true;
            radListDataItem13.Text = "None";
            radListDataItem13.TextWrap = true;
            this.radComboSortMode.Items.Add(radListDataItem11);
            this.radComboSortMode.Items.Add(radListDataItem12);
            this.radComboSortMode.Items.Add(radListDataItem13);
            this.radComboSortMode.Location = new System.Drawing.Point(14, 52);
            this.radComboSortMode.Name = "radComboSortMode";
            // 
            // 
            // 
            this.radComboSortMode.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboSortMode.Size = new System.Drawing.Size(135, 20);
            this.radComboSortMode.TabIndex = 1;

            // 
            // radLblSortMode
            // 
            this.radLblSortMode.Location = new System.Drawing.Point(12, 28);
            this.radLblSortMode.Name = "radLblSortMode";
            this.radLblSortMode.Size = new System.Drawing.Size(61, 18);
            this.radLblSortMode.TabIndex = 0;
            this.radLblSortMode.Text = "Sort mode:";
            // 
            // Form1
            // 
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2, 35, 2, 4);
            this.Size = new System.Drawing.Size(1142, 536);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radComboDemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSettings)).EndInit();
            this.radGroupSettings.ResumeLayout(false);
            this.radGroupSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTxtText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTxtIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblItemIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboSortMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblSortMode)).EndInit();
            this.ResumeLayout(false);

		}


		#endregion

        private Telerik.WinControls.UI.RadDropDownList radComboDemo;
		private System.Windows.Forms.ImageList imageList1;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private Telerik.WinControls.UI.RadGroupBox radGroupSettings;
        private Telerik.WinControls.UI.RadDropDownList radComboSortMode;
        private Telerik.WinControls.UI.RadLabel radLblSortMode;
        private Telerik.WinControls.UI.RadButton radBtnAdd;
        private Telerik.WinControls.UI.RadButton radBtnRemove;
        private Telerik.WinControls.UI.RadTextBox radTxtText;
        private Telerik.WinControls.UI.RadTextBox radTxtIndex;
        private Telerik.WinControls.UI.RadLabel radLblText;
        private Telerik.WinControls.UI.RadLabel radLblItemIndex;
	}
}