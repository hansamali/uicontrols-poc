namespace Telerik.Examples.WinControls.DropDownListAndListControl.ListControl.Settings
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.RadListDataItem radListDataItem28 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem29 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem30 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem31 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem32 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem33 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem34 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem18 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem19 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem20 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem21 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem22 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem23 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem24 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem25 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem26 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem27 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radListBoxDemo = new Telerik.WinControls.UI.RadListControl();
            this.radGroupSettings = new Telerik.WinControls.UI.RadGroupBox();
            this.radComboSortStyle = new Telerik.WinControls.UI.RadDropDownList();
            this.radComboSelectionMode = new Telerik.WinControls.UI.RadDropDownList();
            this.radLblSort = new Telerik.WinControls.UI.RadLabel();
            this.radLblSelection = new Telerik.WinControls.UI.RadLabel();
            this.radGroupItem = new Telerik.WinControls.UI.RadGroupBox();
            this.radComboTextOrientation = new Telerik.WinControls.UI.RadDropDownList();
            this.radComboTextAlign = new Telerik.WinControls.UI.RadDropDownList();
            this.radComboImageAlign = new Telerik.WinControls.UI.RadDropDownList();
            this.radComboTextImage = new Telerik.WinControls.UI.RadDropDownList();
            this.radLblTextImage = new Telerik.WinControls.UI.RadLabel();
            this.radLblTextOrientation = new Telerik.WinControls.UI.RadLabel();
            this.radLblTextAlignment = new Telerik.WinControls.UI.RadLabel();
            this.radLblImageAlign = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radListBoxDemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSettings)).BeginInit();
            this.radGroupSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radComboSortStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboSelectionMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblSort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupItem)).BeginInit();
            this.radGroupItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radComboTextOrientation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboTextAlign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboImageAlign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboTextImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblTextImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblTextOrientation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblTextAlignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblImageAlign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radLabel1);
            this.settingsPanel.Controls.Add(this.radDropDownList1);
            this.settingsPanel.Controls.Add(this.radGroupItem);
            this.settingsPanel.Controls.Add(this.radGroupSettings);
            this.settingsPanel.Location = new System.Drawing.Point(1101, 1);
            this.settingsPanel.Size = new System.Drawing.Size(255, 747);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupSettings, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupItem, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radDropDownList1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel1, 0);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Fuchsia;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "MPEG.gif");
            this.imageList1.Images.SetKeyName(6, "Quicktime.gif");
            this.imageList1.Images.SetKeyName(7, "AudioCD.gif");
            this.imageList1.Images.SetKeyName(8, "Folder.gif");
            this.imageList1.Images.SetKeyName(9, "Help.gif");
            // 
            // radListBoxDemo
            // 
            this.radListBoxDemo.AutoSizeItems = true;
            this.radListBoxDemo.Location = new System.Drawing.Point(0, 0);
            this.radListBoxDemo.Name = "radListBoxDemo";
            this.radListBoxDemo.Size = new System.Drawing.Size(329, 368);
            this.radListBoxDemo.TabIndex = 3;
            this.radListBoxDemo.Text = "(NONE)";
            // 
            // radGroupSettings
            // 
            this.radGroupSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupSettings.Controls.Add(this.radCheckBox1);
            this.radGroupSettings.Controls.Add(this.radComboSortStyle);
            this.radGroupSettings.Controls.Add(this.radComboSelectionMode);
            this.radGroupSettings.Controls.Add(this.radLblSort);
            this.radGroupSettings.Controls.Add(this.radLblSelection);
            this.radGroupSettings.FooterText = "";
            this.radGroupSettings.HeaderText = "ListBox General Settings";
            this.radGroupSettings.Location = new System.Drawing.Point(10, 54);
            this.radGroupSettings.Name = "radGroupSettings";
            this.radGroupSettings.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupSettings.Size = new System.Drawing.Size(235, 143);
            this.radGroupSettings.TabIndex = 0;
            this.radGroupSettings.Text = "ListBox General Settings";
            // 
            // radComboSortStyle
            // 
            this.radComboSortStyle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboSortStyle.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem28.Text = "Ascending";
            radListDataItem29.Text = "Descending";
            radListDataItem30.Text = "None";
            this.radComboSortStyle.Items.Add(radListDataItem28);
            this.radComboSortStyle.Items.Add(radListDataItem29);
            this.radComboSortStyle.Items.Add(radListDataItem30);
            this.radComboSortStyle.Location = new System.Drawing.Point(5, 85);
            this.radComboSortStyle.Name = "radComboSortStyle";
            // 
            // 
            // 
            this.radComboSortStyle.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboSortStyle.Size = new System.Drawing.Size(225, 20);
            this.radComboSortStyle.TabIndex = 1;
            // 
            // radComboSelectionMode
            // 
            this.radComboSelectionMode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboSelectionMode.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem31.Text = "None";
            radListDataItem32.Text = "One";
            radListDataItem33.Text = "MultiSimple";
            radListDataItem34.Text = "MultiExtended";
            this.radComboSelectionMode.Items.Add(radListDataItem31);
            this.radComboSelectionMode.Items.Add(radListDataItem32);
            this.radComboSelectionMode.Items.Add(radListDataItem33);
            this.radComboSelectionMode.Items.Add(radListDataItem34);
            this.radComboSelectionMode.Location = new System.Drawing.Point(5, 39);
            this.radComboSelectionMode.Name = "radComboSelectionMode";
            // 
            // 
            // 
            this.radComboSelectionMode.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboSelectionMode.Size = new System.Drawing.Size(225, 20);
            this.radComboSelectionMode.TabIndex = 1;
            // 
            // radLblSort
            // 
            this.radLblSort.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblSort.Location = new System.Drawing.Point(5, 65);
            this.radLblSort.Name = "radLblSort";
            this.radLblSort.Size = new System.Drawing.Size(55, 18);
            this.radLblSort.TabIndex = 0;
            this.radLblSort.Text = "Sort Style:";
            // 
            // radLblSelection
            // 
            this.radLblSelection.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblSelection.Location = new System.Drawing.Point(5, 19);
            this.radLblSelection.Name = "radLblSelection";
            this.radLblSelection.Size = new System.Drawing.Size(86, 18);
            this.radLblSelection.TabIndex = 0;
            this.radLblSelection.Text = "Selection Mode:";
            // 
            // radGroupItem
            // 
            this.radGroupItem.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupItem.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupItem.Controls.Add(this.radComboTextOrientation);
            this.radGroupItem.Controls.Add(this.radComboTextAlign);
            this.radGroupItem.Controls.Add(this.radComboImageAlign);
            this.radGroupItem.Controls.Add(this.radComboTextImage);
            this.radGroupItem.Controls.Add(this.radLblTextImage);
            this.radGroupItem.Controls.Add(this.radLblTextOrientation);
            this.radGroupItem.Controls.Add(this.radLblTextAlignment);
            this.radGroupItem.Controls.Add(this.radLblImageAlign);
            this.radGroupItem.FooterText = "";
            this.radGroupItem.HeaderText = "Item Settings";
            this.radGroupItem.Location = new System.Drawing.Point(10, 203);
            this.radGroupItem.Name = "radGroupItem";
            this.radGroupItem.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupItem.Size = new System.Drawing.Size(235, 225);
            this.radGroupItem.TabIndex = 0;
            this.radGroupItem.Text = "Item Settings";
            // 
            // radComboTextOrientation
            // 
            this.radComboTextOrientation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboTextOrientation.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem3.Text = "Horizontal";
            radListDataItem4.Text = "Vertical";
            this.radComboTextOrientation.Items.Add(radListDataItem3);
            this.radComboTextOrientation.Items.Add(radListDataItem4);
            this.radComboTextOrientation.Location = new System.Drawing.Point(5, 182);
            this.radComboTextOrientation.Name = "radComboTextOrientation";
            // 
            // 
            // 
            this.radComboTextOrientation.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboTextOrientation.Size = new System.Drawing.Size(225, 20);
            this.radComboTextOrientation.TabIndex = 1;
            // 
            // radComboTextAlign
            // 
            this.radComboTextAlign.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboTextAlign.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem5.Text = "TopLeft";
            radListDataItem6.Text = "TopCenter";
            radListDataItem7.Text = "TopRight";
            radListDataItem8.Text = "MiddleLeft";
            radListDataItem9.Text = "MiddleCenter";
            radListDataItem10.Text = "MiddleRight";
            radListDataItem11.Text = "BottomLeft";
            radListDataItem12.Text = "BottomCenter";
            radListDataItem13.Text = "BottomRight";
            this.radComboTextAlign.Items.Add(radListDataItem5);
            this.radComboTextAlign.Items.Add(radListDataItem6);
            this.radComboTextAlign.Items.Add(radListDataItem7);
            this.radComboTextAlign.Items.Add(radListDataItem8);
            this.radComboTextAlign.Items.Add(radListDataItem9);
            this.radComboTextAlign.Items.Add(radListDataItem10);
            this.radComboTextAlign.Items.Add(radListDataItem11);
            this.radComboTextAlign.Items.Add(radListDataItem12);
            this.radComboTextAlign.Items.Add(radListDataItem13);
            this.radComboTextAlign.Location = new System.Drawing.Point(5, 91);
            this.radComboTextAlign.Name = "radComboTextAlign";
            // 
            // 
            // 
            this.radComboTextAlign.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboTextAlign.Size = new System.Drawing.Size(225, 20);
            this.radComboTextAlign.TabIndex = 1;
            // 
            // radComboImageAlign
            // 
            this.radComboImageAlign.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboImageAlign.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem14.Text = "TopLeft";
            radListDataItem15.Text = "TopCenter";
            radListDataItem16.Text = "TopRight";
            radListDataItem17.Text = "MiddleLeft";
            radListDataItem18.Text = "MiddleCenter";
            radListDataItem19.Text = "MiddleRight";
            radListDataItem20.Text = "BottomLeft";
            radListDataItem21.Text = "BottomCenter";
            radListDataItem22.Text = "BottomRight";
            this.radComboImageAlign.Items.Add(radListDataItem14);
            this.radComboImageAlign.Items.Add(radListDataItem15);
            this.radComboImageAlign.Items.Add(radListDataItem16);
            this.radComboImageAlign.Items.Add(radListDataItem17);
            this.radComboImageAlign.Items.Add(radListDataItem18);
            this.radComboImageAlign.Items.Add(radListDataItem19);
            this.radComboImageAlign.Items.Add(radListDataItem20);
            this.radComboImageAlign.Items.Add(radListDataItem21);
            this.radComboImageAlign.Items.Add(radListDataItem22);
            this.radComboImageAlign.Location = new System.Drawing.Point(5, 44);
            this.radComboImageAlign.Name = "radComboImageAlign";
            // 
            // 
            // 
            this.radComboImageAlign.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboImageAlign.Size = new System.Drawing.Size(225, 20);
            this.radComboImageAlign.TabIndex = 1;
            // 
            // radComboTextImage
            // 
            this.radComboTextImage.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboTextImage.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem23.Text = "ImageAboveText";
            radListDataItem24.Text = "ImageBeforeText";
            radListDataItem25.Text = "TextAboveImage";
            radListDataItem26.Text = "TextBeforeImage";
            radListDataItem27.Text = "Overlay";
            this.radComboTextImage.Items.Add(radListDataItem23);
            this.radComboTextImage.Items.Add(radListDataItem24);
            this.radComboTextImage.Items.Add(radListDataItem25);
            this.radComboTextImage.Items.Add(radListDataItem26);
            this.radComboTextImage.Items.Add(radListDataItem27);
            this.radComboTextImage.Location = new System.Drawing.Point(5, 137);
            this.radComboTextImage.Name = "radComboTextImage";
            // 
            // 
            // 
            this.radComboTextImage.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboTextImage.Size = new System.Drawing.Size(225, 20);
            this.radComboTextImage.TabIndex = 1;
            // 
            // radLblTextImage
            // 
            this.radLblTextImage.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblTextImage.Location = new System.Drawing.Point(5, 117);
            this.radLblTextImage.Name = "radLblTextImage";
            this.radLblTextImage.Size = new System.Drawing.Size(108, 18);
            this.radLblTextImage.TabIndex = 0;
            this.radLblTextImage.Text = "Text-Image Relation:";
            // 
            // radLblTextOrientation
            // 
            this.radLblTextOrientation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblTextOrientation.Location = new System.Drawing.Point(5, 162);
            this.radLblTextOrientation.Name = "radLblTextOrientation";
            this.radLblTextOrientation.Size = new System.Drawing.Size(88, 18);
            this.radLblTextOrientation.TabIndex = 0;
            this.radLblTextOrientation.Text = "Text Orientation:";
            // 
            // radLblTextAlignment
            // 
            this.radLblTextAlignment.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblTextAlignment.Location = new System.Drawing.Point(5, 70);
            this.radLblTextAlignment.Name = "radLblTextAlignment";
            this.radLblTextAlignment.Size = new System.Drawing.Size(83, 18);
            this.radLblTextAlignment.TabIndex = 0;
            this.radLblTextAlignment.Text = "Text Alignment:";
            // 
            // radLblImageAlign
            // 
            this.radLblImageAlign.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblImageAlign.Location = new System.Drawing.Point(5, 23);
            this.radLblImageAlign.Name = "radLblImageAlign";
            this.radLblImageAlign.Size = new System.Drawing.Size(94, 18);
            this.radLblImageAlign.TabIndex = 0;
            this.radLblImageAlign.Text = "Image Alignment:";
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "RadListDataItems";
            radListDataItem2.Text = "DescriptionTextListDataItems";
            this.radDropDownList1.Items.Add(radListDataItem1);
            this.radDropDownList1.Items.Add(radListDataItem2);
            this.radDropDownList1.Location = new System.Drawing.Point(10, 25);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(235, 20);
            this.radDropDownList1.TabIndex = 1;
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(10, 2);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(74, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Populate with";
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.Location = new System.Drawing.Point(5, 119);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(129, 18);
            this.radCheckBox1.TabIndex = 2;
            this.radCheckBox1.Text = "Alternating item color";
            this.radCheckBox1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBox1_ToggleStateChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radListBoxDemo);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2, 35, 2, 4);
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Size = new System.Drawing.Size(1399, 917);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radListBoxDemo, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radListBoxDemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSettings)).EndInit();
            this.radGroupSettings.ResumeLayout(false);
            this.radGroupSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radComboSortStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboSelectionMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblSort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupItem)).EndInit();
            this.radGroupItem.ResumeLayout(false);
            this.radGroupItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radComboTextOrientation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboTextAlign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboImageAlign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboTextImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblTextImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblTextOrientation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblTextAlignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblImageAlign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private Telerik.WinControls.UI.RadListControl radListBoxDemo;
        private System.Windows.Forms.ImageList imageList1;
        private Telerik.WinControls.UI.RadGroupBox radGroupSettings;
        private Telerik.WinControls.UI.RadDropDownList radComboSortStyle;
        private Telerik.WinControls.UI.RadDropDownList radComboSelectionMode;
        private Telerik.WinControls.UI.RadLabel radLblSort;
        private Telerik.WinControls.UI.RadLabel radLblSelection;
        private Telerik.WinControls.UI.RadGroupBox radGroupItem;
        private Telerik.WinControls.UI.RadDropDownList radComboTextOrientation;
        private Telerik.WinControls.UI.RadDropDownList radComboTextImage;
        private Telerik.WinControls.UI.RadLabel radLblTextOrientation;
        private Telerik.WinControls.UI.RadDropDownList radComboImageAlign;
        private Telerik.WinControls.UI.RadLabel radLblImageAlign;
        private Telerik.WinControls.UI.RadDropDownList radComboTextAlign;
        private Telerik.WinControls.UI.RadLabel radLblTextAlignment;
        private Telerik.WinControls.UI.RadLabel radLblTextImage;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
	}
}