﻿namespace Telerik.Examples.WinControls.DropDownListAndListControl.CheckedDropDownList
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem1 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem2 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem3 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem4 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem5 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem6 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem7 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem8 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem9 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem10 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem11 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem12 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            Telerik.WinControls.UI.RadCheckedListDataItem radCheckedListDataItem13 = new Telerik.WinControls.UI.RadCheckedListDataItem();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckedDropDownList1 = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckedDropDownList2 = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.radTimePicker1 = new Telerik.WinControls.UI.RadTimePicker();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radToggleButton1 = new Telerik.WinControls.UI.RadToggleButton();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedDropDownList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleButton1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radToggleButton1);
            this.settingsPanel.Padding = new System.Windows.Forms.Padding(3);
            this.settingsPanel.Controls.SetChildIndex(this.radToggleButton1, 0);
            // 
            // themePanel
            // 
            this.themePanel.Padding = new System.Windows.Forms.Padding(3);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel1.Location = new System.Drawing.Point(0, 27);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(62, 21);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "New task";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(0, 103);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(51, 18);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "Category";
            // 
            // radCheckedDropDownList1
            // 
            radCheckedListDataItem1.Checked = true;
            radCheckedListDataItem1.Text = "work";
            radCheckedListDataItem2.Checked = true;
            radCheckedListDataItem2.Text = "to do";
            radCheckedListDataItem3.Checked = true;
            radCheckedListDataItem3.Text = "emails";
            radCheckedListDataItem4.Text = "forwarded";
            radCheckedListDataItem5.Text = "replied";
            radCheckedListDataItem6.Text = "duplicate";
            this.radCheckedDropDownList1.Items.Add(radCheckedListDataItem1);
            this.radCheckedDropDownList1.Items.Add(radCheckedListDataItem2);
            this.radCheckedDropDownList1.Items.Add(radCheckedListDataItem3);
            this.radCheckedDropDownList1.Items.Add(radCheckedListDataItem4);
            this.radCheckedDropDownList1.Items.Add(radCheckedListDataItem5);
            this.radCheckedDropDownList1.Items.Add(radCheckedListDataItem6);
            this.radCheckedDropDownList1.Location = new System.Drawing.Point(1, 121);
            this.radCheckedDropDownList1.Name = "radCheckedDropDownList1";
            this.radCheckedDropDownList1.Size = new System.Drawing.Size(300, 20);
            this.radCheckedDropDownList1.TabIndex = 5;
            this.radCheckedDropDownList1.Text = "work;to do;emails;";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(0, 154);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(54, 18);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "Reminder";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(0, 210);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(98, 18);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Text = "Appointment time";
            // 
            // radCheckedDropDownList2
            // 
            radCheckedListDataItem7.Checked = true;
            radCheckedListDataItem7.Text = "Monday";
            radCheckedListDataItem8.Checked = true;
            radCheckedListDataItem8.Text = "Tuesday";
            radCheckedListDataItem9.Text = "Wednesday";
            radCheckedListDataItem10.Text = "Thursday";
            radCheckedListDataItem11.Text = "Friday";
            radCheckedListDataItem12.Text = "Saturday";
            radCheckedListDataItem13.Text = "Sunday";
            this.radCheckedDropDownList2.Items.Add(radCheckedListDataItem7);
            this.radCheckedDropDownList2.Items.Add(radCheckedListDataItem8);
            this.radCheckedDropDownList2.Items.Add(radCheckedListDataItem9);
            this.radCheckedDropDownList2.Items.Add(radCheckedListDataItem10);
            this.radCheckedDropDownList2.Items.Add(radCheckedListDataItem11);
            this.radCheckedDropDownList2.Items.Add(radCheckedListDataItem12);
            this.radCheckedDropDownList2.Items.Add(radCheckedListDataItem13);
            this.radCheckedDropDownList2.Location = new System.Drawing.Point(0, 173);
            this.radCheckedDropDownList2.Name = "radCheckedDropDownList2";
            this.radCheckedDropDownList2.Size = new System.Drawing.Size(300, 20);
            this.radCheckedDropDownList2.TabIndex = 8;
            this.radCheckedDropDownList2.Text = "Monday;Tuesday;";
            // 
            // radTimePicker1
            // 
            this.radTimePicker1.Location = new System.Drawing.Point(0, 230);
            this.radTimePicker1.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 0);
            this.radTimePicker1.MinValue = new System.DateTime(((long)(0)));
            this.radTimePicker1.Name = "radTimePicker1";
            this.radTimePicker1.Size = new System.Drawing.Size(298, 20);
            this.radTimePicker1.TabIndex = 9;
            this.radTimePicker1.TabStop = false;
            this.radTimePicker1.Text = "radTimePicker1";
            this.radTimePicker1.Value = new System.DateTime(2014, 9, 3, 10, 43, 22, 508);
            // 
            // radTextBox1
            // 
            this.radTextBox1.Location = new System.Drawing.Point(0, 70);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.NullText = "Task name";
            this.radTextBox1.Size = new System.Drawing.Size(300, 20);
            this.radTextBox1.TabIndex = 10;
            this.radTextBox1.Text = "Task name";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(0, 279);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(110, 24);
            this.radButton1.TabIndex = 11;
            this.radButton1.Text = "Create";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radToggleButton1
            // 
            this.radToggleButton1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radToggleButton1.Location = new System.Drawing.Point(10, 57);
            this.radToggleButton1.Name = "radToggleButton1";
            this.radToggleButton1.Size = new System.Drawing.Size(210, 24);
            this.radToggleButton1.TabIndex = 1;
            this.radToggleButton1.Text = "Show CheckAll Item";
            this.radToggleButton1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radToggleButton1_ToggleStateChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.radTextBox1);
            this.Controls.Add(this.radTimePicker1);
            this.Controls.Add(this.radCheckedDropDownList2);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radCheckedDropDownList1);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1220, 695);
            this.Controls.SetChildIndex(this.radLabel1, 0);
            this.Controls.SetChildIndex(this.radLabel2, 0);
            this.Controls.SetChildIndex(this.radCheckedDropDownList1, 0);
            this.Controls.SetChildIndex(this.radLabel3, 0);
            this.Controls.SetChildIndex(this.radLabel4, 0);
            this.Controls.SetChildIndex(this.radCheckedDropDownList2, 0);
            this.Controls.SetChildIndex(this.radTimePicker1, 0);
            this.Controls.SetChildIndex(this.radTextBox1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radButton1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedDropDownList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleButton1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadCheckedDropDownList radCheckedDropDownList1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadCheckedDropDownList radCheckedDropDownList2;
        private Telerik.WinControls.UI.RadTimePicker radTimePicker1;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadToggleButton radToggleButton1;
    }
}