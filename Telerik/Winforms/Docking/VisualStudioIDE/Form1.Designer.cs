﻿using System.Collections.Generic;
using Telerik.WinControls.UI;
namespace Telerik.Examples.WinControls.Docking.VisualStudioIDE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem18 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem19 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem20 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem21 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem22 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem23 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem24 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadTreeNode radTreeNode1 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode2 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode3 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode4 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode5 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode6 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode7 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode8 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode9 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode10 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode11 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode12 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode13 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode14 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode15 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode16 = new Telerik.WinControls.UI.RadTreeNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadListDataItem radListDataItem25 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem26 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewImageColumn gridViewImageColumn1 = new Telerik.WinControls.UI.GridViewImageColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radDock1 = new Telerik.WinControls.UI.Docking.RadDock();
            this.toolWindow1 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radListBox1 = new Telerik.WinControls.UI.RadListControl();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.toolTabStrip3 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.toolTabStrip4 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.toolWindow2 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolTabStrip5 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.toolWindow3 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.richTextBoxOutput = new Telerik.WinControls.UI.RadTextBoxControl();
            this.toolWindow4 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radGridViewTasks = new Telerik.WinControls.UI.RadGridView();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radComboBox1 = new Telerik.WinControls.UI.RadDropDownList();
            this.toolWindow5 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radGridViewErrors = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).BeginInit();
            this.radDock1.SuspendLayout();
            this.toolWindow1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radListBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).BeginInit();
            this.toolTabStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip4)).BeginInit();
            this.toolTabStrip4.SuspendLayout();
            this.toolWindow2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip5)).BeginInit();
            this.toolTabStrip5.SuspendLayout();
            this.toolWindow3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.richTextBoxOutput)).BeginInit();
            this.toolWindow4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewTasks.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radComboBox1)).BeginInit();
            this.toolWindow5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewErrors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewErrors.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radDock1
            // 
            this.radDock1.ActiveWindow = this.toolWindow1;
            this.radDock1.AutoHideAnimation = Telerik.WinControls.UI.Docking.AutoHideAnimateMode.AnimateHide;
            this.radDock1.CausesValidation = false;
            this.radDock1.Controls.Add(this.radSplitContainer1);
            this.radDock1.Controls.Add(this.toolTabStrip5);
            this.radDock1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDock1.IsCleanUpTarget = true;
            this.radDock1.Location = new System.Drawing.Point(0, 0);
            this.radDock1.MainDocumentContainer = this.documentContainer1;
            this.radDock1.Name = "radDock1";
            this.radDock1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radDock1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radDock1.ShowDocumentCloseButton = true;
            this.radDock1.ShowDocumentPinButton = true;
            this.radDock1.Size = new System.Drawing.Size(1115, 755);
            this.radDock1.TabIndex = 0;
            this.radDock1.TabStop = false;
            this.radDock1.Text = "radDock1";
            // 
            // toolWindow1
            // 
            this.toolWindow1.Caption = null;
            this.toolWindow1.Controls.Add(this.radListBox1);
            this.toolWindow1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow1.Location = new System.Drawing.Point(1, 24);
            this.toolWindow1.Name = "toolWindow1";
            this.toolWindow1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow1.Size = new System.Drawing.Size(217, 515);
            this.toolWindow1.Text = "Toolbox";
            // 
            // radListBox1
            // 
            this.radListBox1.BackColor = System.Drawing.Color.Transparent;
            this.radListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radListBox1.FormatString = "{0}";
            this.radListBox1.ItemHeight = 20;
            radListDataItem1.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadButton;
            radListDataItem1.Text = "RadButton";
            radListDataItem2.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadCalendar;
            radListDataItem2.Text = "RadCalendar";
            radListDataItem3.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadLabel;
            radListDataItem3.Text = "RadLabel";
            radListDataItem4.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadCarousel;
            radListDataItem4.Text = "RadCarousel";
            radListDataItem5.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadRadioButton;
            radListDataItem5.Text = "RadRadioButton";
            radListDataItem6.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadCheckBox;
            radListDataItem6.Text = "RadCheckBox";
            radListDataItem7.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadPanel;
            radListDataItem7.Text = "RadPanel";
            radListDataItem8.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadProgressBar;
            radListDataItem8.Text = "RadProgressBar";
            radListDataItem9.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadTextBox;
            radListDataItem9.Text = "RadTextBox";
            radListDataItem10.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadPanelBar;
            radListDataItem10.Text = "RadPanelBar";
            radListDataItem11.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadTabStrip;
            radListDataItem11.Text = "RadTabStrip";
            radListDataItem12.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadMenu;
            radListDataItem12.Text = "RadMenu";
            radListDataItem13.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadContextMenuManager;
            radListDataItem13.Text = "RadContextMenu";
            radListDataItem14.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadSplitContainer;
            radListDataItem14.Text = "RadSplitContainer";
            radListDataItem15.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadTrackBar;
            radListDataItem15.Text = "RadTrackBar";
            radListDataItem16.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadRibbonBar;
            radListDataItem16.Text = "RadRibbonBar";
            radListDataItem17.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadRepeatButton;
            radListDataItem17.Text = "RadRepeatButton";
            radListDataItem18.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadGroupBox;
            radListDataItem18.Text = "RadGroupBox";
            radListDataItem19.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadSplitButton;
            radListDataItem19.Text = "RadSplitButton";
            radListDataItem20.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadForm;
            radListDataItem20.Text = "RadForm";
            radListDataItem21.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadMaskedEditBox;
            radListDataItem21.Text = "RadMaskedEditBox";
            radListDataItem22.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadComboBox;
            radListDataItem22.Text = "RadComboxBox";
            radListDataItem23.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadDropDownButton;
            radListDataItem23.Text = "RadDropDownButton";
            radListDataItem24.Image = global::Telerik.Examples.WinControls.Properties.Resources.RadDateTimePicker;
            radListDataItem24.Text = "RadDateTimePicker";
            this.radListBox1.Items.Add(radListDataItem1);
            this.radListBox1.Items.Add(radListDataItem2);
            this.radListBox1.Items.Add(radListDataItem3);
            this.radListBox1.Items.Add(radListDataItem4);
            this.radListBox1.Items.Add(radListDataItem5);
            this.radListBox1.Items.Add(radListDataItem6);
            this.radListBox1.Items.Add(radListDataItem7);
            this.radListBox1.Items.Add(radListDataItem8);
            this.radListBox1.Items.Add(radListDataItem9);
            this.radListBox1.Items.Add(radListDataItem10);
            this.radListBox1.Items.Add(radListDataItem11);
            this.radListBox1.Items.Add(radListDataItem12);
            this.radListBox1.Items.Add(radListDataItem13);
            this.radListBox1.Items.Add(radListDataItem14);
            this.radListBox1.Items.Add(radListDataItem15);
            this.radListBox1.Items.Add(radListDataItem16);
            this.radListBox1.Items.Add(radListDataItem17);
            this.radListBox1.Items.Add(radListDataItem18);
            this.radListBox1.Items.Add(radListDataItem19);
            this.radListBox1.Items.Add(radListDataItem20);
            this.radListBox1.Items.Add(radListDataItem21);
            this.radListBox1.Items.Add(radListDataItem22);
            this.radListBox1.Items.Add(radListDataItem23);
            this.radListBox1.Items.Add(radListDataItem24);
            this.radListBox1.Location = new System.Drawing.Point(0, 0);
            this.radListBox1.Name = "radListBox1";
            this.radListBox1.Padding = new System.Windows.Forms.Padding(2);
            this.radListBox1.Size = new System.Drawing.Size(217, 515);
            this.radListBox1.TabIndex = 0;
            this.radListBox1.Text = "RadPanel";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.CausesValidation = false;
            this.radSplitContainer1.Controls.Add(this.toolTabStrip3);
            this.radSplitContainer1.Controls.Add(this.documentContainer1);
            this.radSplitContainer1.Controls.Add(this.toolTabStrip4);
            this.radSplitContainer1.IsCleanUpTarget = true;
            this.radSplitContainer1.Location = new System.Drawing.Point(5, 5);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(1105, 541);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            // 
            // toolTabStrip3
            // 
            this.toolTabStrip3.CanUpdateChildIndex = true;
            this.toolTabStrip3.CausesValidation = false;
            this.toolTabStrip3.Controls.Add(this.toolWindow1);
            this.toolTabStrip3.Location = new System.Drawing.Point(0, 0);
            this.toolTabStrip3.Name = "toolTabStrip3";
            // 
            // 
            // 
            this.toolTabStrip3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip3.SelectedIndex = 0;
            this.toolTabStrip3.Size = new System.Drawing.Size(219, 541);
            this.toolTabStrip3.SizeInfo.AbsoluteSize = new System.Drawing.Size(219, 200);
            this.toolTabStrip3.SizeInfo.SplitterCorrection = new System.Drawing.Size(19, 0);
            this.toolTabStrip3.TabIndex = 1;
            this.toolTabStrip3.TabStop = false;
            // 
            // documentContainer1
            // 
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentContainer1.SizeInfo.AbsoluteSize = new System.Drawing.Size(608, 200);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-89, 0);
            this.documentContainer1.TabIndex = 2;
            // 
            // toolTabStrip4
            // 
            this.toolTabStrip4.CanUpdateChildIndex = true;
            this.toolTabStrip4.Controls.Add(this.toolWindow2);
            this.toolTabStrip4.Location = new System.Drawing.Point(835, 0);
            this.toolTabStrip4.Name = "toolTabStrip4";
            // 
            // 
            // 
            this.toolTabStrip4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip4.SelectedIndex = 0;
            this.toolTabStrip4.Size = new System.Drawing.Size(270, 541);
            this.toolTabStrip4.SizeInfo.AbsoluteSize = new System.Drawing.Size(270, 200);
            this.toolTabStrip4.SizeInfo.SplitterCorrection = new System.Drawing.Size(70, 0);
            this.toolTabStrip4.TabIndex = 2;
            this.toolTabStrip4.TabStop = false;
            // 
            // toolWindow2
            // 
            this.toolWindow2.Caption = null;
            this.toolWindow2.Controls.Add(this.radTreeView1);
            this.toolWindow2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow2.Location = new System.Drawing.Point(1, 24);
            this.toolWindow2.Name = "toolWindow2";
            this.toolWindow2.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow2.Size = new System.Drawing.Size(268, 515);
            this.toolWindow2.Text = "Solution Explorer - Solution \'ProjectTracker\' (4 project)";
            // 
            // radTreeView1
            // 
            this.radTreeView1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radTreeView1.ImageList = this.ImageList1;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Name = "radTreeView1";
            radTreeNode1.Expanded = true;
            radTreeNode1.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode1.Image")));
            radTreeNode1.ImageIndex = 7;
            radTreeNode2.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode2.Image")));
            radTreeNode2.ImageIndex = 6;
            radTreeNode2.Text = "Common";
            radTreeNode3.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode3.Image")));
            radTreeNode3.ImageIndex = 6;
            radTreeNode3.Text = "DataAccess";
            radTreeNode4.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode4.Image")));
            radTreeNode4.ImageIndex = 6;
            radTreeNode4.Text = "Business";
            radTreeNode5.Expanded = true;
            radTreeNode5.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode5.Image")));
            radTreeNode5.ImageIndex = 6;
            radTreeNode6.Expanded = true;
            radTreeNode6.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode6.Image")));
            radTreeNode6.ImageIndex = 9;
            radTreeNode7.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode7.Image")));
            radTreeNode7.ImageIndex = 0;
            radTreeNode7.Text = "AssemblyInfo.cs";
            radTreeNode6.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode7});
            radTreeNode6.Text = "Properties";
            radTreeNode8.Expanded = true;
            radTreeNode8.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode8.Image")));
            radTreeNode8.ImageIndex = 8;
            radTreeNode9.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode9.Image")));
            radTreeNode9.ImageIndex = 10;
            radTreeNode9.Text = "System";
            radTreeNode10.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode10.Image")));
            radTreeNode10.ImageIndex = 10;
            radTreeNode10.Text = "System.Data";
            radTreeNode11.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode11.Image")));
            radTreeNode11.ImageIndex = 10;
            radTreeNode11.Text = "System.Xml";
            radTreeNode8.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode9,
            radTreeNode10,
            radTreeNode11});
            radTreeNode8.Text = "References";
            radTreeNode12.Expanded = true;
            radTreeNode12.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode12.Image")));
            radTreeNode12.ImageIndex = 4;
            radTreeNode13.Expanded = true;
            radTreeNode13.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode13.Image")));
            radTreeNode13.ImageIndex = 3;
            radTreeNode14.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode14.Image")));
            radTreeNode14.ImageIndex = 15;
            radTreeNode14.Text = "Form1.Designer.cs";
            radTreeNode15.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode15.Image")));
            radTreeNode15.ImageIndex = 15;
            radTreeNode15.Text = "Form1.resx";
            radTreeNode13.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode14,
            radTreeNode15});
            radTreeNode13.Text = "Form1.cs";
            radTreeNode12.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode13});
            radTreeNode12.Text = "Forms";
            radTreeNode16.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode16.Image")));
            radTreeNode16.ImageIndex = 0;
            radTreeNode16.Text = "Program.cs";
            radTreeNode5.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode6,
            radTreeNode8,
            radTreeNode12,
            radTreeNode16});
            radTreeNode5.Text = "UI";
            radTreeNode1.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode2,
            radTreeNode3,
            radTreeNode4,
            radTreeNode5});
            radTreeNode1.Text = "Solution \'ProjectTracker\' (4 projects)";
            this.radTreeView1.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode1});
            this.radTreeView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView1.Size = new System.Drawing.Size(268, 515);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 0;
            this.radTreeView1.Text = "radTreeView1";
            this.radTreeView1.ToggleMode = Telerik.WinControls.UI.ToggleMode.None;
            this.radTreeView1.SelectedNodeChanged += new Telerik.WinControls.UI.RadTreeView.RadTreeViewEventHandler(this.radTreeView1_SelectedNodeChanged);
            this.radTreeView1.NodeMouseDoubleClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.radTreeView1_NodeMouseDoubleClick);
            this.radTreeView1.NodeExpandedChanged += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.radTreeView1_NodeExpandedChanged);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "vss_csfile.ico");
            this.ImageList1.Images.SetKeyName(1, "vss_error.bmp");
            this.ImageList1.Images.SetKeyName(2, "vss_folder.png");
            this.ImageList1.Images.SetKeyName(3, "vss_form.ico");
            this.ImageList1.Images.SetKeyName(4, "vss_open_folder.png");
            this.ImageList1.Images.SetKeyName(5, "vss_output.bmp");
            this.ImageList1.Images.SetKeyName(6, "vss_project.ico");
            this.ImageList1.Images.SetKeyName(7, "vss_solution.ico");
            this.ImageList1.Images.SetKeyName(8, "vss_referencesfolder.png");
            this.ImageList1.Images.SetKeyName(9, "vss_propertiesfolder.png");
            this.ImageList1.Images.SetKeyName(10, "vss_refassembly.ico");
            this.ImageList1.Images.SetKeyName(11, "Control_ErrorProvider.png");
            this.ImageList1.Images.SetKeyName(12, "vss_Warning.png");
            this.ImageList1.Images.SetKeyName(13, "vss_Information.png");
            this.ImageList1.Images.SetKeyName(14, "vss_Task.png");
            this.ImageList1.Images.SetKeyName(15, "vss_formbehind.png");
            // 
            // toolTabStrip5
            // 
            this.toolTabStrip5.CanUpdateChildIndex = true;
            this.toolTabStrip5.Controls.Add(this.toolWindow3);
            this.toolTabStrip5.Controls.Add(this.toolWindow4);
            this.toolTabStrip5.Controls.Add(this.toolWindow5);
            this.toolTabStrip5.Location = new System.Drawing.Point(5, 550);
            this.toolTabStrip5.Name = "toolTabStrip5";
            // 
            // 
            // 
            this.toolTabStrip5.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip5.SelectedIndex = 0;
            this.toolTabStrip5.Size = new System.Drawing.Size(1105, 200);
            this.toolTabStrip5.TabIndex = 3;
            this.toolTabStrip5.TabStop = false;
            // 
            // toolWindow3
            // 
            this.toolWindow3.Caption = null;
            this.toolWindow3.Controls.Add(this.radPanel3);
            this.toolWindow3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow3.Location = new System.Drawing.Point(1, 24);
            this.toolWindow3.Name = "toolWindow3";
            this.toolWindow3.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow3.Size = new System.Drawing.Size(1103, 150);
            this.toolWindow3.Text = "Output";
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.richTextBoxOutput);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(0, 0);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.radPanel3.Size = new System.Drawing.Size(1103, 150);
            this.radPanel3.TabIndex = 1;
            this.radPanel3.Text = "radPanel3";
            // 
            // richTextBoxOutput
            // 
            this.richTextBoxOutput.AcceptsReturn = true;
            this.richTextBoxOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(231)))), ((int)(((byte)(232)))));
            this.richTextBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.richTextBoxOutput.IsReadOnly = true;
            this.richTextBoxOutput.Location = new System.Drawing.Point(1, 1);
            this.richTextBoxOutput.Multiline = true;
            this.richTextBoxOutput.Name = "richTextBoxOutput";
            this.richTextBoxOutput.Size = new System.Drawing.Size(1101, 148);
            this.richTextBoxOutput.TabIndex = 0;
            // 
            // toolWindow4
            // 
            this.toolWindow4.Caption = null;
            this.toolWindow4.Controls.Add(this.radGridViewTasks);
            this.toolWindow4.Controls.Add(this.radPanel2);
            this.toolWindow4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow4.Location = new System.Drawing.Point(1, 17);
            this.toolWindow4.Name = "toolWindow4";
            this.toolWindow4.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow4.Size = new System.Drawing.Size(790, 162);
            this.toolWindow4.Text = "Task List";
            // 
            // radGridViewTasks
            // 
            this.radGridViewTasks.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(254)))));
            this.radGridViewTasks.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radGridViewTasks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridViewTasks.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridViewTasks.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridViewTasks.Location = new System.Drawing.Point(0, 28);
            // 
            // 
            // 
            this.radGridViewTasks.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewTasks.MasterTemplate.AllowColumnReorder = false;
            gridViewTextBoxColumn1.AllowGroup = false;
            gridViewTextBoxColumn1.FieldName = "firstColumn";
            gridViewTextBoxColumn1.HeaderText = "!";
            gridViewTextBoxColumn1.MaxWidth = 20;
            gridViewTextBoxColumn1.MinWidth = 20;
            gridViewTextBoxColumn1.Name = "firstColumn";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 20;
            gridViewTextBoxColumn2.AllowGroup = false;
            gridViewTextBoxColumn2.FieldName = "Description";
            gridViewTextBoxColumn2.HeaderText = "Description";
            gridViewTextBoxColumn2.Name = "Description";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 239;
            gridViewTextBoxColumn3.AllowGroup = false;
            gridViewTextBoxColumn3.FieldName = "File";
            gridViewTextBoxColumn3.HeaderText = "File";
            gridViewTextBoxColumn3.MaxWidth = 100;
            gridViewTextBoxColumn3.MinWidth = 100;
            gridViewTextBoxColumn3.Name = "File";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.AllowGroup = false;
            gridViewTextBoxColumn4.FieldName = "Line";
            gridViewTextBoxColumn4.HeaderText = "Line";
            gridViewTextBoxColumn4.MaxWidth = 80;
            gridViewTextBoxColumn4.MinWidth = 80;
            gridViewTextBoxColumn4.Name = "Line";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 80;
            this.radGridViewTasks.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            sortDescriptor1.PropertyName = "File";
            this.radGridViewTasks.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.radGridViewTasks.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridViewTasks.Name = "radGridViewTasks";
            this.radGridViewTasks.ReadOnly = true;
            this.radGridViewTasks.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridViewTasks.ShowGroupPanel = false;
            this.radGridViewTasks.Size = new System.Drawing.Size(790, 134);
            this.radGridViewTasks.TabIndex = 1;
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radComboBox1);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel2.Location = new System.Drawing.Point(0, 0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(790, 28);
            this.radPanel2.TabIndex = 0;
            // 
            // radComboBox1
            // 
            this.radComboBox1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radComboBox1.FormatString = "{0}";
            radListDataItem25.Text = "User Tasks";
            radListDataItem26.Text = "Comments";
            this.radComboBox1.Items.Add(radListDataItem25);
            this.radComboBox1.Items.Add(radListDataItem26);
            this.radComboBox1.Location = new System.Drawing.Point(3, 3);
            this.radComboBox1.Name = "radComboBox1";
            // 
            // 
            // 
            this.radComboBox1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboBox1.Size = new System.Drawing.Size(162, 20);
            this.radComboBox1.TabIndex = 0;
            this.radComboBox1.Text = "Comments";
            // 
            // toolWindow5
            // 
            this.toolWindow5.Caption = null;
            this.toolWindow5.Controls.Add(this.radGridViewErrors);
            this.toolWindow5.Controls.Add(this.radPanel1);
            this.toolWindow5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow5.Location = new System.Drawing.Point(1, 17);
            this.toolWindow5.Name = "toolWindow5";
            this.toolWindow5.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow5.Size = new System.Drawing.Size(790, 162);
            this.toolWindow5.Text = "Error List";
            // 
            // radGridViewErrors
            // 
            this.radGridViewErrors.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(254)))));
            this.radGridViewErrors.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radGridViewErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridViewErrors.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridViewErrors.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridViewErrors.Location = new System.Drawing.Point(0, 31);
            // 
            // 
            // 
            this.radGridViewErrors.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewErrors.MasterTemplate.AllowColumnReorder = false;
            gridViewImageColumn1.FieldName = "columnImage";
            gridViewImageColumn1.HeaderText = "columnImage";
            gridViewImageColumn1.MaxWidth = 20;
            gridViewImageColumn1.MinWidth = 20;
            gridViewImageColumn1.Name = "columnImage";
            gridViewImageColumn1.Width = 20;
            gridViewTextBoxColumn5.AllowGroup = false;
            gridViewTextBoxColumn5.FieldName = "columnNumber";
            gridViewTextBoxColumn5.HeaderText = "columnNumber";
            gridViewTextBoxColumn5.MaxWidth = 20;
            gridViewTextBoxColumn5.MinWidth = 20;
            gridViewTextBoxColumn5.Name = "columnNumber";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 20;
            gridViewTextBoxColumn6.AllowGroup = false;
            gridViewTextBoxColumn6.FieldName = "Description";
            gridViewTextBoxColumn6.HeaderText = "Description";
            gridViewTextBoxColumn6.Name = "Description";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 200;
            gridViewTextBoxColumn7.AllowGroup = false;
            gridViewTextBoxColumn7.FieldName = "File";
            gridViewTextBoxColumn7.HeaderText = "File";
            gridViewTextBoxColumn7.MaxWidth = 80;
            gridViewTextBoxColumn7.MinWidth = 80;
            gridViewTextBoxColumn7.Name = "File";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn7.Width = 80;
            gridViewTextBoxColumn8.AllowGroup = false;
            gridViewTextBoxColumn8.FieldName = "Line";
            gridViewTextBoxColumn8.HeaderText = "Line";
            gridViewTextBoxColumn8.MaxWidth = 50;
            gridViewTextBoxColumn8.MinWidth = 50;
            gridViewTextBoxColumn8.Name = "Line";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn9.AllowGroup = false;
            gridViewTextBoxColumn9.FieldName = "Column";
            gridViewTextBoxColumn9.HeaderText = "Column";
            gridViewTextBoxColumn9.MaxWidth = 96;
            gridViewTextBoxColumn9.MinWidth = 96;
            gridViewTextBoxColumn9.Name = "Column";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 96;
            gridViewTextBoxColumn10.AllowGroup = false;
            gridViewTextBoxColumn10.FieldName = "Project";
            gridViewTextBoxColumn10.HeaderText = "Project";
            gridViewTextBoxColumn10.MaxWidth = 104;
            gridViewTextBoxColumn10.MinWidth = 104;
            gridViewTextBoxColumn10.Name = "Project";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.Width = 104;
            this.radGridViewErrors.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewImageColumn1,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            sortDescriptor2.PropertyName = "File";
            this.radGridViewErrors.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.radGridViewErrors.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridViewErrors.Name = "radGridViewErrors";
            this.radGridViewErrors.ReadOnly = true;
            this.radGridViewErrors.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridViewErrors.ShowGroupPanel = false;
            this.radGridViewErrors.Size = new System.Drawing.Size(790, 131);
            this.radGridViewErrors.TabIndex = 1;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radButton3);
            this.radPanel1.Controls.Add(this.radButton2);
            this.radPanel1.Controls.Add(this.radButton1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(790, 31);
            this.radPanel1.TabIndex = 0;
            // 
            // radButton3
            // 
            this.radButton3.ImageIndex = 13;
            this.radButton3.ImageKey = "vss_Information.png";
            this.radButton3.ImageList = this.ImageList1;
            this.radButton3.Location = new System.Drawing.Point(165, 3);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(83, 23);
            this.radButton3.TabIndex = 2;
            this.radButton3.Text = "0 Messages";
            this.radButton3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButton2
            // 
            this.radButton2.ImageIndex = 12;
            this.radButton2.ImageList = this.ImageList1;
            this.radButton2.Location = new System.Drawing.Point(78, 3);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(81, 23);
            this.radButton2.TabIndex = 1;
            this.radButton2.Text = "0 Warnings";
            this.radButton2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButton1
            // 
            this.radButton1.ImageIndex = 11;
            this.radButton1.ImageList = this.ImageList1;
            this.radButton1.Location = new System.Drawing.Point(11, 3);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(61, 23);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "1 Error";
            this.radButton1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 755);
            this.Controls.Add(this.radDock1);
            this.Name = "Form1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Telerik Studio";
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).EndInit();
            this.radDock1.ResumeLayout(false);
            this.toolWindow1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radListBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).EndInit();
            this.toolTabStrip3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip4)).EndInit();
            this.toolTabStrip4.ResumeLayout(false);
            this.toolWindow2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip5)).EndInit();
            this.toolTabStrip5.ResumeLayout(false);
            this.toolWindow3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.richTextBoxOutput)).EndInit();
            this.toolWindow4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewTasks.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radComboBox1)).EndInit();
            this.toolWindow5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewErrors.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewErrors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.Docking.RadDock radDock1;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow1;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow2;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow5;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip3;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip4;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip5;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow3;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow4;
        private Telerik.WinControls.UI.RadListControl radListBox1;
        private System.Windows.Forms.ImageList ImageList1;
        private Telerik.WinControls.UI.RadTextBoxControl richTextBoxOutput;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGridView radGridViewErrors;
        private Telerik.WinControls.UI.RadGridView radGridViewTasks;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadDropDownList radComboBox1;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        private Telerik.WinControls.UI.RadPanel radPanel3;
    }
}

