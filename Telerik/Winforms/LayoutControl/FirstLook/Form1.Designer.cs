﻿namespace Telerik.Examples.WinControls.LayoutControl.FirstLook
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radLayoutControl1 = new Telerik.WinControls.UI.RadLayoutControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.employeeOrdersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.northwindDataSet = new Telerik.Examples.WinControls.DataSources.NorthwindDataSet();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox5 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox6 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox7 = new Telerik.WinControls.UI.RadTextBox();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.layoutControlItem2 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlTabbedGroup1 = new Telerik.WinControls.UI.LayoutControlTabbedGroup();
            this.layoutControlGroupItem1 = new Telerik.WinControls.UI.LayoutControlGroupItem();
            this.layoutControlItem5 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlGroupItem2 = new Telerik.WinControls.UI.LayoutControlGroupItem();
            this.layoutControlItem11 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem12 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem13 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem3 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem4 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem10 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem18 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem1 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlLabelItem1 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.layoutControlSeparatorItem1 = new Telerik.WinControls.UI.LayoutControlSeparatorItem();
            this.layoutControlGroupItem3 = new Telerik.WinControls.UI.LayoutControlGroupItem();
            this.layoutControlItem6 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlSplitterItem1 = new Telerik.WinControls.UI.LayoutControlSplitterItem();
            this.layoutControlSplitterItem2 = new Telerik.WinControls.UI.LayoutControlSplitterItem();
            this.employeeOrdersTableAdapter = new Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.EmployeeOrdersTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.radLayoutControl1)).BeginInit();
            this.radLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeOrdersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLayoutControl1
            // 
            this.radLayoutControl1.BackColor = System.Drawing.Color.Transparent;
            this.radLayoutControl1.Controls.Add(this.pictureBox1);
            this.radLayoutControl1.Controls.Add(this.radTextBox1);
            this.radLayoutControl1.Controls.Add(this.radTextBox2);
            this.radLayoutControl1.Controls.Add(this.radTextBox4);
            this.radLayoutControl1.Controls.Add(this.radDateTimePicker1);
            this.radLayoutControl1.Controls.Add(this.radGridView1);
            this.radLayoutControl1.Controls.Add(this.radTextBox3);
            this.radLayoutControl1.Controls.Add(this.radTextBox5);
            this.radLayoutControl1.Controls.Add(this.radTextBox6);
            this.radLayoutControl1.Controls.Add(this.radTextBox7);
            this.radLayoutControl1.Controls.Add(this.radDateTimePicker2);
            this.radLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLayoutControl1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.layoutControlItem2,
            this.layoutControlTabbedGroup1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem10,
            this.layoutControlItem18,
            this.layoutControlItem1,
            this.layoutControlLabelItem1,
            this.layoutControlSeparatorItem1,
            this.layoutControlGroupItem3,
            this.layoutControlSplitterItem1,
            this.layoutControlSplitterItem2});
            this.radLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.radLayoutControl1.Name = "radLayoutControl1";
            this.radLayoutControl1.Size = new System.Drawing.Size(598, 496);
            this.radLayoutControl1.TabIndex = 0;
            this.radLayoutControl1.Text = "radLayoutControl1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Telerik.Examples.WinControls.Properties.Resources.Janet;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(422, 3);
            this.pictureBox1.MaximumSize = new System.Drawing.Size(0, 202);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(173, 202);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // radTextBox1
            // 
            this.radTextBox1.Location = new System.Drawing.Point(3, 23);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(202, 20);
            this.radTextBox1.TabIndex = 4;
            this.radTextBox1.Text = "Janet";
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(3, 127);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.Size = new System.Drawing.Size(202, 20);
            this.radTextBox2.TabIndex = 5;
            this.radTextBox2.Text = "Sales Representative";
            // 
            // radTextBox4
            // 
            this.radTextBox4.AutoSize = false;
            this.radTextBox4.Location = new System.Drawing.Point(7, 181);
            this.radTextBox4.Multiline = true;
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.Size = new System.Drawing.Size(401, 86);
            this.radTextBox4.TabIndex = 7;
            this.radTextBox4.Text = resources.GetString("radTextBox4.Text");
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.Location = new System.Drawing.Point(211, 127);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.Size = new System.Drawing.Size(201, 20);
            this.radDateTimePicker1.TabIndex = 13;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "Wednesday, April 01, 1992";
            this.radDateTimePicker1.Value = new System.DateTime(1992, 4, 1, 0, 0, 0, 0);
            // 
            // radGridView1
            // 
            this.radGridView1.Location = new System.Drawing.Point(13, 316);
            // 
            // 
            // 
            this.radGridView1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "LastName";
            gridViewTextBoxColumn1.HeaderText = "LastName";
            gridViewTextBoxColumn1.IsAutoGenerated = true;
            gridViewTextBoxColumn1.Name = "LastName";
            gridViewTextBoxColumn1.Width = 70;
            gridViewTextBoxColumn2.FieldName = "FirstName";
            gridViewTextBoxColumn2.HeaderText = "FirstName";
            gridViewTextBoxColumn2.IsAutoGenerated = true;
            gridViewTextBoxColumn2.Name = "FirstName";
            gridViewTextBoxColumn2.Width = 70;
            gridViewTextBoxColumn3.FieldName = "Title";
            gridViewTextBoxColumn3.HeaderText = "Title";
            gridViewTextBoxColumn3.IsAutoGenerated = true;
            gridViewTextBoxColumn3.Name = "Title";
            gridViewTextBoxColumn3.Width = 70;
            gridViewTextBoxColumn4.FieldName = "City";
            gridViewTextBoxColumn4.HeaderText = "City";
            gridViewTextBoxColumn4.IsAutoGenerated = true;
            gridViewTextBoxColumn4.Name = "City";
            gridViewTextBoxColumn4.Width = 70;
            gridViewTextBoxColumn5.FieldName = "Country";
            gridViewTextBoxColumn5.HeaderText = "Country";
            gridViewTextBoxColumn5.IsAutoGenerated = true;
            gridViewTextBoxColumn5.Name = "Country";
            gridViewTextBoxColumn5.Width = 70;
            gridViewTextBoxColumn6.FieldName = "ShipName";
            gridViewTextBoxColumn6.HeaderText = "ShipName";
            gridViewTextBoxColumn6.IsAutoGenerated = true;
            gridViewTextBoxColumn6.Name = "ShipName";
            gridViewTextBoxColumn6.Width = 70;
            gridViewDecimalColumn1.FieldName = "UnitPrice";
            gridViewDecimalColumn1.HeaderText = "UnitPrice";
            gridViewDecimalColumn1.IsAutoGenerated = true;
            gridViewDecimalColumn1.Name = "UnitPrice";
            gridViewDecimalColumn1.Width = 70;
            gridViewDecimalColumn2.DataType = typeof(short);
            gridViewDecimalColumn2.FieldName = "Quantity";
            gridViewDecimalColumn2.HeaderText = "Quantity";
            gridViewDecimalColumn2.IsAutoGenerated = true;
            gridViewDecimalColumn2.Name = "Quantity";
            gridViewDecimalColumn2.Width = 68;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewDecimalColumn1,
            gridViewDecimalColumn2});
            this.radGridView1.MasterTemplate.DataSource = this.employeeOrdersBindingSource;
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Size = new System.Drawing.Size(571, 161);
            this.radGridView1.TabIndex = 14;
            this.radGridView1.Text = "radGridView1";
            // 
            // employeeOrdersBindingSource
            // 
            this.employeeOrdersBindingSource.DataMember = "EmployeeOrders";
            this.employeeOrdersBindingSource.DataSource = this.northwindDataSet;
            // 
            // northwindDataSet
            // 
            this.northwindDataSet.DataSetName = "NorthwindDataSet";
            this.northwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radTextBox3
            // 
            this.radTextBox3.AutoSize = false;
            this.radTextBox3.Location = new System.Drawing.Point(13, 384);
            this.radTextBox3.Multiline = true;
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(571, 97);
            this.radTextBox3.TabIndex = 15;
            this.radTextBox3.Text = "507 - 20th Ave. E.";
            // 
            // radTextBox5
            // 
            this.radTextBox5.Location = new System.Drawing.Point(510, 338);
            this.radTextBox5.Name = "radTextBox5";
            this.radTextBox5.Size = new System.Drawing.Size(74, 20);
            this.radTextBox5.TabIndex = 16;
            this.radTextBox5.Text = "Seattle";
            // 
            // radTextBox6
            // 
            this.radTextBox6.Location = new System.Drawing.Point(13, 338);
            this.radTextBox6.Name = "radTextBox6";
            this.radTextBox6.Size = new System.Drawing.Size(491, 20);
            this.radTextBox6.TabIndex = 17;
            this.radTextBox6.Text = "USA";
            // 
            // radTextBox7
            // 
            this.radTextBox7.Location = new System.Drawing.Point(211, 23);
            this.radTextBox7.Name = "radTextBox7";
            this.radTextBox7.Size = new System.Drawing.Size(201, 20);
            this.radTextBox7.TabIndex = 23;
            this.radTextBox7.Text = "Leverling";
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.Location = new System.Drawing.Point(3, 73);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            this.radDateTimePicker2.Size = new System.Drawing.Size(202, 20);
            this.radDateTimePicker2.TabIndex = 25;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.Text = "Saturday, August 17, 1963";
            this.radDateTimePicker2.Value = new System.DateTime(1963, 8, 17, 0, 0, 0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AssociatedControl = this.pictureBox1;
            this.layoutControlItem2.Bounds = new System.Drawing.Rectangle(419, 0, 179, 274);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(270, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(120, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            // 
            // layoutControlTabbedGroup1
            // 
            this.layoutControlTabbedGroup1.Bounds = new System.Drawing.Rectangle(0, 278, 598, 218);
            this.layoutControlTabbedGroup1.ItemGroups.AddRange(new Telerik.WinControls.RadItem[] {
            this.layoutControlGroupItem1,
            this.layoutControlGroupItem2});
            this.layoutControlTabbedGroup1.Name = "layoutControlTabbedGroup1";
            this.layoutControlTabbedGroup1.SelectedGroup = this.layoutControlGroupItem2;
            // 
            // layoutControlGroupItem1
            // 
            this.layoutControlGroupItem1.AccessibleDescription = "layoutControlGroupItem1";
            this.layoutControlGroupItem1.AccessibleName = "layoutControlGroupItem1";
            this.layoutControlGroupItem1.Bounds = new System.Drawing.Rectangle(0, 0, 587, 178);
            this.layoutControlGroupItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.layoutControlItem5});
            this.layoutControlGroupItem1.Name = "layoutControlGroupItem1";
            this.layoutControlGroupItem1.Text = "Sales";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AssociatedControl = this.radGridView1;
            this.layoutControlItem5.Bounds = new System.Drawing.Rectangle(0, 0, 577, 167);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(46, 120);
            this.layoutControlItem5.Name = "layoutControlItem5";
            // 
            // layoutControlGroupItem2
            // 
            this.layoutControlGroupItem2.AccessibleDescription = "layoutControlGroupItem2";
            this.layoutControlGroupItem2.AccessibleName = "layoutControlGroupItem2";
            this.layoutControlGroupItem2.Bounds = new System.Drawing.Rectangle(0, 0, 577, 170);
            this.layoutControlGroupItem2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13});
            this.layoutControlGroupItem2.Name = "layoutControlGroupItem2";
            this.layoutControlGroupItem2.Text = "Address";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AccessibleDescription = "Street Address";
            this.layoutControlItem11.AccessibleName = "Street Address";
            this.layoutControlItem11.AssociatedControl = this.radTextBox3;
            this.layoutControlItem11.Bounds = new System.Drawing.Rectangle(0, 46, 577, 123);
            this.layoutControlItem11.DrawText = true;
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Text = "Street Address";
            this.layoutControlItem11.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem11.TextFixedSize = 20;
            this.layoutControlItem11.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem11.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AccessibleDescription = "City";
            this.layoutControlItem12.AccessibleName = "City";
            this.layoutControlItem12.AssociatedControl = this.radTextBox5;
            this.layoutControlItem12.Bounds = new System.Drawing.Rectangle(497, 0, 80, 46);
            this.layoutControlItem12.DrawText = true;
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 46);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(46, 46);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Text = "City";
            this.layoutControlItem12.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem12.TextFixedSize = 20;
            this.layoutControlItem12.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem12.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AccessibleDescription = "Country";
            this.layoutControlItem13.AccessibleName = "Country";
            this.layoutControlItem13.AssociatedControl = this.radTextBox6;
            this.layoutControlItem13.Bounds = new System.Drawing.Rectangle(0, 0, 497, 46);
            this.layoutControlItem13.DrawText = true;
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 46);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(46, 46);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Text = "Country";
            this.layoutControlItem13.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem13.TextFixedSize = 20;
            this.layoutControlItem13.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem13.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AccessibleDescription = "First Name";
            this.layoutControlItem3.AccessibleName = "First Name";
            this.layoutControlItem3.AssociatedControl = this.radTextBox1;
            this.layoutControlItem3.Bounds = new System.Drawing.Rectangle(0, 0, 208, 50);
            this.layoutControlItem3.DrawText = true;
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(100, 50);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Text = "First Name";
            this.layoutControlItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem3.TextFixedSize = 20;
            this.layoutControlItem3.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem3.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AccessibleDescription = "Job Title";
            this.layoutControlItem4.AccessibleName = "Job Title";
            this.layoutControlItem4.AssociatedControl = this.radTextBox2;
            this.layoutControlItem4.Bounds = new System.Drawing.Rectangle(0, 104, 208, 50);
            this.layoutControlItem4.DrawText = true;
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(100, 50);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Text = "Job Title";
            this.layoutControlItem4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem4.TextFixedSize = 20;
            this.layoutControlItem4.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem4.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AccessibleDescription = "Hire Date";
            this.layoutControlItem10.AccessibleName = "Hire Date";
            this.layoutControlItem10.AssociatedControl = this.radDateTimePicker1;
            this.layoutControlItem10.Bounds = new System.Drawing.Rectangle(208, 104, 207, 50);
            this.layoutControlItem10.DrawText = true;
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(100, 50);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Text = "Hire Date";
            this.layoutControlItem10.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem10.TextFixedSize = 20;
            this.layoutControlItem10.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem10.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AccessibleDescription = "Last Name";
            this.layoutControlItem18.AccessibleName = "Last Name";
            this.layoutControlItem18.AssociatedControl = this.radTextBox7;
            this.layoutControlItem18.Bounds = new System.Drawing.Rectangle(208, 0, 207, 50);
            this.layoutControlItem18.DrawText = true;
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(100, 50);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Text = "Last Name";
            this.layoutControlItem18.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem18.TextFixedSize = 20;
            this.layoutControlItem18.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem18.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AccessibleDescription = "Birth date";
            this.layoutControlItem1.AccessibleName = "Birth date";
            this.layoutControlItem1.AssociatedControl = this.radDateTimePicker2;
            this.layoutControlItem1.Bounds = new System.Drawing.Rectangle(0, 50, 208, 50);
            this.layoutControlItem1.DrawText = true;
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(100, 50);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Text = "Birth date";
            this.layoutControlItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem1.TextFixedSize = 20;
            this.layoutControlItem1.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem1.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlLabelItem1
            // 
            this.layoutControlLabelItem1.Bounds = new System.Drawing.Rectangle(208, 50, 207, 50);
            this.layoutControlLabelItem1.DrawText = false;
            this.layoutControlLabelItem1.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlLabelItem1.MinSize = new System.Drawing.Size(46, 50);
            this.layoutControlLabelItem1.Name = "layoutControlLabelItem1";
            // 
            // layoutControlSeparatorItem1
            // 
            this.layoutControlSeparatorItem1.Bounds = new System.Drawing.Rectangle(0, 100, 415, 4);
            this.layoutControlSeparatorItem1.Name = "layoutControlSeparatorItem1";
            // 
            // layoutControlGroupItem3
            // 
            this.layoutControlGroupItem3.AccessibleDescription = "Description";
            this.layoutControlGroupItem3.AccessibleName = "Description";
            this.layoutControlGroupItem3.Bounds = new System.Drawing.Rectangle(0, 154, 415, 120);
            this.layoutControlGroupItem3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.layoutControlItem6});
            this.layoutControlGroupItem3.Name = "layoutControlGroupItem3";
            this.layoutControlGroupItem3.Text = "Description";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AccessibleDescription = "Details";
            this.layoutControlItem6.AccessibleName = "Details";
            this.layoutControlItem6.AssociatedControl = this.radTextBox4;
            this.layoutControlItem6.Bounds = new System.Drawing.Rectangle(0, 0, 407, 92);
            this.layoutControlItem6.DrawText = false;
            this.layoutControlItem6.MinSize = new System.Drawing.Size(46, 60);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Text = "Details";
            this.layoutControlItem6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem6.TextFixedSize = 22;
            this.layoutControlItem6.TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            this.layoutControlItem6.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem6.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlSplitterItem1
            // 
            this.layoutControlSplitterItem1.Bounds = new System.Drawing.Rectangle(0, 274, 598, 4);
            this.layoutControlSplitterItem1.Name = "layoutControlSplitterItem1";
            // 
            // layoutControlSplitterItem2
            // 
            this.layoutControlSplitterItem2.Bounds = new System.Drawing.Rectangle(415, 0, 4, 274);
            this.layoutControlSplitterItem2.Name = "layoutControlSplitterItem2";
            // 
            // employeeOrdersTableAdapter
            // 
            this.employeeOrdersTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 496);
            this.Controls.Add(this.radLayoutControl1);
            this.Name = "Form1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLayoutControl1)).EndInit();
            this.radLayoutControl1.ResumeLayout(false);
            this.radLayoutControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeOrdersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLayoutControl radLayoutControl1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem2;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
        private Telerik.WinControls.UI.RadTextBox radTextBox4;
        private Telerik.WinControls.UI.LayoutControlTabbedGroup layoutControlTabbedGroup1;
        private Telerik.WinControls.UI.LayoutControlGroupItem layoutControlGroupItem1;
        private Telerik.WinControls.UI.LayoutControlGroupItem layoutControlGroupItem2;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem3;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem4;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem6;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem5;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem11;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem10;
        private Telerik.WinControls.UI.RadTextBox radTextBox5;
        private Telerik.WinControls.UI.RadTextBox radTextBox6;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem12;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem13;
        private Telerik.WinControls.UI.RadTextBox radTextBox7;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem18;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem1;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem1;
        private Telerik.WinControls.UI.LayoutControlSeparatorItem layoutControlSeparatorItem1;
        private Telerik.WinControls.UI.LayoutControlGroupItem layoutControlGroupItem3;
        private Telerik.WinControls.UI.LayoutControlSplitterItem layoutControlSplitterItem1;
        private Telerik.WinControls.UI.LayoutControlSplitterItem layoutControlSplitterItem2;
        private DataSources.NorthwindDataSet northwindDataSet;
        private System.Windows.Forms.BindingSource employeeOrdersBindingSource;
        private DataSources.NorthwindDataSetTableAdapters.EmployeeOrdersTableAdapter employeeOrdersTableAdapter;

    }
}