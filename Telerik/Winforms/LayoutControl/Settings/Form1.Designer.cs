﻿namespace Telerik.Examples.WinControls.LayoutControl.Settings
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.radLayoutControl1 = new Telerik.WinControls.UI.RadLayoutControl();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBox5 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBox6 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBox7 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.layoutControlItem3 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem6 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem9 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlLabelItem10 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.layoutControlLabelItem11 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.layoutControlLabelItem12 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.layoutControlLabelItem13 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.layoutControlLabelItem1 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.layoutControlItem4 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem14 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem5 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlLabelItem15 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.layoutControlItem17 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem18 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem19 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlLabelItem2 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.layoutControlSeparatorItem1 = new Telerik.WinControls.UI.LayoutControlSeparatorItem();
            this.layoutControlLabelItem20 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.radButtonAddMember = new Telerik.WinControls.UI.RadButton();
            this.radButtonEditLayout = new Telerik.WinControls.UI.RadButton();
            this.radButtonSaveLayout = new Telerik.WinControls.UI.RadButton();
            this.radButtonLoadLayout = new Telerik.WinControls.UI.RadButton();
            this.layoutControlSeparatorItem2 = new Telerik.WinControls.UI.LayoutControlSeparatorItem();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLayoutControl1)).BeginInit();
            this.radLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonAddMember)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonEditLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSaveLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonLoadLayout)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radButtonAddMember);
            this.settingsPanel.Controls.Add(this.radButtonLoadLayout);
            this.settingsPanel.Controls.Add(this.radButtonEditLayout);
            this.settingsPanel.Controls.Add(this.radButtonSaveLayout);
            this.settingsPanel.Location = new System.Drawing.Point(857, 3);
            this.settingsPanel.Controls.SetChildIndex(this.radButtonSaveLayout, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radButtonEditLayout, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radButtonLoadLayout, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radButtonAddMember, 0);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(857, 209);
            // 
            // radLayoutControl1
            // 
            this.radLayoutControl1.Controls.Add(this.radTextBox1);
            this.radLayoutControl1.Controls.Add(this.radTextBox3);
            this.radLayoutControl1.Controls.Add(this.radTextBox4);
            this.radLayoutControl1.Controls.Add(this.radButton1);
            this.radLayoutControl1.Controls.Add(this.radButton2);
            this.radLayoutControl1.Controls.Add(this.radTextBox2);
            this.radLayoutControl1.Controls.Add(this.radTextBox5);
            this.radLayoutControl1.Controls.Add(this.radTextBox6);
            this.radLayoutControl1.Controls.Add(this.radTextBox7);
            this.radLayoutControl1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem9,
            this.layoutControlLabelItem10,
            this.layoutControlLabelItem11,
            this.layoutControlLabelItem12,
            this.layoutControlLabelItem13,
            this.layoutControlLabelItem1,
            this.layoutControlItem4,
            this.layoutControlItem14,
            this.layoutControlItem5,
            this.layoutControlLabelItem15,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlLabelItem2,
            this.layoutControlSeparatorItem1,
            this.layoutControlLabelItem20,
            this.layoutControlSeparatorItem2});
            this.radLayoutControl1.Location = new System.Drawing.Point(13, 13);
            this.radLayoutControl1.Name = "radLayoutControl1";
            this.radLayoutControl1.Size = new System.Drawing.Size(626, 632);
            this.radLayoutControl1.TabIndex = 0;
            this.radLayoutControl1.Text = "radLayoutControl1";
            // 
            // radTextBox1
            // 
            this.radTextBox1.Location = new System.Drawing.Point(3, 137);
            this.radTextBox1.Multiline = true;
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(620, 74);
            this.radTextBox1.TabIndex = 6;
            this.radTextBox1.Text = "UI components for Windows Forms";
            // 
            // radTextBox3
            // 
            this.radTextBox3.Location = new System.Drawing.Point(3, 471);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(620, 20);
            this.radTextBox3.TabIndex = 9;
            this.radTextBox3.Text = "Anne Dodsworth";
            // 
            // radTextBox4
            // 
            this.radTextBox4.Location = new System.Drawing.Point(316, 91);
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.Size = new System.Drawing.Size(307, 20);
            this.radTextBox4.TabIndex = 13;
            this.radTextBox4.Text = "http://www.telerik.com/products/winforms.aspx";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(316, 217);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(150, 24);
            this.radButton1.TabIndex = 18;
            this.radButton1.Text = "Clone";
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(472, 217);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(151, 24);
            this.radButton2.TabIndex = 19;
            this.radButton2.Text = "Download ZIP";
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(3, 91);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.Size = new System.Drawing.Size(307, 20);
            this.radTextBox2.TabIndex = 20;
            this.radTextBox2.Text = "Telerik UI for WinForms";
            // 
            // radTextBox5
            // 
            this.radTextBox5.Location = new System.Drawing.Point(3, 419);
            this.radTextBox5.Name = "radTextBox5";
            this.radTextBox5.Size = new System.Drawing.Size(620, 20);
            this.radTextBox5.TabIndex = 22;
            this.radTextBox5.Text = "Andrew Fuller";
            // 
            // radTextBox6
            // 
            this.radTextBox6.Location = new System.Drawing.Point(3, 367);
            this.radTextBox6.Name = "radTextBox6";
            this.radTextBox6.Size = new System.Drawing.Size(620, 20);
            this.radTextBox6.TabIndex = 23;
            this.radTextBox6.Text = "Nancy Davolio";
            // 
            // radTextBox7
            // 
            this.radTextBox7.Location = new System.Drawing.Point(3, 315);
            this.radTextBox7.Name = "radTextBox7";
            this.radTextBox7.Size = new System.Drawing.Size(620, 20);
            this.radTextBox7.TabIndex = 24;
            this.radTextBox7.Text = "John Smith";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AccessibleDescription = "Mandra";
            this.layoutControlItem3.AccessibleName = "Mandra";
            this.layoutControlItem3.AssociatedControl = this.radTextBox1;
            this.layoutControlItem3.Bounds = new System.Drawing.Rectangle(0, 114, 626, 100);
            this.layoutControlItem3.DrawText = true;
            this.layoutControlItem3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(46, 100);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Text = "Description";
            this.layoutControlItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem3.TextFixedSize = 20;
            this.layoutControlItem3.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem3.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AccessibleDescription = "asdadas";
            this.layoutControlItem6.AccessibleName = "asdadas";
            this.layoutControlItem6.AssociatedControl = this.radTextBox3;
            this.layoutControlItem6.Bounds = new System.Drawing.Rectangle(0, 442, 626, 52);
            this.layoutControlItem6.DrawText = true;
            this.layoutControlItem6.Image = global::Telerik.Examples.WinControls.Properties.Resources.user;
            this.layoutControlItem6.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem6.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 52);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(46, 52);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Text = "Team Member 4";
            this.layoutControlItem6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem6.TextFixedSize = 26;
            this.layoutControlItem6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.layoutControlItem6.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem6.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AccessibleDescription = "Website";
            this.layoutControlItem9.AccessibleName = "Website";
            this.layoutControlItem9.AssociatedControl = this.radTextBox4;
            this.layoutControlItem9.Bounds = new System.Drawing.Rectangle(313, 68, 313, 46);
            this.layoutControlItem9.DrawText = true;
            this.layoutControlItem9.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 46);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(46, 46);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Text = "Website";
            this.layoutControlItem9.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem9.TextFixedSize = 20;
            this.layoutControlItem9.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem9.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlLabelItem10
            // 
            this.layoutControlLabelItem10.AccessibleDescription = "<html><b>3875</b> commits";
            this.layoutControlLabelItem10.AccessibleName = "<html><b>3875</b> commits";
            this.layoutControlLabelItem10.Bounds = new System.Drawing.Rectangle(0, 38, 156, 26);
            this.layoutControlLabelItem10.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlLabelItem10.Name = "layoutControlLabelItem10";
            this.layoutControlLabelItem10.Padding = new System.Windows.Forms.Padding(3);
            this.layoutControlLabelItem10.Text = "<html><span style=\"font-family:Segoe UI Light;font-size:12pt;color:#1A9B56\"><b>38" +
    "75</b> </span><span style=\"font-size:8.5pt\">commits</span>";
            this.layoutControlLabelItem10.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlLabelItem11
            // 
            this.layoutControlLabelItem11.AccessibleDescription = "<html><b>24</b> releases";
            this.layoutControlLabelItem11.AccessibleName = "<html><b>24</b> releases";
            this.layoutControlLabelItem11.Bounds = new System.Drawing.Rectangle(312, 38, 156, 26);
            this.layoutControlLabelItem11.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlLabelItem11.Name = "layoutControlLabelItem11";
            this.layoutControlLabelItem11.Padding = new System.Windows.Forms.Padding(3);
            this.layoutControlLabelItem11.Text = "<html><span style=\"font-family:Segoe UI Light;font-size:12pt;color:#1A9B56\"><b>24" +
    "</b> </span><span style=\"font-size:8.5pt\">releases</span>";
            this.layoutControlLabelItem11.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlLabelItem12
            // 
            this.layoutControlLabelItem12.AccessibleDescription = "<html><b>5</b> branches";
            this.layoutControlLabelItem12.AccessibleName = "<html><b>5</b> branches";
            this.layoutControlLabelItem12.Bounds = new System.Drawing.Rectangle(156, 38, 156, 26);
            this.layoutControlLabelItem12.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlLabelItem12.Name = "layoutControlLabelItem12";
            this.layoutControlLabelItem12.Padding = new System.Windows.Forms.Padding(3);
            this.layoutControlLabelItem12.Text = "<html><span style=\"font-family:Segoe UI Light;font-size:12pt;color:#1A9B56\"><b>5<" +
    "/b> </span><span style=\"font-size:8.5pt\">branches</span>";
            this.layoutControlLabelItem12.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlLabelItem13
            // 
            this.layoutControlLabelItem13.AccessibleDescription = "<html><b>32</b> contributors";
            this.layoutControlLabelItem13.AccessibleName = "<html><b>32</b> contributors";
            this.layoutControlLabelItem13.Bounds = new System.Drawing.Rectangle(468, 38, 158, 26);
            this.layoutControlLabelItem13.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlLabelItem13.Name = "layoutControlLabelItem13";
            this.layoutControlLabelItem13.Padding = new System.Windows.Forms.Padding(3);
            this.layoutControlLabelItem13.Text = "<html><span style=\"font-family:Segoe UI Light;font-size:12pt;color:#1A9B56\"><b>32" +
    "</b> </span><span style=\"font-size:8.5pt\">contributors</span>";
            this.layoutControlLabelItem13.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlLabelItem1
            // 
            this.layoutControlLabelItem1.Bounds = new System.Drawing.Rectangle(0, 214, 313, 30);
            this.layoutControlLabelItem1.DrawText = false;
            this.layoutControlLabelItem1.Name = "layoutControlLabelItem1";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AssociatedControl = this.radButton1;
            this.layoutControlItem4.Bounds = new System.Drawing.Rectangle(313, 214, 156, 30);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(46, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AssociatedControl = this.radButton2;
            this.layoutControlItem14.Bounds = new System.Drawing.Rectangle(469, 214, 157, 30);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(46, 30);
            this.layoutControlItem14.Name = "layoutControlItem14";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AccessibleDescription = "Project name";
            this.layoutControlItem5.AccessibleName = "Project name";
            this.layoutControlItem5.AssociatedControl = this.radTextBox2;
            this.layoutControlItem5.Bounds = new System.Drawing.Rectangle(0, 68, 313, 46);
            this.layoutControlItem5.DrawText = true;
            this.layoutControlItem5.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 46);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(46, 46);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Text = "Project name";
            this.layoutControlItem5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem5.TextFixedSize = 20;
            this.layoutControlItem5.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem5.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlLabelItem15
            // 
            this.layoutControlLabelItem15.AccessibleDescription = "Team Members";
            this.layoutControlLabelItem15.AccessibleName = "Team Members";
            this.layoutControlLabelItem15.Bounds = new System.Drawing.Rectangle(0, 248, 626, 38);
            this.layoutControlLabelItem15.Font = new System.Drawing.Font("Segoe UI Light", 15F, System.Drawing.FontStyle.Bold);
            this.layoutControlLabelItem15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(155)))), ((int)(((byte)(86)))));
            this.layoutControlLabelItem15.MaxSize = new System.Drawing.Size(0, 38);
            this.layoutControlLabelItem15.MinSize = new System.Drawing.Size(46, 38);
            this.layoutControlLabelItem15.Name = "layoutControlLabelItem15";
            this.layoutControlLabelItem15.Padding = new System.Windows.Forms.Padding(3);
            this.layoutControlLabelItem15.Text = "team members";
            this.layoutControlLabelItem15.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AccessibleDescription = "Team Member";
            this.layoutControlItem17.AccessibleName = "Team Member";
            this.layoutControlItem17.AssociatedControl = this.radTextBox5;
            this.layoutControlItem17.Bounds = new System.Drawing.Rectangle(0, 390, 626, 52);
            this.layoutControlItem17.DrawImage = true;
            this.layoutControlItem17.DrawText = true;
            this.layoutControlItem17.Image = global::Telerik.Examples.WinControls.Properties.Resources.user;
            this.layoutControlItem17.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem17.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 52);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(46, 52);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Text = "Team Member 3";
            this.layoutControlItem17.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem17.TextFixedSize = 26;
            this.layoutControlItem17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.layoutControlItem17.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem17.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AccessibleDescription = "Team Member";
            this.layoutControlItem18.AccessibleName = "Team Member";
            this.layoutControlItem18.AssociatedControl = this.radTextBox6;
            this.layoutControlItem18.Bounds = new System.Drawing.Rectangle(0, 338, 626, 52);
            this.layoutControlItem18.DrawImage = true;
            this.layoutControlItem18.DrawText = true;
            this.layoutControlItem18.Image = global::Telerik.Examples.WinControls.Properties.Resources.user;
            this.layoutControlItem18.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem18.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 52);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(46, 52);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Text = "Team Member 2";
            this.layoutControlItem18.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem18.TextFixedSize = 26;
            this.layoutControlItem18.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.layoutControlItem18.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem18.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AccessibleDescription = "Team Member";
            this.layoutControlItem19.AccessibleName = "Team Member";
            this.layoutControlItem19.AssociatedControl = this.radTextBox7;
            this.layoutControlItem19.Bounds = new System.Drawing.Rectangle(0, 286, 626, 52);
            this.layoutControlItem19.DrawImage = true;
            this.layoutControlItem19.DrawText = true;
            this.layoutControlItem19.Image = global::Telerik.Examples.WinControls.Properties.Resources.user;
            this.layoutControlItem19.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem19.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(0, 52);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(46, 52);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Text = "Team Member 1";
            this.layoutControlItem19.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem19.TextFixedSize = 26;
            this.layoutControlItem19.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.layoutControlItem19.TextPosition = Telerik.WinControls.UI.LayoutItemTextPosition.Top;
            this.layoutControlItem19.TextSizeMode = Telerik.WinControls.UI.LayoutItemTextSizeMode.Fixed;
            // 
            // layoutControlLabelItem2
            // 
            this.layoutControlLabelItem2.Bounds = new System.Drawing.Rectangle(0, 494, 626, 138);
            this.layoutControlLabelItem2.DrawText = false;
            this.layoutControlLabelItem2.Name = "layoutControlLabelItem2";
            // 
            // layoutControlSeparatorItem1
            // 
            this.layoutControlSeparatorItem1.Bounds = new System.Drawing.Rectangle(0, 64, 626, 4);
            this.layoutControlSeparatorItem1.Name = "layoutControlSeparatorItem1";
            // 
            // layoutControlLabelItem20
            // 
            this.layoutControlLabelItem20.AccessibleDescription = "Project";
            this.layoutControlLabelItem20.AccessibleName = "Project";
            this.layoutControlLabelItem20.Bounds = new System.Drawing.Rectangle(0, 0, 626, 38);
            this.layoutControlLabelItem20.Font = new System.Drawing.Font("Segoe UI Light", 15F, System.Drawing.FontStyle.Bold);
            this.layoutControlLabelItem20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(155)))), ((int)(((byte)(86)))));
            this.layoutControlLabelItem20.MaxSize = new System.Drawing.Size(0, 38);
            this.layoutControlLabelItem20.MinSize = new System.Drawing.Size(46, 38);
            this.layoutControlLabelItem20.Name = "layoutControlLabelItem20";
            this.layoutControlLabelItem20.Padding = new System.Windows.Forms.Padding(3);
            this.layoutControlLabelItem20.Text = "telerik/winforms";
            this.layoutControlLabelItem20.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radButtonAddMember
            // 
            this.radButtonAddMember.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radButtonAddMember.Location = new System.Drawing.Point(10, 32);
            this.radButtonAddMember.Name = "radButtonAddMember";
            this.radButtonAddMember.Size = new System.Drawing.Size(210, 24);
            this.radButtonAddMember.TabIndex = 1;
            this.radButtonAddMember.Text = "Add team member";
            this.radButtonAddMember.Click += new System.EventHandler(this.radButtonAddMember_Click);
            // 
            // radButtonEditLayout
            // 
            this.radButtonEditLayout.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radButtonEditLayout.Location = new System.Drawing.Point(10, 62);
            this.radButtonEditLayout.Name = "radButtonEditLayout";
            this.radButtonEditLayout.Size = new System.Drawing.Size(210, 24);
            this.radButtonEditLayout.TabIndex = 1;
            this.radButtonEditLayout.Text = "Edit Layout";
            this.radButtonEditLayout.Click += new System.EventHandler(this.radButtonCustomize_Click);
            // 
            // radButtonSaveLayout
            // 
            this.radButtonSaveLayout.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radButtonSaveLayout.Location = new System.Drawing.Point(10, 92);
            this.radButtonSaveLayout.Name = "radButtonSaveLayout";
            this.radButtonSaveLayout.Size = new System.Drawing.Size(210, 24);
            this.radButtonSaveLayout.TabIndex = 1;
            this.radButtonSaveLayout.Text = "Save Layout";
            this.radButtonSaveLayout.Click += new System.EventHandler(this.radButtonSaveLayout_Click);
            // 
            // radButtonLoadLayout
            // 
            this.radButtonLoadLayout.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radButtonLoadLayout.Location = new System.Drawing.Point(10, 122);
            this.radButtonLoadLayout.Name = "radButtonLoadLayout";
            this.radButtonLoadLayout.Size = new System.Drawing.Size(210, 24);
            this.radButtonLoadLayout.TabIndex = 1;
            this.radButtonLoadLayout.Text = "Load Layout";
            this.radButtonLoadLayout.Click += new System.EventHandler(this.radButtonLoadLayout_Click);
            // 
            // layoutControlSeparatorItem2
            // 
            this.layoutControlSeparatorItem2.Bounds = new System.Drawing.Rectangle(0, 244, 626, 4);
            this.layoutControlSeparatorItem2.Name = "layoutControlSeparatorItem2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radLayoutControl1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1094, 1039);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLayoutControl1)).EndInit();
            this.radLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonAddMember)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonEditLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSaveLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonLoadLayout)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLayoutControl radLayoutControl1;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBox1;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBox3;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBox4;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem3;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem6;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem9;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem10;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem11;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem12;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem13;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBox2;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBox5;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBox6;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBox7;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem1;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem4;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem14;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem5;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem15;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem17;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem18;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem19;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem2;
        private Telerik.WinControls.UI.LayoutControlSeparatorItem layoutControlSeparatorItem1;
        private Telerik.WinControls.UI.RadButton radButtonAddMember;
        private Telerik.WinControls.UI.RadButton radButtonEditLayout;
        private Telerik.WinControls.UI.RadButton radButtonSaveLayout;
        private Telerik.WinControls.UI.RadButton radButtonLoadLayout;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem20;
        private Telerik.WinControls.UI.LayoutControlSeparatorItem layoutControlSeparatorItem2;
    }
}