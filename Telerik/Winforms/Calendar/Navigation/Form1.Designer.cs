namespace Telerik.Examples.WinControls.Calendar.Navigation
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.radCalendar1 = new Telerik.WinControls.UI.RadCalendar();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox3 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker3 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupRenderOpts = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupDateOpts = new Telerik.WinControls.UI.RadGroupBox();
            this.radSpinNavigationStep = new Telerik.WinControls.UI.RadSpinEditor();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radDateTimePicker4 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditor1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupRenderOpts)).BeginInit();
            this.radGroupRenderOpts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupDateOpts)).BeginInit();
            this.radGroupDateOpts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinNavigationStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Controls.Add(this.radGroupRenderOpts);
            this.settingsPanel.Controls.Add(this.radGroupDateOpts);
            this.settingsPanel.Location = new System.Drawing.Point(729, 1);
            this.settingsPanel.Size = new System.Drawing.Size(250, 867);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupDateOpts, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupRenderOpts, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            // 
            // radCalendar1
            // 
            this.radCalendar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.radCalendar1.FocusedDate = new System.DateTime(2009, 3, 7, 0, 0, 0, 0);
            this.radCalendar1.ForeColor = System.Drawing.Color.Black;
            this.radCalendar1.Location = new System.Drawing.Point(0, 0);
            this.radCalendar1.Name = "radCalendar1";
            this.radCalendar1.Size = new System.Drawing.Size(288, 229);
            this.radCalendar1.TabIndex = 0;
            this.radCalendar1.Text = "radCalendar1";
            this.radCalendar1.ZoomFactor = 1.2F;
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(5, 147);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(89, 18);
            this.radLabel1.TabIndex = 4;
            this.radLabel1.Text = "Navigate to date";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox1.Location = new System.Drawing.Point(5, 50);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(147, 18);
            this.radCheckBox1.TabIndex = 9;
            this.radCheckBox1.Text = "Show Navigation Buttons";
            this.radCheckBox1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBox2
            // 
            this.radCheckBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox2.Location = new System.Drawing.Point(5, 73);
            this.radCheckBox2.Name = "radCheckBox2";
            this.radCheckBox2.Size = new System.Drawing.Size(128, 18);
            this.radCheckBox2.TabIndex = 10;
            this.radCheckBox2.Text = "Show Fast Navigation";
            this.radCheckBox2.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBox3
            // 
            this.radCheckBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox3.Location = new System.Drawing.Point(5, 27);
            this.radCheckBox3.Name = "radCheckBox3";
            this.radCheckBox3.Size = new System.Drawing.Size(124, 18);
            this.radCheckBox3.TabIndex = 11;
            this.radCheckBox3.Text = "Show Navigation Bar";
            this.radCheckBox3.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radLabel9
            // 
            this.radLabel9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel9.Location = new System.Drawing.Point(5, 129);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(106, 18);
            this.radLabel9.TabIndex = 23;
            this.radLabel9.Text = "Fast navigation step";
            this.radLabel9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radDateTimePicker3
            // 
            this.radDateTimePicker3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDateTimePicker3.Checked = true;
            this.radDateTimePicker3.Location = new System.Drawing.Point(5, 168);
            this.radDateTimePicker3.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker3.Name = "radDateTimePicker3";
            this.radDateTimePicker3.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker3.Size = new System.Drawing.Size(220, 20);
            this.radDateTimePicker3.TabIndex = 13;
            this.radDateTimePicker3.TabStop = false;
            this.radDateTimePicker3.Text = "Monday, September 10, 2007";
            this.radDateTimePicker3.Value = new System.DateTime(2007, 9, 10, 10, 33, 34, 212);
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDateTimePicker1.Checked = true;
            this.radDateTimePicker1.Location = new System.Drawing.Point(5, 108);
            this.radDateTimePicker1.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker1.Size = new System.Drawing.Size(220, 20);
            this.radDateTimePicker1.TabIndex = 19;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "Friday, August 24, 1900";
            this.radDateTimePicker1.Value = new System.DateTime(1900, 8, 24, 14, 50, 0, 0);
            // 
            // radLabel8
            // 
            this.radLabel8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel8.Location = new System.Drawing.Point(5, 87);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(86, 18);
            this.radLabel8.TabIndex = 22;
            this.radLabel8.Text = "Range Min Date";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDateTimePicker2.Checked = true;
            this.radDateTimePicker2.Location = new System.Drawing.Point(5, 48);
            this.radDateTimePicker2.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            this.radDateTimePicker2.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker2.Size = new System.Drawing.Size(220, 20);
            this.radDateTimePicker2.TabIndex = 20;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.Text = "Tuesday, August 24, 2100";
            this.radDateTimePicker2.Value = new System.DateTime(2100, 8, 24, 14, 50, 0, 0);
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel7.Location = new System.Drawing.Point(5, 27);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(88, 18);
            this.radLabel7.TabIndex = 21;
            this.radLabel7.Text = "Range Max Date";
            this.radLabel7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radGroupRenderOpts
            // 
            this.radGroupRenderOpts.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupRenderOpts.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupRenderOpts.Controls.Add(this.radCheckBox3);
            this.radGroupRenderOpts.Controls.Add(this.radCheckBox1);
            this.radGroupRenderOpts.Controls.Add(this.radCheckBox2);
            this.radGroupRenderOpts.HeaderText = "Rendering Options";
            this.radGroupRenderOpts.Location = new System.Drawing.Point(10, 6);
            this.radGroupRenderOpts.Name = "radGroupRenderOpts";
            this.radGroupRenderOpts.Size = new System.Drawing.Size(230, 104);
            this.radGroupRenderOpts.TabIndex = 13;
            this.radGroupRenderOpts.Text = "Rendering Options";
            // 
            // radGroupDateOpts
            // 
            this.radGroupDateOpts.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupDateOpts.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupDateOpts.Controls.Add(this.radLabel7);
            this.radGroupDateOpts.Controls.Add(this.radDateTimePicker3);
            this.radGroupDateOpts.Controls.Add(this.radDateTimePicker2);
            this.radGroupDateOpts.Controls.Add(this.radLabel8);
            this.radGroupDateOpts.Controls.Add(this.radDateTimePicker1);
            this.radGroupDateOpts.Controls.Add(this.radLabel1);
            this.radGroupDateOpts.HeaderText = "Date Options";
            this.radGroupDateOpts.Location = new System.Drawing.Point(10, 116);
            this.radGroupDateOpts.Name = "radGroupDateOpts";
            this.radGroupDateOpts.Size = new System.Drawing.Size(230, 202);
            this.radGroupDateOpts.TabIndex = 13;
            this.radGroupDateOpts.Text = "Date Options";
            // 
            // radSpinNavigationStep
            // 
            this.radSpinNavigationStep.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinNavigationStep.Location = new System.Drawing.Point(5, 150);
            this.radSpinNavigationStep.Name = "radSpinNavigationStep";
            // 
            // 
            // 
            this.radSpinNavigationStep.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinNavigationStep.Size = new System.Drawing.Size(220, 20);
            this.radSpinNavigationStep.TabIndex = 24;
            this.radSpinNavigationStep.TabStop = false;
            this.radSpinNavigationStep.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.radDropDownList1);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radSpinEditor1);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radSpinNavigationStep);
            this.radGroupBox1.HeaderText = "Navigation Options";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 334);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(230, 188);
            this.radGroupBox1.TabIndex = 25;
            this.radGroupBox1.Text = "Navigation Options";
            // 
            // radDateTimePicker4
            // 
            this.radDateTimePicker4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDateTimePicker4.Checked = true;
            this.radDateTimePicker4.Location = new System.Drawing.Point(5, 168);
            this.radDateTimePicker4.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker4.Name = "radDateTimePicker4";
            this.radDateTimePicker4.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker4.Size = new System.Drawing.Size(220, 20);
            this.radDateTimePicker4.TabIndex = 13;
            this.radDateTimePicker4.TabStop = false;
            this.radDateTimePicker4.Text = "Monday, September 10, 2007";
            this.radDateTimePicker4.Value = new System.DateTime(2007, 9, 10, 10, 33, 34, 212);
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(5, 80);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(64, 18);
            this.radLabel2.TabIndex = 25;
            this.radLabel2.Text = "Month step";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radSpinEditor1
            // 
            this.radSpinEditor1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditor1.Location = new System.Drawing.Point(5, 101);
            this.radSpinEditor1.Name = "radSpinEditor1";
            // 
            // 
            // 
            this.radSpinEditor1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor1.Size = new System.Drawing.Size(220, 20);
            this.radSpinEditor1.TabIndex = 26;
            this.radSpinEditor1.TabStop = false;
            this.radSpinEditor1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(6, 28);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(130, 18);
            this.radLabel3.TabIndex = 27;
            this.radLabel3.Text = "Header navigation mode";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.AllowShowFocusCues = false;
            this.radDropDownList1.Location = new System.Drawing.Point(6, 49);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(219, 20);
            this.radDropDownList1.TabIndex = 28;
            this.radDropDownList1.Text = "radDropDownList1";
            this.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Form1
            // 
            this.Controls.Add(this.radCalendar1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1237, 814);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radCalendar1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupRenderOpts)).EndInit();
            this.radGroupRenderOpts.ResumeLayout(false);
            this.radGroupRenderOpts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupDateOpts)).EndInit();
            this.radGroupDateOpts.ResumeLayout(false);
            this.radGroupDateOpts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinNavigationStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private Telerik.WinControls.UI.RadCalendar radCalendar1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
		private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
		private Telerik.WinControls.UI.RadCheckBox radCheckBox2;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox3;
		private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
		private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
		private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
		private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker3;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadGroupBox radGroupRenderOpts;
        private Telerik.WinControls.UI.RadGroupBox radGroupDateOpts;
        private Telerik.WinControls.UI.RadSpinEditor radSpinNavigationStep;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker4;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
	}
}