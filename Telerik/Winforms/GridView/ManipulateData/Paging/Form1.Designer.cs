﻿namespace Telerik.Examples.WinControls.GridView.ManipulateData.Paging
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorPageSize = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownListPagingGroupingPriority = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorPageButtons = new Telerik.WinControls.UI.RadSpinEditor();
            this.radCheckBoxFirstButton = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxPreviousButton = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxFastBackButton = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxNumericalButtons = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxFastForwardButton = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxNextButton = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxLastButton = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxButtonsStrip = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxTextBoxStrip = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorPageSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListPagingGroupingPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorPageButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFirstButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxPreviousButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFastBackButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxNumericalButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFastForwardButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxNextButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxLastButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxButtonsStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxTextBoxStrip)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radCheckBoxLastButton);
            this.settingsPanel.Controls.Add(this.radCheckBoxNextButton);
            this.settingsPanel.Controls.Add(this.radCheckBoxFastForwardButton);
            this.settingsPanel.Controls.Add(this.radCheckBoxNumericalButtons);
            this.settingsPanel.Controls.Add(this.radCheckBoxFastBackButton);
            this.settingsPanel.Controls.Add(this.radCheckBoxTextBoxStrip);
            this.settingsPanel.Controls.Add(this.radCheckBoxPreviousButton);
            this.settingsPanel.Controls.Add(this.radCheckBoxButtonsStrip);
            this.settingsPanel.Controls.Add(this.radCheckBoxFirstButton);
            this.settingsPanel.Controls.Add(this.radDropDownListPagingGroupingPriority);
            this.settingsPanel.Controls.Add(this.radSpinEditorPageButtons);
            this.settingsPanel.Controls.Add(this.radSpinEditorPageSize);
            this.settingsPanel.Controls.Add(this.radLabel2);
            this.settingsPanel.Controls.Add(this.radLabel3);
            this.settingsPanel.Controls.Add(this.radLabel1);
            this.settingsPanel.Size = new System.Drawing.Size(230, 507);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel3, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorPageSize, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorPageButtons, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radDropDownListPagingGroupingPriority, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxFirstButton, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxButtonsStrip, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxPreviousButton, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxTextBoxStrip, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxFastBackButton, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxNumericalButtons, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxFastForwardButton, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxNextButton, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxLastButton, 0);
            // 
            // radGridView1
            // 
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            // 
            // radGridView1
            // 
            this.radGridView1.MasterTemplate.EnableFiltering = true;
            this.radGridView1.MasterTemplate.EnablePaging = true;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Size = new System.Drawing.Size(1490, 1084);
            this.radGridView1.TabIndex = 0;
            this.radGridView1.Text = "radGridView1";
            ((Telerik.WinControls.UI.PagingPanelElement)(this.radGridView1.GetChildAt(0).GetChildAt(0).GetChildAt(1))).NumericButtonsCount = 10;
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(10, 33);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(52, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Page size";
            // 
            // radSpinEditorPageSize
            // 
            this.radSpinEditorPageSize.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorPageSize.Location = new System.Drawing.Point(10, 58);
            this.radSpinEditorPageSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.radSpinEditorPageSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.radSpinEditorPageSize.Name = "radSpinEditorPageSize";
            this.radSpinEditorPageSize.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorPageSize.TabIndex = 2;
            this.radSpinEditorPageSize.TabStop = false;
            this.radSpinEditorPageSize.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(10, 84);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(150, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Paging and grouping priority";
            // 
            // radDropDownListPagingGroupingPriority
            // 
            this.radDropDownListPagingGroupingPriority.Anchor = System.Windows.Forms.AnchorStyles.Top;
            radListDataItem1.Text = "Paging before grouping";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Grouping before paging";
            radListDataItem2.TextWrap = true;
            this.radDropDownListPagingGroupingPriority.Items.Add(radListDataItem1);
            this.radDropDownListPagingGroupingPriority.Items.Add(radListDataItem2);
            this.radDropDownListPagingGroupingPriority.Location = new System.Drawing.Point(10, 109);
            this.radDropDownListPagingGroupingPriority.Name = "radDropDownListPagingGroupingPriority";
            this.radDropDownListPagingGroupingPriority.Size = new System.Drawing.Size(210, 20);
            this.radDropDownListPagingGroupingPriority.TabIndex = 3;
            this.radDropDownListPagingGroupingPriority.Text = "Paging before grouping";
            this.radDropDownListPagingGroupingPriority.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(10, 135);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(129, 18);
            this.radLabel3.TabIndex = 1;
            this.radLabel3.Text = "Number of page buttons";
            // 
            // radSpinEditorPageButtons
            // 
            this.radSpinEditorPageButtons.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorPageButtons.Location = new System.Drawing.Point(10, 160);
            this.radSpinEditorPageButtons.Maximum = new decimal(new int[] {
            35,
            0,
            0,
            0});
            this.radSpinEditorPageButtons.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.radSpinEditorPageButtons.Name = "radSpinEditorPageButtons";
            this.radSpinEditorPageButtons.Size = new System.Drawing.Size(210, 20);
            this.radSpinEditorPageButtons.TabIndex = 2;
            this.radSpinEditorPageButtons.TabStop = false;
            this.radSpinEditorPageButtons.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // radCheckBoxFirstButton
            // 
            this.radCheckBoxFirstButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxFirstButton.Location = new System.Drawing.Point(10, 245);
            this.radCheckBoxFirstButton.Name = "radCheckBoxFirstButton";
            this.radCheckBoxFirstButton.Size = new System.Drawing.Size(106, 18);
            this.radCheckBoxFirstButton.TabIndex = 4;
            this.radCheckBoxFirstButton.Text = "Show first button";
            this.radCheckBoxFirstButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxPreviousButton
            // 
            this.radCheckBoxPreviousButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxPreviousButton.Location = new System.Drawing.Point(10, 269);
            this.radCheckBoxPreviousButton.Name = "radCheckBoxPreviousButton";
            this.radCheckBoxPreviousButton.Size = new System.Drawing.Size(130, 18);
            this.radCheckBoxPreviousButton.TabIndex = 4;
            this.radCheckBoxPreviousButton.Text = "Show previous button";
            this.radCheckBoxPreviousButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxFastBackButton
            // 
            this.radCheckBoxFastBackButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxFastBackButton.Location = new System.Drawing.Point(10, 293);
            this.radCheckBoxFastBackButton.Name = "radCheckBoxFastBackButton";
            this.radCheckBoxFastBackButton.Size = new System.Drawing.Size(132, 18);
            this.radCheckBoxFastBackButton.TabIndex = 4;
            this.radCheckBoxFastBackButton.Text = "Show fast back button";
            this.radCheckBoxFastBackButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxNumericalButtons
            // 
            this.radCheckBoxNumericalButtons.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxNumericalButtons.Location = new System.Drawing.Point(10, 317);
            this.radCheckBoxNumericalButtons.Name = "radCheckBoxNumericalButtons";
            this.radCheckBoxNumericalButtons.Size = new System.Drawing.Size(141, 18);
            this.radCheckBoxNumericalButtons.TabIndex = 4;
            this.radCheckBoxNumericalButtons.Text = "Show numerical buttons";
            this.radCheckBoxNumericalButtons.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxFastForwardButton
            // 
            this.radCheckBoxFastForwardButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxFastForwardButton.Location = new System.Drawing.Point(10, 341);
            this.radCheckBoxFastForwardButton.Name = "radCheckBoxFastForwardButton";
            this.radCheckBoxFastForwardButton.Size = new System.Drawing.Size(147, 18);
            this.radCheckBoxFastForwardButton.TabIndex = 4;
            this.radCheckBoxFastForwardButton.Text = "Show fast forward button";
            this.radCheckBoxFastForwardButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxNextButton
            // 
            this.radCheckBoxNextButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxNextButton.Location = new System.Drawing.Point(10, 365);
            this.radCheckBoxNextButton.Name = "radCheckBoxNextButton";
            this.radCheckBoxNextButton.Size = new System.Drawing.Size(109, 18);
            this.radCheckBoxNextButton.TabIndex = 4;
            this.radCheckBoxNextButton.Text = "Show next button";
            this.radCheckBoxNextButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxLastButton
            // 
            this.radCheckBoxLastButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxLastButton.Location = new System.Drawing.Point(10, 389);
            this.radCheckBoxLastButton.Name = "radCheckBoxLastButton";
            this.radCheckBoxLastButton.Size = new System.Drawing.Size(105, 18);
            this.radCheckBoxLastButton.TabIndex = 4;
            this.radCheckBoxLastButton.Text = "Show last button";
            this.radCheckBoxLastButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxButtonsStrip
            // 
            this.radCheckBoxButtonsStrip.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxButtonsStrip.Location = new System.Drawing.Point(10, 197);
            this.radCheckBoxButtonsStrip.Name = "radCheckBoxButtonsStrip";
            this.radCheckBoxButtonsStrip.Size = new System.Drawing.Size(114, 18);
            this.radCheckBoxButtonsStrip.TabIndex = 4;
            this.radCheckBoxButtonsStrip.Text = "Show buttons strip";
            this.radCheckBoxButtonsStrip.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBoxTextBoxStrip
            // 
            this.radCheckBoxTextBoxStrip.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxTextBoxStrip.Location = new System.Drawing.Point(10, 221);
            this.radCheckBoxTextBoxStrip.Name = "radCheckBoxTextBoxStrip";
            this.radCheckBoxTextBoxStrip.Size = new System.Drawing.Size(113, 18);
            this.radCheckBoxTextBoxStrip.TabIndex = 4;
            this.radCheckBoxTextBoxStrip.Text = "Show textbox strip";
            this.radCheckBoxTextBoxStrip.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGridView1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1500, 1094);
            this.Controls.SetChildIndex(this.radGridView1, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorPageSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListPagingGroupingPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorPageButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFirstButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxPreviousButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFastBackButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxNumericalButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFastForwardButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxNextButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxLastButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxButtonsStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxTextBoxStrip)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownListPagingGroupingPriority;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorPageSize;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorPageButtons;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxLastButton;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxNextButton;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxFastForwardButton;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxNumericalButtons;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxFastBackButton;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxPreviousButton;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxFirstButton;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxTextBoxStrip;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxButtonsStrip;

    }
}