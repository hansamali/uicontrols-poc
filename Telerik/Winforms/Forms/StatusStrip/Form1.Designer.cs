﻿namespace Telerik.Examples.WinControls.Forms.StatusStrip
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.radStatusBar1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement1 = new Telerik.WinControls.UI.RadButtonElement();
            this.CommandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabelElement3 = new Telerik.WinControls.UI.RadLabelElement();
            this.CommandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabelElement2 = new Telerik.WinControls.UI.RadLabelElement();
            this.radProgressBarElement1 = new Telerik.WinControls.UI.RadProgressBarElement();
            this.CommandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCheckBoxElement1 = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radLabelElement1 = new Telerik.WinControls.UI.RadLabelElement();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radSplitButtonElement1 = new Telerik.WinControls.UI.RadSplitButtonElement();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem10 = new Telerik.WinControls.UI.RadMenuItem();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem8 = new Telerik.WinControls.UI.RadMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.roundRectShape1 = new Telerik.WinControls.RoundRectShape(this.components);
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radStatusBar1
            // 
            this.radStatusBar1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement1,
            this.CommandBarSeparator3,
            this.radLabelElement3,
            this.CommandBarSeparator4,
            this.radLabelElement2,
            this.radProgressBarElement1,
            this.CommandBarSeparator5,
            this.radCheckBoxElement1,
            this.radLabelElement1,
            this.CommandBarSeparator2,
            this.radSplitButtonElement1,
            this.CommandBarSeparator1});
            this.radStatusBar1.Location = new System.Drawing.Point(0, 217);
            this.radStatusBar1.Name = "radStatusBar1";
            this.radStatusBar1.Size = new System.Drawing.Size(631, 26);
            this.radStatusBar1.TabIndex = 0;
            this.radStatusBar1.Text = "radStatusBar1";
            ((Telerik.WinControls.UI.RadStatusBarElement)(this.radStatusBar1.GetChildAt(0))).Text = "radStatusBar1";
            // 
            // radButtonElement1
            // 
            this.radButtonElement1.AccessibleDescription = "Page 1 of 1";
            this.radButtonElement1.AccessibleName = "Page 1 of 1";
            this.radButtonElement1.CanFocus = true;
            this.radButtonElement1.Name = "radButtonElement1";
            this.radStatusBar1.SetSpring(this.radButtonElement1, false);
            this.radButtonElement1.Text = "Page 1 of 1";
            this.radButtonElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;

            // 
            // CommandBarSeparator3
            // 
            this.CommandBarSeparator3.Margin = new System.Windows.Forms.Padding(1);
            this.CommandBarSeparator3.MinSize = new System.Drawing.Size(2, 17);
            this.CommandBarSeparator3.Name = "CommandBarSeparator3";
            this.radStatusBar1.SetSpring(this.CommandBarSeparator3, false);
            this.CommandBarSeparator3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.CommandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radLabelElement3
            // 
            this.radLabelElement3.AccessibleDescription = "Words: 2";
            this.radLabelElement3.AccessibleName = "Words: 2";
            this.radLabelElement3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(165)))), ((int)(((byte)(165)))));
            this.radLabelElement3.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.radLabelElement3.Name = "radLabelElement3";
            this.radStatusBar1.SetSpring(this.radLabelElement3, false);
            this.radLabelElement3.Text = "Words: 2";
            this.radLabelElement3.TextWrap = true;
            this.radLabelElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // CommandBarSeparator4
            // 
            this.CommandBarSeparator4.MinSize = new System.Drawing.Size(2, 17);
            this.CommandBarSeparator4.Name = "CommandBarSeparator4";
            this.radStatusBar1.SetSpring(this.CommandBarSeparator4, false);
            this.CommandBarSeparator4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.CommandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radLabelElement2
            // 
            this.radLabelElement2.AccessibleDescription = "Saving...";
            this.radLabelElement2.AccessibleName = "Saving...";
            this.radLabelElement2.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.radLabelElement2.Name = "radLabelElement2";
            this.radStatusBar1.SetSpring(this.radLabelElement2, false);
            this.radLabelElement2.Text = "Saving...";
            this.radLabelElement2.TextWrap = true;
            this.radLabelElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radProgressBarElement1
            // 
            this.radProgressBarElement1.AutoSize = false;
            this.radProgressBarElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.radProgressBarElement1.Bounds = new System.Drawing.Rectangle(0, 0, 133, 16);
            this.radProgressBarElement1.ClipDrawing = true;
            this.radProgressBarElement1.DefaultSize = new System.Drawing.Size(130, 16);
            this.radProgressBarElement1.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.radProgressBarElement1.Name = "radProgressBarElement1";
            this.radProgressBarElement1.SeparatorColor1 = System.Drawing.Color.White;
            this.radProgressBarElement1.SeparatorColor2 = System.Drawing.Color.White;
            this.radProgressBarElement1.SeparatorColor3 = System.Drawing.Color.White;
            this.radProgressBarElement1.SeparatorColor4 = System.Drawing.Color.White;
            this.radProgressBarElement1.SeparatorGradientAngle = 0;
            this.radProgressBarElement1.SeparatorGradientPercentage1 = 0.4F;
            this.radProgressBarElement1.SeparatorGradientPercentage2 = 0.6F;
            this.radProgressBarElement1.SeparatorNumberOfColors = 2;
            this.radStatusBar1.SetSpring(this.radProgressBarElement1, false);
            this.radProgressBarElement1.StepWidth = 14;
            this.radProgressBarElement1.SweepAngle = 90;
            this.radProgressBarElement1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radProgressBarElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // CommandBarSeparator5
            // 
            this.CommandBarSeparator5.Margin = new System.Windows.Forms.Padding(1);
            this.CommandBarSeparator5.MinSize = new System.Drawing.Size(2, 17);
            this.CommandBarSeparator5.Name = "CommandBarSeparator5";
            this.radStatusBar1.SetSpring(this.CommandBarSeparator5, false);
            this.CommandBarSeparator5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.CommandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radCheckBoxElement1
            // 
            this.radCheckBoxElement1.AccessibleDescription = "Check for errors";
            this.radCheckBoxElement1.AccessibleName = "Check for errors";
            this.radCheckBoxElement1.CanFocus = true;
            this.radCheckBoxElement1.Checked = false;
            this.radCheckBoxElement1.Margin = new System.Windows.Forms.Padding(0);
            this.radCheckBoxElement1.Name = "radCheckBoxElement1";
            this.radCheckBoxElement1.ShowBorder = false;
            this.radStatusBar1.SetSpring(this.radCheckBoxElement1, false);
            this.radCheckBoxElement1.Text = "Check for errors";
            this.radCheckBoxElement1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            this.radCheckBoxElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radLabelElement1
            // 
            this.radLabelElement1.AccessibleDescription = "English (US)";
            this.radLabelElement1.AccessibleName = "English (US)";
            this.radLabelElement1.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.radLabelElement1.Name = "radLabelElement1";
            this.radStatusBar1.SetSpring(this.radLabelElement1, false);
            this.radLabelElement1.Text = "English (US)";
            this.radLabelElement1.TextWrap = true;
            this.radLabelElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.Margin = new System.Windows.Forms.Padding(1);
            this.CommandBarSeparator2.MinSize = new System.Drawing.Size(2, 17);
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.radStatusBar1.SetSpring(this.CommandBarSeparator2, false);
            this.CommandBarSeparator2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radSplitButtonElement1
            // 
            this.radSplitButtonElement1.AccessibleDescription = "Zoom";
            this.radSplitButtonElement1.AccessibleName = "Zoom";
            this.radSplitButtonElement1.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radSplitButtonElement1.DefaultItem = null;
            this.radSplitButtonElement1.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radSplitButtonElement1.ExpandArrowButton = false;
            this.radSplitButtonElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2,
            this.radMenuItem9,
            this.radMenuItem10});
            this.radSplitButtonElement1.Margin = new System.Windows.Forms.Padding(0);
            this.radSplitButtonElement1.Name = "radSplitButtonElement1";
            this.radStatusBar1.SetSpring(this.radSplitButtonElement1, false);
            this.radSplitButtonElement1.Text = "Zoom";
            this.radSplitButtonElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.radSplitButtonElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "50%";
            this.radMenuItem1.AccessibleName = "50%";
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "50%";
            this.radMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "75%";
            this.radMenuItem2.AccessibleName = "75%";
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "75%";
            this.radMenuItem2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem9
            // 
            this.radMenuItem9.AccessibleDescription = "100%";
            this.radMenuItem9.AccessibleName = "100%";
            this.radMenuItem9.Name = "radMenuItem9";
            this.radMenuItem9.Text = "100%";
            this.radMenuItem9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem10
            // 
            this.radMenuItem10.AccessibleDescription = "200%";
            this.radMenuItem10.AccessibleName = "200%";
            this.radMenuItem10.Name = "radMenuItem10";
            this.radMenuItem10.Text = "200%";
            this.radMenuItem10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.MinSize = new System.Drawing.Size(2, 17);
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.radStatusBar1.SetSpring(this.CommandBarSeparator1, false);
            this.CommandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.AccessibleDescription = "radMenuItem3";
            this.radMenuItem3.AccessibleName = "radMenuItem3";
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Text = "radMenuItem3";
            this.radMenuItem3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.AccessibleDescription = "radMenuItem4";
            this.radMenuItem4.AccessibleName = "radMenuItem4";
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Text = "radMenuItem4";
            this.radMenuItem4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem5
            // 
            this.radMenuItem5.AccessibleDescription = "radMenuItem5";
            this.radMenuItem5.AccessibleName = "radMenuItem5";
            this.radMenuItem5.Name = "radMenuItem5";
            this.radMenuItem5.Text = "radMenuItem5";
            this.radMenuItem5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6
            // 
            this.radMenuItem6.AccessibleDescription = "radMenuItem6";
            this.radMenuItem6.AccessibleName = "radMenuItem6";
            this.radMenuItem6.Name = "radMenuItem6";
            this.radMenuItem6.Text = "radMenuItem6";
            this.radMenuItem6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem7
            // 
            this.radMenuItem7.AccessibleDescription = "radMenuItem7";
            this.radMenuItem7.AccessibleName = "radMenuItem7";
            this.radMenuItem7.Name = "radMenuItem7";
            this.radMenuItem7.Text = "radMenuItem7";
            this.radMenuItem7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem8
            // 
            this.radMenuItem8.AccessibleDescription = "radMenuItem8";
            this.radMenuItem8.AccessibleName = "radMenuItem8";
            this.radMenuItem8.Name = "radMenuItem8";
            this.radMenuItem8.Text = "radMenuItem8";
            this.radMenuItem8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;

            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.BackColor = System.Drawing.Color.Transparent;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.radLabel1.Location = new System.Drawing.Point(0, 0);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(631, 217);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = resources.GetString("radLabel1.Text");
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 243);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radStatusBar1);
            this.Name = "Form1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "RadStatusStrip Example";
            ((System.ComponentModel.ISupportInitialize)(this.radStatusBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadStatusStrip radStatusBar1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement1;
        private Telerik.WinControls.UI.RadProgressBarElement radProgressBarElement1;
        private Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        private System.Windows.Forms.Timer timer1;
        private Telerik.WinControls.UI.RadSplitButtonElement radSplitButtonElement1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement1;
        private Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem10;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement2;
        private Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator3;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement3;
        private Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator4;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement1;
        private Telerik.WinControls.RoundRectShape roundRectShape1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}