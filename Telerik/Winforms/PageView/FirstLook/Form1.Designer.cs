﻿namespace Telerik.Examples.WinControls.PageView.FirstLook
{
    partial class Form1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.DateTimeInterval dateTimeInterval1 = new Telerik.WinControls.UI.DateTimeInterval();
            Telerik.WinControls.UI.SchedulerDailyPrintStyle schedulerDailyPrintStyle1 = new Telerik.WinControls.UI.SchedulerDailyPrintStyle();
            this.pageView = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radScrollablePanel2 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radScrollablePanel5 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radScrollablePanel4 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.radPageViewPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radScrollablePanel3 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.radPageViewPage6 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radScheduler1 = new Telerik.WinControls.UI.RadScheduler();
            this.radSchedulerNavigator1 = new Telerik.WinControls.UI.RadSchedulerNavigator();
            this.radSchedulerReminder1 = new Telerik.WinControls.UI.RadSchedulerReminder(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageView)).BeginInit();
            this.pageView.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel2)).BeginInit();
            this.radScrollablePanel2.PanelContainer.SuspendLayout();
            this.radScrollablePanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel5)).BeginInit();
            this.radScrollablePanel5.PanelContainer.SuspendLayout();
            this.radScrollablePanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.radPageViewPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel4)).BeginInit();
            this.radScrollablePanel4.PanelContainer.SuspendLayout();
            this.radScrollablePanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.radPageViewPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel3)).BeginInit();
            this.radScrollablePanel3.PanelContainer.SuspendLayout();
            this.radScrollablePanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.radPageViewPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScheduler1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSchedulerNavigator1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(715, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 723);
          
            // 
            // pageView
            // 
            this.pageView.Controls.Add(this.radPageViewPage1);
            this.pageView.Controls.Add(this.radPageViewPage2);
            this.pageView.Controls.Add(this.radPageViewPage3);
            this.pageView.Controls.Add(this.radPageViewPage4);
            this.pageView.Controls.Add(this.radPageViewPage6);
            this.pageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageView.Location = new System.Drawing.Point(0, 0);
            this.pageView.Name = "pageView";
            this.pageView.SelectedPage = this.radPageViewPage1;
            this.pageView.Size = new System.Drawing.Size(237, 519);
            this.pageView.TabIndex = 1;
            this.pageView.ViewMode = Telerik.WinControls.UI.PageViewMode.Outlook;
            // 
            // radPageViewPage1
            // 
// TODO: Code generation for '' failed because of Exception 'Invalid Primitive Type: System.IntPtr. Consider using CodeObjectCreateExpression.'.
            this.radPageViewPage1.Controls.Add(this.radScrollablePanel2);
            this.radPageViewPage1.Location = new System.Drawing.Point(5, 31);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(227, 284);
            this.radPageViewPage1.Text = "Notes";
            // 
            // radScrollablePanel2
            // 
            this.radScrollablePanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel2.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel2.Name = "radScrollablePanel2";
            // 
            // radScrollablePanel2.PanelContainer
            // 
            this.radScrollablePanel2.PanelContainer.Controls.Add(this.pictureBox5);
            this.radScrollablePanel2.PanelContainer.Size = new System.Drawing.Size(208, 265);
            // 
            // 
            // 
            this.radScrollablePanel2.RootElement.Padding = new System.Windows.Forms.Padding(1);
            this.radScrollablePanel2.Size = new System.Drawing.Size(227, 284);
            this.radScrollablePanel2.TabIndex = 3;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Telerik.Examples.WinControls.Properties.Resources.Notes;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(279, 269);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // radPageViewPage2
            // 
// TODO: Code generation for '' failed because of Exception 'Invalid Primitive Type: System.IntPtr. Consider using CodeObjectCreateExpression.'.
            this.radPageViewPage2.Controls.Add(this.radScrollablePanel5);
            this.radPageViewPage2.Location = new System.Drawing.Point(5, 28);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(205, 493);
            this.radPageViewPage2.Text = "Tasks";
            // 
            // radScrollablePanel5
            // 
            this.radScrollablePanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel5.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel5.Name = "radScrollablePanel5";
            // 
            // radScrollablePanel5.PanelContainer
            // 
            this.radScrollablePanel5.PanelContainer.Controls.Add(this.pictureBox4);
            this.radScrollablePanel5.PanelContainer.Size = new System.Drawing.Size(203, 474);
            // 
            // 
            // 
            this.radScrollablePanel5.RootElement.Padding = new System.Windows.Forms.Padding(1);
            this.radScrollablePanel5.Size = new System.Drawing.Size(205, 493);
            this.radScrollablePanel5.TabIndex = 3;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Telerik.Examples.WinControls.Properties.Resources.Tasks;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(279, 454);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // radPageViewPage3
            // 
// TODO: Code generation for '' failed because of Exception 'Invalid Primitive Type: System.IntPtr. Consider using CodeObjectCreateExpression.'.
            this.radPageViewPage3.Controls.Add(this.radScrollablePanel4);
            this.radPageViewPage3.Location = new System.Drawing.Point(5, 28);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(205, 493);
            this.radPageViewPage3.Text = "Contacts";
            // 
            // radScrollablePanel4
            // 
            this.radScrollablePanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel4.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel4.Name = "radScrollablePanel4";
            // 
            // radScrollablePanel4.PanelContainer
            // 
            this.radScrollablePanel4.PanelContainer.Controls.Add(this.pictureBox3);
            this.radScrollablePanel4.PanelContainer.Size = new System.Drawing.Size(203, 474);
            // 
            // 
            // 
            this.radScrollablePanel4.RootElement.Padding = new System.Windows.Forms.Padding(1);
            this.radScrollablePanel4.Size = new System.Drawing.Size(205, 493);
            this.radScrollablePanel4.TabIndex = 3;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Telerik.Examples.WinControls.Properties.Resources.Contacts;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(279, 326);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // radPageViewPage4
            // 
// TODO: Code generation for '' failed because of Exception 'Invalid Primitive Type: System.IntPtr. Consider using CodeObjectCreateExpression.'.
            this.radPageViewPage4.Controls.Add(this.radScrollablePanel3);
            this.radPageViewPage4.Location = new System.Drawing.Point(5, 28);
            this.radPageViewPage4.Name = "radPageViewPage4";
            this.radPageViewPage4.Size = new System.Drawing.Size(205, 493);
            this.radPageViewPage4.Text = "Calendar";
            // 
            // radScrollablePanel3
            // 
            this.radScrollablePanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel3.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel3.Name = "radScrollablePanel3";
            // 
            // radScrollablePanel3.PanelContainer
            // 
            this.radScrollablePanel3.PanelContainer.Controls.Add(this.pictureBox2);
            this.radScrollablePanel3.PanelContainer.Size = new System.Drawing.Size(186, 474);
            // 
            // 
            // 
            this.radScrollablePanel3.RootElement.Padding = new System.Windows.Forms.Padding(1);
            this.radScrollablePanel3.Size = new System.Drawing.Size(205, 493);
            this.radScrollablePanel3.TabIndex = 3;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Telerik.Examples.WinControls.Properties.Resources.Calendar;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(279, 556);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // radPageViewPage6
            // 
// TODO: Code generation for '' failed because of Exception 'Invalid Primitive Type: System.IntPtr. Consider using CodeObjectCreateExpression.'.
            this.radPageViewPage6.Controls.Add(this.radScrollablePanel1);
            this.radPageViewPage6.Location = new System.Drawing.Point(5, 28);
            this.radPageViewPage6.Name = "radPageViewPage6";
            this.radPageViewPage6.Size = new System.Drawing.Size(205, 493);
            this.radPageViewPage6.Text = "Mail";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.pictureBox1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(203, 474);
            // 
            // 
            // 
            this.radScrollablePanel1.RootElement.Padding = new System.Windows.Forms.Padding(1);
            this.radScrollablePanel1.Size = new System.Drawing.Size(205, 493);
            this.radScrollablePanel1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Telerik.Examples.WinControls.Properties.Resources.Mail;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(278, 385);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1009, 519);
            this.radSplitContainer1.SplitterWidth = 4;
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.pageView);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(237, 519);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2645161F, 0F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-328, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radScheduler1);
            this.splitPanel2.Controls.Add(this.radSchedulerNavigator1);
            this.splitPanel2.Location = new System.Drawing.Point(241, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(768, 519);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2645161F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(328, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radScheduler1
            // 
            dateTimeInterval1.End = new System.DateTime(((long)(0)));
            dateTimeInterval1.Start = new System.DateTime(((long)(0)));
            this.radScheduler1.AccessibleInterval = dateTimeInterval1;
            this.radScheduler1.AppointmentTitleFormat = null;
            this.radScheduler1.Culture = new System.Globalization.CultureInfo("en-US");
            this.radScheduler1.DataSource = null;
            this.radScheduler1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScheduler1.GroupType = Telerik.WinControls.UI.GroupType.None;
            this.radScheduler1.HeaderFormat = "dd dddd";
            this.radScheduler1.Location = new System.Drawing.Point(0, 77);
            this.radScheduler1.Name = "radScheduler1";
            schedulerDailyPrintStyle1.AppointmentFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            schedulerDailyPrintStyle1.DateEndRange = new System.DateTime(2013, 3, 17, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.DateHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            schedulerDailyPrintStyle1.DateStartRange = new System.DateTime(2013, 3, 12, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.PageHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold);
            this.radScheduler1.PrintStyle = schedulerDailyPrintStyle1;
            this.radScheduler1.Size = new System.Drawing.Size(768, 442);
            this.radScheduler1.TabIndex = 1;
            this.radScheduler1.Text = "radScheduler1";
            // 
            // radSchedulerNavigator1
            // 
            this.radSchedulerNavigator1.AssociatedScheduler = null;
            this.radSchedulerNavigator1.DateFormat = "yyyy/MM/dd";
            this.radSchedulerNavigator1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radSchedulerNavigator1.Location = new System.Drawing.Point(0, 0);
            this.radSchedulerNavigator1.Name = "radSchedulerNavigator1";
            this.radSchedulerNavigator1.NavigationStepType = Telerik.WinControls.UI.NavigationStepTypes.Day;
            // 
            // 
            // 
            this.radSchedulerNavigator1.RootElement.StretchVertically = false;
            this.radSchedulerNavigator1.Size = new System.Drawing.Size(768, 77);
            this.radSchedulerNavigator1.TabIndex = 0;
            this.radSchedulerNavigator1.Text = "radSchedulerNavigator1";
            // 
            // radSchedulerReminder1
            // 
            this.radSchedulerReminder1.AssociatedScheduler = null;
            //this.radSchedulerReminder1.StartNotification = System.TimeSpan.Parse("00:15:00");

            this.radSchedulerReminder1.TimeInterval = 60000;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radSplitContainer1);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.Size = new System.Drawing.Size(1029, 519);
            this.Controls.SetChildIndex(this.radSplitContainer1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageView)).EndInit();
            this.pageView.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.radScrollablePanel2.PanelContainer.ResumeLayout(false);
            this.radScrollablePanel2.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel2)).EndInit();
            this.radScrollablePanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            this.radScrollablePanel5.PanelContainer.ResumeLayout(false);
            this.radScrollablePanel5.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel5)).EndInit();
            this.radScrollablePanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.radPageViewPage3.ResumeLayout(false);
            this.radScrollablePanel4.PanelContainer.ResumeLayout(false);
            this.radScrollablePanel4.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel4)).EndInit();
            this.radScrollablePanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.radPageViewPage4.ResumeLayout(false);
            this.radScrollablePanel3.PanelContainer.ResumeLayout(false);
            this.radScrollablePanel3.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel3)).EndInit();
            this.radScrollablePanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.radPageViewPage6.ResumeLayout(false);
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            this.radScrollablePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            this.splitPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScheduler1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSchedulerNavigator1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView pageView;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage4;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel2;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSchedulerNavigator radSchedulerNavigator1;
        private Telerik.WinControls.UI.RadScheduler radScheduler1;
        private Telerik.WinControls.UI.RadSchedulerReminder radSchedulerReminder1;
    }
}
