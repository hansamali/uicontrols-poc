﻿namespace Telerik.Examples.WinControls.PageView.StripView.Settings
{
    partial class Form1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.stripAlignCombo = new Telerik.WinControls.UI.RadDropDownList();
            this.itemAlignCombo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.fitModeCombo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.sizeModeCombo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.orientationCombo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.spacingSpin = new Telerik.WinControls.UI.RadSpinEditor();
            this.addPageButton = new Telerik.WinControls.UI.RadButton();
            this.multiLineFitCombo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stripAlignCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemAlignCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fitModeCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeModeCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orientationCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spacingSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addPageButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.multiLineFitCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.multiLineFitCombo);
            this.settingsPanel.Controls.Add(this.addPageButton);
            this.settingsPanel.Controls.Add(this.spacingSpin);
            this.settingsPanel.Controls.Add(this.radLabel7);
            this.settingsPanel.Controls.Add(this.radLabel6);
            this.settingsPanel.Controls.Add(this.orientationCombo);
            this.settingsPanel.Controls.Add(this.radLabel5);
            this.settingsPanel.Controls.Add(this.sizeModeCombo);
            this.settingsPanel.Controls.Add(this.radLabel4);
            this.settingsPanel.Controls.Add(this.fitModeCombo);
            this.settingsPanel.Controls.Add(this.radLabel3);
            this.settingsPanel.Controls.Add(this.itemAlignCombo);
            this.settingsPanel.Controls.Add(this.radLabel2);
            this.settingsPanel.Controls.Add(this.stripAlignCombo);
            this.settingsPanel.Controls.Add(this.radLabel1);
            this.settingsPanel.Location = new System.Drawing.Point(1002, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 524);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.stripAlignCombo, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.itemAlignCombo, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel3, 0);
            this.settingsPanel.Controls.SetChildIndex(this.fitModeCombo, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel4, 0);
            this.settingsPanel.Controls.SetChildIndex(this.sizeModeCombo, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel5, 0);
            this.settingsPanel.Controls.SetChildIndex(this.orientationCombo, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel6, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel7, 0);
            this.settingsPanel.Controls.SetChildIndex(this.spacingSpin, 0);
            this.settingsPanel.Controls.SetChildIndex(this.addPageButton, 0);
            this.settingsPanel.Controls.SetChildIndex(this.multiLineFitCombo, 0);
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(10, 37);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(86, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Strip Alignment:";
            // 
            // stripAlignCombo
            // 
            this.stripAlignCombo.AllowShowFocusCues = false;
            this.stripAlignCombo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.stripAlignCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.stripAlignCombo.Location = new System.Drawing.Point(10, 59);
            this.stripAlignCombo.Name = "stripAlignCombo";
            // 
            // 
            // 
            this.stripAlignCombo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.stripAlignCombo.Size = new System.Drawing.Size(180, 20);
            this.stripAlignCombo.TabIndex = 2;
            // 
            // itemAlignCombo
            // 
            this.itemAlignCombo.AllowShowFocusCues = false;
            this.itemAlignCombo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.itemAlignCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.itemAlignCombo.Location = new System.Drawing.Point(10, 107);
            this.itemAlignCombo.Name = "itemAlignCombo";
            // 
            // 
            // 
            this.itemAlignCombo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.itemAlignCombo.Size = new System.Drawing.Size(180, 20);
            this.itemAlignCombo.TabIndex = 4;
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(10, 85);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(85, 18);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "Item Alignment:";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(834, 654);
            this.radPageView1.TabIndex = 1;
            this.radPageView1.Text = "radPageView1";
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(112F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(813, 606);
            this.radPageViewPage1.Text = "radPageViewPage1";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(112F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(9, 33);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(574, 285);
            this.radPageViewPage2.Text = "radPageViewPage2";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.ItemSize = new System.Drawing.SizeF(112F, 28F);
            this.radPageViewPage3.Location = new System.Drawing.Point(9, 33);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(574, 285);
            this.radPageViewPage3.Text = "radPageViewPage3";
            // 
            // fitModeCombo
            // 
            this.fitModeCombo.AllowShowFocusCues = false;
            this.fitModeCombo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.fitModeCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.fitModeCombo.Location = new System.Drawing.Point(10, 155);
            this.fitModeCombo.Name = "fitModeCombo";
            // 
            // 
            // 
            this.fitModeCombo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fitModeCombo.Size = new System.Drawing.Size(180, 20);
            this.fitModeCombo.TabIndex = 6;
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(10, 133);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(78, 18);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "Item Fit Mode:";
            // 
            // sizeModeCombo
            // 
            this.sizeModeCombo.AllowShowFocusCues = false;
            this.sizeModeCombo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.sizeModeCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.sizeModeCombo.Location = new System.Drawing.Point(10, 251);
            this.sizeModeCombo.Name = "sizeModeCombo";
            // 
            // 
            // 
            this.sizeModeCombo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.sizeModeCombo.Size = new System.Drawing.Size(180, 20);
            this.sizeModeCombo.TabIndex = 8;
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel4.Location = new System.Drawing.Point(10, 229);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(86, 18);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Text = "Item Size Mode:";
            // 
            // orientationCombo
            // 
            this.orientationCombo.AllowShowFocusCues = false;
            this.orientationCombo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.orientationCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.orientationCombo.Location = new System.Drawing.Point(10, 299);
            this.orientationCombo.Name = "orientationCombo";
            // 
            // 
            // 
            this.orientationCombo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.orientationCombo.Size = new System.Drawing.Size(180, 20);
            this.orientationCombo.TabIndex = 10;
            // 
            // radLabel5
            // 
            this.radLabel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel5.Location = new System.Drawing.Point(10, 277);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(133, 18);
            this.radLabel5.TabIndex = 9;
            this.radLabel5.Text = "Item Content Orientation:";
            // 
            // radLabel6
            // 
            this.radLabel6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel6.Location = new System.Drawing.Point(10, 325);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(73, 18);
            this.radLabel6.TabIndex = 11;
            this.radLabel6.Text = "Item Spacing:";
            // 
            // spacingSpin
            // 
            this.spacingSpin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spacingSpin.Location = new System.Drawing.Point(10, 347);
            this.spacingSpin.Name = "spacingSpin";
            // 
            // 
            // 
            this.spacingSpin.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.spacingSpin.Size = new System.Drawing.Size(180, 20);
            this.spacingSpin.TabIndex = 12;
            this.spacingSpin.TabStop = false;
            // 
            // addPageButton
            // 
            this.addPageButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addPageButton.Location = new System.Drawing.Point(10, 404);
            this.addPageButton.Name = "addPageButton";
            this.addPageButton.Size = new System.Drawing.Size(180, 24);
            this.addPageButton.TabIndex = 13;
            this.addPageButton.Text = "Add Page";
            // 
            // multiLineFitCombo
            // 
            this.multiLineFitCombo.AllowShowFocusCues = false;
            this.multiLineFitCombo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.multiLineFitCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.multiLineFitCombo.Enabled = false;
            this.multiLineFitCombo.Location = new System.Drawing.Point(10, 203);
            this.multiLineFitCombo.Name = "multiLineFitCombo";
            // 
            // 
            // 
            this.multiLineFitCombo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.multiLineFitCombo.Size = new System.Drawing.Size(180, 20);
            this.multiLineFitCombo.TabIndex = 15;
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel7.Location = new System.Drawing.Point(10, 181);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(102, 18);
            this.radLabel7.TabIndex = 14;
            this.radLabel7.Text = "MultiLine Fit Mode:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPageView1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1573, 958);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radPageView1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stripAlignCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemAlignCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fitModeCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeModeCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orientationCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spacingSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addPageButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.multiLineFitCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList itemAlignCombo;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList stripAlignCombo;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private Telerik.WinControls.UI.RadDropDownList fitModeCombo;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadSpinEditor spacingSpin;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadDropDownList orientationCombo;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList sizeModeCombo;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadButton addPageButton;
        private Telerik.WinControls.UI.RadDropDownList multiLineFitCombo;
        private Telerik.WinControls.UI.RadLabel radLabel7;
    }
}
