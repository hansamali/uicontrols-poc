using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Drawing;
using Telerik.Examples.WinControls;

namespace Telerik.Examples.WinControls.PageView.StripView.ItemDragDrop
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}


		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn53 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn85 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn86 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn87 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn88 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn21 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn22 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn89 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn90 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn91 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn92 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn93 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn94 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn95 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewImageColumn gridViewImageColumn5 = new Telerik.WinControls.UI.GridViewImageColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn96 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn54 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn55 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn97 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn56 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn23 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn24 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn25 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn57 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn58 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn98 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn99 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn100 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn101 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn102 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn103 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn59 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn104 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn60 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn61 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn105 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn62 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn63 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn64 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn65 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn5 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.northwindDataSet = new Telerik.Examples.WinControls.DataSources.NorthwindDataSet();
            this.radGridView2 = new Telerik.WinControls.UI.RadGridView();
            this.employeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radGridView3 = new Telerik.WinControls.UI.RadGridView();
            this.ordersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radButtonClear = new Telerik.WinControls.UI.RadButton();
            this.textBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.productsTableAdapter = new Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.ProductsTableAdapter();
            this.employeesTableAdapter = new Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.EmployeesTableAdapter();
            this.ordersTableAdapter = new Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.OrdersTableAdapter();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.dragModeCombo = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            this.radPageViewPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dragModeCombo)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.dragModeCombo);
            this.settingsPanel.Controls.Add(this.radLabel1);
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(1093, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 549);
   
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.dragModeCombo, 0);
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "Products";
            this.productsBindingSource.DataSource = this.northwindDataSet;
            // 
            // northwindDataSet
            // 
            this.northwindDataSet.DataSetName = "NorthwindDataSet";
            this.northwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radGridView2
            // 
            this.radGridView2.BackColor = System.Drawing.Color.Transparent;
            this.radGridView2.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radGridView2.ForeColor = System.Drawing.Color.Black;
            this.radGridView2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView2.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewDecimalColumn53.DataType = typeof(int);
            gridViewDecimalColumn53.FieldName = "EmployeeID";
            gridViewDecimalColumn53.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn53.HeaderText = "EmployeeID";
            gridViewDecimalColumn53.IsAutoGenerated = true;
            gridViewDecimalColumn53.IsVisible = false;
            gridViewDecimalColumn53.Name = "EmployeeID";
            gridViewTextBoxColumn85.FieldName = "LastName";
            gridViewTextBoxColumn85.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn85.HeaderText = "LastName";
            gridViewTextBoxColumn85.IsAutoGenerated = true;
            gridViewTextBoxColumn85.IsVisible = false;
            gridViewTextBoxColumn85.Name = "LastName";
            gridViewTextBoxColumn86.FieldName = "FirstName";
            gridViewTextBoxColumn86.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn86.HeaderText = "FirstName";
            gridViewTextBoxColumn86.IsAutoGenerated = true;
            gridViewTextBoxColumn86.Name = "FirstName";
            gridViewTextBoxColumn87.FieldName = "Title";
            gridViewTextBoxColumn87.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn87.HeaderText = "Title";
            gridViewTextBoxColumn87.IsAutoGenerated = true;
            gridViewTextBoxColumn87.Name = "Title";
            gridViewTextBoxColumn88.FieldName = "TitleOfCourtesy";
            gridViewTextBoxColumn88.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn88.HeaderText = "TitleOfCourtesy";
            gridViewTextBoxColumn88.IsAutoGenerated = true;
            gridViewTextBoxColumn88.IsVisible = false;
            gridViewTextBoxColumn88.Name = "TitleOfCourtesy";
            gridViewDateTimeColumn21.FieldName = "BirthDate";
            gridViewDateTimeColumn21.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDateTimeColumn21.HeaderText = "BirthDate";
            gridViewDateTimeColumn21.IsAutoGenerated = true;
            gridViewDateTimeColumn21.IsVisible = false;
            gridViewDateTimeColumn21.Name = "BirthDate";
            gridViewDateTimeColumn22.FieldName = "HireDate";
            gridViewDateTimeColumn22.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDateTimeColumn22.HeaderText = "HireDate";
            gridViewDateTimeColumn22.IsAutoGenerated = true;
            gridViewDateTimeColumn22.IsVisible = false;
            gridViewDateTimeColumn22.Name = "HireDate";
            gridViewTextBoxColumn89.FieldName = "Address";
            gridViewTextBoxColumn89.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn89.HeaderText = "Address";
            gridViewTextBoxColumn89.IsAutoGenerated = true;
            gridViewTextBoxColumn89.Name = "Address";
            gridViewTextBoxColumn90.FieldName = "City";
            gridViewTextBoxColumn90.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn90.HeaderText = "City";
            gridViewTextBoxColumn90.IsAutoGenerated = true;
            gridViewTextBoxColumn90.IsVisible = false;
            gridViewTextBoxColumn90.Name = "City";
            gridViewTextBoxColumn91.FieldName = "Region";
            gridViewTextBoxColumn91.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn91.HeaderText = "Region";
            gridViewTextBoxColumn91.IsAutoGenerated = true;
            gridViewTextBoxColumn91.IsVisible = false;
            gridViewTextBoxColumn91.Name = "Region";
            gridViewTextBoxColumn92.FieldName = "PostalCode";
            gridViewTextBoxColumn92.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn92.HeaderText = "PostalCode";
            gridViewTextBoxColumn92.IsAutoGenerated = true;
            gridViewTextBoxColumn92.IsVisible = false;
            gridViewTextBoxColumn92.Name = "PostalCode";
            gridViewTextBoxColumn93.FieldName = "Country";
            gridViewTextBoxColumn93.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn93.HeaderText = "Country";
            gridViewTextBoxColumn93.IsAutoGenerated = true;
            gridViewTextBoxColumn93.IsVisible = false;
            gridViewTextBoxColumn93.Name = "Country";
            gridViewTextBoxColumn94.FieldName = "HomePhone";
            gridViewTextBoxColumn94.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn94.HeaderText = "HomePhone";
            gridViewTextBoxColumn94.IsAutoGenerated = true;
            gridViewTextBoxColumn94.IsVisible = false;
            gridViewTextBoxColumn94.Name = "HomePhone";
            gridViewTextBoxColumn95.FieldName = "Extension";
            gridViewTextBoxColumn95.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn95.HeaderText = "Extension";
            gridViewTextBoxColumn95.IsAutoGenerated = true;
            gridViewTextBoxColumn95.IsVisible = false;
            gridViewTextBoxColumn95.Name = "Extension";
            gridViewImageColumn5.DataType = typeof(byte[]);
            gridViewImageColumn5.FieldName = "Photo";
            gridViewImageColumn5.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewImageColumn5.HeaderText = "Photo";
            gridViewImageColumn5.IsAutoGenerated = true;
            gridViewImageColumn5.IsVisible = false;
            gridViewImageColumn5.Name = "Photo";
            gridViewTextBoxColumn96.FieldName = "Notes";
            gridViewTextBoxColumn96.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn96.HeaderText = "Notes";
            gridViewTextBoxColumn96.IsAutoGenerated = true;
            gridViewTextBoxColumn96.IsVisible = false;
            gridViewTextBoxColumn96.Name = "Notes";
            gridViewDecimalColumn54.DataType = typeof(int);
            gridViewDecimalColumn54.FieldName = "ReportsTo";
            gridViewDecimalColumn54.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn54.HeaderText = "ReportsTo";
            gridViewDecimalColumn54.IsAutoGenerated = true;
            gridViewDecimalColumn54.IsVisible = false;
            gridViewDecimalColumn54.Name = "ReportsTo";
            this.radGridView2.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn53,
            gridViewTextBoxColumn85,
            gridViewTextBoxColumn86,
            gridViewTextBoxColumn87,
            gridViewTextBoxColumn88,
            gridViewDateTimeColumn21,
            gridViewDateTimeColumn22,
            gridViewTextBoxColumn89,
            gridViewTextBoxColumn90,
            gridViewTextBoxColumn91,
            gridViewTextBoxColumn92,
            gridViewTextBoxColumn93,
            gridViewTextBoxColumn94,
            gridViewTextBoxColumn95,
            gridViewImageColumn5,
            gridViewTextBoxColumn96,
            gridViewDecimalColumn54});
            this.radGridView2.MasterTemplate.DataSource = this.employeesBindingSource;
            this.radGridView2.MasterTemplate.EnableGrouping = false;
            this.radGridView2.Name = "radGridView2";
            this.radGridView2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radGridView2.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView2.ShowGroupPanel = false;
            this.radGridView2.Size = new System.Drawing.Size(607, 418);
            this.radGridView2.TabIndex = 0;
            this.radGridView2.Text = "radGridViewPreview";
            // 
            // employeesBindingSource
            // 
            this.employeesBindingSource.DataMember = "Employees";
            this.employeesBindingSource.DataSource = this.northwindDataSet;
            // 
            // radGridView3
            // 
            this.radGridView3.BackColor = System.Drawing.Color.Transparent;
            this.radGridView3.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radGridView3.ForeColor = System.Drawing.Color.Black;
            this.radGridView3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView3.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewDecimalColumn55.DataType = typeof(int);
            gridViewDecimalColumn55.FieldName = "OrderID";
            gridViewDecimalColumn55.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn55.HeaderText = "OrderID";
            gridViewDecimalColumn55.IsAutoGenerated = true;
            gridViewDecimalColumn55.Name = "OrderID";
            gridViewDecimalColumn55.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn97.FieldName = "CustomerID";
            gridViewTextBoxColumn97.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn97.HeaderText = "CustomerID";
            gridViewTextBoxColumn97.IsAutoGenerated = true;
            gridViewTextBoxColumn97.IsVisible = false;
            gridViewTextBoxColumn97.Name = "CustomerID";
            gridViewDecimalColumn56.DataType = typeof(int);
            gridViewDecimalColumn56.FieldName = "EmployeeID";
            gridViewDecimalColumn56.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn56.HeaderText = "EmployeeID";
            gridViewDecimalColumn56.IsAutoGenerated = true;
            gridViewDecimalColumn56.IsVisible = false;
            gridViewDecimalColumn56.Name = "EmployeeID";
            gridViewDateTimeColumn23.FieldName = "OrderDate";
            gridViewDateTimeColumn23.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDateTimeColumn23.HeaderText = "OrderDate";
            gridViewDateTimeColumn23.IsAutoGenerated = true;
            gridViewDateTimeColumn23.Name = "OrderDate";
            gridViewDateTimeColumn24.FieldName = "RequiredDate";
            gridViewDateTimeColumn24.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDateTimeColumn24.HeaderText = "RequiredDate";
            gridViewDateTimeColumn24.IsAutoGenerated = true;
            gridViewDateTimeColumn24.IsVisible = false;
            gridViewDateTimeColumn24.Name = "RequiredDate";
            gridViewDateTimeColumn25.FieldName = "ShippedDate";
            gridViewDateTimeColumn25.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDateTimeColumn25.HeaderText = "ShippedDate";
            gridViewDateTimeColumn25.IsAutoGenerated = true;
            gridViewDateTimeColumn25.IsVisible = false;
            gridViewDateTimeColumn25.Name = "ShippedDate";
            gridViewDecimalColumn57.DataType = typeof(int);
            gridViewDecimalColumn57.FieldName = "ShipVia";
            gridViewDecimalColumn57.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn57.HeaderText = "ShipVia";
            gridViewDecimalColumn57.IsAutoGenerated = true;
            gridViewDecimalColumn57.IsVisible = false;
            gridViewDecimalColumn57.Name = "ShipVia";
            gridViewDecimalColumn58.FieldName = "Freight";
            gridViewDecimalColumn58.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn58.HeaderText = "Freight";
            gridViewDecimalColumn58.IsAutoGenerated = true;
            gridViewDecimalColumn58.IsVisible = false;
            gridViewDecimalColumn58.Name = "Freight";
            gridViewTextBoxColumn98.FieldName = "ShipName";
            gridViewTextBoxColumn98.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn98.HeaderText = "ShipName";
            gridViewTextBoxColumn98.IsAutoGenerated = true;
            gridViewTextBoxColumn98.Name = "ShipName";
            gridViewTextBoxColumn99.FieldName = "ShipAddress";
            gridViewTextBoxColumn99.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn99.HeaderText = "ShipAddress";
            gridViewTextBoxColumn99.IsAutoGenerated = true;
            gridViewTextBoxColumn99.IsVisible = false;
            gridViewTextBoxColumn99.Name = "ShipAddress";
            gridViewTextBoxColumn100.FieldName = "ShipCity";
            gridViewTextBoxColumn100.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn100.HeaderText = "ShipCity";
            gridViewTextBoxColumn100.IsAutoGenerated = true;
            gridViewTextBoxColumn100.IsVisible = false;
            gridViewTextBoxColumn100.Name = "ShipCity";
            gridViewTextBoxColumn101.FieldName = "ShipRegion";
            gridViewTextBoxColumn101.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn101.HeaderText = "ShipRegion";
            gridViewTextBoxColumn101.IsAutoGenerated = true;
            gridViewTextBoxColumn101.IsVisible = false;
            gridViewTextBoxColumn101.Name = "ShipRegion";
            gridViewTextBoxColumn102.FieldName = "ShipPostalCode";
            gridViewTextBoxColumn102.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn102.HeaderText = "ShipPostalCode";
            gridViewTextBoxColumn102.IsAutoGenerated = true;
            gridViewTextBoxColumn102.IsVisible = false;
            gridViewTextBoxColumn102.Name = "ShipPostalCode";
            gridViewTextBoxColumn103.FieldName = "ShipCountry";
            gridViewTextBoxColumn103.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn103.HeaderText = "ShipCountry";
            gridViewTextBoxColumn103.IsAutoGenerated = true;
            gridViewTextBoxColumn103.IsVisible = false;
            gridViewTextBoxColumn103.Name = "ShipCountry";
            this.radGridView3.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn55,
            gridViewTextBoxColumn97,
            gridViewDecimalColumn56,
            gridViewDateTimeColumn23,
            gridViewDateTimeColumn24,
            gridViewDateTimeColumn25,
            gridViewDecimalColumn57,
            gridViewDecimalColumn58,
            gridViewTextBoxColumn98,
            gridViewTextBoxColumn99,
            gridViewTextBoxColumn100,
            gridViewTextBoxColumn101,
            gridViewTextBoxColumn102,
            gridViewTextBoxColumn103});
            this.radGridView3.MasterTemplate.DataSource = this.ordersBindingSource;
            this.radGridView3.MasterTemplate.EnableGrouping = false;
            this.radGridView3.Name = "radGridView3";
            this.radGridView3.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radGridView3.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView3.ShowGroupPanel = false;
            this.radGridView3.Size = new System.Drawing.Size(607, 418);
            this.radGridView3.TabIndex = 1;
            this.radGridView3.Text = "radGridViewPreview";
            // 
            // ordersBindingSource
            // 
            this.ordersBindingSource.DataMember = "Orders";
            this.ordersBindingSource.DataSource = this.northwindDataSet;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.radButtonClear);
            this.radGroupBox1.Controls.Add(this.textBox1);
            this.radGroupBox1.FooterText = "";
            this.radGroupBox1.HeaderText = "Events";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 91);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox1.Size = new System.Drawing.Size(180, 235);
            this.radGroupBox1.TabIndex = 9;
            this.radGroupBox1.Text = "Events";
            // 
            // radButtonClear
            // 
            this.radButtonClear.Location = new System.Drawing.Point(23, 197);
            this.radButtonClear.Name = "radButtonClear";
            this.radButtonClear.Size = new System.Drawing.Size(134, 23);
            this.radButtonClear.TabIndex = 10;
            this.radButtonClear.Text = "Clear";
       
            // 
            // textBox1
            // 
            this.textBox1.AutoSize = false;
            this.textBox1.Location = new System.Drawing.Point(23, 22);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(134, 169);
            this.textBox1.TabIndex = 0;
            this.textBox1.TabStop = false;

            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // employeesTableAdapter
            // 
            this.employeesTableAdapter.ClearBeforeFill = true;
            // 
            // ordersTableAdapter
            // 
            this.ordersTableAdapter.ClearBeforeFill = true;
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(640, 466);
            this.radPageView1.TabIndex = 9;
            this.radPageView1.Text = "radPageView1";
            // 
            // radPageViewPage1
            // 
// TODO: Code generation for '' failed because of Exception 'Invalid Primitive Type: System.IntPtr. Consider using CodeObjectCreateExpression.'.
            this.radPageViewPage1.Controls.Add(this.radGridView1);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(619, 418);
            this.radPageViewPage1.Text = "Products";
            // 
            // radGridView1
            // 
            this.radGridView1.BackColor = System.Drawing.Color.Transparent;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radGridView1.ForeColor = System.Drawing.Color.Black;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            // 
            // radGridView1
            // 
            gridViewDecimalColumn59.DataType = typeof(int);
            gridViewDecimalColumn59.FieldName = "ProductID";
            gridViewDecimalColumn59.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn59.HeaderText = "ProductID";
            gridViewDecimalColumn59.IsAutoGenerated = true;
            gridViewDecimalColumn59.IsVisible = false;
            gridViewDecimalColumn59.Name = "ProductID";
            gridViewTextBoxColumn104.FieldName = "ProductName";
            gridViewTextBoxColumn104.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn104.HeaderText = "ProductName";
            gridViewTextBoxColumn104.IsAutoGenerated = true;
            gridViewTextBoxColumn104.Name = "ProductName";
            gridViewDecimalColumn60.DataType = typeof(int);
            gridViewDecimalColumn60.FieldName = "SupplierID";
            gridViewDecimalColumn60.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn60.HeaderText = "SupplierID";
            gridViewDecimalColumn60.IsAutoGenerated = true;
            gridViewDecimalColumn60.IsVisible = false;
            gridViewDecimalColumn60.Name = "SupplierID";
            gridViewDecimalColumn61.DataType = typeof(int);
            gridViewDecimalColumn61.FieldName = "CategoryID";
            gridViewDecimalColumn61.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn61.HeaderText = "CategoryID";
            gridViewDecimalColumn61.IsAutoGenerated = true;
            gridViewDecimalColumn61.IsVisible = false;
            gridViewDecimalColumn61.Name = "CategoryID";
            gridViewTextBoxColumn105.FieldName = "QuantityPerUnit";
            gridViewTextBoxColumn105.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewTextBoxColumn105.HeaderText = "QuantityPerUnit";
            gridViewTextBoxColumn105.IsAutoGenerated = true;
            gridViewTextBoxColumn105.Name = "QuantityPerUnit";
            gridViewDecimalColumn62.FieldName = "UnitPrice";
            gridViewDecimalColumn62.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn62.HeaderText = "UnitPrice";
            gridViewDecimalColumn62.IsAutoGenerated = true;
            gridViewDecimalColumn62.Name = "UnitPrice";
            gridViewDecimalColumn63.DataType = typeof(short);
            gridViewDecimalColumn63.FieldName = "UnitsInStock";
            gridViewDecimalColumn63.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn63.HeaderText = "UnitsInStock";
            gridViewDecimalColumn63.IsAutoGenerated = true;
            gridViewDecimalColumn63.IsVisible = false;
            gridViewDecimalColumn63.Name = "UnitsInStock";
            gridViewDecimalColumn64.DataType = typeof(short);
            gridViewDecimalColumn64.FieldName = "UnitsOnOrder";
            gridViewDecimalColumn64.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn64.HeaderText = "UnitsOnOrder";
            gridViewDecimalColumn64.IsAutoGenerated = true;
            gridViewDecimalColumn64.IsVisible = false;
            gridViewDecimalColumn64.Name = "UnitsOnOrder";
            gridViewDecimalColumn65.DataType = typeof(short);
            gridViewDecimalColumn65.FieldName = "ReorderLevel";
            gridViewDecimalColumn65.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewDecimalColumn65.HeaderText = "ReorderLevel";
            gridViewDecimalColumn65.IsAutoGenerated = true;
            gridViewDecimalColumn65.IsVisible = false;
            gridViewDecimalColumn65.Name = "ReorderLevel";
            gridViewCheckBoxColumn5.FieldName = "Discontinued";
            gridViewCheckBoxColumn5.FormatInfo = new System.Globalization.CultureInfo("");
            gridViewCheckBoxColumn5.HeaderText = "Discontinued";
            gridViewCheckBoxColumn5.IsAutoGenerated = true;
            gridViewCheckBoxColumn5.IsVisible = false;
            gridViewCheckBoxColumn5.Name = "Discontinued";
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn59,
            gridViewTextBoxColumn104,
            gridViewDecimalColumn60,
            gridViewDecimalColumn61,
            gridViewTextBoxColumn105,
            gridViewDecimalColumn62,
            gridViewDecimalColumn63,
            gridViewDecimalColumn64,
            gridViewDecimalColumn65,
            gridViewCheckBoxColumn5});
            this.radGridView1.MasterTemplate.DataSource = this.productsBindingSource;
            this.radGridView1.MasterTemplate.EnableGrouping = false;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radGridView1.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.ShowGroupPanel = false;
            this.radGridView1.Size = new System.Drawing.Size(619, 418);
            this.radGridView1.TabIndex = 0;
            this.radGridView1.Text = "radGridViewPreview";
            // 
            // radPageViewPage2
            // 
// TODO: Code generation for '' failed because of Exception 'Invalid Primitive Type: System.IntPtr. Consider using CodeObjectCreateExpression.'.
            this.radPageViewPage2.Controls.Add(this.radGridView2);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(607, 418);
            this.radPageViewPage2.Text = "Company Info";
            // 
            // radPageViewPage3
            // 
// TODO: Code generation for '' failed because of Exception 'Invalid Primitive Type: System.IntPtr. Consider using CodeObjectCreateExpression.'.
            this.radPageViewPage3.Controls.Add(this.radGridView3);
            this.radPageViewPage3.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(607, 418);
            this.radPageViewPage3.Text = "Orders";
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(10, 37);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(91, 18);
            this.radLabel1.TabIndex = 10;
            this.radLabel1.Text = "Item Drag Mode:";
            // 
            // dragModeCombo
            // 
            this.dragModeCombo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dragModeCombo.DropDownAnimationEnabled = false;
            this.dragModeCombo.Location = new System.Drawing.Point(10, 59);
            this.dragModeCombo.Name = "dragModeCombo";
            this.dragModeCombo.Size = new System.Drawing.Size(180, 20);
            this.dragModeCombo.TabIndex = 11;
            this.dragModeCombo.Text = "radDropDownList1";
            this.dragModeCombo.DropDownStyle = RadDropDownStyle.DropDownList;
           
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPageView1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1029, 672);
            this.Controls.SetChildIndex(this.radPageView1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButtonClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            this.radPageViewPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dragModeCombo)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion
        
        private RadGroupBox radGroupBox1;
        private RadTextBox textBox1;
        private RadButton radButtonClear;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private Telerik.Examples.WinControls.DataSources.NorthwindDataSet northwindDataSet;
        private RadGridView radGridView2;
        private Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.ProductsTableAdapter productsTableAdapter;
        private System.Windows.Forms.BindingSource employeesBindingSource;
        private RadGridView radGridView3;
        private System.Windows.Forms.BindingSource ordersBindingSource;
        private Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.EmployeesTableAdapter employeesTableAdapter;
        private Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.OrdersTableAdapter ordersTableAdapter;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private RadPageViewPage radPageViewPage1;
        private RadPageViewPage radPageViewPage2;
        private RadPageViewPage radPageViewPage3;
        private RadGridView radGridView1;
        private RadDropDownList dragModeCombo;
        private RadLabel radLabel1;
	}
}
