﻿namespace Telerik.Examples.WinControls.CommandBar.TextPad
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.radCommandBarLineElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.radCommandBarButtonItem1 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarButtonItem2 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarButtonItem3 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarButtonItem4 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarButtonItem5 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarSeparatorItem1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCommandBarButtonItem6 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarButtonItem7 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarButtonItem8 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarSeparatorItem2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCommandBarButtonItem9 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarButtonItem10 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarLineElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.radCommandBarToggleButtonItem1 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCommandBarToggleButtonItem2 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCommandBarToggleButtonItem3 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCommandBarSeparatorItem3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCommandBarDropDownListItem1 = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.radCommandBarDropDownListItem2 = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.radCommandBarSeparatorItem4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCommandBarToggleButtonItem4 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCommandBarToggleButtonItem5 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCommandBarToggleButtonItem6 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCommandBarToggleButtonItem7 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCommandBarSeparatorItem5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCommandBarToggleButtonItem8 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCommandBarToggleButtonItem9 = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.settingsPanel.BackColor = System.Drawing.Color.Transparent;
            this.settingsPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.settingsPanel.Location = new System.Drawing.Point(884, 1);
            this.settingsPanel.Size = new System.Drawing.Size(327, 635);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Telerik.Examples.WinControls.Properties.Resources.WordExample_bg;
            this.pictureBox1.Location = new System.Drawing.Point(-5, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(642, 339);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Location = new System.Drawing.Point(4, 56);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.radCommandBarLineElement1,
            this.radCommandBarLineElement2});
            this.radCommandBar1.Size = new System.Drawing.Size(624, 60);
            this.radCommandBar1.TabIndex = 1;
            this.radCommandBar1.Text = "radCommandBar1";
            this.radCommandBar1.ThemeName = "ControlDefault";
            // 
            // radCommandBarLineElement1
            // 
            this.radCommandBarLineElement1.BorderLeftShadowColor = System.Drawing.Color.Empty;
            this.radCommandBarLineElement1.DisplayName = null;
            this.radCommandBarLineElement1.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.radCommandBarStripElement1});
            this.radCommandBarLineElement1.Text = "";
            // 
            // radCommandBarStripElement1
            // 
            this.radCommandBarStripElement1.DisplayName = "Commands Strip";
            this.radCommandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.radCommandBarButtonItem1,
            this.radCommandBarButtonItem2,
            this.radCommandBarButtonItem3,
            this.radCommandBarButtonItem4,
            this.radCommandBarButtonItem5,
            this.radCommandBarSeparatorItem1,
            this.radCommandBarButtonItem6,
            this.radCommandBarButtonItem7,
            this.radCommandBarButtonItem8,
            this.radCommandBarSeparatorItem2,
            this.radCommandBarButtonItem9,
            this.radCommandBarButtonItem10});
            this.radCommandBarStripElement1.Name = "radCommandBarStripElement1";
            this.radCommandBarStripElement1.Text = "";
            // 
            // radCommandBarButtonItem1
            // 
            this.radCommandBarButtonItem1.AccessibleDescription = "radCommandBarButtonItem1";
            this.radCommandBarButtonItem1.AccessibleName = "radCommandBarButtonItem1";
            this.radCommandBarButtonItem1.DisplayName = "New File Button";
            this.radCommandBarButtonItem1.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem1.Image")));
            this.radCommandBarButtonItem1.Name = "radCommandBarButtonItem1";
            this.radCommandBarButtonItem1.Text = "radCommandBarButtonItem1";
            this.radCommandBarButtonItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarButtonItem2
            // 
            this.radCommandBarButtonItem2.AccessibleDescription = "radCommandBarButtonItem2";
            this.radCommandBarButtonItem2.AccessibleName = "radCommandBarButtonItem2";
            this.radCommandBarButtonItem2.DisplayName = "Open File Button";
            this.radCommandBarButtonItem2.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem2.Image")));
            this.radCommandBarButtonItem2.Name = "radCommandBarButtonItem2";
            this.radCommandBarButtonItem2.Text = "radCommandBarButtonItem2";
            this.radCommandBarButtonItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarButtonItem3
            // 
            this.radCommandBarButtonItem3.AccessibleDescription = "radCommandBarButtonItem3";
            this.radCommandBarButtonItem3.AccessibleName = "radCommandBarButtonItem3";
            this.radCommandBarButtonItem3.DisplayName = "Save File Button";
            this.radCommandBarButtonItem3.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem3.Image")));
            this.radCommandBarButtonItem3.Name = "radCommandBarButtonItem3";
            this.radCommandBarButtonItem3.Text = "radCommandBarButtonItem3";
            this.radCommandBarButtonItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarButtonItem4
            // 
            this.radCommandBarButtonItem4.AccessibleDescription = "radCommandBarButtonItem4";
            this.radCommandBarButtonItem4.AccessibleName = "radCommandBarButtonItem4";
            this.radCommandBarButtonItem4.DisplayName = "Undo Button";
            this.radCommandBarButtonItem4.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem4.Image")));
            this.radCommandBarButtonItem4.Name = "radCommandBarButtonItem4";
            this.radCommandBarButtonItem4.Text = "radCommandBarButtonItem4";
            this.radCommandBarButtonItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarButtonItem5
            // 
            this.radCommandBarButtonItem5.AccessibleDescription = "radCommandBarButtonItem5";
            this.radCommandBarButtonItem5.AccessibleName = "radCommandBarButtonItem5";
            this.radCommandBarButtonItem5.DisplayName = "Redo Button";
            this.radCommandBarButtonItem5.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem5.Image")));
            this.radCommandBarButtonItem5.Name = "radCommandBarButtonItem5";
            this.radCommandBarButtonItem5.Text = "radCommandBarButtonItem5";
            this.radCommandBarButtonItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarSeparatorItem1
            // 
            this.radCommandBarSeparatorItem1.DisplayName = "Separator";
            this.radCommandBarSeparatorItem1.Name = "radCommandBarSeparatorItem1";
            this.radCommandBarSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radCommandBarSeparatorItem1.VisibleInOverflowMenu = false;
            // 
            // radCommandBarButtonItem6
            // 
            this.radCommandBarButtonItem6.AccessibleDescription = "radCommandBarButtonItem6";
            this.radCommandBarButtonItem6.AccessibleName = "radCommandBarButtonItem6";
            this.radCommandBarButtonItem6.DisplayName = "Cut Button";
            this.radCommandBarButtonItem6.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem6.Image")));
            this.radCommandBarButtonItem6.Name = "radCommandBarButtonItem6";
            this.radCommandBarButtonItem6.Text = "radCommandBarButtonItem6";
            this.radCommandBarButtonItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarButtonItem7
            // 
            this.radCommandBarButtonItem7.AccessibleDescription = "radCommandBarButtonItem7";
            this.radCommandBarButtonItem7.AccessibleName = "radCommandBarButtonItem7";
            this.radCommandBarButtonItem7.DisplayName = "Copy Button";
            this.radCommandBarButtonItem7.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem7.Image")));
            this.radCommandBarButtonItem7.Name = "radCommandBarButtonItem7";
            this.radCommandBarButtonItem7.Text = "radCommandBarButtonItem7";
            this.radCommandBarButtonItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarButtonItem8
            // 
            this.radCommandBarButtonItem8.AccessibleDescription = "radCommandBarButtonItem8";
            this.radCommandBarButtonItem8.AccessibleName = "radCommandBarButtonItem8";
            this.radCommandBarButtonItem8.DisplayName = "Paste Button";
            this.radCommandBarButtonItem8.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem8.Image")));
            this.radCommandBarButtonItem8.Name = "radCommandBarButtonItem8";
            this.radCommandBarButtonItem8.Text = "radCommandBarButtonItem8";
            this.radCommandBarButtonItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarSeparatorItem2
            // 
            this.radCommandBarSeparatorItem2.DisplayName = "Separator";
            this.radCommandBarSeparatorItem2.Name = "radCommandBarSeparatorItem2";
            this.radCommandBarSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radCommandBarSeparatorItem2.VisibleInOverflowMenu = false;
            // 
            // radCommandBarButtonItem9
            // 
            this.radCommandBarButtonItem9.AccessibleDescription = "radCommandBarButtonItem9";
            this.radCommandBarButtonItem9.AccessibleName = "radCommandBarButtonItem9";
            this.radCommandBarButtonItem9.DisplayName = "Print Button";
            this.radCommandBarButtonItem9.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem9.Image")));
            this.radCommandBarButtonItem9.Name = "radCommandBarButtonItem9";
            this.radCommandBarButtonItem9.Text = "radCommandBarButtonItem9";
            this.radCommandBarButtonItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarButtonItem10
            // 
            this.radCommandBarButtonItem10.AccessibleDescription = "radCommandBarButtonItem10";
            this.radCommandBarButtonItem10.AccessibleName = "radCommandBarButtonItem10";
            this.radCommandBarButtonItem10.DisplayName = "Print Preview Button";
            this.radCommandBarButtonItem10.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarButtonItem10.Image")));
            this.radCommandBarButtonItem10.Name = "radCommandBarButtonItem10";
            this.radCommandBarButtonItem10.Text = "radCommandBarButtonItem10";
            this.radCommandBarButtonItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarLineElement2
            // 
            this.radCommandBarLineElement2.DisplayName = null;
            this.radCommandBarLineElement2.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.radCommandBarStripElement2});
            // 
            // radCommandBarStripElement2
            // 
            this.radCommandBarStripElement2.DisplayName = "Text Style Strip";
            this.radCommandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.radCommandBarToggleButtonItem1,
            this.radCommandBarToggleButtonItem2,
            this.radCommandBarToggleButtonItem3,
            this.radCommandBarSeparatorItem3,
            this.radCommandBarDropDownListItem1,
            this.radCommandBarDropDownListItem2,
            this.radCommandBarSeparatorItem4,
            this.radCommandBarToggleButtonItem4,
            this.radCommandBarToggleButtonItem5,
            this.radCommandBarToggleButtonItem6,
            this.radCommandBarToggleButtonItem7,
            this.radCommandBarSeparatorItem5,
            this.radCommandBarToggleButtonItem8,
            this.radCommandBarToggleButtonItem9});
            this.radCommandBarStripElement2.Name = "radCommandBarStripElement2";
            this.radCommandBarStripElement2.Text = "";
            // 
            // radCommandBarToggleButtonItem1
            // 
            this.radCommandBarToggleButtonItem1.AccessibleDescription = "radCommandBarToggleButtonItem1";
            this.radCommandBarToggleButtonItem1.AccessibleName = "radCommandBarToggleButtonItem1";
            this.radCommandBarToggleButtonItem1.DisplayName = "Bold Toggle Button";
            this.radCommandBarToggleButtonItem1.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem1.Image")));
            this.radCommandBarToggleButtonItem1.Name = "radCommandBarToggleButtonItem1";
            this.radCommandBarToggleButtonItem1.Text = "radCommandBarToggleButtonItem1";
            this.radCommandBarToggleButtonItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarToggleButtonItem2
            // 
            this.radCommandBarToggleButtonItem2.AccessibleDescription = "radCommandBarToggleButtonItem2";
            this.radCommandBarToggleButtonItem2.AccessibleName = "radCommandBarToggleButtonItem2";
            this.radCommandBarToggleButtonItem2.DisplayName = "Italic Toggle Button";
            this.radCommandBarToggleButtonItem2.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem2.Image")));
            this.radCommandBarToggleButtonItem2.Name = "radCommandBarToggleButtonItem2";
            this.radCommandBarToggleButtonItem2.Text = "radCommandBarToggleButtonItem2";
            this.radCommandBarToggleButtonItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarToggleButtonItem3
            // 
            this.radCommandBarToggleButtonItem3.AccessibleDescription = "radCommandBarToggleButtonItem3";
            this.radCommandBarToggleButtonItem3.AccessibleName = "radCommandBarToggleButtonItem3";
            this.radCommandBarToggleButtonItem3.DisplayName = "Underline Toggle Button";
            this.radCommandBarToggleButtonItem3.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem3.Image")));
            this.radCommandBarToggleButtonItem3.Name = "radCommandBarToggleButtonItem3";
            this.radCommandBarToggleButtonItem3.Text = "radCommandBarToggleButtonItem3";
            this.radCommandBarToggleButtonItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarSeparatorItem3
            // 
            this.radCommandBarSeparatorItem3.DisplayName = "Separator";
            this.radCommandBarSeparatorItem3.Name = "radCommandBarSeparatorItem3";
            this.radCommandBarSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radCommandBarSeparatorItem3.VisibleInOverflowMenu = false;
            // 
            // radCommandBarDropDownListItem1
            // 
            this.radCommandBarDropDownListItem1.DisplayName = "Font Family Dropdown";
            this.radCommandBarDropDownListItem1.DropDownAnimationEnabled = true;
            radListDataItem1.Text = "Arial";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Tahoma";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "Verdana";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "Times New Roman";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "Segoe UI";
            radListDataItem5.TextWrap = true;
            this.radCommandBarDropDownListItem1.Items.Add(radListDataItem1);
            this.radCommandBarDropDownListItem1.Items.Add(radListDataItem2);
            this.radCommandBarDropDownListItem1.Items.Add(radListDataItem3);
            this.radCommandBarDropDownListItem1.Items.Add(radListDataItem4);
            this.radCommandBarDropDownListItem1.Items.Add(radListDataItem5);
            this.radCommandBarDropDownListItem1.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.radCommandBarDropDownListItem1.MaxDropDownItems = 0;
            this.radCommandBarDropDownListItem1.Name = "radCommandBarDropDownListItem1";
            this.radCommandBarDropDownListItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarDropDownListItem2
            // 
            this.radCommandBarDropDownListItem2.DisplayName = "Font Size Dropdown";
            this.radCommandBarDropDownListItem2.DropDownAnimationEnabled = true;
            radListDataItem6.Text = "8";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "10";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "12";
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "16";
            radListDataItem9.TextWrap = true;
            radListDataItem10.Text = "18";
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "22";
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "24";
            radListDataItem12.TextWrap = true;
            this.radCommandBarDropDownListItem2.Items.Add(radListDataItem6);
            this.radCommandBarDropDownListItem2.Items.Add(radListDataItem7);
            this.radCommandBarDropDownListItem2.Items.Add(radListDataItem8);
            this.radCommandBarDropDownListItem2.Items.Add(radListDataItem9);
            this.radCommandBarDropDownListItem2.Items.Add(radListDataItem10);
            this.radCommandBarDropDownListItem2.Items.Add(radListDataItem11);
            this.radCommandBarDropDownListItem2.Items.Add(radListDataItem12);
            this.radCommandBarDropDownListItem2.MaxDropDownItems = 0;
            this.radCommandBarDropDownListItem2.Name = "radCommandBarDropDownListItem2";
            this.radCommandBarDropDownListItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarSeparatorItem4
            // 
            this.radCommandBarSeparatorItem4.DisplayName = "Separator";
            this.radCommandBarSeparatorItem4.Name = "radCommandBarSeparatorItem4";
            this.radCommandBarSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radCommandBarSeparatorItem4.VisibleInOverflowMenu = false;
            // 
            // radCommandBarToggleButtonItem4
            // 
            this.radCommandBarToggleButtonItem4.AccessibleDescription = "radCommandBarToggleButtonItem4";
            this.radCommandBarToggleButtonItem4.AccessibleName = "radCommandBarToggleButtonItem4";
            this.radCommandBarToggleButtonItem4.DisplayName = "Left Align Button";
            this.radCommandBarToggleButtonItem4.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem4.Image")));
            this.radCommandBarToggleButtonItem4.Name = "radCommandBarToggleButtonItem4";
            this.radCommandBarToggleButtonItem4.Text = "radCommandBarToggleButtonItem4";
            this.radCommandBarToggleButtonItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarToggleButtonItem5
            // 
            this.radCommandBarToggleButtonItem5.AccessibleDescription = "radCommandBarToggleButtonItem5";
            this.radCommandBarToggleButtonItem5.AccessibleName = "radCommandBarToggleButtonItem5";
            this.radCommandBarToggleButtonItem5.DisplayName = "Center Align Button";
            this.radCommandBarToggleButtonItem5.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem5.Image")));
            this.radCommandBarToggleButtonItem5.Name = "radCommandBarToggleButtonItem5";
            this.radCommandBarToggleButtonItem5.Text = "radCommandBarToggleButtonItem5";
            this.radCommandBarToggleButtonItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarToggleButtonItem6
            // 
            this.radCommandBarToggleButtonItem6.AccessibleDescription = "radCommandBarToggleButtonItem6";
            this.radCommandBarToggleButtonItem6.AccessibleName = "radCommandBarToggleButtonItem6";
            this.radCommandBarToggleButtonItem6.DisplayName = "Right Align Button";
            this.radCommandBarToggleButtonItem6.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem6.Image")));
            this.radCommandBarToggleButtonItem6.Name = "radCommandBarToggleButtonItem6";
            this.radCommandBarToggleButtonItem6.Text = "radCommandBarToggleButtonItem6";
            this.radCommandBarToggleButtonItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarToggleButtonItem7
            // 
            this.radCommandBarToggleButtonItem7.AccessibleDescription = "radCommandBarToggleButtonItem7";
            this.radCommandBarToggleButtonItem7.AccessibleName = "radCommandBarToggleButtonItem7";
            this.radCommandBarToggleButtonItem7.DisplayName = "Justify Text Button";
            this.radCommandBarToggleButtonItem7.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem7.Image")));
            this.radCommandBarToggleButtonItem7.Name = "radCommandBarToggleButtonItem7";
            this.radCommandBarToggleButtonItem7.Text = "radCommandBarToggleButtonItem7";
            this.radCommandBarToggleButtonItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarSeparatorItem5
            // 
            this.radCommandBarSeparatorItem5.DisplayName = "Separator";
            this.radCommandBarSeparatorItem5.Name = "radCommandBarSeparatorItem5";
            this.radCommandBarSeparatorItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radCommandBarSeparatorItem5.VisibleInOverflowMenu = false;
            // 
            // radCommandBarToggleButtonItem8
            // 
            this.radCommandBarToggleButtonItem8.AccessibleDescription = "radCommandBarToggleButtonItem8";
            this.radCommandBarToggleButtonItem8.AccessibleName = "radCommandBarToggleButtonItem8";
            this.radCommandBarToggleButtonItem8.DisplayName = "Bulleted List Button";
            this.radCommandBarToggleButtonItem8.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem8.Image")));
            this.radCommandBarToggleButtonItem8.Name = "radCommandBarToggleButtonItem8";
            this.radCommandBarToggleButtonItem8.Text = "radCommandBarToggleButtonItem8";
            this.radCommandBarToggleButtonItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBarToggleButtonItem9
            // 
            this.radCommandBarToggleButtonItem9.AccessibleDescription = "radCommandBarToggleButtonItem9";
            this.radCommandBarToggleButtonItem9.AccessibleName = "radCommandBarToggleButtonItem9";
            this.radCommandBarToggleButtonItem9.DisplayName = "Ordered List Button";
            this.radCommandBarToggleButtonItem9.Image = ((System.Drawing.Image)(resources.GetObject("radCommandBarToggleButtonItem9.Image")));
            this.radCommandBarToggleButtonItem9.Name = "radCommandBarToggleButtonItem9";
            this.radCommandBarToggleButtonItem9.Text = "radCommandBarToggleButtonItem9";
            this.radCommandBarToggleButtonItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radCommandBar1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(634, 336);
            this.panel1.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1196, 599);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement1;
        private Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem1;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem2;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem3;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem4;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem5;
        private Telerik.WinControls.UI.CommandBarSeparator radCommandBarSeparatorItem1;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem6;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem7;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem8;
        private Telerik.WinControls.UI.CommandBarSeparator radCommandBarSeparatorItem2;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem9;
        private Telerik.WinControls.UI.CommandBarButton radCommandBarButtonItem10;
        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement2;
        private Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem1;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem2;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem3;
        private Telerik.WinControls.UI.CommandBarSeparator radCommandBarSeparatorItem3;
        private Telerik.WinControls.UI.CommandBarDropDownList radCommandBarDropDownListItem1;
        private Telerik.WinControls.UI.CommandBarDropDownList radCommandBarDropDownListItem2;
        private Telerik.WinControls.UI.CommandBarSeparator radCommandBarSeparatorItem4;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem4;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem5;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem6;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem7;
        private Telerik.WinControls.UI.CommandBarSeparator radCommandBarSeparatorItem5;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem8;
        private Telerik.WinControls.UI.CommandBarToggleButton radCommandBarToggleButtonItem9;
        private System.Windows.Forms.Panel panel1;
	}
}