﻿namespace Telerik.Examples.WinControls.SplitContainer.FirstLook
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadTreeNode radTreeNode1 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode2 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode3 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode4 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode5 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode6 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode7 = new Telerik.WinControls.UI.RadTreeNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radSplitContainer_1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel_1 = new Telerik.WinControls.UI.SplitPanel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radPanelBar1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage5 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage6 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage7 = new Telerik.WinControls.UI.RadPageViewPage();
            this.splitPanel_2 = new Telerik.WinControls.UI.SplitPanel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.carsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.northwindDataSet = new Telerik.Examples.WinControls.DataSources.NorthwindDataSet();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.splitPanel_3 = new Telerik.WinControls.UI.SplitPanel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radSplitContainer_2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel_4 = new Telerik.WinControls.UI.SplitPanel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.splitPanel_5 = new Telerik.WinControls.UI.SplitPanel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView2 = new Telerik.WinControls.UI.RadGridView();
            this.radCalendar1 = new Telerik.WinControls.UI.RadCalendar();
            this.radRibbonBar1 = new Telerik.WinControls.UI.RadRibbonBar();
            this.ribbonTab2 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup8 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.btnOpenDesigner = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup7 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radGalleryElement1 = new Telerik.WinControls.UI.RadGalleryElement();
            this.radGalleryItem1 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem2 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem3 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem4 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem5 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem6 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem7 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem8 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem9 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radRibbonBarGroup4 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement1 = new Telerik.WinControls.UI.RadButtonElement();
            this.radDropDownButtonElement1 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radRibbonBarGroup6 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup5 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radDropDownButtonElement2 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radButtonElement5 = new Telerik.WinControls.UI.RadButtonElement();
            this.radDropDownButtonElement4 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radButtonElement2 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup5 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement7 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement8 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement9 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup12 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radComboBoxElement1 = new Telerik.WinControls.UI.RadDropDownListElement();
            this.radButtonElement4 = new Telerik.WinControls.UI.RadButtonElement();
            this.radDropDownButtonElement9 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.ribbonTab1 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup10 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radDropDownButtonElement3 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radDropDownButtonElement5 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radDropDownButtonElement6 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radRibbonBarGroup11 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement3 = new Telerik.WinControls.UI.RadButtonElement();
            this.radDropDownButtonElement7 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radDropDownButtonElement8 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radButtonElement6 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement11 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement12 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonFormBehavior1 = new Telerik.WinControls.UI.RadRibbonFormBehavior();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.carsTableAdapter = new Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.CarsTableAdapter();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer_1)).BeginInit();
            this.radSplitContainer_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_1)).BeginInit();
            this.splitPanel_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelBar1)).BeginInit();
            this.radPanelBar1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_2)).BeginInit();
            this.splitPanel_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_3)).BeginInit();
            this.splitPanel_3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer_2)).BeginInit();
            this.radSplitContainer_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_4)).BeginInit();
            this.splitPanel_4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_5)).BeginInit();
            this.splitPanel_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboBoxElement1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer_1
            // 
            this.radSplitContainer_1.Controls.Add(this.splitPanel_1);
            this.radSplitContainer_1.Controls.Add(this.splitPanel_2);
            this.radSplitContainer_1.Controls.Add(this.splitPanel_3);
            this.radSplitContainer_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer_1.Location = new System.Drawing.Point(1, 1);
            this.radSplitContainer_1.Name = "radSplitContainer_1";
            // 
            // 
            // 
            this.radSplitContainer_1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer_1.Size = new System.Drawing.Size(1314, 864);
            this.radSplitContainer_1.TabIndex = 0;
            this.radSplitContainer_1.TabStop = false;
            this.radSplitContainer_1.Text = "radSplitContainer1";
            // 
            // splitPanel_1
            // 
            this.splitPanel_1.Controls.Add(this.radLabel1);
            this.splitPanel_1.Controls.Add(this.radPanelBar1);
            this.splitPanel_1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel_1.Name = "splitPanel_1";
            this.splitPanel_1.Padding = new System.Windows.Forms.Padding(1);
            // 
            // 
            // 
            this.splitPanel_1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel_1.Size = new System.Drawing.Size(200, 864);
            this.splitPanel_1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.1412151F, 0F);
            this.splitPanel_1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Absolute;
            this.splitPanel_1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-144, 0);
            this.splitPanel_1.TabIndex = 0;
            this.splitPanel_1.TabStop = false;
            this.splitPanel_1.Text = "splitPanel1";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(7, 8);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(68, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Split Panel 1";
            // 
            // radPanelBar1
            // 
            this.radPanelBar1.Controls.Add(this.radPageViewPage1);
            this.radPanelBar1.Controls.Add(this.radPageViewPage2);
            this.radPanelBar1.Controls.Add(this.radPageViewPage3);
            this.radPanelBar1.Controls.Add(this.radPageViewPage4);
            this.radPanelBar1.Controls.Add(this.radPageViewPage5);
            this.radPanelBar1.Controls.Add(this.radPageViewPage6);
            this.radPanelBar1.Controls.Add(this.radPageViewPage7);
            this.radPanelBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanelBar1.ItemSizeMode = ((Telerik.WinControls.UI.PageViewItemSizeMode)((Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth | Telerik.WinControls.UI.PageViewItemSizeMode.EqualHeight)));
            this.radPanelBar1.Location = new System.Drawing.Point(1, 1);
            this.radPanelBar1.Name = "radPanelBar1";
            this.radPanelBar1.SelectedPage = this.radPageViewPage1;
            this.radPanelBar1.Size = new System.Drawing.Size(198, 862);
            this.radPanelBar1.TabIndex = 2;
            this.radPanelBar1.Text = "radPanelBar1";
            this.radPanelBar1.ViewMode = Telerik.WinControls.UI.PageViewMode.Outlook;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.radTreeView1);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(200F, 32F);
            this.radPageViewPage1.Location = new System.Drawing.Point(5, 31);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(188, 565);
            this.radPageViewPage1.Text = "Mail";
            // 
            // radTreeView1
            // 
            this.radTreeView1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView1.Font = new System.Drawing.Font("Tahoma", 8.6F);
            this.radTreeView1.ImageList = this.imageList1;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Name = "radTreeView1";
            radTreeNode1.Expanded = true;
            radTreeNode1.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode1.Image")));
            radTreeNode1.ImageKey = "splitcon_firstlook_folder_star.png";
            radTreeNode2.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode2.Image")));
            radTreeNode2.ImageIndex = 4;
            radTreeNode2.Text = "Sent Items";
            radTreeNode3.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode3.Image")));
            radTreeNode3.ImageKey = "splitcon_firstlook_bin_closed.png";
            radTreeNode3.Text = "Delete Items";
            radTreeNode1.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode2,
            radTreeNode3});
            radTreeNode1.Text = "Favorites";
            radTreeNode4.Expanded = true;
            radTreeNode4.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode4.Image")));
            radTreeNode4.ImageIndex = 3;
            radTreeNode5.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode5.Image")));
            radTreeNode5.ImageIndex = 5;
            radTreeNode5.Text = "Inbox";
            radTreeNode6.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode6.Image")));
            radTreeNode6.ImageKey = "splitcon_firstlook_folder_draft.gif";
            radTreeNode6.Text = "Drafts";
            radTreeNode7.Image = ((System.Drawing.Image)(resources.GetObject("radTreeNode7.Image")));
            radTreeNode7.ImageIndex = 4;
            radTreeNode7.Text = "Sent Items";
            radTreeNode4.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode5,
            radTreeNode6,
            radTreeNode7});
            radTreeNode4.Text = "MailBox";
            this.radTreeView1.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode1,
            radTreeNode4});
            this.radTreeView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView1.ShowLines = true;
            this.radTreeView1.Size = new System.Drawing.Size(188, 565);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 1;
            this.radTreeView1.Text = "radTreeView1";
            this.radTreeView1.ThemeName = "ControlDefault";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "splitcon_firstlook_folder_star.png");
            this.imageList1.Images.SetKeyName(1, "splitcon_firstlook_bin_closed.png");
            this.imageList1.Images.SetKeyName(2, "splitcon_firstlook_folder_draft.gif");
            this.imageList1.Images.SetKeyName(3, "splitcon_firstlook_mailbox.png");
            this.imageList1.Images.SetKeyName(4, "splitcon_firstlook_sentitems.png");
            this.imageList1.Images.SetKeyName(5, "splitcon_firstlook_inbox.png");
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(200F, 32F);
            this.radPageViewPage2.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(198, 154);
            this.radPageViewPage2.Text = "Calendar";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.ItemSize = new System.Drawing.SizeF(200F, 32F);
            this.radPageViewPage3.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(200, 100);
            this.radPageViewPage3.Text = "Contacts";
            // 
            // radPageViewPage4
            // 
            this.radPageViewPage4.ItemSize = new System.Drawing.SizeF(200F, 32F);
            this.radPageViewPage4.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage4.Name = "radPageViewPage4";
            this.radPageViewPage4.Size = new System.Drawing.Size(200, 100);
            this.radPageViewPage4.Text = "Tasks";
            // 
            // radPageViewPage5
            // 
            this.radPageViewPage5.ItemSize = new System.Drawing.SizeF(200F, 32F);
            this.radPageViewPage5.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage5.Name = "radPageViewPage5";
            this.radPageViewPage5.Size = new System.Drawing.Size(200, 100);
            this.radPageViewPage5.Text = "Notes";
            // 
            // radPageViewPage6
            // 
            this.radPageViewPage6.ItemSize = new System.Drawing.SizeF(200F, 32F);
            this.radPageViewPage6.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage6.Name = "radPageViewPage6";
            this.radPageViewPage6.Size = new System.Drawing.Size(200, 100);
            this.radPageViewPage6.Text = "Folder List";
            // 
            // radPageViewPage7
            // 
            this.radPageViewPage7.ItemSize = new System.Drawing.SizeF(200F, 32F);
            this.radPageViewPage7.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage7.Name = "radPageViewPage7";
            this.radPageViewPage7.Size = new System.Drawing.Size(200, 100);
            this.radPageViewPage7.Text = "Shortcuts";
            // 
            // splitPanel_2
            // 
            this.splitPanel_2.Controls.Add(this.radGroupBox2);
            this.splitPanel_2.Controls.Add(this.radLabel2);
            this.splitPanel_2.Location = new System.Drawing.Point(204, 0);
            this.splitPanel_2.Name = "splitPanel_2";
            this.splitPanel_2.Padding = new System.Windows.Forms.Padding(1);
            // 
            // 
            // 
            this.splitPanel_2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel_2.Size = new System.Drawing.Size(406, 864);
            this.splitPanel_2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.1343186F, 0F);
            this.splitPanel_2.SizeInfo.SplitterCorrection = new System.Drawing.Size(-125, 0);
            this.splitPanel_2.TabIndex = 1;
            this.splitPanel_2.TabStop = false;
            this.splitPanel_2.Text = "splitPanel2";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radGridView1);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox2.GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office;
            this.radGroupBox2.HeaderText = "Inbox";
            this.radGroupBox2.Location = new System.Drawing.Point(1, 1);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox2.Size = new System.Drawing.Size(404, 862);
            this.radGroupBox2.TabIndex = 1;
            this.radGroupBox2.Text = "Inbox";
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox2.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox2.GetChildAt(0).GetChildAt(1))).GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox2.GetChildAt(0).GetChildAt(1))).MinSize = new System.Drawing.Size(0, 20);
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.Location = new System.Drawing.Point(1, 21);
            // 
            // 
            // 
            gridViewDateTimeColumn1.FieldName = "Date";
            gridViewDateTimeColumn1.HeaderText = "Date";
            gridViewDateTimeColumn1.IsAutoGenerated = true;
            gridViewDateTimeColumn1.Name = "Date";
            gridViewTextBoxColumn1.HeaderText = "Newest on top";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 90;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn1});
            this.radGridView1.MasterTemplate.DataSource = this.carsBindingSource;
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Size = new System.Drawing.Size(403, 841);
            this.radGridView1.TabIndex = 1;
            // 
            // carsBindingSource
            // 
            this.carsBindingSource.DataMember = "Cars";
            this.carsBindingSource.DataSource = this.northwindDataSet;
            // 
            // northwindDataSet
            // 
            this.northwindDataSet.DataSetName = "NorthwindDataSet";
            this.northwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(13, 8);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(68, 18);
            this.radLabel2.TabIndex = 0;
            this.radLabel2.Text = "Split Panel 2";
            // 
            // splitPanel_3
            // 
            this.splitPanel_3.Controls.Add(this.radPanel2);
            this.splitPanel_3.Location = new System.Drawing.Point(614, 0);
            this.splitPanel_3.Name = "splitPanel_3";
            this.splitPanel_3.Padding = new System.Windows.Forms.Padding(1);
            // 
            // 
            // 
            this.splitPanel_3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel_3.Size = new System.Drawing.Size(700, 864);
            this.splitPanel_3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2755336F, 0F);
            this.splitPanel_3.SizeInfo.SplitterCorrection = new System.Drawing.Size(269, 0);
            this.splitPanel_3.TabIndex = 2;
            this.splitPanel_3.TabStop = false;
            this.splitPanel_3.Text = "splitPanel3";
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radSplitContainer_2);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(1, 1);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.radPanel2.Size = new System.Drawing.Size(698, 862);
            this.radPanel2.TabIndex = 0;
            this.radPanel2.Text = "radPanel2";
            // 
            // radSplitContainer_2
            // 
            this.radSplitContainer_2.Controls.Add(this.splitPanel_4);
            this.radSplitContainer_2.Controls.Add(this.splitPanel_5);
            this.radSplitContainer_2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radSplitContainer_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer_2.Location = new System.Drawing.Point(1, 1);
            this.radSplitContainer_2.Name = "radSplitContainer_2";
            // 
            // 
            // 
            this.radSplitContainer_2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer_2.Size = new System.Drawing.Size(696, 860);
            this.radSplitContainer_2.TabIndex = 0;
            this.radSplitContainer_2.TabStop = false;
            this.radSplitContainer_2.Text = "BrowserToDoBarSplitContainer";
            // 
            // splitPanel_4
            // 
            this.splitPanel_4.Controls.Add(this.webBrowser1);
            this.splitPanel_4.Controls.Add(this.radLabel3);
            this.splitPanel_4.Location = new System.Drawing.Point(0, 0);
            this.splitPanel_4.Name = "splitPanel_4";
            this.splitPanel_4.Padding = new System.Windows.Forms.Padding(1);
            // 
            // 
            // 
            this.splitPanel_4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel_4.Size = new System.Drawing.Size(497, 860);
            this.splitPanel_4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2186992F, -0.009803922F);
            this.splitPanel_4.SizeInfo.SplitterCorrection = new System.Drawing.Size(115, -7);
            this.splitPanel_4.TabIndex = 0;
            this.splitPanel_4.TabStop = false;
            this.splitPanel_4.Text = "splitPanel4";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(1, 1);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(495, 858);
            this.webBrowser1.TabIndex = 1;
            this.webBrowser1.Url = new System.Uri("", System.UriKind.Relative);
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 8);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(68, 18);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Split Panel 3";
            // 
            // splitPanel_5
            // 
            this.splitPanel_5.Controls.Add(this.radGroupBox1);
            this.splitPanel_5.Location = new System.Drawing.Point(501, 0);
            this.splitPanel_5.Name = "splitPanel_5";
            this.splitPanel_5.Padding = new System.Windows.Forms.Padding(1);
            // 
            // 
            // 
            this.splitPanel_5.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel_5.Size = new System.Drawing.Size(195, 860);
            this.splitPanel_5.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2186992F, 0.009803922F);
            this.splitPanel_5.SizeInfo.SplitterCorrection = new System.Drawing.Size(-115, 7);
            this.splitPanel_5.TabIndex = 1;
            this.splitPanel_5.TabStop = false;
            this.splitPanel_5.Text = "splitPanel5";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radGridView2);
            this.radGroupBox1.Controls.Add(this.radCalendar1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office;
            this.radGroupBox1.HeaderText = "To-Do Bar";
            this.radGroupBox1.Location = new System.Drawing.Point(1, 1);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox1.Size = new System.Drawing.Size(193, 858);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "To-Do Bar";
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).MinSize = new System.Drawing.Size(0, 20);
            // 
            // radGridView2
            // 
            this.radGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView2.Location = new System.Drawing.Point(1, 171);
            // 
            // 
            // 
            gridViewTextBoxColumn2.HeaderText = "Tasks";
            gridViewTextBoxColumn2.Name = "column1";
            gridViewTextBoxColumn2.Width = 150;
            this.radGridView2.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn2});
            this.radGridView2.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView2.Name = "radGridView2";
            this.radGridView2.Size = new System.Drawing.Size(191, 689);
            this.radGridView2.TabIndex = 2;
            // 
            // radCalendar1
            // 
            this.radCalendar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radCalendar1.FastNavigationNextImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.FastNavigationNextImage")));
            this.radCalendar1.FastNavigationPrevImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.FastNavigationPrevImage")));
            this.radCalendar1.Location = new System.Drawing.Point(0, 20);
            this.radCalendar1.Name = "radCalendar1";
            this.radCalendar1.NavigationNextImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.NavigationNextImage")));
            this.radCalendar1.NavigationPrevImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.NavigationPrevImage")));
            this.radCalendar1.SelectedDates.AddRange(new System.DateTime[] {
            new System.DateTime(1900, 1, 1, 0, 0, 0, 0)});
            this.radCalendar1.Size = new System.Drawing.Size(196, 151);
            this.radCalendar1.TabIndex = 1;
            this.radCalendar1.Text = "radCalendar1";
            this.radCalendar1.ZoomFactor = 1.2F;
            // 
            // radRibbonBar1
            // 
            this.radRibbonBar1.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.ribbonTab2,
            this.ribbonTab1});
            // 
            // 
            // 
            this.radRibbonBar1.ExitButton.Text = "Exit";
            this.radRibbonBar1.Location = new System.Drawing.Point(0, 0);
            this.radRibbonBar1.Name = "radRibbonBar1";
            // 
            // 
            // 
            this.radRibbonBar1.OptionsButton.Text = "Options";
            this.radRibbonBar1.QuickAccessToolBarItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement6,
            this.radButtonElement11,
            this.radButtonElement12});
            this.radRibbonBar1.Size = new System.Drawing.Size(1316, 174);
            this.radRibbonBar1.TabIndex = 1;
            this.radRibbonBar1.Text = "RadSplitContainer";
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            this.ribbonTab2.IsSelected = true;
            this.ribbonTab2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup8,
            this.radRibbonBarGroup7,
            this.radRibbonBarGroup4,
            this.radRibbonBarGroup6,
            this.radRibbonBarGroup5,
            this.radRibbonBarGroup12});
            this.ribbonTab2.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.ribbonTab2.Name = "ribbonTab2";
            this.ribbonTab2.StretchHorizontally = false;
            this.ribbonTab2.Text = "Home";
            // 
            // radRibbonBarGroup8
            // 
            this.radRibbonBarGroup8.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnOpenDesigner});
            this.radRibbonBarGroup8.Name = "radRibbonBarGroup8";
            this.radRibbonBarGroup8.Text = "Layout";
            // 
            // btnOpenDesigner
            // 
            this.btnOpenDesigner.MaxSize = new System.Drawing.Size(150, 0);
            this.btnOpenDesigner.Name = "btnOpenDesigner";
            this.btnOpenDesigner.Text = "Open Layout Properties Window";
            this.btnOpenDesigner.TextWrap = true;
            // 
            // radRibbonBarGroup7
            // 
            this.radRibbonBarGroup7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryElement1});
            this.radRibbonBarGroup7.Name = "radRibbonBarGroup7";
            this.radRibbonBarGroup7.Text = "Quick Steps";
            // 
            // radGalleryElement1
            // 
            this.radGalleryElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryItem1,
            this.radGalleryItem2,
            this.radGalleryItem3,
            this.radGalleryItem4,
            this.radGalleryItem5,
            this.radGalleryItem6,
            this.radGalleryItem7,
            this.radGalleryItem8,
            this.radGalleryItem9});
            this.radGalleryElement1.MaxColumns = 3;
            this.radGalleryElement1.MaxRows = 3;
            this.radGalleryElement1.Name = "radGalleryElement1";
            this.radGalleryElement1.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.radGalleryElement1.Text = "radGalleryElement1";
            // 
            // radGalleryItem1
            // 
            this.radGalleryItem1.DescriptionText = "";
            this.radGalleryItem1.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_forwardTeam;
            this.radGalleryItem1.Name = "radGalleryItem1";
            this.radGalleryItem1.StretchHorizontally = false;
            this.radGalleryItem1.StretchVertically = false;
            this.radGalleryItem1.Text = "Forward to team";
            this.radGalleryItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGalleryItem2
            // 
            this.radGalleryItem2.DescriptionText = "";
            this.radGalleryItem2.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_forwardTeam;
            this.radGalleryItem2.Name = "radGalleryItem2";
            this.radGalleryItem2.StretchHorizontally = false;
            this.radGalleryItem2.StretchVertically = false;
            this.radGalleryItem2.Text = "To Manager";
            this.radGalleryItem2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGalleryItem3
            // 
            this.radGalleryItem3.DescriptionText = "";
            this.radGalleryItem3.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_teamemail;
            this.radGalleryItem3.Name = "radGalleryItem3";
            this.radGalleryItem3.StretchHorizontally = false;
            this.radGalleryItem3.StretchVertically = false;
            this.radGalleryItem3.Text = "Team E-Mail";
            this.radGalleryItem3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGalleryItem4
            // 
            this.radGalleryItem4.DescriptionText = "";
            this.radGalleryItem4.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_moveto;
            this.radGalleryItem4.Name = "radGalleryItem4";
            this.radGalleryItem4.StretchHorizontally = false;
            this.radGalleryItem4.StretchVertically = false;
            this.radGalleryItem4.Text = "Move To ?";
            this.radGalleryItem4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGalleryItem5
            // 
            this.radGalleryItem5.DescriptionText = "";
            this.radGalleryItem5.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_meetingreply;
            this.radGalleryItem5.Name = "radGalleryItem5";
            this.radGalleryItem5.StretchHorizontally = false;
            this.radGalleryItem5.StretchVertically = false;
            this.radGalleryItem5.Text = "Meeting Reply";
            this.radGalleryItem5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGalleryItem6
            // 
            this.radGalleryItem6.DescriptionText = "";
            this.radGalleryItem6.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_forwardTeam;
            this.radGalleryItem6.Name = "radGalleryItem6";
            this.radGalleryItem6.StretchHorizontally = false;
            this.radGalleryItem6.StretchVertically = false;
            this.radGalleryItem6.Text = "Forward: FYI";
            this.radGalleryItem6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGalleryItem7
            // 
            this.radGalleryItem7.DescriptionText = "";
            this.radGalleryItem7.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_done;
            this.radGalleryItem7.Name = "radGalleryItem7";
            this.radGalleryItem7.StretchHorizontally = false;
            this.radGalleryItem7.StretchVertically = false;
            this.radGalleryItem7.Text = "Done";
            this.radGalleryItem7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGalleryItem8
            // 
            this.radGalleryItem8.DescriptionText = "";
            this.radGalleryItem8.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_replydelete;
            this.radGalleryItem8.Name = "radGalleryItem8";
            this.radGalleryItem8.StretchHorizontally = false;
            this.radGalleryItem8.StretchVertically = false;
            this.radGalleryItem8.Text = "Reply && Delete";
            this.radGalleryItem8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGalleryItem9
            // 
            this.radGalleryItem9.DescriptionText = "";
            this.radGalleryItem9.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_teammeeting;
            this.radGalleryItem9.Name = "radGalleryItem9";
            this.radGalleryItem9.StretchHorizontally = false;
            this.radGalleryItem9.StretchVertically = false;
            this.radGalleryItem9.Text = "Team Meeting";
            this.radGalleryItem9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radRibbonBarGroup4
            // 
            this.radRibbonBarGroup4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement1,
            this.radDropDownButtonElement1});
            this.radRibbonBarGroup4.Name = "radRibbonBarGroup4";
            this.radRibbonBarGroup4.Text = "New";
            // 
            // radButtonElement1
            // 
            this.radButtonElement1.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_mail_write_48;
            this.radButtonElement1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement1.Name = "radButtonElement1";
            this.radButtonElement1.SmallImage = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_mail_write_16;
            this.radButtonElement1.Text = "<html> New <br>E-mail";
            this.radButtonElement1.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radDropDownButtonElement1
            // 
            this.radDropDownButtonElement1.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement1.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement1.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement1.ExpandArrowButton = false;
            this.radDropDownButtonElement1.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_newitems_big;
            this.radDropDownButtonElement1.Name = "radDropDownButtonElement1";
            this.radDropDownButtonElement1.Text = "<html> New <br>Items";
            this.radDropDownButtonElement1.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radDropDownButtonElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement1.GetChildAt(1))).ExpandArrow = false;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement1.GetChildAt(1))).ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            ((Telerik.WinControls.UI.RadArrowButtonElement)(this.radDropDownButtonElement1.GetChildAt(1).GetChildAt(0))).MinSize = new System.Drawing.Size(12, 12);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownButtonElement1.GetChildAt(1).GetChildAt(0).GetChildAt(1))).LeftWidth = 0F;
            // 
            // radRibbonBarGroup6
            // 
            this.radRibbonBarGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup5,
            this.radButtonElement2});
            this.radRibbonBarGroup6.Name = "radRibbonBarGroup6";
            this.radRibbonBarGroup6.Text = "Delete";
            // 
            // radRibbonBarButtonGroup5
            // 
            this.radRibbonBarButtonGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement2,
            this.radButtonElement5,
            this.radDropDownButtonElement4});
            this.radRibbonBarButtonGroup5.Name = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup5.Padding = new System.Windows.Forms.Padding(1);
            this.radRibbonBarButtonGroup5.ShowBackColor = false;
            this.radRibbonBarButtonGroup5.StretchVertically = false;
            this.radRibbonBarButtonGroup5.Text = "radRibbonBarButtonGroup5";
            // 
            // radDropDownButtonElement2
            // 
            this.radDropDownButtonElement2.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement2.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement2.ExpandArrowButton = false;
            this.radDropDownButtonElement2.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_junk;
            this.radDropDownButtonElement2.Margin = new System.Windows.Forms.Padding(0, 0, 1, 1);
            this.radDropDownButtonElement2.Name = "radDropDownButtonElement2";
            this.radDropDownButtonElement2.Text = "Junk";
            this.radDropDownButtonElement2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radDropDownButtonElement2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButtonElement5
            // 
            this.radButtonElement5.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_ignore;
            this.radButtonElement5.MinSize = new System.Drawing.Size(0, 20);
            this.radButtonElement5.Name = "radButtonElement5";
            this.radButtonElement5.ShowBorder = false;
            this.radButtonElement5.Text = "Ignore";
            this.radButtonElement5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radDropDownButtonElement4
            // 
            this.radDropDownButtonElement4.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement4.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement4.ExpandArrowButton = false;
            this.radDropDownButtonElement4.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_cleanup;
            this.radDropDownButtonElement4.Margin = new System.Windows.Forms.Padding(0, 0, 1, 1);
            this.radDropDownButtonElement4.Name = "radDropDownButtonElement4";
            this.radDropDownButtonElement4.Text = "Clean Up";
            this.radDropDownButtonElement4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radDropDownButtonElement4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButtonElement2
            // 
            this.radButtonElement2.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_Delete;
            this.radButtonElement2.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement2.Name = "radButtonElement2";
            this.radButtonElement2.SmallImage = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_Delete_small;
            this.radButtonElement2.Text = "Delete ";
            this.radButtonElement2.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radButtonElement2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radRibbonBarGroup5
            // 
            this.radRibbonBarGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement7,
            this.radButtonElement8,
            this.radButtonElement9});
            this.radRibbonBarGroup5.Name = "radRibbonBarGroup5";
            this.radRibbonBarGroup5.Text = "Respond";
            // 
            // radButtonElement7
            // 
            this.radButtonElement7.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_mail_reply;
            this.radButtonElement7.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement7.Name = "radButtonElement7";
            this.radButtonElement7.Text = "<html> Reply<br /></html>";
            this.radButtonElement7.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // radButtonElement8
            // 
            this.radButtonElement8.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcont_firstlook_mail_replayall;
            this.radButtonElement8.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement8.Name = "radButtonElement8";
            this.radButtonElement8.Text = "<html> Reply <br>All";
            this.radButtonElement8.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radButtonElement8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radButtonElement9
            // 
            this.radButtonElement9.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_mail_replay;
            this.radButtonElement9.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement9.Name = "radButtonElement9";
            this.radButtonElement9.Text = "<html>Forward<br /></html>";
            this.radButtonElement9.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // radRibbonBarGroup12
            // 
            this.radRibbonBarGroup12.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radComboBoxElement1,
            this.radButtonElement4,
            this.radDropDownButtonElement9});
            this.radRibbonBarGroup12.Name = "radRibbonBarGroup12";
            this.radRibbonBarGroup12.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup12.Text = "Find";
            // 
            // radComboBoxElement1
            // 
            this.radComboBoxElement1.ArrowButtonMinWidth = 17;
            this.radComboBoxElement1.AutoCompleteAppend = null;
            this.radComboBoxElement1.AutoCompleteDataSource = null;
            this.radComboBoxElement1.AutoCompleteDisplayMember = null;
            this.radComboBoxElement1.AutoCompleteSuggest = null;
            this.radComboBoxElement1.AutoCompleteValueMember = null;
            this.radComboBoxElement1.DataMember = "";
            this.radComboBoxElement1.DataSource = null;
            this.radComboBoxElement1.DefaultValue = null;
            this.radComboBoxElement1.DisplayMember = "";
            this.radComboBoxElement1.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radComboBoxElement1.DropDownAnimationEnabled = true;
            this.radComboBoxElement1.DropDownSizingMode = ((Telerik.WinControls.UI.SizingMode)((Telerik.WinControls.UI.SizingMode.RightBottom | Telerik.WinControls.UI.SizingMode.UpDown)));
            this.radComboBoxElement1.EditableElementText = "Find a Contact";
            this.radComboBoxElement1.EditorElement = this.radComboBoxElement1;
            this.radComboBoxElement1.EditorManager = null;
            this.radComboBoxElement1.Filter = null;
            this.radComboBoxElement1.FilterExpression = "";
            this.radComboBoxElement1.Focusable = true;
            this.radComboBoxElement1.FormatString = "{0}";
            this.radComboBoxElement1.FormattingEnabled = true;
            this.radComboBoxElement1.ItemHeight = 18;
            this.radComboBoxElement1.MaxDropDownItems = 0;
            this.radComboBoxElement1.MaxLength = 32767;
            this.radComboBoxElement1.MaxValue = null;
            this.radComboBoxElement1.MinSize = new System.Drawing.Size(140, 0);
            this.radComboBoxElement1.MinValue = null;
            this.radComboBoxElement1.Name = "radComboBoxElement1";
            this.radComboBoxElement1.NullText = "Find a contact";
            this.radComboBoxElement1.NullValue = null;
            this.radComboBoxElement1.OwnerOffset = 0;
            this.radComboBoxElement1.ShowImageInEditorArea = true;
            this.radComboBoxElement1.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radComboBoxElement1.StretchVertically = false;
            this.radComboBoxElement1.Value = null;
            this.radComboBoxElement1.ValueMember = "";
            // 
            // radButtonElement4
            // 
            this.radButtonElement4.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_addressbook;
            this.radButtonElement4.Name = "radButtonElement4";
            this.radButtonElement4.Text = "Address Book";
            this.radButtonElement4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radDropDownButtonElement9
            // 
            this.radDropDownButtonElement9.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement9.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement9.ExpandArrowButton = false;
            this.radDropDownButtonElement9.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_related;
            this.radDropDownButtonElement9.Name = "radDropDownButtonElement9";
            this.radDropDownButtonElement9.Text = "Related";
            this.radDropDownButtonElement9.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radDropDownButtonElement9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            this.ribbonTab1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup10,
            this.radRibbonBarGroup11,
            this.radRibbonBarGroup7});
            this.ribbonTab1.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.StretchHorizontally = false;
            this.ribbonTab1.Text = "Advanced";
            // 
            // radRibbonBarGroup10
            // 
            this.radRibbonBarGroup10.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement3,
            this.radDropDownButtonElement5,
            this.radDropDownButtonElement6});
            this.radRibbonBarGroup10.Name = "radRibbonBarGroup10";
            this.radRibbonBarGroup10.Text = "Actions";
            // 
            // radDropDownButtonElement3
            // 
            this.radDropDownButtonElement3.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement3.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement3.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement3.ExpandArrowButton = false;
            this.radDropDownButtonElement3.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_move_big;
            this.radDropDownButtonElement3.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radDropDownButtonElement3.Name = "radDropDownButtonElement3";
            this.radDropDownButtonElement3.Text = "Move";
            this.radDropDownButtonElement3.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radDropDownButtonElement3.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement3.GetChildAt(1))).ExpandArrow = false;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement3.GetChildAt(1))).ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            ((Telerik.WinControls.UI.RadArrowButtonElement)(this.radDropDownButtonElement3.GetChildAt(1).GetChildAt(0))).MinSize = new System.Drawing.Size(12, 12);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownButtonElement3.GetChildAt(1).GetChildAt(0).GetChildAt(1))).LeftWidth = 0F;
            // 
            // radDropDownButtonElement5
            // 
            this.radDropDownButtonElement5.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement5.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement5.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement5.ExpandArrowButton = false;
            this.radDropDownButtonElement5.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_rules_big;
            this.radDropDownButtonElement5.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radDropDownButtonElement5.Name = "radDropDownButtonElement5";
            this.radDropDownButtonElement5.Text = "Rules";
            this.radDropDownButtonElement5.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radDropDownButtonElement5.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement5.GetChildAt(1))).ExpandArrow = false;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement5.GetChildAt(1))).ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            ((Telerik.WinControls.UI.RadArrowButtonElement)(this.radDropDownButtonElement5.GetChildAt(1).GetChildAt(0))).MinSize = new System.Drawing.Size(12, 12);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownButtonElement5.GetChildAt(1).GetChildAt(0).GetChildAt(1))).LeftWidth = 0F;
            // 
            // radDropDownButtonElement6
            // 
            this.radDropDownButtonElement6.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement6.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement6.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement6.ExpandArrowButton = false;
            this.radDropDownButtonElement6.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_moreactions_big;
            this.radDropDownButtonElement6.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radDropDownButtonElement6.Name = "radDropDownButtonElement6";
            this.radDropDownButtonElement6.Text = "<html>More <br>Actions";
            this.radDropDownButtonElement6.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radDropDownButtonElement6.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement6.GetChildAt(1))).ExpandArrow = false;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement6.GetChildAt(1))).ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            ((Telerik.WinControls.UI.RadArrowButtonElement)(this.radDropDownButtonElement6.GetChildAt(1).GetChildAt(0))).MinSize = new System.Drawing.Size(12, 12);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownButtonElement6.GetChildAt(1).GetChildAt(0).GetChildAt(1))).LeftWidth = 0F;
            // 
            // radRibbonBarGroup11
            // 
            this.radRibbonBarGroup11.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement3,
            this.radDropDownButtonElement7,
            this.radDropDownButtonElement8});
            this.radRibbonBarGroup11.Name = "radRibbonBarGroup11";
            this.radRibbonBarGroup11.Text = "Tags";
            // 
            // radButtonElement3
            // 
            this.radButtonElement3.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_readunread_big;
            this.radButtonElement3.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement3.Name = "radButtonElement3";
            this.radButtonElement3.Text = "<html>Unread/<br>Read";
            this.radButtonElement3.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radButtonElement3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radDropDownButtonElement7
            // 
            this.radDropDownButtonElement7.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement7.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement7.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement7.ExpandArrowButton = false;
            this.radDropDownButtonElement7.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_categorize_big;
            this.radDropDownButtonElement7.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radDropDownButtonElement7.Name = "radDropDownButtonElement7";
            this.radDropDownButtonElement7.Text = "Categorize";
            this.radDropDownButtonElement7.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radDropDownButtonElement7.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement7.GetChildAt(1))).ExpandArrow = false;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement7.GetChildAt(1))).ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            ((Telerik.WinControls.UI.RadArrowButtonElement)(this.radDropDownButtonElement7.GetChildAt(1).GetChildAt(0))).MinSize = new System.Drawing.Size(12, 12);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownButtonElement7.GetChildAt(1).GetChildAt(0).GetChildAt(1))).LeftWidth = 0F;
            // 
            // radDropDownButtonElement8
            // 
            this.radDropDownButtonElement8.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement8.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement8.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement8.ExpandArrowButton = false;
            this.radDropDownButtonElement8.Image = global::Telerik.Examples.WinControls.Properties.Resources.splitcon_firstlook_flag;
            this.radDropDownButtonElement8.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radDropDownButtonElement8.Name = "radDropDownButtonElement8";
            this.radDropDownButtonElement8.Text = "<html> Follow <br>Up";
            this.radDropDownButtonElement8.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.radDropDownButtonElement8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement8.GetChildAt(1))).ExpandArrow = false;
            ((Telerik.WinControls.UI.DropDownEditorLayoutPanel)(this.radDropDownButtonElement8.GetChildAt(1))).ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            ((Telerik.WinControls.UI.RadArrowButtonElement)(this.radDropDownButtonElement8.GetChildAt(1).GetChildAt(0))).MinSize = new System.Drawing.Size(12, 12);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownButtonElement8.GetChildAt(1).GetChildAt(0).GetChildAt(1))).LeftWidth = 0F;
            // 
            // radButtonElement6
            // 
            this.radButtonElement6.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement6.MaxSize = new System.Drawing.Size(0, 18);
            this.radButtonElement6.MinSize = new System.Drawing.Size(16, 0);
            this.radButtonElement6.Name = "radButtonElement6";
            this.radButtonElement6.Padding = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.radButtonElement6.StretchHorizontally = false;
            this.radButtonElement6.StretchVertically = false;
            this.radButtonElement6.Text = "Send / Receive";
            this.radButtonElement6.ZIndex = 3;
            // 
            // radButtonElement11
            // 
            this.radButtonElement11.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement11.MaxSize = new System.Drawing.Size(0, 18);
            this.radButtonElement11.MinSize = new System.Drawing.Size(16, 0);
            this.radButtonElement11.Name = "radButtonElement11";
            this.radButtonElement11.Padding = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.radButtonElement11.StretchHorizontally = false;
            this.radButtonElement11.StretchVertically = false;
            this.radButtonElement11.Text = "Undo";
            this.radButtonElement11.ZIndex = 2;
            // 
            // radButtonElement12
            // 
            this.radButtonElement12.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement12.MaxSize = new System.Drawing.Size(0, 18);
            this.radButtonElement12.MinSize = new System.Drawing.Size(16, 0);
            this.radButtonElement12.Name = "radButtonElement12";
            this.radButtonElement12.Padding = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.radButtonElement12.StretchHorizontally = false;
            this.radButtonElement12.StretchVertically = false;
            this.radButtonElement12.Text = "Redo";
            this.radButtonElement12.ZIndex = 1;
            // 
            // radRibbonFormBehavior1
            // 
            this.radRibbonFormBehavior1.Form = this;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radSplitContainer_1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 174);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.radPanel1.Size = new System.Drawing.Size(1316, 866);
            this.radPanel1.TabIndex = 2;
            this.radPanel1.Text = "radPanel1";
            // 
            // carsTableAdapter
            // 
            this.carsTableAdapter.ClearBeforeFill = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1316, 1040);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.radRibbonBar1);
            this.FormBehavior = this.radRibbonFormBehavior1;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "Form1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "RadSplitContainer";
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer_1)).EndInit();
            this.radSplitContainer_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_1)).EndInit();
            this.splitPanel_1.ResumeLayout(false);
            this.splitPanel_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelBar1)).EndInit();
            this.radPanelBar1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_2)).EndInit();
            this.splitPanel_2.ResumeLayout(false);
            this.splitPanel_2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_3)).EndInit();
            this.splitPanel_3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer_2)).EndInit();
            this.radSplitContainer_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_4)).EndInit();
            this.splitPanel_4.ResumeLayout(false);
            this.splitPanel_4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel_5)).EndInit();
            this.splitPanel_5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboBoxElement1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer_1;
        private Telerik.WinControls.UI.SplitPanel splitPanel_1;
        private Telerik.WinControls.UI.SplitPanel splitPanel_2;
        private Telerik.WinControls.UI.SplitPanel splitPanel_3;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer_2;
        private Telerik.WinControls.UI.SplitPanel splitPanel_4;
        private Telerik.WinControls.UI.RadRibbonBar radRibbonBar1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab1;
        private Telerik.WinControls.UI.RadRibbonFormBehavior radRibbonFormBehavior1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadCalendar radCalendar1;
        private Telerik.Examples.WinControls.DataSources.NorthwindDataSet northwindDataSet;
        private System.Windows.Forms.BindingSource carsBindingSource;
        private Telerik.Examples.WinControls.DataSources.NorthwindDataSetTableAdapters.CarsTableAdapter carsTableAdapter;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab2;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup4;
        private Telerik.WinControls.UI.SplitPanel splitPanel_5;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGridView radGridView2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup6;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup5;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement5;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup7;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement7;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement8;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement9;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup10;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup11;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup12;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement1;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement1;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement2;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement4;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement3;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement5;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement6;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement3;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement7;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement8;
        private Telerik.WinControls.UI.RadDropDownListElement radComboBoxElement1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement4;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement9;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement6;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement11;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement12;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Telerik.WinControls.UI.RadGalleryElement radGalleryElement1;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem1;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem2;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem3;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem4;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem5;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem6;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem7;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem8;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem9;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup8;
        private Telerik.WinControls.UI.RadButtonElement btnOpenDesigner;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPageView radPanelBar1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage4;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage5;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage6;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage7;

        
    }
}
