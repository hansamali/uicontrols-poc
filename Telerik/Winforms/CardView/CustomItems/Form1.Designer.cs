﻿namespace Telerik.Examples.WinControls.CardView.CustomItems
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            this.sofiaCarRentalDataSet = new Telerik.Examples.WinControls.DataSources.SofiaCarRentalDataSet();
            this.carsRatesDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.carsRatesDataTableTableAdapter = new Telerik.Examples.WinControls.DataSources.SofiaCarRentalDataSetTableAdapters.CarsRatesDataTableTableAdapter();
            this.radCardView1 = new Telerik.WinControls.UI.RadCardView();
            this.cardViewItem16 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem1 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem7 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem8 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem9 = new Telerik.WinControls.UI.CardViewItem();
            this.layoutControlLabelItem1 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.cardViewItem2 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem3 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem4 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem6 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewGroupItem1 = new Telerik.WinControls.UI.CardViewGroupItem();
            this.cardViewItem10 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem11 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem12 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem13 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem14 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem15 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem5 = new Telerik.WinControls.UI.CardViewItem();
            this.layoutControlLabelItem2 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarDropDownSort = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel2 = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarDropDownGroup = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel3 = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarTextBoxFilter = new Telerik.WinControls.UI.CommandBarTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sofiaCarRentalDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carsRatesDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCardView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCardView1.CardTemplate)).BeginInit();
            this.radCardView1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1313, 3);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(1313, 158);
            // 
            // sofiaCarRentalDataSet
            // 
            this.sofiaCarRentalDataSet.DataSetName = "SofiaCarRentalDataSet";
            this.sofiaCarRentalDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // carsRatesDataTableBindingSource
            // 
            this.carsRatesDataTableBindingSource.DataMember = "CarsRatesDataTable";
            this.carsRatesDataTableBindingSource.DataSource = this.sofiaCarRentalDataSet;
            // 
            // carsRatesDataTableTableAdapter
            // 
            this.carsRatesDataTableTableAdapter.ClearBeforeFill = true;
            // 
            // radCardView1
            // 
            // 
            // radCardView1Template
            // 
            this.radCardView1.CardTemplate.HiddenItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.cardViewItem16,
            this.cardViewItem1,
            this.cardViewItem7,
            this.cardViewItem8,
            this.cardViewItem9,
            this.cardViewItem3,
            this.layoutControlLabelItem1});
            this.radCardView1.CardTemplate.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cardViewItem2,
            this.cardViewItem4,
            this.cardViewItem6,
            this.cardViewGroupItem1,
            this.cardViewItem5,
            this.layoutControlLabelItem2});
            this.radCardView1.CardTemplate.Location = new System.Drawing.Point(570, 228);
            this.radCardView1.CardTemplate.Name = "radCardView1Template";
            this.radCardView1.CardTemplate.Size = new System.Drawing.Size(360, 420);
            this.radCardView1.CardTemplate.TabIndex = 0;
            this.radCardView1.DataSource = this.carsRatesDataTableBindingSource;
            this.radCardView1.DisplayMember = "Make";
            this.radCardView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCardView1.ItemSize = new System.Drawing.Size(360, 420);
            this.radCardView1.Location = new System.Drawing.Point(0, 30);
            this.radCardView1.Name = "radCardView1";
            this.radCardView1.Size = new System.Drawing.Size(1501, 877);
            this.radCardView1.TabIndex = 2;
            this.radCardView1.Text = "radCardView1";
            this.radCardView1.ValueMember = "CarID";
            ((Telerik.WinControls.UI.RadCardViewElement)(this.radCardView1.GetChildAt(0))).DrawBorder = false;
            // 
            // cardViewItem16
            // 
            this.cardViewItem16.Bounds = new System.Drawing.Rectangle(0, 390, 192, 26);
            this.cardViewItem16.FieldName = "Available";
            this.cardViewItem16.Name = "cardViewItem16";
            this.cardViewItem16.Text = "Available";
            // 
            // cardViewItem1
            // 
            this.cardViewItem1.Bounds = new System.Drawing.Rectangle(0, 0, 192, 26);
            this.cardViewItem1.FieldName = "CarID";
            this.cardViewItem1.Name = "cardViewItem1";
            this.cardViewItem1.Text = "CarID";
            // 
            // cardViewItem7
            // 
            this.cardViewItem7.Bounds = new System.Drawing.Rectangle(0, 156, 192, 26);
            this.cardViewItem7.FieldName = "Daily";
            this.cardViewItem7.Name = "cardViewItem7";
            this.cardViewItem7.Text = "Daily";
            // 
            // cardViewItem8
            // 
            this.cardViewItem8.Bounds = new System.Drawing.Rectangle(0, 182, 192, 26);
            this.cardViewItem8.FieldName = "Weekly";
            this.cardViewItem8.Name = "cardViewItem8";
            this.cardViewItem8.Text = "Weekly";
            // 
            // cardViewItem9
            // 
            this.cardViewItem9.Bounds = new System.Drawing.Rectangle(0, 208, 192, 26);
            this.cardViewItem9.FieldName = "Monthly";
            this.cardViewItem9.Name = "cardViewItem9";
            this.cardViewItem9.Text = "Monthly";
            // 
            // layoutControlLabelItem1
            // 
            this.layoutControlLabelItem1.Bounds = new System.Drawing.Rectangle(0, 0, 360, 26);
            this.layoutControlLabelItem1.Name = "layoutControlLabelItem1";
            this.layoutControlLabelItem1.Text = "layoutControlLabelItem1";
            // 
            // cardViewItem2
            // 
            this.cardViewItem2.Bounds = new System.Drawing.Rectangle(0, 160, 360, 30);
            this.cardViewItem2.FieldName = "Make";
            this.cardViewItem2.MaxSize = new System.Drawing.Size(0, 0);
            this.cardViewItem2.MinSize = new System.Drawing.Size(0, 30);
            this.cardViewItem2.Name = "cardViewItem2";
            this.cardViewItem2.Text = "Make";
            this.cardViewItem2.TextProportionalSize = 0F;
            // 
            // cardViewItem3
            // 
            this.cardViewItem3.Bounds = new System.Drawing.Rectangle(350, 160, 10, 30);
            this.cardViewItem3.MaxSize = new System.Drawing.Size(10, 30);
            this.cardViewItem3.MinSize = new System.Drawing.Size(10, 30);
            this.cardViewItem3.Name = "cardViewItem3";
            this.cardViewItem3.Text = "Model";
            this.cardViewItem3.TextProportionalSize = 0F;
            // 
            // cardViewItem4
            // 
            this.cardViewItem4.Bounds = new System.Drawing.Rectangle(0, 190, 50, 30);
            this.cardViewItem4.FieldName = "CarYear";
            this.cardViewItem4.MaxSize = new System.Drawing.Size(50, 30);
            this.cardViewItem4.MinSize = new System.Drawing.Size(50, 30);
            this.cardViewItem4.Name = "cardViewItem4";
            this.cardViewItem4.Text = "CarYear";
            this.cardViewItem4.TextProportionalSize = 0F;
            // 
            // cardViewItem6
            // 
            this.cardViewItem6.Bounds = new System.Drawing.Rectangle(50, 190, 310, 30);
            this.cardViewItem6.FieldName = "CategoryName";
            this.cardViewItem6.MaxSize = new System.Drawing.Size(0, 30);
            this.cardViewItem6.MinSize = new System.Drawing.Size(0, 30);
            this.cardViewItem6.Name = "cardViewItem6";
            this.cardViewItem6.Text = "CategoryName";
            this.cardViewItem6.TextProportionalSize = 0F;
            // 
            // cardViewGroupItem1
            // 
            this.cardViewGroupItem1.Bounds = new System.Drawing.Rectangle(0, 230, 360, 190);
            this.cardViewGroupItem1.DrawText = true;
            this.cardViewGroupItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cardViewItem10,
            this.cardViewItem11,
            this.cardViewItem12,
            this.cardViewItem13,
            this.cardViewItem14,
            this.cardViewItem15});
            this.cardViewGroupItem1.Name = "cardViewGroupItem1";
            // 
            // cardViewItem10
            // 
            this.cardViewItem10.Bounds = new System.Drawing.Rectangle(0, 0, 352, 28);
            this.cardViewItem10.FieldName = "Mp3Player";
            this.cardViewItem10.Name = "cardViewItem10";
            this.cardViewItem10.Text = "Mp3Player";
            // 
            // cardViewItem11
            // 
            this.cardViewItem11.Bounds = new System.Drawing.Rectangle(0, 28, 352, 28);
            this.cardViewItem11.FieldName = "DVDPlayer";
            this.cardViewItem11.Name = "cardViewItem11";
            this.cardViewItem11.Text = "DVDPlayer";
            // 
            // cardViewItem12
            // 
            this.cardViewItem12.Bounds = new System.Drawing.Rectangle(0, 56, 352, 28);
            this.cardViewItem12.FieldName = "AirConditioner";
            this.cardViewItem12.Name = "cardViewItem12";
            this.cardViewItem12.Text = "AirConditioner";
            // 
            // cardViewItem13
            // 
            this.cardViewItem13.Bounds = new System.Drawing.Rectangle(0, 84, 352, 26);
            this.cardViewItem13.FieldName = "ABS";
            this.cardViewItem13.Name = "cardViewItem13";
            this.cardViewItem13.Text = "ABS";
            // 
            // cardViewItem14
            // 
            this.cardViewItem14.Bounds = new System.Drawing.Rectangle(0, 110, 352, 26);
            this.cardViewItem14.FieldName = "ASR";
            this.cardViewItem14.Name = "cardViewItem14";
            this.cardViewItem14.Text = "ASR";
            // 
            // cardViewItem15
            // 
            this.cardViewItem15.Bounds = new System.Drawing.Rectangle(0, 136, 352, 26);
            this.cardViewItem15.FieldName = "Navigation";
            this.cardViewItem15.Name = "cardViewItem15";
            this.cardViewItem15.Text = "Navigation";
            // 
            // cardViewItem5
            // 
            this.cardViewItem5.Bounds = new System.Drawing.Rectangle(0, 0, 360, 160);
            this.cardViewItem5.FieldName = "ImageFileName";
            this.cardViewItem5.MaxSize = new System.Drawing.Size(0, 160);
            this.cardViewItem5.MinSize = new System.Drawing.Size(0, 160);
            this.cardViewItem5.Name = "cardViewItem5";
            this.cardViewItem5.Text = "ImageFileName";
            // 
            // layoutControlLabelItem2
            // 
            this.layoutControlLabelItem2.Bounds = new System.Drawing.Rectangle(0, 220, 360, 10);
            this.layoutControlLabelItem2.DrawText = false;
            this.layoutControlLabelItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.layoutControlLabelItem2.MinSize = new System.Drawing.Size(46, 10);
            this.layoutControlLabelItem2.Name = "layoutControlLabelItem2";
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1501, 30);
            this.radCommandBar1.TabIndex = 4;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.DisplayName = null;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.commandBarSeparator4,
            this.commandBarLabel1,
            this.commandBarDropDownSort,
            this.commandBarSeparator1,
            this.commandBarLabel2,
            this.commandBarDropDownGroup,
            this.commandBarSeparator2,
            this.commandBarSeparator3,
            this.commandBarLabel3,
            this.commandBarTextBoxFilter});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.Text = "";
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.AccessibleDescription = "commandBarSeparator4";
            this.commandBarSeparator4.AccessibleName = "commandBarSeparator4";
            this.commandBarSeparator4.DisplayName = "commandBarSeparator4";
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel1
            // 
            this.commandBarLabel1.DisplayName = "commandBarLabel1";
            this.commandBarLabel1.Name = "commandBarLabel1";
            this.commandBarLabel1.Text = "Sort By:";
            // 
            // commandBarDropDownSort
            // 
            this.commandBarDropDownSort.DisplayName = "commandBarDropDownList1";
            this.commandBarDropDownSort.DropDownAnimationEnabled = true;
            this.commandBarDropDownSort.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "None";
            radListDataItem2.Text = "Make";
            radListDataItem3.Text = "Model";
            radListDataItem4.Text = "Category";
            radListDataItem5.Text = "Year";
            this.commandBarDropDownSort.Items.Add(radListDataItem1);
            this.commandBarDropDownSort.Items.Add(radListDataItem2);
            this.commandBarDropDownSort.Items.Add(radListDataItem3);
            this.commandBarDropDownSort.Items.Add(radListDataItem4);
            this.commandBarDropDownSort.Items.Add(radListDataItem5);
            this.commandBarDropDownSort.MaxDropDownItems = 0;
            this.commandBarDropDownSort.Name = "commandBarDropDownSort";
            this.commandBarDropDownSort.Text = "None";
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel2
            // 
            this.commandBarLabel2.DisplayName = "commandBarLabel2";
            this.commandBarLabel2.Name = "commandBarLabel2";
            this.commandBarLabel2.Text = "Group By:";
            // 
            // commandBarDropDownGroup
            // 
            this.commandBarDropDownGroup.DisplayName = "commandBarDropDownList2";
            this.commandBarDropDownGroup.DropDownAnimationEnabled = true;
            this.commandBarDropDownGroup.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem6.Text = "None";
            radListDataItem7.Text = "Make";
            radListDataItem8.Text = "Category";
            radListDataItem9.Text = "Year";
            this.commandBarDropDownGroup.Items.Add(radListDataItem6);
            this.commandBarDropDownGroup.Items.Add(radListDataItem7);
            this.commandBarDropDownGroup.Items.Add(radListDataItem8);
            this.commandBarDropDownGroup.Items.Add(radListDataItem9);
            this.commandBarDropDownGroup.MaxDropDownItems = 0;
            this.commandBarDropDownGroup.Name = "commandBarDropDownGroup";
            this.commandBarDropDownGroup.Text = "";
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.AccessibleDescription = "commandBarSeparator2";
            this.commandBarSeparator2.AccessibleName = "commandBarSeparator2";
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.AccessibleDescription = "commandBarSeparator3";
            this.commandBarSeparator3.AccessibleName = "commandBarSeparator3";
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel3
            // 
            this.commandBarLabel3.DisplayName = "commandBarLabel3";
            this.commandBarLabel3.Name = "commandBarLabel3";
            this.commandBarLabel3.Text = "Filter:";
            // 
            // commandBarTextBoxFilter
            // 
            this.commandBarTextBoxFilter.DisplayName = "commandBarTextBox1";
            this.commandBarTextBoxFilter.MinSize = new System.Drawing.Size(200, 0);
            this.commandBarTextBoxFilter.Name = "commandBarTextBoxFilter";
            this.commandBarTextBoxFilter.StretchHorizontally = true;
            this.commandBarTextBoxFilter.Text = "";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radCardView1);
            this.radPanel1.Controls.Add(this.radCommandBar1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1501, 907);
            this.radPanel1.TabIndex = 5;
            this.radPanel1.Text = "radPanel1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1511, 917);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radPanel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sofiaCarRentalDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carsRatesDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCardView1.CardTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCardView1)).EndInit();
            this.radCardView1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource carsRatesDataTableBindingSource;
        private DataSources.SofiaCarRentalDataSet sofiaCarRentalDataSet;
        private DataSources.SofiaCarRentalDataSetTableAdapters.CarsRatesDataTableTableAdapter carsRatesDataTableTableAdapter;
        private Telerik.WinControls.UI.RadCardView radCardView1;
        private Telerik.WinControls.UI.CardViewGroupItem cardViewGroupItem1;
        private Telerik.WinControls.UI.CardViewItem cardViewItem1;
        private Telerik.WinControls.UI.CardViewItem cardViewItem2;
        private Telerik.WinControls.UI.CardViewItem cardViewItem3;
        private Telerik.WinControls.UI.CardViewItem cardViewItem4;
        private Telerik.WinControls.UI.CardViewItem cardViewItem5;
        private Telerik.WinControls.UI.CardViewItem cardViewItem6;
        private Telerik.WinControls.UI.CardViewItem cardViewItem7;
        private Telerik.WinControls.UI.CardViewItem cardViewItem8;
        private Telerik.WinControls.UI.CardViewItem cardViewItem9;
        private Telerik.WinControls.UI.CardViewItem cardViewItem10;
        private Telerik.WinControls.UI.CardViewItem cardViewItem11;
        private Telerik.WinControls.UI.CardViewItem cardViewItem12;
        private Telerik.WinControls.UI.CardViewItem cardViewItem13;
        private Telerik.WinControls.UI.CardViewItem cardViewItem14;
        private Telerik.WinControls.UI.CardViewItem cardViewItem15;
        private Telerik.WinControls.UI.CardViewItem cardViewItem16;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem1;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel1;
        private Telerik.WinControls.UI.CommandBarDropDownList commandBarDropDownSort;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel2;
        private Telerik.WinControls.UI.CommandBarDropDownList commandBarDropDownGroup;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarTextBox commandBarTextBoxFilter;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel3;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem2;
    }
}