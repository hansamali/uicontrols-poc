﻿namespace Telerik.Examples.WinControls.CardView.Settings
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn1 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 0", "Image");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn2 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 1", "City");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn3 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 2", "Population");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn4 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 3", "Country");
            this.radGroupBoxProperties = new Telerik.WinControls.UI.RadGroupBox();
            this.radDropDownListOrientation = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabelOrientation = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBoxAllowCustomize = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxHotTracking = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxShowGroups = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxAllowEdit = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBoxVisualSettings = new Telerik.WinControls.UI.RadGroupBox();
            this.radColorBoxBorderColor = new Telerik.WinControls.UI.RadColorBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radBrowseEditorFont = new Telerik.WinControls.UI.RadBrowseEditor();
            this.radColorBoxForeColor = new Telerik.WinControls.UI.RadColorBox();
            this.radColorBoxBackColor = new Telerik.WinControls.UI.RadColorBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownListVisualItems = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabelSelectItem = new Telerik.WinControls.UI.RadLabel();
            this.radCardView1 = new Telerik.WinControls.UI.RadCardView();
            this.cardViewGroupItem1 = new Telerik.WinControls.UI.CardViewGroupItem();
            this.cardViewItem1 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem2 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem3 = new Telerik.WinControls.UI.CardViewItem();
            this.cardViewItem4 = new Telerik.WinControls.UI.CardViewItem();
            this.radContextMenu1 = new Telerik.WinControls.UI.RadContextMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBoxProperties)).BeginInit();
            this.radGroupBoxProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListOrientation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelOrientation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAllowCustomize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxHotTracking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxShowGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAllowEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBoxVisualSettings)).BeginInit();
            this.radGroupBoxVisualSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radColorBoxBorderColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBrowseEditorFont)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radColorBoxForeColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radColorBoxBackColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListVisualItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSelectItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCardView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCardView1.CardTemplate)).BeginInit();
            this.radCardView1.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBoxVisualSettings);
            this.settingsPanel.Controls.Add(this.radGroupBoxProperties);
            this.settingsPanel.Location = new System.Drawing.Point(1238, 20);
            this.settingsPanel.Size = new System.Drawing.Size(235, 393);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBoxProperties, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBoxVisualSettings, 0);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(1243, 419);
            // 
            // radGroupBoxProperties
            // 
            this.radGroupBoxProperties.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBoxProperties.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBoxProperties.Controls.Add(this.radDropDownListOrientation);
            this.radGroupBoxProperties.Controls.Add(this.radLabelOrientation);
            this.radGroupBoxProperties.Controls.Add(this.radCheckBoxAllowCustomize);
            this.radGroupBoxProperties.Controls.Add(this.radCheckBoxHotTracking);
            this.radGroupBoxProperties.Controls.Add(this.radCheckBoxShowGroups);
            this.radGroupBoxProperties.Controls.Add(this.radCheckBoxAllowEdit);
            this.radGroupBoxProperties.HeaderText = "Properties";
            this.radGroupBoxProperties.Location = new System.Drawing.Point(10, 33);
            this.radGroupBoxProperties.Name = "radGroupBoxProperties";
            this.radGroupBoxProperties.Size = new System.Drawing.Size(215, 152);
            this.radGroupBoxProperties.TabIndex = 1;
            this.radGroupBoxProperties.Tag = "1";
            this.radGroupBoxProperties.Text = "Properties";
            // 
            // radDropDownListOrientation
            // 
            this.radDropDownListOrientation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDropDownListOrientation.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Selected = true;
            radListDataItem1.Text = "Horizontal";
            radListDataItem2.Text = "Vertical";
            this.radDropDownListOrientation.Items.Add(radListDataItem1);
            this.radDropDownListOrientation.Items.Add(radListDataItem2);
            this.radDropDownListOrientation.Location = new System.Drawing.Point(105, 121);
            this.radDropDownListOrientation.Name = "radDropDownListOrientation";
            this.radDropDownListOrientation.Size = new System.Drawing.Size(96, 20);
            this.radDropDownListOrientation.TabIndex = 7;
            this.radDropDownListOrientation.Tag = "1";
            this.radDropDownListOrientation.Text = "Horizontal";
            this.radDropDownListOrientation.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownListOrientation_SelectedIndexChanged);
            // 
            // radLabelOrientation
            // 
            this.radLabelOrientation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabelOrientation.Location = new System.Drawing.Point(5, 123);
            this.radLabelOrientation.Name = "radLabelOrientation";
            this.radLabelOrientation.Size = new System.Drawing.Size(92, 18);
            this.radLabelOrientation.TabIndex = 6;
            this.radLabelOrientation.Text = "Items Orientation";
            // 
            // radCheckBoxAllowCustomize
            // 
            this.radCheckBoxAllowCustomize.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxAllowCustomize.Location = new System.Drawing.Point(5, 97);
            this.radCheckBoxAllowCustomize.Name = "radCheckBoxAllowCustomize";
            this.radCheckBoxAllowCustomize.Size = new System.Drawing.Size(176, 18);
            this.radCheckBoxAllowCustomize.TabIndex = 3;
            this.radCheckBoxAllowCustomize.Text = "Allow Customize CardTemplate";
            this.radCheckBoxAllowCustomize.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBoxAllowCustomize_ToggleStateChanged);
            // 
            // radCheckBoxHotTracking
            // 
            this.radCheckBoxHotTracking.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxHotTracking.Location = new System.Drawing.Point(5, 72);
            this.radCheckBoxHotTracking.Name = "radCheckBoxHotTracking";
            this.radCheckBoxHotTracking.Size = new System.Drawing.Size(117, 18);
            this.radCheckBoxHotTracking.TabIndex = 2;
            this.radCheckBoxHotTracking.Text = "Enable HotTracking";
            this.radCheckBoxHotTracking.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBoxHotTracking_ToggleStateChanged);
            // 
            // radCheckBoxShowGroups
            // 
            this.radCheckBoxShowGroups.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxShowGroups.Location = new System.Drawing.Point(5, 47);
            this.radCheckBoxShowGroups.Name = "radCheckBoxShowGroups";
            this.radCheckBoxShowGroups.Size = new System.Drawing.Size(86, 18);
            this.radCheckBoxShowGroups.TabIndex = 1;
            this.radCheckBoxShowGroups.Text = "Show groups";
            this.radCheckBoxShowGroups.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBoxShowGroups_ToggleStateChanged);
            // 
            // radCheckBoxAllowEdit
            // 
            this.radCheckBoxAllowEdit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxAllowEdit.Location = new System.Drawing.Point(5, 22);
            this.radCheckBoxAllowEdit.Name = "radCheckBoxAllowEdit";
            this.radCheckBoxAllowEdit.Size = new System.Drawing.Size(109, 18);
            this.radCheckBoxAllowEdit.TabIndex = 0;
            this.radCheckBoxAllowEdit.Text = "Enable Edit Items ";
            this.radCheckBoxAllowEdit.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBoxAllowEdit_ToggleStateChanged);
            // 
            // radGroupBoxVisualSettings
            // 
            this.radGroupBoxVisualSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBoxVisualSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBoxVisualSettings.Controls.Add(this.radColorBoxBorderColor);
            this.radGroupBoxVisualSettings.Controls.Add(this.radLabel4);
            this.radGroupBoxVisualSettings.Controls.Add(this.radBrowseEditorFont);
            this.radGroupBoxVisualSettings.Controls.Add(this.radColorBoxForeColor);
            this.radGroupBoxVisualSettings.Controls.Add(this.radColorBoxBackColor);
            this.radGroupBoxVisualSettings.Controls.Add(this.radLabel3);
            this.radGroupBoxVisualSettings.Controls.Add(this.radLabel2);
            this.radGroupBoxVisualSettings.Controls.Add(this.radLabel1);
            this.radGroupBoxVisualSettings.Controls.Add(this.radDropDownListVisualItems);
            this.radGroupBoxVisualSettings.Controls.Add(this.radLabelSelectItem);
            this.radGroupBoxVisualSettings.HeaderText = "Edit Visual Settings";
            this.radGroupBoxVisualSettings.Location = new System.Drawing.Point(10, 191);
            this.radGroupBoxVisualSettings.Name = "radGroupBoxVisualSettings";
            this.radGroupBoxVisualSettings.Size = new System.Drawing.Size(215, 189);
            this.radGroupBoxVisualSettings.TabIndex = 2;
            this.radGroupBoxVisualSettings.Tag = "1";
            this.radGroupBoxVisualSettings.Text = "Edit Visual Settings";
            // 
            // radColorBoxBorderColor
            // 
            this.radColorBoxBorderColor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radColorBoxBorderColor.Location = new System.Drawing.Point(83, 153);
            this.radColorBoxBorderColor.Name = "radColorBoxBorderColor";
            this.radColorBoxBorderColor.Size = new System.Drawing.Size(118, 20);
            this.radColorBoxBorderColor.TabIndex = 10;
            this.radColorBoxBorderColor.Tag = "1";
            this.radColorBoxBorderColor.Text = "radColorBox1";
            this.radColorBoxBorderColor.ValueChanged += new System.EventHandler(this.radColorBoxBorderColor_ValueChanged);
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel4.Location = new System.Drawing.Point(5, 156);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(66, 18);
            this.radLabel4.TabIndex = 8;
            this.radLabel4.Text = "BorderColor";
            // 
            // radBrowseEditorFont
            // 
            this.radBrowseEditorFont.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radBrowseEditorFont.DialogType = Telerik.WinControls.UI.BrowseEditorDialogType.FontDialog;
            this.radBrowseEditorFont.Location = new System.Drawing.Point(83, 119);
            this.radBrowseEditorFont.Name = "radBrowseEditorFont";
            this.radBrowseEditorFont.Size = new System.Drawing.Size(118, 20);
            this.radBrowseEditorFont.TabIndex = 7;
            this.radBrowseEditorFont.Tag = "1";
            this.radBrowseEditorFont.Text = "radBrowseEditor1";
            this.radBrowseEditorFont.ValueChanged += new System.EventHandler(this.radBrowseEditorFont_ValueChanged);
            // 
            // radColorBoxForeColor
            // 
            this.radColorBoxForeColor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radColorBoxForeColor.Location = new System.Drawing.Point(84, 85);
            this.radColorBoxForeColor.Name = "radColorBoxForeColor";
            this.radColorBoxForeColor.Size = new System.Drawing.Size(117, 20);
            this.radColorBoxForeColor.TabIndex = 6;
            this.radColorBoxForeColor.Tag = "1";
            this.radColorBoxForeColor.Text = "radColorBox2";
            this.radColorBoxForeColor.ValueChanged += new System.EventHandler(this.radColorBoxForeColor_ValueChanged);
            // 
            // radColorBoxBackColor
            // 
            this.radColorBoxBackColor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radColorBoxBackColor.Location = new System.Drawing.Point(84, 53);
            this.radColorBoxBackColor.Name = "radColorBoxBackColor";
            this.radColorBoxBackColor.Size = new System.Drawing.Size(117, 20);
            this.radColorBoxBackColor.TabIndex = 5;
            this.radColorBoxBackColor.Tag = "1";
            this.radColorBoxBackColor.Text = "radColorBox1";
            this.radColorBoxBackColor.ValueChanged += new System.EventHandler(this.radColorBoxBackColor_ValueChanged);
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(5, 120);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(28, 18);
            this.radLabel3.TabIndex = 4;
            this.radLabel3.Text = "Font";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(5, 87);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(54, 18);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "ForeColor";
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(5, 55);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(55, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "BackColor";
            // 
            // radDropDownListVisualItems
            // 
            this.radDropDownListVisualItems.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDropDownListVisualItems.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownListVisualItems.Location = new System.Drawing.Point(84, 21);
            this.radDropDownListVisualItems.Name = "radDropDownListVisualItems";
            this.radDropDownListVisualItems.Size = new System.Drawing.Size(117, 20);
            this.radDropDownListVisualItems.TabIndex = 1;
            this.radDropDownListVisualItems.Tag = "1";
            this.radDropDownListVisualItems.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownListVisualItems_SelectedIndexChanged);
            // 
            // radLabelSelectItem
            // 
            this.radLabelSelectItem.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabelSelectItem.Location = new System.Drawing.Point(5, 21);
            this.radLabelSelectItem.Name = "radLabelSelectItem";
            this.radLabelSelectItem.Size = new System.Drawing.Size(63, 18);
            this.radLabelSelectItem.TabIndex = 0;
            this.radLabelSelectItem.Text = "Select Item:";
            // 
            // radCardView1
            // 
            this.radCardView1.AllowArbitraryItemHeight = false;
            // 
            // radCardView1Template
            // 
            this.radCardView1.CardTemplate.HiddenItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.cardViewGroupItem1});
            this.radCardView1.CardTemplate.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cardViewItem1,
            this.cardViewItem2,
            this.cardViewItem3,
            this.cardViewItem4});
            this.radCardView1.CardTemplate.Location = new System.Drawing.Point(0, 0);
            this.radCardView1.CardTemplate.Name = "radCardView1Template";
            this.radCardView1.CardTemplate.Size = new System.Drawing.Size(220, 300);
            this.radCardView1.CardTemplate.TabIndex = 0;
            listViewDetailColumn1.HeaderText = "Image";
            listViewDetailColumn2.HeaderText = "City";
            listViewDetailColumn3.HeaderText = "Population";
            listViewDetailColumn4.HeaderText = "Country";
            this.radCardView1.Columns.AddRange(new Telerik.WinControls.UI.ListViewDetailColumn[] {
            listViewDetailColumn1,
            listViewDetailColumn2,
            listViewDetailColumn3,
            listViewDetailColumn4});
            this.radCardView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCardView1.ItemSize = new System.Drawing.Size(220, 300);
            this.radCardView1.Location = new System.Drawing.Point(0, 0);
            this.radCardView1.Name = "radCardView1";
            this.radCardView1.SelectLastAddedItem = false;
            this.radCardView1.Size = new System.Drawing.Size(1501, 907);
            this.radCardView1.TabIndex = 2;
            this.radCardView1.Text = "radCardView1";
            // 
            // cardViewGroupItem1
            // 
            this.cardViewGroupItem1.Bounds = new System.Drawing.Rectangle(0, 0, 230, 320);
            this.cardViewGroupItem1.Name = "cardViewGroupItem1";
            // 
            // cardViewItem1
            // 
            this.cardViewItem1.Bounds = new System.Drawing.Rectangle(0, 0, 220, 222);
            this.cardViewItem1.FieldName = "Column 0";
            this.cardViewItem1.Name = "cardViewItem1";
            this.cardViewItem1.Text = "Column 0";
            this.cardViewItem1.TextProportionalSize = 0F;
            // 
            // cardViewItem2
            // 
            this.cardViewItem2.Bounds = new System.Drawing.Rectangle(0, 222, 220, 26);
            this.cardViewItem2.FieldName = "Column 1";
            this.cardViewItem2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.cardViewItem2.Name = "cardViewItem2";
            this.cardViewItem2.Text = "Column 1";
            this.cardViewItem2.TextProportionalSize = 0.35F;
            // 
            // cardViewItem3
            // 
            this.cardViewItem3.Bounds = new System.Drawing.Rectangle(0, 248, 220, 26);
            this.cardViewItem3.FieldName = "Column 2";
            this.cardViewItem3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.cardViewItem3.Name = "cardViewItem3";
            this.cardViewItem3.Text = "Column 2";
            this.cardViewItem3.TextProportionalSize = 0.35F;
            // 
            // cardViewItem4
            // 
            this.cardViewItem4.Bounds = new System.Drawing.Rectangle(0, 274, 220, 26);
            this.cardViewItem4.FieldName = "Column 3";
            this.cardViewItem4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.cardViewItem4.Name = "cardViewItem4";
            this.cardViewItem4.Text = "Column 3";
            this.cardViewItem4.TextProportionalSize = 0.35F;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radCardView1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1511, 917);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radCardView1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBoxProperties)).EndInit();
            this.radGroupBoxProperties.ResumeLayout(false);
            this.radGroupBoxProperties.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListOrientation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelOrientation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAllowCustomize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxHotTracking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxShowGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAllowEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBoxVisualSettings)).EndInit();
            this.radGroupBoxVisualSettings.ResumeLayout(false);
            this.radGroupBoxVisualSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radColorBoxBorderColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBrowseEditorFont)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radColorBoxForeColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radColorBoxBackColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListVisualItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSelectItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCardView1.CardTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCardView1)).EndInit();
            this.radCardView1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBoxProperties;
        private Telerik.WinControls.UI.RadDropDownList radDropDownListOrientation;
        private Telerik.WinControls.UI.RadLabel radLabelOrientation;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxAllowCustomize;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxHotTracking;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxShowGroups;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxAllowEdit;
        private Telerik.WinControls.UI.RadGroupBox radGroupBoxVisualSettings;
        private Telerik.WinControls.UI.RadDropDownList radDropDownListVisualItems;
        private Telerik.WinControls.UI.RadLabel radLabelSelectItem;
        private Telerik.WinControls.UI.RadCardView radCardView1;
        private Telerik.WinControls.UI.CardViewGroupItem cardViewGroupItem1;
        private Telerik.WinControls.UI.CardViewItem cardViewItem1;
        private Telerik.WinControls.UI.CardViewItem cardViewItem2;
        private Telerik.WinControls.UI.CardViewItem cardViewItem3;
        private Telerik.WinControls.UI.CardViewItem cardViewItem4;
        private Telerik.WinControls.UI.RadColorBox radColorBoxForeColor;
        private Telerik.WinControls.UI.RadColorBox radColorBoxBackColor;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadContextMenu radContextMenu1;
        private Telerik.WinControls.UI.RadColorBox radColorBoxBorderColor;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadBrowseEditor radBrowseEditorFont;
    }
}