﻿namespace Telerik.Examples.WinControls.Buttons.ToggleSwitch
{
    partial class Form1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            this.radLabelNewAppointment = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxControl1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabelReminder = new Telerik.WinControls.UI.RadLabel();
            this.radLabelReminderDate = new Telerik.WinControls.UI.RadLabel();
            this.radLabelReminderTime = new Telerik.WinControls.UI.RadLabel();
            this.radLabelRecurrence = new Telerik.WinControls.UI.RadLabel();
            this.radLabelRecurrenceDate = new Telerik.WinControls.UI.RadLabel();
            this.radLabelPlaySound = new Telerik.WinControls.UI.RadLabel();
            this.radToggleSwitchReminder = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitchRecurrence = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitchPlaySound = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radTimePicker1 = new Telerik.WinControls.UI.RadTimePicker();
            this.radDropDownListRecurrence = new Telerik.WinControls.UI.RadDropDownList();
            this.radButtonCreateAppointment = new Telerik.WinControls.UI.RadButton();
            this.radCheckBoxAllowAnimation = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBoxRightToLeft = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabelSwitchElasticity = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorSwitchElasticity = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabelThumbTickness = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorThumbTickness = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabelAnimationFrames = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditorAnimationFrames = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelNewAppointment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelReminder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelReminderDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelReminderTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelRecurrence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelRecurrenceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelPlaySound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitchReminder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitchRecurrence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitchPlaySound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListRecurrence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonCreateAppointment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAllowAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxRightToLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSwitchElasticity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorSwitchElasticity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelThumbTickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorThumbTickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelAnimationFrames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorAnimationFrames)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radButtonCreateAppointment);
            this.radPanelDemoHolder.Controls.Add(this.radDropDownListRecurrence);
            this.radPanelDemoHolder.Controls.Add(this.radTimePicker1);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker1);
            this.radPanelDemoHolder.Controls.Add(this.radToggleSwitchPlaySound);
            this.radPanelDemoHolder.Controls.Add(this.radToggleSwitchRecurrence);
            this.radPanelDemoHolder.Controls.Add(this.radToggleSwitchReminder);
            this.radPanelDemoHolder.Controls.Add(this.radLabelPlaySound);
            this.radPanelDemoHolder.Controls.Add(this.radLabelRecurrenceDate);
            this.radPanelDemoHolder.Controls.Add(this.radLabelRecurrence);
            this.radPanelDemoHolder.Controls.Add(this.radLabelReminderTime);
            this.radPanelDemoHolder.Controls.Add(this.radLabelReminderDate);
            this.radPanelDemoHolder.Controls.Add(this.radLabelReminder);
            this.radPanelDemoHolder.Controls.Add(this.radTextBoxControl1);
            this.radPanelDemoHolder.Controls.Add(this.radLabelNewAppointment);
            this.radPanelDemoHolder.Size = new System.Drawing.Size(377, 451);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radSpinEditorAnimationFrames);
            this.settingsPanel.Controls.Add(this.radLabelAnimationFrames);
            this.settingsPanel.Controls.Add(this.radSpinEditorThumbTickness);
            this.settingsPanel.Controls.Add(this.radLabelThumbTickness);
            this.settingsPanel.Controls.Add(this.radSpinEditorSwitchElasticity);
            this.settingsPanel.Controls.Add(this.radLabelSwitchElasticity);
            this.settingsPanel.Controls.Add(this.radCheckBoxRightToLeft);
            this.settingsPanel.Controls.Add(this.radCheckBoxAllowAnimation);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxAllowAnimation, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radCheckBoxRightToLeft, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabelSwitchElasticity, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorSwitchElasticity, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabelThumbTickness, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorThumbTickness, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabelAnimationFrames, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radSpinEditorAnimationFrames, 0);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(922, 1);
            this.themePanel.Size = new System.Drawing.Size(230, 598);
            // 
            // radLabelNewAppointment
            // 
            this.radLabelNewAppointment.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.radLabelNewAppointment.Location = new System.Drawing.Point(-5, 24);
            this.radLabelNewAppointment.Name = "radLabelNewAppointment";
            this.radLabelNewAppointment.Size = new System.Drawing.Size(162, 29);
            this.radLabelNewAppointment.TabIndex = 0;
            this.radLabelNewAppointment.Text = "New appointment";
            // 
            // radTextBoxControl1
            // 
            this.radTextBoxControl1.Location = new System.Drawing.Point(1, 67);
            this.radTextBoxControl1.Name = "radTextBoxControl1";
            this.radTextBoxControl1.Size = new System.Drawing.Size(301, 20);
            this.radTextBoxControl1.TabIndex = 1;
            this.radTextBoxControl1.Text = "Name";
            // 
            // radLabelReminder
            // 
            this.radLabelReminder.Font = new System.Drawing.Font("Segoe UI", 10.25F);
            this.radLabelReminder.Location = new System.Drawing.Point(-3, 103);
            this.radLabelReminder.Name = "radLabelReminder";
            this.radLabelReminder.Size = new System.Drawing.Size(67, 22);
            this.radLabelReminder.TabIndex = 2;
            this.radLabelReminder.Text = "Reminder";
            // 
            // radLabelReminderDate
            // 
            this.radLabelReminderDate.Font = new System.Drawing.Font("Segoe UI", 8.75F);
            this.radLabelReminderDate.Location = new System.Drawing.Point(-3, 137);
            this.radLabelReminderDate.Name = "radLabelReminderDate";
            this.radLabelReminderDate.Size = new System.Drawing.Size(31, 19);
            this.radLabelReminderDate.TabIndex = 3;
            this.radLabelReminderDate.Text = "Date";
            // 
            // radLabelReminderTime
            // 
            this.radLabelReminderTime.Font = new System.Drawing.Font("Segoe UI", 8.75F);
            this.radLabelReminderTime.Location = new System.Drawing.Point(-2, 187);
            this.radLabelReminderTime.Name = "radLabelReminderTime";
            this.radLabelReminderTime.Size = new System.Drawing.Size(32, 19);
            this.radLabelReminderTime.TabIndex = 4;
            this.radLabelReminderTime.Text = "Time";
            // 
            // radLabelRecurrence
            // 
            this.radLabelRecurrence.Font = new System.Drawing.Font("Segoe UI", 10.25F);
            this.radLabelRecurrence.Location = new System.Drawing.Point(-3, 249);
            this.radLabelRecurrence.Name = "radLabelRecurrence";
            this.radLabelRecurrence.Size = new System.Drawing.Size(76, 22);
            this.radLabelRecurrence.TabIndex = 5;
            this.radLabelRecurrence.Text = "Recurrence";
            // 
            // radLabelRecurrenceDate
            // 
            this.radLabelRecurrenceDate.Font = new System.Drawing.Font("Segoe UI", 8.75F);
            this.radLabelRecurrenceDate.Location = new System.Drawing.Point(-3, 277);
            this.radLabelRecurrenceDate.Name = "radLabelRecurrenceDate";
            this.radLabelRecurrenceDate.Size = new System.Drawing.Size(31, 19);
            this.radLabelRecurrenceDate.TabIndex = 6;
            this.radLabelRecurrenceDate.Text = "Date";
            // 
            // radLabelPlaySound
            // 
            this.radLabelPlaySound.Font = new System.Drawing.Font("Segoe UI", 10.25F);
            this.radLabelPlaySound.Location = new System.Drawing.Point(-3, 337);
            this.radLabelPlaySound.Name = "radLabelPlaySound";
            this.radLabelPlaySound.Size = new System.Drawing.Size(75, 22);
            this.radLabelPlaySound.TabIndex = 7;
            this.radLabelPlaySound.Text = "Play sound";
            // 
            // radToggleSwitchReminder
            // 
            this.radToggleSwitchReminder.Location = new System.Drawing.Point(252, 105);
            this.radToggleSwitchReminder.Name = "radToggleSwitchReminder";
            this.radToggleSwitchReminder.Size = new System.Drawing.Size(50, 20);
            this.radToggleSwitchReminder.TabIndex = 8;
            this.radToggleSwitchReminder.Text = "radToggleSwitch1";
            this.radToggleSwitchReminder.ValueChanged += new System.EventHandler(this.radToggleSwitchReminder_ValueChanged);
            // 
            // radToggleSwitchRecurrence
            // 
            this.radToggleSwitchRecurrence.Location = new System.Drawing.Point(252, 251);
            this.radToggleSwitchRecurrence.Name = "radToggleSwitchRecurrence";
            this.radToggleSwitchRecurrence.Size = new System.Drawing.Size(50, 20);
            this.radToggleSwitchRecurrence.TabIndex = 9;
            this.radToggleSwitchRecurrence.Text = "radToggleSwitch2";
            this.radToggleSwitchRecurrence.ValueChanged += new System.EventHandler(this.radToggleSwitchRecurrence_ValueChanged);
            // 
            // radToggleSwitchPlaySound
            // 
            this.radToggleSwitchPlaySound.Location = new System.Drawing.Point(252, 339);
            this.radToggleSwitchPlaySound.Name = "radToggleSwitchPlaySound";
            this.radToggleSwitchPlaySound.Size = new System.Drawing.Size(50, 20);
            this.radToggleSwitchPlaySound.TabIndex = 10;
            this.radToggleSwitchPlaySound.Text = "radToggleSwitch3";
            this.radToggleSwitchPlaySound.Value = false;
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.CustomFormat = "dd\\/MM\\/yyyy";
            this.radDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker1.Location = new System.Drawing.Point(1, 158);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.Size = new System.Drawing.Size(301, 20);
            this.radDateTimePicker1.TabIndex = 11;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "11/06/2015";
            this.radDateTimePicker1.Value = new System.DateTime(2015, 6, 11, 11, 15, 0, 0);
            // 
            // radTimePicker1
            // 
            this.radTimePicker1.Location = new System.Drawing.Point(1, 208);
            this.radTimePicker1.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 0);
            this.radTimePicker1.MinValue = new System.DateTime(((long)(0)));
            this.radTimePicker1.Name = "radTimePicker1";
            this.radTimePicker1.Size = new System.Drawing.Size(301, 20);
            this.radTimePicker1.TabIndex = 12;
            this.radTimePicker1.TabStop = false;
            this.radTimePicker1.Text = "radTimePicker1";
            this.radTimePicker1.Value = new System.DateTime(2015, 6, 11, 11, 15, 0, 0);
            // 
            // radDropDownListRecurrence
            // 
            radListDataItem1.Text = "Every hour";
            radListDataItem2.Selected = true;
            radListDataItem2.Text = "Every day";
            radListDataItem3.Text = "Every week";
            radListDataItem4.Text = "Every month";
            this.radDropDownListRecurrence.Items.Add(radListDataItem1);
            this.radDropDownListRecurrence.Items.Add(radListDataItem2);
            this.radDropDownListRecurrence.Items.Add(radListDataItem3);
            this.radDropDownListRecurrence.Items.Add(radListDataItem4);
            this.radDropDownListRecurrence.Location = new System.Drawing.Point(1, 298);
            this.radDropDownListRecurrence.Name = "radDropDownListRecurrence";
            this.radDropDownListRecurrence.Size = new System.Drawing.Size(301, 20);
            this.radDropDownListRecurrence.TabIndex = 13;
            this.radDropDownListRecurrence.Text = "Every day";
            // 
            // radButtonCreateAppointment
            // 
            this.radButtonCreateAppointment.Location = new System.Drawing.Point(0, 384);
            this.radButtonCreateAppointment.Name = "radButtonCreateAppointment";
            this.radButtonCreateAppointment.Size = new System.Drawing.Size(110, 24);
            this.radButtonCreateAppointment.TabIndex = 14;
            this.radButtonCreateAppointment.Text = "Create";
            this.radButtonCreateAppointment.Click += new System.EventHandler(this.radButtonCreateAppointment_Click);
            // 
            // radCheckBoxAllowAnimation
            // 
            this.radCheckBoxAllowAnimation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxAllowAnimation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBoxAllowAnimation.Location = new System.Drawing.Point(10, 34);
            this.radCheckBoxAllowAnimation.Name = "radCheckBoxAllowAnimation";
            this.radCheckBoxAllowAnimation.Size = new System.Drawing.Size(103, 18);
            this.radCheckBoxAllowAnimation.TabIndex = 1;
            this.radCheckBoxAllowAnimation.Text = "Allow Animation";
            this.radCheckBoxAllowAnimation.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radCheckBoxAllowAnimation.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBoxAllowAnimation_ToggleStateChanged);
            // 
            // radCheckBoxRightToLeft
            // 
            this.radCheckBoxRightToLeft.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBoxRightToLeft.Location = new System.Drawing.Point(10, 59);
            this.radCheckBoxRightToLeft.Name = "radCheckBoxRightToLeft";
            this.radCheckBoxRightToLeft.Size = new System.Drawing.Size(84, 18);
            this.radCheckBoxRightToLeft.TabIndex = 2;
            this.radCheckBoxRightToLeft.Text = "Right To Left";
            this.radCheckBoxRightToLeft.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBoxRightToLeft_ToggleStateChanged);
            // 
            // radLabelSwitchElasticity
            // 
            this.radLabelSwitchElasticity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabelSwitchElasticity.Location = new System.Drawing.Point(10, 84);
            this.radLabelSwitchElasticity.Name = "radLabelSwitchElasticity";
            this.radLabelSwitchElasticity.Size = new System.Drawing.Size(84, 18);
            this.radLabelSwitchElasticity.TabIndex = 3;
            this.radLabelSwitchElasticity.Text = "Switch Elasticity";
            // 
            // radSpinEditorSwitchElasticity
            // 
            this.radSpinEditorSwitchElasticity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorSwitchElasticity.DecimalPlaces = 2;
            this.radSpinEditorSwitchElasticity.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.radSpinEditorSwitchElasticity.Location = new System.Drawing.Point(10, 102);
            this.radSpinEditorSwitchElasticity.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.radSpinEditorSwitchElasticity.Name = "radSpinEditorSwitchElasticity";
            this.radSpinEditorSwitchElasticity.Size = new System.Drawing.Size(180, 20);
            this.radSpinEditorSwitchElasticity.TabIndex = 4;
            this.radSpinEditorSwitchElasticity.TabStop = false;
            this.radSpinEditorSwitchElasticity.ThousandsSeparator = true;
            this.radSpinEditorSwitchElasticity.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.radSpinEditorSwitchElasticity.ValueChanged += new System.EventHandler(this.radSpinEditorSwitchElasticity_ValueChanged);
            // 
            // radLabelThumbTickness
            // 
            this.radLabelThumbTickness.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabelThumbTickness.Location = new System.Drawing.Point(10, 129);
            this.radLabelThumbTickness.Name = "radLabelThumbTickness";
            this.radLabelThumbTickness.Size = new System.Drawing.Size(85, 18);
            this.radLabelThumbTickness.TabIndex = 5;
            this.radLabelThumbTickness.Text = "Thumb Tickness";
            // 
            // radSpinEditorThumbTickness
            // 
            this.radSpinEditorThumbTickness.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorThumbTickness.Location = new System.Drawing.Point(10, 147);
            this.radSpinEditorThumbTickness.Maximum = new decimal(new int[] {
            35,
            0,
            0,
            0});
            this.radSpinEditorThumbTickness.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.radSpinEditorThumbTickness.Name = "radSpinEditorThumbTickness";
            this.radSpinEditorThumbTickness.Size = new System.Drawing.Size(180, 20);
            this.radSpinEditorThumbTickness.TabIndex = 1;
            this.radSpinEditorThumbTickness.TabStop = false;
            this.radSpinEditorThumbTickness.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.radSpinEditorThumbTickness.ValueChanged += new System.EventHandler(this.radSpinEditorThumbTickness_ValueChanged);
            // 
            // radLabelAnimationFrames
            // 
            this.radLabelAnimationFrames.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabelAnimationFrames.Location = new System.Drawing.Point(10, 174);
            this.radLabelAnimationFrames.Name = "radLabelAnimationFrames";
            this.radLabelAnimationFrames.Size = new System.Drawing.Size(96, 18);
            this.radLabelAnimationFrames.TabIndex = 6;
            this.radLabelAnimationFrames.Text = "Animation Frames";
            // 
            // radSpinEditorAnimationFrames
            // 
            this.radSpinEditorAnimationFrames.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorAnimationFrames.Location = new System.Drawing.Point(10, 193);
            this.radSpinEditorAnimationFrames.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.radSpinEditorAnimationFrames.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.radSpinEditorAnimationFrames.Name = "radSpinEditorAnimationFrames";
            this.radSpinEditorAnimationFrames.Size = new System.Drawing.Size(180, 20);
            this.radSpinEditorAnimationFrames.TabIndex = 7;
            this.radSpinEditorAnimationFrames.TabStop = false;
            this.radSpinEditorAnimationFrames.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.radSpinEditorAnimationFrames.ValueChanged += new System.EventHandler(this.radSpinEditorAnimationFrames_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1513, 917);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelNewAppointment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelReminder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelReminderDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelReminderTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelRecurrence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelRecurrenceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelPlaySound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitchReminder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitchRecurrence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitchPlaySound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListRecurrence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonCreateAppointment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxAllowAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxRightToLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSwitchElasticity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorSwitchElasticity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelThumbTickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorThumbTickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelAnimationFrames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorAnimationFrames)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadDropDownList radDropDownListRecurrence;
        private Telerik.WinControls.UI.RadTimePicker radTimePicker1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitchPlaySound;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitchRecurrence;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitchReminder;
        private Telerik.WinControls.UI.RadLabel radLabelPlaySound;
        private Telerik.WinControls.UI.RadLabel radLabelRecurrenceDate;
        private Telerik.WinControls.UI.RadLabel radLabelRecurrence;
        private Telerik.WinControls.UI.RadLabel radLabelReminderTime;
        private Telerik.WinControls.UI.RadLabel radLabelReminderDate;
        private Telerik.WinControls.UI.RadLabel radLabelReminder;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl1;
        private Telerik.WinControls.UI.RadLabel radLabelNewAppointment;
        private Telerik.WinControls.UI.RadButton radButtonCreateAppointment;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxRightToLeft;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxAllowAnimation;
        private Telerik.WinControls.UI.RadLabel radLabelSwitchElasticity;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorSwitchElasticity;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorThumbTickness;
        private Telerik.WinControls.UI.RadLabel radLabelThumbTickness;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorAnimationFrames;
        private Telerik.WinControls.UI.RadLabel radLabelAnimationFrames;

    }
}
