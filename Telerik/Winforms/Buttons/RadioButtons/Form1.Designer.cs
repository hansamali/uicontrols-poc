namespace Telerik.Examples.WinControls.Buttons.RadioButtons
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.radTextBoxEvents = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioDonut = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioSquare = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioOffice = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioRound = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioRegular = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioCustShape = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioDonut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioSquare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioOffice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioRound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioRegular)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioCustShape)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radRadioDonut);
            this.radPanelDemoHolder.Controls.Add(this.radRadioRegular);
            this.radPanelDemoHolder.Controls.Add(this.radRadioSquare);
            this.radPanelDemoHolder.Controls.Add(this.radRadioCustShape);
            this.radPanelDemoHolder.Controls.Add(this.radRadioRound);
            this.radPanelDemoHolder.Controls.Add(this.radRadioOffice);
            this.radPanelDemoHolder.Location = new System.Drawing.Point(0, 0);
            this.radPanelDemoHolder.Size = new System.Drawing.Size(175, 182);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(973, 1);
            this.settingsPanel.Size = new System.Drawing.Size(250, 534);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            // 
            // radTextBoxEvents
            // 
            this.radTextBoxEvents.AutoSize = false;
            this.radTextBoxEvents.Location = new System.Drawing.Point(12, 21);
            this.radTextBoxEvents.Multiline = true;
            this.radTextBoxEvents.Name = "radTextBoxEvents";
            this.radTextBoxEvents.Size = new System.Drawing.Size(139, 170);
            this.radTextBoxEvents.TabIndex = 0;
            this.radTextBoxEvents.TabStop = false;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radTextBoxEvents);
            this.radGroupBox1.FooterText = "";
            this.radGroupBox1.HeaderText = " Events ";
            this.radGroupBox1.Location = new System.Drawing.Point(15, 6);
            this.radGroupBox1.Name = "radGroupBox1";
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.radGroupBox1.Size = new System.Drawing.Size(162, 203);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = " Events ";
            // 
            // radRadioDonut
            // 
            this.radRadioDonut.Location = new System.Drawing.Point(0, 81);
            this.radRadioDonut.Name = "radRadioDonut";
            this.radRadioDonut.Size = new System.Drawing.Size(51, 18);
            this.radRadioDonut.TabIndex = 0;
            this.radRadioDonut.Text = "Donut";

            // 
            // radRadioSquare
            // 
            this.radRadioSquare.Location = new System.Drawing.Point(0, 61);
            this.radRadioSquare.Name = "radRadioSquare";
            this.radRadioSquare.Size = new System.Drawing.Size(55, 18);
            this.radRadioSquare.TabIndex = 0;
            this.radRadioSquare.Text = "Square";

            // 
            // radRadioOffice
            // 
            this.radRadioOffice.Location = new System.Drawing.Point(0, 101);
            this.radRadioOffice.Name = "radRadioOffice";
            this.radRadioOffice.Size = new System.Drawing.Size(71, 18);
            this.radRadioOffice.TabIndex = 0;
            this.radRadioOffice.Text = "Office Tab";

            // 
            // radRadioRound
            // 
            this.radRadioRound.Location = new System.Drawing.Point(0, 40);
            this.radRadioRound.Name = "radRadioRound";
            this.radRadioRound.Size = new System.Drawing.Size(105, 18);
            this.radRadioRound.TabIndex = 0;
            this.radRadioRound.Text = "Round Rectangle";
      
            // 
            // radRadioRegular
            // 
            this.radRadioRegular.Location = new System.Drawing.Point(0, 0);
            this.radRadioRegular.Name = "radRadioRegular";
            this.radRadioRegular.Size = new System.Drawing.Size(58, 18);
            this.radRadioRegular.TabIndex = 0;
            this.radRadioRegular.Text = "Regular";

            // 
            // radRadioCustShape
            // 
            this.radRadioCustShape.Location = new System.Drawing.Point(0, 20);
            this.radRadioCustShape.Name = "radRadioCustShape";
            this.radRadioCustShape.Size = new System.Drawing.Size(93, 18);
            this.radRadioCustShape.TabIndex = 0;
            this.radRadioCustShape.Text = "Custom Shape";
 
            // 
            // Form1
            // 
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1170, 671);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radRadioDonut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioSquare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioOffice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioRound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioRegular)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioCustShape)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox radTextBoxEvents;
        private Telerik.WinControls.UI.RadRadioButton radRadioDonut;
        private Telerik.WinControls.UI.RadRadioButton radRadioSquare;
        private Telerik.WinControls.UI.RadRadioButton radRadioOffice;
        private Telerik.WinControls.UI.RadRadioButton radRadioRound;
        private Telerik.WinControls.UI.RadRadioButton radRadioCustShape;
        private Telerik.WinControls.UI.RadRadioButton radRadioRegular;
	}
}
