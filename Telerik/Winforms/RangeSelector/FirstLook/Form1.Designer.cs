﻿using System.Windows.Forms;
namespace Telerik.Examples.WinControls.RangeSelector.FirstLook
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.LineSeries lineSeries1 = new Telerik.WinControls.UI.LineSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint1 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint2 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint3 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint4 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint5 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint6 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint7 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint8 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint9 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint10 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint11 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint12 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.WinControls.UI.LineSeries lineSeries2 = new Telerik.WinControls.UI.LineSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint13 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint14 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint15 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint16 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint17 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint18 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint19 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint20 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint21 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint22 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint23 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint24 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.WinControls.UI.LineSeries lineSeries3 = new Telerik.WinControls.UI.LineSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint25 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint26 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint27 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint28 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint29 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint30 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint31 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint32 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint33 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint34 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint35 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint36 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.WinControls.UI.LineSeries lineSeries4 = new Telerik.WinControls.UI.LineSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint37 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint38 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint39 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint40 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint41 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint42 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint43 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint44 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint45 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint46 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint47 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint48 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.WinControls.UI.LineSeries lineSeries5 = new Telerik.WinControls.UI.LineSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint49 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint50 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint51 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint52 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint53 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint54 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint55 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint56 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint57 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint58 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint59 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint60 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.WinControls.UI.LineSeries lineSeries6 = new Telerik.WinControls.UI.LineSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint61 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint62 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint63 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint64 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint65 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint66 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint67 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint68 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint69 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint70 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint71 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint72 = new Telerik.Charting.CategoricalDataPoint();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radRangeSlider1 = new Telerik.WinControls.UI.RadRangeSelector();
            this.radChartView1 = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRangeSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(766, 60);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(766, 226);
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1492, 711);
            this.radSplitContainer1.SplitterWidth = 4;
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radRangeSlider1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1492, 189);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2330017F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -141);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radRangeSlider1
            // 
            this.radRangeSlider1.AssociatedControl = this.radChartView1;
            this.radRangeSlider1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radRangeSlider1.EndRange = 70F;
            this.radRangeSlider1.Location = new System.Drawing.Point(0, 0);
            this.radRangeSlider1.Name = "radRangeSlider1";
            this.radRangeSlider1.RangeSelectorViewZoomEnd = 100F;
            this.radRangeSlider1.RangeSelectorViewZoomStart = 0F;
            this.radRangeSlider1.Size = new System.Drawing.Size(1492, 189);
            this.radRangeSlider1.StartRange = 30F;
            this.radRangeSlider1.TabIndex = 0;
            this.radRangeSlider1.Text = "radRangeSlider1";
            // 
            // radChartView1
            // 
            this.radChartView1.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            categoricalAxis1.LabelRotationAngle = 300D;
            categoricalAxis1.Title = "";
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.LabelRotationAngle = 300D;
            linearAxis1.MajorStep = 20D;
            linearAxis1.Title = "";
            this.radChartView1.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.radChartView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radChartView1.Location = new System.Drawing.Point(0, 0);
            this.radChartView1.Name = "radChartView1";
            categoricalDataPoint1.Category = "Jan";
            categoricalDataPoint1.Label = 41D;
            categoricalDataPoint1.Value = 41D;
            categoricalDataPoint2.Category = "Feb";
            categoricalDataPoint2.Label = 77D;
            categoricalDataPoint2.Value = 77D;
            categoricalDataPoint3.Category = "Mar";
            categoricalDataPoint3.Label = 14D;
            categoricalDataPoint3.Value = 14D;
            categoricalDataPoint4.Category = "Apr";
            categoricalDataPoint4.Label = 63D;
            categoricalDataPoint4.Value = 63D;
            categoricalDataPoint5.Category = "May";
            categoricalDataPoint5.Label = 28D;
            categoricalDataPoint5.Value = 28D;
            categoricalDataPoint6.Category = "Jun";
            categoricalDataPoint6.Label = 39D;
            categoricalDataPoint6.Value = 39D;
            categoricalDataPoint7.Category = "Jul";
            categoricalDataPoint7.Label = 77D;
            categoricalDataPoint7.Value = 77D;
            categoricalDataPoint8.Category = "Aug";
            categoricalDataPoint8.Label = 87D;
            categoricalDataPoint8.Value = 87D;
            categoricalDataPoint9.Category = "Sep";
            categoricalDataPoint9.Label = 94D;
            categoricalDataPoint9.Value = 94D;
            categoricalDataPoint10.Category = "Oct";
            categoricalDataPoint10.Label = 82D;
            categoricalDataPoint10.Value = 82D;
            categoricalDataPoint11.Category = "Nov";
            categoricalDataPoint11.Label = 90D;
            categoricalDataPoint11.Value = 90D;
            categoricalDataPoint12.Category = "Dec";
            categoricalDataPoint12.Label = 95D;
            categoricalDataPoint12.Value = 95D;
            lineSeries1.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint1,
            categoricalDataPoint2,
            categoricalDataPoint3,
            categoricalDataPoint4,
            categoricalDataPoint5,
            categoricalDataPoint6,
            categoricalDataPoint7,
            categoricalDataPoint8,
            categoricalDataPoint9,
            categoricalDataPoint10,
            categoricalDataPoint11,
            categoricalDataPoint12});
            lineSeries1.HorizontalAxis = categoricalAxis1;
            lineSeries1.LabelAngle = 90D;
            lineSeries1.LabelDistanceToPoint = 15D;
            lineSeries1.LegendTitle = null;
            lineSeries1.VerticalAxis = linearAxis1;
            categoricalDataPoint13.Category = "Jan";
            categoricalDataPoint13.Label = 13D;
            categoricalDataPoint13.Value = 13D;
            categoricalDataPoint14.Category = "Feb";
            categoricalDataPoint14.Label = 42D;
            categoricalDataPoint14.Value = 42D;
            categoricalDataPoint15.Category = "Mar";
            categoricalDataPoint15.Label = 92D;
            categoricalDataPoint15.Value = 92D;
            categoricalDataPoint16.Category = "Apr";
            categoricalDataPoint16.Label = 85D;
            categoricalDataPoint16.Value = 85D;
            categoricalDataPoint17.Category = "May";
            categoricalDataPoint17.Label = 78D;
            categoricalDataPoint17.Value = 78D;
            categoricalDataPoint18.Category = "Jun";
            categoricalDataPoint18.Label = 87D;
            categoricalDataPoint18.Value = 87D;
            categoricalDataPoint19.Category = "Jul";
            categoricalDataPoint19.Label = 96D;
            categoricalDataPoint19.Value = 96D;
            categoricalDataPoint20.Category = "Aug";
            categoricalDataPoint20.Label = 81D;
            categoricalDataPoint20.Value = 81D;
            categoricalDataPoint21.Category = "Sep";
            categoricalDataPoint21.Label = 62D;
            categoricalDataPoint21.Value = 62D;
            categoricalDataPoint22.Category = "Oct";
            categoricalDataPoint22.Label = 43D;
            categoricalDataPoint22.Value = 43D;
            categoricalDataPoint23.Category = "Nov";
            categoricalDataPoint23.Label = 40D;
            categoricalDataPoint23.Value = 40D;
            categoricalDataPoint24.Category = "Dec";
            categoricalDataPoint24.Label = 20D;
            categoricalDataPoint24.Value = 20D;
            lineSeries2.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint13,
            categoricalDataPoint14,
            categoricalDataPoint15,
            categoricalDataPoint16,
            categoricalDataPoint17,
            categoricalDataPoint18,
            categoricalDataPoint19,
            categoricalDataPoint20,
            categoricalDataPoint21,
            categoricalDataPoint22,
            categoricalDataPoint23,
            categoricalDataPoint24});
            lineSeries2.HorizontalAxis = categoricalAxis1;
            lineSeries2.LabelAngle = 90D;
            lineSeries2.LabelDistanceToPoint = 15D;
            lineSeries2.VerticalAxis = linearAxis1;
            categoricalDataPoint25.Category = "Jan";
            categoricalDataPoint25.Label = 90D;
            categoricalDataPoint25.Value = 90D;
            categoricalDataPoint26.Category = "Feb";
            categoricalDataPoint26.Label = 70D;
            categoricalDataPoint26.Value = 70D;
            categoricalDataPoint27.Category = "Mar";
            categoricalDataPoint27.Label = 105D;
            categoricalDataPoint27.Value = 105D;
            categoricalDataPoint28.Category = "Apr";
            categoricalDataPoint28.Label = 42D;
            categoricalDataPoint28.Value = 42D;
            categoricalDataPoint29.Category = "May";
            categoricalDataPoint29.Label = 120D;
            categoricalDataPoint29.Value = 120D;
            categoricalDataPoint30.Category = "Jun";
            categoricalDataPoint30.Label = 5D;
            categoricalDataPoint30.Value = 5D;
            categoricalDataPoint31.Category = "Jul";
            categoricalDataPoint31.Label = 20D;
            categoricalDataPoint31.Value = 20D;
            categoricalDataPoint32.Category = "Aug";
            categoricalDataPoint32.Label = 32D;
            categoricalDataPoint32.Value = 32D;
            categoricalDataPoint33.Category = "Sep";
            categoricalDataPoint33.Label = 11D;
            categoricalDataPoint33.Value = 11D;
            categoricalDataPoint34.Category = "Oct";
            categoricalDataPoint34.Label = 2D;
            categoricalDataPoint34.Value = 2D;
            categoricalDataPoint35.Category = "Nov";
            categoricalDataPoint35.Label = 3D;
            categoricalDataPoint35.Value = 3D;
            categoricalDataPoint36.Category = "Dec";
            categoricalDataPoint36.Label = 1D;
            categoricalDataPoint36.Value = 1D;
            lineSeries3.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint25,
            categoricalDataPoint26,
            categoricalDataPoint27,
            categoricalDataPoint28,
            categoricalDataPoint29,
            categoricalDataPoint30,
            categoricalDataPoint31,
            categoricalDataPoint32,
            categoricalDataPoint33,
            categoricalDataPoint34,
            categoricalDataPoint35,
            categoricalDataPoint36});
            lineSeries3.HorizontalAxis = categoricalAxis1;
            lineSeries3.LabelAngle = 90D;
            lineSeries3.LabelDistanceToPoint = 15D;
            lineSeries3.VerticalAxis = linearAxis1;
            categoricalDataPoint37.Category = "Jan";
            categoricalDataPoint37.Label = 5D;
            categoricalDataPoint37.Value = 5D;
            categoricalDataPoint38.Category = "Feb";
            categoricalDataPoint38.Label = 15D;
            categoricalDataPoint38.Value = 15D;
            categoricalDataPoint39.Category = "Mar";
            categoricalDataPoint39.Label = 35D;
            categoricalDataPoint39.Value = 35D;
            categoricalDataPoint40.Category = "Apr";
            categoricalDataPoint40.Label = 38D;
            categoricalDataPoint40.Value = 38D;
            categoricalDataPoint41.Category = "May";
            categoricalDataPoint41.Label = 55D;
            categoricalDataPoint41.Value = 55D;
            categoricalDataPoint42.Category = "Jun";
            categoricalDataPoint42.Label = 78D;
            categoricalDataPoint42.Value = 78D;
            categoricalDataPoint43.Category = "Jul";
            categoricalDataPoint43.Label = 83D;
            categoricalDataPoint43.Value = 83D;
            categoricalDataPoint44.Category = "Aug";
            categoricalDataPoint44.Label = 90D;
            categoricalDataPoint44.Value = 90D;
            categoricalDataPoint45.Category = "Sep";
            categoricalDataPoint45.Label = 110D;
            categoricalDataPoint45.Value = 110D;
            categoricalDataPoint46.Category = "Oct";
            categoricalDataPoint46.Label = 120D;
            categoricalDataPoint46.Value = 120D;
            categoricalDataPoint47.Category = "Nov";
            categoricalDataPoint47.Label = 115D;
            categoricalDataPoint47.Value = 115D;
            categoricalDataPoint48.Category = "Dec";
            categoricalDataPoint48.Label = 120D;
            categoricalDataPoint48.Value = 120D;
            lineSeries4.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint37,
            categoricalDataPoint38,
            categoricalDataPoint39,
            categoricalDataPoint40,
            categoricalDataPoint41,
            categoricalDataPoint42,
            categoricalDataPoint43,
            categoricalDataPoint44,
            categoricalDataPoint45,
            categoricalDataPoint46,
            categoricalDataPoint47,
            categoricalDataPoint48});
            lineSeries4.HorizontalAxis = categoricalAxis1;
            lineSeries4.LabelAngle = 90D;
            lineSeries4.LabelDistanceToPoint = 15D;
            lineSeries4.VerticalAxis = linearAxis1;
            categoricalDataPoint49.Category = "Jan";
            categoricalDataPoint49.Label = 0D;
            categoricalDataPoint49.Value = 0D;
            categoricalDataPoint50.Category = "Feb";
            categoricalDataPoint50.Label = 35D;
            categoricalDataPoint50.Value = 35D;
            categoricalDataPoint51.Category = "Mar";
            categoricalDataPoint51.Label = 45D;
            categoricalDataPoint51.Value = 45D;
            categoricalDataPoint52.Category = "Apr";
            categoricalDataPoint52.Label = 35D;
            categoricalDataPoint52.Value = 35D;
            categoricalDataPoint53.Category = "May";
            categoricalDataPoint53.Label = 20D;
            categoricalDataPoint53.Value = 20D;
            categoricalDataPoint54.Category = "Jun";
            categoricalDataPoint54.Label = 35D;
            categoricalDataPoint54.Value = 35D;
            categoricalDataPoint55.Category = "Jul";
            categoricalDataPoint55.Label = 45D;
            categoricalDataPoint55.Value = 45D;
            categoricalDataPoint56.Category = "Aug";
            categoricalDataPoint56.Label = 55D;
            categoricalDataPoint56.Value = 55D;
            categoricalDataPoint57.Category = "Sep";
            categoricalDataPoint57.Label = 45D;
            categoricalDataPoint57.Value = 45D;
            categoricalDataPoint58.Category = "Oct";
            categoricalDataPoint58.Label = 65D;
            categoricalDataPoint58.Value = 65D;
            categoricalDataPoint59.Category = "Nov";
            categoricalDataPoint59.Label = 55D;
            categoricalDataPoint59.Value = 55D;
            categoricalDataPoint60.Category = "Dec";
            categoricalDataPoint60.Label = 100D;
            categoricalDataPoint60.Value = 100D;
            lineSeries5.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint49,
            categoricalDataPoint50,
            categoricalDataPoint51,
            categoricalDataPoint52,
            categoricalDataPoint53,
            categoricalDataPoint54,
            categoricalDataPoint55,
            categoricalDataPoint56,
            categoricalDataPoint57,
            categoricalDataPoint58,
            categoricalDataPoint59,
            categoricalDataPoint60});
            lineSeries5.HorizontalAxis = categoricalAxis1;
            lineSeries5.LabelAngle = 90D;
            lineSeries5.LabelDistanceToPoint = 15D;
            lineSeries5.VerticalAxis = linearAxis1;
            categoricalDataPoint61.Category = "Jan";
            categoricalDataPoint61.Label = 70D;
            categoricalDataPoint61.Value = 70D;
            categoricalDataPoint62.Category = "Feb";
            categoricalDataPoint62.Label = 60D;
            categoricalDataPoint62.Value = 60D;
            categoricalDataPoint63.Category = "Mar";
            categoricalDataPoint63.Label = 80D;
            categoricalDataPoint63.Value = 80D;
            categoricalDataPoint64.Category = "Apr";
            categoricalDataPoint64.Label = 50D;
            categoricalDataPoint64.Value = 50D;
            categoricalDataPoint65.Category = "May";
            categoricalDataPoint65.Label = 60D;
            categoricalDataPoint65.Value = 60D;
            categoricalDataPoint66.Category = "Jun";
            categoricalDataPoint66.Label = 70D;
            categoricalDataPoint66.Value = 70D;
            categoricalDataPoint67.Category = "Jul";
            categoricalDataPoint67.Label = 40D;
            categoricalDataPoint67.Value = 40D;
            categoricalDataPoint68.Category = "Aug";
            categoricalDataPoint68.Label = 15D;
            categoricalDataPoint68.Value = 15D;
            categoricalDataPoint69.Category = "Sep";
            categoricalDataPoint69.Label = 40D;
            categoricalDataPoint69.Value = 40D;
            categoricalDataPoint70.Category = "Oct";
            categoricalDataPoint70.Label = 90D;
            categoricalDataPoint70.Value = 90D;
            categoricalDataPoint71.Category = "Nov";
            categoricalDataPoint71.Label = 50D;
            categoricalDataPoint71.Value = 50D;
            categoricalDataPoint72.Category = "Dec";
            categoricalDataPoint72.Label = 20D;
            categoricalDataPoint72.Value = 20D;
            lineSeries6.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint61,
            categoricalDataPoint62,
            categoricalDataPoint63,
            categoricalDataPoint64,
            categoricalDataPoint65,
            categoricalDataPoint66,
            categoricalDataPoint67,
            categoricalDataPoint68,
            categoricalDataPoint69,
            categoricalDataPoint70,
            categoricalDataPoint71,
            categoricalDataPoint72});
            lineSeries6.HorizontalAxis = categoricalAxis1;
            lineSeries6.LabelAngle = 90D;
            lineSeries6.LabelDistanceToPoint = 15D;
            lineSeries6.VerticalAxis = linearAxis1;
            this.radChartView1.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            lineSeries1,
            lineSeries2,
            lineSeries3,
            lineSeries4,
            lineSeries5,
            lineSeries6});
            this.radChartView1.ShowGrid = false;
            this.radChartView1.ShowPanZoom = true;
            this.radChartView1.Size = new System.Drawing.Size(1492, 518);
            this.radChartView1.TabIndex = 0;
            this.radChartView1.Text = "radChartView1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radChartView1);
            this.splitPanel2.Location = new System.Drawing.Point(0, 193);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1492, 518);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2330017F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 141);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radSplitContainer1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1502, 721);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radRangeSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadRangeSelector radRangeSlider1;
        private Telerik.WinControls.UI.RadChartView radChartView1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
    }
}