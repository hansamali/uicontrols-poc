namespace Telerik.Examples.WinControls.MenuStrip.Animation
{
	partial class Form1
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.WinControls.Keyboard.InputBinding inputBinding13 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord13 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier13 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Elements.ClickCommand clickCommand2 = new Telerik.WinControls.Elements.ClickCommand();
            Telerik.WinControls.Keyboard.InputBinding inputBinding14 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord14 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier14 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding15 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord15 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier15 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding16 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord16 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier16 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding17 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord17 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier17 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding18 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord18 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier18 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding19 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord19 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier19 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding20 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord20 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier20 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding21 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord21 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier21 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding22 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord22 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier22 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding23 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord23 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier23 = new Telerik.WinControls.Keyboard.ChordModifier();
            Telerik.WinControls.Keyboard.InputBinding inputBinding24 = new Telerik.WinControls.Keyboard.InputBinding();
            Telerik.WinControls.Keyboard.Chord chord24 = new Telerik.WinControls.Keyboard.Chord();
            Telerik.WinControls.Keyboard.ChordModifier chordModifier24 = new Telerik.WinControls.Keyboard.ChordModifier();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem13 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem14 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem15 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem17 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem19 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem20 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem21 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuDemo = new Telerik.WinControls.UI.RadMenu();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem3 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem16 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem18 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radGroupSettings = new Telerik.WinControls.UI.RadGroupBox();
            this.radCheckEnabled = new Telerik.WinControls.UI.RadCheckBox();
            this.radSpinEditorFrames = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLblFrames = new Telerik.WinControls.UI.RadLabel();
            this.radLblAnim = new Telerik.WinControls.UI.RadLabel();
            this.radComboAnimation = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuDemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSettings)).BeginInit();
            this.radGroupSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorFrames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblFrames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblAnim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupSettings);
            this.settingsPanel.Location = new System.Drawing.Point(1023, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 818);
            this.settingsPanel.ThemeName = "ControlDefault";
            this.settingsPanel.Controls.SetChildIndex(this.radGroupSettings, 0);
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.AccessibleDescription = "New";
            this.radMenuItem4.AccessibleName = "New";
            this.radMenuItem4.HintText = "Ctrl+N";
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Text = "New";
            this.radMenuItem4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem9
            // 
            this.radMenuItem9.AccessibleDescription = "Print...";
            this.radMenuItem9.AccessibleName = "Print...";
            this.radMenuItem9.HintText = "Ctrl+P";
            this.radMenuItem9.Name = "radMenuItem9";
            this.radMenuItem9.Text = "Print...";
            this.radMenuItem9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem5
            // 
            this.radMenuItem5.AccessibleDescription = "Open...";
            this.radMenuItem5.AccessibleName = "Open...";
            this.radMenuItem5.HintText = "Ctrl+O";
            this.radMenuItem5.Name = "radMenuItem5";
            this.radMenuItem5.Text = "Open...";
            this.radMenuItem5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6
            // 
            this.radMenuItem6.AccessibleDescription = "Save";
            this.radMenuItem6.AccessibleName = "Save";
            this.radMenuItem6.HintText = "Ctrl+S";
            this.radMenuItem6.Name = "radMenuItem6";
            this.radMenuItem6.Text = "Save";
            this.radMenuItem6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem11
            // 
            this.radMenuItem11.AccessibleDescription = "Undo";
            this.radMenuItem11.AccessibleName = "Undo";
            this.radMenuItem11.HintText = "Ctrl+Z";
            this.radMenuItem11.Name = "radMenuItem11";
            this.radMenuItem11.Text = "Undo";
            this.radMenuItem11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem13
            // 
            this.radMenuItem13.AccessibleDescription = "Cut";
            this.radMenuItem13.AccessibleName = "Cut";
            this.radMenuItem13.HintText = "Ctrl+X";
            this.radMenuItem13.Name = "radMenuItem13";
            this.radMenuItem13.Text = "Cut";
            this.radMenuItem13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem13.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem14
            // 
            this.radMenuItem14.AccessibleDescription = "Copy";
            this.radMenuItem14.AccessibleName = "Copy";
            this.radMenuItem14.HintText = "Ctrl+C";
            this.radMenuItem14.Name = "radMenuItem14";
            this.radMenuItem14.Text = "Copy";
            this.radMenuItem14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem14.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem15
            // 
            this.radMenuItem15.AccessibleDescription = "Paste";
            this.radMenuItem15.AccessibleName = "Paste";
            this.radMenuItem15.HintText = "Ctrl+V";
            this.radMenuItem15.Name = "radMenuItem15";
            this.radMenuItem15.Text = "Paste";
            this.radMenuItem15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem15.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem17
            // 
            this.radMenuItem17.AccessibleDescription = "Find...";
            this.radMenuItem17.AccessibleName = "Find...";
            this.radMenuItem17.HintText = "Ctrl+F";
            this.radMenuItem17.Name = "radMenuItem17";
            this.radMenuItem17.Text = "Find...";
            this.radMenuItem17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem17.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem19
            // 
            this.radMenuItem19.AccessibleDescription = "Replace...";
            this.radMenuItem19.AccessibleName = "Replace...";
            this.radMenuItem19.HintText = "Ctrl+H";
            this.radMenuItem19.Name = "radMenuItem19";
            this.radMenuItem19.Text = "Replace...";
            this.radMenuItem19.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem19.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem20
            // 
            this.radMenuItem20.AccessibleDescription = "Go To...";
            this.radMenuItem20.AccessibleName = "Go To...";
            this.radMenuItem20.HintText = "Ctrl+G";
            this.radMenuItem20.Name = "radMenuItem20";
            this.radMenuItem20.Text = "Go To...";
            this.radMenuItem20.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem20.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem21
            // 
            this.radMenuItem21.AccessibleDescription = "Select All";
            this.radMenuItem21.AccessibleName = "Select All";
            this.radMenuItem21.HintText = "Ctrl+A";
            this.radMenuItem21.Name = "radMenuItem21";
            this.radMenuItem21.Text = "Select All";
            this.radMenuItem21.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem21.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuDemo
            // 
            this.radMenuDemo.AllowMerge = false;
            chordModifier13.AltModifier = false;
            chordModifier13.ControlModifier = true;
            chordModifier13.ShiftModifier = false;
            chord13.ChordModifier = chordModifier13;
            chord13.Keys = "Ctrl+N";
            inputBinding13.Chord = chord13;
            clickCommand2.ContextType = null;
            clickCommand2.ImageList = null;
            clickCommand2.Name = "ActionCommand";
            clickCommand2.OwnerType = typeof(Telerik.WinControls.RadItem);
            clickCommand2.Text = "This command rises the Click event of a selected RadItem instance.";
            clickCommand2.Type = "";
            inputBinding13.Command = clickCommand2;
            inputBinding13.CommandContext = this.radMenuItem4;
            chordModifier14.AltModifier = false;
            chordModifier14.ControlModifier = true;
            chordModifier14.ShiftModifier = false;
            chord14.ChordModifier = chordModifier14;
            chord14.Keys = "Ctrl+P";
            inputBinding14.Chord = chord14;
            inputBinding14.Command = clickCommand2;
            inputBinding14.CommandContext = this.radMenuItem9;
            chordModifier15.AltModifier = false;
            chordModifier15.ControlModifier = true;
            chordModifier15.ShiftModifier = false;
            chord15.ChordModifier = chordModifier15;
            chord15.Keys = "Ctrl+O";
            inputBinding15.Chord = chord15;
            inputBinding15.Command = clickCommand2;
            inputBinding15.CommandContext = this.radMenuItem5;
            chordModifier16.AltModifier = false;
            chordModifier16.ControlModifier = true;
            chordModifier16.ShiftModifier = false;
            chord16.ChordModifier = chordModifier16;
            chord16.Keys = "Ctrl+S";
            inputBinding16.Chord = chord16;
            inputBinding16.Command = clickCommand2;
            inputBinding16.CommandContext = this.radMenuItem6;
            chordModifier17.AltModifier = false;
            chordModifier17.ControlModifier = true;
            chordModifier17.ShiftModifier = false;
            chord17.ChordModifier = chordModifier17;
            chord17.Keys = "Ctrl+Z";
            inputBinding17.Chord = chord17;
            inputBinding17.Command = clickCommand2;
            inputBinding17.CommandContext = this.radMenuItem11;
            chordModifier18.AltModifier = false;
            chordModifier18.ControlModifier = true;
            chordModifier18.ShiftModifier = false;
            chord18.ChordModifier = chordModifier18;
            chord18.Keys = "Ctrl+X";
            inputBinding18.Chord = chord18;
            inputBinding18.Command = clickCommand2;
            inputBinding18.CommandContext = this.radMenuItem13;
            chordModifier19.AltModifier = false;
            chordModifier19.ControlModifier = true;
            chordModifier19.ShiftModifier = false;
            chord19.ChordModifier = chordModifier19;
            chord19.Keys = "Ctrl+C";
            inputBinding19.Chord = chord19;
            inputBinding19.Command = clickCommand2;
            inputBinding19.CommandContext = this.radMenuItem14;
            chordModifier20.AltModifier = false;
            chordModifier20.ControlModifier = true;
            chordModifier20.ShiftModifier = false;
            chord20.ChordModifier = chordModifier20;
            chord20.Keys = "Ctrl+V";
            inputBinding20.Chord = chord20;
            inputBinding20.Command = clickCommand2;
            inputBinding20.CommandContext = this.radMenuItem15;
            chordModifier21.AltModifier = false;
            chordModifier21.ControlModifier = true;
            chordModifier21.ShiftModifier = false;
            chord21.ChordModifier = chordModifier21;
            chord21.Keys = "Ctrl+F";
            inputBinding21.Chord = chord21;
            inputBinding21.Command = clickCommand2;
            inputBinding21.CommandContext = this.radMenuItem17;
            chordModifier22.AltModifier = false;
            chordModifier22.ControlModifier = true;
            chordModifier22.ShiftModifier = false;
            chord22.ChordModifier = chordModifier22;
            chord22.Keys = "Ctrl+H";
            inputBinding22.Chord = chord22;
            inputBinding22.Command = clickCommand2;
            inputBinding22.CommandContext = this.radMenuItem19;
            chordModifier23.AltModifier = false;
            chordModifier23.ControlModifier = true;
            chordModifier23.ShiftModifier = false;
            chord23.ChordModifier = chordModifier23;
            chord23.Keys = "Ctrl+G";
            inputBinding23.Chord = chord23;
            inputBinding23.Command = clickCommand2;
            inputBinding23.CommandContext = this.radMenuItem20;
            chordModifier24.AltModifier = false;
            chordModifier24.ControlModifier = true;
            chordModifier24.ShiftModifier = false;
            chord24.ChordModifier = chordModifier24;
            chord24.Keys = "Ctrl+A";
            inputBinding24.Chord = chord24;
            inputBinding24.Command = clickCommand2;
            inputBinding24.CommandContext = this.radMenuItem21;
            this.radMenuDemo.CommandBindings.AddRange(new Telerik.WinControls.Keyboard.InputBinding[] {
            inputBinding13,
            inputBinding14,
            inputBinding15,
            inputBinding16,
            inputBinding17,
            inputBinding18,
            inputBinding19,
            inputBinding20,
            inputBinding21,
            inputBinding22,
            inputBinding23,
            inputBinding24});
            this.radMenuDemo.DropDownAnimationEnabled = true;
            this.radMenuDemo.DropDownAnimationFrames = 7;
            this.radMenuDemo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2});
            this.radMenuDemo.Location = new System.Drawing.Point(0, 0);
            this.radMenuDemo.Name = "radMenuDemo";
            // 
            // 
            // 
            this.radMenuDemo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radMenuDemo.Size = new System.Drawing.Size(500, 20);
            this.radMenuDemo.TabIndex = 5;
            this.radMenuDemo.Text = "radMenu1";
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "File";
            this.radMenuItem1.AccessibleName = "File";
            this.radMenuItem1.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem4,
            this.radMenuItem5,
            this.radMenuItem6,
            this.radMenuItem7,
            this.radMenuSeparatorItem1,
            this.radMenuItem8,
            this.radMenuItem9,
            this.radMenuSeparatorItem2,
            this.radMenuItem10});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.ShowArrow = false;
            this.radMenuItem1.Text = "File";
            this.radMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem7
            // 
            this.radMenuItem7.AccessibleDescription = "Save As...";
            this.radMenuItem7.AccessibleName = "Save As...";
            this.radMenuItem7.Name = "radMenuItem7";
            this.radMenuItem7.Text = "Save As...";
            this.radMenuItem7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "New item";
            this.radMenuSeparatorItem1.AccessibleName = "New item";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "New item";
            this.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem8
            // 
            this.radMenuItem8.AccessibleDescription = "Page Setup...";
            this.radMenuItem8.AccessibleName = "Page Setup...";
            this.radMenuItem8.Name = "radMenuItem8";
            this.radMenuItem8.Text = "Page Setup...";
            this.radMenuItem8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.AccessibleDescription = "New item";
            this.radMenuSeparatorItem2.AccessibleName = "New item";
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Text = "New item";
            this.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem10
            // 
            this.radMenuItem10.AccessibleDescription = "Exit";
            this.radMenuItem10.AccessibleName = "Exit";
            this.radMenuItem10.Name = "radMenuItem10";
            this.radMenuItem10.Text = "Exit";
            this.radMenuItem10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Edit";
            this.radMenuItem2.AccessibleName = "Edit";
            this.radMenuItem2.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem11,
            this.radMenuItem12,
            this.radMenuSeparatorItem3,
            this.radMenuItem13,
            this.radMenuItem14,
            this.radMenuItem15,
            this.radMenuItem16,
            this.radMenuSeparatorItem4,
            this.radMenuItem17,
            this.radMenuItem18,
            this.radMenuItem19,
            this.radMenuItem20,
            this.radMenuSeparatorItem5,
            this.radMenuItem21});
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.ShowArrow = false;
            this.radMenuItem2.Text = "Edit";
            this.radMenuItem2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem12
            // 
            this.radMenuItem12.AccessibleDescription = "Redo";
            this.radMenuItem12.AccessibleName = "Redo";
            this.radMenuItem12.Name = "radMenuItem12";
            this.radMenuItem12.Text = "Redo";
            this.radMenuItem12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem3
            // 
            this.radMenuSeparatorItem3.AccessibleDescription = "New item";
            this.radMenuSeparatorItem3.AccessibleName = "New item";
            this.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Text = "New item";
            this.radMenuSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem16
            // 
            this.radMenuItem16.AccessibleDescription = "Delete";
            this.radMenuItem16.AccessibleName = "Delete";
            this.radMenuItem16.HintText = "Del";
            this.radMenuItem16.Name = "radMenuItem16";
            this.radMenuItem16.Text = "Delete";
            this.radMenuItem16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem16.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem4
            // 
            this.radMenuSeparatorItem4.AccessibleDescription = "New item";
            this.radMenuSeparatorItem4.AccessibleName = "New item";
            this.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Text = "New item";
            this.radMenuSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem18
            // 
            this.radMenuItem18.AccessibleDescription = "Find Next";
            this.radMenuItem18.AccessibleName = "Find Next";
            this.radMenuItem18.HintText = "F3";
            this.radMenuItem18.Name = "radMenuItem18";
            this.radMenuItem18.Text = "Find Next";
            this.radMenuItem18.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem18.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem5
            // 
            this.radMenuSeparatorItem5.AccessibleDescription = "New item";
            this.radMenuSeparatorItem5.AccessibleName = "New item";
            this.radMenuSeparatorItem5.Name = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Text = "New item";
            this.radMenuSeparatorItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupSettings
            // 
            this.radGroupSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupSettings.Controls.Add(this.radCheckEnabled);
            this.radGroupSettings.Controls.Add(this.radSpinEditorFrames);
            this.radGroupSettings.Controls.Add(this.radLblFrames);
            this.radGroupSettings.Controls.Add(this.radLblAnim);
            this.radGroupSettings.Controls.Add(this.radComboAnimation);
            this.radGroupSettings.FooterText = "";
            this.radGroupSettings.HeaderText = " Animation ";
            this.radGroupSettings.Location = new System.Drawing.Point(10, 5);
            this.radGroupSettings.Name = "radGroupSettings";
            this.radGroupSettings.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupSettings.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupSettings.Size = new System.Drawing.Size(180, 159);
            this.radGroupSettings.TabIndex = 0;
            this.radGroupSettings.Text = " Animation ";
            // 
            // radCheckEnabled
            // 
            this.radCheckEnabled.Location = new System.Drawing.Point(13, 30);
            this.radCheckEnabled.Name = "radCheckEnabled";
            this.radCheckEnabled.Size = new System.Drawing.Size(60, 18);
            this.radCheckEnabled.TabIndex = 3;
            this.radCheckEnabled.Text = "Enabled";
          
            // 
            // radSpinEditorFrames
            // 
            this.radSpinEditorFrames.Location = new System.Drawing.Point(30, 119);
            this.radSpinEditorFrames.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.radSpinEditorFrames.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.radSpinEditorFrames.Name = "radSpinEditorFrames";
            // 
            // 
            // 
            this.radSpinEditorFrames.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditorFrames.Size = new System.Drawing.Size(120, 20);
            this.radSpinEditorFrames.TabIndex = 2;
            this.radSpinEditorFrames.TabStop = false;
            this.radSpinEditorFrames.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});

            // 
            // radLblFrames
            // 
            this.radLblFrames.Location = new System.Drawing.Point(13, 99);
            this.radLblFrames.Name = "radLblFrames";
            this.radLblFrames.Size = new System.Drawing.Size(44, 18);
            this.radLblFrames.TabIndex = 1;
            this.radLblFrames.Text = "Frames:";
            // 
            // radLblAnim
            // 
            this.radLblAnim.Location = new System.Drawing.Point(13, 53);
            this.radLblAnim.Name = "radLblAnim";
            this.radLblAnim.Size = new System.Drawing.Size(118, 18);
            this.radLblAnim.TabIndex = 1;
            this.radLblAnim.Text = "DropDown Animation:";
            // 
            // radComboAnimation
            // 
            this.radComboAnimation.Location = new System.Drawing.Point(30, 73);
            this.radComboAnimation.Name = "radComboAnimation";
            this.radComboAnimation.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // 
            // 
            this.radComboAnimation.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboAnimation.Size = new System.Drawing.Size(120, 20);
            this.radComboAnimation.TabIndex = 0;
         
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radMenuDemo);
            this.radPanel1.ForeColor = System.Drawing.Color.Black;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(500, 350);
            this.radPanel1.TabIndex = 6;
            // 
            // Form1
            // 
            this.Controls.Add(this.radPanel1);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(5, 10, 5, 5);
            this.Size = new System.Drawing.Size(1170, 754);
            this.Controls.SetChildIndex(this.radPanel1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuDemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSettings)).EndInit();
            this.radGroupSettings.ResumeLayout(false);
            this.radGroupSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorFrames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblFrames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblAnim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

        private Telerik.WinControls.UI.RadMenu radMenuDemo;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem5;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem6;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem7;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem8;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem9;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem10;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem11;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem12;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem3;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem13;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem14;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem15;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem16;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem4;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem17;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem18;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem19;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem20;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem5;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem21;
        private Telerik.WinControls.UI.RadGroupBox radGroupSettings;
        private Telerik.WinControls.UI.RadLabel radLblAnim;
        private Telerik.WinControls.UI.RadDropDownList radComboAnimation;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorFrames;
        private Telerik.WinControls.UI.RadLabel radLblFrames;
        private Telerik.WinControls.UI.RadCheckBox radCheckEnabled;
        private Telerik.WinControls.UI.RadPanel radPanel1;
	}
}
