namespace Telerik.Examples.WinControls.MenuStrip.MenuOrientation
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.ThemeSource themeSource1 = new Telerik.WinControls.ThemeSource();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radMenuItemSrc_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItemSrc_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItemSrc_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.radPanelDemoHolder = new Telerik.WinControls.UI.RadPanel();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem3 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem17 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem18 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem19 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem13 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem20 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem21 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem22 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem14 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem15 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem6 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem16 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem23 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem7 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem24 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem25 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem26 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem8 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem27 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem28 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem29 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem30 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem31 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem9 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem32 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem10 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem33 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem34 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem35 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem36 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem37 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem38 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem39 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem11 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem40 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem41 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem42 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem43 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem44 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem45 = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(623, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 705);
            this.settingsPanel.ThemeName = "ControlDefault";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "CloseSolution.bmp");
            // 
            // radMenuItemSrc_1
            // 
            this.radMenuItemSrc_1.AccessibleDescription = "View History";
            this.radMenuItemSrc_1.AccessibleName = "View History";
            this.radMenuItemSrc_1.Name = "radMenuItemSrc_1";
            this.radMenuItemSrc_1.Text = "View History";
            this.radMenuItemSrc_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItemSrc_2
            // 
            this.radMenuItemSrc_2.AccessibleDescription = "Refresh Status";
            this.radMenuItemSrc_2.AccessibleName = "Refresh Status";
            this.radMenuItemSrc_2.Name = "radMenuItemSrc_2";
            this.radMenuItemSrc_2.Text = "Refresh Status";
            this.radMenuItemSrc_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItemSrc_3
            // 
            this.radMenuItemSrc_3.AccessibleDescription = "Compare...";
            this.radMenuItemSrc_3.AccessibleName = "Compare...";
            this.radMenuItemSrc_3.Name = "radMenuItemSrc_3";
            this.radMenuItemSrc_3.Text = "Compare...";
            this.radMenuItemSrc_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_1
            // 
            this.radMenuItem6_1.AccessibleDescription = "How Do I";
            this.radMenuItem6_1.AccessibleName = "How Do I";
            this.radMenuItem6_1.Name = "radMenuItem6_1";
            this.radMenuItem6_1.Text = "How Do I";
            this.radMenuItem6_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_2
            // 
            this.radMenuItem6_2.AccessibleDescription = "Search";
            this.radMenuItem6_2.AccessibleName = "Search";
            this.radMenuItem6_2.Name = "radMenuItem6_2";
            this.radMenuItem6_2.Text = "Search";
            this.radMenuItem6_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_3
            // 
            this.radMenuItem6_3.AccessibleDescription = "Contents";
            this.radMenuItem6_3.AccessibleName = "Contents";
            this.radMenuItem6_3.Name = "radMenuItem6_3";
            this.radMenuItem6_3.Text = "Contents";
            this.radMenuItem6_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_4
            // 
            this.radMenuItem6_4.AccessibleDescription = "Index";
            this.radMenuItem6_4.AccessibleName = "Index";
            this.radMenuItem6_4.Name = "radMenuItem6_4";
            this.radMenuItem6_4.Text = "Index";
            this.radMenuItem6_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_5
            // 
            this.radMenuItem6_5.AccessibleDescription = "Help Favorites";
            this.radMenuItem6_5.AccessibleName = "Help Favorites";
            this.radMenuItem6_5.Name = "radMenuItem6_5";
            this.radMenuItem6_5.Text = "Help Favorites";
            this.radMenuItem6_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_6
            // 
            this.radMenuItem6_6.AccessibleDescription = "Dynamic Help";
            this.radMenuItem6_6.AccessibleName = "Dynamic Help";
            this.radMenuItem6_6.Name = "radMenuItem6_6";
            this.radMenuItem6_6.Text = "Dynamic Help";
            this.radMenuItem6_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_7
            // 
            this.radMenuItem6_7.AccessibleDescription = "Index Results";
            this.radMenuItem6_7.AccessibleName = "Index Results";
            this.radMenuItem6_7.Name = "radMenuItem6_7";
            this.radMenuItem6_7.Text = "Index Results";
            this.radMenuItem6_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_8
            // 
            this.radMenuItem6_8.AccessibleDescription = "Customer Feedback Options...";
            this.radMenuItem6_8.AccessibleName = "Customer Feedback Options...";
            this.radMenuItem6_8.Name = "radMenuItem6_8";
            this.radMenuItem6_8.Text = "Customer Feedback Options...";
            this.radMenuItem6_8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_9
            // 
            this.radMenuItem6_9.AccessibleDescription = "Register Product...";
            this.radMenuItem6_9.AccessibleName = "Register Product...";
            this.radMenuItem6_9.Name = "radMenuItem6_9";
            this.radMenuItem6_9.Text = "Register Product...";
            this.radMenuItem6_9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_10
            // 
            this.radMenuItem6_10.AccessibleDescription = "Check for Update";
            this.radMenuItem6_10.AccessibleName = "Check for Update";
            this.radMenuItem6_10.Name = "radMenuItem6_10";
            this.radMenuItem6_10.Text = "Check for Update";
            this.radMenuItem6_10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_11
            // 
            this.radMenuItem6_11.AccessibleDescription = "Technical Support";
            this.radMenuItem6_11.AccessibleName = "Technical Support";
            this.radMenuItem6_11.Name = "radMenuItem6_11";
            this.radMenuItem6_11.Text = "Technical Support";
            this.radMenuItem6_11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_12
            // 
            this.radMenuItem6_12.AccessibleDescription = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.AccessibleName = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.Name = "radMenuItem6_12";
            this.radMenuItem6_12.Text = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButton2
            // 
            this.radButton2.Image = global::Telerik.Examples.WinControls.Properties.Resources.button2;
            this.radButton2.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButton2.Location = new System.Drawing.Point(425, 155);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(133, 89);
            this.radButton2.TabIndex = 4;
            this.radButton2.Text = "Switch Text Orientation";
            this.radButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;

            // 
            // radButton1
            // 
            this.radButton1.Image = global::Telerik.Examples.WinControls.Properties.Resources.button1;
            this.radButton1.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButton1.Location = new System.Drawing.Point(173, 155);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(138, 89);
            this.radButton1.TabIndex = 3;
            this.radButton1.Text = "Switch Menu Orientation";
            this.radButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;

            // 
            // radThemeManager1
            // 
            themeSource1.StorageType = Telerik.WinControls.ThemeStorageType.Resource;
            themeSource1.ThemeLocation = "Telerik.Examples.WinControls.Resources.MenuButtonTheme.xml";
            this.radThemeManager1.LoadedThemes.AddRange(new Telerik.WinControls.ThemeSource[] {
            themeSource1});
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.BackgroundImage = global::Telerik.Examples.WinControls.Properties.Resources.background;
            this.radPanelDemoHolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.radPanelDemoHolder.Controls.Add(this.radMenu1);
            this.radPanelDemoHolder.Controls.Add(this.radButton2);
            this.radPanelDemoHolder.Controls.Add(this.radButton1);
            this.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black;
            this.radPanelDemoHolder.Location = new System.Drawing.Point(0, 0);
            this.radPanelDemoHolder.Name = "radPanelDemoHolder";
            this.radPanelDemoHolder.Size = new System.Drawing.Size(618, 354);
            this.radPanelDemoHolder.TabIndex = 15;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radPanelDemoHolder.GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanelDemoHolder.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // radMenu1
            // 
            this.radMenu1.Dock = System.Windows.Forms.DockStyle.None;
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2,
            this.radMenuItem3,
            this.radMenuItem4});
            this.radMenu1.Location = new System.Drawing.Point(33, 30);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(525, 20);
            this.radMenu1.TabIndex = 5;
            this.radMenu1.Text = "radMenu1";
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "File";
            this.radMenuItem1.AccessibleName = "File";
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem5,
            this.radMenuItem6,
            this.radMenuSeparatorItem1,
            this.radMenuItem7,
            this.radMenuSeparatorItem2,
            this.radMenuItem8,
            this.radMenuItem9,
            this.radMenuSeparatorItem3,
            this.radMenuItem10,
            this.radMenuItem11,
            this.radMenuItem12,
            this.radMenuSeparatorItem4,
            this.radMenuItem13,
            this.radMenuSeparatorItem5,
            this.radMenuItem14,
            this.radMenuItem15,
            this.radMenuSeparatorItem6,
            this.radMenuItem16,
            this.radMenuItem23,
            this.radMenuSeparatorItem7,
            this.radMenuItem24});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "File";
            this.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem5
            // 
            this.radMenuItem5.AccessibleDescription = "New";
            this.radMenuItem5.AccessibleName = "New";
            this.radMenuItem5.Name = "radMenuItem5";
            this.radMenuItem5.Text = "New";
            this.radMenuItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6
            // 
            this.radMenuItem6.AccessibleDescription = "Open";
            this.radMenuItem6.AccessibleName = "Open";
            this.radMenuItem6.Name = "radMenuItem6";
            this.radMenuItem6.Text = "Open";
            this.radMenuItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem7
            // 
            this.radMenuItem7.AccessibleDescription = "Add";
            this.radMenuItem7.AccessibleName = "Add";
            this.radMenuItem7.Name = "radMenuItem7";
            this.radMenuItem7.Text = "Add";
            this.radMenuItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.AccessibleDescription = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.AccessibleName = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem8
            // 
            this.radMenuItem8.AccessibleDescription = "Close";
            this.radMenuItem8.AccessibleName = "Close";
            this.radMenuItem8.Name = "radMenuItem8";
            this.radMenuItem8.Text = "Close";
            this.radMenuItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem9
            // 
            this.radMenuItem9.AccessibleDescription = "Close Solution";
            this.radMenuItem9.AccessibleName = "Close Solution";
            this.radMenuItem9.Name = "radMenuItem9";
            this.radMenuItem9.Text = "Close Solution";
            this.radMenuItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem3
            // 
            this.radMenuSeparatorItem3.AccessibleDescription = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.AccessibleName = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Text = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem10
            // 
            this.radMenuItem10.AccessibleDescription = "Advanced Save Options...";
            this.radMenuItem10.AccessibleName = "Advanced Save Options...";
            this.radMenuItem10.Name = "radMenuItem10";
            this.radMenuItem10.Text = "Advanced Save Options...";
            this.radMenuItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem11
            // 
            this.radMenuItem11.AccessibleDescription = "Save All";
            this.radMenuItem11.AccessibleName = "Save All";
            this.radMenuItem11.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem17,
            this.radMenuItem18,
            this.radMenuItem19});
            this.radMenuItem11.Name = "radMenuItem11";
            this.radMenuItem11.Text = "Save All";
            this.radMenuItem11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem17
            // 
            this.radMenuItem17.AccessibleDescription = "radMenuItem17";
            this.radMenuItem17.AccessibleName = "radMenuItem17";
            this.radMenuItem17.Name = "radMenuItem17";
            this.radMenuItem17.Text = "radMenuItem17";
            this.radMenuItem17.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem18
            // 
            this.radMenuItem18.AccessibleDescription = "radMenuItem18";
            this.radMenuItem18.AccessibleName = "radMenuItem18";
            this.radMenuItem18.Name = "radMenuItem18";
            this.radMenuItem18.Text = "radMenuItem18";
            this.radMenuItem18.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem19
            // 
            this.radMenuItem19.AccessibleDescription = "radMenuItem19";
            this.radMenuItem19.AccessibleName = "radMenuItem19";
            this.radMenuItem19.Name = "radMenuItem19";
            this.radMenuItem19.Text = "radMenuItem19";
            this.radMenuItem19.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem12
            // 
            this.radMenuItem12.AccessibleDescription = "Export Template...";
            this.radMenuItem12.AccessibleName = "Export Template...";
            this.radMenuItem12.Name = "radMenuItem12";
            this.radMenuItem12.Text = "Export Template...";
            this.radMenuItem12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem4
            // 
            this.radMenuSeparatorItem4.AccessibleDescription = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.AccessibleName = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Text = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem13
            // 
            this.radMenuItem13.AccessibleDescription = "Source Control";
            this.radMenuItem13.AccessibleName = "Source Control";
            this.radMenuItem13.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem20,
            this.radMenuItem21,
            this.radMenuItem22});
            this.radMenuItem13.Name = "radMenuItem13";
            this.radMenuItem13.Text = "Source Control";
            this.radMenuItem13.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem20
            // 
            this.radMenuItem20.AccessibleDescription = "View History";
            this.radMenuItem20.AccessibleName = "View History";
            this.radMenuItem20.Name = "radMenuItem20";
            this.radMenuItem20.Text = "View History";
            this.radMenuItem20.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem21
            // 
            this.radMenuItem21.AccessibleDescription = "Refresh Status";
            this.radMenuItem21.AccessibleName = "Refresh Status";
            this.radMenuItem21.Name = "radMenuItem21";
            this.radMenuItem21.Text = "Refresh Status";
            this.radMenuItem21.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem22
            // 
            this.radMenuItem22.AccessibleDescription = "Compare...";
            this.radMenuItem22.AccessibleName = "Compare...";
            this.radMenuItem22.Name = "radMenuItem22";
            this.radMenuItem22.Text = "Compare...";
            this.radMenuItem22.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem5
            // 
            this.radMenuSeparatorItem5.AccessibleDescription = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.AccessibleName = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Name = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Text = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem14
            // 
            this.radMenuItem14.AccessibleDescription = "Page Setup...";
            this.radMenuItem14.AccessibleName = "Page Setup...";
            this.radMenuItem14.Name = "radMenuItem14";
            this.radMenuItem14.Text = "Page Setup...";
            this.radMenuItem14.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem15
            // 
            this.radMenuItem15.AccessibleDescription = "Print...";
            this.radMenuItem15.AccessibleName = "Print...";
            this.radMenuItem15.Name = "radMenuItem15";
            this.radMenuItem15.Text = "Print...";
            this.radMenuItem15.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem6
            // 
            this.radMenuSeparatorItem6.AccessibleDescription = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.AccessibleName = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Name = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Text = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem16
            // 
            this.radMenuItem16.AccessibleDescription = "Recent Files";
            this.radMenuItem16.AccessibleName = "Recent Files";
            this.radMenuItem16.Name = "radMenuItem16";
            this.radMenuItem16.Text = "Recent Files";
            this.radMenuItem16.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem23
            // 
            this.radMenuItem23.AccessibleDescription = "Recent Projects";
            this.radMenuItem23.AccessibleName = "Recent Projects";
            this.radMenuItem23.Name = "radMenuItem23";
            this.radMenuItem23.Text = "Recent Projects";
            this.radMenuItem23.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem7
            // 
            this.radMenuSeparatorItem7.AccessibleDescription = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.AccessibleName = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Name = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Text = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem24
            // 
            this.radMenuItem24.AccessibleDescription = "Exit";
            this.radMenuItem24.AccessibleName = "Exit";
            this.radMenuItem24.Name = "radMenuItem24";
            this.radMenuItem24.Text = "Exit";
            this.radMenuItem24.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Edit";
            this.radMenuItem2.AccessibleName = "Edit";
            this.radMenuItem2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem25,
            this.radMenuItem26,
            this.radMenuSeparatorItem8,
            this.radMenuItem27,
            this.radMenuItem28,
            this.radMenuItem29,
            this.radMenuItem30,
            this.radMenuItem31,
            this.radMenuSeparatorItem9,
            this.radMenuItem32,
            this.radMenuSeparatorItem10,
            this.radMenuItem33,
            this.radMenuItem34,
            this.radMenuItem35,
            this.radMenuItem36,
            this.radMenuItem37,
            this.radMenuItem38,
            this.radMenuItem39,
            this.radMenuSeparatorItem11,
            this.radMenuItem40});
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "Edit";
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem25
            // 
            this.radMenuItem25.AccessibleDescription = "Undo";
            this.radMenuItem25.AccessibleName = "Undo";
            this.radMenuItem25.Name = "radMenuItem25";
            this.radMenuItem25.Text = "Undo";
            this.radMenuItem25.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem26
            // 
            this.radMenuItem26.AccessibleDescription = "Redo";
            this.radMenuItem26.AccessibleName = "Redo";
            this.radMenuItem26.Name = "radMenuItem26";
            this.radMenuItem26.Text = "Redo";
            this.radMenuItem26.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem8
            // 
            this.radMenuSeparatorItem8.AccessibleDescription = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.AccessibleName = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Name = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Text = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem27
            // 
            this.radMenuItem27.AccessibleDescription = "Cut";
            this.radMenuItem27.AccessibleName = "Cut";
            this.radMenuItem27.Name = "radMenuItem27";
            this.radMenuItem27.Text = "Cut";
            this.radMenuItem27.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem28
            // 
            this.radMenuItem28.AccessibleDescription = "Copy";
            this.radMenuItem28.AccessibleName = "Copy";
            this.radMenuItem28.Name = "radMenuItem28";
            this.radMenuItem28.Text = "Copy";
            this.radMenuItem28.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem29
            // 
            this.radMenuItem29.AccessibleDescription = "Paste";
            this.radMenuItem29.AccessibleName = "Paste";
            this.radMenuItem29.Name = "radMenuItem29";
            this.radMenuItem29.Text = "Paste";
            this.radMenuItem29.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem30
            // 
            this.radMenuItem30.AccessibleDescription = "Cycle Clipboard Ring";
            this.radMenuItem30.AccessibleName = "Cycle Clipboard Ring";
            this.radMenuItem30.Name = "radMenuItem30";
            this.radMenuItem30.Text = "Cycle Clipboard Ring";
            this.radMenuItem30.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem31
            // 
            this.radMenuItem31.AccessibleDescription = "Delete";
            this.radMenuItem31.AccessibleName = "Delete";
            this.radMenuItem31.Name = "radMenuItem31";
            this.radMenuItem31.Text = "Delete";
            this.radMenuItem31.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem9
            // 
            this.radMenuSeparatorItem9.AccessibleDescription = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.AccessibleName = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.Name = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.Text = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem32
            // 
            this.radMenuItem32.AccessibleDescription = "Select All";
            this.radMenuItem32.AccessibleName = "Select All";
            this.radMenuItem32.Name = "radMenuItem32";
            this.radMenuItem32.Text = "Select All";
            this.radMenuItem32.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem10
            // 
            this.radMenuSeparatorItem10.AccessibleDescription = "radMenuSeparatorItem10";
            this.radMenuSeparatorItem10.AccessibleName = "radMenuSeparatorItem10";
            this.radMenuSeparatorItem10.Name = "radMenuSeparatorItem10";
            this.radMenuSeparatorItem10.Text = "radMenuSeparatorItem10";
            this.radMenuSeparatorItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem33
            // 
            this.radMenuItem33.AccessibleDescription = "Find and Replace";
            this.radMenuItem33.AccessibleName = "Find and Replace";
            this.radMenuItem33.Name = "radMenuItem33";
            this.radMenuItem33.Text = "Find and Replace";
            this.radMenuItem33.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem34
            // 
            this.radMenuItem34.AccessibleDescription = "Go To...";
            this.radMenuItem34.AccessibleName = "Go To...";
            this.radMenuItem34.Name = "radMenuItem34";
            this.radMenuItem34.Text = "Go To...";
            this.radMenuItem34.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem35
            // 
            this.radMenuItem35.AccessibleDescription = "Show Source File";
            this.radMenuItem35.AccessibleName = "Show Source File";
            this.radMenuItem35.Name = "radMenuItem35";
            this.radMenuItem35.Text = "Show Source File";
            this.radMenuItem35.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem36
            // 
            this.radMenuItem36.AccessibleDescription = "Insert File as Text...";
            this.radMenuItem36.AccessibleName = "Insert File as Text...";
            this.radMenuItem36.Name = "radMenuItem36";
            this.radMenuItem36.Text = "Insert File as Text...";
            this.radMenuItem36.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem37
            // 
            this.radMenuItem37.AccessibleDescription = "Advanced";
            this.radMenuItem37.AccessibleName = "Advanced";
            this.radMenuItem37.Name = "radMenuItem37";
            this.radMenuItem37.Text = "Advanced";
            this.radMenuItem37.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem38
            // 
            this.radMenuItem38.AccessibleDescription = "Bookmakrs";
            this.radMenuItem38.AccessibleName = "Bookmakrs";
            this.radMenuItem38.Name = "radMenuItem38";
            this.radMenuItem38.Text = "Bookmakrs";
            this.radMenuItem38.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem39
            // 
            this.radMenuItem39.AccessibleDescription = "Outlining";
            this.radMenuItem39.AccessibleName = "Outlining";
            this.radMenuItem39.Name = "radMenuItem39";
            this.radMenuItem39.Text = "Outlining";
            this.radMenuItem39.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem11
            // 
            this.radMenuSeparatorItem11.AccessibleDescription = "radMenuSeparatorItem11";
            this.radMenuSeparatorItem11.AccessibleName = "radMenuSeparatorItem11";
            this.radMenuSeparatorItem11.Name = "radMenuSeparatorItem11";
            this.radMenuSeparatorItem11.Text = "radMenuSeparatorItem11";
            this.radMenuSeparatorItem11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem40
            // 
            this.radMenuItem40.AccessibleDescription = "InteliSence";
            this.radMenuItem40.AccessibleName = "InteliSence";
            this.radMenuItem40.Name = "radMenuItem40";
            this.radMenuItem40.Text = "InteliSence";
            this.radMenuItem40.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.AccessibleDescription = "Build";
            this.radMenuItem3.AccessibleName = "Build";
            this.radMenuItem3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem41,
            this.radMenuItem42,
            this.radMenuItem43});
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Text = "Build";
            this.radMenuItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem41
            // 
            this.radMenuItem41.AccessibleDescription = "Build Solution";
            this.radMenuItem41.AccessibleName = "Build Solution";
            this.radMenuItem41.Name = "radMenuItem41";
            this.radMenuItem41.Text = "Build Solution";
            this.radMenuItem41.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem42
            // 
            this.radMenuItem42.AccessibleDescription = "Build Examples";
            this.radMenuItem42.AccessibleName = "Build Examples";
            this.radMenuItem42.Name = "radMenuItem42";
            this.radMenuItem42.Text = "Build Examples";
            this.radMenuItem42.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem43
            // 
            this.radMenuItem43.AccessibleDescription = "Publish Examples";
            this.radMenuItem43.AccessibleName = "Publish Examples";
            this.radMenuItem43.Name = "radMenuItem43";
            this.radMenuItem43.Text = "Publish Examples";
            this.radMenuItem43.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.AccessibleDescription = "Data";
            this.radMenuItem4.AccessibleName = "Data";
            this.radMenuItem4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem44,
            this.radMenuItem45});
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Text = "Data";
            this.radMenuItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem44
            // 
            this.radMenuItem44.AccessibleDescription = "Show Data Sources";
            this.radMenuItem44.AccessibleName = "Show Data Sources";
            this.radMenuItem44.Name = "radMenuItem44";
            this.radMenuItem44.Text = "Show Data Sources";
            this.radMenuItem44.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem45
            // 
            this.radMenuItem45.AccessibleDescription = "Add New Data Source...";
            this.radMenuItem45.AccessibleName = "Add New Data Source...";
            this.radMenuItem45.Name = "radMenuItem45";
            this.radMenuItem45.Text = "Add New Data Source...";
            this.radMenuItem45.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // Form1
            // 
            this.Controls.Add(this.radPanelDemoHolder);
            this.MaximumSize = new System.Drawing.Size(1280, 994);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2, 35, 2, 4);
            this.Size = new System.Drawing.Size(1170, 671);
            this.Controls.SetChildIndex(this.radPanelDemoHolder, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_10;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_11;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_12;
        private System.Windows.Forms.ImageList imageList1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_3;
		private Telerik.WinControls.UI.RadButton radButton2;
		private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private Telerik.WinControls.UI.RadPanel radPanelDemoHolder;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem7;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem10;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem11;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem17;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem18;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem19;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem12;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem13;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem14;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem15;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem16;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem20;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem21;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem22;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem23;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem24;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem25;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem26;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem27;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem28;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem29;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem30;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem31;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem32;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem10;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem33;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem34;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem35;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem36;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem37;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem38;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem39;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem11;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem40;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem41;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem42;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem43;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem44;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem45;
	}
}