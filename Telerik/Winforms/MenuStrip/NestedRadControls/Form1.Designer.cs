namespace Telerik.Examples.WinControls.MenuStrip.NestedRadControls
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.RadListDataItem radListDataItem19 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem20 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem21 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem22 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem23 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem24 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem25 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radMenuDemo = new Telerik.WinControls.UI.RadMenu();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem3 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItemSrc_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItemSrc_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItemSrc_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem6 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuComboItem1 = new Telerik.WinControls.UI.RadMenuComboItem();
            this.radMenuComboItem2 = new Telerik.WinControls.UI.RadMenuComboItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuComboItem3 = new Telerik.WinControls.UI.RadMenuComboItem();
            this.radMenuComboItem4 = new Telerik.WinControls.UI.RadMenuComboItem();
            this.radMenuSeparatorItem8 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem2_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem9 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem2_8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_13 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_14 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_15 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_16 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_17 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_18 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_19 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_20 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_21 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuDemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem1.ComboBoxElement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem2.ComboBoxElement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem3.ComboBoxElement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem4.ComboBoxElement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1023, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 735);

            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "CloseSolution.bmp");
            // 
            // radMenuDemo
            // 
            this.radMenuDemo.AllowMerge = false;
            this.radMenuDemo.BackColor = System.Drawing.Color.Transparent;
            this.radMenuDemo.ImageList = this.imageList1;
            this.radMenuDemo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2});
            this.radMenuDemo.Location = new System.Drawing.Point(0, 0);
            this.radMenuDemo.Name = "radMenuDemo";
            // 
            // 
            // 
            this.radMenuDemo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radMenuDemo.Size = new System.Drawing.Size(500, 20);
            this.radMenuDemo.TabIndex = 0;
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "File";
            this.radMenuItem1.AccessibleName = "File";
            this.radMenuItem1.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1_1,
            this.radMenuItem1_2,
            this.radMenuSeparatorItem1,
            this.radMenuItem1_3,
            this.radMenuSeparatorItem2,
            this.radMenuItem1_4,
            this.radMenuItem1_5,
            this.radMenuSeparatorItem3,
            this.radMenuItem1_6,
            this.radMenuItem1_7,
            this.radMenuSeparatorItem4,
            this.radMenuItem1_9,
            this.radMenuSeparatorItem6,
            this.radMenuComboItem1,
            this.radMenuComboItem2});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.PopupDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radMenuItem1.ShowArrow = false;
            this.radMenuItem1.Text = "File";
            this.radMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_1
            // 
            this.radMenuItem1_1.AccessibleDescription = "New";
            this.radMenuItem1_1.AccessibleName = "New";
            this.radMenuItem1_1.Name = "radMenuItem1_1";
            this.radMenuItem1_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem1_1.Text = "New";
            this.radMenuItem1_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_2
            // 
            this.radMenuItem1_2.AccessibleDescription = "Open";
            this.radMenuItem1_2.AccessibleName = "Open";
            this.radMenuItem1_2.Name = "radMenuItem1_2";
            this.radMenuItem1_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem1_2.Text = "Open";
            this.radMenuItem1_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_3
            // 
            this.radMenuItem1_3.AccessibleDescription = "Add";
            this.radMenuItem1_3.AccessibleName = "Add";
            this.radMenuItem1_3.Name = "radMenuItem1_3";
            this.radMenuItem1_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem1_3.Text = "Add";
            this.radMenuItem1_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_4
            // 
            this.radMenuItem1_4.AccessibleDescription = "Close";
            this.radMenuItem1_4.AccessibleName = "Close";
            this.radMenuItem1_4.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItem1_4.Image")));
            this.radMenuItem1_4.ImageIndex = 0;
            this.radMenuItem1_4.Name = "radMenuItem1_4";
            this.radMenuItem1_4.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem1_4.Text = "Close";
            this.radMenuItem1_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_5
            // 
            this.radMenuItem1_5.AccessibleDescription = "Close Solution";
            this.radMenuItem1_5.AccessibleName = "Close Solution";
            this.radMenuItem1_5.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItem1_5.Image")));
            this.radMenuItem1_5.ImageIndex = 5;
            this.radMenuItem1_5.Name = "radMenuItem1_5";
            this.radMenuItem1_5.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem1_5.Text = "Close Solution";
            this.radMenuItem1_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem3
            // 
            this.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_6
            // 
            this.radMenuItem1_6.AccessibleDescription = "Advanced Save Options...";
            this.radMenuItem1_6.AccessibleName = "Advanced Save Options...";
            this.radMenuItem1_6.Name = "radMenuItem1_6";
            this.radMenuItem1_6.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem1_6.Text = "Advanced Save Options...";
            this.radMenuItem1_6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_7
            // 
            this.radMenuItem1_7.AccessibleDescription = "Save All";
            this.radMenuItem1_7.AccessibleName = "Save All";
            this.radMenuItem1_7.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItem1_7.Image")));
            this.radMenuItem1_7.ImageIndex = 0;
            this.radMenuItem1_7.Name = "radMenuItem1_7";
            this.radMenuItem1_7.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem1_7.Text = "Save All";
            this.radMenuItem1_7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem4
            // 
            this.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_9
            // 
            this.radMenuItem1_9.AccessibleDescription = "Source Control";
            this.radMenuItem1_9.AccessibleName = "Source Control";
            this.radMenuItem1_9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItemSrc_1,
            this.radMenuItemSrc_2,
            this.radMenuItemSrc_3});
            this.radMenuItem1_9.Name = "radMenuItem1_9";
            this.radMenuItem1_9.PopupDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.radMenuItem1_9.Text = "Source Control";
            this.radMenuItem1_9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItemSrc_1
            // 
            this.radMenuItemSrc_1.AccessibleDescription = "View History";
            this.radMenuItemSrc_1.AccessibleName = "View History";
            this.radMenuItemSrc_1.Name = "radMenuItemSrc_1";
            this.radMenuItemSrc_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItemSrc_1.Text = "View History";
            this.radMenuItemSrc_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItemSrc_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItemSrc_2
            // 
            this.radMenuItemSrc_2.AccessibleDescription = "Refresh Status";
            this.radMenuItemSrc_2.AccessibleName = "Refresh Status";
            this.radMenuItemSrc_2.Name = "radMenuItemSrc_2";
            this.radMenuItemSrc_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItemSrc_2.Text = "Refresh Status";
            this.radMenuItemSrc_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItemSrc_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItemSrc_3
            // 
            this.radMenuItemSrc_3.AccessibleDescription = "Compare...";
            this.radMenuItemSrc_3.AccessibleName = "Compare...";
            this.radMenuItemSrc_3.Name = "radMenuItemSrc_3";
            this.radMenuItemSrc_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItemSrc_3.Text = "Compare...";
            this.radMenuItemSrc_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItemSrc_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem6
            // 
            this.radMenuSeparatorItem6.Name = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuComboItem1
            // 
            // 
            // 
            // 
            this.radMenuComboItem1.ComboBoxElement.ArrowButtonMinWidth = 16;
            this.radMenuComboItem1.ComboBoxElement.AutoCompleteAppend = null;
            this.radMenuComboItem1.ComboBoxElement.AutoCompleteDataSource = null;
            this.radMenuComboItem1.ComboBoxElement.AutoCompleteDisplayMember = null;
            this.radMenuComboItem1.ComboBoxElement.AutoCompleteSuggest = null;
            this.radMenuComboItem1.ComboBoxElement.AutoCompleteValueMember = null;
            this.radMenuComboItem1.ComboBoxElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.radMenuComboItem1.ComboBoxElement.DataMember = "";
            this.radMenuComboItem1.ComboBoxElement.DataSource = null;
            this.radMenuComboItem1.ComboBoxElement.DefaultItemsCountInDropDown = 6;
            this.radMenuComboItem1.ComboBoxElement.DefaultValue = null;
            this.radMenuComboItem1.ComboBoxElement.DisplayMember = "";
            this.radMenuComboItem1.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radMenuComboItem1.ComboBoxElement.DropDownAnimationEnabled = true;
            this.radMenuComboItem1.ComboBoxElement.DropDownMinSize = new System.Drawing.Size(250, 0);
            this.radMenuComboItem1.ComboBoxElement.DropDownSizingMode = ((Telerik.WinControls.UI.SizingMode)((Telerik.WinControls.UI.SizingMode.RightBottom | Telerik.WinControls.UI.SizingMode.UpDown)));
            this.radMenuComboItem1.ComboBoxElement.DropDownWidth = 250;
            this.radMenuComboItem1.ComboBoxElement.EditableElementText = "";
            this.radMenuComboItem1.ComboBoxElement.EditorElement = this.radMenuComboItem1.ComboBoxElement;
            this.radMenuComboItem1.ComboBoxElement.EditorManager = null;
            this.radMenuComboItem1.ComboBoxElement.Filter = null;
            this.radMenuComboItem1.ComboBoxElement.FilterExpression = "";
            this.radMenuComboItem1.ComboBoxElement.Focusable = true;
            this.radMenuComboItem1.ComboBoxElement.FormatString = "";
            this.radMenuComboItem1.ComboBoxElement.FormattingEnabled = true;
            this.radMenuComboItem1.ComboBoxElement.ItemHeight = 18;
            radListDataItem19.Text = "1 c:\\Projects\\Form1.cs";
            radListDataItem19.TextWrap = true;
            radListDataItem20.Text = "2 c:\\Projects\\RadMenu.cs";
            radListDataItem20.TextWrap = true;
            radListDataItem21.Text = "3 c:\\Projects\\RadComboBox.cs";
            radListDataItem21.TextWrap = true;
            radListDataItem22.Text = "4 c:\\Projects\\Program.cs";
            radListDataItem22.TextWrap = true;
            radListDataItem23.Text = "5 c:\\Projects\\MyTheme.xml";
            radListDataItem23.TextWrap = true;
            radListDataItem24.Text = "6 c:\\Projects\\Form2.cs";
            radListDataItem24.TextWrap = true;
            radListDataItem25.Text = "7 C:\\Projects\\Items\\RadComboBoxItem.cs";
            radListDataItem25.TextWrap = true;
            this.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem19);
            this.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem20);
            this.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem21);
            this.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem22);
            this.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem23);
            this.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem24);
            this.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem25);
            this.radMenuComboItem1.ComboBoxElement.MaxDropDownItems = 0;
            this.radMenuComboItem1.ComboBoxElement.MaxLength = 32767;
            this.radMenuComboItem1.ComboBoxElement.MaxValue = null;
            this.radMenuComboItem1.ComboBoxElement.MinSize = new System.Drawing.Size(100, 20);
            this.radMenuComboItem1.ComboBoxElement.MinValue = null;
            this.radMenuComboItem1.ComboBoxElement.NullText = "Select recent file...";
            this.radMenuComboItem1.ComboBoxElement.NullValue = null;
            this.radMenuComboItem1.ComboBoxElement.OwnerOffset = 0;
            this.radMenuComboItem1.ComboBoxElement.ShowImageInEditorArea = true;
            this.radMenuComboItem1.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radMenuComboItem1.ComboBoxElement.Value = null;
            this.radMenuComboItem1.ComboBoxElement.ValueMember = "";
            this.radMenuComboItem1.Name = "radMenuComboItem1";
            this.radMenuComboItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radMenuComboItem1.GetChildAt(3))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radMenuComboItem1.GetChildAt(3))).MinSize = new System.Drawing.Size(100, 20);
            // 
            // radMenuComboItem2
            // 
            this.radMenuComboItem2.AccessibleDescription = "New item";
            this.radMenuComboItem2.AccessibleName = "New item";
            // 
            // 
            // 
            this.radMenuComboItem2.ComboBoxElement.ArrowButtonMinWidth = 16;
            this.radMenuComboItem2.ComboBoxElement.AutoCompleteAppend = null;
            this.radMenuComboItem2.ComboBoxElement.AutoCompleteDataSource = null;
            this.radMenuComboItem2.ComboBoxElement.AutoCompleteDisplayMember = null;
            this.radMenuComboItem2.ComboBoxElement.AutoCompleteSuggest = null;
            this.radMenuComboItem2.ComboBoxElement.AutoCompleteValueMember = null;
            this.radMenuComboItem2.ComboBoxElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.radMenuComboItem2.ComboBoxElement.DataMember = "";
            this.radMenuComboItem2.ComboBoxElement.DataSource = null;
            this.radMenuComboItem2.ComboBoxElement.DefaultItemsCountInDropDown = 6;
            this.radMenuComboItem2.ComboBoxElement.DefaultValue = null;
            this.radMenuComboItem2.ComboBoxElement.DisplayMember = "";
            this.radMenuComboItem2.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radMenuComboItem2.ComboBoxElement.DropDownAnimationEnabled = true;
            this.radMenuComboItem2.ComboBoxElement.DropDownMinSize = new System.Drawing.Size(250, 0);
            this.radMenuComboItem2.ComboBoxElement.DropDownSizingMode = ((Telerik.WinControls.UI.SizingMode)((Telerik.WinControls.UI.SizingMode.RightBottom | Telerik.WinControls.UI.SizingMode.UpDown)));
            this.radMenuComboItem2.ComboBoxElement.DropDownWidth = 250;
            this.radMenuComboItem2.ComboBoxElement.EditableElementText = "";
            this.radMenuComboItem2.ComboBoxElement.EditorElement = this.radMenuComboItem2.ComboBoxElement;
            this.radMenuComboItem2.ComboBoxElement.EditorManager = null;
            this.radMenuComboItem2.ComboBoxElement.Filter = null;
            this.radMenuComboItem2.ComboBoxElement.FilterExpression = "";
            this.radMenuComboItem2.ComboBoxElement.Focusable = true;
            this.radMenuComboItem2.ComboBoxElement.FormatString = "";
            this.radMenuComboItem2.ComboBoxElement.FormattingEnabled = true;
            this.radMenuComboItem2.ComboBoxElement.ItemHeight = 18;
            radListDataItem1.Text = "1 C:\\DemoApplication\\DemoApplication.sln";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "2 C:\\TestSolution1\\TestSolution1.sln";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "3 C:\\WindowsApplication1.csproj";
            radListDataItem3.TextWrap = true;
            this.radMenuComboItem2.ComboBoxElement.Items.Add(radListDataItem1);
            this.radMenuComboItem2.ComboBoxElement.Items.Add(radListDataItem2);
            this.radMenuComboItem2.ComboBoxElement.Items.Add(radListDataItem3);
            this.radMenuComboItem2.ComboBoxElement.MaxDropDownItems = 0;
            this.radMenuComboItem2.ComboBoxElement.MaxLength = 32767;
            this.radMenuComboItem2.ComboBoxElement.MaxValue = null;
            this.radMenuComboItem2.ComboBoxElement.MinSize = new System.Drawing.Size(100, 20);
            this.radMenuComboItem2.ComboBoxElement.MinValue = null;
            this.radMenuComboItem2.ComboBoxElement.NullText = "Select recent project...";
            this.radMenuComboItem2.ComboBoxElement.NullValue = null;
            this.radMenuComboItem2.ComboBoxElement.OwnerOffset = 0;
            this.radMenuComboItem2.ComboBoxElement.ShowImageInEditorArea = true;
            this.radMenuComboItem2.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radMenuComboItem2.ComboBoxElement.Value = null;
            this.radMenuComboItem2.ComboBoxElement.ValueMember = "";
            this.radMenuComboItem2.Name = "radMenuComboItem2";
            this.radMenuComboItem2.Text = "New item";
            this.radMenuComboItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radMenuComboItem2.GetChildAt(3))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radMenuComboItem2.GetChildAt(3))).MinSize = new System.Drawing.Size(100, 20);
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Edit";
            this.radMenuItem2.AccessibleName = "Edit";
            this.radMenuItem2.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuComboItem3,
            this.radMenuComboItem4,
            this.radMenuSeparatorItem8,
            this.radMenuItem2_3,
            this.radMenuItem2_4,
            this.radMenuItem2_5,
            this.radMenuItem2_6,
            this.radMenuItem2_7,
            this.radMenuSeparatorItem9,
            this.radMenuItem2_8});
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.PopupDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radMenuItem2.ShowArrow = false;
            this.radMenuItem2.Text = "Edit";
            this.radMenuItem2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuComboItem3
            // 
            this.radMenuComboItem3.AccessibleDescription = "Undo";
            this.radMenuComboItem3.AccessibleName = "Undo";
            // 
            // 
            // 
            this.radMenuComboItem3.ComboBoxElement.ArrowButtonMinWidth = 16;
            this.radMenuComboItem3.ComboBoxElement.AutoCompleteAppend = null;
            this.radMenuComboItem3.ComboBoxElement.AutoCompleteDataSource = null;
            this.radMenuComboItem3.ComboBoxElement.AutoCompleteDisplayMember = null;
            this.radMenuComboItem3.ComboBoxElement.AutoCompleteSuggest = null;
            this.radMenuComboItem3.ComboBoxElement.AutoCompleteValueMember = null;
            this.radMenuComboItem3.ComboBoxElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.radMenuComboItem3.ComboBoxElement.DataMember = "";
            this.radMenuComboItem3.ComboBoxElement.DataSource = null;
            this.radMenuComboItem3.ComboBoxElement.DefaultItemsCountInDropDown = 6;
            this.radMenuComboItem3.ComboBoxElement.DefaultValue = null;
            this.radMenuComboItem3.ComboBoxElement.DisplayMember = "";
            this.radMenuComboItem3.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radMenuComboItem3.ComboBoxElement.DropDownAnimationEnabled = true;
            this.radMenuComboItem3.ComboBoxElement.DropDownSizingMode = ((Telerik.WinControls.UI.SizingMode)((Telerik.WinControls.UI.SizingMode.RightBottom | Telerik.WinControls.UI.SizingMode.UpDown)));
            this.radMenuComboItem3.ComboBoxElement.EditableElementText = "";
            this.radMenuComboItem3.ComboBoxElement.EditorElement = this.radMenuComboItem3.ComboBoxElement;
            this.radMenuComboItem3.ComboBoxElement.EditorManager = null;
            this.radMenuComboItem3.ComboBoxElement.Filter = null;
            this.radMenuComboItem3.ComboBoxElement.FilterExpression = "";
            this.radMenuComboItem3.ComboBoxElement.Focusable = true;
            this.radMenuComboItem3.ComboBoxElement.FormatString = "";
            this.radMenuComboItem3.ComboBoxElement.FormattingEnabled = true;
            this.radMenuComboItem3.ComboBoxElement.ItemHeight = 18;
            radListDataItem4.Text = "Copy text";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = " Type \"This is my..\"";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "Font color";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = " Font size";
            radListDataItem7.TextWrap = true;
            this.radMenuComboItem3.ComboBoxElement.Items.Add(radListDataItem4);
            this.radMenuComboItem3.ComboBoxElement.Items.Add(radListDataItem5);
            this.radMenuComboItem3.ComboBoxElement.Items.Add(radListDataItem6);
            this.radMenuComboItem3.ComboBoxElement.Items.Add(radListDataItem7);
            this.radMenuComboItem3.ComboBoxElement.MaxDropDownItems = 0;
            this.radMenuComboItem3.ComboBoxElement.MaxLength = 32767;
            this.radMenuComboItem3.ComboBoxElement.MaxValue = null;
            this.radMenuComboItem3.ComboBoxElement.MinSize = new System.Drawing.Size(100, 20);
            this.radMenuComboItem3.ComboBoxElement.MinValue = null;
            this.radMenuComboItem3.ComboBoxElement.NullValue = null;
            this.radMenuComboItem3.ComboBoxElement.OwnerOffset = 0;
            this.radMenuComboItem3.ComboBoxElement.ShowImageInEditorArea = true;
            this.radMenuComboItem3.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radMenuComboItem3.ComboBoxElement.Value = null;
            this.radMenuComboItem3.ComboBoxElement.ValueMember = "";
            this.radMenuComboItem3.Name = "radMenuComboItem3";
            this.radMenuComboItem3.Text = "Undo";
            this.radMenuComboItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radMenuComboItem3.GetChildAt(3))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radMenuComboItem3.GetChildAt(3))).MinSize = new System.Drawing.Size(100, 20);
            // 
            // radMenuComboItem4
            // 
            this.radMenuComboItem4.AccessibleDescription = "Redo";
            this.radMenuComboItem4.AccessibleName = "Redo";
            // 
            // 
            // 
            this.radMenuComboItem4.ComboBoxElement.ArrowButtonMinWidth = 16;
            this.radMenuComboItem4.ComboBoxElement.AutoCompleteAppend = null;
            this.radMenuComboItem4.ComboBoxElement.AutoCompleteDataSource = null;
            this.radMenuComboItem4.ComboBoxElement.AutoCompleteDisplayMember = null;
            this.radMenuComboItem4.ComboBoxElement.AutoCompleteSuggest = null;
            this.radMenuComboItem4.ComboBoxElement.AutoCompleteValueMember = null;
            this.radMenuComboItem4.ComboBoxElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.radMenuComboItem4.ComboBoxElement.DataMember = "";
            this.radMenuComboItem4.ComboBoxElement.DataSource = null;
            this.radMenuComboItem4.ComboBoxElement.DefaultItemsCountInDropDown = 6;
            this.radMenuComboItem4.ComboBoxElement.DefaultValue = null;
            this.radMenuComboItem4.ComboBoxElement.DisplayMember = "";
            this.radMenuComboItem4.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radMenuComboItem4.ComboBoxElement.DropDownAnimationEnabled = true;
            this.radMenuComboItem4.ComboBoxElement.DropDownSizingMode = ((Telerik.WinControls.UI.SizingMode)((Telerik.WinControls.UI.SizingMode.RightBottom | Telerik.WinControls.UI.SizingMode.UpDown)));
            this.radMenuComboItem4.ComboBoxElement.EditableElementText = "";
            this.radMenuComboItem4.ComboBoxElement.EditorElement = this.radMenuComboItem4.ComboBoxElement;
            this.radMenuComboItem4.ComboBoxElement.EditorManager = null;
            this.radMenuComboItem4.ComboBoxElement.Filter = null;
            this.radMenuComboItem4.ComboBoxElement.FilterExpression = "";
            this.radMenuComboItem4.ComboBoxElement.Focusable = true;
            this.radMenuComboItem4.ComboBoxElement.FormatString = "";
            this.radMenuComboItem4.ComboBoxElement.FormattingEnabled = true;
            this.radMenuComboItem4.ComboBoxElement.ItemHeight = 18;
            radListDataItem8.Text = "Format paragraph";
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "Ordered list";
            radListDataItem9.TextWrap = true;
            radListDataItem10.Text = "Page Break";
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "Rotate";
            radListDataItem11.TextWrap = true;
            this.radMenuComboItem4.ComboBoxElement.Items.Add(radListDataItem8);
            this.radMenuComboItem4.ComboBoxElement.Items.Add(radListDataItem9);
            this.radMenuComboItem4.ComboBoxElement.Items.Add(radListDataItem10);
            this.radMenuComboItem4.ComboBoxElement.Items.Add(radListDataItem11);
            this.radMenuComboItem4.ComboBoxElement.MaxDropDownItems = 0;
            this.radMenuComboItem4.ComboBoxElement.MaxLength = 32767;
            this.radMenuComboItem4.ComboBoxElement.MaxValue = null;
            this.radMenuComboItem4.ComboBoxElement.MinSize = new System.Drawing.Size(100, 20);
            this.radMenuComboItem4.ComboBoxElement.MinValue = null;
            this.radMenuComboItem4.ComboBoxElement.NullValue = null;
            this.radMenuComboItem4.ComboBoxElement.OwnerOffset = 0;
            this.radMenuComboItem4.ComboBoxElement.ShowImageInEditorArea = true;
            this.radMenuComboItem4.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radMenuComboItem4.ComboBoxElement.Value = null;
            this.radMenuComboItem4.ComboBoxElement.ValueMember = "";
            this.radMenuComboItem4.Name = "radMenuComboItem4";
            this.radMenuComboItem4.Text = "Redo";
            this.radMenuComboItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radMenuComboItem4.GetChildAt(3))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radMenuComboItem4.GetChildAt(3))).MinSize = new System.Drawing.Size(100, 20);
            // 
            // radMenuSeparatorItem8
            // 
            this.radMenuSeparatorItem8.Name = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_3
            // 
            this.radMenuItem2_3.AccessibleDescription = "Cut";
            this.radMenuItem2_3.AccessibleName = "Cut";
            this.radMenuItem2_3.Name = "radMenuItem2_3";
            this.radMenuItem2_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem2_3.Text = "Cut";
            this.radMenuItem2_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_4
            // 
            this.radMenuItem2_4.AccessibleDescription = "Copy";
            this.radMenuItem2_4.AccessibleName = "Copy";
            this.radMenuItem2_4.Name = "radMenuItem2_4";
            this.radMenuItem2_4.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem2_4.Text = "Copy";
            this.radMenuItem2_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_5
            // 
            this.radMenuItem2_5.AccessibleDescription = "Paste";
            this.radMenuItem2_5.AccessibleName = "Paste";
            this.radMenuItem2_5.Name = "radMenuItem2_5";
            this.radMenuItem2_5.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem2_5.Text = "Paste";
            this.radMenuItem2_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_6
            // 
            this.radMenuItem2_6.AccessibleDescription = "Cycle Clipboard Ring";
            this.radMenuItem2_6.AccessibleName = "Cycle Clipboard Ring";
            this.radMenuItem2_6.Name = "radMenuItem2_6";
            this.radMenuItem2_6.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem2_6.Text = "Cycle Clipboard Ring";
            this.radMenuItem2_6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_7
            // 
            this.radMenuItem2_7.AccessibleDescription = "Delete";
            this.radMenuItem2_7.AccessibleName = "Delete";
            this.radMenuItem2_7.Name = "radMenuItem2_7";
            this.radMenuItem2_7.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem2_7.Text = "Delete";
            this.radMenuItem2_7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem9
            // 
            this.radMenuSeparatorItem9.Name = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_8
            // 
            this.radMenuItem2_8.AccessibleDescription = "Select All";
            this.radMenuItem2_8.AccessibleName = "Select All";
            this.radMenuItem2_8.Name = "radMenuItem2_8";
            this.radMenuItem2_8.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem2_8.Text = "Select All";
            this.radMenuItem2_8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_1
            // 
            this.radMenuItem3_1.AccessibleDescription = "Code";
            this.radMenuItem3_1.AccessibleName = "Code";
            this.radMenuItem3_1.Name = "radMenuItem3_1";
            this.radMenuItem3_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_1.Text = "Code";
            this.radMenuItem3_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_2
            // 
            this.radMenuItem3_2.AccessibleDescription = "Open";
            this.radMenuItem3_2.AccessibleName = "Open";
            this.radMenuItem3_2.Name = "radMenuItem3_2";
            this.radMenuItem3_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_2.Text = "Open";
            this.radMenuItem3_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_3
            // 
            this.radMenuItem3_3.AccessibleDescription = "Open With...";
            this.radMenuItem3_3.AccessibleName = "Open With...";
            this.radMenuItem3_3.Name = "radMenuItem3_3";
            this.radMenuItem3_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_3.Text = "Open With...";
            this.radMenuItem3_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_4
            // 
            this.radMenuItem3_4.AccessibleDescription = "Server Explorer";
            this.radMenuItem3_4.AccessibleName = "Server Explorer";
            this.radMenuItem3_4.Name = "radMenuItem3_4";
            this.radMenuItem3_4.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_4.Text = "Server Explorer";
            this.radMenuItem3_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_5
            // 
            this.radMenuItem3_5.AccessibleDescription = "Solution Explorer";
            this.radMenuItem3_5.AccessibleName = "Solution Explorer";
            this.radMenuItem3_5.Name = "radMenuItem3_5";
            this.radMenuItem3_5.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_5.Text = "Solution Explorer";
            this.radMenuItem3_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_6
            // 
            this.radMenuItem3_6.AccessibleDescription = "Bookmark Window";
            this.radMenuItem3_6.AccessibleName = "Bookmark Window";
            this.radMenuItem3_6.Name = "radMenuItem3_6";
            this.radMenuItem3_6.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_6.Text = "Bookmark Window";
            this.radMenuItem3_6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_7
            // 
            this.radMenuItem3_7.AccessibleDescription = "Class View";
            this.radMenuItem3_7.AccessibleName = "Class View";
            this.radMenuItem3_7.Name = "radMenuItem3_7";
            this.radMenuItem3_7.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_7.Text = "Class View";
            this.radMenuItem3_7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_8
            // 
            this.radMenuItem3_8.AccessibleDescription = "Code Definition Window";
            this.radMenuItem3_8.AccessibleName = "Code Definition Window";
            this.radMenuItem3_8.Name = "radMenuItem3_8";
            this.radMenuItem3_8.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_8.Text = "Code Definition Window";
            this.radMenuItem3_8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_9
            // 
            this.radMenuItem3_9.AccessibleDescription = "Object Browser";
            this.radMenuItem3_9.AccessibleName = "Object Browser";
            this.radMenuItem3_9.Name = "radMenuItem3_9";
            this.radMenuItem3_9.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_9.Text = "Object Browser";
            this.radMenuItem3_9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_10
            // 
            this.radMenuItem3_10.AccessibleDescription = "Error List";
            this.radMenuItem3_10.AccessibleName = "Error List";
            this.radMenuItem3_10.Name = "radMenuItem3_10";
            this.radMenuItem3_10.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_10.Text = "Error List";
            this.radMenuItem3_10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_11
            // 
            this.radMenuItem3_11.AccessibleDescription = "Output";
            this.radMenuItem3_11.AccessibleName = "Output";
            this.radMenuItem3_11.Name = "radMenuItem3_11";
            this.radMenuItem3_11.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_11.Text = "Output";
            this.radMenuItem3_11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_12
            // 
            this.radMenuItem3_12.AccessibleDescription = "Properties Window";
            this.radMenuItem3_12.AccessibleName = "Properties Window";
            this.radMenuItem3_12.Name = "radMenuItem3_12";
            this.radMenuItem3_12.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_12.Text = "Properties Window";
            this.radMenuItem3_12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_13
            // 
            this.radMenuItem3_13.AccessibleDescription = "Task List";
            this.radMenuItem3_13.AccessibleName = "Task List";
            this.radMenuItem3_13.Name = "radMenuItem3_13";
            this.radMenuItem3_13.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_13.Text = "Task List";
            this.radMenuItem3_13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_13.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_14
            // 
            this.radMenuItem3_14.AccessibleDescription = "Toolbox";
            this.radMenuItem3_14.AccessibleName = "Toolbox";
            this.radMenuItem3_14.Name = "radMenuItem3_14";
            this.radMenuItem3_14.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_14.Text = "Toolbox";
            this.radMenuItem3_14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_14.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_15
            // 
            this.radMenuItem3_15.AccessibleDescription = "Find Results";
            this.radMenuItem3_15.AccessibleName = "Find Results";
            this.radMenuItem3_15.Name = "radMenuItem3_15";
            this.radMenuItem3_15.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_15.Text = "Find Results";
            this.radMenuItem3_15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_15.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_16
            // 
            this.radMenuItem3_16.AccessibleDescription = "Other Windows";
            this.radMenuItem3_16.AccessibleName = "Other Windows";
            this.radMenuItem3_16.Name = "radMenuItem3_16";
            this.radMenuItem3_16.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_16.Text = "Other Windows";
            this.radMenuItem3_16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_16.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_17
            // 
            this.radMenuItem3_17.AccessibleDescription = "Toolbars";
            this.radMenuItem3_17.AccessibleName = "Toolbars";
            this.radMenuItem3_17.Name = "radMenuItem3_17";
            this.radMenuItem3_17.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_17.Text = "Toolbars";
            this.radMenuItem3_17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_17.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_18
            // 
            this.radMenuItem3_18.AccessibleDescription = "Full Screen";
            this.radMenuItem3_18.AccessibleName = "Full Screen";
            this.radMenuItem3_18.Name = "radMenuItem3_18";
            this.radMenuItem3_18.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_18.Text = "Full Screen";
            this.radMenuItem3_18.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_18.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_19
            // 
            this.radMenuItem3_19.AccessibleDescription = "Pending Checkins";
            this.radMenuItem3_19.AccessibleName = "Pending Checkins";
            this.radMenuItem3_19.Name = "radMenuItem3_19";
            this.radMenuItem3_19.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_19.Text = "Pending Checkins";
            this.radMenuItem3_19.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_19.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_20
            // 
            this.radMenuItem3_20.AccessibleDescription = "Navigate Backward";
            this.radMenuItem3_20.AccessibleName = "Navigate Backward";
            this.radMenuItem3_20.Name = "radMenuItem3_20";
            this.radMenuItem3_20.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_20.Text = "Navigate Backward";
            this.radMenuItem3_20.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_20.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_21
            // 
            this.radMenuItem3_21.AccessibleDescription = "Navigate Forward";
            this.radMenuItem3_21.AccessibleName = "Navigate Forward";
            this.radMenuItem3_21.Name = "radMenuItem3_21";
            this.radMenuItem3_21.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem3_21.Text = "Navigate Forward";
            this.radMenuItem3_21.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_21.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4_1
            // 
            this.radMenuItem4_1.AccessibleDescription = "Build Solution";
            this.radMenuItem4_1.AccessibleName = "Build Solution";
            this.radMenuItem4_1.Name = "radMenuItem4_1";
            this.radMenuItem4_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem4_1.Text = "Build Solution";
            this.radMenuItem4_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4_2
            // 
            this.radMenuItem4_2.AccessibleDescription = "Build Examples";
            this.radMenuItem4_2.AccessibleName = "Build Examples";
            this.radMenuItem4_2.Name = "radMenuItem4_2";
            this.radMenuItem4_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem4_2.Text = "Build Examples";
            this.radMenuItem4_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4_3
            // 
            this.radMenuItem4_3.AccessibleDescription = "Publish Examples";
            this.radMenuItem4_3.AccessibleName = "Publish Examples";
            this.radMenuItem4_3.Name = "radMenuItem4_3";
            this.radMenuItem4_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem4_3.Text = "Publish Examples";
            this.radMenuItem4_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem5_1
            // 
            this.radMenuItem5_1.AccessibleDescription = "Show Data Sources";
            this.radMenuItem5_1.AccessibleName = "Show Data Sources";
            this.radMenuItem5_1.Name = "radMenuItem5_1";
            this.radMenuItem5_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem5_1.Text = "Show Data Sources";
            this.radMenuItem5_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem5_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem5_2
            // 
            this.radMenuItem5_2.AccessibleDescription = "Add New Data Source...";
            this.radMenuItem5_2.AccessibleName = "Add New Data Source...";
            this.radMenuItem5_2.Name = "radMenuItem5_2";
            this.radMenuItem5_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem5_2.Text = "Add New Data Source...";
            this.radMenuItem5_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem5_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_1
            // 
            this.radMenuItem6_1.AccessibleDescription = "How Do I";
            this.radMenuItem6_1.AccessibleName = "How Do I";
            this.radMenuItem6_1.Name = "radMenuItem6_1";
            this.radMenuItem6_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_1.Text = "How Do I";
            this.radMenuItem6_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_2
            // 
            this.radMenuItem6_2.AccessibleDescription = "Search";
            this.radMenuItem6_2.AccessibleName = "Search";
            this.radMenuItem6_2.Name = "radMenuItem6_2";
            this.radMenuItem6_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_2.Text = "Search";
            this.radMenuItem6_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_3
            // 
            this.radMenuItem6_3.AccessibleDescription = "Contents";
            this.radMenuItem6_3.AccessibleName = "Contents";
            this.radMenuItem6_3.Name = "radMenuItem6_3";
            this.radMenuItem6_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_3.Text = "Contents";
            this.radMenuItem6_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_4
            // 
            this.radMenuItem6_4.AccessibleDescription = "Index";
            this.radMenuItem6_4.AccessibleName = "Index";
            this.radMenuItem6_4.Name = "radMenuItem6_4";
            this.radMenuItem6_4.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_4.Text = "Index";
            this.radMenuItem6_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_5
            // 
            this.radMenuItem6_5.AccessibleDescription = "Help Favorites";
            this.radMenuItem6_5.AccessibleName = "Help Favorites";
            this.radMenuItem6_5.Name = "radMenuItem6_5";
            this.radMenuItem6_5.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_5.Text = "Help Favorites";
            this.radMenuItem6_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_6
            // 
            this.radMenuItem6_6.AccessibleDescription = "Dynamic Help";
            this.radMenuItem6_6.AccessibleName = "Dynamic Help";
            this.radMenuItem6_6.Name = "radMenuItem6_6";
            this.radMenuItem6_6.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_6.Text = "Dynamic Help";
            this.radMenuItem6_6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_7
            // 
            this.radMenuItem6_7.AccessibleDescription = "Index Results";
            this.radMenuItem6_7.AccessibleName = "Index Results";
            this.radMenuItem6_7.Name = "radMenuItem6_7";
            this.radMenuItem6_7.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_7.Text = "Index Results";
            this.radMenuItem6_7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_8
            // 
            this.radMenuItem6_8.AccessibleDescription = "Customer Feedback Options...";
            this.radMenuItem6_8.AccessibleName = "Customer Feedback Options...";
            this.radMenuItem6_8.Name = "radMenuItem6_8";
            this.radMenuItem6_8.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_8.Text = "Customer Feedback Options...";
            this.radMenuItem6_8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_9
            // 
            this.radMenuItem6_9.AccessibleDescription = "Register Product...";
            this.radMenuItem6_9.AccessibleName = "Register Product...";
            this.radMenuItem6_9.Name = "radMenuItem6_9";
            this.radMenuItem6_9.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_9.Text = "Register Product...";
            this.radMenuItem6_9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_10
            // 
            this.radMenuItem6_10.AccessibleDescription = "Check for Update";
            this.radMenuItem6_10.AccessibleName = "Check for Update";
            this.radMenuItem6_10.Name = "radMenuItem6_10";
            this.radMenuItem6_10.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_10.Text = "Check for Update";
            this.radMenuItem6_10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_11
            // 
            this.radMenuItem6_11.AccessibleDescription = "Technical Support";
            this.radMenuItem6_11.AccessibleName = "Technical Support";
            this.radMenuItem6_11.Name = "radMenuItem6_11";
            this.radMenuItem6_11.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_11.Text = "Technical Support";
            this.radMenuItem6_11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_12
            // 
            this.radMenuItem6_12.AccessibleDescription = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.AccessibleName = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.Name = "radMenuItem6_12";
            this.radMenuItem6_12.PopupDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radMenuItem6_12.Text = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radMenuDemo);
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            // 
            // 
            // 
            this.radPanel1.RootElement.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.radPanel1.Size = new System.Drawing.Size(500, 350);
            this.radPanel1.TabIndex = 2;
            // 
            // Form1
            // 
            this.Controls.Add(this.radPanel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1170, 671);
            this.Controls.SetChildIndex(this.radPanel1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuDemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem1.ComboBoxElement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem2.ComboBoxElement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem3.ComboBoxElement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem4.ComboBoxElement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion


        private Telerik.WinControls.UI.RadMenu radMenuDemo;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_10;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_11;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_12;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_13;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_14;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_15;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_16;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_17;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_18;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_19;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_20;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_21;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_10;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_11;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_12;
        private System.Windows.Forms.ImageList imageList1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_3;
        private Telerik.WinControls.UI.RadMenuComboItem radMenuComboItem1;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem3;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem4;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem6;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem8;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem9;
        private Telerik.WinControls.UI.RadMenuComboItem radMenuComboItem2;
        private Telerik.WinControls.UI.RadMenuComboItem radMenuComboItem3;
        private Telerik.WinControls.UI.RadMenuComboItem radMenuComboItem4;
        private Telerik.WinControls.UI.RadPanel radPanel1;
	}
}