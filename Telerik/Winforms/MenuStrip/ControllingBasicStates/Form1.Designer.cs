namespace Telerik.Examples.WinControls.MenuStrip.ControllingBasicStates
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radMenuDemo = new Telerik.WinControls.UI.RadMenu();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem13 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem14 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem15 = new Telerik.WinControls.UI.RadMenuItem();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuDemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1023, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 818);
            // 
            // radMenuDemo
            // 
            this.radMenuDemo.AllowMerge = false;
            this.radMenuDemo.BackColor = System.Drawing.Color.Transparent;
            this.radMenuDemo.ForeColor = System.Drawing.Color.Black;
            this.radMenuDemo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2,
            this.radMenuItem3});
            this.radMenuDemo.Location = new System.Drawing.Point(0, 0);
            this.radMenuDemo.Name = "radMenuDemo";
            // 
            // 
            // 
            this.radMenuDemo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radMenuDemo.Size = new System.Drawing.Size(600, 20);
            this.radMenuDemo.TabIndex = 0;
            this.radMenuDemo.Text = "radMenu1";
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "Add items at runtime";
            this.radMenuItem1.AccessibleName = "Add items at runtime";
            this.radMenuItem1.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem4,
            this.radMenuItem5,
            this.radMenuItem6,
            this.radMenuItem7,
            this.radMenuSeparatorItem1});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.ShowArrow = false;
            this.radMenuItem1.Text = "Add items at runtime";
            this.radMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.AccessibleDescription = "Add a new Text file";
            this.radMenuItem4.AccessibleName = "Add a new Text file";
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Text = "Add a new Text file";
            this.radMenuItem4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
        
            // 
            // radMenuItem5
            // 
            this.radMenuItem5.AccessibleDescription = "Add a new XML file";
            this.radMenuItem5.AccessibleName = "Add a new XML file";
            this.radMenuItem5.Name = "radMenuItem5";
            this.radMenuItem5.Text = "Add a new XML file";
            this.radMenuItem5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
 
            // 
            // radMenuItem6
            // 
            this.radMenuItem6.AccessibleDescription = "Add a new Project";
            this.radMenuItem6.AccessibleName = "Add a new Project";
            this.radMenuItem6.Name = "radMenuItem6";
            this.radMenuItem6.Text = "Add a new Project";
            this.radMenuItem6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
       
            // 
            // radMenuItem7
            // 
            this.radMenuItem7.AccessibleDescription = "Add a new Solution";
            this.radMenuItem7.AccessibleName = "Add a new Solution";
            this.radMenuItem7.Name = "radMenuItem7";
            this.radMenuItem7.Text = "Add a new Solution";
            this.radMenuItem7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;

            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Remove items at runtime";
            this.radMenuItem2.AccessibleName = "Remove items at runtime";
            this.radMenuItem2.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem8,
            this.radMenuItem9,
            this.radMenuItem10,
            this.radMenuItem11});
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.ShowArrow = false;
            this.radMenuItem2.Text = "Remove items at runtime";
            this.radMenuItem2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem8
            // 
            this.radMenuItem8.AccessibleDescription = "Remove this Text file";
            this.radMenuItem8.AccessibleName = "Remove this Text file";
            this.radMenuItem8.Name = "radMenuItem8";
            this.radMenuItem8.Text = "Remove this Text file";
            this.radMenuItem8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
    
            // 
            // radMenuItem9
            // 
            this.radMenuItem9.AccessibleDescription = "Remove this XML file";
            this.radMenuItem9.AccessibleName = "Remove this XML file";
            this.radMenuItem9.Name = "radMenuItem9";
            this.radMenuItem9.Text = "Remove this XML file";
            this.radMenuItem9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;

            // 
            // radMenuItem10
            // 
            this.radMenuItem10.AccessibleDescription = "Remove this Project";
            this.radMenuItem10.AccessibleName = "Remove this Project";
            this.radMenuItem10.Name = "radMenuItem10";
            this.radMenuItem10.Text = "Remove this Project";
            this.radMenuItem10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible;

            // 
            // radMenuItem11
            // 
            this.radMenuItem11.AccessibleDescription = "Remove this Solution";
            this.radMenuItem11.AccessibleName = "Remove this Solution";
            this.radMenuItem11.Name = "radMenuItem11";
            this.radMenuItem11.Text = "Remove this Solution";
            this.radMenuItem11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
          
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.AccessibleDescription = "Disable items at runtime";
            this.radMenuItem3.AccessibleName = "Disable items at runtime";
            this.radMenuItem3.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem12,
            this.radMenuItem13,
            this.radMenuItem14,
            this.radMenuItem15});
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.ShowArrow = false;
            this.radMenuItem3.Text = "Disable items at runtime";
            this.radMenuItem3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem12
            // 
            this.radMenuItem12.AccessibleDescription = "Disable this Text file";
            this.radMenuItem12.AccessibleName = "Disable this Text file";
            this.radMenuItem12.Name = "radMenuItem12";
            this.radMenuItem12.Text = "Disable this Text file";
            this.radMenuItem12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
       
            // 
            // radMenuItem13
            // 
            this.radMenuItem13.AccessibleDescription = "Disable this XML file";
            this.radMenuItem13.AccessibleName = "Disable this XML file";
            this.radMenuItem13.Name = "radMenuItem13";
            this.radMenuItem13.Text = "Disable this XML file";
            this.radMenuItem13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem13.Visibility = Telerik.WinControls.ElementVisibility.Visible;

            // 
            // radMenuItem14
            // 
            this.radMenuItem14.AccessibleDescription = "Disable this Project";
            this.radMenuItem14.AccessibleName = "Disable this Project";
            this.radMenuItem14.Name = "radMenuItem14";
            this.radMenuItem14.Text = "Disable this Project";
            this.radMenuItem14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem14.Visibility = Telerik.WinControls.ElementVisibility.Visible;

            // 
            // radMenuItem15
            // 
            this.radMenuItem15.AccessibleDescription = "Disable this Solution";
            this.radMenuItem15.AccessibleName = "Disable this Solution";
            this.radMenuItem15.Name = "radMenuItem15";
            this.radMenuItem15.Text = "Disable this Solution";
            this.radMenuItem15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem15.Visibility = Telerik.WinControls.ElementVisibility.Visible;
   
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radMenuDemo);
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(600, 350);
            this.radPanel1.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoSize = true;
            this.Controls.Add(this.radPanel1);
            this.MaximumSize = new System.Drawing.Size(1280, 994);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2, 35, 2, 4);
            this.Size = new System.Drawing.Size(1170, 754);
            this.Controls.SetChildIndex(this.radPanel1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuDemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

		private Telerik.WinControls.UI.RadMenu radMenuDemo;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem5;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem6;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem7;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem8;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem9;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem10;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem11;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem12;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem13;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem14;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem15;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
    }
}