﻿namespace Telerik.Examples.WinControls.Map.Shapefile
{
    partial class BookingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxEmail = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBoxPhone = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBoxName = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBoxCardNumber = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBoxExpirationDate = new Telerik.WinControls.UI.RadTextBox();
            this.radLabelPerformance = new Telerik.WinControls.UI.RadLabel();
            this.radLabelTickets = new Telerik.WinControls.UI.RadLabel();
            this.radLabelTotalPrice = new Telerik.WinControls.UI.RadLabel();
            this.radButtonBuyReserve = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxCardNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxExpirationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelPerformance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTickets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTotalPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonBuyReserve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(12, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(78, 19);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Performance:";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(12, 49);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(46, 19);
            this.radLabel2.TabIndex = 0;
            this.radLabel2.Text = "Tickets:";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(12, 76);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(70, 19);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Total price: ";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(12, 129);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(95, 21);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Contact details";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(12, 154);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(36, 19);
            this.radLabel5.TabIndex = 0;
            this.radLabel5.Text = "Email";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(12, 180);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(41, 19);
            this.radLabel6.TabIndex = 0;
            this.radLabel6.Text = "Phone";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.Location = new System.Drawing.Point(12, 206);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(103, 19);
            this.radLabel7.TabIndex = 0;
            this.radLabel7.Text = "Cardholder Name";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(12, 232);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(80, 19);
            this.radLabel8.TabIndex = 0;
            this.radLabel8.Text = "Card Number";
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel9.Location = new System.Drawing.Point(12, 258);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(90, 19);
            this.radLabel9.TabIndex = 0;
            this.radLabel9.Text = "Expiration Date";
            // 
            // radTextBoxEmail
            // 
            this.radTextBoxEmail.Location = new System.Drawing.Point(130, 154);
            this.radTextBoxEmail.Name = "radTextBoxEmail";
            this.radTextBoxEmail.Size = new System.Drawing.Size(184, 20);
            this.radTextBoxEmail.TabIndex = 1;
            // 
            // radTextBoxPhone
            // 
            this.radTextBoxPhone.Location = new System.Drawing.Point(130, 180);
            this.radTextBoxPhone.Name = "radTextBoxPhone";
            this.radTextBoxPhone.Size = new System.Drawing.Size(184, 20);
            this.radTextBoxPhone.TabIndex = 1;
            // 
            // radTextBoxName
            // 
            this.radTextBoxName.Location = new System.Drawing.Point(130, 206);
            this.radTextBoxName.Name = "radTextBoxName";
            this.radTextBoxName.Size = new System.Drawing.Size(184, 20);
            this.radTextBoxName.TabIndex = 1;
            // 
            // radTextBoxCardNumber
            // 
            this.radTextBoxCardNumber.Location = new System.Drawing.Point(130, 232);
            this.radTextBoxCardNumber.Name = "radTextBoxCardNumber";
            this.radTextBoxCardNumber.Size = new System.Drawing.Size(184, 20);
            this.radTextBoxCardNumber.TabIndex = 1;
            // 
            // radTextBoxExpirationDate
            // 
            this.radTextBoxExpirationDate.Location = new System.Drawing.Point(130, 258);
            this.radTextBoxExpirationDate.Name = "radTextBoxExpirationDate";
            this.radTextBoxExpirationDate.Size = new System.Drawing.Size(184, 20);
            this.radTextBoxExpirationDate.TabIndex = 1;
            // 
            // radLabelPerformance
            // 
            this.radLabelPerformance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelPerformance.Location = new System.Drawing.Point(96, 21);
            this.radLabelPerformance.Name = "radLabelPerformance";
            this.radLabelPerformance.Size = new System.Drawing.Size(81, 19);
            this.radLabelPerformance.TabIndex = 0;
            this.radLabelPerformance.Text = "Performance";
            // 
            // radLabelTickets
            // 
            this.radLabelTickets.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelTickets.Location = new System.Drawing.Point(64, 49);
            this.radLabelTickets.Name = "radLabelTickets";
            this.radLabelTickets.Size = new System.Drawing.Size(47, 19);
            this.radLabelTickets.TabIndex = 0;
            this.radLabelTickets.Text = "Tickets";
            // 
            // radLabelTotalPrice
            // 
            this.radLabelTotalPrice.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelTotalPrice.Location = new System.Drawing.Point(88, 76);
            this.radLabelTotalPrice.Name = "radLabelTotalPrice";
            this.radLabelTotalPrice.Size = new System.Drawing.Size(68, 19);
            this.radLabelTotalPrice.TabIndex = 0;
            this.radLabelTotalPrice.Text = "Total price";
            // 
            // radButtonBuyReserve
            // 
            this.radButtonBuyReserve.Location = new System.Drawing.Point(204, 300);
            this.radButtonBuyReserve.Name = "radButtonBuyReserve";
            this.radButtonBuyReserve.Size = new System.Drawing.Size(110, 24);
            this.radButtonBuyReserve.TabIndex = 2;
            this.radButtonBuyReserve.Text = "BUY";
            this.radButtonBuyReserve.Click += new System.EventHandler(this.radButtonBuyReserve_Click);
            // 
            // BookingForm
            // 
            this.AcceptButton = this.radButtonBuyReserve;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 341);
            this.Controls.Add(this.radButtonBuyReserve);
            this.Controls.Add(this.radTextBoxExpirationDate);
            this.Controls.Add(this.radTextBoxCardNumber);
            this.Controls.Add(this.radTextBoxName);
            this.Controls.Add(this.radTextBoxPhone);
            this.Controls.Add(this.radTextBoxEmail);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radLabelTotalPrice);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabelTickets);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabelPerformance);
            this.Controls.Add(this.radLabel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BookingForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Booking Form";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxCardNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxExpirationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelPerformance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTickets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTotalPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonBuyReserve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox radTextBoxEmail;
        private Telerik.WinControls.UI.RadTextBox radTextBoxPhone;
        private Telerik.WinControls.UI.RadTextBox radTextBoxName;
        private Telerik.WinControls.UI.RadTextBox radTextBoxCardNumber;
        private Telerik.WinControls.UI.RadTextBox radTextBoxExpirationDate;
        private Telerik.WinControls.UI.RadLabel radLabelPerformance;
        private Telerik.WinControls.UI.RadLabel radLabelTickets;
        private Telerik.WinControls.UI.RadLabel radLabelTotalPrice;
        private Telerik.WinControls.UI.RadButton radButtonBuyReserve;
    }
}