﻿using Telerik.QuickStart.WinControls;

namespace Telerik.Examples.WinControls.Map.KML
{
    public partial class Form1 : ExternalProcessForm
    {
        protected override string GetExecutablePath()
        {
            return @"\..\..\RadMap\Bin\RadMap.exe";
        }
    }
}