﻿namespace Telerik.Examples.WinControls.Editors.DateTimePicker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLblLongDateFormat = new Telerik.WinControls.UI.RadLabel();
            this.radLblShortDateFormat = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLblTimeFormat = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker3 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radBtnClearAll = new Telerik.WinControls.UI.RadButton();
            this.radGroupExampleSettings = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadio24Hours = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadio12Hours = new Telerik.WinControls.UI.RadRadioButton();
            this.radDateTimePicker4 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker5 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker6 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker7 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblLongDateFormat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblShortDateFormat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblTimeFormat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupExampleSettings)).BeginInit();
            this.radGroupExampleSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadio24Hours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadio12Hours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radLabel2);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker5);
            this.radPanelDemoHolder.Controls.Add(this.radLabel3);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker6);
            this.radPanelDemoHolder.Controls.Add(this.radLabel4);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker7);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker4);
            this.radPanelDemoHolder.Controls.Add(this.radLabel1);
            this.radPanelDemoHolder.Controls.Add(this.radLblLongDateFormat);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker2);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker1);
            this.radPanelDemoHolder.Controls.Add(this.radLblShortDateFormat);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker3);
            this.radPanelDemoHolder.Controls.Add(this.radLblTimeFormat);
            this.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black;
            this.radPanelDemoHolder.Size = new System.Drawing.Size(450, 270);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupExampleSettings);
            this.settingsPanel.Location = new System.Drawing.Point(1023, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 735);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupExampleSettings, 0);
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radDateTimePicker1.Checked = true;
            this.radDateTimePicker1.Location = new System.Drawing.Point(170, 7);
            this.radDateTimePicker1.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker1.Size = new System.Drawing.Size(191, 20);
            this.radDateTimePicker1.TabIndex = 0;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "Thursday, August 23, 2007";
            this.radDateTimePicker1.Value = new System.DateTime(2007, 8, 23, 15, 29, 8, 309);
            // 
            // radLblLongDateFormat
            // 
            this.radLblLongDateFormat.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radLblLongDateFormat.Location = new System.Drawing.Point(63, 8);
            this.radLblLongDateFormat.Name = "radLblLongDateFormat";
            this.radLblLongDateFormat.Size = new System.Drawing.Size(96, 18);
            this.radLblLongDateFormat.TabIndex = 1;
            this.radLblLongDateFormat.Text = "Long date format:";
            // 
            // radLblShortDateFormat
            // 
            this.radLblShortDateFormat.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radLblShortDateFormat.Location = new System.Drawing.Point(62, 43);
            this.radLblShortDateFormat.Name = "radLblShortDateFormat";
            this.radLblShortDateFormat.Size = new System.Drawing.Size(97, 18);
            this.radLblShortDateFormat.TabIndex = 3;
            this.radLblShortDateFormat.Text = "Short date format:";
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radDateTimePicker2.Checked = true;
            this.radDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.radDateTimePicker2.Location = new System.Drawing.Point(170, 42);
            this.radDateTimePicker2.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            this.radDateTimePicker2.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker2.Size = new System.Drawing.Size(191, 20);
            this.radDateTimePicker2.TabIndex = 2;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.Text = "1/1/1980";
            this.radDateTimePicker2.Value = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            // 
            // radLblTimeFormat
            // 
            this.radLblTimeFormat.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radLblTimeFormat.Location = new System.Drawing.Point(89, 78);
            this.radLblTimeFormat.Name = "radLblTimeFormat";
            this.radLblTimeFormat.Size = new System.Drawing.Size(70, 18);
            this.radLblTimeFormat.TabIndex = 5;
            this.radLblTimeFormat.Text = "Time format:";
            // 
            // radDateTimePicker3
            // 
            this.radDateTimePicker3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radDateTimePicker3.Checked = true;
            this.radDateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.radDateTimePicker3.Location = new System.Drawing.Point(170, 77);
            this.radDateTimePicker3.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker3.Name = "radDateTimePicker3";
            this.radDateTimePicker3.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker3.ShowUpDown = true;
            this.radDateTimePicker3.Size = new System.Drawing.Size(191, 20);
            this.radDateTimePicker3.TabIndex = 4;
            this.radDateTimePicker3.TabStop = false;
            this.radDateTimePicker3.Text = "12:00:00 AM";
            this.radDateTimePicker3.Value = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            // 
            // radBtnClearAll
            // 
            this.radBtnClearAll.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radBtnClearAll.Location = new System.Drawing.Point(5, 84);
            this.radBtnClearAll.Name = "radBtnClearAll";
            this.radBtnClearAll.Size = new System.Drawing.Size(170, 23);
            this.radBtnClearAll.TabIndex = 6;
            this.radBtnClearAll.Text = "Clear All";
            // 
            // radGroupExampleSettings
            // 
            this.radGroupExampleSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupExampleSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupExampleSettings.Controls.Add(this.radRadio24Hours);
            this.radGroupExampleSettings.Controls.Add(this.radRadio12Hours);
            this.radGroupExampleSettings.Controls.Add(this.radBtnClearAll);
            this.radGroupExampleSettings.FooterText = "";
            this.radGroupExampleSettings.HeaderMargin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radGroupExampleSettings.HeaderText = " Settings ";
            this.radGroupExampleSettings.Location = new System.Drawing.Point(10, 6);
            this.radGroupExampleSettings.Name = "radGroupExampleSettings";
            this.radGroupExampleSettings.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupExampleSettings.Size = new System.Drawing.Size(180, 122);
            this.radGroupExampleSettings.TabIndex = 7;
            this.radGroupExampleSettings.Text = " Settings ";
            // 
            // radRadio24Hours
            // 
            this.radRadio24Hours.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radRadio24Hours.Location = new System.Drawing.Point(5, 53);
            this.radRadio24Hours.Name = "radRadio24Hours";
            this.radRadio24Hours.Size = new System.Drawing.Size(65, 18);
            this.radRadio24Hours.TabIndex = 7;
            this.radRadio24Hours.Text = "24 Hours";
            // 
            // radRadio12Hours
            // 
            this.radRadio12Hours.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radRadio12Hours.Location = new System.Drawing.Point(5, 31);
            this.radRadio12Hours.Name = "radRadio12Hours";
            this.radRadio12Hours.Size = new System.Drawing.Size(65, 18);
            this.radRadio12Hours.TabIndex = 7;
            this.radRadio12Hours.Text = "12 Hours";
            // 
            // radDateTimePicker4
            // 
            this.radDateTimePicker4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radDateTimePicker4.Checked = true;
            this.radDateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.radDateTimePicker4.Location = new System.Drawing.Point(170, 112);
            this.radDateTimePicker4.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker4.Name = "radDateTimePicker4";
            this.radDateTimePicker4.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker4.Size = new System.Drawing.Size(191, 20);
            this.radDateTimePicker4.TabIndex = 6;
            this.radDateTimePicker4.TabStop = false;
            this.radDateTimePicker4.Text = "1/1/1980";
            this.radDateTimePicker4.Value = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radLabel1.Location = new System.Drawing.Point(5, 113);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(154, 18);
            this.radLabel1.TabIndex = 7;
            this.radLabel1.Text = "Time format with Time Picker:";
            // 
            // radDateTimePicker5
            // 
            this.radDateTimePicker5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radDateTimePicker5.Checked = true;
            this.radDateTimePicker5.Culture = new System.Globalization.CultureInfo("ar-SA");
            this.radDateTimePicker5.Location = new System.Drawing.Point(170, 147);
            this.radDateTimePicker5.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker5.Name = "radDateTimePicker5";
            this.radDateTimePicker5.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker5.Size = new System.Drawing.Size(191, 20);
            this.radDateTimePicker5.TabIndex = 0;
            this.radDateTimePicker5.TabStop = false;
            this.radDateTimePicker5.Text = "10/شعبان/1428";
            this.radDateTimePicker5.Value = new System.DateTime(2007, 8, 23, 15, 29, 8, 309);
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radLabel2.Location = new System.Drawing.Point(68, 183);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(91, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Hijri date format:";
            // 
            // radDateTimePicker6
            // 
            this.radDateTimePicker6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radDateTimePicker6.Checked = true;
            this.radDateTimePicker6.Location = new System.Drawing.Point(170, 217);
            this.radDateTimePicker6.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker6.Name = "radDateTimePicker6";
            this.radDateTimePicker6.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker6.Size = new System.Drawing.Size(191, 20);
            this.radDateTimePicker6.TabIndex = 0;
            this.radDateTimePicker6.TabStop = false;
            this.radDateTimePicker6.Text = "Thursday, August 23, 2007";
            this.radDateTimePicker6.Value = new System.DateTime(2007, 8, 23, 15, 29, 8, 309);
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radLabel3.Location = new System.Drawing.Point(36, 148);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(123, 18);
            this.radLabel3.TabIndex = 1;
            this.radLabel3.Text = "UmAlQura date format:";
            // 
            // radDateTimePicker7
            // 
            this.radDateTimePicker7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radDateTimePicker7.Checked = true;
            this.radDateTimePicker7.Culture = new System.Globalization.CultureInfo("ps-AF");
            this.radDateTimePicker7.Location = new System.Drawing.Point(170, 182);
            this.radDateTimePicker7.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker7.Name = "radDateTimePicker7";
            this.radDateTimePicker7.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker7.Size = new System.Drawing.Size(191, 20);
            this.radDateTimePicker7.TabIndex = 0;
            this.radDateTimePicker7.TabStop = false;
            this.radDateTimePicker7.Text = "1 وږی 1386";
            this.radDateTimePicker7.Value = new System.DateTime(2007, 8, 23, 15, 29, 8, 309);
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radLabel4.Location = new System.Drawing.Point(53, 218);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(106, 18);
            this.radLabel4.TabIndex = 1;
            this.radLabel4.Text = "Persian date format:";
            // 
            // Form1
            // 
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1439, 482);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblLongDateFormat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblShortDateFormat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblTimeFormat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupExampleSettings)).EndInit();
            this.radGroupExampleSettings.ResumeLayout(false);
            this.radGroupExampleSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadio24Hours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadio12Hours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadLabel radLblLongDateFormat;
        private Telerik.WinControls.UI.RadLabel radLblShortDateFormat;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
        private Telerik.WinControls.UI.RadLabel radLblTimeFormat;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker3;
        private Telerik.WinControls.UI.RadButton radBtnClearAll;
        private Telerik.WinControls.UI.RadGroupBox radGroupExampleSettings;
        private Telerik.WinControls.UI.RadRadioButton radRadio24Hours;
        private Telerik.WinControls.UI.RadRadioButton radRadio12Hours;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker4;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker5;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker6;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker7;
        private Telerik.WinControls.UI.RadLabel radLabel4;

    }
}