﻿namespace Telerik.Examples.WinControls.Editors.PopupEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.CartesianArea cartesianArea2 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis2 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis2 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries3 = new Telerik.WinControls.UI.BarSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint11 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint12 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint13 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint14 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint15 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.WinControls.UI.BarSeries barSeries4 = new Telerik.WinControls.UI.BarSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint16 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint17 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint18 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint19 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint20 = new Telerik.Charting.CategoricalDataPoint();
            this.radPopupEditor1 = new Telerik.WinControls.UI.RadPopupEditor();
            this.radPopupContainer1 = new Telerik.WinControls.UI.RadPopupContainer();
            this.radClock1 = new Telerik.WinControls.UI.RadClock();
            this.radCalendar1 = new Telerik.WinControls.UI.RadCalendar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.radPopupContainer2 = new Telerik.WinControls.UI.RadPopupContainer();
            this.radChartView1 = new Telerik.WinControls.UI.RadChartView();
            this.northwindDataSet = new Telerik.Examples.WinControls.DataSources.NorthwindDataSet();
            this.northwindDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radPopupEditor2 = new Telerik.WinControls.UI.RadPopupEditor();
            this.radPopupContainer3 = new Telerik.WinControls.UI.RadPopupContainer();
            this.radTrackBar3 = new Telerik.WinControls.UI.RadTrackBar();
            this.radTrackBar2 = new Telerik.WinControls.UI.RadTrackBar();
            this.radTrackBar1 = new Telerik.WinControls.UI.RadTrackBar();
            this.radPopupEditor3 = new Telerik.WinControls.UI.RadPopupEditor();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer1)).BeginInit();
            this.radPopupContainer1.PanelContainer.SuspendLayout();
            this.radPopupContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radClock1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer2)).BeginInit();
            this.radPopupContainer2.PanelContainer.SuspendLayout();
            this.radPopupContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer3)).BeginInit();
            this.radPopupContainer3.PanelContainer.SuspendLayout();
            this.radPopupContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditor3)).BeginInit();
            this.SuspendLayout();
            // 
            // radPopupEditor1
            // 
            this.radPopupEditor1.AssociatedControl = this.radPopupContainer1;
            this.radPopupEditor1.Location = new System.Drawing.Point(0, 80);
            this.radPopupEditor1.Name = "radPopupEditor1";
            this.radPopupEditor1.ShowTextBox = false;
            this.radPopupEditor1.Size = new System.Drawing.Size(155, 20);
            this.radPopupEditor1.TabIndex = 0;
            this.radPopupEditor1.Text = "radPopupEditor1";
            // 
            // radPopupContainer1
            // 
            this.radPopupContainer1.Location = new System.Drawing.Point(3, 23);
            this.radPopupContainer1.Name = "radPopupContainer1";
            // 
            // radPopupContainer1.PanelContainer
            // 
            this.radPopupContainer1.PanelContainer.Controls.Add(this.radClock1);
            this.radPopupContainer1.PanelContainer.Controls.Add(this.radCalendar1);
            this.radPopupContainer1.PanelContainer.Size = new System.Drawing.Size(383, 196);
            this.radPopupContainer1.Size = new System.Drawing.Size(385, 198);
            this.radPopupContainer1.TabIndex = 1;
            this.radPopupContainer1.Text = "radPopupContainer1";
            // 
            // radClock1
            // 
            this.radClock1.Location = new System.Drawing.Point(242, 29);
            this.radClock1.Name = "radClock1";
            this.radClock1.Size = new System.Drawing.Size(134, 135);
            this.radClock1.TabIndex = 1;
            this.radClock1.Text = "radClock1";
            // 
            // radCalendar1
            // 
            this.radCalendar1.Location = new System.Drawing.Point(3, 3);
            this.radCalendar1.Name = "radCalendar1";
            this.radCalendar1.Size = new System.Drawing.Size(233, 190);
            this.radCalendar1.TabIndex = 0;
            this.radCalendar1.Text = "radCalendar1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // radPopupContainer2
            // 
            this.radPopupContainer2.Location = new System.Drawing.Point(3, 274);
            this.radPopupContainer2.Name = "radPopupContainer2";
            // 
            // radPopupContainer2.PanelContainer
            // 
            this.radPopupContainer2.PanelContainer.Controls.Add(this.radChartView1);
            this.radPopupContainer2.PanelContainer.Size = new System.Drawing.Size(525, 287);
            this.radPopupContainer2.Size = new System.Drawing.Size(527, 289);
            this.radPopupContainer2.TabIndex = 2;
            this.radPopupContainer2.Text = "radPopupContainer2";
            // 
            // radChartView1
            // 
            this.radChartView1.AreaDesign = cartesianArea2;
            categoricalAxis2.IsPrimary = true;
            categoricalAxis2.LabelRotationAngle = 300D;
            categoricalAxis2.Title = "";
            linearAxis2.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis2.IsPrimary = true;
            linearAxis2.LabelRotationAngle = 300D;
            linearAxis2.MajorStep = 10D;
            linearAxis2.TickOrigin = null;
            linearAxis2.Title = "";
            this.radChartView1.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis2,
            linearAxis2});
            this.radChartView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radChartView1.Location = new System.Drawing.Point(0, 0);
            this.radChartView1.Name = "radChartView1";
            categoricalDataPoint11.Category = "A";
            categoricalDataPoint11.Label = 64D;
            categoricalDataPoint11.Value = 64D;
            categoricalDataPoint12.Category = "B";
            categoricalDataPoint12.Label = 68D;
            categoricalDataPoint12.Value = 68D;
            categoricalDataPoint13.Category = "C";
            categoricalDataPoint13.Label = 56D;
            categoricalDataPoint13.Value = 56D;
            categoricalDataPoint14.Category = "D";
            categoricalDataPoint14.Label = 97D;
            categoricalDataPoint14.Value = 97D;
            categoricalDataPoint15.Category = "E";
            categoricalDataPoint15.Label = 99D;
            categoricalDataPoint15.Value = 99D;
            barSeries3.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint11,
            categoricalDataPoint12,
            categoricalDataPoint13,
            categoricalDataPoint14,
            categoricalDataPoint15});
            barSeries3.HorizontalAxis = categoricalAxis2;
            barSeries3.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries3.LegendTitle = null;
            barSeries3.VerticalAxis = linearAxis2;
            categoricalDataPoint16.Category = "A";
            categoricalDataPoint16.Label = 13D;
            categoricalDataPoint16.Value = 13D;
            categoricalDataPoint17.Category = "B";
            categoricalDataPoint17.Label = 59D;
            categoricalDataPoint17.Value = 59D;
            categoricalDataPoint18.Category = "C";
            categoricalDataPoint18.Label = 99D;
            categoricalDataPoint18.Value = 99D;
            categoricalDataPoint19.Category = "D";
            categoricalDataPoint19.Label = 27D;
            categoricalDataPoint19.Value = 27D;
            categoricalDataPoint20.Category = "E";
            categoricalDataPoint20.Label = 66D;
            categoricalDataPoint20.Value = 66D;
            barSeries4.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint16,
            categoricalDataPoint17,
            categoricalDataPoint18,
            categoricalDataPoint19,
            categoricalDataPoint20});
            barSeries4.HorizontalAxis = categoricalAxis2;
            barSeries4.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries4.LegendTitle = null;
            barSeries4.VerticalAxis = linearAxis2;
            this.radChartView1.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries3,
            barSeries4});
            this.radChartView1.ShowGrid = false;
            this.radChartView1.Size = new System.Drawing.Size(525, 287);
            this.radChartView1.TabIndex = 0;
            this.radChartView1.Text = "radChartView1";
            // 
            // northwindDataSet
            // 
            this.northwindDataSet.DataSetName = "NorthwindDataSet";
            this.northwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // northwindDataSetBindingSource
            // 
            this.northwindDataSetBindingSource.DataSource = this.northwindDataSet;
            this.northwindDataSetBindingSource.Position = 0;
            // 
            // radPopupEditor2
            // 
            this.radPopupEditor2.AssociatedControl = this.radPopupContainer2;
            this.radPopupEditor2.Location = new System.Drawing.Point(0, 40);
            this.radPopupEditor2.Name = "radPopupEditor2";
            this.radPopupEditor2.ShowTextBox = false;
            this.radPopupEditor2.Size = new System.Drawing.Size(155, 20);
            this.radPopupEditor2.TabIndex = 3;
            this.radPopupEditor2.Text = "Trends";
            // 
            // radPopupContainer3
            // 
            this.radPopupContainer3.Location = new System.Drawing.Point(4, 613);
            this.radPopupContainer3.Name = "radPopupContainer3";
            // 
            // radPopupContainer3.PanelContainer
            // 
            this.radPopupContainer3.PanelContainer.Controls.Add(this.radTrackBar3);
            this.radPopupContainer3.PanelContainer.Controls.Add(this.radTrackBar2);
            this.radPopupContainer3.PanelContainer.Controls.Add(this.radTrackBar1);
            this.radPopupContainer3.PanelContainer.Size = new System.Drawing.Size(446, 238);
            this.radPopupContainer3.Size = new System.Drawing.Size(448, 240);
            this.radPopupContainer3.TabIndex = 4;
            this.radPopupContainer3.Text = "radPopupContainer3";
            // 
            // radTrackBar3
            // 
            this.radTrackBar3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTrackBar3.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.BottomRight;
            this.radTrackBar3.Location = new System.Drawing.Point(17, 179);
            this.radTrackBar3.Name = "radTrackBar3";
            this.radTrackBar3.Size = new System.Drawing.Size(405, 45);
            this.radTrackBar3.TabIndex = 2;
            this.radTrackBar3.Text = "radTrackBar3";
            this.radTrackBar3.TickStyle = Telerik.WinControls.Enumerations.TickStyles.TopLeft;
            this.radTrackBar3.Value = 15F;            
            // 
            // radTrackBar2
            // 
            this.radTrackBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTrackBar2.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.TopLeft;
            this.radTrackBar2.Location = new System.Drawing.Point(17, 100);
            this.radTrackBar2.Name = "radTrackBar2";
            this.radTrackBar2.Size = new System.Drawing.Size(405, 45);
            this.radTrackBar2.TabIndex = 1;
            this.radTrackBar2.Text = "radTrackBar2";
            this.radTrackBar2.TickStyle = Telerik.WinControls.Enumerations.TickStyles.BottomRight;
            this.radTrackBar2.Value = 5F;            
            // 
            // radTrackBar1
            // 
            this.radTrackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTrackBar1.Location = new System.Drawing.Point(17, 19);
            this.radTrackBar1.Name = "radTrackBar1";
            this.radTrackBar1.Size = new System.Drawing.Size(405, 37);
            this.radTrackBar1.TabIndex = 0;
            this.radTrackBar1.Text = "radTrackBar1";
            this.radTrackBar1.Value = 2F;
            // 
            // radPopupEditor3
            // 
            this.radPopupEditor3.AssociatedControl = this.radPopupContainer3;
            this.radPopupEditor3.Location = new System.Drawing.Point(0, 0);
            this.radPopupEditor3.Name = "radPopupEditor3";
            this.radPopupEditor3.ShowTextBox = false;
            this.radPopupEditor3.Size = new System.Drawing.Size(155, 20);
            this.radPopupEditor3.TabIndex = 5;
            this.radPopupEditor3.Text = "Settings";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1416, 878);
            this.Controls.Add(this.radPopupEditor3);
            this.Controls.Add(this.radPopupContainer3);
            this.Controls.Add(this.radPopupEditor2);
            this.Controls.Add(this.radPopupContainer2);
            this.Controls.Add(this.radPopupContainer1);
            this.Controls.Add(this.radPopupEditor1);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditor1)).EndInit();
            this.radPopupContainer1.PanelContainer.ResumeLayout(false);
            this.radPopupContainer1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer1)).EndInit();
            this.radPopupContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radClock1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).EndInit();
            this.radPopupContainer2.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer2)).EndInit();
            this.radPopupContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditor2)).EndInit();
            this.radPopupContainer3.PanelContainer.ResumeLayout(false);
            this.radPopupContainer3.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer3)).EndInit();
            this.radPopupContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditor3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPopupEditor radPopupEditor1;
        private System.Windows.Forms.Timer timer1;
        private Telerik.WinControls.UI.RadPopupContainer radPopupContainer1;
        private Telerik.WinControls.UI.RadClock radClock1;
        private Telerik.WinControls.UI.RadCalendar radCalendar1;
        private Telerik.WinControls.UI.RadPopupContainer radPopupContainer2;
        private Telerik.WinControls.UI.RadChartView radChartView1;
        private DataSources.NorthwindDataSet northwindDataSet;
        private System.Windows.Forms.BindingSource northwindDataSetBindingSource;
        private Telerik.WinControls.UI.RadPopupEditor radPopupEditor2;
        private Telerik.WinControls.UI.RadPopupContainer radPopupContainer3;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar3;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar2;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar1;
        private Telerik.WinControls.UI.RadPopupEditor radPopupEditor3;
    }
}