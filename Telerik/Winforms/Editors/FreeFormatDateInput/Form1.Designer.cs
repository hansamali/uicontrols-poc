﻿namespace Telerik.Examples.WinControls.Editors.FreeFormatDateInput
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLblCulture = new Telerik.WinControls.UI.RadLabel();
            this.radComboCultures = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radMaskedEditBox1 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radTimePicker1 = new Telerik.WinControls.UI.RadTimePicker();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker3 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblCulture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboCultures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radTimePicker1);
            this.radPanelDemoHolder.Controls.Add(this.radDateTimePicker1);
            this.radPanelDemoHolder.Controls.Add(this.radMaskedEditBox1);
            this.radPanelDemoHolder.Controls.Add(this.radLabel10);
            this.radPanelDemoHolder.Controls.Add(this.radLabel9);
            this.radPanelDemoHolder.Controls.Add(this.radLabel8);
            this.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black;
            this.radPanelDemoHolder.Size = new System.Drawing.Size(661, 403);
            this.radPanelDemoHolder.ThemeName = "ControlDefault";
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radLabel2);
            this.settingsPanel.Controls.Add(this.radLabel1);
            this.settingsPanel.Controls.Add(this.radDateTimePicker3);
            this.settingsPanel.Controls.Add(this.radDateTimePicker2);
            this.settingsPanel.Controls.Add(this.radLblCulture);
            this.settingsPanel.Controls.Add(this.radComboCultures);
            this.settingsPanel.Location = new System.Drawing.Point(945, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 527);
            this.settingsPanel.Controls.SetChildIndex(this.radComboCultures, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLblCulture, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radDateTimePicker2, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radDateTimePicker3, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel2, 0);
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(210, 3);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(86, 18);
            this.radLabel8.TabIndex = 34;
            this.radLabel8.Text = "DateTimePicker:";
            // 
            // radLblCulture
            // 
            this.radLblCulture.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblCulture.Location = new System.Drawing.Point(10, 56);
            this.radLblCulture.Name = "radLblCulture";
            this.radLblCulture.Size = new System.Drawing.Size(83, 18);
            this.radLblCulture.TabIndex = 37;
            this.radLblCulture.Text = "Choose culture:";
            // 
            // radComboCultures
            // 
            this.radComboCultures.AllowShowFocusCues = false;
            this.radComboCultures.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboCultures.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.radComboCultures.DropDownSizingMode = ((Telerik.WinControls.UI.SizingMode)((Telerik.WinControls.UI.SizingMode.RightBottom | Telerik.WinControls.UI.SizingMode.UpDown)));
            this.radComboCultures.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radComboCultures.Location = new System.Drawing.Point(10, 76);
            this.radComboCultures.MaxDropDownItems = 6;
            this.radComboCultures.Name = "radComboCultures";
            // 
            // 
            // 
            this.radComboCultures.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboCultures.Size = new System.Drawing.Size(180, 20);
            this.radComboCultures.TabIndex = 36;
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(470, 5);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(63, 18);
            this.radLabel9.TabIndex = 31;
            this.radLabel9.Text = "TimePicker:";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(5, 6);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(98, 18);
            this.radLabel10.TabIndex = 31;
            this.radLabel10.Text = "MaskedEditedBox:";
            // 
            // radMaskedEditBox1
            // 
            this.radMaskedEditBox1.Location = new System.Drawing.Point(5, 26);
            this.radMaskedEditBox1.MaskType = Telerik.WinControls.UI.MaskType.FreeFormDateTime;
            this.radMaskedEditBox1.Name = "radMaskedEditBox1";
            this.radMaskedEditBox1.Size = new System.Drawing.Size(141, 20);
            this.radMaskedEditBox1.TabIndex = 38;
            this.radMaskedEditBox1.TabStop = false;
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.Location = new System.Drawing.Point(200, 26);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.Size = new System.Drawing.Size(220, 20);
            this.radDateTimePicker1.TabIndex = 39;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "Monday, May 26, 2014";
            this.radDateTimePicker1.Value = new System.DateTime(2014, 5, 26, 10, 14, 52, 620);
            // 
            // radTimePicker1
            // 
            this.radTimePicker1.Location = new System.Drawing.Point(470, 26);
            this.radTimePicker1.Name = "radTimePicker1";
            this.radTimePicker1.Size = new System.Drawing.Size(100, 20);
            this.radTimePicker1.TabIndex = 40;
            this.radTimePicker1.TabStop = false;
            this.radTimePicker1.Text = "radTimePicker1";
            this.radTimePicker1.Value = new System.DateTime(2014, 5, 26, 10, 15, 3, 0);
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDateTimePicker2.Location = new System.Drawing.Point(10, 135);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            this.radDateTimePicker2.Size = new System.Drawing.Size(180, 20);
            this.radDateTimePicker2.TabIndex = 38;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.Text = "Monday, May 26, 2014";
            this.radDateTimePicker2.Value = new System.DateTime(2014, 5, 26, 10, 17, 53, 369);
            // 
            // radDateTimePicker3
            // 
            this.radDateTimePicker3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDateTimePicker3.Location = new System.Drawing.Point(10, 197);
            this.radDateTimePicker3.Name = "radDateTimePicker3";
            this.radDateTimePicker3.Size = new System.Drawing.Size(180, 20);
            this.radDateTimePicker3.TabIndex = 39;
            this.radDateTimePicker3.TabStop = false;
            this.radDateTimePicker3.Text = "Monday, May 26, 2014";
            this.radDateTimePicker3.Value = new System.DateTime(2014, 5, 26, 10, 18, 5, 30);
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(10, 111);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(54, 18);
            this.radLabel1.TabIndex = 40;
            this.radLabel1.Text = "Min Date:";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(10, 173);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(55, 18);
            this.radLabel2.TabIndex = 41;
            this.radLabel2.Text = "Max Date:";
            // 
            // Form1
            // 
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1176, 514);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblCulture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboCultures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLblCulture;
        private Telerik.WinControls.UI.RadDropDownList radComboCultures;
        private Telerik.WinControls.UI.RadMaskedEditBox radMaskedEditBox1;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTimePicker radTimePicker1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker3;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;

    }
}