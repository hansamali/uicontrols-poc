﻿namespace Telerik.Examples.WinControls.Editors.TimePicker
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            this.radTimePicker1 = new Telerik.WinControls.UI.RadTimePicker();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radTimePicker3 = new Telerik.WinControls.UI.RadTimePicker();
            this.radTimePicker2 = new Telerik.WinControls.UI.RadTimePicker();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.TablesDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.ClockPossitionDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radLabelTimeZone = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDate = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radClock1 = new Telerik.WinControls.UI.RadClock();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablesDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClockPossitionDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTimeZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radClock1)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radPageView1);
            this.radPanelDemoHolder.Size = new System.Drawing.Size(469, 323);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox2);
            this.settingsPanel.Location = new System.Drawing.Point(493, 1);
            this.settingsPanel.Size = new System.Drawing.Size(264, 467);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox2, 0);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(0, 202);
            // 
            // radTimePicker1
            // 
            this.radTimePicker1.Location = new System.Drawing.Point(192, 116);
            this.radTimePicker1.Name = "radTimePicker1";
            this.radTimePicker1.Size = new System.Drawing.Size(106, 20);
            this.radTimePicker1.TabIndex = 0;
            this.radTimePicker1.TabStop = false;
            this.radTimePicker1.Text = "radTimePicker1";
            this.radTimePicker1.Value = new System.DateTime(((long)(0)));
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox2.Controls.Add(this.radLabel7);
            this.radGroupBox2.Controls.Add(this.radLabel5);
            this.radGroupBox2.Controls.Add(this.radTimePicker3);
            this.radGroupBox2.Controls.Add(this.radTimePicker2);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.TablesDropDownList);
            this.radGroupBox2.Controls.Add(this.ClockPossitionDropDownList);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.radCheckBox1);
            this.radGroupBox2.HeaderText = "RadTimePicker Settings";
            this.radGroupBox2.Location = new System.Drawing.Point(10, 47);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(244, 241);
            this.radGroupBox2.TabIndex = 3;
            this.radGroupBox2.Text = "RadTimePicker Settings";
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel7.Location = new System.Drawing.Point(5, 191);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(56, 18);
            this.radLabel7.TabIndex = 7;
            this.radLabel7.Text = "Max Time:";
            // 
            // radLabel5
            // 
            this.radLabel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel5.Location = new System.Drawing.Point(5, 145);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(54, 18);
            this.radLabel5.TabIndex = 6;
            this.radLabel5.Text = "Min Time:";
            // 
            // radTimePicker3
            // 
           // this.radTimePicker3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radTimePicker3.Location = new System.Drawing.Point(5, 212);
            this.radTimePicker3.Name = "radTimePicker3";
            this.radTimePicker3.Size = new System.Drawing.Size(234, 24);
            this.radTimePicker3.TabIndex = 5;
            this.radTimePicker3.TabStop = false;
            this.radTimePicker3.Text = "radTimePicker3";
            this.radTimePicker3.Value = new System.DateTime(2014, 6, 10, 16, 15, 44, 0);
            // 
            // radTimePicker2
            // 
            //this.radTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radTimePicker2.Location = new System.Drawing.Point(5, 166);
            this.radTimePicker2.Name = "radTimePicker2";
            this.radTimePicker2.Size = new System.Drawing.Size(234, 24);
            this.radTimePicker2.TabIndex = 4;
            this.radTimePicker2.TabStop = false;
            this.radTimePicker2.Text = "radTimePicker2";
            this.radTimePicker2.Value = new System.DateTime(2014, 6, 10, 16, 15, 44, 0);
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel4.Location = new System.Drawing.Point(5, 101);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(86, 18);
            this.radLabel4.TabIndex = 3;
            this.radLabel4.Text = "Time table style:";
            // 
            // TablesDropDownList
            // 
            this.TablesDropDownList.AllowShowFocusCues = false;
            this.TablesDropDownList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TablesDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "One table";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Two tables";
            radListDataItem2.TextWrap = true;
            this.TablesDropDownList.Items.Add(radListDataItem1);
            this.TablesDropDownList.Items.Add(radListDataItem2);
            this.TablesDropDownList.Location = new System.Drawing.Point(5, 121);
            this.TablesDropDownList.Name = "TablesDropDownList";
            this.TablesDropDownList.Size = new System.Drawing.Size(234, 20);
            this.TablesDropDownList.TabIndex = 2;
            // 
            // ClockPossitionDropDownList
            // 
            this.ClockPossitionDropDownList.AllowShowFocusCues = false;
            this.ClockPossitionDropDownList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ClockPossitionDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem3.Text = "Clock Before Tables";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "Clock Above Tables";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "Hide Clock";
            radListDataItem5.TextWrap = true;
            this.ClockPossitionDropDownList.Items.Add(radListDataItem3);
            this.ClockPossitionDropDownList.Items.Add(radListDataItem4);
            this.ClockPossitionDropDownList.Items.Add(radListDataItem5);
            this.ClockPossitionDropDownList.Location = new System.Drawing.Point(5, 75);
            this.ClockPossitionDropDownList.Name = "ClockPossitionDropDownList";
            this.ClockPossitionDropDownList.Size = new System.Drawing.Size(234, 20);
            this.ClockPossitionDropDownList.TabIndex = 1;
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(5, 56);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(78, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Clock position:";
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckBox1.Location = new System.Drawing.Point(5, 21);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(68, 18);
            this.radCheckBox1.TabIndex = 0;
            this.radCheckBox1.Text = "ReadOnly";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(454, 306);
            this.radPageView1.TabIndex = 1;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.radButton1);
            this.radPageViewPage1.Controls.Add(this.radTimePicker1);
            this.radPageViewPage1.Controls.Add(this.radLabelTimeZone);
            this.radPageViewPage1.Controls.Add(this.radLabel6);
            this.radPageViewPage1.Controls.Add(this.radSeparator1);
            this.radPageViewPage1.Controls.Add(this.radLabel3);
            this.radPageViewPage1.Controls.Add(this.radLabelDate);
            this.radPageViewPage1.Controls.Add(this.radLabel1);
            this.radPageViewPage1.Controls.Add(this.radClock1);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(89F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(433, 258);
            this.radPageViewPage1.Text = "Date and Time";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(221, 228);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(184, 24);
            this.radButton1.TabIndex = 9;
            this.radButton1.Text = "Change time zone...";
            // 
            // radLabelTimeZone
            // 
            this.radLabelTimeZone.Location = new System.Drawing.Point(4, 205);
            this.radLabelTimeZone.Name = "radLabelTimeZone";
            this.radLabelTimeZone.Size = new System.Drawing.Size(27, 18);
            this.radLabelTimeZone.TabIndex = 8;
            this.radLabelTimeZone.Text = "UTC";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(2, 180);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(64, 18);
            this.radLabel6.TabIndex = 7;
            this.radLabel6.Text = "Time zone  ";
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(3, 185);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(427, 10);
            this.radSeparator1.TabIndex = 7;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(189, 94);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(33, 18);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "Time:";
            // 
            // radLabelDate
            // 
            this.radLabelDate.Location = new System.Drawing.Point(189, 54);
            this.radLabelDate.Name = "radLabelDate";
            this.radLabelDate.Size = new System.Drawing.Size(104, 18);
            this.radLabelDate.TabIndex = 2;
            this.radLabelDate.Text = "12 February 2012 y.";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(189, 34);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(32, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Date:";
            // 
            // radClock1
            // 
            this.radClock1.BackColor = System.Drawing.Color.Transparent;
            this.radClock1.Location = new System.Drawing.Point(21, 16);
            this.radClock1.Name = "radClock1";
            this.radClock1.Size = new System.Drawing.Size(134, 135);
            this.radClock1.TabIndex = 0;
            this.radClock1.Text = "radClock1";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(103F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(433, 258);
            this.radPageViewPage2.Text = "Additional Clocks";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.ItemSize = new System.Drawing.SizeF(83F, 28F);
            this.radPageViewPage3.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(433, 258);
            this.radPageViewPage3.Text = "Internet Time";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1079, 520);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablesDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClockPossitionDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.radPageViewPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTimeZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radClock1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private Telerik.WinControls.UI.RadTimePicker radTimePicker1;
		private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
		private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
		private Telerik.WinControls.UI.RadDropDownList ClockPossitionDropDownList;
		private Telerik.WinControls.UI.RadDropDownList TablesDropDownList;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadLabel radLabelTimeZone;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabelDate;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadClock radClock1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTimePicker radTimePicker3;
        private Telerik.WinControls.UI.RadTimePicker radTimePicker2;
	}
}