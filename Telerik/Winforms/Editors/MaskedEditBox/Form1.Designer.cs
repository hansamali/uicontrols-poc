namespace Telerik.Examples.WinControls.Editors.MaskedEditBox
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.RadMaskedEditBox5 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadMaskedEditBox4 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadMaskedEditBox3 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLblUniversal = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox2 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLblFullDateTime = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox1 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLblFullTime = new Telerik.WinControls.UI.RadLabel();
            this.radLblShortTime = new Telerik.WinControls.UI.RadLabel();
            this.radLblSortable = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox6 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadMaskedEditBox7 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox8 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox9 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox10 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox11 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLblNumber = new Telerik.WinControls.UI.RadLabel();
            this.radLblShortDate = new Telerik.WinControls.UI.RadLabel();
            this.radLblPhoneNumber = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox12 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.RadMaskedEditBox13 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadMaskedEditBox14 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadMaskedEditBox15 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadMaskedEditBox16 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLblCulture = new Telerik.WinControls.UI.RadLabel();
            this.radComboCultures = new Telerik.WinControls.UI.RadDropDownList();
            this.radMaskedEditBox17 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radMaskedEditBox18 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radMaskedEditBox19 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblUniversal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblFullDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblFullTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblShortTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblSortable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblShortDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblPhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblCulture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboCultures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radMaskedEditBox18);
            this.radPanelDemoHolder.Controls.Add(this.radMaskedEditBox19);
            this.radPanelDemoHolder.Controls.Add(this.radMaskedEditBox17);
            this.radPanelDemoHolder.Controls.Add(this.radLblUniversal);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox5);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox14);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox4);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox11);
            this.radPanelDemoHolder.Controls.Add(this.radLblSortable);
            this.radPanelDemoHolder.Controls.Add(this.radLabel10);
            this.radPanelDemoHolder.Controls.Add(this.radLabel11);
            this.radPanelDemoHolder.Controls.Add(this.radLabel9);
            this.radPanelDemoHolder.Controls.Add(this.radLblNumber);
            this.radPanelDemoHolder.Controls.Add(this.radLblShortTime);
            this.radPanelDemoHolder.Controls.Add(this.radLblShortDate);
            this.radPanelDemoHolder.Controls.Add(this.radLblFullTime);
            this.radPanelDemoHolder.Controls.Add(this.radLblPhoneNumber);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox1);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox12);
            this.radPanelDemoHolder.Controls.Add(this.radLblFullDateTime);
            this.radPanelDemoHolder.Controls.Add(this.radLabel6);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox2);
            this.radPanelDemoHolder.Controls.Add(this.radLabel7);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox3);
            this.radPanelDemoHolder.Controls.Add(this.radLabel8);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox13);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox15);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox16);
            this.radPanelDemoHolder.Controls.Add(this.radLabel5);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox10);
            this.radPanelDemoHolder.Controls.Add(this.radLabel4);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox9);
            this.radPanelDemoHolder.Controls.Add(this.radLabel3);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox8);
            this.radPanelDemoHolder.Controls.Add(this.radLabel2);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox7);
            this.radPanelDemoHolder.Controls.Add(this.RadMaskedEditBox6);
            this.radPanelDemoHolder.Controls.Add(this.radLabel1);
            this.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black;
            this.radPanelDemoHolder.Size = new System.Drawing.Size(661, 403);
            this.radPanelDemoHolder.ThemeName = "ControlDefault";
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radTextBox1);
            this.settingsPanel.Controls.Add(this.radLblCulture);
            this.settingsPanel.Controls.Add(this.radLabel12);
            this.settingsPanel.Controls.Add(this.radComboCultures);
            this.settingsPanel.Location = new System.Drawing.Point(945, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 527);
            this.settingsPanel.Controls.SetChildIndex(this.radComboCultures, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLabel12, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radLblCulture, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radTextBox1, 0);
            // 
            // RadMaskedEditBox5
            // 
            this.RadMaskedEditBox5.Location = new System.Drawing.Point(387, 322);
            this.RadMaskedEditBox5.Mask = "g";
            this.RadMaskedEditBox5.MaskType = Telerik.WinControls.UI.MaskType.DateTime;
            this.RadMaskedEditBox5.Name = "RadMaskedEditBox5";
            // 
            // 
            // 
            this.RadMaskedEditBox5.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox5.SelectedText = "1";
            this.RadMaskedEditBox5.SelectionLength = 1;
            this.RadMaskedEditBox5.Size = new System.Drawing.Size(238, 20);
            this.RadMaskedEditBox5.TabIndex = 15;
            this.RadMaskedEditBox5.TabStop = false;
            this.RadMaskedEditBox5.Text = "11/1/2007 12:00 AM";
            // 
            // RadMaskedEditBox4
            // 
            this.RadMaskedEditBox4.Location = new System.Drawing.Point(387, 203);
            this.RadMaskedEditBox4.Mask = "T";
            this.RadMaskedEditBox4.MaskType = Telerik.WinControls.UI.MaskType.DateTime;
            this.RadMaskedEditBox4.Name = "RadMaskedEditBox4";
            // 
            // 
            // 
            this.RadMaskedEditBox4.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox4.SelectedText = "1";
            this.RadMaskedEditBox4.SelectionLength = 1;
            this.RadMaskedEditBox4.Size = new System.Drawing.Size(238, 20);
            this.RadMaskedEditBox4.TabIndex = 14;
            this.RadMaskedEditBox4.TabStop = false;
            this.RadMaskedEditBox4.Text = "12:00:00 AM";
            // 
            // RadMaskedEditBox3
            // 
            this.RadMaskedEditBox3.Location = new System.Drawing.Point(387, 140);
            this.RadMaskedEditBox3.Mask = "F";
            this.RadMaskedEditBox3.MaskType = Telerik.WinControls.UI.MaskType.DateTime;
            this.RadMaskedEditBox3.Name = "RadMaskedEditBox3";
            // 
            // 
            // 
            this.RadMaskedEditBox3.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox3.SelectedText = "Thurs";
            this.RadMaskedEditBox3.SelectionLength = 5;
            this.RadMaskedEditBox3.Size = new System.Drawing.Size(238, 20);
            this.RadMaskedEditBox3.TabIndex = 13;
            this.RadMaskedEditBox3.TabStop = false;
            this.RadMaskedEditBox3.Text = "Thursday, November 1, 2007 12:00:00 AM";
            // 
            // radLblUniversal
            // 
            this.radLblUniversal.Location = new System.Drawing.Point(387, 66);
            this.radLblUniversal.Name = "radLblUniversal";
            this.radLblUniversal.Size = new System.Drawing.Size(108, 18);
            this.radLblUniversal.TabIndex = 20;
            this.radLblUniversal.Text = "Universal Date Time:";
            // 
            // RadMaskedEditBox2
            // 
            this.RadMaskedEditBox2.Location = new System.Drawing.Point(387, 263);
            this.RadMaskedEditBox2.Mask = "t";
            this.RadMaskedEditBox2.MaskType = Telerik.WinControls.UI.MaskType.DateTime;
            this.RadMaskedEditBox2.Name = "RadMaskedEditBox2";
            // 
            // 
            // 
            this.RadMaskedEditBox2.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox2.SelectedText = "1";
            this.RadMaskedEditBox2.SelectionLength = 1;
            this.RadMaskedEditBox2.Size = new System.Drawing.Size(238, 20);
            this.RadMaskedEditBox2.TabIndex = 12;
            this.RadMaskedEditBox2.TabStop = false;
            this.RadMaskedEditBox2.Text = "12:00 AM";
            // 
            // radLblFullDateTime
            // 
            this.radLblFullDateTime.Location = new System.Drawing.Point(387, 120);
            this.radLblFullDateTime.Name = "radLblFullDateTime";
            this.radLblFullDateTime.Size = new System.Drawing.Size(80, 18);
            this.radLblFullDateTime.TabIndex = 19;
            this.radLblFullDateTime.Text = "Full Date Time:";
            // 
            // RadMaskedEditBox1
            // 
            this.RadMaskedEditBox1.Location = new System.Drawing.Point(387, 86);
            this.RadMaskedEditBox1.Mask = "u";
            this.RadMaskedEditBox1.MaskType = Telerik.WinControls.UI.MaskType.DateTime;
            this.RadMaskedEditBox1.Name = "RadMaskedEditBox1";
            // 
            // 
            // 
            this.RadMaskedEditBox1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox1.SelectedText = "2007";
            this.RadMaskedEditBox1.SelectionLength = 4;
            this.RadMaskedEditBox1.Size = new System.Drawing.Size(238, 20);
            this.RadMaskedEditBox1.TabIndex = 11;
            this.RadMaskedEditBox1.TabStop = false;
            this.RadMaskedEditBox1.Text = "2007-11-01 04:00:00Z";
            // 
            // radLblFullTime
            // 
            this.radLblFullTime.Location = new System.Drawing.Point(387, 183);
            this.radLblFullTime.Name = "radLblFullTime";
            this.radLblFullTime.Size = new System.Drawing.Size(54, 18);
            this.radLblFullTime.TabIndex = 18;
            this.radLblFullTime.Text = "Full Time:";
            // 
            // radLblShortTime
            // 
            this.radLblShortTime.Location = new System.Drawing.Point(387, 243);
            this.radLblShortTime.Name = "radLblShortTime";
            this.radLblShortTime.Size = new System.Drawing.Size(63, 18);
            this.radLblShortTime.TabIndex = 16;
            this.radLblShortTime.Text = "Short Time:";
            // 
            // radLblSortable
            // 
            this.radLblSortable.Location = new System.Drawing.Point(387, 302);
            this.radLblSortable.Name = "radLblSortable";
            this.radLblSortable.Size = new System.Drawing.Size(143, 18);
            this.radLblSortable.TabIndex = 17;
            this.radLblSortable.Text = "Sortable Date Time Pattern:";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(198, 302);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(50, 18);
            this.radLabel1.TabIndex = 10;
            this.radLabel1.Text = "Number:";
            // 
            // RadMaskedEditBox6
            // 
            this.RadMaskedEditBox6.Location = new System.Drawing.Point(198, 203);
            this.RadMaskedEditBox6.Mask = "d6";
            this.RadMaskedEditBox6.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RadMaskedEditBox6.Name = "RadMaskedEditBox6";
            this.RadMaskedEditBox6.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox6.TabIndex = 4;
            this.RadMaskedEditBox6.TabStop = false;
            this.RadMaskedEditBox6.Text = "000000";
            // 
            // RadMaskedEditBox7
            // 
            this.RadMaskedEditBox7.Location = new System.Drawing.Point(198, 322);
            this.RadMaskedEditBox7.Mask = "n3";
            this.RadMaskedEditBox7.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RadMaskedEditBox7.Name = "RadMaskedEditBox7";
            this.RadMaskedEditBox7.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox7.TabIndex = 3;
            this.RadMaskedEditBox7.TabStop = false;
            this.RadMaskedEditBox7.Text = "0.000";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(198, 243);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(66, 18);
            this.radLabel2.TabIndex = 10;
            this.radLabel2.Text = "Fixed-point:";
            // 
            // RadMaskedEditBox8
            // 
            this.RadMaskedEditBox8.Location = new System.Drawing.Point(198, 140);
            this.RadMaskedEditBox8.Mask = "p";
            this.RadMaskedEditBox8.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RadMaskedEditBox8.Name = "RadMaskedEditBox8";
            this.RadMaskedEditBox8.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox8.TabIndex = 2;
            this.RadMaskedEditBox8.TabStop = false;
            this.RadMaskedEditBox8.Text = "0.00 %";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(198, 183);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(47, 18);
            this.radLabel3.TabIndex = 10;
            this.radLabel3.Text = "6 Digits:";
            // 
            // RadMaskedEditBox9
            // 
            this.RadMaskedEditBox9.Location = new System.Drawing.Point(198, 263);
            this.RadMaskedEditBox9.Mask = "f";
            this.RadMaskedEditBox9.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RadMaskedEditBox9.Name = "RadMaskedEditBox9";
            this.RadMaskedEditBox9.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox9.TabIndex = 1;
            this.RadMaskedEditBox9.TabStop = false;
            this.RadMaskedEditBox9.Text = "0.00";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(197, 120);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(46, 18);
            this.radLabel4.TabIndex = 10;
            this.radLabel4.Text = "Percent:";
            // 
            // RadMaskedEditBox10
            // 
            this.RadMaskedEditBox10.Location = new System.Drawing.Point(198, 86);
            this.RadMaskedEditBox10.Mask = "c";
            this.RadMaskedEditBox10.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RadMaskedEditBox10.Name = "RadMaskedEditBox10";
            this.RadMaskedEditBox10.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox10.TabIndex = 0;
            this.RadMaskedEditBox10.TabStop = false;
            this.RadMaskedEditBox10.Text = "$0.00";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(197, 66);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(53, 18);
            this.radLabel5.TabIndex = 10;
            this.radLabel5.Text = "Currency:";
            // 
            // RadMaskedEditBox11
            // 
            this.RadMaskedEditBox11.Location = new System.Drawing.Point(198, 26);
            this.RadMaskedEditBox11.Mask = "00000-9999";
            this.RadMaskedEditBox11.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.RadMaskedEditBox11.Name = "RadMaskedEditBox11";
            // 
            // 
            // 
            this.RadMaskedEditBox11.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox11.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox11.TabIndex = 29;
            this.RadMaskedEditBox11.TabStop = false;
            this.RadMaskedEditBox11.Text = "_____-____";
            // 
            // radLblNumber
            // 
            this.radLblNumber.Location = new System.Drawing.Point(5, 66);
            this.radLblNumber.Name = "radLblNumber";
            this.radLblNumber.Size = new System.Drawing.Size(96, 18);
            this.radLblNumber.TabIndex = 31;
            this.radLblNumber.Text = "Number (5 digits):";
            // 
            // radLblShortDate
            // 
            this.radLblShortDate.Location = new System.Drawing.Point(5, 183);
            this.radLblShortDate.Name = "radLblShortDate";
            this.radLblShortDate.Size = new System.Drawing.Size(62, 18);
            this.radLblShortDate.TabIndex = 30;
            this.radLblShortDate.Text = "Short Date:";
            // 
            // radLblPhoneNumber
            // 
            this.radLblPhoneNumber.Location = new System.Drawing.Point(5, 120);
            this.radLblPhoneNumber.Name = "radLblPhoneNumber";
            this.radLblPhoneNumber.Size = new System.Drawing.Size(85, 18);
            this.radLblPhoneNumber.TabIndex = 35;
            this.radLblPhoneNumber.Text = "Phone Number:";
            // 
            // RadMaskedEditBox12
            // 
            this.RadMaskedEditBox12.Location = new System.Drawing.Point(5, 322);
            this.RadMaskedEditBox12.Mask = "90:00";
            this.RadMaskedEditBox12.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.RadMaskedEditBox12.Name = "RadMaskedEditBox12";
            // 
            // 
            // 
            this.RadMaskedEditBox12.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox12.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox12.TabIndex = 28;
            this.RadMaskedEditBox12.TabStop = false;
            this.RadMaskedEditBox12.Text = "__:__";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(5, 243);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(125, 18);
            this.radLabel6.TabIndex = 32;
            this.radLabel6.Text = "Social Security Number:";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(5, 302);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(33, 18);
            this.radLabel7.TabIndex = 33;
            this.radLabel7.Text = "Time:";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(198, 6);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(54, 18);
            this.radLabel8.TabIndex = 34;
            this.radLabel8.Text = "Zip Code:";
            // 
            // RadMaskedEditBox13
            // 
            this.RadMaskedEditBox13.Location = new System.Drawing.Point(5, 263);
            this.RadMaskedEditBox13.Mask = "000-00-0000";
            this.RadMaskedEditBox13.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.RadMaskedEditBox13.Name = "RadMaskedEditBox13";
            // 
            // 
            // 
            this.RadMaskedEditBox13.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox13.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox13.TabIndex = 27;
            this.RadMaskedEditBox13.TabStop = false;
            this.RadMaskedEditBox13.Text = "___-__-____";
            // 
            // RadMaskedEditBox14
            // 
            this.RadMaskedEditBox14.Location = new System.Drawing.Point(5, 203);
            this.RadMaskedEditBox14.Mask = "00/00/0000";
            this.RadMaskedEditBox14.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.RadMaskedEditBox14.Name = "RadMaskedEditBox14";
            // 
            // 
            // 
            this.RadMaskedEditBox14.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox14.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox14.TabIndex = 26;
            this.RadMaskedEditBox14.TabStop = false;
            this.RadMaskedEditBox14.Text = "__/__/____";
            // 
            // RadMaskedEditBox15
            // 
            this.RadMaskedEditBox15.Location = new System.Drawing.Point(5, 140);
            this.RadMaskedEditBox15.Mask = "(999) 000-0000";
            this.RadMaskedEditBox15.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.RadMaskedEditBox15.Name = "RadMaskedEditBox15";
            // 
            // 
            // 
            this.RadMaskedEditBox15.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox15.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox15.TabIndex = 25;
            this.RadMaskedEditBox15.TabStop = false;
            this.RadMaskedEditBox15.Text = "(___) ___-____";
            // 
            // RadMaskedEditBox16
            // 
            this.RadMaskedEditBox16.Location = new System.Drawing.Point(5, 86);
            this.RadMaskedEditBox16.Mask = "00000";
            this.RadMaskedEditBox16.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.RadMaskedEditBox16.Name = "RadMaskedEditBox16";
            // 
            // 
            // 
            this.RadMaskedEditBox16.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RadMaskedEditBox16.Size = new System.Drawing.Size(141, 20);
            this.RadMaskedEditBox16.TabIndex = 24;
            this.RadMaskedEditBox16.TabStop = false;
            this.RadMaskedEditBox16.Text = "_____";
            // 
            // radLblCulture
            // 
            this.radLblCulture.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblCulture.Location = new System.Drawing.Point(10, 56);
            this.radLblCulture.Name = "radLblCulture";
            this.radLblCulture.Size = new System.Drawing.Size(83, 18);
            this.radLblCulture.TabIndex = 37;
            this.radLblCulture.Text = "Choose culture:";
            // 
            // radComboCultures
            // 
            this.radComboCultures.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboCultures.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.radComboCultures.DropDownSizingMode = ((Telerik.WinControls.UI.SizingMode)((Telerik.WinControls.UI.SizingMode.RightBottom | Telerik.WinControls.UI.SizingMode.UpDown)));
            this.radComboCultures.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radComboCultures.Location = new System.Drawing.Point(10, 76);
            this.radComboCultures.MaxDropDownItems = 6;
            this.radComboCultures.Name = "radComboCultures";
            // 
            // 
            // 
            this.radComboCultures.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboCultures.Size = new System.Drawing.Size(180, 20);
            this.radComboCultures.TabIndex = 36;
            // 
            // radMaskedEditBox17
            // 
            this.radMaskedEditBox17.Location = new System.Drawing.Point(387, 26);
            this.radMaskedEditBox17.MaskType = Telerik.WinControls.UI.MaskType.EMail;
            this.radMaskedEditBox17.Name = "radMaskedEditBox17";
            this.radMaskedEditBox17.Size = new System.Drawing.Size(238, 20);
            this.radMaskedEditBox17.TabIndex = 38;
            this.radMaskedEditBox17.TabStop = false;
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(387, 6);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(36, 18);
            this.radLabel9.TabIndex = 31;
            this.radLabel9.Text = "EMail:";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(5, 6);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(18, 18);
            this.radLabel10.TabIndex = 31;
            this.radLabel10.Text = "IP:";
            // 
            // radMaskedEditBox18
            // 
            this.radMaskedEditBox18.Location = new System.Drawing.Point(5, 26);
            this.radMaskedEditBox18.Mask = "<>";
            this.radMaskedEditBox18.MaskType = Telerik.WinControls.UI.MaskType.IP;
            this.radMaskedEditBox18.Name = "radMaskedEditBox18";
            this.radMaskedEditBox18.Size = new System.Drawing.Size(141, 20);
            this.radMaskedEditBox18.TabIndex = 38;
            this.radMaskedEditBox18.TabStop = false;
            this.radMaskedEditBox18.Text = "   .   .   .   ";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(5, 362);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(39, 18);
            this.radLabel11.TabIndex = 31;
            this.radLabel11.Text = "Regex:";
            // 
            // radMaskedEditBox19
            // 
            this.radMaskedEditBox19.Location = new System.Drawing.Point(5, 382);
            this.radMaskedEditBox19.Mask = "[A-z]";
            this.radMaskedEditBox19.MaskType = Telerik.WinControls.UI.MaskType.Regex;
            this.radMaskedEditBox19.Name = "radMaskedEditBox19";
            this.radMaskedEditBox19.Size = new System.Drawing.Size(238, 20);
            this.radMaskedEditBox19.TabIndex = 38;
            this.radMaskedEditBox19.TabStop = false;
            // 
            // radLabel12
            // 
            this.radLabel12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel12.Location = new System.Drawing.Point(10, 121);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(38, 18);
            this.radLabel12.TabIndex = 37;
            this.radLabel12.Text = "Regex:";
            // 
            // radTextBox1
            // 
            this.radTextBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radTextBox1.Location = new System.Drawing.Point(10, 141);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(180, 20);
            this.radTextBox1.TabIndex = 38;
            this.radTextBox1.TabStop = false;
            this.radTextBox1.Text = "[A-z]";
            // 
            // Form1
            // 
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1273, 942);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblUniversal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblFullDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblFullTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblShortTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblSortable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblShortDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblPhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadMaskedEditBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLblCulture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboCultures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox5;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox4;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox3;
        private Telerik.WinControls.UI.RadLabel radLblUniversal;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox2;
        private Telerik.WinControls.UI.RadLabel radLblFullDateTime;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox1;
        private Telerik.WinControls.UI.RadLabel radLblFullTime;
        private Telerik.WinControls.UI.RadLabel radLblShortTime;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLblSortable;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox10;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox9;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox8;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox7;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox6;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox11;
        private Telerik.WinControls.UI.RadLabel radLblNumber;
        private Telerik.WinControls.UI.RadLabel radLblShortDate;
        private Telerik.WinControls.UI.RadLabel radLblPhoneNumber;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox12;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox13;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox14;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox15;
        private Telerik.WinControls.UI.RadMaskedEditBox RadMaskedEditBox16;
        private Telerik.WinControls.UI.RadLabel radLblCulture;
        private Telerik.WinControls.UI.RadDropDownList radComboCultures;
        private Telerik.WinControls.UI.RadMaskedEditBox radMaskedEditBox18;
        private Telerik.WinControls.UI.RadMaskedEditBox radMaskedEditBox19;
        private Telerik.WinControls.UI.RadMaskedEditBox radMaskedEditBox17;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;

    }
}