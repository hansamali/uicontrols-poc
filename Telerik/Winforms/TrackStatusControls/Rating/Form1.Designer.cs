﻿namespace Telerik.Examples.WinControls.TrackStatusControls.Rating
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radRating1 = new Telerik.WinControls.UI.RadRating();
            this.ratingStarVisualElement1 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement2 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement3 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement4 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement5 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.radRating2 = new Telerik.WinControls.UI.RadRating();
            this.ratingDiamondVisualElement1 = new Telerik.WinControls.UI.RatingDiamondVisualElement();
            this.ratingDiamondVisualElement2 = new Telerik.WinControls.UI.RatingDiamondVisualElement();
            this.ratingDiamondVisualElement3 = new Telerik.WinControls.UI.RatingDiamondVisualElement();
            this.ratingDiamondVisualElement4 = new Telerik.WinControls.UI.RatingDiamondVisualElement();
            this.ratingDiamondVisualElement5 = new Telerik.WinControls.UI.RatingDiamondVisualElement();
            this.radRating3 = new Telerik.WinControls.UI.RadRating();
            this.ratingHeartVisualElement1 = new Telerik.WinControls.UI.RatingHeartVisualElement();
            this.ratingHeartVisualElement2 = new Telerik.WinControls.UI.RatingHeartVisualElement();
            this.ratingHeartVisualElement3 = new Telerik.WinControls.UI.RatingHeartVisualElement();
            this.ratingHeartVisualElement4 = new Telerik.WinControls.UI.RatingHeartVisualElement();
            this.ratingHeartVisualElement5 = new Telerik.WinControls.UI.RatingHeartVisualElement();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton3 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton4 = new Telerik.WinControls.UI.RadRadioButton();
            this.radSpinEditor2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radCheckBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton1 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton2 = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radRating4 = new Telerik.WinControls.UI.RadRating();
            this.ratingStarVisualElement6 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement7 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement8 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingHeartVisualElement6 = new Telerik.WinControls.UI.RatingHeartVisualElement();
            this.ratingHeartVisualElement7 = new Telerik.WinControls.UI.RatingHeartVisualElement();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRating1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRating2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRating3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRating4)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(851, 3);
            this.settingsPanel.Size = new System.Drawing.Size(355, 354);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(851, 387);
            // 
            // radRating1
            // 
            this.radRating1.Caption = "The Godfather 1972";
            this.radRating1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ratingStarVisualElement1,
            this.ratingStarVisualElement2,
            this.ratingStarVisualElement3,
            this.ratingStarVisualElement4,
            this.ratingStarVisualElement5});
            this.radRating1.Location = new System.Drawing.Point(50, 50);
            this.radRating1.Name = "radRating1";
            this.radRating1.Size = new System.Drawing.Size(140, 51);
            this.radRating1.TabIndex = 2;
            this.radRating1.Text = "radRating1";
            // 
            // ratingStarVisualElement1
            // 
            this.ratingStarVisualElement1.AccessibleDescription = "ratingStarVisualElement1";
            this.ratingStarVisualElement1.AccessibleName = "ratingStarVisualElement1";
            this.ratingStarVisualElement1.Name = "ratingStarVisualElement1";
            this.ratingStarVisualElement1.Text = "ratingStarVisualElement1";
            this.ratingStarVisualElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingStarVisualElement2
            // 
            this.ratingStarVisualElement2.AccessibleDescription = "ratingStarVisualElement2";
            this.ratingStarVisualElement2.AccessibleName = "ratingStarVisualElement2";
            this.ratingStarVisualElement2.Name = "ratingStarVisualElement2";
            this.ratingStarVisualElement2.Text = "ratingStarVisualElement2";
            this.ratingStarVisualElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingStarVisualElement3
            // 
            this.ratingStarVisualElement3.AccessibleDescription = "ratingStarVisualElement3";
            this.ratingStarVisualElement3.AccessibleName = "ratingStarVisualElement3";
            this.ratingStarVisualElement3.Name = "ratingStarVisualElement3";
            this.ratingStarVisualElement3.Text = "ratingStarVisualElement3";
            this.ratingStarVisualElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingStarVisualElement4
            // 
            this.ratingStarVisualElement4.AccessibleDescription = "ratingStarVisualElement4";
            this.ratingStarVisualElement4.AccessibleName = "ratingStarVisualElement4";
            this.ratingStarVisualElement4.Name = "ratingStarVisualElement4";
            this.ratingStarVisualElement4.Text = "ratingStarVisualElement4";
            this.ratingStarVisualElement4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingStarVisualElement5
            // 
            this.ratingStarVisualElement5.AccessibleDescription = "ratingStarVisualElement5";
            this.ratingStarVisualElement5.AccessibleName = "ratingStarVisualElement5";
            this.ratingStarVisualElement5.Name = "ratingStarVisualElement5";
            this.ratingStarVisualElement5.Text = "ratingStarVisualElement5";
            this.ratingStarVisualElement5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRating2
            // 
            this.radRating2.Caption = "Forrest Gump 1994";
            this.radRating2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ratingDiamondVisualElement1,
            this.ratingDiamondVisualElement2,
            this.ratingDiamondVisualElement3,
            this.ratingDiamondVisualElement4,
            this.ratingDiamondVisualElement5});
            this.radRating2.Location = new System.Drawing.Point(50, 120);
            this.radRating2.Name = "radRating2";
            this.radRating2.Size = new System.Drawing.Size(140, 51);
            this.radRating2.TabIndex = 3;
            this.radRating2.Text = "radRating2";
            // 
            // ratingDiamondVisualElement1
            // 
            this.ratingDiamondVisualElement1.AccessibleDescription = "ratingDiamondVisualElement1";
            this.ratingDiamondVisualElement1.AccessibleName = "ratingDiamondVisualElement1";
            this.ratingDiamondVisualElement1.Name = "ratingDiamondVisualElement1";
            this.ratingDiamondVisualElement1.Text = "ratingDiamondVisualElement1";
            this.ratingDiamondVisualElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingDiamondVisualElement2
            // 
            this.ratingDiamondVisualElement2.AccessibleDescription = "ratingDiamondVisualElement2";
            this.ratingDiamondVisualElement2.AccessibleName = "ratingDiamondVisualElement2";
            this.ratingDiamondVisualElement2.Name = "ratingDiamondVisualElement2";
            this.ratingDiamondVisualElement2.Text = "ratingDiamondVisualElement2";
            this.ratingDiamondVisualElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingDiamondVisualElement3
            // 
            this.ratingDiamondVisualElement3.AccessibleDescription = "ratingDiamondVisualElement3";
            this.ratingDiamondVisualElement3.AccessibleName = "ratingDiamondVisualElement3";
            this.ratingDiamondVisualElement3.Name = "ratingDiamondVisualElement3";
            this.ratingDiamondVisualElement3.Text = "ratingDiamondVisualElement3";
            this.ratingDiamondVisualElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingDiamondVisualElement4
            // 
            this.ratingDiamondVisualElement4.AccessibleDescription = "ratingDiamondVisualElement4";
            this.ratingDiamondVisualElement4.AccessibleName = "ratingDiamondVisualElement4";
            this.ratingDiamondVisualElement4.Name = "ratingDiamondVisualElement4";
            this.ratingDiamondVisualElement4.Text = "ratingDiamondVisualElement4";
            this.ratingDiamondVisualElement4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingDiamondVisualElement5
            // 
            this.ratingDiamondVisualElement5.AccessibleDescription = "ratingDiamondVisualElement5";
            this.ratingDiamondVisualElement5.AccessibleName = "ratingDiamondVisualElement5";
            this.ratingDiamondVisualElement5.Name = "ratingDiamondVisualElement5";
            this.ratingDiamondVisualElement5.Text = "ratingDiamondVisualElement5";
            this.ratingDiamondVisualElement5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRating3
            // 
            this.radRating3.Caption = "The Sixth Sense 1999";
            this.radRating3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ratingHeartVisualElement1,
            this.ratingHeartVisualElement2,
            this.ratingHeartVisualElement3,
            this.ratingHeartVisualElement4,
            this.ratingHeartVisualElement5});
            this.radRating3.Location = new System.Drawing.Point(250, 50);
            this.radRating3.Name = "radRating3";
            this.radRating3.Size = new System.Drawing.Size(140, 51);
            this.radRating3.TabIndex = 4;
            this.radRating3.Text = "radRating3";
            // 
            // ratingHeartVisualElement1
            // 
            this.ratingHeartVisualElement1.AccessibleDescription = "ratingHeartVisualElement1";
            this.ratingHeartVisualElement1.AccessibleName = "ratingHeartVisualElement1";
            this.ratingHeartVisualElement1.Name = "ratingHeartVisualElement1";
            this.ratingHeartVisualElement1.Text = "ratingHeartVisualElement1";
            this.ratingHeartVisualElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingHeartVisualElement2
            // 
            this.ratingHeartVisualElement2.AccessibleDescription = "ratingHeartVisualElement2";
            this.ratingHeartVisualElement2.AccessibleName = "ratingHeartVisualElement2";
            this.ratingHeartVisualElement2.Name = "ratingHeartVisualElement2";
            this.ratingHeartVisualElement2.Text = "ratingHeartVisualElement2";
            this.ratingHeartVisualElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingHeartVisualElement3
            // 
            this.ratingHeartVisualElement3.AccessibleDescription = "ratingHeartVisualElement3";
            this.ratingHeartVisualElement3.AccessibleName = "ratingHeartVisualElement3";
            this.ratingHeartVisualElement3.Name = "ratingHeartVisualElement3";
            this.ratingHeartVisualElement3.Text = "ratingHeartVisualElement3";
            this.ratingHeartVisualElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingHeartVisualElement4
            // 
            this.ratingHeartVisualElement4.AccessibleDescription = "ratingHeartVisualElement4";
            this.ratingHeartVisualElement4.AccessibleName = "ratingHeartVisualElement4";
            this.ratingHeartVisualElement4.Name = "ratingHeartVisualElement4";
            this.ratingHeartVisualElement4.Text = "ratingHeartVisualElement4";
            this.ratingHeartVisualElement4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingHeartVisualElement5
            // 
            this.ratingHeartVisualElement5.AccessibleDescription = "ratingHeartVisualElement5";
            this.ratingHeartVisualElement5.AccessibleName = "ratingHeartVisualElement5";
            this.ratingHeartVisualElement5.Name = "ratingHeartVisualElement5";
            this.ratingHeartVisualElement5.Text = "ratingHeartVisualElement5";
            this.ratingHeartVisualElement5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radGroupBox3);
            this.radGroupBox1.Controls.Add(this.radSpinEditor2);
            this.radGroupBox1.Controls.Add(this.radSpinEditor1);
            this.radGroupBox1.Controls.Add(this.radCheckBox2);
            this.radGroupBox1.Controls.Add(this.radCheckBox1);
            this.radGroupBox1.Controls.Add(this.radGroupBox2);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.radDropDownList1);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.HeaderText = "Settings";
            this.radGroupBox1.Location = new System.Drawing.Point(4, 47);
            this.radGroupBox1.Name = "radGroupBox1";
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.radGroupBox1.Size = new System.Drawing.Size(187, 304);
            this.radGroupBox1.TabIndex = 2;
            this.radGroupBox1.Text = "Settings";
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.radRadioButton3);
            this.radGroupBox3.Controls.Add(this.radRadioButton4);
            this.radGroupBox3.HeaderText = "Orientation";
            this.radGroupBox3.Location = new System.Drawing.Point(5, 191);
            this.radGroupBox3.Name = "radGroupBox3";
            // 
            // 
            // 
            this.radGroupBox3.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.radGroupBox3.Size = new System.Drawing.Size(170, 53);
            this.radGroupBox3.TabIndex = 21;
            this.radGroupBox3.Text = "Orientation";
            // 
            // radRadioButton3
            // 
            this.radRadioButton3.Location = new System.Drawing.Point(5, 21);
            this.radRadioButton3.Name = "radRadioButton3";
            this.radRadioButton3.Size = new System.Drawing.Size(72, 18);
            this.radRadioButton3.TabIndex = 0;
            this.radRadioButton3.TabStop = true;
            this.radRadioButton3.Text = "Horizontal";
            this.radRadioButton3.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radRadioButton4
            // 
            this.radRadioButton4.Location = new System.Drawing.Point(89, 21);
            this.radRadioButton4.Name = "radRadioButton4";
            this.radRadioButton4.Size = new System.Drawing.Size(57, 18);
            this.radRadioButton4.TabIndex = 1;
            this.radRadioButton4.Text = "Vertical";
            // 
            // radSpinEditor2
            // 
            this.radSpinEditor2.Location = new System.Drawing.Point(88, 95);
            this.radSpinEditor2.Name = "radSpinEditor2";
            this.radSpinEditor2.Size = new System.Drawing.Size(87, 20);
            this.radSpinEditor2.TabIndex = 20;
            this.radSpinEditor2.TabStop = false;
            this.radSpinEditor2.Tag = "Right";
            // 
            // radSpinEditor1
            // 
            this.radSpinEditor1.Location = new System.Drawing.Point(88, 70);
            this.radSpinEditor1.Name = "radSpinEditor1";
            this.radSpinEditor1.Size = new System.Drawing.Size(87, 20);
            this.radSpinEditor1.TabIndex = 19;
            this.radSpinEditor1.TabStop = false;
            this.radSpinEditor1.Tag = "Right";
            // 
            // radCheckBox2
            // 
            this.radCheckBox2.Location = new System.Drawing.Point(10, 277);
            this.radCheckBox2.Name = "radCheckBox2";
            this.radCheckBox2.Size = new System.Drawing.Size(68, 18);
            this.radCheckBox2.TabIndex = 16;
            this.radCheckBox2.Text = "ReadOnly";
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.Location = new System.Drawing.Point(10, 253);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(78, 18);
            this.radCheckBox1.TabIndex = 15;
            this.radCheckBox1.Text = "RightToLeft";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radRadioButton1);
            this.radGroupBox2.Controls.Add(this.radRadioButton2);
            this.radGroupBox2.HeaderText = "Direction";
            this.radGroupBox2.Location = new System.Drawing.Point(5, 132);
            this.radGroupBox2.Name = "radGroupBox2";
            // 
            // 
            // 
            this.radGroupBox2.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.radGroupBox2.Size = new System.Drawing.Size(170, 53);
            this.radGroupBox2.TabIndex = 14;
            this.radGroupBox2.Text = "Direction";
            // 
            // radRadioButton1
            // 
            this.radRadioButton1.Location = new System.Drawing.Point(5, 21);
            this.radRadioButton1.Name = "radRadioButton1";
            this.radRadioButton1.Size = new System.Drawing.Size(65, 18);
            this.radRadioButton1.TabIndex = 0;
            this.radRadioButton1.TabStop = true;
            this.radRadioButton1.Text = "Standard";
            this.radRadioButton1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radRadioButton2
            // 
            this.radRadioButton2.Location = new System.Drawing.Point(89, 21);
            this.radRadioButton2.Name = "radRadioButton2";
            this.radRadioButton2.Size = new System.Drawing.Size(66, 18);
            this.radRadioButton2.TabIndex = 1;
            this.radRadioButton2.Text = "Reversed";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(4, 97);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(55, 18);
            this.radLabel8.TabIndex = 8;
            this.radLabel8.Text = "MaxValue";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(5, 72);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(53, 18);
            this.radLabel7.TabIndex = 6;
            this.radLabel7.Text = "MinValue";
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.Location = new System.Drawing.Point(88, 44);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(87, 20);
            this.radDropDownList1.TabIndex = 3;
            this.radDropDownList1.Tag = "Right";
            this.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(5, 44);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(81, 18);
            this.radLabel5.TabIndex = 2;
            this.radLabel5.Text = "SelectionMode";
            // 
            // radRating4
            // 
            this.radRating4.Caption = "Office Space 1999";
            this.radRating4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ratingStarVisualElement6,
            this.ratingStarVisualElement7,
            this.ratingStarVisualElement8,
            this.ratingHeartVisualElement6,
            this.ratingHeartVisualElement7});
            this.radRating4.Location = new System.Drawing.Point(250, 120);
            this.radRating4.Name = "radRating4";
            this.radRating4.Size = new System.Drawing.Size(140, 51);
            this.radRating4.TabIndex = 5;
            this.radRating4.Text = "radRating4";
            // 
            // ratingStarVisualElement6
            // 
            this.ratingStarVisualElement6.AccessibleDescription = "ratingStarVisualElement6";
            this.ratingStarVisualElement6.AccessibleName = "ratingStarVisualElement6";
            this.ratingStarVisualElement6.Name = "ratingStarVisualElement6";
            this.ratingStarVisualElement6.Text = "ratingStarVisualElement6";
            this.ratingStarVisualElement6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingStarVisualElement7
            // 
            this.ratingStarVisualElement7.AccessibleDescription = "ratingStarVisualElement7";
            this.ratingStarVisualElement7.AccessibleName = "ratingStarVisualElement7";
            this.ratingStarVisualElement7.Name = "ratingStarVisualElement7";
            this.ratingStarVisualElement7.Text = "ratingStarVisualElement7";
            this.ratingStarVisualElement7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingStarVisualElement8
            // 
            this.ratingStarVisualElement8.AccessibleDescription = "ratingStarVisualElement8";
            this.ratingStarVisualElement8.AccessibleName = "ratingStarVisualElement8";
            this.ratingStarVisualElement8.Name = "ratingStarVisualElement8";
            this.ratingStarVisualElement8.Text = "ratingStarVisualElement8";
            this.ratingStarVisualElement8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingHeartVisualElement6
            // 
            this.ratingHeartVisualElement6.AccessibleDescription = "ratingHeartVisualElement6";
            this.ratingHeartVisualElement6.AccessibleName = "ratingHeartVisualElement6";
            this.ratingHeartVisualElement6.Name = "ratingHeartVisualElement6";
            this.ratingHeartVisualElement6.Text = "ratingHeartVisualElement6";
            this.ratingHeartVisualElement6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ratingHeartVisualElement7
            // 
            this.ratingHeartVisualElement7.AccessibleDescription = "ratingHeartVisualElement7";
            this.ratingHeartVisualElement7.AccessibleName = "ratingHeartVisualElement7";
            this.ratingHeartVisualElement7.Name = "ratingHeartVisualElement7";
            this.ratingHeartVisualElement7.Text = "ratingHeartVisualElement7";
            this.ratingHeartVisualElement7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radRating4);
            this.Controls.Add(this.radRating3);
            this.Controls.Add(this.radRating2);
            this.Controls.Add(this.radRating1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1136, 633);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radRating1, 0);
            this.Controls.SetChildIndex(this.radRating2, 0);
            this.Controls.SetChildIndex(this.radRating3, 0);
            this.Controls.SetChildIndex(this.radRating4, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRating1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRating2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRating3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRating4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadRating radRating1;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement1;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement2;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement3;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement4;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement5;
        private Telerik.WinControls.UI.RadRating radRating2;
        private Telerik.WinControls.UI.RatingDiamondVisualElement ratingDiamondVisualElement1;
        private Telerik.WinControls.UI.RatingDiamondVisualElement ratingDiamondVisualElement2;
        private Telerik.WinControls.UI.RatingDiamondVisualElement ratingDiamondVisualElement3;
        private Telerik.WinControls.UI.RatingDiamondVisualElement ratingDiamondVisualElement4;
        private Telerik.WinControls.UI.RatingDiamondVisualElement ratingDiamondVisualElement5;
        private Telerik.WinControls.UI.RadRating radRating3;
        private Telerik.WinControls.UI.RatingHeartVisualElement ratingHeartVisualElement1;
        private Telerik.WinControls.UI.RatingHeartVisualElement ratingHeartVisualElement2;
        private Telerik.WinControls.UI.RatingHeartVisualElement ratingHeartVisualElement3;
        private Telerik.WinControls.UI.RatingHeartVisualElement ratingHeartVisualElement4;
        private Telerik.WinControls.UI.RatingHeartVisualElement ratingHeartVisualElement5;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor2;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox2;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton2;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadRating radRating4;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement6;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement7;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement8;
        private Telerik.WinControls.UI.RatingHeartVisualElement ratingHeartVisualElement6;
        private Telerik.WinControls.UI.RatingHeartVisualElement ratingHeartVisualElement7;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton4;


    }
}