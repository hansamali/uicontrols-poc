namespace Telerik.Examples.WinControls.TrackStatusControls.ProgressBar
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.radTrackBar1 = new Telerik.WinControls.UI.RadTrackBar();
            this.radProgressBar5 = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBar2 = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBar4 = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBar1 = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBar6 = new Telerik.WinControls.UI.RadProgressBar();
            this.radGroupHorizontal = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupImage = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupHorizontal)).BeginInit();
            this.radGroupHorizontal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupImage)).BeginInit();
            this.radGroupImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(821, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 705);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(574, 5);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 50;
            // 
            // radTrackBar1
            // 
            this.radTrackBar1.Location = new System.Drawing.Point(15, 138);
            this.radTrackBar1.Maximum = 10F;
            this.radTrackBar1.Name = "radTrackBar1";
            this.radTrackBar1.Size = new System.Drawing.Size(320, 34);
            this.radTrackBar1.TabIndex = 18;
            this.radTrackBar1.Text = "radTrackBar1";
            this.radTrackBar1.Value = 10F;
            // 
            // radProgressBar5
            // 
            this.radProgressBar5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radProgressBar5.Image = ((System.Drawing.Image)(resources.GetObject("radProgressBar5.Image")));
            this.radProgressBar5.Location = new System.Drawing.Point(15, 23);
            this.radProgressBar5.Name = "radProgressBar5";
            this.radProgressBar5.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar5.SeparatorWidth = 4;
            this.radProgressBar5.Size = new System.Drawing.Size(320, 85);
            this.radProgressBar5.StepWidth = 13;
            this.radProgressBar5.TabIndex = 14;
            this.radProgressBar5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radProgressBar5.Value1 = 100;
            // 
            // radProgressBar2
            // 
            this.radProgressBar2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radProgressBar2.Location = new System.Drawing.Point(15, 64);
            this.radProgressBar2.Name = "radProgressBar2";
            this.radProgressBar2.ProgressOrientation = Telerik.WinControls.ProgressOrientation.Right;
            this.radProgressBar2.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar2.SeparatorWidth = 0;
            this.radProgressBar2.ShowProgressIndicators = true;
            this.radProgressBar2.Size = new System.Drawing.Size(320, 28);
            this.radProgressBar2.StepWidth = 13;
            this.radProgressBar2.TabIndex = 13;
            this.radProgressBar2.Text = "60 %";
            this.radProgressBar2.Value1 = 60;
            // 
            // radProgressBar4
            // 
            this.radProgressBar4.Location = new System.Drawing.Point(59, 23);
            this.radProgressBar4.Name = "radProgressBar4";
            this.radProgressBar4.ProgressOrientation = Telerik.WinControls.ProgressOrientation.Top;
            this.radProgressBar4.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar4.SeparatorWidth = 4;
            this.radProgressBar4.ShowProgressIndicators = true;
            this.radProgressBar4.Size = new System.Drawing.Size(31, 269);
            this.radProgressBar4.StepWidth = 13;
            this.radProgressBar4.TabIndex = 12;
            this.radProgressBar4.Text = "60 %";
            this.radProgressBar4.Value1 = 60;
            // 
            // radProgressBar1
            // 
            this.radProgressBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radProgressBar1.ImageScalingSize = new System.Drawing.Size(0, 0);
            this.radProgressBar1.Location = new System.Drawing.Point(15, 23);
            this.radProgressBar1.Name = "radProgressBar1";
            this.radProgressBar1.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar1.SeparatorWidth = 0;
            this.radProgressBar1.ShowProgressIndicators = true;
            this.radProgressBar1.Size = new System.Drawing.Size(320, 28);
            this.radProgressBar1.StepWidth = 13;
            this.radProgressBar1.SweepAngle = 120;
            this.radProgressBar1.TabIndex = 6;
            this.radProgressBar1.Text = "60 %";
            this.radProgressBar1.Value1 = 60;
            // 
            // radProgressBar6
            // 
            this.radProgressBar6.Location = new System.Drawing.Point(15, 23);
            this.radProgressBar6.Name = "radProgressBar6";
            this.radProgressBar6.ProgressOrientation = Telerik.WinControls.ProgressOrientation.Bottom;
            this.radProgressBar6.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar6.SeparatorWidth = 0;
            this.radProgressBar6.ShowProgressIndicators = true;
            this.radProgressBar6.Size = new System.Drawing.Size(31, 269);
            this.radProgressBar6.StepWidth = 13;
            this.radProgressBar6.TabIndex = 25;
            this.radProgressBar6.Text = "60 %";
            this.radProgressBar6.Value1 = 60;
            // 
            // radGroupHorizontal
            // 
            this.radGroupHorizontal.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupHorizontal.Controls.Add(this.radProgressBar1);
            this.radGroupHorizontal.Controls.Add(this.radProgressBar2);
            this.radGroupHorizontal.FooterText = "";
            this.radGroupHorizontal.HeaderText = "Horizontal";
            this.radGroupHorizontal.Location = new System.Drawing.Point(0, 0);
            this.radGroupHorizontal.Name = "radGroupHorizontal";
            this.radGroupHorizontal.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupHorizontal.Size = new System.Drawing.Size(350, 106);
            this.radGroupHorizontal.TabIndex = 30;
            this.radGroupHorizontal.Text = "Horizontal";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radProgressBar6);
            this.radGroupBox2.Controls.Add(this.radProgressBar4);
            this.radGroupBox2.FooterText = "";
            this.radGroupBox2.HeaderText = "Vertical";
            this.radGroupBox2.Location = new System.Drawing.Point(363, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox2.Size = new System.Drawing.Size(105, 305);
            this.radGroupBox2.TabIndex = 31;
            this.radGroupBox2.Text = "Vertical";
            // 
            // radGroupImage
            // 
            this.radGroupImage.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupImage.Controls.Add(this.radLabel1);
            this.radGroupImage.Controls.Add(this.radTrackBar1);
            this.radGroupImage.Controls.Add(this.radProgressBar5);
            this.radGroupImage.FooterText = "";
            this.radGroupImage.HeaderText = "Image progress bar";
            this.radGroupImage.Location = new System.Drawing.Point(0, 118);
            this.radGroupImage.Name = "radGroupImage";
            this.radGroupImage.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupImage.Size = new System.Drawing.Size(350, 187);
            this.radGroupImage.TabIndex = 32;
            this.radGroupImage.Text = "Image progress bar";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(15, 114);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(52, 18);
            this.radLabel1.TabIndex = 19;
            this.radLabel1.Text = "Slide me!";
            // 
            // Form1
            // 
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupImage);
            this.Controls.Add(this.radGroupHorizontal);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2, 35, 2, 4);
            this.Size = new System.Drawing.Size(1438, 917);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radGroupHorizontal, 0);
            this.Controls.SetChildIndex(this.radGroupImage, 0);
            this.Controls.SetChildIndex(this.radGroupBox2, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupHorizontal)).EndInit();
            this.radGroupHorizontal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupImage)).EndInit();
            this.radGroupImage.ResumeLayout(false);
            this.radGroupImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Timer timer1;
		private Telerik.WinControls.UI.RadProgressBar radProgressBar1;
        private Telerik.WinControls.UI.RadProgressBar radProgressBar4;
        private Telerik.WinControls.UI.RadProgressBar radProgressBar2;
        private Telerik.WinControls.UI.RadProgressBar radProgressBar5;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar1;
        private Telerik.WinControls.UI.RadProgressBar radProgressBar6;
        private Telerik.WinControls.UI.RadGroupBox radGroupHorizontal;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupImage;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    

    }
}
