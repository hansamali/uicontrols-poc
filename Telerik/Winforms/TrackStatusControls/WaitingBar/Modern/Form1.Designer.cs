﻿namespace Telerik.Examples.WinControls.TrackStatusControls.WaitingBar.Modern
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radRadioButtonDotsLine = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButtonDotsSpinner = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButtonDotsRing = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButtonFadingRing = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButtonLineRing = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButtonSegmentedRing = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButtonRotatingRings = new Telerik.WinControls.UI.RadRadioButton();
            this.radButtonLoadImages = new Telerik.WinControls.UI.RadButton();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.radWaitingBar2 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsSpinnerWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.radWaitingBar3 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsRingWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement();
            this.radWaitingBar4 = new Telerik.WinControls.UI.RadWaitingBar();
            this.fadingRingWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.FadingRingWaitingBarIndicatorElement();
            this.radWaitingBar5 = new Telerik.WinControls.UI.RadWaitingBar();
            this.lineRingWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.LineRingWaitingBarIndicatorElement();
            this.radWaitingBar6 = new Telerik.WinControls.UI.RadWaitingBar();
            this.segmentedRingWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.SegmentedRingWaitingBarIndicatorElement();
            this.radWaitingBar7 = new Telerik.WinControls.UI.RadWaitingBar();
            this.rotatingRingsWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.RotatingRingsWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonDotsLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonDotsSpinner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonDotsRing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonFadingRing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonLineRing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonSegmentedRing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonRotatingRings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonLoadImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar7)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1041, 49);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(1041, 215);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radLabel1.Location = new System.Drawing.Point(4, 4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(173, 19);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "CHOOSE  WAITING BAR STYLE";
            // 
            // radRadioButtonDotsLine
            // 
            this.radRadioButtonDotsLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButtonDotsLine.Location = new System.Drawing.Point(43, 44);
            this.radRadioButtonDotsLine.Name = "radRadioButtonDotsLine";
            this.radRadioButtonDotsLine.Size = new System.Drawing.Size(15, 15);
            this.radRadioButtonDotsLine.TabIndex = 3;
            this.radRadioButtonDotsLine.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButtonDotsLine.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioButton_ToggleStateChanged);
            // 
            // radRadioButtonDotsSpinner
            // 
            this.radRadioButtonDotsSpinner.Location = new System.Drawing.Point(133, 44);
            this.radRadioButtonDotsSpinner.Name = "radRadioButtonDotsSpinner";
            this.radRadioButtonDotsSpinner.Size = new System.Drawing.Size(15, 15);
            this.radRadioButtonDotsSpinner.TabIndex = 4;
            this.radRadioButtonDotsSpinner.TabStop = false;
            this.radRadioButtonDotsSpinner.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioButton_ToggleStateChanged);
            // 
            // radRadioButtonDotsRing
            // 
            this.radRadioButtonDotsRing.Location = new System.Drawing.Point(217, 44);
            this.radRadioButtonDotsRing.Name = "radRadioButtonDotsRing";
            this.radRadioButtonDotsRing.Size = new System.Drawing.Size(15, 15);
            this.radRadioButtonDotsRing.TabIndex = 4;
            this.radRadioButtonDotsRing.TabStop = false;
            this.radRadioButtonDotsRing.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioButton_ToggleStateChanged);
            // 
            // radRadioButtonFadingRing
            // 
            this.radRadioButtonFadingRing.Location = new System.Drawing.Point(300, 44);
            this.radRadioButtonFadingRing.Name = "radRadioButtonFadingRing";
            this.radRadioButtonFadingRing.Size = new System.Drawing.Size(15, 15);
            this.radRadioButtonFadingRing.TabIndex = 4;
            this.radRadioButtonFadingRing.TabStop = false;
            this.radRadioButtonFadingRing.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioButton_ToggleStateChanged);
            // 
            // radRadioButtonLineRing
            // 
            this.radRadioButtonLineRing.Location = new System.Drawing.Point(385, 44);
            this.radRadioButtonLineRing.Name = "radRadioButtonLineRing";
            this.radRadioButtonLineRing.Size = new System.Drawing.Size(15, 15);
            this.radRadioButtonLineRing.TabIndex = 4;
            this.radRadioButtonLineRing.TabStop = false;
            this.radRadioButtonLineRing.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioButton_ToggleStateChanged);
            // 
            // radRadioButtonSegmentedRing
            // 
            this.radRadioButtonSegmentedRing.Location = new System.Drawing.Point(469, 44);
            this.radRadioButtonSegmentedRing.Name = "radRadioButtonSegmentedRing";
            this.radRadioButtonSegmentedRing.Size = new System.Drawing.Size(15, 15);
            this.radRadioButtonSegmentedRing.TabIndex = 4;
            this.radRadioButtonSegmentedRing.TabStop = false;
            this.radRadioButtonSegmentedRing.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioButton_ToggleStateChanged);
            // 
            // radRadioButtonRotatingRings
            // 
            this.radRadioButtonRotatingRings.Location = new System.Drawing.Point(550, 44);
            this.radRadioButtonRotatingRings.Name = "radRadioButtonRotatingRings";
            this.radRadioButtonRotatingRings.Size = new System.Drawing.Size(15, 15);
            this.radRadioButtonRotatingRings.TabIndex = 4;
            this.radRadioButtonRotatingRings.TabStop = false;
            this.radRadioButtonRotatingRings.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioButton_ToggleStateChanged);
            // 
            // radButtonLoadImages
            // 
            this.radButtonLoadImages.Location = new System.Drawing.Point(4, 138);
            this.radButtonLoadImages.Name = "radButtonLoadImages";
            this.radButtonLoadImages.Size = new System.Drawing.Size(140, 24);
            this.radButtonLoadImages.TabIndex = 5;
            this.radButtonLoadImages.Text = "Load images";
            this.radButtonLoadImages.Click += new System.EventHandler(this.radButtonLoadImages_Click);
            // 
            // radPanel1
            // 
            this.radPanel1.Location = new System.Drawing.Point(4, 178);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(240, 160);
            this.radPanel1.TabIndex = 6;
            // 
            // radPanel2
            // 
            this.radPanel2.Location = new System.Drawing.Point(254, 178);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(240, 160);
            this.radPanel2.TabIndex = 7;
            // 
            // radPanel3
            // 
            this.radPanel3.Location = new System.Drawing.Point(504, 178);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(240, 160);
            this.radPanel3.TabIndex = 8;
            // 
            // radPanel4
            // 
            this.radPanel4.Location = new System.Drawing.Point(4, 346);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(240, 160);
            this.radPanel4.TabIndex = 11;
            // 
            // radPanel5
            // 
            this.radPanel5.Location = new System.Drawing.Point(254, 346);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(240, 160);
            this.radPanel5.TabIndex = 10;
            // 
            // radPanel6
            // 
            this.radPanel6.Location = new System.Drawing.Point(504, 346);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(240, 160);
            this.radPanel6.TabIndex = 9;
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.Location = new System.Drawing.Point(10, 78);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(80, 24);
            this.radWaitingBar1.TabIndex = 12;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement2);
            this.radWaitingBar1.WaitingSpeed = 80;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement2
            // 
            this.dotsLineWaitingBarIndicatorElement2.Name = "dotsLineWaitingBarIndicatorElement2";
            // 
            // radWaitingBar2
            // 
            this.radWaitingBar2.Location = new System.Drawing.Point(115, 65);
            this.radWaitingBar2.Name = "radWaitingBar2";
            this.radWaitingBar2.Size = new System.Drawing.Size(50, 50);
            this.radWaitingBar2.TabIndex = 13;
            this.radWaitingBar2.Text = "radWaitingBar2";
            this.radWaitingBar2.WaitingIndicators.Add(this.dotsSpinnerWaitingBarIndicatorElement2);
            this.radWaitingBar2.WaitingSpeed = 100;
            this.radWaitingBar2.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            // 
            // dotsSpinnerWaitingBarIndicatorElement2
            // 
            this.dotsSpinnerWaitingBarIndicatorElement2.Name = "dotsSpinnerWaitingBarIndicatorElement2";
            // 
            // radWaitingBar3
            // 
            this.radWaitingBar3.Location = new System.Drawing.Point(199, 65);
            this.radWaitingBar3.Name = "radWaitingBar3";
            this.radWaitingBar3.Size = new System.Drawing.Size(50, 50);
            this.radWaitingBar3.TabIndex = 14;
            this.radWaitingBar3.Text = "radWaitingBar3";
            this.radWaitingBar3.WaitingIndicators.Add(this.dotsRingWaitingBarIndicatorElement2);
            this.radWaitingBar3.WaitingSpeed = 20;
            this.radWaitingBar3.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsRing;
            // 
            // dotsRingWaitingBarIndicatorElement2
            // 
            this.dotsRingWaitingBarIndicatorElement2.Name = "dotsRingWaitingBarIndicatorElement2";
            // 
            // radWaitingBar4
            // 
            this.radWaitingBar4.Location = new System.Drawing.Point(282, 65);
            this.radWaitingBar4.Name = "radWaitingBar4";
            this.radWaitingBar4.Size = new System.Drawing.Size(50, 50);
            this.radWaitingBar4.TabIndex = 15;
            this.radWaitingBar4.Text = "radWaitingBar4";
            this.radWaitingBar4.WaitingIndicators.Add(this.fadingRingWaitingBarIndicatorElement2);
            this.radWaitingBar4.WaitingStep = 8;
            this.radWaitingBar4.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.FadingRing;
            // 
            // fadingRingWaitingBarIndicatorElement2
            // 
            this.fadingRingWaitingBarIndicatorElement2.Name = "fadingRingWaitingBarIndicatorElement2";
            // 
            // radWaitingBar5
            // 
            this.radWaitingBar5.Location = new System.Drawing.Point(367, 65);
            this.radWaitingBar5.Name = "radWaitingBar5";
            this.radWaitingBar5.Size = new System.Drawing.Size(50, 50);
            this.radWaitingBar5.TabIndex = 16;
            this.radWaitingBar5.Text = "radWaitingBar5";
            this.radWaitingBar5.WaitingIndicators.Add(this.lineRingWaitingBarIndicatorElement2);
            this.radWaitingBar5.WaitingSpeed = 50;
            this.radWaitingBar5.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.LineRing;
            // 
            // lineRingWaitingBarIndicatorElement2
            // 
            this.lineRingWaitingBarIndicatorElement2.Name = "lineRingWaitingBarIndicatorElement2";
            // 
            // radWaitingBar6
            // 
            this.radWaitingBar6.Location = new System.Drawing.Point(452, 65);
            this.radWaitingBar6.Name = "radWaitingBar6";
            this.radWaitingBar6.Size = new System.Drawing.Size(50, 50);
            this.radWaitingBar6.TabIndex = 17;
            this.radWaitingBar6.Text = "radWaitingBar6";
            this.radWaitingBar6.WaitingIndicators.Add(this.segmentedRingWaitingBarIndicatorElement2);
            this.radWaitingBar6.WaitingSpeed = 20;
            this.radWaitingBar6.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.SegmentedRing;
            // 
            // segmentedRingWaitingBarIndicatorElement2
            // 
            this.segmentedRingWaitingBarIndicatorElement2.Name = "segmentedRingWaitingBarIndicatorElement2";
            // 
            // radWaitingBar7
            // 
            this.radWaitingBar7.Location = new System.Drawing.Point(532, 65);
            this.radWaitingBar7.Name = "radWaitingBar7";
            this.radWaitingBar7.Size = new System.Drawing.Size(50, 50);
            this.radWaitingBar7.TabIndex = 18;
            this.radWaitingBar7.Text = "radWaitingBar7";
            this.radWaitingBar7.WaitingIndicators.Add(this.rotatingRingsWaitingBarIndicatorElement2);
            this.radWaitingBar7.WaitingStep = 7;
            this.radWaitingBar7.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.RotatingRings;
            // 
            // rotatingRingsWaitingBarIndicatorElement2
            // 
            this.rotatingRingsWaitingBarIndicatorElement2.Name = "rotatingRingsWaitingBarIndicatorElement2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radWaitingBar7);
            this.Controls.Add(this.radWaitingBar6);
            this.Controls.Add(this.radWaitingBar5);
            this.Controls.Add(this.radWaitingBar4);
            this.Controls.Add(this.radWaitingBar3);
            this.Controls.Add(this.radWaitingBar2);
            this.Controls.Add(this.radWaitingBar1);
            this.Controls.Add(this.radPanel4);
            this.Controls.Add(this.radPanel3);
            this.Controls.Add(this.radPanel5);
            this.Controls.Add(this.radPanel2);
            this.Controls.Add(this.radPanel6);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.radButtonLoadImages);
            this.Controls.Add(this.radRadioButtonRotatingRings);
            this.Controls.Add(this.radRadioButtonSegmentedRing);
            this.Controls.Add(this.radRadioButtonLineRing);
            this.Controls.Add(this.radRadioButtonFadingRing);
            this.Controls.Add(this.radRadioButtonDotsRing);
            this.Controls.Add(this.radRadioButtonDotsSpinner);
            this.Controls.Add(this.radRadioButtonDotsLine);
            this.Controls.Add(this.radLabel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1308, 850);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radLabel1, 0);
            this.Controls.SetChildIndex(this.radRadioButtonDotsLine, 0);
            this.Controls.SetChildIndex(this.radRadioButtonDotsSpinner, 0);
            this.Controls.SetChildIndex(this.radRadioButtonDotsRing, 0);
            this.Controls.SetChildIndex(this.radRadioButtonFadingRing, 0);
            this.Controls.SetChildIndex(this.radRadioButtonLineRing, 0);
            this.Controls.SetChildIndex(this.radRadioButtonSegmentedRing, 0);
            this.Controls.SetChildIndex(this.radRadioButtonRotatingRings, 0);
            this.Controls.SetChildIndex(this.radButtonLoadImages, 0);
            this.Controls.SetChildIndex(this.radPanel1, 0);
            this.Controls.SetChildIndex(this.radPanel6, 0);
            this.Controls.SetChildIndex(this.radPanel2, 0);
            this.Controls.SetChildIndex(this.radPanel5, 0);
            this.Controls.SetChildIndex(this.radPanel3, 0);
            this.Controls.SetChildIndex(this.radPanel4, 0);
            this.Controls.SetChildIndex(this.radWaitingBar1, 0);
            this.Controls.SetChildIndex(this.radWaitingBar2, 0);
            this.Controls.SetChildIndex(this.radWaitingBar3, 0);
            this.Controls.SetChildIndex(this.radWaitingBar4, 0);
            this.Controls.SetChildIndex(this.radWaitingBar5, 0);
            this.Controls.SetChildIndex(this.radWaitingBar6, 0);
            this.Controls.SetChildIndex(this.radWaitingBar7, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonDotsLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonDotsSpinner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonDotsRing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonFadingRing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonLineRing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonSegmentedRing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonRotatingRings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonLoadImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonDotsLine;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonDotsSpinner;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonDotsRing;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonFadingRing;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonLineRing;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonSegmentedRing;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonRotatingRings;
        private Telerik.WinControls.UI.RadButton radButtonLoadImages;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar2;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar3;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar4;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar5;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar6;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar7;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement2;
        private Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement dotsSpinnerWaitingBarIndicatorElement2;
        private Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement dotsRingWaitingBarIndicatorElement2;
        private Telerik.WinControls.UI.FadingRingWaitingBarIndicatorElement fadingRingWaitingBarIndicatorElement2;
        private Telerik.WinControls.UI.LineRingWaitingBarIndicatorElement lineRingWaitingBarIndicatorElement2;
        private Telerik.WinControls.UI.SegmentedRingWaitingBarIndicatorElement segmentedRingWaitingBarIndicatorElement2;
        private Telerik.WinControls.UI.RotatingRingsWaitingBarIndicatorElement rotatingRingsWaitingBarIndicatorElement2;
    }
}