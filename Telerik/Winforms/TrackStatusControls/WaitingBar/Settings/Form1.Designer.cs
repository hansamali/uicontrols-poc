﻿namespace Telerik.Examples.WinControls.TrackStatusControls.WaitingBar.Settings
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.lineRingWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.LineRingWaitingBarIndicatorElement();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.lineThicknessSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.elementCountSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radiusSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.innerRadiusSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.elementColorColorBox = new Telerik.WinControls.UI.RadColorBox();
            this.elementColor2ColorBox = new Telerik.WinControls.UI.RadColorBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.elementColor3ColorBox = new Telerik.WinControls.UI.RadColorBox();
            this.elementColor4ColorBox = new Telerik.WinControls.UI.RadColorBox();
            this.gradientPercentageSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.gradientPercentage2SpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.numberOfColorsSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.animationSpeedSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.animationStepSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineThicknessSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementCountSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radiusSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.innerRadiusSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementColorColorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementColor2ColorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementColor3ColorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementColor4ColorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradientPercentageSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradientPercentage2SpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfColorsSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationSpeedSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationStepSpinEditor)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1048, 32);
            // 
            // themePanel
            // 
            this.themePanel.Location = new System.Drawing.Point(1048, 198);
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.Location = new System.Drawing.Point(24, 32);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(153, 136);
            this.radWaitingBar1.TabIndex = 2;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.WaitingIndicators.Add(this.lineRingWaitingBarIndicatorElement2);
            this.radWaitingBar1.WaitingSpeed = 50;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.LineRing;
            // 
            // lineRingWaitingBarIndicatorElement2
            // 
            this.lineRingWaitingBarIndicatorElement2.Name = "lineRingWaitingBarIndicatorElement2";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(195, 32);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(73, 18);
            this.radLabel1.TabIndex = 3;
            this.radLabel1.Text = "Line settings";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(440, 32);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(80, 18);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "Color settings";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(195, 57);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(78, 18);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "Line Thickness";
            // 
            // lineThicknessSpinEditor
            // 
            this.lineThicknessSpinEditor.DecimalPlaces = 1;
            this.lineThicknessSpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.lineThicknessSpinEditor.Location = new System.Drawing.Point(199, 82);
            this.lineThicknessSpinEditor.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.lineThicknessSpinEditor.Name = "lineThicknessSpinEditor";
            this.lineThicknessSpinEditor.Size = new System.Drawing.Size(100, 20);
            this.lineThicknessSpinEditor.TabIndex = 6;
            this.lineThicknessSpinEditor.TabStop = false;
            this.lineThicknessSpinEditor.Value = new decimal(new int[] {
            20,
            0,
            0,
            65536});
            this.lineThicknessSpinEditor.ValueChanged += new System.EventHandler(this.lineThicknessSpinEditor_ValueChanged);
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(309, 57);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(80, 18);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Text = "Element Count";
            // 
            // elementCountSpinEditor
            // 
            this.elementCountSpinEditor.Location = new System.Drawing.Point(313, 82);
            this.elementCountSpinEditor.Name = "elementCountSpinEditor";
            this.elementCountSpinEditor.Size = new System.Drawing.Size(100, 20);
            this.elementCountSpinEditor.TabIndex = 8;
            this.elementCountSpinEditor.TabStop = false;
            this.elementCountSpinEditor.Value = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.elementCountSpinEditor.ValueChanged += new System.EventHandler(this.elementCountSpinEditor_ValueChanged);
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(195, 119);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(39, 18);
            this.radLabel5.TabIndex = 9;
            this.radLabel5.Text = "Radius";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(309, 119);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(68, 18);
            this.radLabel6.TabIndex = 10;
            this.radLabel6.Text = "Inner Radius";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(195, 180);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(97, 18);
            this.radLabel7.TabIndex = 11;
            this.radLabel7.Text = "Rotation Direction";
            // 
            // radiusSpinEditor
            // 
            this.radiusSpinEditor.Location = new System.Drawing.Point(199, 144);
            this.radiusSpinEditor.Name = "radiusSpinEditor";
            this.radiusSpinEditor.Size = new System.Drawing.Size(100, 20);
            this.radiusSpinEditor.TabIndex = 12;
            this.radiusSpinEditor.TabStop = false;
            this.radiusSpinEditor.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.radiusSpinEditor.ValueChanging += new Telerik.WinControls.UI.ValueChangingEventHandler(this.radiusSpinEditor_ValueChanging);
            // 
            // innerRadiusSpinEditor
            // 
            this.innerRadiusSpinEditor.Location = new System.Drawing.Point(313, 144);
            this.innerRadiusSpinEditor.Name = "innerRadiusSpinEditor";
            this.innerRadiusSpinEditor.Size = new System.Drawing.Size(100, 20);
            this.innerRadiusSpinEditor.TabIndex = 13;
            this.innerRadiusSpinEditor.TabStop = false;
            this.innerRadiusSpinEditor.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.innerRadiusSpinEditor.ValueChanging += new Telerik.WinControls.UI.ValueChangingEventHandler(this.innerRadiusSpinEditor_ValueChanging);
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.Location = new System.Drawing.Point(199, 204);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(100, 20);
            this.radDropDownList1.TabIndex = 14;
            this.radDropDownList1.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownList1_SelectedIndexChanged);
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(439, 237);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(96, 18);
            this.radLabel8.TabIndex = 15;
            this.radLabel8.Text = "Number of Colors";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(439, 57);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(77, 18);
            this.radLabel9.TabIndex = 16;
            this.radLabel9.Text = "Element Color";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(553, 57);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(86, 18);
            this.radLabel10.TabIndex = 17;
            this.radLabel10.Text = "Element Color 2";
            // 
            // elementColorColorBox
            // 
            this.elementColorColorBox.Location = new System.Drawing.Point(443, 82);
            this.elementColorColorBox.Name = "elementColorColorBox";
            this.elementColorColorBox.Size = new System.Drawing.Size(100, 20);
            this.elementColorColorBox.TabIndex = 18;
            this.elementColorColorBox.Text = "radColorBox1";
            this.elementColorColorBox.Value = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(58)))), ((int)(((byte)(131)))));
            this.elementColorColorBox.ValueChanged += new System.EventHandler(this.elementColorColorBox_ValueChanged);
            // 
            // elementColor2ColorBox
            // 
            this.elementColor2ColorBox.Location = new System.Drawing.Point(557, 82);
            this.elementColor2ColorBox.Name = "elementColor2ColorBox";
            this.elementColor2ColorBox.Size = new System.Drawing.Size(100, 20);
            this.elementColor2ColorBox.TabIndex = 19;
            this.elementColor2ColorBox.Text = "radColorBox2";
            this.elementColor2ColorBox.Value = System.Drawing.Color.Transparent;
            this.elementColor2ColorBox.ValueChanged += new System.EventHandler(this.elementColor2ColorBox_ValueChanged);
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(439, 118);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(86, 18);
            this.radLabel11.TabIndex = 20;
            this.radLabel11.Text = "Element Color 3";
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(553, 118);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(86, 18);
            this.radLabel12.TabIndex = 21;
            this.radLabel12.Text = "Element Color 4";
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(439, 180);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(75, 18);
            this.radLabel13.TabIndex = 21;
            this.radLabel13.Text = "Gradient Stop";
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(553, 180);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(85, 18);
            this.radLabel14.TabIndex = 21;
            this.radLabel14.Text = "Gradient Stop 2";
            // 
            // elementColor3ColorBox
            // 
            this.elementColor3ColorBox.Location = new System.Drawing.Point(443, 144);
            this.elementColor3ColorBox.Name = "elementColor3ColorBox";
            this.elementColor3ColorBox.Size = new System.Drawing.Size(100, 20);
            this.elementColor3ColorBox.TabIndex = 22;
            this.elementColor3ColorBox.Text = "radColorBox1";
            this.elementColor3ColorBox.Value = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(137)))), ((int)(((byte)(191)))));
            this.elementColor3ColorBox.ValueChanged += new System.EventHandler(this.elementColor3ColorBox_ValueChanged);
            // 
            // elementColor4ColorBox
            // 
            this.elementColor4ColorBox.Location = new System.Drawing.Point(557, 144);
            this.elementColor4ColorBox.Name = "elementColor4ColorBox";
            this.elementColor4ColorBox.Size = new System.Drawing.Size(100, 20);
            this.elementColor4ColorBox.TabIndex = 23;
            this.elementColor4ColorBox.Text = "radColorBox2";
            this.elementColor4ColorBox.Value = System.Drawing.Color.Transparent;
            this.elementColor4ColorBox.ValueChanged += new System.EventHandler(this.elementColor4ColorBox_ValueChanged);
            // 
            // gradientPercentageSpinEditor
            // 
            this.gradientPercentageSpinEditor.DecimalPlaces = 2;
            this.gradientPercentageSpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.gradientPercentageSpinEditor.Location = new System.Drawing.Point(443, 204);
            this.gradientPercentageSpinEditor.Name = "gradientPercentageSpinEditor";
            this.gradientPercentageSpinEditor.Size = new System.Drawing.Size(100, 20);
            this.gradientPercentageSpinEditor.TabIndex = 24;
            this.gradientPercentageSpinEditor.TabStop = false;
            this.gradientPercentageSpinEditor.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.gradientPercentageSpinEditor.ValueChanged += new System.EventHandler(this.gradientPercentageSpinEditor_ValueChanged);
            // 
            // gradientPercentage2SpinEditor
            // 
            this.gradientPercentage2SpinEditor.DecimalPlaces = 2;
            this.gradientPercentage2SpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.gradientPercentage2SpinEditor.Location = new System.Drawing.Point(557, 204);
            this.gradientPercentage2SpinEditor.Name = "gradientPercentage2SpinEditor";
            this.gradientPercentage2SpinEditor.Size = new System.Drawing.Size(100, 20);
            this.gradientPercentage2SpinEditor.TabIndex = 25;
            this.gradientPercentage2SpinEditor.TabStop = false;
            this.gradientPercentage2SpinEditor.Value = new decimal(new int[] {
            67,
            0,
            0,
            131072});
            this.gradientPercentage2SpinEditor.ValueChanged += new System.EventHandler(this.gradientPercentage2SpinEditor_ValueChanged);
            // 
            // numberOfColorsSpinEditor
            // 
            this.numberOfColorsSpinEditor.Location = new System.Drawing.Point(443, 261);
            this.numberOfColorsSpinEditor.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numberOfColorsSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberOfColorsSpinEditor.Name = "numberOfColorsSpinEditor";
            this.numberOfColorsSpinEditor.Size = new System.Drawing.Size(100, 20);
            this.numberOfColorsSpinEditor.TabIndex = 26;
            this.numberOfColorsSpinEditor.TabStop = false;
            this.numberOfColorsSpinEditor.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numberOfColorsSpinEditor.ValueChanged += new System.EventHandler(this.numberOfColorsSpinEditor_ValueChanged);
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel15.Location = new System.Drawing.Point(684, 31);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(107, 18);
            this.radLabel15.TabIndex = 27;
            this.radLabel15.Text = "Animation settings";
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(684, 56);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(92, 18);
            this.radLabel16.TabIndex = 28;
            this.radLabel16.Text = "Animation Speed";
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(684, 118);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(83, 18);
            this.radLabel17.TabIndex = 29;
            this.radLabel17.Text = "Animation Step";
            // 
            // animationSpeedSpinEditor
            // 
            this.animationSpeedSpinEditor.Location = new System.Drawing.Point(687, 82);
            this.animationSpeedSpinEditor.Name = "animationSpeedSpinEditor";
            this.animationSpeedSpinEditor.Size = new System.Drawing.Size(100, 20);
            this.animationSpeedSpinEditor.TabIndex = 30;
            this.animationSpeedSpinEditor.TabStop = false;
            this.animationSpeedSpinEditor.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.animationSpeedSpinEditor.ValueChanged += new System.EventHandler(this.animationSpeedSpinEditor_ValueChanged);
            // 
            // animationStepSpinEditor
            // 
            this.animationStepSpinEditor.Location = new System.Drawing.Point(687, 144);
            this.animationStepSpinEditor.Name = "animationStepSpinEditor";
            this.animationStepSpinEditor.Size = new System.Drawing.Size(100, 20);
            this.animationStepSpinEditor.TabIndex = 31;
            this.animationStepSpinEditor.TabStop = false;
            this.animationStepSpinEditor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.animationStepSpinEditor.ValueChanged += new System.EventHandler(this.animationStepSpinEditor_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.animationStepSpinEditor);
            this.Controls.Add(this.animationSpeedSpinEditor);
            this.Controls.Add(this.radLabel17);
            this.Controls.Add(this.radLabel16);
            this.Controls.Add(this.radLabel15);
            this.Controls.Add(this.numberOfColorsSpinEditor);
            this.Controls.Add(this.gradientPercentage2SpinEditor);
            this.Controls.Add(this.gradientPercentageSpinEditor);
            this.Controls.Add(this.elementColor4ColorBox);
            this.Controls.Add(this.elementColor3ColorBox);
            this.Controls.Add(this.radLabel14);
            this.Controls.Add(this.radLabel13);
            this.Controls.Add(this.radLabel12);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.elementColor2ColorBox);
            this.Controls.Add(this.elementColorColorBox);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.radDropDownList1);
            this.Controls.Add(this.innerRadiusSpinEditor);
            this.Controls.Add(this.radiusSpinEditor);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.elementCountSpinEditor);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.lineThicknessSpinEditor);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radWaitingBar1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1548, 1000);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radWaitingBar1, 0);
            this.Controls.SetChildIndex(this.radLabel1, 0);
            this.Controls.SetChildIndex(this.radLabel2, 0);
            this.Controls.SetChildIndex(this.radLabel3, 0);
            this.Controls.SetChildIndex(this.lineThicknessSpinEditor, 0);
            this.Controls.SetChildIndex(this.radLabel4, 0);
            this.Controls.SetChildIndex(this.elementCountSpinEditor, 0);
            this.Controls.SetChildIndex(this.radLabel5, 0);
            this.Controls.SetChildIndex(this.radLabel6, 0);
            this.Controls.SetChildIndex(this.radLabel7, 0);
            this.Controls.SetChildIndex(this.radiusSpinEditor, 0);
            this.Controls.SetChildIndex(this.innerRadiusSpinEditor, 0);
            this.Controls.SetChildIndex(this.radDropDownList1, 0);
            this.Controls.SetChildIndex(this.radLabel8, 0);
            this.Controls.SetChildIndex(this.radLabel9, 0);
            this.Controls.SetChildIndex(this.radLabel10, 0);
            this.Controls.SetChildIndex(this.elementColorColorBox, 0);
            this.Controls.SetChildIndex(this.elementColor2ColorBox, 0);
            this.Controls.SetChildIndex(this.radLabel11, 0);
            this.Controls.SetChildIndex(this.radLabel12, 0);
            this.Controls.SetChildIndex(this.radLabel13, 0);
            this.Controls.SetChildIndex(this.radLabel14, 0);
            this.Controls.SetChildIndex(this.elementColor3ColorBox, 0);
            this.Controls.SetChildIndex(this.elementColor4ColorBox, 0);
            this.Controls.SetChildIndex(this.gradientPercentageSpinEditor, 0);
            this.Controls.SetChildIndex(this.gradientPercentage2SpinEditor, 0);
            this.Controls.SetChildIndex(this.numberOfColorsSpinEditor, 0);
            this.Controls.SetChildIndex(this.radLabel15, 0);
            this.Controls.SetChildIndex(this.radLabel16, 0);
            this.Controls.SetChildIndex(this.radLabel17, 0);
            this.Controls.SetChildIndex(this.animationSpeedSpinEditor, 0);
            this.Controls.SetChildIndex(this.animationStepSpinEditor, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineThicknessSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementCountSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radiusSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.innerRadiusSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementColorColorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementColor2ColorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementColor3ColorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementColor4ColorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradientPercentageSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradientPercentage2SpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfColorsSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationSpeedSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationStepSpinEditor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadSpinEditor lineThicknessSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor elementCountSpinEditor;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadSpinEditor radiusSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor innerRadiusSpinEditor;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadColorBox elementColorColorBox;
        private Telerik.WinControls.UI.RadColorBox elementColor2ColorBox;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadSpinEditor gradientPercentageSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor gradientPercentage2SpinEditor;
        private Telerik.WinControls.UI.RadColorBox elementColor3ColorBox;
        private Telerik.WinControls.UI.RadColorBox elementColor4ColorBox;
        private Telerik.WinControls.UI.RadSpinEditor numberOfColorsSpinEditor;
        private Telerik.WinControls.UI.LineRingWaitingBarIndicatorElement lineRingWaitingBarIndicatorElement2;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadSpinEditor animationSpeedSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor animationStepSpinEditor;
    }
}